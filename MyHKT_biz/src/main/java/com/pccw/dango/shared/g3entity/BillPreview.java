package com.pccw.dango.shared.g3entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

//import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement(name = "BillPreview")
public class BillPreview implements Serializable 
{
    private static final long serialVersionUID = -6084889501142446421L;
    
    private List<BillPreviewItemDTO> previewItems;
	private String currentBalance;
	private String billAmount;
	private String billDate;
	private String acctNum;
	private String dueDate;
	private Date displayBillDate;
	private Date displayDueDate;
	private String isDate;
	private String billMsg;
	private String receiptAcctNo;
	private String isTos;
	// 20110322 - Added for Web Service - Start 
	private String loginID;
	private boolean noBillFlg = false;
	private boolean noBillMsgFlg = false;
	// 20110322 - Added for Web Service - end 
	
	public List<BillPreviewItemDTO> getPreviewItems() {
		return previewItems;
	}
	public void setPreviewItems(List<BillPreviewItemDTO> previewItems) {
		this.previewItems = previewItems;
	}
	public String getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getBillAmount() {
		return billAmount;
	}
	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}
	public String getBillDate() {
		return billDate;
	}
	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
	public String getAcctNum() {
		return acctNum;
	}
	public void setAcctNum(String acctNum) {
		this.acctNum = acctNum;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public Date getDisplayBillDate() {
		return displayBillDate;
	}
	public void setDisplayBillDate(Date displayBillDate) {
		this.displayBillDate = displayBillDate;
	}
	public Date getDisplayDueDate() {
		return displayDueDate;
	}
	public void setDisplayDueDate(Date displayDueDate) {
		this.displayDueDate = displayDueDate;
	}
	public String getIsDate() {
		return isDate;
	}
	public void setIsDate(String isDate) {
		this.isDate = isDate;
	}
	public String getBillMsg() {
		return billMsg;
	}
	public void setBillMsg(String billMsg) {
		this.billMsg = billMsg;
	}
	public String getReceiptAcctNo() {
		return receiptAcctNo;
	}
	public void setReceiptAcctNo(String receiptAcctNo) {
		this.receiptAcctNo = receiptAcctNo;
	}
	public String getIsTos() {
		return isTos;
	}
	public void setIsTos(String isTos) {
		this.isTos = isTos;
	}
	// 20110322 - Added for Web Service - Start 
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	public boolean isNoBillFlg() {
		return noBillFlg;
	}
	public void setNoBillFlg(boolean noBillFlg) {
		this.noBillFlg = noBillFlg;
	}
	public boolean isNoBillMsgFlg() {
		return noBillMsgFlg;
	}
	public void setNoBillMsgFlg(boolean noBillMsgFlg) {
		this.noBillMsgFlg = noBillMsgFlg;
	}
	// 20110322 - Added for Web Service - end
}
