/*
    Crate for Bill Inquiry - operation for One Account Only

    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.Account;
import com.pccw.dango.shared.entity.BillList;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.BillPreview;
import com.pccw.dango.shared.g3entity.G3OutstandingBalanceDTO;
import com.pccw.dango.shared.g3entity.G3ServiceCallBarStatusWS;

public class BinqCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 1626497264865094319L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private Account                 iAcct;              /* Account                                       */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    private String                  iBillDate;          /* Bill Date                                     */
    
    private BillList                oBillList;          /* List of Bill for this A/C                     */
    private String                  oSummUrl;           /* Summary URL (for IMS use)                     */
    private BillPreview             oBillPreview;       /* Bill Preview                                  */
    private G3OutstandingBalanceDTO oG3OutstandingBalanceDTO;
                                                        /* Outstanding Balance (for MOB use)             */
    private G3ServiceCallBarStatusWS    
                                    oG3ServiceCallBarStatusWS;
                                                        /* Svc Call Bar status (for MOB use)             */ 

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public BinqCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearIAcct();
        clearISubnRec();
        clearIBillDate();
        clearOBillList();
        clearOSummUrl();
        clearOBillPreview();
        clearOG3OutstandingBalanceDTO();
        clearOG3ServiceCallBarStatusWS();
    }


    public BinqCra copyFrom(BinqCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setIAcct(rSrc.getIAcct());
        setISubnRec(rSrc.getISubnRec());
        setIBillDate(rSrc.getIBillDate());
        setOBillList(rSrc.getOBillList());
        setOSummUrl(rSrc.getOSummUrl());
        setOBillPreview(rSrc.getOBillPreview());
        setOG3OutstandingBalanceDTO(rSrc.getOG3OutstandingBalanceDTO());
        setOG3ServiceCallBarStatusWS(rSrc.getOG3ServiceCallBarStatusWS());

        return (this);
    }


    public BinqCra copyTo(BinqCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BinqCra copyMe()
    {
        BinqCra                     rDes;

        rDes = new BinqCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearIAcct()
    {
        iAcct = new Account();
    }


    public void setIAcct(Account rArg)
    {
        iAcct = rArg;
    }


    public Account getIAcct()
    {
        return (iAcct);
    }

    
    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }


    public void clearIBillDate()
    {
        iBillDate = "";
    }


    public void setIBillDate(String rArg)
    {
        iBillDate = rArg;
    }


    public String getIBillDate()
    {
        return (iBillDate);
    }


    public void clearOBillList()
    {
        setOBillList(new BillList());
    }


    public void setOBillList(BillList rArg)
    {
        oBillList = rArg;
    }


    public BillList getOBillList()
    {
        return (oBillList);
    }


    public void clearOSummUrl()
    {
        oSummUrl = "";
    }


    public void setOSummUrl(String rArg)
    {
        oSummUrl = rArg;
    }


    public String getOSummUrl()
    {
        return (oSummUrl);
    }


    public void clearOBillPreview()
    {
        oBillPreview = new BillPreview();
    }


    public void setOBillPreview(BillPreview rArg)
    {
        oBillPreview = rArg;
    }


    public BillPreview getOBillPreview()
    {
        return (oBillPreview);
    }

    
    public void clearOG3OutstandingBalanceDTO()
    {
        oG3OutstandingBalanceDTO = new G3OutstandingBalanceDTO();
    }


    public void setOG3OutstandingBalanceDTO(G3OutstandingBalanceDTO rArg)
    {
        oG3OutstandingBalanceDTO = rArg;
    }


    public G3OutstandingBalanceDTO getOG3OutstandingBalanceDTO()
    {
        return (oG3OutstandingBalanceDTO);
    }

    
    public void clearOG3ServiceCallBarStatusWS()
    {
        oG3ServiceCallBarStatusWS = new G3ServiceCallBarStatusWS();
    }


    public void setOG3ServiceCallBarStatusWS(G3ServiceCallBarStatusWS rArg)
    {
        oG3ServiceCallBarStatusWS = rArg;
    }


    public G3ServiceCallBarStatusWS getOG3ServiceCallBarStatusWS()
    {
        return (oG3ServiceCallBarStatusWS);
    }
}
