/*
    Class for BOM Appointment
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class BomAppt implements Serializable 
{
    private static final long serialVersionUID = 5394859850762386876L;
    
    private String                  ocId;               /* OC ID                                         */
    private String                  srvId;              /* Service ID                                    */
    private String                  acctNum;            /* Account Number                                */
    private String                  tvAcctNum;          /* TV Account Number                             */
    private String                  ordTy;              /* Ordinary Type                                 */
    private String                  loginId;            /* Login ID                                      */
    private String                  oAdr;               /* Old Address                                   */
    private String                  Adr;                /* Address                                       */
    private String                  srvBndy;            /* Service Boundry                               */
    private String                  srvTy;              /* Service Type                                  */
    private String                  datCd;              /* Data Code                                     */
    private String                  apptStDT;           /* Appointment Start Datetime                    */
    private String                  apptEnDT;           /* Appointment End Datetime                      */
    private String                  pwApptStDT;         /* Pre-wire Appointment Start Datetime           */
    private String                  pwApptEnDT;         /* Pre-wire Appointment End Datetime             */
    private String                  drgOrdNum;          /* Dragon Ordinary Number                        */
    private String                  grpTy;              /* Group Type                                    */
    private String                  grpNum;             /* Group Number                                  */
    private String                  peInd;              /* PE Indicator                                  */
    private boolean                 clone;              /* Clone?                                        */

    
    public static String            TY_TEL      = "TEL";
    public static String            TY_IMS      = "IMS";
    public static String            GP_EYE      = "EYE";
    public static String            GP_EYEX     = "EYEX";
    public static String            GP_EYE3     = "EYE3";


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public BomAppt()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }

    


    protected void init()
    {
    }


    public void clear()
    {
        clearOcId();
        clearSrvId();
        clearAcctNum();
        clearTvAcctNum();
        clearOrdTy();
        clearLoginId();
        clearOAdr();
        clearAdr();
        clearSrvBndy();
        clearSrvTy();
        clearDatCd();
        clearApptStDT();
        clearApptEnDT();
        clearPwApptStDT();
        clearPwApptEnDT();
        clearDrgOrdNum();
        clearGrpTy();
        clearGrpNum();
        clearPeInd();
        clearClone();
    }


    public BomAppt copyFrom(BomAppt rSrc)
    {
        setOcId(rSrc.getOcId());
        setSrvId(rSrc.getSrvId());
        setAcctNum(rSrc.getAcctNum());
        setTvAcctNum(rSrc.getTvAcctNum());
        setOrdTy(rSrc.getOrdTy());
        setLoginId(rSrc.getLoginId());
        setOAdr(rSrc.getOAdr());
        setAdr(rSrc.getAdr());
        setSrvBndy(rSrc.getSrvBndy());
        setSrvTy(rSrc.getSrvTy());
        setDatCd(rSrc.getDatCd());
        setApptStDT(rSrc.getApptStDT());
        setApptEnDT(rSrc.getApptEnDT());
        setPwApptStDT(rSrc.getPwApptStDT());
        setPwApptEnDT(rSrc.getPwApptEnDT());
        setDrgOrdNum(rSrc.getDrgOrdNum());
        setGrpTy(rSrc.getGrpTy());
        setGrpNum(rSrc.getGrpNum());
        setPeInd(rSrc.getPeInd());
        setClone(rSrc.isClone());

        return (this);
    }


    public BomAppt copyTo(BomAppt rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BomAppt copyMe()
    {
        BomAppt                    rDes;

        rDes = new BomAppt();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearOcId()
    {
        ocId = "";
    }


    public void setOcId(String rArg)
    {
        ocId = rArg;
    }


    public String getOcId()
    {
        return (ocId);
    }


    public void clearSrvId()
    {
        srvId = "";
    }


    public void setSrvId(String rArg)
    {
        srvId = rArg;
    }


    public String getSrvId()
    {
        return (srvId);
    }


    public void clearAcctNum()
    {
        acctNum = "";
    }


    public void setAcctNum(String rArg)
    {
        acctNum = rArg;
    }


    public String getAcctNum()
    {
        return (acctNum);
    }


    public void clearTvAcctNum()
    {
        tvAcctNum = "";
    }


    public void setTvAcctNum(String rArg)
    {
        tvAcctNum = rArg;
    }


    public String getTvAcctNum()
    {
        return (tvAcctNum);
    }


    public void clearOrdTy()
    {
        ordTy = "";
    }


    public void setOrdTy(String rArg)
    {
        ordTy = rArg;
    }


    public String getOrdTy()
    {
        return (ordTy);
    }


    public void clearLoginId()
    {
        loginId = "";
    }


    public void setLoginId(String rArg)
    {
        loginId = rArg;
    }


    public String getLoginId()
    {
        return (loginId);
    }


    public void clearOAdr()
    {
        oAdr = "";
    }


    public void setOAdr(String rArg)
    {
        oAdr = rArg;
    }


    public String getOAdr()
    {
        return (oAdr);
    }


    public void clearAdr()
    {
        Adr = "";
    }


    public void setAdr(String rArg)
    {
        Adr = rArg;
    }


    public String getAdr()
    {
        return (Adr);
    }


    public void clearSrvBndy()
    {
        srvBndy = "";
    }


    public void setSrvBndy(String rArg)
    {
        srvBndy = rArg;
    }


    public String getSrvBndy()
    {
        return (srvBndy);
    }


    public void clearSrvTy()
    {
        srvTy = "";
    }


    public void setSrvTy(String rArg)
    {
        srvTy = rArg;
    }


    public String getSrvTy()
    {
        return (srvTy);
    }


    public void clearDatCd()
    {
        datCd = "";
    }


    public void setDatCd(String rArg)
    {
        datCd = rArg;
    }


    public String getDatCd()
    {
        return (datCd);
    }


    public void clearApptStDT()
    {
        apptStDT = "";
    }


    public void setApptStDT(String rArg)
    {
        apptStDT = rArg;
    }


    public String getApptStDT()
    {
        return (apptStDT);
    }


    public void clearApptEnDT()
    {
        apptEnDT = "";
    }


    public void setApptEnDT(String rArg)
    {
        apptEnDT = rArg;
    }


    public String getApptEnDT()
    {
        return (apptEnDT);
    }


    public void clearPwApptStDT()
    {
        pwApptStDT = "";
    }


    public void setPwApptStDT(String rArg)
    {
        pwApptStDT = rArg;
    }


    public String getPwApptStDT()
    {
        return (pwApptStDT);
    }


    public void clearPwApptEnDT()
    {
        pwApptEnDT = "";
    }


    public void setPwApptEnDT(String rArg)
    {
        pwApptEnDT = rArg;
    }


    public String getPwApptEnDT()
    {
        return (pwApptEnDT);
    }


    public void clearDrgOrdNum()
    {
        drgOrdNum = "";
    }


    public void setDrgOrdNum(String rArg)
    {
        drgOrdNum = rArg;
    }


    public String getDrgOrdNum()
    {
        return (drgOrdNum);
    }


    public void clearGrpTy()
    {
        grpTy = "";
    }


    public void setGrpTy(String rArg)
    {
        grpTy = rArg;
    }


    public String getGrpTy()
    {
        return (grpTy);
    }


    public void clearGrpNum()
    {
        grpNum = "";
    }


    public void setGrpNum(String rArg)
    {
        grpNum = rArg;
    }


    public String getGrpNum()
    {
        return (grpNum);
    }


    public void clearPeInd()
    {
        peInd = "";
    }


    public void setPeInd(String rArg)
    {
        peInd = rArg;
    }


    public String getPeInd()
    {
        return (peInd);
    }


    public void clearClone()
    {
        clone = false;
    }


    public void setClone(boolean rArg)
    {
        clone = rArg;
    }


    public boolean isClone()
    {
        return (clone);
    }
}
