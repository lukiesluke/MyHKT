/*
    Crate for Plan (TV)
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.SrvPlan;
import com.pccw.dango.shared.entity.SubnRec;

public class PlanTvCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -5069241945547034237L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    
    private SrvPlan[]               oPlanAry;           /* Array of Service Plan                         */
    private SrvPlan[]               oVasAry;            /* Array of VAS                                  */
    private String                  oNowDollBal;        /* Now Dollar Balance                            */
    private String                  oNowDollExp;        /* Now Dollar Expiry Date                        */
    private Boolean                 oPonCvg;            /* Pon Coverage                                  */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public PlanTvCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearISubnRec();
        clearOPlanAry();
        clearOVasAry();
        clearONowDollBal();
        clearONowDollExp();
        clearOPonCvg();
    }


    public PlanTvCra copyFrom(PlanTvCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setISubnRec(rSrc.getISubnRec());
        setOPlanAry(rSrc.getOPlanAry());
        setOVasAry(rSrc.getOVasAry());
        setONowDollBal(rSrc.getONowDollBal());
        setONowDollExp(rSrc.getONowDollExp());
        setOPonCvg(rSrc.getOPonCvg());

        return (this);
    }


    public PlanTvCra copyTo(PlanTvCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public PlanTvCra copyMe()
    {
        PlanTvCra                   rDes;

        rDes = new PlanTvCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }


    public void clearOPlanAry()
    {
        oPlanAry = new SrvPlan[0];
    }


    public void setOPlanAry(SrvPlan[] rArg)
    {
        oPlanAry = rArg;
    }


    public SrvPlan[] getOPlanAry()
    {
        return (oPlanAry);
    }

    
    public void clearOVasAry()
    {
        oVasAry = new SrvPlan[0];
    }


    public void setOVasAry(SrvPlan[] rArg)
    {
        oVasAry = rArg;
    }


    public SrvPlan[] getOVasAry()
    {
        return (oVasAry);
    }
    

    public void clearONowDollBal()
    {
        oNowDollBal = "";
    }


    public void setONowDollBal(String rArg)
    {
        oNowDollBal = rArg;
    }


    public String getONowDollBal()
    {
        return (oNowDollBal);
    }


    public void clearONowDollExp()
    {
        oNowDollExp = "";
    }


    public void setONowDollExp(String rArg)
    {
        oNowDollExp = rArg;
    }


    public String getONowDollExp()
    {
        return (oNowDollExp);
    }


    public void clearOPonCvg()
    {
        oPonCvg = new Boolean(false);
    }


    public void setOPonCvg(Boolean rArg)
    {
        oPonCvg = rArg;
    }


    public Boolean getOPonCvg()
    {
        return (oPonCvg);
    }
}
