/*
    Class for LOB associations

    Also, abandon NE in all LOB. NE should be no longer exist in the CDW input

    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
 */

package com.pccw.dango.shared.tool;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.wheat.shared.tool.MiniRtException;
import com.pccw.wheat.shared.tool.Tool;

public class Lobs implements Serializable
{
	private static final long serialVersionUID = -5165734329039059188L;

	/* Sign-up Tags */
	public static final SuTag      csumSuLobAry[]       = { 
		new SuTag(SubnRec.LOB_LTS,   Msgs.LOB_LTS,     Msgs.SN_DESN_4LTS,  Msgs.SN_BRIEF_4LTS),
		new SuTag(SubnRec.LOB_PCD,   Msgs.LOB_PCD,     Msgs.SN_DESN_4PCD,  Msgs.SN_BRIEF_4PCD),
		new SuTag(SubnRec.LOB_TV,    Msgs.LOB_TV,      Msgs.SN_DESN_4TV,   Msgs.SN_BRIEF_4TV),
		new SuTag(SubnRec.WLOB_CSL,  Msgs.XLOB_CSL,    Msgs.SN_DESN_4CSL,  Msgs.SN_BRIEF_4CSL),
		new SuTag(SubnRec.WLOB_X101, Msgs.XLOB_101,    Msgs.SN_DESN_4101,  Msgs.SN_BRIEF_4101)
	};
	/* add one more LOB_LTS, for LTS_ONE in the Registration PullDown list */
	public static final SuTag       commSuLobAry[]      = { 
		new SuTag(SubnRec.LOB_LTS,   Msgs.LOB_LTS_TEL, Msgs.SN_DESN_4LTS,  Msgs.SN_BRIEF_4LTS),
		new SuTag(SubnRec.LOB_LTS,   Msgs.LOB_LTS_ONE, Msgs.SN_DESN_4LTS,  Msgs.SN_BRIEF_4LTS),
		new SuTag(SubnRec.LOB_PCD,   Msgs.LOB_PCD,     Msgs.SN_DESN_4PCD,  Msgs.SN_BRIEF_4PCD),
		new SuTag(SubnRec.LOB_IMS,   Msgs.LOB_IMS,     Msgs.SN_DESN_4PCD,  Msgs.SN_BRIEF_4IMS),
		new SuTag(SubnRec.LOB_TV,    Msgs.LOB_TV,      Msgs.SN_DESN_4TV,   Msgs.SN_BRIEF_4TV)
	};


	/* Generic User Interface Tags */
	/* Re the LTS, two UiLob are introduced for Line Test display */
	public static final UiTag       csumUiLobAry[]      = {
		new UiTag(SubnRec.LOB_LTS,   null,                Msgs.LOB_LTS,       Msgs.IMG_LTS_W0060,     Msgs.SN_DESN_4LTS,    false),
		new UiTag(SubnRec.LOB_LTS,   SubnRec.TOS_LTS_TEL, Msgs.LOB_LTS,       Msgs.IMG_LTS,           Msgs.SN_DESN_4LTS,    false),
		new UiTag(SubnRec.LOB_LTS,   SubnRec.TOS_LTS_EYE, Msgs.LOB_LTS,       Msgs.IMG_LTS,           Msgs.SN_DESN_4LTS,    false),
		new UiTag(SubnRec.LOB_PCD,   null,                Msgs.LOB_PCD,       Msgs.IMG_PCD,           Msgs.SN_DESN_4PCD,    false),
		new UiTag(SubnRec.LOB_TV,    null,                Msgs.LOB_TV,        Msgs.IMG_TV,            Msgs.SN_DESN_4TV,     false),
		new UiTag(SubnRec.WLOB_CSL,  null,                Msgs.XLOB_CSL,      Msgs.IMG_CSL,           Msgs.SN_DESN_4CSL,    true),
		new UiTag(SubnRec.WLOB_X101, null,                Msgs.XLOB_101,      Msgs.IMG_101,           Msgs.SN_DESN_4101,    true)
	};

	public static final UiTag       commUiLobAry[]      = {
		new UiTag(SubnRec.LOB_LTS,   Msgs.LOB_LTS_ONE,  Msgs.IMG_LTS_ONE,   Msgs.SN_DESN_4LTS),
		new UiTag(SubnRec.LOB_PCD,   Msgs.LOB_PCD,      Msgs.IMG_PCD,       Msgs.SN_DESN_4PCD),
		new UiTag(SubnRec.LOB_IMS,   Msgs.LOB_IMS,      Msgs.IMG_IMS,       Msgs.SN_DESN_4IMS),
		new UiTag(SubnRec.LOB_TV,    Msgs.LOB_TV,       Msgs.IMG_TV,        Msgs.SN_DESN_4TV)
	};



	/* Contact Info Tags, due to Contact itself has other images, Damn! */
    public static final CtTag       csumCtLobAry[]      = {
    	new CtTag(SubnRec.LOB_LTS,   Msgs.LOB_LTS,       Msgs.IMG_LTS_W0060),
    	new CtTag(SubnRec.LOB_PCD,   Msgs.LOB_PCD,       Msgs.IMG_PCD_TV),
    	new CtTag(SubnRec.LOB_TV,    Msgs.LOB_TV,        Msgs.IMG_PCD_TV),
    	new CtTag(SubnRec.WLOB_CSL,  Msgs.XLOB_CSL,      Msgs.IMG_CSL_101),
    	new CtTag(SubnRec.WLOB_X101, Msgs.XLOB_101,      Msgs.IMG_CSL_101),
    };
    
    
    public static final CtTag       commCtLobAry[]      = {
    	new CtTag(SubnRec.LOB_LTS,   Msgs.LOB_LTS_ONE,  Msgs.IMG_LTS_ONE),
    	new CtTag(SubnRec.LOB_PCD,   Msgs.LOB_PCD,      Msgs.IMG_PCD),
    	new CtTag(SubnRec.LOB_IMS,   Msgs.LOB_IMS,      Msgs.IMG_IMS),
    	new CtTag(SubnRec.LOB_TV,    Msgs.LOB_TV,       Msgs.IMG_TV)
    };

    
    
	/* Generic U/I to Tags Mapping */
	public static final UiMap       uiMapAry[]         = {
		new UiMap(SubnRec.LOB_LTS,  SubnRec.LOB_LTS),
		new UiMap(SubnRec.LOB_PCD,  SubnRec.LOB_PCD),
		new UiMap(SubnRec.LOB_IMS,  SubnRec.LOB_IMS),
		new UiMap(SubnRec.LOB_TV,   SubnRec.LOB_TV),
		new UiMap(SubnRec.LOB_MOB,  SubnRec.WLOB_CSL,       true),
		new UiMap(SubnRec.LOB_O2F,  SubnRec.WLOB_CSL,       true),
		new UiMap(SubnRec.LOB_101,  SubnRec.WLOB_X101,      true),
		new UiMap(SubnRec.LOB_IOI,  SubnRec.WLOB_X101,      true)
	};



	public static void main(String rArg[])
	{
		System.out.println(getVer());
		return;
	}


	public static String getVer()
	{
		return ("$URL: $, $Rev: $");
	}


	public static SuTag[] getSuTagAry(String rPhylum)
	{
		if (rPhylum.equals(CustRec.PH_CSUM)) return (csumSuLobAry);
		if (rPhylum.equals(CustRec.PH_COMM)) return (commSuLobAry);

		throw new MiniRtException("Unexpected Phylum("+rPhylum+")!");
	}


	public static UiTag[] getUiTagAry(String rPhylum)
	{
		if (rPhylum.equals(CustRec.PH_CSUM)) return (csumUiLobAry);
		if (rPhylum.equals(CustRec.PH_COMM)) return (commUiLobAry);

		throw new MiniRtException("Unexpected Phylum("+rPhylum+")!");
	}    


    public static CtTag[] getCtTagAry(String rPhylum)
    {
        if (rPhylum.equals(CustRec.PH_CSUM)) return (csumCtLobAry);
        if (rPhylum.equals(CustRec.PH_COMM)) return (commCtLobAry);
        
        throw new MiniRtException("Unexpected Phylum("+rPhylum+")!");
    }


	public static SuTag getSuTag(String rPhylum, String rUiLob)
	{
		/*
            Get SuTag by Phylum and UiLob
		 */

		int                         rx, ri, rl;

		for (rx=0; rx<getSuTagAry(rPhylum).length; rx++) {
			if (rUiLob.equals(getSuTagAry(rPhylum)[rx].uiLob)) {
				return (getSuTagAry(rPhylum)[rx]);
			}
		}

		throw new MiniRtException("UiLob ("+rUiLob+") cannot be found!");
	}


	public static UiTag getUiTag(String rPhylum, String rUiLob)
	{
		return (getUiTag(rPhylum, rUiLob, null));
	}


	public static UiTag getUiTag(String rPhylum, String rUiLob, String rUiTos)
	{
		UiTag                       rResTag;

		/* Search with specific TOS */
		rResTag = getUiTagNoDefault(rPhylum, rUiLob, rUiTos);

		/* if not found, try without TOS, as default */
		if (rResTag == null) rResTag = getUiTagNoDefault(rPhylum, rUiLob, null);

		if (rResTag == null) {
			/* if still not found, raise Exception */
			throw new MiniRtException("UiLob/Tos ("+rUiLob+"/"+rUiTos+") cannot be found!");
		}

		return (rResTag);
	}


	public static UiTag getUiTagNoDefault(String rPhylum, String rUiLob, String rUiTos)
	{
		/*
            Get UiTag by Phylum, UiLob and UiTos
		 */

		int                         rx, ri, rl;
		UiTag                       rUiTag;

		for (rx=0; rx<getUiTagAry(rPhylum).length; rx++) {
			if (rUiLob.equals(getUiTagAry(rPhylum)[rx].uiLob)) {

				rUiTag  = getUiTagAry(rPhylum)[rx];
				if (Tool.isNil(rUiTos)) {    
					/* if no TOS specified, return the 1st hit UiTag */
					return (rUiTag);
				}
				else {
					/* if same as the one specified, return the UiTag */
					if (rUiTos.equals(rUiTag.uiTos)) {
						return (rUiTag);
					}
				}
			}
		}

		/* No such combination is found */
		return (null);
	}

	public static UiMap getUiMap(String rLob)
	{
		int                         rx, ri, rl;

		for (rx=0; rx<uiMapAry.length; rx++) {
			if (rLob.equals(uiMapAry[rx].lob)) {
				return (uiMapAry[rx]);
			}
		}

		throw new MiniRtException("Lob ("+rLob+") cannot be found!");
	}


	public static String[] getLobAryByUiLob(String rUiLob)
	{
		/*
            Return underlying LOBs of a UiLob;
		 */

		List<String>                rResLst;
		int                         rx, ri, rl;

		rResLst = new ArrayList<String>();
		for (rx=0; rx<uiMapAry.length; rx++) {
			if (uiMapAry[rx].uiLob.equals(rUiLob)) {
				rResLst.add(uiMapAry[rx].lob);
			}
		}

		return rResLst.toArray(new String[0]);
	}


	public static CtTag getCtTag(String rPhylum, String rUiLob)
	{
		/*
            Get SuTag by Phylum and UiLob
		 */

		int                         rx, ri, rl;

		for (rx=0; rx<getCtTagAry(rPhylum).length; rx++) {
			if (rUiLob.equals(getCtTagAry(rPhylum)[rx].uiLob)) {
				return (getCtTagAry(rPhylum)[rx]);
			}
		}

		throw new MiniRtException("UiLob ("+rUiLob+") cannot be found!");
	}


	public static class SuTag
	{
		/*
	        Data Structure for Self-Registration (Signup) LOB (for pickup the Service)
		 */

		public final String             uiLob;      /* LOB defined in SubnRec (Raw + Wild)      */
		public final String             tag;        /* Tag for Display on U/I                   */
		public final String             tag4Sn;     /* Tag for Service Number Description       */
		public final String             tag4bf;     /* Tag for Service Number Line Brief      */


		public SuTag(String rUiLob, String rTag, String rTag4Sn, String rTag4Bf)
		{
			uiLob   = rUiLob;
			tag     = rTag;
			tag4Sn  = rTag4Sn;
			tag4bf  = rTag4Bf;
		}
	}


	public static class UiTag
	{
		/*
        Data Structure for U/I LOB (for main.jsp)
		 */

		public final String             uiLob;      /* LOB defined in SubnRec (Raw + Wild)      */
		public final String             uiTos;      /* TOS defined in SubnRec (Optional)        */
		public final String             tag;        /* Tag for Display on U/I                   */
		public final String             imgTag;     /* Tag for Image display on U/I             */
		public final String             tag4Sn;     /* Tag for Service Number Description       */
		public final boolean            isMob;      /* True if it is a Wireless                 */


		public UiTag(String rUiLob, String rUiTos, String rTag, String rImgTag, String rTag4Sn, boolean rIsMob)
		{
			uiLob   = rUiLob;
			uiTos   = rUiTos;
			tag     = rTag;
			imgTag  = rImgTag;
			tag4Sn  = rTag4Sn;
			isMob   = rIsMob;
		}


		public UiTag(String rUiLob, String rTag, String rImgTag, String rTag4Sn)
		{
			/* Constructor for Non-Mobile */
			this(rUiLob, null, rTag, rImgTag, rTag4Sn, false);
		}
	}


	public static class UiMap
	{
		/*
        Data Structure for U/I LOB Mapping (for Processing)
		 */


		public final String             lob;        /* LOB in SubnRec (Raw Only)                */
		public final String             uiLob;      /* LOB defined in SubnRec (Raw + Wild)      */
		public final boolean            isMob;      /* True if it is a Wireless                 */    


		public UiMap(String rLob, String rUiLob, boolean rIsMob)
		{
			lob     = rLob;
			uiLob   = rUiLob;
			isMob   = rIsMob;
		}


		public UiMap(String rUiLob, String rTag)
		{
			/* Constructor for Non-Mobile */
			this(rUiLob, rTag, false);
		}
	}

	public static class CtTag
	{
		/*
        Data Structure for Contact Info
		 */

		public final String             uiLob;      /* LOB defined in SubnRec (Raw + Wild)      */
		public final String             tag;        /* Tag for Display on U/I                   */
		public final String             imgTag;     /* Tag for Image display on U/I             */


		public CtTag(String rUiLob, String rTag, String rImgTag)
		{
			uiLob   = rUiLob;
			tag     = rTag;
			imgTag  = rImgTag;
		}
	}


}    
