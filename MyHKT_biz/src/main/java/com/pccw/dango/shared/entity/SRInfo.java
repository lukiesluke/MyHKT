/*
    Service Request Information

    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class SRInfo implements Serializable
{
    private static final long serialVersionUID = -5364037711398478747L;
    
    private String                  custAdr;            /* Customer Address                              */
    private String                  flatCd;             /* Flat Code                                     */
    private String                  floorCd;            /* Floor Code                                    */
    private String                  srvBndy;            /* Service Boundry                               */
    private String                  ctNum;              /* Contact Number                                */
    private String                  ctNm;               /* Contact Name                                  */
    private String                  ponLine;            /* PON Line                                      */
    private String                  ponCvg;             /* PON Coverage                                  */
    private String                  mdmRst;             /* Modem Reset                                   */
    private String                  bsn;                /* BSN                                           */
    private String                  fsa;                /* FSA                                           */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public SRInfo()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearCustAdr();
        clearFlatCd();
        clearFloorCd();
        clearSrvBndy();
        clearCtNum();
        clearCtNm();
        clearPonLine();
        clearPonCvg();
        clearMdmRst();
        clearBsn();
        clearFsa();
    }


    public SRInfo copyFrom(SRInfo rSrc)
    {
        setCustAdr(rSrc.getCustAdr());
        setFlatCd(rSrc.getFlatCd());
        setFloorCd(rSrc.getFloorCd());
        setSrvBndy(rSrc.getSrvBndy());
        setCtNum(rSrc.getCtNum());
        setCtNm(rSrc.getCtNm());
        setPonLine(rSrc.getPonLine());
        setPonCvg(rSrc.getPonCvg());
        setMdmRst(rSrc.getMdmRst());
        setBsn(rSrc.getBsn());
        setFsa(rSrc.getFsa());

        return (this);
    }


    public SRInfo copyTo(SRInfo rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SRInfo copyMe()
    {
        SRInfo                      rDes;

        rDes = new SRInfo();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearCustAdr()
    {
        custAdr = "";
    }


    public void setCustAdr(String rArg)
    {
        custAdr = rArg;
    }


    public String getCustAdr()
    {
        return (custAdr);
    }


    public void clearFlatCd()
    {
        flatCd = "";
    }


    public void setFlatCd(String rArg)
    {
        flatCd = rArg;
    }


    public String getFlatCd()
    {
        return (flatCd);
    }


    public void clearFloorCd()
    {
        floorCd = "";
    }


    public void setFloorCd(String rArg)
    {
        floorCd = rArg;
    }


    public String getFloorCd()
    {
        return (floorCd);
    }


    public void clearSrvBndy()
    {
        srvBndy = "";
    }


    public void setSrvBndy(String rArg)
    {
        srvBndy = rArg;
    }


    public String getSrvBndy()
    {
        return (srvBndy);
    }


    public void clearCtNum()
    {
        ctNum = "";
    }


    public void setCtNum(String rArg)
    {
        ctNum = rArg;
    }


    public String getCtNum()
    {
        return (ctNum);
    }


    public void clearCtNm()
    {
        ctNm = "";
    }


    public void setCtNm(String rArg)
    {
        ctNm = rArg;
    }


    public String getCtNm()
    {
        return (ctNm);
    }


    public void clearPonLine()
    {
        ponLine = "";
    }


    public void setPonLine(String rArg)
    {
        ponLine = rArg;
    }


    public String getPonLine()
    {
        return (ponLine);
    }


    public void clearPonCvg()
    {
        ponCvg = "";
    }


    public void setPonCvg(String rArg)
    {
        ponCvg = rArg;
    }


    public String getPonCvg()
    {
        return (ponCvg);
    }


    public void clearMdmRst()
    {
        mdmRst = "";
    }


    public void setMdmRst(String rArg)
    {
        mdmRst = rArg;
    }


    public String getMdmRst()
    {
        return (mdmRst);
    }


    public void clearBsn()
    {
        bsn = "";
    }


    public void setBsn(String rArg)
    {
        bsn = rArg;
    }


    public String getBsn()
    {
        return (bsn);
    }


    public void clearFsa()
    {
        fsa = "";
    }


    public void setFsa(String rArg)
    {
        fsa = rArg;
    }


    public String getFsa()
    {
        return (fsa);
    }
}
