/*
    Class for General Appointment
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;
import java.util.ArrayList;


public class GnrlAppt implements Serializable
{
    private static final long serialVersionUID = 7819153467008655902L;
    
    private String                  ocId;
    private String                  iAdr;
    private String                  srvBndy;
    private String[]                drgOrdNumAry;
    private String                  apptStDT;
    private String                  apptEnDT;
    private boolean                 showDtls;
    private String[]                srvAry;
    
    public static String            SRV_TEL      = "TEL";
    public static String            SRV_EYE      = "EYE";
    public static String            SRV_IMS      = "IMS";
    public static String            SRV_TV       = "TV";
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public GnrlAppt()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        clearOcId();
        clearIAdr();
        clearSrvBndy();
        clearDrgOrdNumAry();
        clearApptStDT();
        clearApptEnDT();
        clearShowDtls();
        clearSrvAry();
    }
    
    
    public GnrlAppt copyFrom(GnrlAppt rSrc)
    {
        setOcId(rSrc.getOcId());
        setIAdr(rSrc.getIAdr());
        setSrvBndy(rSrc.getSrvBndy());
        setDrgOrdNumAry(rSrc.getDrgOrdNumAry());
        setApptStDT(rSrc.getApptStDT());
        setApptEnDT(rSrc.getApptEnDT());
        setShowDtls(rSrc.getShowDtls());
        setSrvAry(rSrc.getSrvAry());
        
        return (this);
    }
    
    
    public GnrlAppt copyTo(GnrlAppt rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public GnrlAppt copyMe()
    {
        GnrlAppt             rDes;
        
        rDes = new GnrlAppt();
        rDes.copyFrom(this);
        
        return (rDes);
    }

    
    public void clearOcId()
    {
        setOcId("");
    }
    
    
    public void setOcId(String rArg)
    {
        ocId = rArg;
    }
    
    
    public String getOcId()
    {
        return (ocId);
    }
    
    
    public void clearIAdr()
    {
        setIAdr("");
    }
    
    
    public void setIAdr(String rArg)
    {
        iAdr = rArg;
    }
    
    
    public String getIAdr()
    {
        return (iAdr);
    }

    
    public void clearSrvBndy()
    {
        setSrvBndy("");
    }
    
    
    public void setSrvBndy(String rArg)
    {
        srvBndy = rArg;
    }
    
    
    public String getSrvBndy()
    {
        return (srvBndy);
    }

    
    public void clearApptStDT()
    {
        setApptStDT("");
    }
    
    
    public void setApptStDT(String rArg)
    {
        apptStDT = rArg;
    }
    
    
    public String getApptStDT()
    {
        return (apptStDT);
    }

    
    public void clearApptEnDT()
    {
        setApptEnDT("");
    }
    
    
    public void setApptEnDT(String rArg)
    {
        apptEnDT = rArg;
    }
    
    
    public String getApptEnDT()
    {
        return (apptEnDT);
    }
    
    
    public void clearDrgOrdNumAry()
    {
        setDrgOrdNumAry(new String[0]);
    }
    
    
    public void setDrgOrdNumAry(String[] rArg)
    {
        drgOrdNumAry = rArg;
    }
    

    public void setDrgOrdNumAry(ArrayList<String> rArg)
    {
        if (rArg.size() > 0) {
            drgOrdNumAry = rArg.toArray(new String[rArg.size()]);
        }
    }
    
    
    public String[] getDrgOrdNumAry()
    {
        return (drgOrdNumAry);
    }
    
    
    public void clearShowDtls()
    {
        setShowDtls(false);
    }
    
    
    public void setShowDtls(boolean rArg)
    {
        showDtls = rArg;
    }
    
    
    public boolean getShowDtls()
    {
        return (showDtls);
    }
    
    
    public void clearSrvAry()
    {
        setSrvAry(new String[0]);
    }
    
    
    public void setSrvAry(String[] rArg)
    {
        srvAry = rArg;
    }
    
    
    public void setSrvAry(ArrayList<String> rArg)
    {
        if (rArg.size() > 0) {
            srvAry = rArg.toArray(new String[rArg.size()]);
        }
    }

    
    public String[] getSrvAry()
    {
        return (srvAry);
    }
}
