/*
    Crate for Appointment
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.ApptInfo;
import com.pccw.dango.shared.entity.BomAppt;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;

public class ApptCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -6190729566714400235L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private CustRec                 iCustRec;           /* Customer Information                          */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    private SubnRec                 iSubnRecAry[];      /* Array of Subscription Record                  */
    private SrvReq                  iSrvReq;            /* Service Request                               */
    private String                  iEnqSRTy;           /* Enquiry SR Type                               */
    private ApptInfo                iSRApptInfo;        /* SR Appointment Info                           */  
    
    private GnrlAppt[]              oGnrlApptAry;       /* Array of General Appointment                  */
    private BomAppt[]               oBomApptAry;       /* Array of General Appointment                  */
    private SrvReq[]                oSrvReqAry;         /* Array of Service Request                      */
    private ApptInfo                oSRApptInfo;        /* SR Appointment Info                           */
    private SrvReq                  oPendSrvReq;        /* Pending SR                                    */
    
    public static String            TTL_MR      = "Mr.";
    public static String            TTL_MS      = "Ms.";
    public static String            TTL_MRS     = "Mrs.";
    public static String            TTL_MISS    = "Miss";
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public ApptCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearICustRec();
        clearISubnRec();
        clearISubnRecAry();
        clearISrvReq();
        clearIEnqSRTy();
        clearISRApptInfo();
        
        clearOGnrlApptAry();
        clearOBomApptAry();
        clearOSrvReqAry();
        clearOSRApptInfo();
        clearOPendSrvReq();
    }


    public ApptCra copyFrom(ApptCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setICustRec(rSrc.getICustRec());
        setISubnRec(rSrc.getISubnRec());
        setISubnRecAry(rSrc.getISubnRecAry());
        setISrvReq(rSrc.getISrvReq());
        setIEnqSRTy(rSrc.getIEnqSRTy());
        setISRApptInfo(rSrc.getISRApptInfo());
                
        setOGnrlApptAry(rSrc.getOGnrlApptAry());
        setOBomApptAry(rSrc.getOBomApptAry());
        setOSrvReqAry(rSrc.getOSrvReqAry());
        setOSRApptInfo(rSrc.getOSRApptInfo());
        setOPendSrvReq(rSrc.getOPendSrvReq());
        
        return (this);
    }


    public ApptCra copyTo(ApptCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public ApptCra copyMe()
    {
        ApptCra                     rDes;

        rDes = new ApptCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearICustRec()
    {
        iCustRec = new CustRec();
    }


    public void setICustRec(CustRec rArg)
    {
        iCustRec = rArg;
    }


    public CustRec getICustRec()
    {
        return (iCustRec);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }
    
    
    public void clearISubnRecAry()
    {
        iSubnRecAry = new SubnRec[0];
    }


    public void setISubnRecAry(SubnRec[] rArg)
    {
        iSubnRecAry = rArg;
    }


    public SubnRec[] getISubnRecAry()
    {
        return (iSubnRecAry);
    }

    
    public void clearOGnrlApptAry()
    {
        oGnrlApptAry = new GnrlAppt[0];
    }


    public void setOGnrlApptAry(GnrlAppt[] rArg)
    {
        oGnrlApptAry = rArg;
    }


    public GnrlAppt[] getOGnrlApptAry()
    {
        return (oGnrlApptAry);
    }

    
    public void clearOBomApptAry()
    {
        oBomApptAry = new BomAppt[0];
    }


    public void setOBomApptAry(BomAppt[] rArg)
    {
        oBomApptAry = rArg;
    }


    public BomAppt[] getOBomApptAry()
    {
        return (oBomApptAry);
    }


    public void clearOSrvReqAry()
    {
        oSrvReqAry = new SrvReq[0];
    }


    public void clearISrvReq()
    {
        iSrvReq = new SrvReq();
    }


    public void setISrvReq(SrvReq rArg)
    {
        iSrvReq = rArg;
    }


    public SrvReq getISrvReq()
    {
        return (iSrvReq);
    }

    
    public void clearIEnqSRTy()
    {
        iEnqSRTy = "";
    }


    public void setIEnqSRTy(String rArg)
    {
        iEnqSRTy = rArg;
    }


    public String getIEnqSRTy()
    {
        return (iEnqSRTy);
    }
    
    
    public void clearISRApptInfo()
    {
        iSRApptInfo = new ApptInfo();
    }


    public void setISRApptInfo(ApptInfo rArg)
    {
        iSRApptInfo = rArg;
    }


    public ApptInfo getISRApptInfo()
    {
        return (iSRApptInfo);
    }

    
    
    public void setOSrvReqAry(SrvReq[] rArg)
    {
        oSrvReqAry = rArg;
    }


    public SrvReq[] getOSrvReqAry()
    {
        return (oSrvReqAry);
    }


    public void clearOSRApptInfo()
    {
        oSRApptInfo = new ApptInfo();
    }


    public void setOSRApptInfo(ApptInfo rArg)
    {
        oSRApptInfo = rArg;
    }


    public ApptInfo getOSRApptInfo()
    {
        return (oSRApptInfo);
    }

    
    public void clearOPendSrvReq()
    {
        oPendSrvReq = new SrvReq();
    }


    public void setOPendSrvReq(SrvReq rArg)
    {
        oPendSrvReq = rArg;
    }


    public SrvReq getOPendSrvReq()
    {
        return (oPendSrvReq);
    }
}
