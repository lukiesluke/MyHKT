/*
    Crate for CARE registration
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.CareRec;
import com.pccw.dango.shared.entity.QualSvee;

public class CareCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 4747435517758709387L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private String                  iCareTok;           /* Care Token (registration mode)                */
    private int                     iMode;              /* CARE registration mode                        */
    private CareRec                 iCareRec;           /* CARE registration record                      */
    private boolean                 iChgCare;           /* Change CARE?                                  */
    private boolean                 iChgBipt;           /* Change BillProtector?                         */
    
    private CareRec                 oCareRec;           /* CARE registration record                      */
    private QualSvee                oQualSvee;          /* Qualified Servee                              */

    
    public static final int         MODE_REG    = 1;    /* Thru Registration                             */
    public static final int         MODE_LGI    = 2;    /* Thru Login                                    */
    public static final int         MODE_MNU    = 3;    /* Thru NavBar                                   */
    public static final int         MODE_ADW    = 4;    /* Thru AdminWS                                  */
    public static final int         MODE_IG     = 5;    /* Thru IGuard (Vendor)                          */
    public static final int         MODE_API    = 6;    /* Thru ARQ API                                  */

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public CareCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearICareTok();
        clearIMode();
        clearICareRec();
        clearIChgCare();
        clearIChgBipt();
        clearOCareRec();
        clearOQualSvee();
    }


    public CareCra copyFrom(CareCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setICareTok(rSrc.getICareTok());
        setIMode(rSrc.getIMode());
        setICareRec(rSrc.getICareRec());       
        setIChgCare(rSrc.isIChgCare());
        setIChgBipt(rSrc.isIChgBipt());
        setOCareRec(rSrc.getOCareRec());
        setOQualSvee(rSrc.getOQualSvee());

        return (this);
    }


    public CareCra copyTo(CareCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public CareCra copyMe()
    {
        CareCra                     rDes;

        rDes = new CareCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }

    
    public void clearICareTok()
    {
        iCareTok = "";
    }


    public void setICareTok(String rArg)
    {
        iCareTok = rArg;
    }


    public String getICareTok()
    {
        return (iCareTok);
    }


    public void clearIMode()
    {
        iMode = -1;
    }


    public void setIMode(int rArg)
    {
        iMode = rArg;
    }


    public int getIMode()
    {
        return (iMode);
    }


    public void clearICareRec()
    {
        iCareRec = new CareRec();
    }


    public void setICareRec(CareRec rArg)
    {
        iCareRec = rArg;
    }


    public CareRec getICareRec()
    {
        return (iCareRec);
    }


    public void clearIChgCare()
    {
        iChgCare = false;
    }


    public void setIChgCare(boolean rArg)
    {
        iChgCare = rArg;
    }


    public boolean isIChgCare()
    {
        return (iChgCare);
    }


    public void clearIChgBipt()
    {
        iChgBipt = false;
    }


    public void setIChgBipt(boolean rArg)
    {
        iChgBipt = rArg;
    }


    public boolean isIChgBipt()
    {
        return (iChgBipt);
    }


    public void clearOCareRec()
    {
        oCareRec = new CareRec();
    }


    public void setOCareRec(CareRec rArg)
    {
        oCareRec = rArg;
    }


    public CareRec getOCareRec()
    {
        return (oCareRec);
    }


    public void clearOQualSvee()
    {
        oQualSvee = new QualSvee();
    }


    public void setOQualSvee(QualSvee rArg)
    {
        oQualSvee = rArg;
    }


    public QualSvee getOQualSvee()
    {
        return (oQualSvee);
    }
}
