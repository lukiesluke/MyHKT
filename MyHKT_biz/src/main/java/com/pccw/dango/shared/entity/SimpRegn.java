/*
    Class for Simplified Registration

    Supplementary information for other Registration thru API.
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.MiniRtException;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;


public class SimpRegn implements Serializable 
{
    private static final long serialVersionUID = 1544611615458193937L;
    
    private int                     rmode;
    private String                  salesChnl;
    private String                  teamCode;
    private String                  staffId;
    private String                  keyTy;
    private String                  keyVal;

    /* Registration Mode */
    public static final int         RM_NORMAL       = 0;  /* Normal Registration                 */
    public static final int         RM_PRE_IDENT    = 3;  /* Regn by Pre-Ident (by Branded Site) */ 
    public static final int         RM_PCD_PREG     = 4;  /* PCD PreRegistration                 */
    public static final int         RM_BULK         = 8;  /* Bulk Creation                       */
    public static final int         RM_TV_PREG      = 9;  /* TV PreRegistration                  */
    public static final int         RM_CLUB_REG     = 10; /* Club Registration                   */
    public static final int         RM_EMBO_T       = 12; /* SBOS w/ TNC Spelled Out             */
    public static final int         RM_LARVA_T      = 13; /* AdminWS w/ TNC Spelled Out          */
    public static final int         RM_EMBOPRO_T    = 14; /* SB/BOM w/ TNC Spelled Out           */
    public static final int         RM_CLBL_REG_T   = 15; /* CLUB by Staff w/ TNC Spelled Out    */

    /* Eclosion Mode, EC_* shall not be used in the backend */
    public static final int         EC_LARVA        = 106;
    public static final int         EC_EMBOPRO      = 107;
    public static final int         EC_BULK         = 108;
    public static final int         EC_TV_PREG      = 109;
    public static final int         EC_CLUB_REG     = 110;
    public static final int         EC_CLBL_REG     = 111;
    public static final int         EC_LARVA_T      = 113;
    public static final int         EC_EMBOPRO_T    = 114;
    public static final int         EC_CLBL_REG_T   = 115;
    public static final int         EC_CLUB_ACTV    = 116;
    
    public static final String      KT_AIK          = "aik"; /* Account Identification Key  */

    public static final int         L_KEYVAL        = 256;
    
    public static final int         L_SALESCHNL     = SveeRec.L_SALES_CHNL;
    public static final int         L_TEAMCODE      = SveeRec.L_TEAM_CODE;
    public static final int         L_STAFFID       = SveeRec.L_STAFF_ID;

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public SimpRegn()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        clearRmode();
        clearKeyTy();
        clearKeyVal();
        clearSalesChnl();
        clearTeamCode();
        clearStaffId();
    }
    

    public SimpRegn copyFrom(SimpRegn rSrc)
    {
        setRmode(rSrc.getRmode());
        setKeyTy(rSrc.getKeyTy());
        setKeyVal(rSrc.getKeyVal());
        setSalesChnl(rSrc.getSalesChnl());
        setTeamCode(rSrc.getTeamCode());
        setStaffId(rSrc.getStaffId());
        
        return (this);
    }
    
    
    public SimpRegn copyTo(SimpRegn rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SimpRegn copyMe()
    {
        SimpRegn                      rDes;
        
        rDes = new SimpRegn();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearRmode()
    {
        setRmode(0);
    }


    public void setRmode(int rRmode) 
    {
        rmode = rRmode;
    }
    
    
    public int getRmode() 
    {
        return rmode;
    }


    public void clearKeyTy()
    {
        setKeyTy("");
    }


    public void setKeyTy(String rKeyTy) 
    {
        keyTy = rKeyTy;
    }
    
    
    public String getKeyTy() 
    {
        return keyTy;
    }


    public void clearKeyVal()
    {
        setKeyVal("");
    }


    public void setKeyVal(String rKeyVal) 
    {
        keyVal = rKeyVal;
    }
    
    
    public String getKeyVal() 
    {
        return keyVal;
    }


    public void clearSalesChnl()
    {
        setSalesChnl("");
    }


    public void setSalesChnl(String rSalesChnl) 
    {
    	salesChnl = rSalesChnl.trim();
    }
    
    
    public String getSalesChnl() 
    {
        return salesChnl;
    }

    
    public void clearTeamCode()
    {
        setTeamCode("");
    }


    public void setTeamCode(String rTeamCode) 
    {
    	teamCode = rTeamCode.trim();
    }
    
    
    public String getTeamCode() 
    {
        return teamCode;
    }

    
    public void clearStaffId()
    {
        setStaffId("");
    }


    public void setStaffId(String rStaffId) 
    {
    	staffId = rStaffId.trim();
    }
    
    
    public String getStaffId() 
    {
        return staffId;
    }
    
    
    public void deriveEcMode4Fill(String rState, boolean rAcptTnc)
    {
        int                         rRmode;
        
        /*
            Base on the rState to derive the Rmode (for Eclosion Only)
            The rAcptTnc is for 'assertion' only.
        */
        
        rRmode = -1;
        
        if (rAcptTnc) {
           /* State should be accepted TNC during Registration */

           if (rState.equals(SveeRec.SE_CLBL_REG_T)) {
               rRmode = EC_CLBL_REG_T;
           }
           else if (rState.equals(SveeRec.SE_INITPRO_T)) {
               rRmode = EC_EMBOPRO_T;
           }
           else if (rState.equals(SveeRec.SE_LARVA_T)) {
               rRmode = EC_LARVA_T;
           }
           else if (rState.equals(SveeRec.SE_CLUB_REG)) {
               rRmode = EC_CLUB_REG;
           }
           else if (rState.equals(SveeRec.SE_CLBL_REG_T)) {
               rRmode = EC_CLBL_REG_T;
           }
           else if (rState.equals(SveeRec.SE_CLUB_ACTV)) {
               rRmode = EC_CLUB_ACTV;
           }
        }
        else {
            /* State not yet accepted TNC during Registration */
            
            if (rState.equals(SveeRec.SE_BULK)) {
                rRmode = EC_BULK;
            }
            else if (rState.equals(SveeRec.SE_INITPRO)) {
                rRmode = EC_EMBOPRO;
            }
            else if (rState.equals(SveeRec.SE_LARVA)) {
                rRmode = EC_LARVA;
            }
            else if (rState.equals(SveeRec.SE_CLBL_REG)) {
                rRmode = EC_CLBL_REG;
            }
        }
        
        if (rRmode < 0) throw new MiniRtException("Unexpected STATE!");
        setRmode(rRmode);
    }
    
    
    public Reply verifyBaseInput()
    {
        Reply                       rRR;
        
        rRR = verifyRmode();
        if (!rRR.isSucc()) return (rRR);
        
        if (getRmode() == RM_PRE_IDENT) {
            rRR = verifyPreIdent();
        }
        else if (getRmode() == RM_BULK) { 
            rRR = verifyBulk();
        }
        
        return (rRR);
    }

    
    public Reply verifyRmode()
    {
        if (getRmode() != RM_NORMAL    &&
            getRmode() != RM_PRE_IDENT &&
            getRmode() != RM_PCD_PREG  &&
            getRmode() != RM_BULK      &&
            getRmode() != RM_TV_PREG   &&
            getRmode() != RM_EMBO_T    &&
            getRmode() != RM_LARVA_T   &&
            getRmode() != RM_EMBOPRO_T &&
            getRmode() != RM_CLUB_REG  &&
            getRmode() != RM_CLBL_REG_T) {
            return (new Reply(RC.SFRG_IVRMODE));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPreIdent()
    {
        Reply                       rRC;
        
        rRC = verifyKeyTy();
        if (rRC.isSucc()) rRC = verifyKeyVal();
        
        return (rRC);
    }
    
    
    public Reply verifyLarva()
    {
        Reply                       rRC;
        
        rRC = verifySalesChnl();
        if (rRC.isSucc()) rRC = verifyTeamCode();
        if (rRC.isSucc()) rRC = verifyStaffId();
        
        return (rRC);
    }
    
    
    public Reply verifyEmboPro()
    {
        Reply                       rRC;
        
        rRC = verifySalesChnl();
        if (rRC.isSucc()) rRC = verifyTeamCode();
        if (rRC.isSucc()) rRC = verifyStaffId();
        
        return (rRC);
    }
    
    
    public Reply verifyBulk()
    {
        Reply                       rRC;
        
        rRC = verifySalesChnl(true);
        if (rRC.isSucc()) rRC = verifyTeamCode(true);
        if (rRC.isSucc()) rRC = verifyStaffId(true);
        
        return (rRC);
    }
    
    
    public Reply verifyKeyTy()
    {
        if (getRmode() == RM_PRE_IDENT) {
            if (getKeyTy().equals(KT_AIK)) {
                return (Reply.getSucc());
            }
        }
        
        return (new Reply(RC.SFRG_IVKEYTY));
    }
    
    
    public Reply verifyKeyVal()
    {
        if (getKeyVal().length() == 0 || getKeyVal().length() > L_KEYVAL) {
            return (new Reply(RC.SFRG_ILKEYVAL));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifySalesChnl(boolean rAllowEmpty)
    {
        Reply                       rRC;

        if (!Tool.isASC(getSalesChnl())) {
            return (new Reply(RC.SFRG_ILSALESCHNL));
        }
        
        if (getSalesChnl().length() == 0 && !rAllowEmpty) {
            return (new Reply(RC.SFRG_ILSALESCHNL));
        }
        
        if (getSalesChnl().length() > L_SALESCHNL) {
            return (new Reply(RC.SFRG_ILSALESCHNL));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifySalesChnl()
    {
        return (verifySalesChnl(false));
    }
    
    
    public Reply verifyTeamCode(boolean rAllowEmpty)
    {
        Reply                       rRC;

        if (!Tool.isASC(getTeamCode())) {
            return (new Reply(RC.SFRG_ILTEAMCODE));
        }
            
        if (getTeamCode().length() == 0 && !rAllowEmpty) {
            return (new Reply(RC.SFRG_ILTEAMCODE));
        }
                
        if (getTeamCode().length() > L_TEAMCODE) {
            return (new Reply(RC.SFRG_ILTEAMCODE));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyTeamCode()
    {
        return (verifyTeamCode(false));
    }
    
    
    public Reply verifyStaffId(boolean rAllowEmpty)
    {
        Reply                       rRC;

        if (!Tool.isASC(getStaffId())) {
            return (new Reply(RC.SFRG_ILSTAFFID));
        }
        
        if (getStaffId().length() == 0 && !rAllowEmpty) {
            return (new Reply(RC.SFRG_ILSTAFFID));
        }
        
        if (getStaffId().length() > L_STAFFID) {
            return (new Reply(RC.SFRG_ILSTAFFID));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyStaffId()
    {
        return (verifyStaffId(false));
    }
}
