/*
    Class for CARE Record to be used in GWT
    (At the moment of Sep 2016, only WAGASHI will use it)
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.wheat.shared.tool.Alma;



public class ShwdRec implements Serializable
{
    private static final long serialVersionUID = -6746005359523316440L;
    
    public String                   tag;                /* Tag Name                                      */
    public String                   eff_ts;             /* Effective Begin Timestamp                     */
    public String                   end_ts;             /* End of Effective Timestamp                    */
    public String                   desn;               /* Description                                   */
    public String                   status;             /* A/I = Active/Inactive                         */
    public String                   lastupd_ts;         /* Last Update TS                                */
    public String                   lastupd_psn;        /* Last Update Person                            */
    public int                      rev;                /* Record Revision                               */

    public static final int         L_TAG         = 32;
    public static final int         L_EFF_TS      = 14;
    public static final int         L_END_TS      = 14;
    public static final int         L_DESN        = 256;
    public static final int         L_STATUS      = 1;
    public static final int         L_LASTUPD_TS  = 14;
    public static final int         L_LASTUPD_PSN = 40;

    public static final String      NIL_DATE = "20000101000000";
    

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public ShwdRec()
    {
        initAndClear();
    }


    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        tag             = "";
        eff_ts          = "";
        end_ts          = "";
        desn            = "";
        status          = "";
        lastupd_ts      = "";
        lastupd_psn     = "";
        rev             = 0;                
    }


    public ShwdRec copyFrom(ShwdRec rSrc)
    {
        tag                         = rSrc.tag;
        eff_ts                      = rSrc.eff_ts;
        end_ts                      = rSrc.end_ts;
        desn                        = rSrc.desn;
        status                      = rSrc.status;
        lastupd_ts                  = rSrc.lastupd_ts;
        lastupd_psn                 = rSrc.lastupd_psn;
        rev                         = rSrc.rev;

        return (this);
    }


    public ShwdRec copyTo(ShwdRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public ShwdRec copyMe()
    {
        ShwdRec                     rDes;

        rDes = new ShwdRec();
        rDes.copyFrom(this);
        return (rDes);
    }

    
    public boolean verifyEffTs()
    {
        return (Alma.isValid(eff_ts));
    }
    
    
    public boolean verifyEndTs()
    {
        return (Alma.isValid(end_ts));
    }
    
    
    public boolean verifyPeriod()
    {
        if (verifyEffTs()) {
            if (verifyEndTs()) {
                return end_ts.compareTo(eff_ts) >= 0;
            }
        }
        
        return (false);
    }
}    
