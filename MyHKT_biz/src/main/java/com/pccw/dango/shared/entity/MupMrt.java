package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class MupMrt  implements Serializable
{
    private static final long serialVersionUID = 188246044390166908L;
    
    private String					dn;
	private String                  master;
	
	
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v7.0/src/com/pccw/dango/shared/entity/MobUsage.java $, $Rev: 844 $");
    }

    
	public MupMrt()
	{
		init();
	}

	
    final void initAndClear()
    {
        init();
        clear();
    }

    
	public void init() 
	{
	}

	
    public void clear()
    {
        clearDn();
        clearMaster();
    }
    
    
    public MupMrt copyFrom(MupMrt rSrc)
    {
        setDn(rSrc.getDn());
        setMaster(rSrc.getMaster());

        return (this);
    }


    public MupMrt copyTo(MupMrt rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public MupMrt copyMe()
    {
        MupMrt                      rDes;

        rDes = new MupMrt();
        rDes.copyFrom(this);
        return (rDes);
    }

	
    public void clearDn()
    {
        setDn("");
    }
    
    
    public void setDn(String rArg)
    {
    	dn = rArg;
    }
    
    
    public String getDn()
    {
        return dn;
    }
    
    
    public void clearMaster()
    {
        setMaster("");
    }
    
    
    public void setMaster(String rArg)
    {
        master = rArg;
    }
    
    
    public String getMaster()
    {
        return (master);
    }
}
