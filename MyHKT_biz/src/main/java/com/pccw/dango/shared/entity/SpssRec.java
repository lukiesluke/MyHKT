/*
    SmartPhone Session Record
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Const;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;

public class SpssRec implements Serializable
{
    private static final long serialVersionUID = 1929537799618613588L;
    
    public int                      rid;                /* Record Id                                     */
    public int                      dervRid;            /* Derived Record Id                             */
    public int                      sveeRid;            /* SVEE RID                                      */
    public String                   tcId;               /* Tomcat IP/Id                                  */
    public String                   tcSess;             /* Tomcat Session Id                             */
    public String                   devId;              /* Device Id                                     */
    public String                   devTy;              /* Device Type: A=Android/I=iPhone               */
    public String                   gni;                /* General Notification Indicator                */
    public String                   bni;                /* Bill Notification Indicator                   */
    public String                   lang;               /* Language                                      */
    public String                   lastupdTs;          /* Last Update TS                                */
    public String                   lastupdPsn;         /* Last Update Person                            */
    public int                      rev;                /* Record Revision                               */

    public static final int         L_TC_ID       = 20;
    public static final int         L_TC_SESS     = 64;
    public static final int         L_DEV_ID      = 512;
    public static final int         L_DEV_TY      = 1;
    public static final int         L_GNI         = 1;
    public static final int         L_BNI         = 1;
    public static final int         L_LANG        = 2;
    public static final int         L_LASTUPD_TS  = 14;
    public static final int         L_LASTUPD_PSN = 40;

    public static final String      TY_ANDROID     = "A";
    public static final String      TY_IOS         = "I";

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public SpssRec()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        rid             = 0;
        dervRid         = 0;
        sveeRid         = 0;
        tcId            = "";
        tcSess          = "";
        devId           = "";
        devTy           = "";
        gni             = "";
        bni             = "";
        lang            = "";
        lastupdTs       = "";
        lastupdPsn      = "";
        rev             = 0;
    }


    public SpssRec copyFrom(SpssRec rSrc)
    {
        rid             = rSrc.rid;
        dervRid         = rSrc.dervRid;
        sveeRid         = rSrc.sveeRid;
        tcId            = rSrc.tcId;
        tcSess          = rSrc.tcSess;
        devId           = rSrc.devId;
        devTy           = rSrc.devTy;
        gni             = rSrc.gni;
        bni             = rSrc.bni;
        lang            = rSrc.lang;
        lastupdTs       = rSrc.lastupdTs;
        lastupdPsn      = rSrc.lastupdPsn;
        rev             = rSrc.rev;
        
        return (this);
    }


    public SpssRec copyTo(SpssRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SpssRec copyMe()
    {
        SpssRec                     rDes;

        rDes = new SpssRec();
        rDes.copyFrom(this);
        return (rDes);
    }

    
    public Reply verifyDevId()
    {
        if (Tool.isNil(devId.trim()) || (devId.length() > L_DEV_ID) ) {
            return (new Reply(RC.SPSS_IV_DID));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyDevTy()
    {
        if (Tool.isNil(devTy.trim()) || (devTy.length() > L_DEV_TY) ) {
            return (new Reply(RC.SPSS_IV_DTY));
        }
        
        if (!Tool.isInParm(devTy.trim(), TY_ANDROID, TY_IOS)) {
            return (new Reply(RC.SPSS_IV_DTY));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyGni()
    {
        if ((gni.length() > L_GNI) ) {
            return (new Reply(RC.SPSS_IV_NIND));
        }

        if (!Tool.isNil(gni)) {
            if (!isFlag(gni))
                return (new Reply(RC.SPSS_IV_NIND));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyBni()
    {
        if ((bni.length() > L_BNI) ) {
            return (new Reply(RC.SPSS_IV_NIND));
        }

        if (!Tool.isNil(bni)) { 
            if (!isFlag(bni)) 
                return (new Reply(RC.SPSS_IV_NIND));
        }
        
        return (Reply.getSucc());
    }
    
    
    public static boolean isFlag(String rStr)
    {
        /*
            Return true if the rStr is Y or N
        */

        return (Tool.isInParm(rStr, Const.Y, Const.N));
    }
    
    
    public Reply verifyLang()
    {
        if (Tool.isNil(lang.trim()) || (lang.length() > L_LANG) ) {
            return (new Reply(RC.SPSS_IV_LANG));
        }

        if (!Tool.isInParm(lang.trim(), BiTx.LANG_ZH, BiTx.LANG_EN)) {
            return (new Reply(RC.SPSS_IV_LANG));
        }
        
        return (Reply.getSucc());
    }

}

