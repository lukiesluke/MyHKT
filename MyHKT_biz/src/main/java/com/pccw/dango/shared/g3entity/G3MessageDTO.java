package com.pccw.dango.shared.g3entity;

import java.io.Serializable;


public class G3MessageDTO implements Serializable
{
    private static final long serialVersionUID = 7475158929946391604L;
    
    private String textEng;
    private String textChi;
    private int textColorR;
    private int textColorG;
    private int textColorB;
    private String htmlEng;
    private String htmlChi;
    private String initialInMB;
    private String consumedInMB;
    private String remainingInMB;
    private String initialInKB;
    private String remainingInKB;
    private String consumedInKB;


    public G3MessageDTO() { 
    }
    
    public G3MessageDTO(String textEng, String textChi){
        this.textEng = textEng;
        this.textChi = textChi;
        this.textColorR = 0;
        this.textColorG = 0;
        this.textColorB = 0;
        this.htmlEng = "";
        this.htmlChi = "";
    }
    
    public G3MessageDTO(String textEng, String textChi, int textColorR,
            int textColorG, int textColorB, String htmlEng, String htmlChi) {
        this.textEng = textEng;
        this.textChi = textChi;
        this.textColorR = textColorR;
        this.textColorG = textColorG;
        this.textColorB = textColorB;
        this.htmlEng = htmlEng;
        this.htmlChi = htmlChi;
    }

    public String getTextEng() {
        return textEng;
    }
    
    
    public void setTextEng(String textEng) {
        this.textEng = textEng;
    }
    
    
    public String getTextChi() {
        return textChi;
    }
    
    
    public void setTextChi(String textChi) {
        this.textChi = textChi;
    }
    
    
    public int getTextColorR() {
        return textColorR;
    }
    
    
    public void setTextColorR(int textColorR) {
        this.textColorR = textColorR;
    }
    
    
    public int getTextColorG() {
        return textColorG;
    }
    
    
    public void setTextColorG(int textColorG) {
        this.textColorG = textColorG;
    }
    
    
    public int getTextColorB() {
        return textColorB;
    }
    
    
    public void setTextColorB(int textColorB) {
        this.textColorB = textColorB;
    }
    
    
    public String getHtmlEng() {
        return htmlEng;
    }
    
    
    public void setHtmlEng(String htmlEng) {
        this.htmlEng = htmlEng;
    }
    
    
    public String getHtmlChi() {
        return htmlChi;
    }
    
    
    public void setHtmlChi(String htmlChi) {
        this.htmlChi = htmlChi;
    }

    
    public String getInitialInMB() 
    {
        return initialInMB;
    }
    
    
    public void setInitialInMB(String rInitialInMB) 
    {
        initialInMB = rInitialInMB;
    }
    
    
    public String getConsumedInMB() 
    {
        return consumedInMB;
    }
    
    
    public void setConsumedInMB(String rConsumedInMB) 
    {
        consumedInMB = rConsumedInMB;
    }
    
    
    public String getRemainingInMB() 
    {
        return remainingInMB;
    }
    
    
    public void setRemainingInMB(String rRemainingInMB) 
    {
        remainingInMB = rRemainingInMB;
    }
    
    
    public String getRemainingInKB() 
    {
        return remainingInKB;
    }
    
    
    public void setRemainingInKB(String rArg) 
    {
        remainingInKB = rArg;
    }

    public String getInitialInKB() {
        return initialInKB;
    }

    public void setInitialInKB(String initialInKB) {
        this.initialInKB = initialInKB;
    }

    public String getConsumedInKB() {
        return consumedInKB;
    }

    public void setConsumedInKB(String consumedInKB) {
        this.consumedInKB = consumedInKB;
    }
}
