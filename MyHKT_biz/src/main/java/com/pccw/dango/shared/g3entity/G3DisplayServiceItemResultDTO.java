package com.pccw.dango.shared.g3entity;

import java.io.Serializable;
import java.util.List;

public class G3DisplayServiceItemResultDTO implements Serializable 
{
    private static final long serialVersionUID = 1407118998515831144L;

    private List <G3DisplayServiceItemDTO> displayServiceItemList;
    
    public List<G3DisplayServiceItemDTO> getDisplayServiceItemList() {
        return displayServiceItemList;
    }
    public void setDisplayServiceItemList(
            List<G3DisplayServiceItemDTO> displayServiceItemList) {
        this.displayServiceItemList = displayServiceItemList;
    }
    public String getAcctType() {
        return acctType;
    }
    public void setAcctType(String acctType) {
        this.acctType = acctType;
    }
    private String acctType;
    
    private String rtnCode;
    
    public String getRtnCode() {
        return rtnCode;
    }
    public void setRtnCode(String rtnCode) {
        this.rtnCode = rtnCode;
    }
    public String getRtnMsg() {
        return rtnMsg;
    }
    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
    private String rtnMsg;
}
