package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3ServiceCallBarStatusDTO implements Serializable 
{
    private static final long serialVersionUID = -7191864015700592348L;
    
    private String serviceStatusEng;
    private String serviceStatusChi;
    private String showServiceStatus;
    private String hasAlertMessage;
    private String alertMessageEng;
    private String alertMessageChi;
    private String alertLevel;
    private String callBarred;
    private String releaseCallBarPending;
    private String enableReleaseCallBar;
    
    private G3SubExtInfoDTO g3SubExtInfoDTO = new G3SubExtInfoDTO();
    private G3AccountInformationDTO g3AccountInformationDTO = new G3AccountInformationDTO();
    private G3PaymentRejectStatusDTO g3PaymentRejectStatusDTO = new G3PaymentRejectStatusDTO();
    
    public String getServiceStatusEng() {
        return serviceStatusEng;
    }
    public String getShowServiceStatus() {
        return showServiceStatus;
    }
    public void setShowServiceStatus(String showServiceStatus) {
        this.showServiceStatus = showServiceStatus;
    }
    public void setServiceStatusEng(String serviceStatusEng) {
        this.serviceStatusEng = serviceStatusEng;
    }
    public String getServiceStatusChi() {
        return serviceStatusChi;
    }
    public void setServiceStatusChi(String serviceStatusChi) {
        this.serviceStatusChi = serviceStatusChi;
    }
    public String getHasAlertMessage() {
        return hasAlertMessage;
    }
    public void setHasAlertMessage(String hasAlertMessage) {
        this.hasAlertMessage = hasAlertMessage;
    }
    public String getAlertMessageEng() {
        return alertMessageEng;
    }
    public G3SubExtInfoDTO getG3SubExtInfoDTO() {
        return g3SubExtInfoDTO;
    }
    public void setG3SubExtInfoDTO(G3SubExtInfoDTO g3SubExtInfoDTO) {
        this.g3SubExtInfoDTO = g3SubExtInfoDTO;
    }
    public G3AccountInformationDTO getG3AccountInformationDTO() {
        return g3AccountInformationDTO;
    }
    public void setG3AccountInformationDTO(
            G3AccountInformationDTO g3AccountInformationDTO) {
        this.g3AccountInformationDTO = g3AccountInformationDTO;
    }
    public G3PaymentRejectStatusDTO getG3PaymentRejectStatusDTO() {
        return g3PaymentRejectStatusDTO;
    }
    public void setG3PaymentRejectStatusDTO(
            G3PaymentRejectStatusDTO g3PaymentRejectStatusDTO) {
        this.g3PaymentRejectStatusDTO = g3PaymentRejectStatusDTO;
    }
    public void setAlertMessageEng(String alertMessageEng) {
        this.alertMessageEng = alertMessageEng;
    }
    public String getAlertMessageChi() {
        return alertMessageChi;
    }
    public void setAlertMessageChi(String alertMessageChi) {
        this.alertMessageChi = alertMessageChi;
    }
    public String getAlertLevel() {
        return alertLevel;
    }
    public void setAlertLevel(String alertLevel) {
        this.alertLevel = alertLevel;
    }
    public String getCallBarred() {
        return callBarred;
    }
    public void setCallBarred(String callBarred) {
        this.callBarred = callBarred;
    }
    public String getReleaseCallBarPending() {
        return releaseCallBarPending;
    }
    public void setReleaseCallBarPending(String releaseCallBarPending) {
        this.releaseCallBarPending = releaseCallBarPending;
    }
    public String getEnableReleaseCallBar() {
        return enableReleaseCallBar;
    }
    public void setEnableReleaseCallBar(String enableReleaseCallBar) {
        this.enableReleaseCallBar = enableReleaseCallBar;
    }
}
