/*
    Crate for Account Maintenance
    
    Serve:  Registration
            Regenerate SMS activation code
            Activation
            Service Association
            Change Password (after logged in)
            Reset Password (= Change Recalled Password)
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.BcifRec;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SimpRegn;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;

public class AcMainCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -8493300365687149931L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private CustRec                 iCustRec;           /* Customer Record                               */
    private BcifRec                 iBcifRec;           /* HKBR Customer Information                     */
    private SveeRec                 iSveeRec;           /* Servee (Customer with Login Id)               */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    private String                  iActCode;           /* Activation Code                               */
    private SubnRec[]               iSubnRecAry;        /* Array of Subscription Record                  */
    private boolean                 iChgPwd;            /* Change Password?                              */
    private String                  iOrigPwd;           /* Original Password                             */
    private SimpRegn                iSimpRegn;          /* Supplementary for Simplified Registration     */
    private String                  iSoGud;             /* Secure Operation Guard                        */
    private boolean                 iComm;              /* Business Registration?                        */
    private SveeRec                 oSveeRec;           /* Servee (Customer with Login Id)               */
    private BcifRec                 oBcifRec;           /* HKBR Customer                                 */
    private SubnRec[]               oSubnRecAry;        /* Array of Subscription Record                  */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public AcMainCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearICustRec();
        clearIBcifRec();
        clearISveeRec();
        clearISubnRec();
        clearIActCode();
        clearISubnRecAry();
        clearIChgPwd();
        clearIOrigPwd();
        clearISimpRegn();
        clearISoGud();
        clearIComm();
        clearOSveeRec();
        clearOBcifRec();
        clearOSubnRecAry();
    }


    public AcMainCra copyFrom(AcMainCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setICustRec(rSrc.getICustRec());
        setIBcifRec(rSrc.getIBcifRec());
        setISveeRec(rSrc.getISveeRec());
        setISubnRec(rSrc.getISubnRec());
        setIActCode(rSrc.getIActCode());
        setISubnRecAry(rSrc.getISubnRecAry());
        setIChgPwd(rSrc.isIChgPwd());
        setIOrigPwd(rSrc.getIOrigPwd());
        setISimpRegn(rSrc.getISimpRegn());
        setISoGud(rSrc.getISoGud());
        setIComm(rSrc.isIComm());
        setOSveeRec(rSrc.getOSveeRec());
        setOBcifRec(rSrc.getOBcifRec());
        setOSubnRecAry(rSrc.getOSubnRecAry());
        
        return (this);
    }


    public AcMainCra copyTo(AcMainCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public AcMainCra copyMe()
    {
        AcMainCra                     rDes;

        rDes = new AcMainCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearICustRec()
    {
        iCustRec = new CustRec();
    }


    public void setICustRec(CustRec rArg)
    {
        iCustRec = rArg;
    }


    public CustRec getICustRec()
    {
        return (iCustRec);
    }


    public void clearIBcifRec()
    {
        iBcifRec = new BcifRec();
    }


    public void setIBcifRec(BcifRec rArg)
    {
        iBcifRec = rArg;
    }


    public BcifRec getIBcifRec()
    {
        return (iBcifRec);
    }


    public void clearISveeRec()
    {
        iSveeRec = new SveeRec();
    }


    public void setISveeRec(SveeRec rArg)
    {
        iSveeRec = rArg;
    }


    public SveeRec getISveeRec()
    {
        return (iSveeRec);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }


    public void clearIActCode()
    {
        iActCode = "";
    }


    public void setIActCode(String rArg)
    {
        iActCode = rArg;
    }


    public String getIActCode()
    {
        return (iActCode);
    }


    public void clearISubnRecAry()
    {
        iSubnRecAry = new SubnRec[0];
    }


    public void setISubnRecAry(SubnRec[] rArg)
    {
        iSubnRecAry = rArg;
    }


    public SubnRec[] getISubnRecAry()
    {
        return (iSubnRecAry);
    }


    public void clearIChgPwd()
    {
        iChgPwd = false;
    }


    public void setIChgPwd(boolean rArg)
    {
        iChgPwd = rArg;
    }


    public boolean isIChgPwd()
    {
        return (iChgPwd);
    }


    public void clearIOrigPwd()
    {
        iOrigPwd = "";
    }


    public void setIOrigPwd(String rArg)
    {
        iOrigPwd = rArg;
    }


    public String getIOrigPwd()
    {
        return (iOrigPwd);
    }


    public void clearISimpRegn()
    {
        iSimpRegn = new SimpRegn();
    }


    public void setISimpRegn(SimpRegn rArg)
    {
        iSimpRegn = rArg;
    }


    public SimpRegn getISimpRegn()
    {
        return (iSimpRegn);
    }
    
    
    public void clearISoGud()
    {
        setISoGud("");
    }
    
    
    public void setISoGud(String rArg)
    {
        iSoGud = rArg;
    }
    
    
    public String getISoGud()
    {
        return (iSoGud);
    }

    
    public void clearIComm()
    {
        iComm = false;
    }


    public void setIComm(boolean rArg)
    {
        iComm = rArg;
    }


    public boolean isIComm()
    {
        return (iComm);
    }

    
    public void clearOSveeRec()
    {
        oSveeRec = new SveeRec();
    }


    public void setOSveeRec(SveeRec rArg)
    {
        oSveeRec = rArg;
    }


    public SveeRec getOSveeRec()
    {
        return (oSveeRec);
    }
    
    

    public void clearOBcifRec()
    {
        oBcifRec = new BcifRec();
    }


    public void setOBcifRec(BcifRec rArg)
    {
        oBcifRec = rArg;
    }


    public BcifRec getOBcifRec()
    {
        return (oBcifRec);
    }
    
    
    public void clearOSubnRecAry()
    {
        oSubnRecAry = new SubnRec[0];
    }


    public void setOSubnRecAry(SubnRec[] rArg)
    {
        oSubnRecAry = rArg;
    }


    public SubnRec[] getOSubnRecAry()
    {
        return (oSubnRecAry);
    }
}
