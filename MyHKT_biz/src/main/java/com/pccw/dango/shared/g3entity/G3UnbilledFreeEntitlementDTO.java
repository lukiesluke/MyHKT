package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3UnbilledFreeEntitlementDTO  implements Serializable 
{
    private static final long serialVersionUID = -8647749172377786603L;
    
    private String callType;
	private String serviceType;
	private String unit;
	private String value;

	public String getCallType() {
		return callType;
	}

	public void setCallType(String callType) {
		this.callType = callType;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}

