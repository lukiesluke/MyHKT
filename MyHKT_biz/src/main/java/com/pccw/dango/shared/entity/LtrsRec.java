/*
    Line Test Result Record
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class LtrsRec  implements Serializable
{
    private static final long serialVersionUID = 1374898206450462458L;
    
    public int                      rid;                /* Record Id                                     */
    public int                      subnRid;            /* SUBN rid                                      */
    public String                   subnLob;            /* LOB                                           */
    public String                   subnSrvNum;         /* Service Number                                */
    public String                   subnPcdInd;         /* PCD Indicator                                 */
    public String                   subnTvInd;          /* TV Indicator                                  */
    public String                   subnEyeInd;         /* EYE Indicator                                 */
    public String                   status;             /* Status                                        */
    public String                   pon;                /* PON Indicator                                 */
    public String                   bbNtwk;             /* BB Network Test Result                        */
    public String                   modem;              /* Modem Test Result                             */
    public String                   pcd;                /* PCD Device Test Result                        */
    public String                   tv;                 /* TV Device Test Result                         */
    public String                   eye;                /* Eye Device Test Result                        */
    public String                   eyety;              /* Eye Type                                      */
    public String                   voice;              /* Voice Line Test Result                        */
    public String                   pe;                 /* Voice Line PE Test Result                     */
    public String                   txtg4bb;            /* TXTG for BB Result                            */
    public String                   txtg4fxln;          /* TXTG for Fixed Line Result                    */
    public String                   bsn;                /* BSN (for Modem Reset Request)                 */
    public String                   usrRefno;           /* USR Reference Number                          */
    public String                   srStatus;           /* SR Status                                     */
    public String                   mdmMdl;             /* Modem Model                                   */
    public String                   mdmimgL;            /* Modem Image (Large)                           */
    public String                   mdmimgS;            /* Model Image (Small)                           */
    public String                   txtg4mdm;           /* TXTG for Modem (diag/reset instructions)      */
    public String                   doneTs;             /* Completion TS (for Status = I/E/D)            */
    public String                   createTs;           /* Record Create TS                              */
    public String                   createPsn;          /* Record Create Person                          */
    public String                   lastupdTs;          /* Last Update TS                                */
    public String                   lastupdPsn;         /* Last Update Person                            */
    public int                      rev;                /*                                               */
    
    public static final int         L_SUBN_LOB     = 6;
    public static final int         L_SUBN_SRV_NUM = 40;
    public static final int         L_SUBN_PCD_IND = 1;
    public static final int         L_SUBN_TV_IND  = 1;
    public static final int         L_SUBN_EYE_IND = 1;
    public static final int         L_STATUS       = 1;
    public static final int         L_PON          = 3;
    public static final int         L_BB_NTWK      = 3;
    public static final int         L_MODEM        = 3;
    public static final int         L_PCD          = 3;
    public static final int         L_TV           = 3;
    public static final int         L_EYE          = 3;
    public static final int         L_EYETY        = 4;
    public static final int         L_VOICE        = 3;
    public static final int         L_PE           = 3;
    public static final int         L_TXTG4BB      = 48;
    public static final int         L_TXTG4FXLN    = 48;
    public static final int         L_BSN          = 40;
    public static final int         L_USR_REFNO    = 64;
    public static final int         L_SR_STATUS    = 20;
    public static final int         L_MDM_MDL      = 32;
    public static final int         L_MDMIMG_L     = 128;
    public static final int         L_MDMIMG_S     = 128;
    public static final int         L_TXTG4MDM     = 48;
    public static final int         L_DONE_TS      = 14;
    public static final int         L_CREATE_TS    = 14;
    public static final int         L_CREATE_PSN   = 40;
    public static final int         L_LASTUPD_TS   = 14;
    public static final int         L_LASTUPD_PSN  = 40;

    public static final String      ST_PREPARE      = "P";
    public static final String      ST_READY        = "R";
    public static final String      ST_ERROR        = "E";
    public static final String      ST_IGNORE       = "I";
    public static final String      ST_RETEST       = "T";
    public static final String      ST_DONE         = "D";      
    
    public static final String      SR_STS_VCE      = "V";
    public static final String      SR_STS_BB       = "B";
    public static final String      SR_STS_RETEST   = "T";
    
    public static final String      RLT_GOOD        = "GOD";
    public static final String      RLT_NO_GOOD     = "NGD";
    public static final String      RLT_OUT_SYNC    = "OSY";
    public static final String      RLT_NA          = "NA";
    public static final String      RLT_NON_ASSESS  = "NAS";
    public static final String      RLT_ONLINE      = "ONL";
    public static final String      RLT_OFFLINE     = "OFF";
    public static final String      RLT_BRM_ON      = "BON";
    public static final String      RLT_BRM_OFF     = "BOF";
    public static final String      RLT_BUSY_OFFHOOK= "BOH";
    public static final String      RLT_BUSY_IN_CONV= "BIC"; 
    public static final String      RLT_FAULTY      = "FAU";
    public static final String      RLT_ERROR       = "ERR";
    public static final String      RLT_EMPTY       = " ";
    
    public static final String      EYETY_EYE       = "EYE";
    public static final String      EYETY_EYE1      = "EYE1";
    public static final String      EYETY_EYE2      = "EYE2";


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public LtrsRec()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        rid             = 0;
        subnRid         = 0;
        subnLob         = "";
        subnSrvNum      = "";
        subnPcdInd      = "";
        subnTvInd       = "";
        subnEyeInd      = "";
        status          = "";
        pon             = "";
        bbNtwk          = "";
        modem           = "";
        pcd             = "";
        tv              = "";
        eye             = "";
        eyety           = "";
        voice           = "";
        pe              = "";
        txtg4bb         = "";
        txtg4fxln       = "";
        bsn             = "";
        usrRefno        = "";
        srStatus        = "";
        mdmMdl          = "";
        mdmimgL         = "";
        mdmimgS         = "";
        txtg4mdm        = "";
        doneTs          = "";
        createTs        = "";
        createPsn       = "";
        lastupdTs       = "";
        lastupdPsn      = "";
        rev             = 0;
    }


    public LtrsRec copyFrom(LtrsRec rSrc)
    {
        rid             = rSrc.rid;
        subnRid         = rSrc.subnRid;
        subnLob         = rSrc.subnLob;
        subnSrvNum      = rSrc.subnSrvNum;
        subnPcdInd      = rSrc.subnPcdInd;
        subnTvInd       = rSrc.subnTvInd;
        subnEyeInd      = rSrc.subnEyeInd;
        status          = rSrc.status;
        pon             = rSrc.pon;
        bbNtwk          = rSrc.bbNtwk;
        modem           = rSrc.modem;
        pcd             = rSrc.pcd;
        tv              = rSrc.tv; 
        eye             = rSrc.eye;
        eyety           = rSrc.eyety;
        voice           = rSrc.voice;
        pe              = rSrc.pe;
        txtg4bb         = rSrc.txtg4bb;
        txtg4fxln       = rSrc.txtg4fxln;
        bsn             = rSrc.bsn;
        usrRefno        = rSrc.usrRefno;
        srStatus        = rSrc.srStatus;
        mdmMdl          = rSrc.mdmMdl;
        mdmimgL         = rSrc.mdmimgL;
        mdmimgS         = rSrc.mdmimgS;
        txtg4mdm        = rSrc.txtg4mdm;
        doneTs          = rSrc.doneTs;
        createTs        = rSrc.createTs;
        createPsn       = rSrc.createPsn;
        lastupdTs       = rSrc.lastupdTs;
        lastupdPsn      = rSrc.lastupdPsn;
        rev             = rSrc.rev;

        return (this);
    }


    public LtrsRec copyTo(LtrsRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public LtrsRec copyMe()
    {
        LtrsRec                     rDes;

        rDes = new LtrsRec();
        rDes.copyFrom(this);
        return (rDes);
    }
}
