/*
    Class for Directory Inquiry Request
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class DQReq implements Serializable 
{
    private static final long serialVersionUID = -4732716437606340651L;
    
    public String                   cid;
    public String                   lang;
    public String                   fName;
    public String                   gName;
    public String                   perArea;
    public String                   perDist;
    public String                   bizName;
    public String                   bizArea;
    public String                   bizDist;
    public String                   organ;
    public String                   dept;
    public String                   directory;
    public String                   address;
    public String                   remoteAddr;
    public DQPageInfo               pageInfo;


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public DQReq()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        cid             = "";
        lang            = "";
        fName           = "";
        gName           = "";
        perArea         = "";
        perDist         = "";
        bizName         = "";
        bizArea         = "";
        bizDist         = "";
        organ           = "";
        dept            = "";
        directory       = "";
        address         = "";
        remoteAddr      = "";
        pageInfo        = new DQPageInfo();
    }
    
    
    public DQReq copyFrom(DQReq rSrc)
    {
        cid             = rSrc.cid;
        lang            = rSrc.lang;
        fName           = rSrc.fName;
        gName           = rSrc.gName;
        perArea         = rSrc.perArea;
        perDist         = rSrc.perDist;
        bizName         = rSrc.bizName;
        bizArea         = rSrc.bizArea;
        bizDist         = rSrc.bizDist;
        organ           = rSrc.organ;
        dept            = rSrc.dept;
        directory       = rSrc.directory;
        address         = rSrc.address;
        remoteAddr      = rSrc.remoteAddr;
        pageInfo        = rSrc.pageInfo.copyMe();
    
        return (this);
    }
    
    
    public DQReq copyTo(DQReq rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public DQReq copyMe()
    {
        DQReq             rDes;
        
        rDes = new DQReq();
        rDes.copyFrom(this);
        
        return (rDes);
    }
}
