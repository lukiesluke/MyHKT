/*
    Service Request (for Appointment)
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;
import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;

public class SrvReq implements Serializable
{
    private static final long serialVersionUID = 2021050554649309422L;
    
    private String                  srvNum;             /* Service Number                                */
    private String                  srvNumTy;           /* Service Number Type                           */
    private String                  reportNum;          /* Report Number                                 */
    private String                  trunkNum;           /* Trunk Number                                  */
    private String                  extNum;             /* Extension Number                              */
    private String                  refNum;             /* Reference Number                              */
    private String                  outProd;            /* Out Product                                   */
    private SRApptTS                apptTS;             /* Appointment Timeslot                          */
    private String                  ctNmTtl;            /* Contact Name Title                            */
    private String                  ctNm;               /* Contact Name                                  */
    private String                  ctNum;              /* Contact Number                                */
    private String                  allowUpdInd;        /* Allow Update Indicator                        */
    private String                  autoSRInd;          /* Auto SR Indicator: V (Voice), B (BroadBand) o */
    private String                  enSpkr;             /* English Speaker                               */
    private String                  smsLang;            /* SMS Language                                  */
    private SRInfo                  srInfo;             /* SR Information                                */
    private ApptInfo                apptInfo;           /* Appointment Information                       */
    private String                  updTy;              /* Update Type                                   */
    private SubnRec                 assocSubnRec;       /* Link to Subscription Record                   */

    
    public static String            UP_TY_CONTACT       = "CONTACT";
    public static String            UP_TY_APPT          = "APPT";
    public static String            UP_TY_BOTH          = "BOTH";
    
    public static final String      AUTO_SR_IND_B       = "B";                      /* No oustanding - Broadband                        */
    public static final String      AUTO_SR_IND_V       = "V";                      /* No oustanding - Voice                            */
    public static final String      AUTO_SR_IND_N       = "N";                      /* Outstanding SR found                             */

    public static final String      SRV_TY_DN          = "DN";
    public static final String      SRV_TY_FSA         = "FSA";
    public static final String      SRV_TY_BSN         = "BSN";
    
    public static final String      OUT_PROD_VCE       = "Voice";
    public static final String      OUT_PROD_PCD       = "PCD";
    public static final String      OUT_PROD_VI        = "VI";
    public static final String      OUT_PROD_EYE       = "EYE";

    public static final String      PROD_TY_V          = "V";
    public static final String      PROD_TY_I          = "I";

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public SrvReq()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearSrvNum();
        clearSrvNumTy();
        clearReportNum();
        clearTrunkNum();
        clearExtNum();
        clearRefNum();
        clearOutProd();
        clearApptTS();
        clearCtNmTtl();
        clearCtNm();
        clearCtNum();
        clearAllowUpdInd();
        clearAutoSRInd();
        clearEnSpkr();
        clearSmsLang();
        clearSrInfo();
        clearApptInfo();
        clearUpdTy();
        clearAssocSubnRec();
    }


    public SrvReq copyFrom(SrvReq rSrc)
    {
        setSrvNum(rSrc.getSrvNum());
        setSrvNumTy(rSrc.getSrvNumTy());
        setReportNum(rSrc.getReportNum());
        setTrunkNum(rSrc.getTrunkNum());
        setExtNum(rSrc.getExtNum());
        setRefNum(rSrc.getRefNum());
        setOutProd(rSrc.getOutProd());
        setApptTS(rSrc.getApptTS());
        setCtNmTtl(rSrc.getCtNmTtl());
        setCtNm(rSrc.getCtNm());
        setCtNum(rSrc.getCtNum());
        setAllowUpdInd(rSrc.getAllowUpdInd());
        setAutoSRInd(rSrc.getAutoSRInd());
        setEnSpkr(rSrc.getEnSpkr());
        setSmsLang(rSrc.getSmsLang());
        setSrInfo(rSrc.getSrInfo());
        setApptInfo(rSrc.getApptInfo());
        setUpdTy(rSrc.getUpdTy());
        setAssocSubnRec(rSrc.getAssocSubnRec());

        return (this);
    }


    public SrvReq copyTo(SrvReq rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SrvReq copyMe()
    {
        SrvReq                      rDes;

        rDes = new SrvReq();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearSrvNum()
    {
        srvNum = "";
    }


    public void setSrvNum(String rArg)
    {
        srvNum = rArg;
    }


    public String getSrvNum()
    {
        return (srvNum);
    }


    public void clearSrvNumTy()
    {
        srvNumTy = "";
    }


    public void setSrvNumTy(String rArg)
    {
        srvNumTy = rArg;
    }


    public String getSrvNumTy()
    {
        return (srvNumTy);
    }


    public void clearReportNum()
    {
        reportNum = "";
    }


    public void setReportNum(String rArg)
    {
        reportNum = rArg;
    }


    public String getReportNum()
    {
        return (reportNum);
    }


    public void clearTrunkNum()
    {
        trunkNum = "";
    }


    public void setTrunkNum(String rArg)
    {
        trunkNum = rArg;
    }


    public String getTrunkNum()
    {
        return (trunkNum);
    }


    public void clearExtNum()
    {
        extNum = "";
    }


    public void setExtNum(String rArg)
    {
        extNum = rArg;
    }


    public String getExtNum()
    {
        return (extNum);
    }


    public void clearRefNum()
    {
        refNum = "";
    }


    public void setRefNum(String rArg)
    {
        refNum = rArg;
    }


    public String getRefNum()
    {
        return (refNum);
    }


    public void clearOutProd()
    {
        outProd = "";
    }


    public void setOutProd(String rArg)
    {
        outProd = rArg;
    }


    public String getOutProd()
    {
        return (outProd);
    }


    public void clearApptTS()
    {
        apptTS = new SRApptTS();
    }


    public void setApptTS(SRApptTS rArg)
    {
        apptTS = rArg;
    }


    public SRApptTS getApptTS()
    {
        return (apptTS);
    }


    public void clearCtNmTtl()
    {
        ctNmTtl = "";
    }


    public void setCtNmTtl(String rArg)
    {
        ctNmTtl = rArg;
    }


    public String getCtNmTtl()
    {
        return (ctNmTtl);
    }


    public void clearCtNm()
    {
        ctNm = "";
    }


    public void setCtNm(String rArg)
    {
        ctNm = rArg;
    }


    public String getCtNm()
    {
        return (ctNm);
    }


    public void clearCtNum()
    {
        ctNum = "";
    }


    public void setCtNum(String rArg)
    {
        ctNum = rArg;
    }


    public String getCtNum()
    {
        return (ctNum);
    }


    public void clearAllowUpdInd()
    {
        allowUpdInd = "";
    }


    public void setAllowUpdInd(String rArg)
    {
        allowUpdInd = rArg;
    }


    public String getAllowUpdInd()
    {
        return (allowUpdInd);
    }


    public void clearAutoSRInd()
    {
        autoSRInd = "";
    }


    public void setAutoSRInd(String rArg)
    {
        autoSRInd = rArg;
    }


    public String getAutoSRInd()
    {
        return (autoSRInd);
    }


    public void clearEnSpkr()
    {
        enSpkr = "";
    }


    public void setEnSpkr(String rArg)
    {
        enSpkr = rArg;
    }


    public String getEnSpkr()
    {
        return (enSpkr);
    }


    public void clearSmsLang()
    {
        smsLang = "";
    }


    public void setSmsLang(String rArg)
    {
        smsLang = rArg;
    }


    public String getSmsLang()
    {
        return (smsLang);
    }


    public void clearSrInfo()
    {
        srInfo = new SRInfo();
    }


    public void setSrInfo(SRInfo rArg)
    {
        srInfo = rArg;
    }


    public SRInfo getSrInfo()
    {
        return (srInfo);
    }


    public void clearApptInfo()
    {
        apptInfo = new ApptInfo();
    }


    public void setApptInfo(ApptInfo rArg)
    {
        apptInfo = rArg;
    }


    public ApptInfo getApptInfo()
    {
        return (apptInfo);
    }


    public void clearUpdTy()
    {
        updTy = "";
    }


    public void setUpdTy(String rArg)
    {
        updTy = rArg;
    }


    public String getUpdTy()
    {
        return (updTy);
    }


    public void clearAssocSubnRec()
    {
        assocSubnRec = new SubnRec();
    }


    public void setAssocSubnRec(SubnRec rArg)
    {
        assocSubnRec = rArg;
    }


    public SubnRec getAssocSubnRec()
    {
        return (assocSubnRec);
    }
    
    
    public Reply verifyCtNum(boolean rAllowEmpty)
    {
        if (ctNum.length() == 0) {
            if (rAllowEmpty) return (Reply.getSucc());
            return (new Reply(RC.SR_IVCTNUM));
        }
        
        if (!MyTool.isVaMob(ctNum)) return (new Reply(RC.SR_IVCTNUM));
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyCtName()
    {
        if (ctNm.length() < 1 || ctNm.length() > 40) {
            return (new Reply(RC.SR_IVCTNAME));
        }

        if (!Tool.isASC(ctNm)) {
            return (new Reply(RC.SR_NONEN_NAME));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyApptDt()
    {
        if (apptTS.getApptDate().length() < 1) {
            return (new Reply(RC.SR_IVAPPTDT));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyApptTimeSlot()
    {
        if (apptTS.getApptTmslot().length() < 1) {
            return (new Reply(RC.SR_IVAPPTTS));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyEnSpkr()
    {
        if (!MyTool.isYOrN(enSpkr)) {
            return (new Reply(RC.SR_IVENSPKR));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifySmsLang()
    {
        if (!BiTx.LANG_ZH.equals(smsLang) && !BiTx.LANG_EN.equals(smsLang)) {
            return (new Reply(RC.SR_IVSMSLANG));
        }

        return (Reply.getSucc());
    }

    
    public Reply verifyUpdateTy()
    {
        if (!UP_TY_CONTACT.equals(updTy) && !UP_TY_APPT.equals(updTy) && !UP_TY_BOTH.equals(updTy)) {
            return (new Reply(RC.SR_IVUPDTY));
        }

        return (Reply.getSucc());
    }

    
    public Reply verify4CreateSR()
    {
        Reply                       rRc;
        
        rRc = verifyCtName();
        if (!rRc.isSucc()) return rRc;
        
        rRc = verifyCtNum(false);
        if (!rRc.isSucc()) return rRc;

        rRc = verifyApptDt();
        if (!rRc.isSucc()) return rRc;

        rRc = verifyApptTimeSlot();
        if (!rRc.isSucc()) return rRc;

        rRc = verifyEnSpkr();
        if (!rRc.isSucc()) return rRc;

        rRc = verifySmsLang();
        if (!rRc.isSucc()) return rRc;
        
        return (Reply.getSucc());
    }
    
    
    public Reply verify4UpdateSR()
    {
        Reply                       rRc;

        rRc = verifyUpdateTy();
        if (!rRc.isSucc()) return rRc;

        if (UP_TY_CONTACT.equals(updTy) || UP_TY_BOTH.equals(updTy)) 
        {         
            rRc = verifyCtName();
            if (!rRc.isSucc()) return rRc;
            
            rRc = verifyCtNum(false);
            if (!rRc.isSucc()) return rRc;
        }

        if (UP_TY_APPT.equals(updTy) || UP_TY_BOTH.equals(updTy)) 
        {
            rRc = verifyApptDt();
            if (!rRc.isSucc()) return rRc;

            rRc = verifyApptTimeSlot();
            if (!rRc.isSucc()) return rRc;
        }

        rRc = verifySmsLang();
        if (!rRc.isSucc()) return rRc;
        
        return (Reply.getSucc());
    }
}
