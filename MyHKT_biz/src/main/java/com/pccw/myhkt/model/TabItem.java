package com.pccw.myhkt.model;

public class TabItem {
	
	
	public int 		imageRes;
	public String  	text;
	public int	 	pageNum;
	public int 		pageConst = -1;
		
	public TabItem(int imageRes, String text, int pageNum) {
		this.imageRes = imageRes;
		this.text = text;
		this.pageNum = pageNum;
	}
	
	public int getPageConst() {
		return pageConst;
	}

	public void setPageConst(int pageConst) {
		this.pageConst = pageConst;
	}
}