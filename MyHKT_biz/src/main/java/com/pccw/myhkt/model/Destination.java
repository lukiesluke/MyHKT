package com.pccw.myhkt.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Destination implements Parcelable{
	
	public String	engDestName;
	public String	engDestNameExt;
	public String	chiDestName;
	public String	chiDestNameExt;
	
    public Destination() {
    }

    public Destination(Parcel in) {
        this.engDestName = in.readString();
        this.engDestNameExt = in.readString();
        this.chiDestName = in.readString();
        this.chiDestNameExt = in.readString();
    }

    public static final Destination.Creator CREATOR = new Destination.Creator() {
        public Destination createFromParcel(Parcel in) {
            return new Destination(in);
        }

        public Destination[] newArray(int size) {
            return new Destination[size];
        }
    };
    
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

}
