package com.pccw.myhkt.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Rate implements Parcelable{

	public String timeZone;
	public String chiTimeZone;
	public String timeZoneRemarks;
	public String chiTimeZoneRemarks;
	public String rate;

    public Rate() { }

    public Rate(Parcel in) {
        this.timeZone = in.readString();
        this.chiTimeZone = in.readString();
        this.timeZoneRemarks = in.readString();
        this.chiTimeZoneRemarks = in.readString();
        this.rate = in.readString();
    }

    public static final Rate.Creator CREATOR = new Rate.Creator() {
        public Rate createFromParcel(Parcel in) {
            return new Rate(in);
        }

        public Rate[] newArray(int size) {
            return new Rate[size];
        }
    };
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
	}

}
