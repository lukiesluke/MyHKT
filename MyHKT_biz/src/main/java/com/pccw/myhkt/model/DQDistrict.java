package com.pccw.myhkt.model;

import java.io.Serializable;

public class DQDistrict implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 6342059939225174394L;
	public String				ename;
	public String				cname;
	public String				value;

	public DQDistrict() {
		ename = new String();
		cname = new String();
		value = new String();
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename.trim();
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname.trim();
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value.trim();
	}

}