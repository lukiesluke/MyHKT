package com.pccw.myhkt.model;

/************************************************************************
File       : VersionCheck.java
Desc       : GSon object for storing parsed version check data from server
Name       : Version check
Created by : Ryan Wong
Date       : 19/04/2013

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
19/04/2013 Ryan Wong		- First draft

*************************************************************************/

import java.io.Serializable;

public class VersionCheck implements Serializable {

	private static final long	serialVersionUID	= 2640183840618865586L;

	private boolean				forcedCheck;
	private String				headVersion;
	private String[]			blockedVersion;

	public VersionCheck() {
		initAndClear();
	}

	final void initAndClear() {
		init();
		clear();
	}

	protected void init() {
	}

	public void clear() {
		clearForcedCheck();
		clearHeadVersion();
		clearBlockedVersion();
	}

	public VersionCheck copyFrom(VersionCheck rSrc) {
		setForcedCheck(rSrc.getForcedCheck());
		setHeadVersion(rSrc.getHeadVersion());
		setBlockedVersion(rSrc.getBlockedVersion());

		return (this);
	}

	public VersionCheck copyTo(VersionCheck rDes) {
		rDes.copyFrom(this);
		return (rDes);
	}

	public VersionCheck copyMe() {
		VersionCheck rDes;

		rDes = new VersionCheck();
		rDes.copyFrom(this);

		return (rDes);
	}

	public boolean getForcedCheck() {
		return forcedCheck;
	}

	public String getHeadVersion() {
		return headVersion;
	}

	public String[] getBlockedVersion() {
		return blockedVersion;
	}

	public void setForcedCheck(boolean rForcedCheck) {
		forcedCheck = rForcedCheck;
	}

	public void setHeadVersion(String rHeadVersion) {
		headVersion = rHeadVersion;
	}

	public void setBlockedVersion(String[] rBlockedVersion) {
		blockedVersion = rBlockedVersion;
	}

	public void clearForcedCheck() {
		setForcedCheck(false);
	}

	public void clearHeadVersion() {
		setHeadVersion("");
	}

	public void clearBlockedVersion() {
		setBlockedVersion(new String[0]);
	}

}
