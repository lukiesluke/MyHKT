package com.pccw.myhkt.service;

import com.pccw.biz.myhkt.R;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

public class ConnectionCheckService extends IntentService {
	private boolean					debug	= 	false;	// toggle debug logs
	private static ConnectionCheckService	me;
	
    public ConnectionCheckService() {
        super("ConnectionCheckService");
    }
    
    @Override
    public void onCreate() {
    	super.onCreate();
		// The service is being created
		debug = getResources().getBoolean(R.bool.DEBUG);
    	me = this;
    }
    
    @Override
    protected void onHandleIntent(Intent intent) {
      Bundle extras = intent.getExtras();
      boolean isNetworkConnected = extras.getBoolean("isNetworkConnected");
   }

}
