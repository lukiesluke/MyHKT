package com.pccw.myhkt.service;

import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.SplashActivity;
import com.pccw.myhkt.model.PushData;

/************************************************************************
File       : GCMIntentService.java
Desc       : Google Cloud Message Service for Push Notification
Name       : GCMIntentService
Created by : Andy Wong
Date       : 23/03/2016

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
23/03/2016 Andy Wong			- First draft
*************************************************************************/

public class GCMIntentService extends IntentService {
	private boolean					debug	= 	false;	// toggle debug logs
	private static GCMIntentService	me;
	
    public GCMIntentService() {
        super("GCMIntentService");
    }
	
    @Override
    public void onCreate() {
    	super.onCreate();
		// The service is being created
		debug = getResources().getBoolean(R.bool.DEBUG);
    	me = this;
    }
    
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		try {
			if (debug) Log.d("GCM Service says: ", "An Intent Recieved via GCMBroadcast Receiver");
	        Bundle bundle = intent.getExtras();
	        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
	        // The getMessageType() intent parameter must be the intent you received
	        // in your BroadcastReceiver.
	        String messageType = gcm.getMessageType(intent);     
	        if (!bundle.isEmpty()) {  // has effect of unparcelling Bundle
	            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
	                //sendNotification("Send error: " + extras.toString());
	            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
	                //sendNotification("Deleted messages on server: " + extras.toString());
	            // If it's a regular GCM message
	            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
	        		if (debug) Log.d("GCM Service says: ", "Valid GCM Message Received");
	        		// Open bundle
	        		String message = bundle.getString("message");
	        		if (debug) Log.d("GCM Service says: ", "Received message:" + message);
	        		String loginId = bundle.getString("loginId");
	        		if (debug) Log.d("GCM Service says: ", "Received loginid:" + loginId);
	        		String acctNum = bundle.getString("acctNum");
	        		if (debug) Log.d("GCM Service says: ", "Received accNum:" + acctNum);
	        		String type = bundle.getString("type");
	        		if (debug) Log.d("GCM Service says: ", "Received type:" + type);
	        		
	        		//type G Generate General Noti, type B Generate Bill Noti
	        		if (bundle.getString("type").equalsIgnoreCase("G")) {
	        			generateNotificationGeneral(me.getApplicationContext(), intent);
	        		} else if (bundle.getString("type").equalsIgnoreCase("B")) {
	        			generateNotificationBill(me.getApplicationContext(), intent);
	        		} else {
	        			//Error retrieving Type
	        		}
	        			
	        		if (debug) Log.d("GCM Service says: ", "Notification launched. Data: " + bundle.toString());
	        	}
	            //Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
	            // Post notification of received message.
	            //generateNotificationBill(me, "Received: " + bundle.toString());
	            //Log.i(TAG, "Received: " + extras.toString())
	        }
	    	    // Release the wake lock provided by the WakefulBroadcastReceiver.
	    	    GCMBroadcastReceiver.completeWakefulIntent(intent);	    
		} catch (Exception e) {
			// fail to extract object
			if (debug) Log.d("GCM Service says: ", e.toString());
		}
    }

	
	private static void generateNotificationGeneral(Context context, Intent msgIntent) {
		//Notification Intent identifier
		String pass = "GENERAL";
		Bundle bundle = msgIntent.getExtras();
		PushData pushData = new PushData();
		if (bundle.getString("message") != null) pushData.setMessage(bundle.getString("message"));
		if (bundle.getString("loginId") != null) pushData.setLoginId(bundle.getString("loginId"));
		if (bundle.getString("acctNum") != null) pushData.setAcctNum(bundle.getString("acctNum"));
		if (bundle.getString("type") != null) pushData.setType(bundle.getString("type"));
		
		long when = System.currentTimeMillis();
		// Save all data before stopService
		String title = Utils.getString(me.getApplicationContext(), R.string.app_name);

		Intent notificationIntent = null;
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				// MyHKT is not running in foreground
				notificationIntent = new Intent(context, SplashActivity.class);
				// specify notification intent by setData()
				//notificationIntent.setData(new Uri.Builder().scheme("data").appendQueryParameter("text", pass).build());
				notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				notificationIntent.putExtra("GENMSG_READNOW", true);
				notificationIntent.putExtra("SERIAL_GENMSG", pushData);
			} else {
				// MyHKT is running in foreground and directly open the ServiceActivity
				ClnEnv.setPushDataGen(pushData);
				me.redirectDialog(context, R.string.CONST_DIALOG_MSG_GEN);
			}
		}

		// Only notification when the App is inactive
		if (notificationIntent != null) {

			PendingIntent intent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
			mBuilder.setSmallIcon(R.drawable.icon_notification);
			// For Android 3.0 and 3.1 may be crashed by setLargeIcon
			if (Build.VERSION.SDK_INT >= 14) mBuilder.setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hktappicon)).getBitmap());
			mBuilder.setWhen(when);
			mBuilder.setContentTitle(title);
			mBuilder.setTicker(pushData.getMessage());
			mBuilder.setContentText(pushData.getMessage());
			mBuilder.setAutoCancel(true);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);
			mBuilder.setContentIntent(intent);
			Notification notification = mBuilder.build();
			notification = new NotificationCompat.BigTextStyle(mBuilder).bigText(pushData.getMessage()).build();

			NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			//mNotificationManager.notify(context.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_GEN), notification);
			mNotificationManager.notify(Integer.parseInt(Utils.randomString(5)), notification);
		}
	}
	
	
	private static void generateNotificationBill(Context context, Intent msgIntent) {
		if (me.debug) Log.d("GEN BILL", "STARTED");
		//Notification Intent identifier
		String pass = "BILL";
		Bundle bundle = msgIntent.getExtras();
		PushData pushData = new PushData();
		if (bundle.getString("message") != null) pushData.setMessage(bundle.getString("message"));
		if (bundle.getString("loginId") != null) pushData.setLoginId(bundle.getString("loginId"));
		if (bundle.getString("acctNum") != null) pushData.setAcctNum(bundle.getString("acctNum"));
		if (bundle.getString("type") != null) pushData.setType(bundle.getString("type"));
		
		if (me.debug) Log.d("GEN BILL", "Push data received, pushData message = " + pushData.getMessage());
		long when = System.currentTimeMillis();
		// Save all data before stopService
		String title = Utils.getString(me.getApplicationContext(), R.string.app_name);

		Intent notificationIntent = null;
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				// MyHKT is not running in foreground
				notificationIntent = new Intent(context, SplashActivity.class);
				// specify notification intent by setData()
				//notificationIntent.setData(new Uri.Builder().scheme("data").appendQueryParameter("text", pass).build());
			    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				notificationIntent.putExtra("BILLMSG_READNOW", true);
				notificationIntent.putExtra("SERIAL_BILLMSG", pushData);
				if (me.debug) Log.d("GEN BILL", "notificationIntent prepared");
				if (me.debug) Log.d("GEN BILL", "notificationIntent prepared, pushData message = " + pushData.getMessage());
			} else {
				// MyHKT is running in foreground and directly open the ServiceActivity
				ClnEnv.setPushDataBill(pushData);
				try {
					if (ClnEnv.getPushDataBill() != null) {
			        	String loginID = ClnEnv.getPushDataBill().getLoginId();
				    	if ((!"".equalsIgnoreCase(loginID) && !loginID.equalsIgnoreCase(ClnEnv.getSessionLoginID())) || "".equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
				    		Utils.clearBillMsgService(me);
				    	} else if (ClnEnv.isLoggedIn()) {
			        	// Check subscriptionRec is valid in current ServiceList
				        	Boolean isValidSubnRec = false;
					        for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry())) {
					        		if (rSubnRec.acctNum.equalsIgnoreCase(ClnEnv.getPushDataBill().getAcctNum())) {
					        			isValidSubnRec = true;
					        		}
					        }
					        if (isValidSubnRec) {
								me.redirectDialog(context, R.string.CONST_DIALOG_MSG_BILL);
					        } else {
					        	Utils.clearBillMsgService(me);
					        }
				    	}
					}
				} catch (Exception e) {
					//Prevent throw null pointer exception for BillPushData
					ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
					return;
				}
			}
		}

		// Only notification when the App is inactive
		if (notificationIntent != null) {
			PendingIntent intent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
			mBuilder.setSmallIcon(R.drawable.icon_notification);
			// For Android 3.0 and 3.1 may be crashed by setLargeIcon
			if (Build.VERSION.SDK_INT >= 14) mBuilder.setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hktappicon)).getBitmap());
			mBuilder.setWhen(when);
			mBuilder.setContentTitle(title);
			mBuilder.setTicker(pushData.getMessage());
			mBuilder.setContentText(pushData.getMessage());
			mBuilder.setAutoCancel(true);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);
			mBuilder.setContentIntent(intent);
			Notification notification = mBuilder.build();
			notification = new NotificationCompat.BigTextStyle(mBuilder).bigText(pushData.getMessage()).build();

			NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			//mNotificationManager.notify(context.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_BILL), notification);
			mNotificationManager.notify(Integer.parseInt(Utils.randomString(5)), notification);
		}
	}
	
	// redirect to Target Activity
	protected final void redirectDialog(Context context, int dialogType) {
		Intent dialogIntent = new Intent(context, GlobalDialog.class);
		dialogIntent.putExtra("DIALOGTYPE", dialogType);
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(dialogIntent);
	}
}
