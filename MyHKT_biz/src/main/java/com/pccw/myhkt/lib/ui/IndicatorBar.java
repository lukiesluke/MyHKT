package com.pccw.myhkt.lib.ui;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.pccw.biz.myhkt.R;
//Ref : https://github.com/JakeWharton/ViewPagerIndicator/blob/master/library/src/com/viewpagerindicator/PageIndicator.java
public class IndicatorBar extends View {

	private float mPointX=0;
	private float mPointY=0;
	private Bitmap bitmap;
	private int    tabCount;
	private int    tabPos;
	private String TAG = "IndicatorBar";

	public IndicatorBar(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	} 
	public IndicatorBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public IndicatorBar(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public void initPos(int tabPos , int tabCount){
		this.tabCount = tabCount;
		this.tabPos = tabPos;
		isFirstTime = true;
	}


	Paint p;
	Boolean isFirstTime = true;
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		p=new Paint();
	
		if(bitmap ==null) {
			bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.segment_indicator);        
		}

		p.setColor(Color.RED);
		if (isFirstTime) {
			isFirstTime = false;
			mPointX = (float) (canvas.getWidth() / tabCount * (tabPos + 0.5));		
		} else {
			
		}
		canvas.drawBitmap(bitmap, mPointX - bitmap.getWidth()/2 , mPointY, p);		
		this.invalidate();

	}

	public void updatePos(float pointx){
		isFirstTime = false;
		mPointX = pointx;
//		Log.i(TAG, mPointX+"");
		this.invalidate();
	}

	public float getPos(){
		return mPointX;
	}



}
