package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;

public class HomeImageButton extends RelativeLayout {

	private int		m_color				= Color.argb(128, 128, 128, 128);
	private AQuery 	aq ;
	private int	 	textColor			= Color.WHITE;
	private int 	pressedTextColor;
	public HomeImageButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public HomeImageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		//		initViews(context, attrs);
	}

	public HomeImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		//		initViews(context, attrs);
	}

	//	private void initViews(final Context context, AttributeSet attrs) {
	//		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HomeImageButton, 0, 0);
	//		AQuery aq = new AQuery(this);
	//
	//		LayoutInflater.from(context).inflate(R.layout.button_home, this);
	//		try {
	//
	//			int imagepappding = (int) context.getResources().getDimension(R.dimen.mainmenu_gridlayout_padding)*2;
	//
	//			aq.id(R.id.home_txt).text(a.getText(R.styleable.HomeImageButton_android_text));
	//			aq.id(R.id.home_txt).getTextView().setTextSize(Utils.dpToPx(a.getInt(R.styleable.HomeImageButton_android_textSize, 14)));
	//			aq.id(R.id.home_imageview).image(a.getDrawable(R.styleable.HomeImageButton_android_src));
	//
	//			// Style Initialize
	//			//			aq.id(R.id.home_txt).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
	//			aq.id(R.id.home_txt).getTextView().setMinLines(2);
	//			aq.id(R.id.home_txt).getTextView().setMaxLines(2);
	//			aq.id(R.id.home_txt).getTextView().setGravity(Gravity.CENTER);
	//			aq.id(R.id.home_txt).textColor(context.getResources().getColor(R.color.white));
	//			//			aq.id(R.id.home_txt).height(textViewHeight , false);		
	//
	//			aq.id(R.id.home_imageview).getImageView().setScaleType(ImageView.ScaleType.FIT_CENTER);
	//			aq.id(R.id.home_imageview).getImageView().setPadding(imagepappding, imagepappding, imagepappding, imagepappding);
	//
	//		} finally {
	//			a.recycle();
	//		}
	//	}

	private Boolean premier = false;

	public void initViews(final Context context, int imageRes, String text, int textSize, int textColor, int bgcolorRes, int height, boolean isZh, boolean premier) {
		this.textColor = textColor;
		this.premier = premier;
		initViews(context, imageRes, text, textSize, bgcolorRes, height, isZh);
	}
	
	public void initViews(final Context context, int imageRes, String text, int textSize, int textColor, int bgcolorRes, int height, boolean isZh) {
		this.textColor = textColor;
		initViews(context, imageRes, text, textSize, bgcolorRes, height, isZh);
	}
	
	public void initViews(final Context context, int imageRes, String text, int textSize, int bgcolorRes, int height, boolean isZh) {
		LayoutInflater.from(context).inflate(R.layout.button_home, this);
		aq = new AQuery(this);
		aq.id(R.id.home_txt).text(text);
		aq.id(R.id.home_imageview).image(imageRes != -1 ? context.getResources().getDrawable(imageRes) : null);
		int imagepappding = (int) context.getResources().getDimension(R.dimen.mainmenu_gridlayout_padding)*3/2;
		int imageHeight = height * 2 /5;
		int textViewHeight = height * 2 /5;

		//top and bottom space adjustment
		LinearLayout.LayoutParams topParams = (LinearLayout.LayoutParams) aq.id(R.id.home_top_layout).getView().getLayoutParams();
		LinearLayout.LayoutParams botParams = (LinearLayout.LayoutParams) aq.id(R.id.home_bottom_layout).getView().getLayoutParams();
		if (isZh) {
			imageHeight		 = premier ? height * 3/5 : height * 2 /5;
			textViewHeight	 = premier ? height * 3/5 : height * 2 /5;
			topParams.weight = 12.0f;
			botParams.weight = 8.0f;
		} else {
			imageHeight		 = premier ? height * 3/5 : height * 2 /5;
			textViewHeight	 = premier ? height * 3/5 : height * 2 /5;
			topParams.weight = premier ? 12.0f : 10.5f;
			botParams.weight = premier ? 9.0f : 9.5f;
		}
		aq.id(R.id.home_top_layout).getView().setLayoutParams(topParams);
		aq.id(R.id.home_bottom_layout).getView().setLayoutParams(botParams);


		// Style Initialize
		//		aq.id(R.id.home_txt).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		//		aq.id(R.id.home_txt).getTextView().setMinLines(2);
		//		aq.id(R.id.home_txt).getTextView().setMaxLines(2);
		aq.id(R.id.home_txt).getTextView().setGravity(Gravity.CENTER_HORIZONTAL);
		aq.id(R.id.home_txt).textColor(textColor).height(textViewHeight , false);		
		aq.id(R.id.home_txt).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		aq.id(R.id.home_bottom_padding).visibility(premier? View.GONE : View.VISIBLE);
		
		aq.id(R.id.home_imageview).getImageView().setScaleType(ImageView.ScaleType.FIT_CENTER);
		aq.id(R.id.home_imageview).height(imageHeight ,false);

		aq.id(R.id.home_bg).getView().setBackgroundColor(bgcolorRes);
		LayoutParams params = (LayoutParams) aq.id(R.id.home_bg).getView().getLayoutParams();
		params.height = height;
		
		aq.id(R.id.home_bg).getView().setLayoutParams(params);
	}

	public void setSelBg(Boolean isSet) {
		aq.id(R.id.home_select_bg).getView().setVisibility(isSet? View.VISIBLE: View.GONE);
	}

	public void setBg(int bgRes){
		aq.id(R.id.home_bg).getView().setBackgroundResource(bgRes);
	}
	public void setBgColor(int bgRes){
		aq.id(R.id.home_bg).getView().setBackgroundColor(bgRes);
	}
}
