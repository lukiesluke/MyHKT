package com.pccw.myhkt.lib.ui;

import com.pccw.biz.myhkt.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class PopOverView extends View{

	private int viewHeight = 0;
	private int viewWidth = 0;

	private Paint bgPaint = new Paint();

	private Context context;
	public PopOverView(Context context)  {  
		super(context);  
		this.context = context;
		setBackgroundColor(Color.TRANSPARENT);
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		initSetting(context);
	} 

	public PopOverView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		setBackgroundColor(Color.TRANSPARENT);
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		initSetting(context);
	}

	public PopOverView(Context context, AttributeSet attrs,	int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.context = context;
		setBackgroundColor(Color.TRANSPARENT);
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		initSetting(context);
	}	
	
	int roundAngleDp = 7;
	int roundAngle = 25;
	float shadowDeltaDp = (float) 4.3;
	int shadowDelta = 15;
	int strokeWidth = 2;
	int shadowWidthDp = 14;
	int shadowWidth = 50;
	
	//Dp
	int trangleHDp = 13;
	int trangleWDp = 20;
	int trangleH = 40;
	int trangleW = 70;

	Boolean isTop = true;
	int arrowXPos = 0;

	RectF r;
	
	Path triangle;
	Paint greyPaint;
	Paint whitePaint;
	Paint tranglePaint;
	Paint tranglePaint1;
	int startX = 0;
	int startY = 0;

	public void initSetting (Context context) {
		float density = context.getResources().getDisplayMetrics().density;		
		trangleH = (int) (density * trangleHDp);
		trangleW = (int) (density * trangleWDp);
		roundAngle = (int) (density * roundAngleDp);
		shadowDelta = (int) (density * shadowDeltaDp);
		shadowWidth = (int) (density * shadowWidthDp);
	}
	
	
	private void setupPaints() {
		bgPaint = new Paint();
		bgPaint.setColor(Color.WHITE);	
		//		bgPaint.setStyle(Paint.Style.STROKE);
		//		bgPaint.setStrokeWidth(strokeWidth);
		bgPaint.setAntiAlias(true);
		bgPaint.setShadowLayer(shadowDelta, -shadowDelta, shadowDelta, Color.parseColor("#88000000"));		

		whitePaint = new Paint();
		whitePaint.setColor(Color.WHITE);	
		whitePaint.setAntiAlias(true);

		greyPaint = new Paint();
		greyPaint.setColor(Color.WHITE);	
		//		greyPaint.setStyle(Paint.Style.STROKE);
		//		greyPaint.setStrokeWidth(strokeWidth);
		greyPaint.setAntiAlias(true);
		greyPaint.setShadowLayer(shadowDelta, 0, 0, Color.parseColor("#88000000"));

		tranglePaint = new Paint();
		tranglePaint.setColor(Color.WHITE);	
		tranglePaint.setAntiAlias(true);
		tranglePaint.setShadowLayer(shadowDelta, 0, 0, Color.parseColor("#88000000"));		
		
		tranglePaint1 = new Paint();
		tranglePaint1.setColor(Color.WHITE);	
		tranglePaint1.setAntiAlias(true);
		tranglePaint1.setShadowLayer(shadowDelta, -shadowDelta/2, 0, Color.parseColor("#88000000"));		


		startX = shadowWidth /2;
		startY = shadowWidth /2;
		
		if (isTop) {
			r = new RectF(startX,startY+trangleH,viewWidth-shadowWidth/2 ,viewHeight-shadowWidth/2);

			trangleW = trangleH + shadowWidth/2;

			triangle = new Path();
			triangle.moveTo(arrowXPos,0);
			triangle.lineTo(arrowXPos-trangleW/2,trangleH+shadowWidth/2);
			triangle.lineTo(arrowXPos+trangleW/2,trangleH+shadowWidth/2);
			triangle.close();
		} else {
			r = new RectF(startX,startY,viewWidth-shadowWidth/2 ,viewHeight-shadowWidth/2 -trangleH);

			trangleW = trangleH + shadowWidth/2;

			triangle = new Path();
			triangle.moveTo(arrowXPos,viewHeight);
			triangle.lineTo(arrowXPos-trangleW/2,viewHeight - trangleH -shadowWidth/2);
			triangle.lineTo(arrowXPos+trangleW/2,viewHeight - trangleH -shadowWidth/2);
			triangle.close();
		}
	}
	
	public void setArrowPos(int xpos) {
		arrowXPos = xpos;
		setupPaints();
		invalidate();		
	}
	
	public void setIsTop (Boolean isTop) {
		this.isTop = isTop;
		setupPaints();
		invalidate();
	}
	
	

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
		int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
		this.setMeasuredDimension(parentWidth, parentHeight);
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	@Override
	protected void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
		super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
		viewWidth = newWidth;
		viewHeight = newHeight;

		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		//        setupBounds();
		setupPaints();
		invalidate();
	}	 


	public int getShadowWidth(){
		return shadowWidth;
	}
	
	public int getRoundAngle(){
		return roundAngle;
	}
	
	public int getArrowHeight(){
		return trangleH;
	}
	
	public int getArrowWidth(){
		return trangleW;
	}
	
	public int getOuterBndHeight(int innerBndHeight){
		int h = innerBndHeight + shadowWidth + roundAngle * 2;
		return h;
	}
	
	public void setInnerHeight(int innerHeight){
		int h = innerHeight + shadowWidth + roundAngle * 2;
		onSizeChanged(viewWidth, h, viewWidth, viewHeight);
	}
	

	
	

	@Override  
	protected void onDraw(Canvas canvas)  { 		
		super.onDraw(canvas);

		canvas.drawRoundRect(r, roundAngle, roundAngle, bgPaint);
		canvas.drawRoundRect(r, roundAngle, roundAngle, greyPaint);	
		canvas.drawPath(triangle, tranglePaint);
		canvas.drawPath(triangle, tranglePaint1);
		canvas.drawRoundRect(r, roundAngle, roundAngle, whitePaint);
	}  


} 	

