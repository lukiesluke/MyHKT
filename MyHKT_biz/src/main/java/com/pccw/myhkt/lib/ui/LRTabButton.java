package com.pccw.myhkt.lib.ui;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.pccw.biz.myhkt.R;

public class LRTabButton extends LinearLayout {

	private AQuery aq;
	private Boolean isLeftClick = true;
	private Context context;

	private final float DEFAULT_TXT_SIZE = getResources().getDimension(R.dimen.bodytextsize);
	private final int DEFAULT_TXT_COLOR = R.color.hkt_txtcolor_grey;
	private final int DEFAULT_TXT_COLOR_DISABLE = R.color.hkt_txtcolor_grey_disable;

	public final static int leftBTnid = R.id.lrtabbutton_left;
	public final static int rightBTnid = R.id.lrtabbutton_right;
	public LRTabButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public LRTabButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public LRTabButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
	}

	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HKTButton, 0, 0);
		try {
			//			int textSize =	a.getInt(R.styleable.BlueButton_android_textSize, (int)getResources().getDimension(R.dimen.bodytextsize));
			//			String text = a.getString(R.styleable.BlueButton_android_text);
			//			int height = a.getDimensionPixelOffset(R.styleable.BlueButton_android_layout_height, (int)context.getResources().getDimension(R.dimen.buttonblue_height));
			//			int width = a.getDimensionPixelOffset(R.styleable.BlueButton_android_layout_width, (int)context.getResources().getDimension(R.dimen.buttonblue_width));
			//			initViews(context, text, textSize, height, width);
		} finally {
			a.recycle();
		}
	}


	//	public void initViews(final Context context, String leftTxt , String rightTxt) {	
	//		initViews(context, leftTxt, rightTxt, getResources().getDimension(R.dimen.bodytextsize));
	//	}
	//
	/**
	 * 
	 *  No icon one
	 */
	public void initViews(final Context context, String leftTxt , String rightTxt) {	
		initViews(context, leftTxt, rightTxt, -1 ,-1, DEFAULT_TXT_SIZE, (int)context.getResources().getDimension(R.dimen.lr_button_height), LayoutParams.MATCH_PARENT);
	}

	public void initViews(final Context context, String leftTxt , String rightTxt , int leftRes, int rightRes) {	
		initViews(context, leftTxt, rightTxt, leftRes , rightRes, DEFAULT_TXT_SIZE, (int)context.getResources().getDimension(R.dimen.lr_button_height), LayoutParams.MATCH_PARENT);
	}

	public void initViews(final Context context, String leftTxt , String rightTxt, int leftRes, int rightRes, float textSize, int height, int width) {	
		aq = new AQuery(this);
		this.context = context;
		LayoutInflater.from(context).inflate(R.layout.lrtabbutton, this);
		aq.id(R.id.lrtabbutton_left_title).text(leftTxt).height(height, false).textColorId(DEFAULT_TXT_COLOR);	
		aq.id(R.id.lrtabbutton_left_title).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);		
		aq.id(R.id.lrtabbutton_right_title).textSize(textSize).text(rightTxt).height(height, false).textColorId(DEFAULT_TXT_COLOR);	
		aq.id(R.id.lrtabbutton_right_title).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		if (leftRes<0) {
			aq.id(R.id.lrtabbutton_left_icon).visibility(View.GONE);
		} else {
			aq.id(R.id.lrtabbutton_left_icon).image(leftRes);
		}

		if (rightRes<0) {
			aq.id(R.id.lrtabbutton_right_icon).visibility(View.GONE);
		} else {
			aq.id(R.id.lrtabbutton_right_icon).image(rightRes);
		}
		setBtns();
		aq.id(R.id.lrtabbutton_left).clicked(onLeftClick);
		aq.id(R.id.lrtabbutton_right).clicked(onRightClick);
	}

	private OnClickListener onLeftClick = new OnClickListener(){
		@Override
		public void onClick(View v) {
			isLeftClick = true;
			setBtns();
		}		
	};

	private OnClickListener onRightClick = new OnClickListener(){
		@Override
		public void onClick(View v) {			
			isLeftClick = false;
			setBtns();
		}		
	};

	public void clicked(Activity act , String click){
		AAQuery aaq = new AAQuery(this);
		aaq.id(R.id.lrtabbutton_left).clicked(act, click);
		aaq.id(R.id.lrtabbutton_right).clicked(act, click);
	}	

	public void setOnLeftClickLisener(final OnClickListener onLeftClickLisener) {
		if (onLeftClickLisener == null) {
			aq.id(R.id.lrtabbutton_left).clicked(onLeftClick);
		} else {
			aq.id(R.id.lrtabbutton_left).clicked(new OnClickListener(){

				@Override
				public void onClick(View v) {
					if (!isLeftClick) {
						onLeftClickLisener.onClick(v);
						onLeftClick.onClick(v);
					}
				}				
			});	
		}
	}

	public void setOnRightClickLisener(final OnClickListener onRightClickLisener) {
		if (onRightClickLisener == null) {
			aq.id(R.id.lrtabbutton_right).clicked(onRightClick);
		} else {
			aq.id(R.id.lrtabbutton_right).clicked(new OnClickListener(){

				@Override
				public void onClick(View v) {
					if (isLeftClick) {
						onRightClickLisener.onClick(v);
						onRightClick.onClick(v);
					}
				}				
			});	
		}
	}

	public Boolean isLeftClick() {
		return isLeftClick;
	}

	public void setBtns(Boolean isLClick) {
		isLeftClick = isLClick;
		setBtns();
	}
	
	public void selectLeft() {
		aq = new AAQuery(this);
		aq.id(R.id.lrtabbutton_left).getView().performClick();
	}
	
	public void selectRight() {
		aq = new AAQuery(this);
		aq.id(R.id.lrtabbutton_right).getView().performClick();
	}
	
	public boolean isLeftClicked(){
		return isLeftClick;
	}
	
	private void setBtns() {
		aq.id(R.id.lrtabbutton_left).background(isLeftClick? R.drawable.hkt_tabbtnleft_bg_sel :R.drawable.hkt_tabbtnleft_bg_not_sel);
		aq.id(R.id.lrtabbutton_left_title).textColorId(isLeftClick ? DEFAULT_TXT_COLOR : DEFAULT_TXT_COLOR_DISABLE);
		aq.id(R.id.lrtabbutton_right).background(!isLeftClick? R.drawable.hkt_tabbtnright_bg_sel :R.drawable.hkt_tabbtnright_bg_not_sel);
		aq.id(R.id.lrtabbutton_right_title).textColorId(!isLeftClick ? DEFAULT_TXT_COLOR : DEFAULT_TXT_COLOR_DISABLE);
		aq.id(R.id.lrtabbutton_left_icon).enabled(isLeftClick);
		aq.id(R.id.lrtabbutton_right_icon).enabled(!isLeftClick);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			aq.id(R.id.lrtabbutton_left_icon).getImageView().setAlpha(!isLeftClick ?(float) 0.4 : (float)1);
		} 
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			aq.id(R.id.lrtabbutton_right_icon).getImageView().setAlpha(isLeftClick ?(float) 0.4 : (float)1);
		} 
	}

	public void clearData(){
		aq = null;
		
	}
}
