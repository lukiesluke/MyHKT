package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.androidquery.AQuery;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;

public class HKTButton extends Button {
	//Normall user
	//Type name may change later
	/**Blue stroke White bg*/
	public final static int TYPE_BLUE = 0;
	/**White stroke Blue bg*/
	public final static int TYPE_WHITE_BLUE = 1;

	//Normal user at My mob 
	/**Orange stroke White bg*/
	public final static int TYPE_ORANGE = 2;
	/**White stroke Orange bg*/
	public final static int TYPE_WHITE_ORANGE = 3;
	/**Black bg*/
	public final static int TYPE_BLACK = 4;
	/**Black stroke Yellow bg*/
	public final static int TYPE_YELLOW_BLACK = 5;
	
	
	
	private int type = TYPE_BLUE;

	private Context ctx;
	private int width;
	private String text;
	
	public HKTButton(Context context) {
		super(context);
		initViews(context, "");		
	}

	public HKTButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public HKTButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
	}

	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HKTButton, 0, 0);
		try {
			int textSize =	a.getInt(R.styleable.HKTButton_android_textSize, (int)getResources().getDimension(R.dimen.bodytextsize));
			String text = a.getString(R.styleable.HKTButton_android_text);
			int height = (int) context.getResources().getDimension(R.dimen.buttonblue_height);
			int width =  (int)context.getResources().getDimension(R.dimen.buttonblue_width);
			initViews(context, text, textSize,height,width);
		} finally {
			a.recycle();
		}
	}


	public void initViews(final Context context, String text) {	
		initViews(context,  TYPE_BLUE, text);
	}

	public void initViews(final Context context, int btnType , String text) {
		type = btnType;
		initViews(context, text, getResources().getDimension(R.dimen.bodytextsize),(int)getResources().getDimension(R.dimen.buttonblue_height), (int)getResources().getDimension(R.dimen.buttonblue_width));
	}

	public void initViews(final Context context, int btnType , String text, int width) {	
		type = btnType;
		initViews(context, text, getResources().getDimension(R.dimen.bodytextsize), (int)getResources().getDimension(R.dimen.buttonblue_height) ,width) ;
	}

	public void initViews(final Context context, String text, int width) {	
		initViews(context, text, getResources().getDimension(R.dimen.bodytextsize), (int)getResources().getDimension(R.dimen.buttonblue_height) ,width) ;
	}

	public void initViews(final Context context, int btnType, String text, float textSize, int height, int width) {	
		type = btnType;
		initViews(context, text, textSize, height, width) ;

	}

	public void initViews(final Context context, String text, float textSize, int height, int width) {	
		ctx = context;
		if (this.getLayoutParams()!=null) {
			LayoutParams params = getLayoutParams();
			params.height = height;
			params.width = width;
			setLayoutParams(params);
		} else {
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,height);
			setLayoutParams(params);
		}
		this.width = width;
		this.text = text;
		setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		setGravity(Gravity.CENTER);
		setPadding(0, 0, 0, 0);
		setSingleLine(true);
		setText(text);

		setType(type);		
	}

//	public void setDrawable(Drawable draw) {
//
//		setCompoundDrawables(draw, null, null, null);
//	}
//	
	public void setDrawable(Drawable draw) {
		int padding_twocol = (int) ctx.getResources().getDimension(R.dimen.padding_twocol);
		int extralinespace = (int) ctx.getResources().getDimension(R.dimen.extralinespace);
		if ("".equals(text)) {
			//Make the drawable to center , no support full wdith
			if (width == ViewGroup.LayoutParams.WRAP_CONTENT) {
				setPadding(0, 0, 0, 0);	
			} else {
				setPadding((width - draw.getBounds().width())/2, 0, 0, 0);	
			}
		
			setGravity(Gravity.LEFT|Gravity.CENTER);
			setCompoundDrawablePadding(padding_twocol);
			setCompoundDrawables(draw, null, null, null);
		} else {
			final int drawWidth = draw.getBounds().width();
			final int padding_twocol1 =padding_twocol;
			setGravity(Gravity.LEFT|Gravity.CENTER);
			setCompoundDrawablePadding(padding_twocol);
			//To make icon and text center in the btn that the width is wrap_content / match parent
			post(new Runnable() {
		        @Override
		        public void run() {
		        	int btnWidth = getWidth();
		        	Rect bounds = new Rect();
		        	Paint textPaint = getPaint();
		        	textPaint.getTextBounds(text,0,text.length(),bounds);
		        	int textWidth = bounds.width();		        
		        	int leftPadding = (btnWidth - textWidth - drawWidth - padding_twocol1 ) /2;
		        	leftPadding = Math.max( 0 , leftPadding);
		        	setPadding(leftPadding, 0 , 0, 0);	
		        	
		        }
		    });
			setCompoundDrawables(draw, null, null, null);	
		}
	}
	
	
	public void setType(int btnType){
		type = btnType;
		//TODO theme
		switch (type) {
		case TYPE_BLUE:
			setBackgroundResource(R.drawable.hkt_btn_bg_blue_selector);
			setTextColor(ctx.getResources().getColorStateList(R.color.hkt_btn_blue_txtcolor_selector));
			break;
		case TYPE_WHITE_BLUE:
			setBackgroundResource(R.drawable.hkt_btn_bg_white_blue_selector);
			setTextColor(ctx.getResources().getColorStateList(R.color.hkt_btn_white_blue_txtcolor_selector));
			break;
		case TYPE_ORANGE:
			setBackgroundResource(R.drawable.hkt_btn_bg_orange_selector);
			setTextColor(ctx.getResources().getColorStateList(R.color.hkt_btn_orange_txtcolor_selector));
			break;
		case TYPE_WHITE_ORANGE:
			setBackgroundResource(R.drawable.hkt_btn_bg_white_orange_selector);
			setTextColor(ctx.getResources().getColorStateList(R.color.hkt_btn_white_orange_txtcolor_selector));
			break;	
		case TYPE_BLACK:
			setBackgroundResource(R.drawable.hkt_btn_bg_black_selector);
			setTextColor(ctx.getResources().getColorStateList(R.color.hkt_btn_black_txtcolor_selector));
			break;
		case TYPE_YELLOW_BLACK:
			setBackgroundResource(R.drawable.hkt_btn_bg_yellow_black_selector);
			setTextColor(ctx.getResources().getColorStateList(R.color.hkt_btn_yellow_black_txtcolor_selector));
			break;
		default:
			setBackgroundResource(R.drawable.hkt_btn_bg_blue_selector);
			setTextColor(ctx.getResources().getColorStateList(R.color.hkt_btn_blue_txtcolor_selector));
			break;
		}

	}

	public void setLeftMargin(int leftMargin) {


	}
	
	public void setEnableClick(Boolean enable){
		Log.i("HKTButton", "" + enable);
		if (enable) {
			this.setEnabled(enable);
//			getBackground().setAlpha(1*256-1);
			this.setAlpha(1.0f);
//			getBackground().setColorFilter(null);
		} else {
			this.setEnabled(enable);
			int		m_color				= Color.argb(200, 255, 255, 255);
//			getBackground().setColorFilter(m_color, android.graphics.PorterDuff.Mode.SRC_OVER);
//			getBackground().setAlpha((int)(0.2*256-1));
			this.setAlpha(0.2f);
		}
	}
}
