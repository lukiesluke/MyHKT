package com.pccw.myhkt.mapviewballoons;

/************************************************************************
File       : CustomizeFrameLayout.java
Desc       : Customized Frame Layout for UI performance optimization
Name       : CustomizeFrameLayout
Created by : Ryan Wong
Date       : 15/03/2013

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
15/03/2013 Ryan Wong			- First draft
*************************************************************************/

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class CustomizeFrameLayout extends FrameLayout {

	public CustomizeFrameLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public CustomizeFrameLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomizeFrameLayout(Context context) {
		super(context);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		int action = ev.getAction();
		switch (action) {
			case MotionEvent.ACTION_DOWN:
				// Disallow ScrollView to intercept touch events.
				this.getParent().requestDisallowInterceptTouchEvent(true);
				break;

			case MotionEvent.ACTION_UP:
				// Allow ScrollView to intercept touch events.
				this.getParent().requestDisallowInterceptTouchEvent(false);
				break;
		}

		// Handle MapView's touch events.
		super.onTouchEvent(ev);
		return true;
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
			case MotionEvent.ACTION_DOWN:
				// Disallow ScrollView to intercept touch events.
				this.getParent().requestDisallowInterceptTouchEvent(true);
				break;

			case MotionEvent.ACTION_UP:
				// Allow ScrollView to intercept touch events.
				this.getParent().requestDisallowInterceptTouchEvent(false);
				break;
		}

		return super.dispatchTouchEvent(ev);
	}
}
