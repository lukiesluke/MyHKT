package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.adapter.ShopTypeListViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;

public class ShopTypeFragment extends BaseShopFragment{
	private ShopTypeFragment me;
	private AAQuery aq;
	
	private ShopRec[] shopRecAry;
	private int SHOP_TYPES = 9;
	
	// List View
	private ShopTypeListViewAdapter ShopTypeListViewAdapter;
	

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;

		TAG = "ShopTypeFragment";

		
		View fragmentLayout = inflater.inflate(R.layout.fragment_shoptype, container, false);
		return fragmentLayout;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		//Screen Tracker
		
		//derek insert fake data for testing
//		String resultStr ="{\"area\":\"HK\",\"longitude\":0.0,\"latitude\":0.0,\"shopRecAry\":[{\"rid\":10,\"shop_cd\":\"10\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Aberdeen\",\"district_zh\":\"香港仔\",\"addr_en\":\"Shop A1 \u0026 A3:, G/F, 2 Tung Sing Road, Aberdeen, HK\",\"addr_zh\":\"香港仔東勝道2號地下A1及A3號店\",\"ct_num\":\"2552 1601\",\"bz_hr_en\":\"10:00-21:00 (Mon-Sat) \n10:00-21:00 (Sun/PH)\",\"bz_hr_zh\":\"10:00-21:00（星期一至六） \n10:00-21:00（星期日及公眾假期）\",\"longitude\":114.155349,\"latitude\":22.247744,\"desn_en\":\"Tung Sing Road Shop\",\"desn_zh\":\"東勝道專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":7,\"shop_cd\":\"7\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Causeway Bay\",\"district_zh\":\"銅鑼灣\",\"addr_en\":\"Shop G3, G/F McDonald\u0027s Building, 46-54 Yee Woo Street, Causeway Bay, HK\",\"addr_zh\":\"香港銅鑼灣怡和街46-54號麥當勞大廈地下G3商店\",\"ct_num\":\"2881 8898\",\"bz_hr_en\":\"10:00-22:00 (Mon-Sat) \n10:00-22:00 (Sun/PH)\",\"bz_hr_zh\":\"10:00-22:00（星期一至六） \n10:00-22:00（星期日及公眾假期）\",\"longitude\":114.185948,\"latitude\":22.279596,\"desn_en\":\"McDonald\u0027s Building Shop\",\"desn_zh\":\"麥當勞大廈專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":8,\"shop_cd\":\"8\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Causeway Bay\",\"district_zh\":\"銅鑼灣\",\"addr_en\":\"Shop F, G/F, Kwong On Bldg., 8-14 Yee Wo Street, Causeway Bay, HK\",\"addr_zh\":\"香港銅鑼灣怡和街8-14號廣安大廈地下F店\",\"ct_num\":\"2576 4354\",\"bz_hr_en\":\"11:00-22:00 (Mon-Sat) \n11:00-22:00 (Sun/PH)\",\"bz_hr_zh\":\"11:00-22:00（星期一至六） \n11:00-22:00（星期日及公眾假期）\",\"longitude\":114.185014,\"latitude\":22.279839,\"desn_en\":\"Kwong On Building Shop\",\"desn_zh\":\"廣安大廈專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":54,\"shop_cd\":\"54\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Causeway Bay\",\"district_zh\":\"銅鑼灣\",\"addr_en\":\"10/F East Exchange Tower, 38 Leighton Road, Causeway Bay, Hong Kong \r\n(MTR Causeway Bay Station Exit A, opposite to Lee Theatre Plaza)\",\"addr_zh\":\"香港銅鑼灣禮頓道38號東區電訊大厦10樓 \r\n(港鐵銅鑼灣站A出口, 利舞臺廣場對面)\",\"ct_num\":\"1000 / 10088\",\"bz_hr_en\":\"(for both personal and business customers) \r\n08:30 – 20:00 (Mon-Sat) \r\n(for personal customers only) \r\n12:00 – 19:00 (Sun/PH)\",\"bz_hr_zh\":\"(個人及商業客戶) \r\n星期一至六: 08:30 – 20:00 \r\n(個人客戶) \r\n星期日及公眾假期: 12:00 – 19:00\",\"longitude\":114.183778,\"latitude\":22.277712,\"desn_en\":\"East Exchange Tower Customer Center\",\"desn_zh\":\"東區電訊大厦客戶服務中心\",\"name_en\":\"East Exchange Tower Customer Center\",\"name_zh\":\"東區電訊大厦客戶服務中心\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130827164834\",\"lastupd_psn\":\"T1262468\",\"rev\":2},{\"rid\":2,\"shop_cd\":\"2\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Central\",\"district_zh\":\"中環\",\"addr_en\":\"G/F, Far East Consortium Building, 113 Des Voeux Road, Central, HK\",\"addr_zh\":\"中環德輔道中113號遠東發展大廈地下\",\"ct_num\":\"2543 0603\",\"bz_hr_en\":\"10:00-20:30 (Mon-Sat) \n11:00-20:00 (Sun/PH)\",\"bz_hr_zh\":\"10:00-20:30（星期一至六） \n11:00-20:00（星期日及公眾假期）\",\"longitude\":114.155456,\"latitude\":22.285054,\"desn_en\":\"Far East Consortium Building Shop\",\"desn_zh\":\"遠東發展大廈專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":1,\"shop_cd\":\"1\",\"shop_ty\":\"1\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Central\",\"district_zh\":\"中環\",\"addr_en\":\"G/F Kailey Tower, No.23 Wellington Street, Central, HK\",\"addr_zh\":\"中環威靈頓街23號 騏利大廈地下\",\"ct_num\":\"2888 0888\",\"bz_hr_en\":\"09:00-21:00 (Mon-Sat) \n10:30-21:00 (Sun/PH)\",\"bz_hr_zh\":\"09:00-21:00（星期一至六） \n10:30-21:00（星期日及公眾假期）\",\"longitude\":114.155671,\"latitude\":22.281825,\"desn_en\":\"Kailey Tower Shop\",\"desn_zh\":\"騏利大廈店\",\"name_en\":\"PCCW-HKT Central Signature Store\",\"name_zh\":\"PCCW-HKT 中環 Signature Store\",\"create_ts\":\"20130312000000\",\"create_psn\":\"km\",\"lastupd_ts\":\"20140923153327\",\"lastupd_psn\":\"TIMS9I04\",\"rev\":7},{\"rid\":9,\"shop_cd\":\"9\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Chai Wan\",\"district_zh\":\"柴灣\",\"addr_en\":\"Shop 345, Level 3, New Jade Shopping Arcade, Chai Wan, HK\",\"addr_zh\":\"香港柴灣新翠商場3樓345號商店\",\"ct_num\":\"2888 1231\",\"bz_hr_en\":\"11:00-21:00 (Mon-Sat) \n11:00-21:00 (Sun/PH)\",\"bz_hr_zh\":\"11:00-21:00（星期一至六） \n11:00-21:00（星期日及公眾假期）\",\"longitude\":114.236985,\"latitude\":22.26416,\"desn_en\":\"New Jade Shopping Arcade Shop\",\"desn_zh\":\"新翠商場專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":5,\"shop_cd\":\"5\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"North Point\",\"district_zh\":\"北角\",\"addr_en\":\"Shop No. 3, G/F, King\u0027s Centre, 193-209 King\u0027s Road, HK\",\"addr_zh\":\"香港英皇道193-209號英皇中心地下3號舖\",\"ct_num\":\"2883 2885\",\"bz_hr_en\":\"10:30-21:30 (Mon-Sat) \n10:30-21:30 (Sun/PH)\",\"bz_hr_zh\":\"10:30-21:30（星期一至六） \n10:30-21:30（星期日及公眾假期)\",\"longitude\":114.194147,\"latitude\":22.289414,\"desn_en\":\"King\u0027s Centre Shop\",\"desn_zh\":\"英皇中心專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":11,\"shop_cd\":\"11\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Sai Wan Ho\",\"district_zh\":\"西灣河\",\"addr_en\":\"Flat J-1, G/F, Lai Wan Building, No.53 Shau Kei Wan Road, Sai Wan Ho, HK\",\"addr_zh\":\"香港西灣河筲箕灣道53 號麗灣大廈地下J-1號舖\",\"ct_num\":\"2904 8116\",\"bz_hr_en\":\"11:00-21:30 (Mon-Sat) \n11:00-21:30 (Sun/PH)\",\"bz_hr_zh\":\"11:00-21:30（星期一至六） \n11:00-21:30（星期日及公眾假期）\",\"longitude\":114.221178,\"latitude\":22.283269,\"desn_en\":\"Lai Wan Building Shop\",\"desn_zh\":\"麗灣大廈專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":55,\"shop_cd\":\"55\",\"shop_ty\":\"C\",\"shop_cat\":\"YNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Sheung Wan\",\"district_zh\":\"上環\",\"addr_en\":\"24/F West Exchange Tower, 322 Des Voeux Road Central, Sheung Wan, Hong Kong \n(MTR Sheung Wan exit A1, opposite to Western Market)\",\"addr_zh\":\"香港上環德輔道中322號西區電話機樓 24樓 \n(港鐵上環站A1出口, 西港城對面)\",\"ct_num\":\"1000\",\"bz_hr_en\":\"(for personal customers only) \n08:30 – 20:00 (Mon-Sat) \n12:00 – 19:00 (Sun/PH)\",\"bz_hr_zh\":\"(個人客戶) \n星 期一至六: 08:30 – 20:00 \n星期日及公眾假期: 12:00 – 19:00\",\"longitude\":114.150186,\"latitude\":22.286788,\"desn_en\":\"West Exchange Tower Personal Customer Center\",\"desn_zh\":\"西區電話機樓個人客戶服務中心\",\"name_en\":\"West Exchange Tower Personal Customer Center\",\"name_zh\":\"西區電話機樓個人客戶服務中心\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20150324150630\",\"lastupd_psn\":\"TIMS9I04\",\"rev\":5},{\"rid\":4,\"shop_cd\":\"4\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Wanchai\",\"district_zh\":\"灣仔\",\"addr_en\":\"Shop B, G/F, 93-99 Wanchai Road, Wanchai, HK\",\"addr_zh\":\"香港灣仔灣仔道93-99號地下B商店\",\"ct_num\":\"2575 6829\",\"bz_hr_en\":\"10:00-21:00 (Mon-Sat) \n11:00-21:00 (Sun/PH)\",\"bz_hr_zh\":\"10:00-21:00（星期一至六） \n11:00-21:00（星期日及公眾假期)\",\"longitude\":114.175111,\"latitude\":22.276687,\"desn_en\":\"Waichai Road Shop\",\"desn_zh\":\"灣仔道專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":6,\"shop_cd\":\"6\",\"shop_ty\":\"S\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Wanchai\",\"district_zh\":\"灣仔\",\"addr_en\":\"Shop No. 101, 1/F, Wanchai Computer Centre, Southorn Centre, 130 Hennessy Road, Wan Chai, HK\",\"addr_zh\":\"香港灣仔軒尼詩道130號修頓中心灣仔電腦城一樓101號商店\",\"ct_num\":\"2893 3375\",\"bz_hr_en\":\"12:00-21:00 (Mon-Sat) \n12:00-21:00 (Sun/PH)\",\"bz_hr_zh\":\"12:00-21:00（星期一至六） \n12:00-21:00（星期日及公眾假期）\",\"longitude\":114.17315,\"latitude\":22.277516,\"desn_en\":\"Wainchai Computer Centre Shop\",\"desn_zh\":\"灣仔電腦城專門店\",\"name_en\":\"PCCW-HKT Shop\",\"name_zh\":\"PCCW-HKT 專門店\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0},{\"rid\":56,\"shop_cd\":\"56\",\"shop_ty\":\"C\",\"shop_cat\":\"NNNNNNNN\",\"area\":\"HK\",\"district_en\":\"Wanchai\",\"district_zh\":\"灣仔\",\"addr_en\":\"Basement, Lockhart Exchange, 3 Hennessy Road, Wan Chai, Hong Kong \n(MTR Wan Chai exit B1, next to Asian House)\",\"addr_zh\":\"香港灣仔軒尼詩道3號駱克機樓地庫 \n(港鐵灣仔站B1出口, 熙信大廈側)\",\"ct_num\":\"1000 / 10088\",\"bz_hr_en\":\"(for both personal and business customers) \n08:30 – 20:00 (Mon-Sat) \n(for personal customers only) \n12:00 – 19:00 (Sun/PH)\",\"bz_hr_zh\":\"(個人及商業客戶) \n星期一至六: 08:30 – 20:00 \n(個人客戶) \n星期日及公眾假期: 12:00 – 19:00\",\"longitude\":114.169355,\"latitude\":22.278052,\"desn_en\":\"Lockhart Exchange Customer Center\",\"desn_zh\":\"駱克機樓客戶服務中心\",\"name_en\":\"Lockhart Exchange Customer Center\",\"name_zh\":\"駱克機樓客戶服務中心\",\"create_ts\":\"20130312000001\",\"create_psn\":\"km\",\"lastupd_ts\":\"20130312000001\",\"lastupd_psn\":\"km\",\"rev\":0}],\"reply\":{\"code\":\"RC_SUCC\"},\"serverTS\":\"20151208142456\"}";
//			Type collectionType = new TypeToken<ShopCra>() {}.getType();
//			Gson gson = new Gson();
//			ShopCra shopCra = gson.fromJson(resultStr, collectionType);
//			shopRecAry = shopCra.getOShopRecAry();
//			callback.setShopRecAry(shopRecAry);
		//derek 
		init();

	}
	
	@Override
	public void onPause(){
		super.onPause();
//		callback.setPause(true);
//		callback.setActiveSubview(R.string.CONST_SELECTEDFRAG_SHOPTYPE);
	}
	
	@Override
	public void onResume(){
		super.onResume();

	}
	
	private void init() {
		aq = new AAQuery(getActivity());
		
		aq.id(R.id.shoptype_remark).textSize(getResources().getInteger(R.integer.textsize_default_int));
		
		String[] shopType = new String[SHOP_TYPES];
		ShopTypeListViewAdapter = new ShopTypeListViewAdapter(getActivity(), shopType);
		aq.id(R.id.shoptype_listview).adapter(ShopTypeListViewAdapter);
		aq.id(R.id.shoptype_listview).itemClicked(onItemClickListener);
//		((ListView) getActivity().findViewById(R.id.ShopType_listview)).setAdapter(ShopTypeListViewAdapter);
//		((ListView) getActivity().findViewById(R.id.ShopType_listview)).setOnItemClickListener(onItemClickListener);
		//set hyperlink for google tnc
	    SpannableString content;
		content = new SpannableString(Utils.getString(getActivity(), R.string.shopregion_googletacs));
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		aq.id(R.id.shopregion_googletacs).getTextView().setText(content);
		aq.id(R.id.shopregion_googletacs).clicked(this, "onClick");

		content = new SpannableString(Utils.getString(getActivity(), R.string.shopregion_googleprivacy));
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		aq.id(R.id.shopregion_googleprivacy).getTextView().setText(content);
		aq.id(R.id.shopregion_googleprivacy).clicked(this, "onClick");
	}
	

	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {
			case R.id.shopregion_googletacs:
				String gptacsurl = Utils.getString(getActivity(), R.string.shopregion_googletacs_url);
				intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(gptacsurl));
				startActivity(intent);
				break;
			case R.id.shopregion_googleprivacy:
				String gpurl = Utils.getString(getActivity(), R.string.shopregion_googleprivacy_url);
				intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(gpurl));
				startActivity(intent);
				break;
		}
	}
	
	// List View on Item Click Listener
	private OnItemClickListener	onItemClickListener	= new OnItemClickListener() {
														public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
															Intent intent;
															Bundle rbundle;
															
															if (position != 0) {
//																intent = new Intent(getApplicationContext(), ShopDistrictActivity.class);
//																rbundle = new Bundle();
//																rbundle.putInt("SHOPTYPE", position);
//																intent.putExtras(rbundle);
//																startActivity(intent);
//																overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
																callback_main.setShopType(position);
																callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_SHOPDISTRICT);
																callback_main.displaySubview();
															} else {
																callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_SHOPMYLOC);
																callback_main.displaySubview();
															}
														}
													};
													
													
	
	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

}
