package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.adapter.MyApptListViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;

public class MyApptListFragment extends BaseMyApptFragment {
	private MyApptListFragment me;
	private View myView;
	private AAQuery aq;
	private SrvReq										selectSrvReq 		= null;
	private ApptCra									apptCra;
	
	protected String moduleId;
	
	// List View
	private MyApptListViewAdapter myApptListViewAdapter;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_myapptlist, container, false);
		myView = fragmentLayout;
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
		
		// Screen Tracker
	}
	
	@Override
	public void onPause(){
		super.onPause();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		moduleId = getResources().getString(R.string.MODULE_MY_APPT);
	}

	protected void initUI() {
		aq = new AAQuery(myView);
		int extralinespace = (int) getActivity().getResources().getDimension(R.dimen.extralinespace);
		int buttonPadding =  (int) getResources().getDimension(R.dimen.reg_logo_padding);
		// Update NavBar Style for Premier
		//TODO premier switch
//		Utils.changeToThemeNavBar(me, viewholder.appointmentlist_navbar, viewholder.appointmentlist_navbar_title, viewholder.appointmentlist_navbar_back);
		// Update Button Style for Premier
//		Utils.changeToThemeButton(me, R.string.CONST_BTN_LIVECHAT,
//				viewholder.appointmentlist_button_livechat);
//		Utils.changeToThemeButton(me, R.string.CONST_BTN_CALL, 
//				viewholder.appointmentlist_button_call1000);
//		viewholder.appointmentlist_label_call1000.setText(Utils.getString(me, R.string.myhkt_appointment_call1000)); //TODO should i keep this label? to be confirmed
		
		
		//UI
		aq.id(R.id.myapptlist_header).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.id(R.id.myapptlist_empty).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.marginpx(R.id.myapptlist_listview, buttonPadding, 0, buttonPadding, 0);
		aq.line(R.id.myapptlist_listview_line);
		aq.marginpx(R.id.myapptlist_listview_line, buttonPadding, 0, buttonPadding, 0);
		
		aq.id(R.id.question_mark).clicked(this, "onClick");
		aq.marginpx(R.id.question_mark, buttonPadding, 0, buttonPadding, 0);
		
		// Clear all data before calling getApptList API
		doneGetApptList(new ApptCra());
		selectSrvReq = null;
		// Get SubscriptionRec from arraylist
		getApptList();
	}

	protected final void refreshData() {
		super.refreshData();
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.question_mark:
				DialogHelper.createSimpleDialog(me.getActivity(), getResString(R.string.MYHKT_APPT_REMARK) , getString(R.string.btn_ok));
				break;
		}
	}
	
	private final void doneGetApptList(ApptCra rApptCra) {
		aq = new AAQuery(myView);
		// Get ApptAry from Cra
		GnrlAppt[] myApptAry = rApptCra.getOGnrlApptAry();
		SrvReq[] mySRApptAry = rApptCra.getOSrvReqAry();
		
		// List View
		myApptListViewAdapter = new MyApptListViewAdapter(this, myApptAry, mySRApptAry);
		aq.id(R.id.myapptlist_listview).getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		aq.id(R.id.myapptlist_listview).getListView().setAdapter(myApptListViewAdapter);
		aq.id(R.id.myapptlist_listview).getListView().setEmptyView(aq.id(R.id.myapptlist_empty).getTextView());
		aq.id(R.id.myapptlist_listview).getListView().setOnItemClickListener(onItemClickListener);
	}
	
	private final void getApptList() {		
		ApptCra apptCra = new ApptCra();
		apptCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		apptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
		apptCra.setISubnRecAry(ClnEnv.getQualSvee().getSubnRecAry());
		
		APIsManager.doGetAppointment(this, apptCra);
}
	
	private final void doGetAvailableTimeSlot(SrvReq srvReq) {
		ApptCra apptCra = new ApptCra();
		apptCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		apptCra.setICustRec(ClnEnv.getQualSvee().getCustRec());
//		apptCra.setISubnRecAry(ClnEnv.getQualSvee().getSubnRecAry());
		apptCra.setISRApptInfo(srvReq.getApptInfo().copyMe());
		apptCra.getISRApptInfo().setApptTS(srvReq.getApptTS().copyMe());
		
		APIsManager.doGetAvailableTimeSlot(this, apptCra);
	}
	
	private OnItemClickListener	onItemClickListener	= new OnItemClickListener() {
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
			if (myApptListViewAdapter.isSRItem(position)) {
				me.selectSrvReq = (SrvReq) myApptListViewAdapter.getItem(position);
				if ("Y".equalsIgnoreCase(me.selectSrvReq.getAllowUpdInd())) {
					//TODO getavailabletimeslot API call
					me.doGetAvailableTimeSlot(me.selectSrvReq);
				} else {
//					Bundle bundle = new Bundle();
//					bundle.putSerializable("SRVREQ", me.selectSrvReq);
					callback_main.setSrvReq(me.selectSrvReq);
					
					//TODO go to MyApptUpdateFragment
					callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE);
					callback_main.displaySubview();
//					Intent intent = new Intent(getActivity().getApplicationContext(), AppointmentUpdateActivity.class);
//					intent.putExtras(bundle);
//		        	startActivity(intent);
//		        	getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
				}
			} else {
				GnrlAppt gnrlAppt = (GnrlAppt) myApptListViewAdapter.getItem(position);
				if (gnrlAppt.getShowDtls()) {
					String drgOrdNumAry ="";
					
					for (int i = 0 ; i < gnrlAppt.getDrgOrdNumAry().length ; i++) {
						drgOrdNumAry = drgOrdNumAry + gnrlAppt.getDrgOrdNumAry()[i] + "\n";
					}
					
					//ApptDetailDialog
					
					String message = String.format("%s\n\n%s\n%s\n%s\n%s", getString(R.string.ATIMF_ORD_NUM), drgOrdNumAry, getString(R.string.ATIMF_HTLN), getString(R.string.ATIMF_HTLN_NUM), getString(R.string.ATIMF_HTLN_SRVHR));
					DialogHelper.createSimpleDialog(getActivity(), getString(R.string.ATIMF_HDR), message, getString(R.string.btn_ok), new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							alertDialog = null;
						}
					});
				}
			}
		}
	};

	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.APPT.equals(response.getActionTy())) {
			apptCra = new ApptCra();
			apptCra = (ApptCra) response.getCra();
			
			if (apptCra.getOGnrlApptAry() != null) {
				ClnEnv.setPref(getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), false);
				// Determine whether show the alert Icon in MainMenu
				for (int i = 0; i < apptCra.getOGnrlApptAry().length; i++) {
					GnrlAppt gnrlAppt = apptCra.getOGnrlApptAry()[i];
					if (Utils.CompareDateAdd3(getActivity(), gnrlAppt.getApptStDT(), apptCra.getServerTS(), getString(R.string.input_datetime_format))){
						ClnEnv.setPref(getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), true);
						break;
					}
				}
			}
			
			if (apptCra.getOSrvReqAry() != null) {
				ClnEnv.setPref(getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), false);
				// Determine whether show the alert Icon in MainMenu
				for (int i = 0; i < apptCra.getOSrvReqAry().length; i++) {
					SrvReq srvReq = apptCra.getOSrvReqAry()[i];
					if (Utils.CompareDateAdd3(getActivity(), srvReq.getApptTS().getApptDate(), apptCra.getServerTS(), getString(R.string.input_date_format))){
						ClnEnv.setPref(getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), true);
						break;
					}
				}
			}
			
			doneGetApptList(apptCra);
		} else if (APIsManager.SR_AVA_TS.equals(response.getActionTy())) {
			apptCra = new ApptCra();
			apptCra = (ApptCra) response.getCra();
			
			callback_main.setSrvReq(me.selectSrvReq);
			callback_main.setApptInfo(apptCra.getOSRApptInfo()); //TODO it should be callback_main.setSRApptInfo(apptCra.getOSRApptInfo());
			
			//go to update fragment
			callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE);
			callback_main.displaySubview();
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}		
	}

}
