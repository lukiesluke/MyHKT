package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.BiifCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.PopOverInputView;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.wheat.shared.tool.Reply;

public class BillInfoLTSChild1Fragment extends BaseServiceFragment {

	private BillInfoLTSChild1Fragment me;
	private View myView;
	private AAQuery aq;

	private BiifCra biifCra;

	private QuickAction langQuickAction;
	private QuickAction mediaQuickAction;


	private PopOverInputView langPopover;
	private PopOverInputView mediaPopover;

	private Boolean isEBill = false;
	private int screenWidth;

	EditText billinfo_opt2_etxt_addr1,billinfo_opt2_etxt_addr2,billinfo_opt2_etxt_addr3,
			billinfo_opt2_etxt_addr4,billinfo_opt2_etxt_addr5,billinfo_opt2_etxt_addr6;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			callback_main = (OnServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}

		try {
			callback_livechat = (OnLiveChatListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnLiveChatListener");
		}
		if (callback_main == null) { if (debug) Log.i(TAG, "1callback null"); }

	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_billinfolts_child1, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}	

	protected void initUI() {
		// Screen Tracker
		aq = new AAQuery(myView);
		//Header
		aq.id(R.id.billinfo_layout).backgroundColorId(R.color.white);
		int imageRes = Utils.theme(R.drawable.billinfo_icon, callback_main.getLob());
		int rImageRes = R.drawable.ios7_question;
		aq.id(R.id.billinfo_header).getTextView().setCompoundDrawablesWithIntrinsicBounds(imageRes, 0, rImageRes, 0);
		aq.id(R.id.billinfo_header).clicked(this, "onClick");
		
		aq.padding(R.id.billinfo_header_layout, 0, basePadding, 0, basePadding);
		
		setLangPopOver();

		aq.id(R.id.billinfo_opt2_custname).text(getResString(R.string.BUPLTF_BI_NAME));
		aq.normEditText(R.id.billinfo_opt2_etxt_addr1, "", "", 30);
		aq.normEditText(R.id.billinfo_opt2_etxt_addr2, "", "", 30);
		aq.normEditText(R.id.billinfo_opt2_etxt_addr3, "", "", 30);
		aq.normEditText(R.id.billinfo_opt2_etxt_addr4, "", "", 30);
		aq.normEditText(R.id.billinfo_opt2_etxt_addr5, "", "", 30);
		aq.normEditText(R.id.billinfo_opt2_etxt_addr6, "", "", 30);

		//disable all editing ui until data is fully loaded
		aq.normEditText(R.id.billinfo_etxt_email, "", "");
		aq.normEditText(R.id.billinfo_etxt_sms, "", "");

		disableSMSUI(true);
		disabeleViews();

		billinfo_opt2_etxt_addr1 = (EditText) aq.id(R.id.billinfo_opt2_etxt_addr1).getView();
		billinfo_opt2_etxt_addr2 = (EditText) aq.id(R.id.billinfo_opt2_etxt_addr2).getView();
		billinfo_opt2_etxt_addr3 = (EditText) aq.id(R.id.billinfo_opt2_etxt_addr3).getView();
		billinfo_opt2_etxt_addr4 = (EditText) aq.id(R.id.billinfo_opt2_etxt_addr4).getView();
		billinfo_opt2_etxt_addr5 = (EditText) aq.id(R.id.billinfo_opt2_etxt_addr5).getView();
		billinfo_opt2_etxt_addr6 = (EditText) aq.id(R.id.billinfo_opt2_etxt_addr6).getView();
		addressTextWathcer();
		disableViewsLanguage();
	}

	private void addressTextWathcer(){
		billinfo_opt2_etxt_addr1.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(aq.id(R.id.billinfo_opt2_etxt_addr1).getText().toString().equalsIgnoreCase(biifCra.getOBiif4Hkt().getBillAdr1())){
					billinfo_opt2_etxt_addr1.setTextColor(getResources().getColor(R.color.black));
				}
				else{
					billinfo_opt2_etxt_addr1.setTextColor(getResources().getColor(R.color.blue));
				}

				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		billinfo_opt2_etxt_addr2.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(aq.id(R.id.billinfo_opt2_etxt_addr2).getText().toString().equalsIgnoreCase(biifCra.getOBiif4Hkt().getBillAdr2())){
					billinfo_opt2_etxt_addr2.setTextColor(getResources().getColor(R.color.black));
				}
				else{
					billinfo_opt2_etxt_addr2.setTextColor(getResources().getColor(R.color.blue));
				}
				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		billinfo_opt2_etxt_addr3.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(aq.id(R.id.billinfo_opt2_etxt_addr3).getText().toString().equalsIgnoreCase(biifCra.getOBiif4Hkt().getBillAdr3())){
					billinfo_opt2_etxt_addr3.setTextColor(getResources().getColor(R.color.black));
				}
				else{
					billinfo_opt2_etxt_addr3.setTextColor(getResources().getColor(R.color.blue));
				}

				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		billinfo_opt2_etxt_addr4.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(aq.id(R.id.billinfo_opt2_etxt_addr4).getText().toString().equalsIgnoreCase(biifCra.getOBiif4Hkt().getBillAdr4())){
					billinfo_opt2_etxt_addr4.setTextColor(getResources().getColor(R.color.black));
				}
				else{
					billinfo_opt2_etxt_addr4.setTextColor(getResources().getColor(R.color.blue));
				}

				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				billinfo_opt2_etxt_addr4.setTextColor(getResources().getColor(R.color.black));
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		billinfo_opt2_etxt_addr5.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(aq.id(R.id.billinfo_opt2_etxt_addr5).getText().toString().equalsIgnoreCase(biifCra.getOBiif4Hkt().getBillAdr5())){
					billinfo_opt2_etxt_addr5.setTextColor(getResources().getColor(R.color.black));
				}
				else{
					billinfo_opt2_etxt_addr5.setTextColor(getResources().getColor(R.color.blue));
				}

				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				billinfo_opt2_etxt_addr5.setTextColor(getResources().getColor(R.color.black));
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

		billinfo_opt2_etxt_addr6.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(aq.id(R.id.billinfo_opt2_etxt_addr6).getText().toString().equalsIgnoreCase(biifCra.getOBiif4Hkt().getBillAdr6())){
					billinfo_opt2_etxt_addr6.setTextColor(getResources().getColor(R.color.black));
				}
				else{
					billinfo_opt2_etxt_addr6.setTextColor(getResources().getColor(R.color.blue));
				}

				// TODO Auto-generated method stub
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				billinfo_opt2_etxt_addr6.setTextColor(getResources().getColor(R.color.black));
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});

	}

	public void setLangPopOver() {
		//LANGUAGE quickaction
		langQuickAction = new QuickAction(getActivity(), screenWidth/2, 0);
		langPopover = aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.getActivity(), R.string.BUPLTF_BILANGEN));

		//Lang Items
		String[] langItems;
		langItems =	new String[]{Utils.getString(me.getActivity(), R.string.BUPLTF_BILANGEN),  Utils.getString(me.getActivity(), R.string.BUPLTF_BILANGZH),  Utils.getString(me.getActivity(), R.string.BUPLTF_BILANGBI)};
		ActionItem[] langItem = new ActionItem[langItems.length];
		for (int i = 0; i < langItems.length; i++) {
			langItem[i] = new ActionItem(i, langItems[i]);
			langQuickAction.addActionItem(langItem[i]);
		}
		langQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				ActionItem actionItem = quickAction.getActionItem(pos);
				//						aq.id(R.id.popover_input_layouttxt).text(actionItem.getTitle());
				langPopover.setText(actionItem.getTitle());
			}
		});
		aq.id(R.id.billinfo_popover_lang).clicked(this, "onClick");
	}

	public void setMediaPopOver(String[] mediaItems) {
		//Bill Media quickaction
		mediaQuickAction = new QuickAction(getActivity(), screenWidth/2, 0);
		mediaPopover = aq.popOverInputView(R.id.billinfo_popover_billmedia, Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_P));

		//Bill Media Items
		ActionItem[] mediaItem = new ActionItem[mediaItems.length];
		for (int i = 0; i < mediaItems.length; i++) {
			mediaItem[i] = new ActionItem(i, mediaItems[i]);
			mediaQuickAction.addActionItem(mediaItem[i]);
		}
		mediaQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				ActionItem actionItem = quickAction.getActionItem(pos);
				//								aq.id(R.id.popover_input_layouttxt).text(actionItem.getTitle());
				mediaPopover.setText(actionItem.getTitle());

				if(actionItem.getTitle().equals(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_P))) {
					isEBill = false;
					//TODO: Alert
					changeToPaperAlertDialog();
					aq.id(R.id.billinfo_etxt_email).text("");
					aq.id(R.id.billinfo_etxt_sms).text("");
					disableSMSUI(true);

					setCheckEmail();

				} else if (actionItem.getTitle().equals(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_E))
						|| actionItem.getTitle().equals(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_X))) {
					isEBill = true;
					enableEmailAddrUI();
					disableSMSUI(true);

					setCheckEmail();

					if (actionItem.getTitle().equals(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_X))) {
						changeToPaperAlertDialog();
					}
					
				} else {
					isEBill = true;
					enableSMSUI();
					enableEmailAddrUI();
					if (biifCra !=null && biifCra.getOBiif4Hkt()!=null && biifCra.getOBiif4Hkt().getBillMedia()!= null && biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S)) {
						aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());
						aq.id(R.id.billinfo_etxt_sms).text(biifCra.getOBiif4Hkt().getSmsNtfn());
					} else {
						aq.id(R.id.billinfo_etxt_email).text(ClnEnv.getQualSvee().getSveeRec().ctMail);
						aq.id(R.id.billinfo_etxt_sms).text(ClnEnv.getQualSvee().getSveeRec().ctMob);
					}
				}
			}
		});
		aq.id(R.id.billinfo_popover_billmedia).clicked(this, "onClick");
	}

	private void setCheckEmail() {
		if (biifCra !=null && biifCra.getOBiif4Hkt()!=null && biifCra.getOBiif4Hkt().getBillMedia()!= null && biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S)) {
			aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());
		} else {
			aq.id(R.id.billinfo_etxt_email).text(ClnEnv.getQualSvee().getSveeRec().ctMail);
		}
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.billinfo_header:
			displayDialog(Utils.getString(me.getActivity(), R.string.myhkt_BUPLTF_FB_BI_ADR));
			break;
		case R.id.billinfo_popover_lang:
			langQuickAction.show(v, aq.id(R.id.billinfo_layout).getView());
			langQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
			break;
		case R.id.billinfo_popover_billmedia:
			mediaQuickAction.show(v, aq.id(R.id.billinfo_layout).getView());
			mediaQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
			break;
		}		
	}

	protected final void refreshData() {
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
			if (!((BillFragment)getParentFragment().getParentFragment()).isBillSum) {
				if (((BillInfoLTSFragment)getParentFragment()).activeSubView ==  R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1) {
					super.refreshData();
					doGetBillInfo();
				}
			}
		}
	}

	public final void refresh() {
		super.refresh();
		
		doGetBillInfo();
//		disabeleViews();
	}


	private void doGetBillInfo() {
		BiifCra biifCra = new BiifCra();
		biifCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		biifCra.setISubnRec(callback_main.getSubnRec().copyMe());
		APIsManager.doGetBillInfo(me, biifCra);
	}

	public final void initBillInfo() {
		aq.id(R.id.billinfo_opt2_custname_txt).text(biifCra.getOBiif4Hkt().getBillNm());
		callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_BILL));
		aq.id(R.id.billinfo_opt2_etxt_addr1).text(biifCra.getOBiif4Hkt().getBillAdr1().replace("\n", "").replace("\r", ""));
		aq.id(R.id.billinfo_opt2_etxt_addr2).text(biifCra.getOBiif4Hkt().getBillAdr2().replace("\n", "").replace("\r", ""));
		aq.id(R.id.billinfo_opt2_etxt_addr3).text(biifCra.getOBiif4Hkt().getBillAdr3().replace("\n", "").replace("\r", ""));
		aq.id(R.id.billinfo_opt2_etxt_addr4).text(biifCra.getOBiif4Hkt().getBillAdr4().replace("\n", "").replace("\r", ""));
		aq.id(R.id.billinfo_opt2_etxt_addr5).text(biifCra.getOBiif4Hkt().getBillAdr5().replace("\n", "").replace("\r", ""));
		aq.id(R.id.billinfo_opt2_etxt_addr6).text(biifCra.getOBiif4Hkt().getBillAdr6().replace("\n", "").replace("\r", ""));
		aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());

		//bill language
		if (biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_ZH)) {
			aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.getActivity(), R.string.BUPLTF_BILANGZH));
		} else if (biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_EN)) {
			aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.getActivity(), R.string.BUPLTF_BILANGEN));
		} else {
			aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.getActivity(), R.string.BUPLTF_BILANGBI));
		}
		aq.id(R.id.billinfo_popover_lang).enabled(true);

		//disable all editing for zombie account
		if (callback_main.IsZombie() || callback_main.isMyMobAcct() || biifCra == null) {
			disableBillAddUI();
			disableEmailAddUI(false);
			disableSMSUI(false);
			aq.id(R.id.billinfo_popover_lang).enabled(false);
			aq.id(R.id.billinfo_popover_billmedia).enabled(false);
			((BillInfoLTSFragment) getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1);
		} else {
			biifCra.getISubnRec().tos = "TEL";
			if (biifCra.getISubnRec().tos.equals("TEL") || biifCra.getISubnRec().tos.equals("DDI")) {
				String[] mediaItems = new String[]{Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_E), Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_X), Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_P)};

				setMediaPopOver(mediaItems);

				//Bill Media Items
				if (biifCra != null && biifCra.getOBiif4Hkt() != null && BiifCra.BILL_MEDIA_X.equals(biifCra.getOBiif4Hkt().getBillMedia())) {
					//BILL_MEDIA_X = X; Paper Bill + Email for LTS "Paper + Electronic Bill"
					mediaPopover.setText(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_X));
				}
				else if(biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.LTS_BILL_MEDIA_P)){
					mediaPopover.setText(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_P));
				}
				else {
					//BILL_MEDIA_S = "S"; /* Email Bill for LTS "Electronic Bill"
					mediaPopover.setText(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_E));
				}

				if (biifCra != null && biifCra.getOBiif4Hkt() != null && biifCra.getOBiif4Hkt().getBillEmail().isEmpty()) {
					aq.id(R.id.billinfo_etxt_email).text(biifCra.getILoginId());
				} else {
					aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());
				}

				enableEmailAddrUI();
				disableSMSUI(true);
				aq.id(R.id.billinfo_popover_billmedia).enabled(true);
			} else {
//				String[] mediaItems = new String[]{Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_E)};
//				setMediaPopOver(mediaItems);
				aq.id(R.id.billinfo_popover_billmedia).enabled(false);
				aq.id(R.id.billinfo_popover_billmedia).getView().setAlpha(0.5f);
				aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());
				mediaPopover.setText(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_E));
				disableSMSUI(true);
				enableEmailAddrUI();
			}

			if (callback_main.getLtsType() == R.string.CONST_LTS_ICFS ||
					callback_main.getLtsType() == R.string.CONST_LTS_ONECALL ||
					callback_main.getLtsType() == R.string.CONST_LTS_IDD0060 ||
					callback_main.getLtsType() == R.string.CONST_LTS_CALLINGCARD) {
				disableBillAddUI();
			} else {
				enableBillAddUI();
			}
			aq.id(R.id.billinfo_popover_lang).enabled(true);
			((BillInfoLTSFragment) getParentFragment()).showRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1);
		}
		disableViewsLanguage();
	}

	public void disableBillAddUI() {
		aq.id(R.id.billinfo_opt2_etxt_addr1).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr2).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr3).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr4).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr5).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr6).enabled(false);
	}

	public void enableBillAddUI() {
		aq.id(R.id.billinfo_opt2_etxt_addr1).enabled(true);
		aq.id(R.id.billinfo_opt2_etxt_addr2).enabled(true);
		aq.id(R.id.billinfo_opt2_etxt_addr3).enabled(true);
		aq.id(R.id.billinfo_opt2_etxt_addr4).enabled(true);
		aq.id(R.id.billinfo_opt2_etxt_addr5).enabled(true);
		aq.id(R.id.billinfo_opt2_etxt_addr6).enabled(true);
	}	

	public void enableEmailAddrUI() {
		aq.id(R.id.billinfo_email_layout).visibility(View.VISIBLE);
		aq.id(R.id.billinfo_etxt_email).enabled(true);
	}
	public void disableEmailAddressET() {
		aq.id(R.id.billinfo_etxt_email).enabled(false);
	}	
	public void disableEmailAddUI(Boolean isHidden) {
		aq.id(R.id.billinfo_email_layout).visibility(isHidden? View.GONE : View.VISIBLE);
		aq.id(R.id.billinfo_etxt_email).enabled(false);
	}	
	public void enableSMSUI() {
		aq.id(R.id.billinfo_sms_layout).visibility(View.VISIBLE);
		aq.id(R.id.billinfo_etxt_sms).enabled(true);
	}
	public void disableSMSUI(Boolean isHidden) {
		aq.id(R.id.billinfo_sms_layout).visibility(isHidden? View.GONE : View.VISIBLE);
		aq.id(R.id.billinfo_etxt_sms).enabled(false);
	}	
	
	public final void updateBillInfo() {
		BiifCra biifCraUpd = biifCra.copyMe();
		biifCraUpd.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		biifCraUpd.setIPyif4Lts(biifCraUpd.getOPyif4Lts().copyMe());
		biifCraUpd.setIBiif4Hkt(biifCraUpd.getOBiif4Hkt().copyMe());
		biifCraUpd.getIBiif4Hkt().setBillAdr1(aq.id(R.id.billinfo_opt2_etxt_addr1).getText().toString());
		biifCraUpd.getIBiif4Hkt().setBillAdr2(aq.id(R.id.billinfo_opt2_etxt_addr2).getText().toString());
		biifCraUpd.getIBiif4Hkt().setBillAdr3(aq.id(R.id.billinfo_opt2_etxt_addr3).getText().toString());
		biifCraUpd.getIBiif4Hkt().setBillAdr4(aq.id(R.id.billinfo_opt2_etxt_addr4).getText().toString());
		biifCraUpd.getIBiif4Hkt().setBillAdr5(aq.id(R.id.billinfo_opt2_etxt_addr5).getText().toString());
		biifCraUpd.getIBiif4Hkt().setBillAdr6(aq.id(R.id.billinfo_opt2_etxt_addr6).getText().toString());
		biifCraUpd.getIBiif4Hkt().setBillEmail(aq.id(R.id.billinfo_etxt_email).getText().toString());
		biifCraUpd.getIBiif4Hkt().setSmsNtfn(aq.id(R.id.billinfo_etxt_sms).getText().toString());
		biifCraUpd.getIBiif4Hkt().setBillLang(getSelectedLangCode());
		if (mediaPopover.getText().equals(getResString(R.string.BUPLTF_MEDIA_E))) {
			biifCraUpd.getIBiif4Hkt().setBillMedia(BiifCra.BILL_MEDIA_S);
		} else if (mediaPopover.getText().equals(getResString(R.string.BUPLTF_MEDIA_X))){
			biifCraUpd.getIBiif4Hkt().setBillMedia(BiifCra.BILL_MEDIA_X);
		} else {
			biifCraUpd.getIBiif4Hkt().setBillMedia(BiifCra.LTS_BILL_MEDIA_P);
		}
		Reply reply = BiifCra.validAdrLen(SubnRec.LOB_LTS, biifCraUpd.getIBiif4Hkt().getBillAdr1(),biifCraUpd.getIBiif4Hkt().getBillAdr2(),biifCraUpd.getIBiif4Hkt().getBillAdr3(),biifCraUpd.getIBiif4Hkt().getBillAdr4(),biifCraUpd.getIBiif4Hkt().getBillAdr5(),biifCraUpd.getIBiif4Hkt().getBillAdr6());
		if (reply.isSucc()) reply = BiifCra.validAdrChr(biifCraUpd.getIBiif4Hkt().getBillAdr1(),biifCraUpd.getIBiif4Hkt().getBillAdr2(),biifCraUpd.getIBiif4Hkt().getBillAdr3(),biifCraUpd.getIBiif4Hkt().getBillAdr4(),biifCraUpd.getIBiif4Hkt().getBillAdr5(),biifCraUpd.getIBiif4Hkt().getBillAdr6());
		if (reply.isSucc() && biifCraUpd.getIBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S)) reply = BiifCra.validEmailFmt(biifCraUpd.getIBiif4Hkt().getBillEmail());
		if (reply.isSucc() && biifCraUpd.getIBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S) && biifCraUpd.getIBiif4Hkt().getSmsNtfn().length() > 0) {
			if (!MyTool.isVaMob(biifCraUpd.getIBiif4Hkt().getSmsNtfn())) {
				reply = new com.pccw.wheat.shared.tool.Reply(RC.BIIF_IV_SMSNTF);
			}
			//			if(validBupdcra.getSmsNtfn().length() != 8) {
			//				reply = new Reply(Reply.RC_RCUS_IVMOB);
			//			}
			//			if(reply.isSucc()) {
			//				for (int i = 0; i < validBupdcra.getSmsNtfn().length(); i++) {
			//					if (!Character.isDigit(validBupdcra.getSmsNtfn().charAt(i)))
			//						reply = new Reply(Reply.RC_RCUS_IVMOB);
			//				}
			//			}
		}
		if ((biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_EN) || biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_ZH))
				&& biifCraUpd.getIBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_BI)) {
			displayDialog(Utils.getString(me.getActivity(), R.string.MYHKT_ERR_INVALID_BILINGUAL));
		} else if (!reply.isSucc()) {
			displayDialog(InterpretRCManager.interpretRC_BinqLtsMdu(getActivity(), reply.getCode()));
		} else {
			confirmUpdateDialog(biifCraUpd);
		}
	}

	private void confirmUpdateDialog(final BiifCra biifCraUpd) {
		DialogInterface.OnClickListener onPosClick = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				APIsManager.doUpdBillinfo(me, biifCraUpd);
			}
		};
		DialogInterface.OnClickListener onNegClick = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				displayDialog(getResString(R.string.CFMM_DISCARD));
				initBillInfo();
			}
		};
		DialogHelper.createTitleDialog(me.getActivity(), getString(R.string.BUPLTM_CFMHDR),	"",
				getString(R.string.CFMF_CFM), onPosClick, 
				getString(R.string.CFMF_NOTCFM), onNegClick);
	}

	private void resetLayout() {

		aq.id(R.id.billinfo_opt2_etxt_addr1).text("");
		aq.id(R.id.billinfo_opt2_etxt_addr2).text("");
		aq.id(R.id.billinfo_opt2_etxt_addr3).text("");
		aq.id(R.id.billinfo_opt2_etxt_addr4).text("");
		aq.id(R.id.billinfo_opt2_etxt_addr5).text("");
		langPopover.setText("");

	}
	
	//CA: Disable views
	private void disabeleViews() {
		aq.id(R.id.billinfo_opt2_etxt_addr1).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr2).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr3).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr4).enabled(false);
		aq.id(R.id.billinfo_opt2_etxt_addr5).enabled(false);
		
		aq.id(R.id.billinfo_popover_lang).enabled(false);
		aq.id(R.id.billinfo_popover_billmedia).enabled(false);

		aq.id(R.id.billinfo_etxt_email).enabled(false);
		aq.id(R.id.billinfo_etxt_sms).enabled(false);
	}

	private void disableViewsLanguage() {
		aq.id(R.id.billinfo_popover_lang).enabled(false);
		aq.id(R.id.billinfo_popover_lang).getView().setAlpha(0.5f);
	}

	private String getSelectedLangCode() {
		if (langPopover.getText().equalsIgnoreCase(getResString(R.string.BUPLTF_BILANGZH))) {
			return BiifCra.BI_LANG_ZH;
		} else if (langPopover.getText().equalsIgnoreCase(getResString(R.string.BUPLTF_BILANGEN))) {
			return BiifCra.BI_LANG_EN;
		} else if (langPopover.getText().equalsIgnoreCase(getResString(R.string.BUPLTF_BILANGBI))) {
			return BiifCra.BI_LANG_BI;
		}
		return "";
	}
	
	// Simple UI Dialog
	protected final void changeToPaperAlertDialog() {
		if(BiifCra.WAIVE_PAPER_ACCT.equalsIgnoreCase(biifCra.getOBiif4Hkt().getWaivePbChrg()) || !BiifCra.BILL_MEDIA_S.equalsIgnoreCase(biifCra.getOBiif4Hkt().getBillMedia())) {
			return;
		}
		AlertDialog.Builder builder = new Builder(getActivity());
		builder.setMessage(Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_P_MSG));
		builder.setPositiveButton(me.getString(R.string.btn_ok), null);
		builder.create().show();
	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.READ_BIIF.equals(response.getActionTy())) {
			biifCra = new BiifCra();
			biifCra = (BiifCra) response.getCra();	
			resetLayout();
			initBillInfo();
			disableViewsLanguage();

		} else if (APIsManager.UPD_BIIF.equals(response.getActionTy())){
			biifCra = new BiifCra();
			biifCra = (BiifCra) response.getCra();
			if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F || callback_main.getLob() == R.string.CONST_LOB_MOB || callback_main.getLob() == R.string.CONST_LOB_IOI) {
				DialogHelper.createSimpleDialog(getActivity(), getActivity().getString(R.string.BUPMBM_UPD_DONE));
			} else if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
				DialogHelper.createSimpleDialog(getActivity(), getActivity().getString(R.string.BUPLTM_UPD_DONE));
			} else {
				DialogHelper.createSimpleDialog(getActivity(), getActivity().getString(R.string.BUPIMM_UPD_DONE));
			} 
			doGetBillInfo();
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		//		 General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}

		//Hide the update button when there is error for reading bill
		if (APIsManager.READ_BIIF.equals(response.getActionTy())) {
			biifCra = new BiifCra();
			biifCra = (BiifCra) response.getCra();
			resetLayout();
			initBillInfo();
			((BillInfoLTSFragment) getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1);
		}
	}

}

