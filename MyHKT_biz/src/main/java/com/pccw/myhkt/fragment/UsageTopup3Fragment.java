package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.TwoBtnCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageListener;
import com.pccw.myhkt.lib.ui.AAQuery;

/************************************************************************
 * File : UsageTopup3Fragment.java
 * Desc : Top-up complete / thank you page
 * Name : UsageTopup3Fragment
 * by 	: Andy Wong
 * Date : 26/01/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 26/01/2016  Andy Wong 		-First draft
 * 26/01/2016  Derek Tsui		-first draft
 *************************************************************************/

public class UsageTopup3Fragment extends BaseServiceFragment {
	private UsageTopup3Fragment me;
	private View myView;
	private AAQuery aq;
	private List<Cell> cellList;

	private PlanMobCra 	  		planMobCra;
	private OnUsageListener 	callback_usage;
	private CellViewAdapter		cellViewAdapter;
	private Boolean 			isMob;
	private LinearLayout 	frame;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback_usage = (OnUsageListener)getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString() + " must implement OnUsageListener");
		}		
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_usagetopup, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}

	protected void initData() {
		super.initData();	
		isMob = (SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob) || SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob) || SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob));
		planMobCra 		  = callback_usage.getPlanMobCra();
		cellViewAdapter = new CellViewAdapter(me.getActivity());
	}

	@Override
	public void onStart() {
		super.onStart();
		//Screen Tracker
	}

	private IconTextCell titleCell;
	private SmallTextCell roamingCell;
	private SmallTextCell thankYou1Cell;
	private SmallTextCell thankYou2Cell;
	private SmallTextCell ammountCell;
	private SmallTextCell errorCodeCell;
	private SmallTextCell btnComplete;
	private TwoBtnCell	  twoBtnCell;
	protected void initUI() {
		aq = new AAQuery(myView);
		aq.id(R.id.fragment_usagetopup_sv).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagetopup_sv, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagetopup_frame).getView();

		cellList = new ArrayList<Cell>();
		//Title Topup Order Detail
		int topupImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
		titleCell = new IconTextCell(topupImg, getResString(R.string.boost_up_order));
		titleCell.setLeftPadding(basePadding);
		titleCell.setRightPadding(basePadding);
		//		cellList.add(titleCell);

		//Roaming		
		roamingCell = new SmallTextCell(getResString(R.string.usage_roam_day_title) , "");
		roamingCell.setLeftPadding(basePadding);
		roamingCell.setRightPadding(basePadding);
		roamingCell.setTitleSizeDelta(-2);
		roamingCell.setTitleColorId(R.color.hkt_txtcolor_grey);
		//		cellList.add(roamingCell);

		//Thank you 
		thankYou1Cell = new SmallTextCell(getResString(R.string.boost_result_success2) , "");
		thankYou1Cell.setLeftPadding(basePadding);
		thankYou1Cell.setRightPadding(basePadding);
		thankYou1Cell.setTitleSizeDelta(-2);
		thankYou1Cell.setTitleColorId(R.color.hkt_txtcolor_grey);
		//		cellList.add(thankYou1Cell);

		thankYou2Cell = new SmallTextCell(getResString(R.string.myhkt_topup_ordersent) , "");
		thankYou2Cell.setLeftPadding(basePadding);
		thankYou2Cell.setRightPadding(basePadding);
		thankYou2Cell.setTitleSizeDelta(-2);
		thankYou2Cell.setTitleColorId(R.color.hkt_txtcolor_grey);
		//		cellList.add(thankYou2Cell);

		ammountCell = new SmallTextCell("" , "");
		ammountCell.setLeftPadding(basePadding);
		ammountCell.setRightPadding(basePadding);
		ammountCell.setTitleSizeDelta(-2);
		ammountCell.setTitleColorId(R.color.hkt_txtcolor_grey);
		//		cellList.add(ammountCell);

		//		
		//		SmallTextCell errorMsgCell = new SmallTextCell(getResString(R.string.boost_result_fail) , "");
		//		errorMsgCell.setLeftPadding(basePadding);
		//		errorMsgCell.setRightPadding(basePadding);
		//		errorMsgCell.setTitleSizeDelta(-2);
		//		errorMsgCell.setTitleColorId(R.color.hkt_txtcolor_grey);
		////		cellList.add(errorMsgCell);	

		errorCodeCell = new SmallTextCell(getResString(R.string.boost_result_error_cd) , "");
		errorCodeCell.setLeftPadding(basePadding);
		errorCodeCell.setRightPadding(basePadding);
		errorCodeCell.setTitleSizeDelta(-2);
		errorCodeCell.setTitleColorId(R.color.hkt_txtcolor_grey);
		//		cellList.add(errorCodeCell);		

		//Back , Continue
		OnClickListener onBackClick = new OnClickListener(){
			@Override
			public void onClick(View v) {
				if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_MOBUSAGE || callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_1010USAGE) {
					callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_USAGEDATA);
					callback_usage.displayChildview(true);		
				} else {
					callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_ROAMING);
					callback_usage.displayChildview(true);
				}
			}			
		};
		twoBtnCell = new TwoBtnCell(isZh ? callback_usage.getSelectedServiceItem().getTitleLevel1Chi() : callback_usage.getSelectedServiceItem().getTitleLevel1Eng() , "", -1,-1, null);
		twoBtnCell.setLeftClickListener(onBackClick);
		twoBtnCell.setTopMargin(basePadding);
		twoBtnCell.setTopPadding(basePadding);
		twoBtnCell.setLeftPadding(basePadding);
		twoBtnCell.setRightPadding(basePadding);
		
		if (isMob) {
			initMobUI();	
		} else {
			init1010UI();
		}
	}

	private void initMobUI() {

		cellList.add(titleCell);
		if ("ROAMING".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getRegion()) || "D".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getQuotaTopupInfo().getQuotaValidityPeriod())) {
			// show Day-pass
			cellList.add(roamingCell);
		}
		//		if (xMobUsgCra.getXMobUsgRchgQuot().getResultValue() != null && "0".equals(xMobUsgCra.getXMobUsgRchgQuot().getResultValue().getErrCode())) {
		if (planMobCra == null) {
			//Connection fail
			ammountCell.setTitle(getResString(R.string.boost_result_fail));
			cellList.add(ammountCell);
		} else if (RC.SUCC.equalsIgnoreCase(planMobCra.getReply().getCode())) {
			// Topup Success
//			double rechargeValue = Double.parseDouble(callback_usage.getSelectedG3BoostUpOfferDTO().getRechargeValue());
			if ("ROAMING".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getRegion())) {
//				ammountCell.setTitle(String.format(getResString(R.string.boost_result_success), Utils.formatNumber(rechargeValue) + " ", getString(R.string.roam_mobile_data)));
				ammountCell.setTitle(String.format(getResString(R.string.boost_result_success), callback_usage.getSelectedG3BoostUpOfferDTO().getRechargeValueInMB() + " ", getString(R.string.roam_mobile_data)));
			} else {
//				ammountCell.setTitle(String.format(getResString(R.string.boost_result_success), Utils.formatNumber(rechargeValue) + " ", getString(R.string.usage_mobile_data)));
				ammountCell.setTitle(String.format(getResString(R.string.boost_result_success), callback_usage.getSelectedG3BoostUpOfferDTO().getRechargeValueInMB() + " ", getString(R.string.usage_mobile_data)));
			}
			cellList.add(thankYou1Cell);
			cellList.add(thankYou2Cell);
			cellList.add(ammountCell);
		} else {
			if ((RC.MOBA_TPUP_DUPL.equalsIgnoreCase(planMobCra.getReply().getCode()) || RC.MOBA_TPUP_SIMU.equalsIgnoreCase(planMobCra.getReply().getCode()))) {
				ammountCell.setTitle(getResString(R.string.boost_result_dup_top_up_error));
				cellList.add(ammountCell);
			} else {
				//Topup fail
				ammountCell.setTitle(getResString(R.string.boost_result_fail));
				cellList.add(ammountCell);
			}
			// Show the Error Code on Screen
			if (planMobCra.getReply().getCode() != null &&
					!(RC.MOBA_TPUP_DUPL.equalsIgnoreCase(planMobCra.getReply().getCode()) || RC.MOBA_TPUP_SIMU.equalsIgnoreCase(planMobCra.getReply().getCode()))) {
				errorCodeCell.setTitle(String.format(getResString(R.string.boost_result_error_cd), planMobCra.getReply().getCode()));
				cellList.add(errorCodeCell);
			}
		}

		cellList.add(twoBtnCell);
		cellViewAdapter.setView(frame, cellList);
	}

	private void init1010UI() {
		cellList.add(titleCell);
		if (planMobCra == null) {
			//Connection fail
			ammountCell.setTitle(getResString(R.string.boost_result_fail));
			cellList.add(ammountCell);
		} else if (planMobCra.getReply() != null && RC.SUCC.equalsIgnoreCase(planMobCra.getReply().getCode())) {
			// Topup Success
//			double rechargeValue = Double.parseDouble(callback_usage.getSelectedG3BoostUpOfferDTO().getRechargeValueInMB());
			ammountCell.setTitle(String.format(getResString(R.string.boost_result_success), callback_usage.getSelectedG3BoostUpOfferDTO().getRechargeValueInMB() + " ", getString(R.string.usage_mobile_data)));
			cellList.add(thankYou1Cell);
			cellList.add(thankYou2Cell);
			cellList.add(ammountCell);
		} else {
			//Topup fail
			ammountCell.setTitle(getResString(R.string.boost_result_fail));
			cellList.add(ammountCell);
			// Show the Error Code on Screen
			if (planMobCra.getReply().getCode() != null &&
					!(RC.MOBA_TPUP_DUPL.equalsIgnoreCase(planMobCra.getReply().getCode()) || RC.MOBA_TPUP_SIMU.equalsIgnoreCase(planMobCra.getReply().getCode()))) {
				errorCodeCell.setTitle(String.format(getResString(R.string.boost_result_error_cd), planMobCra.getReply().getCode()));
				cellList.add(errorCodeCell);
			}
		}
		cellList.add(twoBtnCell);
		cellViewAdapter.setView(frame, cellList);
	}

	protected void cleanupUI(){

	}


	@Override
	public void onSuccess(APIsResponse response) {

	}

	@Override
	public void onFail(APIsResponse response) {

	}

}
