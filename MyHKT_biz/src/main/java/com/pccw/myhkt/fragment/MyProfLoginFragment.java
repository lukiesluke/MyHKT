package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.fragment.MyProfLoginMainFragment.OnMyProfLoginListener;
import com.pccw.myhkt.fragment.UpdateCompanyInfoFragment.OnUpdateCompInfoListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.utils.FirebaseSetting;
import com.pccw.wheat.shared.tool.Reply;

public class MyProfLoginFragment extends BaseFragment {
    private static final String EVENT_INFO = "Profile Settings - Update Login Profile";
    private MyProfLoginFragment me;
    private AAQuery aq;
    private SveeRec sveeRec = null;
    private OnMyProfLoginListener callback;
    private RegInputItem inputName;
    private RegInputItem inputEmail;
    private RegInputItem inputMobNum;
    private String nameInput;
    private String emailInput;
    private String mobInput;
    private String TAG = "myprofloginidFragment";
    private OnUpdateCompInfoListener onUpdateCopanyInfoListener;
    private FirebaseSetting firebaseSetting;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callback = (OnMyProfLoginListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_myprofloginid, container, false);
        aq = new AAQuery(fragmentLayout);
        initData();
        if (firebaseSetting == null) {
            firebaseSetting = new FirebaseSetting(this.getContext());
        }
        return fragmentLayout;
    }

    public void setOnUpdateCopanyInfoListener(OnUpdateCompInfoListener listener) {
        this.onUpdateCopanyInfoListener = listener;
    }

    protected void initData() {
        super.initData();
        //For validation
        String mobPfx = ClnEnv.getPref(this.getActivity(), "mobPfx", "51,52,53,54,55,56,57,59,6,9,84,85,86,87,89");
        String ltsPfx = ClnEnv.getPref(this.getActivity(), "ltsPfx", "2,3,81,82,83");

        DirNum.getInstance(mobPfx, ltsPfx);
    }

    @Override
    public void onStart() {
        sveeRec = callback.getSveeRec();
        super.onStart();
    }

    protected void initUI() {
        nameInput = sveeRec != null ? sveeRec.nickname : "";
        emailInput = sveeRec != null ? sveeRec.ctMail : "";
        mobInput = sveeRec != null ? sveeRec.ctMob : "";

        //scrollview
        aq.marginpx(R.id.myprofloginid_scrollView, basePadding, 0, basePadding, 0);

        //layout
        aq.id(R.id.myprofloginid_layout1).margin(0, 0, 0, 0);
        aq.id(R.id.myprofloginid_header).textSize(getResources().getInteger(R.integer.textsize_default_int));
        aq.id(R.id.myprofloginid_email).textColorId(R.color.hkt_txtcolor_grey);
        aq.marginpx(R.id.myprofloginid_email, 0, 0, 0, basePadding);
        aq.id(R.id.myprofloginid_email).textSize(getResources().getInteger(R.integer.textsize_default_int));

        //change password button
        Drawable image = getResources().getDrawable(R.drawable.btn_details);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        aq.id(R.id.myprofloginid_changepwd).getTextView().setCompoundDrawables(null, null, image, null);
        aq.id(R.id.myprofloginid_changepwd).getTextView().setCompoundDrawablePadding(10);
        aq.id(R.id.myprofloginid_changepwd).text(R.string.myhkt_myprof_chgpwd).textColorId(R.color.hkt_buttonblue).clicked(this, "onClick");
        aq.id(R.id.myprofloginid_changepwd).textSize(getResources().getInteger(R.integer.textsize_default_int));
        aq.id(R.id.myprofloginid_changepwd).getTextView().setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

        //CA: Update Company Info button
        aq.id(R.id.myprofloginid_update_company_info).getTextView().setCompoundDrawables(null, null, image, null);
        aq.id(R.id.myprofloginid_update_company_info).getTextView().setCompoundDrawablePadding(10);
        aq.id(R.id.myprofloginid_update_company_info).text(R.string.comm_profile_cominfo_btn).textColorId(R.color.hkt_buttonblue).clicked(this, "onClick");
        aq.id(R.id.myprofloginid_update_company_info).textSize(getResources().getInteger(R.integer.textsize_default_int));
        aq.id(R.id.myprofloginid_update_company_info).getTextView().setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        aq.id(R.id.myprofloginid_update_company_info).clicked(this, "onClick");

        //Delete account button
        Drawable imageArrowRight = getResources().getDrawable(R.drawable.btn_details);
        imageArrowRight.setBounds(0, 0, imageArrowRight.getIntrinsicWidth(), imageArrowRight.getIntrinsicHeight());
        aq.id(R.id.myprofloginid_delete).getTextView().setCompoundDrawables(null, null, imageArrowRight, null);
        aq.id(R.id.myprofloginid_delete).getTextView().setCompoundDrawablePadding(10);
        aq.id(R.id.myprofloginid_delete).text(R.string.myhkt_delete_account_header).textColorId(R.color.hkt_buttonblue).clicked(this, "onClick");
        aq.id(R.id.myprofloginid_delete).textSize(getResources().getInteger(R.integer.textsize_default_int));
        aq.id(R.id.myprofloginid_delete).getTextView().setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

        //header
        aq.id(R.id.myprofloginid_header).text(R.string.myhkt_myprof_loginid).getTextView().setTypeface(Typeface.MONOSPACE, Typeface.BOLD);

        //remark
        aq.id(R.id.myprofloginid_remark).text(R.string.myhkt_myprof_remark).textSize(AAQuery.getDefaultTextSize() - 2);
        aq.marginpx(R.id.myprofloginid_remark, 0, 0, 0, basePadding);

        //edit text
        inputName = (RegInputItem) aq.id(R.id.myprofloginid_name_et).getView();
        inputName.initViews(getActivity(), getResString(R.string.myhkt_myprof_nickname), getResString(R.string.myhkt_myprof_nickname_hint), nameInput);
        inputName.setPadding(0, 0, 0, 10);
        inputName.setMaxLength(20);

        inputEmail = (RegInputItem) aq.id(R.id.myprofloginid_email_et).getView();
        inputEmail.initViews(getActivity(), getResString(R.string.myhkt_myprof_email), getResString(R.string.myhkt_myprof_email_hint), emailInput);
        inputEmail.setPadding(0, 0, 0, 10);
        inputEmail.setMaxLength(40);

        inputMobNum = (RegInputItem) aq.id(R.id.myprofloginid_mobnum_et).getView();
        inputMobNum.initViews(getActivity(), getResString(R.string.myhkt_myprof_mobnum), getResString(R.string.myhkt_myprof_mobnum_hint), mobInput, InputType.TYPE_CLASS_PHONE);
        inputMobNum.setPadding(0, 0, 0, 10);
        inputMobNum.setMaxLength(8);

        //button layout
        aq.norm2TxtBtns(R.id.myprofloginid_btn_cancel, R.id.myprofloginid_btn_update, getResString(R.string.MYHKT_BTN_CANCEL), getResString(R.string.MYHKT_BTN_UPDATE));
        aq.id(R.id.myprofloginid_btn_update).clicked(this, "onClick");
        aq.id(R.id.myprofloginid_btn_cancel).clicked(this, "onClick");

        prepareAccData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myprofloginid_changepwd:
                ((MyProfLoginMainFragment) getParentFragment()).openChangePwSubFrag();
                break;
            case R.id.myprofloginid_btn_cancel:
                Utils.closeSoftKeyboard(getActivity(), v);
                ((MyProfLoginMainFragment) getParentFragment()).closeActivity();
                break;
            case R.id.myprofloginid_delete:
                ((MyProfLoginMainFragment) getParentFragment()).openDeleteAccountFrag(sveeRec.loginId);
                break;
            case R.id.myprofloginid_btn_update:
                Utils.closeSoftKeyboard(getActivity(), v);
                nameInput = inputName.getInput();
                emailInput = inputEmail.getInput();
                mobInput = inputMobNum.getInput();

                AcMainCra acMainCra = new AcMainCra();
//			acMainCra.setICustRec(ClnEnv.getLgiCra().getICustRec().copyMe());
                acMainCra.setILoginId(sveeRec.loginId);
                SveeRec mSveeRec = sveeRec.copyMe();
                mSveeRec.ctMob = mobInput;
                mSveeRec.ctMail = emailInput;
                mSveeRec.nickname = nameInput;
                acMainCra.setISveeRec(mSveeRec);

                Reply reply = mSveeRec.verifyNickName(false);
                if (reply.isSucc()) reply = mSveeRec.verifyCtMail();
                if (reply.isSucc()) reply = mSveeRec.verifyCtMob(false);

                if (!reply.isSucc()) {
                    DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), reply));
                } else {
                    APIsManager.doUpdProfile(null, me, acMainCra);
                }
                break;
            case R.id.myprofloginid_update_company_info:
                onUpdateCopanyInfoListener.onShowUpdateCompInfo();
                break;
        }
    }


    private void prepareAccData() {
        if (sveeRec != null) {
            nameInput = sveeRec != null ? sveeRec.nickname : "";
            emailInput = sveeRec != null ? sveeRec.ctMail : "";
            mobInput = sveeRec != null ? sveeRec.ctMob : "";

            aq.id(R.id.myprofloginid_email).text(" " + sveeRec.loginId);
            aq.id(R.id.myprofloginid_email).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.loginid_icon, 0, 0, 0);
            inputName.setEditText(nameInput);
            inputEmail.setEditText(emailInput);
            inputMobNum.setEditText(mobInput);
        }
    }

    @Override
    public void onSuccess(APIsResponse response) {
        AcMainCra acMainCra = (AcMainCra) response.getCra();
        ClnEnv.getLgiCra().getOQualSvee().setSveeRec(acMainCra.getOSveeRec().copyMe());
        callback.setSveeRec(acMainCra.getOSveeRec().copyMe());
        sveeRec = callback.getSveeRec();
        firebaseSetting.onUpdateInfoEvent(EVENT_INFO);
        displayDialog(getResString(R.string.PAMM_DONE));
    }

    @Override
    public void onFail(APIsResponse response) {
        // General Error Message
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else {
            DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), response.getReply()));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        nameInput = inputName.getInput();
        emailInput = inputEmail.getInput();
        mobInput = inputMobNum.getInput();

        savedInstanceState.putString("name", inputName.getInput());
        savedInstanceState.putString("email", inputEmail.getInput());
        savedInstanceState.putString("mob", inputMobNum.getInput());

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }
}
