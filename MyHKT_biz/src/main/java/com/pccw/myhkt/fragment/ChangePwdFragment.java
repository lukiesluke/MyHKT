package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.fragment.MyProfLoginMainFragment.OnMyProfLoginListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.utils.FirebaseSetting;
import com.pccw.wheat.shared.tool.Reply;

public class ChangePwdFragment extends BaseFragment {
    private static final String EVENT_INFO = "Profile Settings - Change Password";
    private ChangePwdFragment me;
    private AAQuery aq;
    private SveeRec sveeRec = null;
    // CallBack
    private OnMyProfLoginListener callback;
    private RegInputItem inputCurrentPw;
    private RegInputItem inputNewPw;
    private RegInputItem inputNewRePw;
    private Button cancelBtn;
    private Button updateBtn;
    private String currentPwInput;
    private String newPwInput;
    private String newRePwInput;
    private String TAG = "ChangePwdFragment";
    private FirebaseSetting firebaseSetting;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callback = (OnMyProfLoginListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnMyProfLoginListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_myproflogin_changepw, container, false);
        initData();
        firebaseSetting = new FirebaseSetting(this.getContext());
        return fragmentLayout;
    }

    protected void initData() {
        sveeRec = callback.getSveeRec();
        super.initData();
    }

    protected void initUI() {
        super.initUI();
        currentPwInput = sveeRec != null ? sveeRec.nickname : "";
        newPwInput = sveeRec != null ? sveeRec.ctMail : "";
        newRePwInput = sveeRec != null ? sveeRec.ctMob : "";

        aq = new AAQuery(getActivity());
        //scrollview
        aq.marginpx(R.id.myproflogin_changepw_scrollView, basePadding, 0, basePadding, 0);

        //header
        aq.id(R.id.myproflogin_changepw_header).margin(0, 0, 0, 0);
        aq.id(R.id.myproflogin_changepw_header).getTextView().setText(getResources().getString(R.string.myhkt_myprof_chgpwd));
        aq.id(R.id.myproflogin_changepw_header).getTextView().setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
        aq.id(R.id.myproflogin_changepw_header).textSize(getResources().getInteger(R.integer.textsize_default_int));

        //edit text
        inputCurrentPw = (RegInputItem) aq.id(R.id.myproflogin_changepw_name_et).getView();
        inputCurrentPw.initViews(getActivity(), getResources().getString(R.string.myhkt_myprof_curpwd), "", "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
        inputCurrentPw.setPadding(0, 0, 0, 10);
        inputCurrentPw.setMaxLength(16);

        inputNewPw = (RegInputItem) aq.id(R.id.myproflogin_changepw_email_et).getView();
        inputNewPw.initViews(getActivity(), getResources().getString(R.string.myhkt_myprof_newpwd), getResources().getString(R.string.REGF_PWD_HINT), "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
        inputNewPw.setPadding(0, 0, 0, 10);
        inputNewPw.setMaxLength(16);

        inputNewRePw = (RegInputItem) aq.id(R.id.myproflogin_changepw_mobnum_et).getView();
        inputNewRePw.initViews(getActivity(), getResources().getString(R.string.myhkt_myprof_retpwd), "", "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
        inputNewRePw.setPadding(0, 0, 0, 10);
        inputNewRePw.setMaxLength(16);

        //button layout
        aq.norm2TxtBtns(R.id.myproflogin_changepw_btn_cancel, R.id.myproflogin_changepw_btn_update, getResString(R.string.MYHKT_BTN_CANCEL), getResString(R.string.MYHKT_BTN_UPDATE));
        aq.id(R.id.myproflogin_changepw_btn_update).clicked(this, "onClick");
        aq.id(R.id.myproflogin_changepw_btn_cancel).clicked(this, "onClick");
//		prepareAccData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myproflogin_changepw_btn_cancel:
                ((MyProfLoginMainFragment) getParentFragment()).openLoginidSubFrag();
                break;
            case R.id.myproflogin_changepw_btn_update:
                Utils.closeSoftKeyboard(getActivity(), v);

                currentPwInput = inputCurrentPw.getInput();
                newPwInput = inputNewPw.getInput();
                newRePwInput = inputNewRePw.getInput();

                AcMainCra acMainCra = new AcMainCra();

//			SveeRec mSveeRec = ClnEnv.getQualSvee().getSveeRec().copyMe();
                SveeRec mSveeRec = sveeRec.copyMe();
//			acMainCra.setISveeRec(mSveeRec);
                acMainCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
                acMainCra.setIChgPwd(true);
                mSveeRec.pwd = newPwInput;
                mSveeRec.lang = ClnEnv.getAppLocale(getActivity().getBaseContext());
                acMainCra.setISveeRec(mSveeRec);
                acMainCra.setIOrigPwd(currentPwInput);

                SveeRec currPwCheck = sveeRec.copyMe(); //for current password checking
                currPwCheck.pwd = currentPwInput;
                Reply reply = currPwCheck.verifyPwd(); //verify current pwd
                if (!reply.isSucc()) {
//				DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), reply));
                    DialogHelper.createSimpleDialog(getActivity(), getString(R.string.PAMM_IVCURRPWD));
                    return;
                }
                reply = mSveeRec.verifyPwd(); //verify new pwd
                if (!reply.isSucc()) {
                    DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), reply));
                    return;
                }

                //Check if the retype pwd = new pwd
                if (!newPwInput.equals(newRePwInput)) {
                    // Display error message PAMM_IVCFMPWD
                    displayDialog(getResString(R.string.PAMM_IVCFMPWD));
                    return;
                }
                acMainCra.setISveeRec(mSveeRec);
                APIsManager.doChangePwd(me, acMainCra);
                break;
        }
    }

    @Override
    public void onSuccess(APIsResponse response) {
        displayDialog(getResString(R.string.PAMM_DONE));
        AcMainCra acMainCra = (AcMainCra) response.getCra();

        //store new Svee
        ClnEnv.getQualSvee().setSveeRec(acMainCra.getOSveeRec());
        callback.setSveeRec(acMainCra.getOSveeRec());

        //update session password
        ClnEnv.setSessionPassword(inputNewPw.getInput());

        //remove old saved password
        ClnEnv.setPref(getActivity().getApplicationContext(), getActivity().getString(R.string.CONST_PREF_SAVEPASSWORD), false);
        ClnEnv.setEncPref(getActivity().getApplicationContext(), "", getActivity().getString(R.string.CONST_PREF_PASSWORD), "");
        firebaseSetting.onUpdateInfoEvent(EVENT_INFO);
        ((MyProfLoginMainFragment) getParentFragment()).openLoginidSubFrag();
    }

    @Override
    public void onFail(APIsResponse response) {
        // General Error Message
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else {
            DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), response.getReply()));
        }
    }
}
