package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentTransaction;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;

public class BillFragment extends BaseServiceFragment{
	private BillFragment me;
	private AAQuery aq;

	private FragmentTransaction ft;
	private BillSumFragment billSumFragment;
	private BillInfoFragment billInfoFragment;
	private BillInfoLTSFragment billInfoLTSFragment;
	private String TAG = "BillFragment";
	public Boolean isBillSum = true;

	public int billInfoLTSSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume(){
		super.onResume();
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_bill, container, false);

		if (isBillSum) {
			openBillSumFrag(false);
		} else {
			openBillInfoFrag(false);
		}		

		return fragmentLayout;
	}

	public void openBillInfoFrag(){
		openBillInfoFrag(true);
	}
	public void openBillInfoFrag(Boolean isModIdSet){
		isBillSum = false;
		ft = getChildFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
		Utils.closeSoftKeyboard(getActivity());
		if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
			billInfoLTSFragment = new BillInfoLTSFragment();	
			if (debug) Log.i(TAG, "is child2 " + (billInfoLTSSubView == R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2));					
			ft.replace(R.id.bill_main_frame, billInfoLTSFragment).commit();
		} else {
			billInfoFragment = new BillInfoFragment();	
			ft.replace(R.id.bill_main_frame, billInfoFragment).commit();
		}
		setModuleId(isModIdSet);
	}
	public void openBillSumFrag(){
		openBillSumFrag(true);
	}

	public void openBillSumFrag(Boolean isModIdSet){
		isBillSum = true;
		billSumFragment = new BillSumFragment();		
		ft = getChildFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
		Utils.closeSoftKeyboard(getActivity());
		ft.replace(R.id.bill_main_frame, billSumFragment).commit();
		setModuleId(isModIdSet);
	}

	@Override
	public final void refreshData() {
		super.refreshData();
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {		
			setModuleId();
		} else {
			//Replace the current bill page to BillSum when bill fragment is not shown 
			isBillSum = true;
			billSumFragment = new BillSumFragment();
			ft = getChildFragmentManager().beginTransaction();
			Utils.closeSoftKeyboard(getActivity());
			ft.replace(R.id.bill_main_frame, billSumFragment).commit();
		}
	}

	@Override
	public final void refresh() {
		super.refresh();
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {		
			setModuleId();		
			if (isBillSum) {
				billSumFragment.refresh();
			} else {
				if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
					billInfoLTSSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1;
					billInfoLTSFragment.refresh();
				} else {
					billInfoFragment.refresh();
				}
			}
		}
	}

	public void closeActivity(){
	}

	protected void setModuleId() {
		//Module id
		//Bill Sum and Bill Info have same module id except LTS
		int lob = callback_main.getLob();
		switch (lob) {
		case R.string.CONST_LOB_LTS:
			if (isBillSum) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_BILL));
			} else {
				billInfoLTSFragment.setModuleId();
			}
			break;
		case R.string.CONST_LOB_PCD:
			if(callback_main.getSubnRec().lob.equalsIgnoreCase("PCD") && callback_main.getSubnRec().isBillByAgent()){ 
				//show the message
				callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_BILL_4MOBCS));
			} else {
				callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_BILL));
			}
			break;
		case R.string.CONST_LOB_TV:
			callback_livechat.setModuleId(getResString(R.string.MODULE_TV_BILL));
			break;
		case R.string.CONST_LOB_1010:		

			if (ClnEnv.isMyMobFlag()) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_101_MM_BILL));
			} else {
				callback_livechat.setModuleId(getResString(R.string.MODULE_101_BILL));
			}			
			break;
		case R.string.CONST_LOB_O2F:
			if (ClnEnv.isMyMobFlag()) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_O2F_MM_BILL));
			} else {
				callback_livechat.setModuleId(getResString(R.string.MODULE_O2F_BILL));
			}
			break;
		case R.string.CONST_LOB_MOB:
			if (ClnEnv.isMyMobFlag()) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_MOB_MM_BILL));
			} else {
				callback_livechat.setModuleId(getResString(R.string.MODULE_MOB_BILL));
			}
			break;
		case R.string.CONST_LOB_IOI:
			if (ClnEnv.isMyMobFlag()) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_IOI_MM_BILL));
			} else {
				callback_livechat.setModuleId(getResString(R.string.MODULE_IOI_BILL));
			}
			break;
		}	
		super.setModuleId();
	}

	protected void setModuleId(Boolean isModSet) {
		if (isModSet) {
			setModuleId();
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
	}

}
