package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.TextView;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.BcifRec;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.fragment.MyProfLoginMainFragment.OnMyProfLoginListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.PopOverInputView;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.utils.FirebaseSetting;
import com.pccw.wheat.shared.tool.Reply;

public class UpdateCompanyInfoFragment extends BaseFragment {
    private static final String EVENT_INFO = "Profile Settings - Update Company Information";
    private static final String[] INDUSTRY_VALUES = {
            "D_INDUST_OPT_ASSOC",
            "D_INDUST_OPT_BANK",
            "D_INDUST_OPT_PROF_SRVCS",
            "D_INDUST_OPT_CONSTRCT",
            "D_INDUST_OPT_EDU_SRVCS",
            "D_INDUST_OPT_INVSTMNT",
            "D_INDUST_OPT_GOVT",
            "D_INDUST_OPT_HEALTH",
            "D_INDUST_OPT_HOTEL",
            "D_INDUST_OPT_INSURNCE",
            "D_INDUST_OPT_MANUF",
            "D_INDUST_OPT_PERSNL_SRVS",
            "D_INDUST_OPT_REAL_ESTATE",
            "D_INDUST_OPT_RESTRNT",
            "D_INDUST_OPT_RETAIL",
            "D_INDUST_OPT_TELECOM",
            "D_INDUST_OPT_TRADE",
            "D_INDUST_OPT_TRANSPO_LOGC",
            //"D_INDUST_OPT_TRAVEL_AGNCY",
            "D_INDUST_OPT_UTILITY",
            "D_INDUST_OPT_WHOLSALE",
            "D_INDUST_OPT_OTHRS"};
    private static final String[] IS_BRANCH_OS_HK_VALUES = {"Y", "N"};
    private static final String[] TITLES_VALUES = {BcifRec.TTL_MR, BcifRec.TTL_MRS, BcifRec.TTL_MS};
    private final int colMaxNum = 3;
    ActionItem[] aIINdustry;
    ActionItem[] aITitles;
    ActionItem[] aIBranches;
    private UpdateCompanyInfoFragment me;
    private AAQuery aq;
    private BcifRec bcifRec;
    //CallBack
    private OnUpdateCompInfoListener onUpdateCompCallback;
    private OnMyProfLoginListener callback;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int buttonPadding = 0;
    private int basePadding = 0;
    private int extralinespace = 0;
    private int txtColor;
    private int btnWidth = 0;
    private RegInputItem regInputCompName;
    private RegInputItem regInputPCName;
    private RegInputItem regInputPCTelNum;
    private RegInputItem regInputPCMobileNum;
    private RegInputItem regInputPCEmail;
    private RegInputItem regInputPCJobTitle;
    private RegInputItem regInputSCName;
    private RegInputItem regInputSCTelNum;
    private RegInputItem regInputSCMobileNum;
    private RegInputItem regInputSCEmail;
    private RegInputItem regInputSCJobTitle;
    private String strCompName;
    private String strPCName;
    private String strPCTelNum;
    private String strPCMobileNum;
    private String strPCEmail;
    private String strPCJobTitle;
    private String strSCName;
    private String strSCTelNum;
    private String strSCMobileNum;
    private String strSCEmail;
    private String strSCJobTitle;
    private QuickAction mQAIndustry;
    private QuickAction mQAStaffNum;
    private QuickAction mQAHKBranches;
    private QuickAction mQABranches; //branches outside HK
    private QuickAction mQAPrimaryTitle;
    private QuickAction mQASecondaryTitle;
    private PopOverInputView regComIndustryPopOver;
    private PopOverInputView regComStaffNumPopOver;
    private PopOverInputView regComHKBranchPopOver;
    private PopOverInputView regComBrachePopOver;
    private PopOverInputView regComPTitlePopOver;
    private PopOverInputView regComSTitlePopOver;
    private FirebaseSetting firebaseSetting;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;

        View fragmentLayout = inflater.inflate(R.layout.fragment_myprofile_update_comp_info, container, false);
        aq = new AAQuery(fragmentLayout);
        initData();
        if (firebaseSetting == null) {
            firebaseSetting = new FirebaseSetting(this.getContext());
        }
        return fragmentLayout;
    }

    public void setOnUpdateInfoListener(OnUpdateCompInfoListener listener) {
        this.onUpdateCompCallback = listener;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callback = (OnMyProfLoginListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
        }
    }

    @Override
    public void onStart() {
        bcifRec = callback.getBcifRec();
        super.onStart();
    }

    protected void initData() {
        super.initData();
        //For validation
        String mobPfx = ClnEnv.getPref(this.getActivity(), "mobPfx", "51,52,53,54,55,56,57,59,6,9,84,85,86,87,89");
        String ltsPfx = ClnEnv.getPref(this.getActivity(), "ltsPfx", "2,3,81,82,83");

        DirNum.getInstance(mobPfx, ltsPfx);
    }

    private void prepareAccData() {
        if (bcifRec != null) {

            strCompName = bcifRec != null ? bcifRec.custNm.trim() : "";
            strPCName = bcifRec != null ? bcifRec.priCtName.trim() : "";
            strPCTelNum = bcifRec != null ? bcifRec.priCtTel.trim() : "";
            strPCMobileNum = bcifRec != null ? bcifRec.priCtMob.trim() : "";
            strPCEmail = bcifRec != null ? bcifRec.priCtMail.trim() : "";
            strPCJobTitle = bcifRec != null ? bcifRec.priCtJobtitle.trim() : "";
            strSCName = bcifRec != null ? bcifRec.secCtName.trim() : "";
            strSCTelNum = bcifRec != null ? bcifRec.secCtTel.trim() : "";
            strSCMobileNum = bcifRec != null ? bcifRec.secCtMob.trim() : "";
            strSCEmail = bcifRec != null ? bcifRec.secCtMail.trim() : "";
            strSCJobTitle = bcifRec != null ? bcifRec.secCtJobtitle.trim() : "";

            regInputCompName.setEditText(strCompName);
            regInputPCName.setEditText(strPCName);
            regInputPCTelNum.setEditText(strPCTelNum);
            regInputPCMobileNum.setEditText(strPCMobileNum);
            regInputPCEmail.setEditText(strPCEmail);
            regInputPCJobTitle.setEditText(strPCJobTitle);
            regInputSCName.setEditText(strSCName);
            regInputSCTelNum.setEditText(strSCTelNum);
            regInputSCMobileNum.setEditText(strSCMobileNum);
            regInputSCEmail.setEditText(strSCEmail);
            regInputSCJobTitle.setEditText(strSCJobTitle);

            int index = getIndex(INDUSTRY_VALUES, bcifRec.industry.trim());
            String selectedIndustry = index != -1 ? aIINdustry[index].getTitle() : getResString(R.string.comm_cominfo_select_default);

            int xborderIndex = getIndex(IS_BRANCH_OS_HK_VALUES, bcifRec.xborder.trim());
            String selectedXBorder = xborderIndex != -1 ? aIBranches[xborderIndex].getTitle() : getResString(R.string.comm_cominfo_select_default);

            int pTitleIndex = getIndex(TITLES_VALUES, bcifRec.priCtTitle.trim());
            String selectedPTitle = pTitleIndex != -1 ? aITitles[pTitleIndex].getTitle() : getResString(R.string.comm_cominfo_select_default);

            int sTitleIndex = getIndex(TITLES_VALUES, bcifRec.secCtTitle.trim());
            String selectedSTitle = sTitleIndex != -1 ? aITitles[sTitleIndex].getTitle() : getResString(R.string.comm_cominfo_select_default);

            regComIndustryPopOver.setText(selectedIndustry);
            regComStaffNumPopOver.setText(!TextUtils.isEmpty(bcifRec.nuStaff.trim()) ? bcifRec.nuStaff.trim() : getResString(R.string.comm_cominfo_select_default));
            regComHKBranchPopOver.setText(!TextUtils.isEmpty(bcifRec.nuPresence.trim()) ? bcifRec.nuPresence.trim() : getResString(R.string.comm_cominfo_select_default));
            regComBrachePopOver.setText(selectedXBorder);
            regComPTitlePopOver.setText(selectedPTitle);
            regComSTitlePopOver.setText(selectedSTitle);


        }
    }

    @Override
    protected void initUI() {
        super.initUI();

        strCompName = bcifRec != null ? bcifRec.custNm.trim() : "";
        strPCName = bcifRec != null ? bcifRec.priCtName.trim() : "";
        strPCTelNum = bcifRec != null ? bcifRec.priCtTel.trim() : "";
        strPCMobileNum = bcifRec != null ? bcifRec.priCtMob.trim() : "";
        strPCEmail = bcifRec != null ? bcifRec.priCtMail.trim() : "";
        strPCJobTitle = bcifRec != null ? bcifRec.priCtJobtitle.trim() : "";
        strSCName = bcifRec != null ? bcifRec.secCtName.trim() : "";
        strSCTelNum = bcifRec != null ? bcifRec.secCtTel.trim() : "";
        strSCMobileNum = bcifRec != null ? bcifRec.secCtMob.trim() : "";
        strSCEmail = bcifRec != null ? bcifRec.secCtMail.trim() : "";
        strSCJobTitle = bcifRec != null ? bcifRec.secCtJobtitle.trim() : "";

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        colWidth = (deviceWidth - basePadding * 4 + buttonPadding * 2) / colMaxNum;
        btnWidth = (deviceWidth - basePadding * 4) / 2;
        txtColor = getResources().getColor(R.color.hkt_txtcolor_grey);

        //scrollview
        //aq.marginpx(R.id.regservice_frame, basePadding, 0, basePadding, 0);

        aq.id(R.id.com_info_title_textview).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(getResources().getColor(R.color.black)).text(getResString(R.string.comm_cominfo_title));
        aq.id(R.id.com_info_title_textview).getView().setPadding(basePadding, 0, basePadding, 0);


        aq.id(R.id.regbasic_primary_contact).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(getResources().getColor(R.color.black)).text(getResString(R.string.comm_cominfo_pricontact));
        aq.id(R.id.regbasic_primary_contact).getView().setPadding(basePadding, 0, basePadding, 0);

        aq.id(R.id.regbasic_secondary_contact).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(getResources().getColor(R.color.black)).text(getResString(R.string.comm_cominfo_seccontact));
        aq.id(R.id.regbasic_secondary_contact).getView().setPadding(basePadding, 0, basePadding, 0);

        regInputCompName = aq.regInputItem(R.id.regbasic_input_comp_name, getResString(R.string.comm_cominfo_company), getResString(R.string.myhkt_myprof_nickname_hint), strCompName, 20);


        //Primary contacts
        regInputPCName = aq.regInputItem(R.id.regbasic_primary_input_name, getResString(R.string.MYHKT_DQ_LBL_NAME), getResString(R.string.myhkt_myprof_nickname_hint), strPCName, 20);
        regInputPCTelNum = aq.regInputItem(R.id.regbasic_primary_input_tel_num, getResString(R.string.comm_cominfo_phone), getResString(R.string.myhkt_myprof_mobnum_hint), strPCTelNum,
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
        regInputPCMobileNum = aq.regInputItem(R.id.regbasic_primary_input_mobile_num, getResString(R.string.comm_cominfo_mobile), getResString(R.string.myhkt_myprof_mobnum_hint), strPCMobileNum,
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
        regInputPCEmail = aq.regInputItem(R.id.regbasic_primary_input_email_add, getResString(R.string.comm_cominfo_email), getResString(R.string.myhkt_myprof_email_hint), strPCEmail,
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS, getResources().getInteger(R.integer.CONST_MAX_LOGINID));
        regInputPCJobTitle = aq.regInputItem(R.id.regbasic_primary_input_job_title, getResString(R.string.comm_cominfo_jobtitle), getResString(R.string.myhkt_myprof_nickname_hint), strPCJobTitle, 20);
        //TODO popover

        //Secondary contacts
        aq.normTextGrey(R.id.regbasic_secondary_title_txt, getResString(R.string.comm_cominfo_name_title));
        regInputSCName = aq.regInputItem(R.id.regbasic_secondary_input_name, getResString(R.string.MYHKT_DQ_LBL_NAME), getResString(R.string.myhkt_myprof_nickname_hint), strSCName, 20);
        regInputSCTelNum = aq.regInputItem(R.id.regbasic_secondary_input_tel_num, getResString(R.string.comm_cominfo_phone), getResString(R.string.myhkt_myprof_mobnum_hint), strSCTelNum,
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
        regInputSCMobileNum = aq.regInputItem(R.id.regbasic_secondary_input_mobile_num, getResString(R.string.comm_cominfo_mobile), getResString(R.string.myhkt_myprof_mobnum_hint), strSCMobileNum,
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
        regInputSCEmail = aq.regInputItem(R.id.regbasic_secondary_input_email_add, getResString(R.string.comm_cominfo_email), getResString(R.string.myhkt_myprof_email_hint), strSCEmail,
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS, getResources().getInteger(R.integer.CONST_MAX_LOGINID));
        regInputSCJobTitle = aq.regInputItem(R.id.regbasic_secondary_input_job_title, getResString(R.string.comm_cominfo_jobtitle), getResString(R.string.myhkt_myprof_nickname_hint), strSCJobTitle, 20);

        regComIndustryPopOver = aq.popOverInputView(R.id.regbasic_industry_spinner, getResString(R.string.comm_cominfo_select_default));
        regComStaffNumPopOver = aq.popOverInputView(R.id.regbasic_num_staff_spinner, getResString(R.string.comm_cominfo_select_default));
        regComHKBranchPopOver = aq.popOverInputView(R.id.regbasic_num_hk_branch_spinner, getResString(R.string.comm_cominfo_select_default));
        regComBrachePopOver = aq.popOverInputView(R.id.regbasic_num_branch_spinner, getResString(R.string.comm_cominfo_select_default));
        regComPTitlePopOver = aq.popOverInputView(R.id.regbasic_primary_title_spinner, getResString(R.string.comm_cominfo_select_default));
        regComSTitlePopOver = aq.popOverInputView(R.id.regbasic_secondary_title_spinner, getResString(R.string.comm_cominfo_select_default));

        //Industry
        aq.id(R.id.regbasic_industry_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_industry_txt, getResString(R.string.comm_cominfo_industry));
        aq.layoutWeight(R.id.regbasic_industry_txt, 1);
        TextView textView2 = aq.id(R.id.regbasic_industry_txt).getTextView();
        textView2.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_industry_spinner, 1);

        //Staff Num
        aq.id(R.id.regbasic_num_staff_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_num_staff_txt, getResString(R.string.comm_cominfo_staffnum));
        aq.layoutWeight(R.id.regbasic_num_staff_txt, 1);
        TextView textView3 = aq.id(R.id.regbasic_num_staff_txt).getTextView();
        textView3.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_num_staff_spinner, 1);

        //HK Branches
        aq.id(R.id.regbasic_num_hk_branch_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_num_hk_branch_txt, getResString(R.string.comm_cominfo_branchnum));
        aq.layoutWeight(R.id.regbasic_num_hk_branch_txt, 1);
        TextView textView4 = aq.id(R.id.regbasic_num_hk_branch_txt).getTextView();
        textView4.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_num_hk_branch_spinner, 1);

        //Outside HK Branch
        aq.id(R.id.regbasic_num_branch_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_num_branch_txt, getResString(R.string.comm_cominfo_branchhk));
        aq.layoutWeight(R.id.regbasic_num_branch_txt, 1);
        TextView textView5 = aq.id(R.id.regbasic_num_branch_txt).getTextView();
        textView5.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_num_branch_spinner, 1);

        aq.id(R.id.regbasic_primary_title_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_primary_title_txt, getResString(R.string.comm_cominfo_name_title));
        aq.layoutWeight(R.id.regbasic_primary_title_txt, 1);
        TextView textView6 = aq.id(R.id.regbasic_primary_title_txt).getTextView();
        textView6.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_primary_title_spinner, 1);

        aq.id(R.id.regbasic_secondary_title_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_secondary_title_txt, getResString(R.string.comm_cominfo_name_title));
        aq.layoutWeight(R.id.regbasic_secondary_title_txt, 1);
        TextView textView7 = aq.id(R.id.regbasic_secondary_title_txt).getTextView();
        textView7.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_secondary_title_spinner, 1);
        setQuickAction();

        aq.norm2TxtBtns(R.id.billinfolts_btn_left, R.id.billinfolts_btn_right,
                getResString(R.string.CFMF_NOTCFM), getResString(R.string.CTAF_SAVE));

        aq.id(R.id.billinfolts_btn_left).clicked(this, "onClick");
        aq.id(R.id.billinfolts_btn_right).clicked(this, "onClick");

        //show data
        prepareAccData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regbasic_industry_spinner:
                Utils.closeSoftKeyboard(getActivity(), v);
                mQAIndustry.show(v, aq.id(R.id.regbasic_industry_spinner).getView());
                mQAIndustry.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_num_staff_spinner:
                Utils.closeSoftKeyboard(getActivity(), v);
                mQAStaffNum.show(v, aq.id(R.id.regbasic_num_staff_spinner).getView());
                mQAStaffNum.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_num_hk_branch_spinner:
                Utils.closeSoftKeyboard(getActivity(), v);
                mQAHKBranches.show(v, aq.id(R.id.regbasic_num_hk_branch_spinner).getView());
                mQAHKBranches.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_num_branch_spinner:
                Utils.closeSoftKeyboard(getActivity(), v);
                mQABranches.show(v, aq.id(R.id.regbasic_industry_spinner).getView());
                mQABranches.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_primary_title_spinner:
                Utils.closeSoftKeyboard(getActivity(), v);
                mQAPrimaryTitle.show(v, aq.id(R.id.regbasic_primary_title_spinner).getView());
                mQAPrimaryTitle.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_secondary_title_spinner:
                Utils.closeSoftKeyboard(getActivity(), v);
                mQASecondaryTitle.show(v, aq.id(R.id.regbasic_secondary_title_spinner).getView());
                mQASecondaryTitle.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.billinfolts_btn_left:
                Utils.closeSoftKeyboard(getActivity(), v);
                onUpdateCompCallback.onDismissUpdateCompanuInfo();
                break;
            case R.id.billinfolts_btn_right:
                Utils.closeSoftKeyboard(getActivity(), v);
                saveCompanyInfo();
                break;
        }
    }

    private void setQuickAction() {
        aIINdustry = new ActionItem[21];
        aIINdustry[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_ind_asso));
        aIINdustry[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_ind_bank));
        aIINdustry[2] = new ActionItem(2, getString(R.string.comm_cominfo_select_ind_business));
        aIINdustry[3] = new ActionItem(3, getString(R.string.comm_cominfo_select_ind_const));
        aIINdustry[4] = new ActionItem(4, getString(R.string.comm_cominfo_select_ind_edu));
        aIINdustry[5] = new ActionItem(5, getString(R.string.comm_cominfo_select_ind_finance));
        aIINdustry[6] = new ActionItem(6, getString(R.string.comm_cominfo_select_ind_gov));
        aIINdustry[7] = new ActionItem(7, getString(R.string.comm_cominfo_select_ind_health));
        aIINdustry[8] = new ActionItem(8, getString(R.string.comm_cominfo_select_ind_hotel));
        aIINdustry[9] = new ActionItem(9, getString(R.string.comm_cominfo_select_ind_insurance));
        aIINdustry[10] = new ActionItem(10, getString(R.string.comm_cominfo_select_ind_manuf));
        aIINdustry[11] = new ActionItem(11, getString(R.string.comm_cominfo_select_ind_personal));
        aIINdustry[12] = new ActionItem(12, getString(R.string.comm_cominfo_select_ind_estate));
        aIINdustry[13] = new ActionItem(13, getString(R.string.comm_cominfo_select_ind_restaurant));
        aIINdustry[14] = new ActionItem(14, getString(R.string.comm_cominfo_select_ind_retail));
        aIINdustry[15] = new ActionItem(15, getString(R.string.comm_cominfo_select_ind_IT));
        aIINdustry[16] = new ActionItem(16, getString(R.string.comm_cominfo_select_ind_trade));
        aIINdustry[17] = new ActionItem(17, getString(R.string.comm_cominfo_select_ind_transport));
        aIINdustry[18] = new ActionItem(18, getString(R.string.comm_cominfo_select_ind_utility));
        aIINdustry[19] = new ActionItem(19, getString(R.string.comm_cominfo_select_ind_wholesale));
        aIINdustry[20] = new ActionItem(20, getString(R.string.comm_cominfo_select_ind_other));


        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        int screenWidth = wm.getDefaultDisplay().getWidth();

        mQAIndustry = new QuickAction(getActivity(), screenWidth / 2, 0);
        for (int i = 0; i < aIINdustry.length; i++) {
            mQAIndustry.addActionItem(aIINdustry[i]);
        }

        //setup the action item click listener
        mQAIndustry.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComIndustryPopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });

        aq.id(R.id.regbasic_industry_spinner).clicked(this, "onClick");

        ActionItem[] aINumberStaff = new ActionItem[6];
        aINumberStaff[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_staffnum_1));
        aINumberStaff[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_staffnum_2));
        aINumberStaff[2] = new ActionItem(2, getString(R.string.comm_cominfo_select_staffnum_3));
        aINumberStaff[3] = new ActionItem(3, getString(R.string.comm_cominfo_select_staffnum_4));
        aINumberStaff[4] = new ActionItem(4, getString(R.string.comm_cominfo_select_staffnum_5));
        aINumberStaff[5] = new ActionItem(5, getString(R.string.comm_cominfo_select_staffnum_6));

        mQAStaffNum = new QuickAction(getActivity(), screenWidth / 2, 0);
        for (int i = 0; i < aINumberStaff.length; i++) {
            mQAStaffNum.addActionItem(aINumberStaff[i]);
        }
        //setup the action item click listener
        mQAStaffNum.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComStaffNumPopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });
        //aq.popOverInputView(R.id.regbasic_num_staff_spinner,aINumberStaff[0].getTitle());
        aq.id(R.id.regbasic_num_staff_spinner).clicked(this, "onClick");

        ActionItem[] aIHkBranches = new ActionItem[4];
        aIHkBranches[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_branchnum_1));
        aIHkBranches[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_branchnum_2));
        aIHkBranches[2] = new ActionItem(2, getString(R.string.comm_cominfo_select_branchnum_3));
        aIHkBranches[3] = new ActionItem(3, getString(R.string.comm_cominfo_select_branchnum_4));

        mQAHKBranches = new QuickAction(getActivity(), screenWidth / 2, 0);
        for (int i = 0; i < aIHkBranches.length; i++) {
            mQAHKBranches.addActionItem(aIHkBranches[i]);
        }

        aIBranches = new ActionItem[2];
        aIBranches[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_yes));
        aIBranches[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_no));

        mQABranches = new QuickAction(getActivity(), screenWidth / 2, 0);
        for (int i = 0; i < aIBranches.length; i++) {
            mQABranches.addActionItem(aIBranches[i]);
        }

        //setup the action item click listener
        mQAHKBranches.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComHKBranchPopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });
        //setup the action item click listener
        mQABranches.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComBrachePopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });
        aq.id(R.id.regbasic_num_hk_branch_spinner).clicked(this, "onClick");
        aq.id(R.id.regbasic_num_branch_spinner).clicked(this, "onClick");

        aITitles = new ActionItem[3];
        aITitles[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_title_mr));
        aITitles[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_title_mrs));
        aITitles[2] = new ActionItem(2, getString(R.string.comm_cominfo_select_title_ms));

        mQAPrimaryTitle = new QuickAction(getActivity(), screenWidth / 2, 0);
        for (int i = 0; i < aITitles.length; i++) {
            mQAPrimaryTitle.addActionItem(aITitles[i]);
        }

        mQASecondaryTitle = new QuickAction(getActivity(), screenWidth / 2, 0);
        for (int i = 0; i < aITitles.length; i++) {
            mQASecondaryTitle.addActionItem(aITitles[i]);
        }

        //setup the action item click listener
        mQAPrimaryTitle.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComPTitlePopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });
        mQASecondaryTitle.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComSTitlePopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });

        aq.id(R.id.regbasic_primary_title_spinner).clicked(this, "onClick");
        aq.id(R.id.regbasic_secondary_title_spinner).clicked(this, "onClick");
    }

    private void saveCompanyInfo() {
        AcMainCra acMainCra = new AcMainCra();
        acMainCra.setILoginId(callback.getSveeRec().loginId);
        acMainCra.setISveeRec(callback.getSveeRec().copyMe());

        BcifRec mBcifRec = bcifRec.copyMe();

        mBcifRec.custNm = regInputCompName.getInput();
        mBcifRec.industry = isValidSpinSelected(regComIndustryPopOver.getText()) ? INDUSTRY_VALUES[getIndex(aIINdustry, regComIndustryPopOver.getText())] : "";
        mBcifRec.nuStaff = isValidSpinSelected(regComStaffNumPopOver.getText()) ? regComStaffNumPopOver.getText() : "";
        mBcifRec.nuPresence = isValidSpinSelected(regComHKBranchPopOver.getText()) ? regComHKBranchPopOver.getText() : "";
        mBcifRec.xborder = isValidSpinSelected(regComBrachePopOver.getText()) ? IS_BRANCH_OS_HK_VALUES[getIndex(aIBranches, regComBrachePopOver.getText())] : "";
        //primary contact
        mBcifRec.priCtName = regInputPCName.getInput();
        mBcifRec.priCtTitle = isValidSpinSelected(regComPTitlePopOver.getText()) ? TITLES_VALUES[getIndex(aITitles, regComPTitlePopOver.getText())] : "";
        mBcifRec.priCtMail = regInputPCEmail.getInput();
        mBcifRec.priCtJobtitle = regInputPCJobTitle.getInput();
        mBcifRec.priCtTel = regInputPCTelNum.getInput();
        mBcifRec.priCtMob = regInputPCMobileNum.getInput();
        //secondary contact
        mBcifRec.secCtName = regInputSCName.getInput();
        mBcifRec.secCtTitle = isValidSpinSelected(regComSTitlePopOver.getText()) ? TITLES_VALUES[getIndex(aITitles, regComSTitlePopOver.getText())] : "";
        mBcifRec.secCtMail = regInputSCEmail.getInput();
        mBcifRec.secCtJobtitle = regInputSCJobTitle.getInput();
        mBcifRec.secCtTel = regInputSCTelNum.getInput();
        mBcifRec.secCtMob = regInputSCMobileNum.getInput();

        acMainCra.setIBcifRec(mBcifRec);

        Reply reply = mBcifRec.verifyCustNm();
        if (reply.isSucc()) reply = mBcifRec.verifyIndustry();
        if (reply.isSucc()) reply = mBcifRec.verifyNuStaff();
        if (reply.isSucc()) reply = mBcifRec.verifyNuPresence();
        if (reply.isSucc()) reply = mBcifRec.verifyXBorder();
        //primary contact
        if (reply.isSucc()) reply = mBcifRec.verifyPriCtName();
        if (reply.isSucc()) reply = mBcifRec.verifyPriCtTitle();
        if (reply.isSucc()) reply = mBcifRec.verifyPriCtMail();
        if (reply.isSucc()) reply = mBcifRec.verifyPriCtJobTitle();
        if (reply.isSucc()) reply = mBcifRec.verifyPriCtTel();
        if (reply.isSucc()) reply = mBcifRec.verifyPriCtMob();

        if (reply.isSucc()) reply = mBcifRec.verifySecCtName();
        if (reply.isSucc()) reply = mBcifRec.verifySecCtTitle();
        if (reply.isSucc()) reply = mBcifRec.verifySecCtMail();
        if (reply.isSucc()) reply = mBcifRec.verifySecCtJobTitle();
        if (reply.isSucc()) reply = mBcifRec.verifySecCtTel();
        if (reply.isSucc()) reply = mBcifRec.verifySecCtMob();


        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), reply));
        } else {
            APIsManager.doUpdProfile(null, me, acMainCra);
        }
    }

    private boolean isValidSpinSelected(String selected) {
        return !selected.equalsIgnoreCase(getResString(R.string.comm_cominfo_select_default));
    }

    private int getIndex(ActionItem[] actionItems, String text) {

        for (int i = 0; i < actionItems.length; i++) {
            if (actionItems[i].getTitle().equals(text)) {
                return i;
            }
        }
        return -1;
    }

    private int getIndex(String[] list, String text) {
        for (int i = 0; i < list.length; i++) {
            if (list[i].equals(text)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onSuccess(APIsResponse response) {
        displayDialog(getResString(R.string.PAMM_DONE));
        AcMainCra acMainCra = (AcMainCra) response.getCra();
        //Save data
        ClnEnv.getQualSvee().setSveeRec(acMainCra.getISveeRec());
        callback.setSveeRec(acMainCra.getISveeRec());

        ClnEnv.getLgiCra().getOQualSvee().setBcifRec(acMainCra.getIBcifRec().copyMe());
        callback.saveBcifRec(acMainCra.getIBcifRec().copyMe());
        bcifRec = callback.getBcifRec();

        firebaseSetting.onUpdateInfoEvent(EVENT_INFO);
        ((MyProfLoginMainFragment) getParentFragment()).openLoginidSubFrag();
    }

    @Override
    public void onFail(APIsResponse response) {
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else {
            DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), response.getReply()));
        }
    }

    public interface OnUpdateCompInfoListener {
        void onDismissUpdateCompanuInfo();

        void onShowUpdateCompInfo();
    }

}
