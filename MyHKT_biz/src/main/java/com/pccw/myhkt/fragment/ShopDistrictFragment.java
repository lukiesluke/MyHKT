package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTIndicator;
import com.pccw.myhkt.model.TabItem;

public class ShopDistrictFragment extends BaseShopFragment{
	private ShopDistrictFragment me;
	private AAQuery aq;
	private FragmentTransaction ft;
	private View thisView;
	private List<TabItem> tapItemList; 
	
	private HKTIndicator 	hktindicator;
	private ViewPager 		viewPager;
	private ShopDistrictPagerAdapter shopDistrictPagerAdapter;
	
	// Fragment
	private FragmentManager				fragmentManager;
	private ShopDistrictChildFragment 					shopDistrictChildFragmentHK;
	private ShopDistrictChildFragment 					shopDistrictChildFragmentKLN;
	private ShopDistrictChildFragment 					shopDistrictChildFragmentNT;
	
	private int 					extralinespace;
	private int 					gridlayoutPadding = 0;
	private int 					deviceWidth = 0;
	private int 					colWidth = 0;
	private int 					currentPage = 0;
	

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;

		TAG = "ShopDistrictFragment";

		
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		gridlayoutPadding = (int) getResources().getDimension(R.dimen.mainmenu_gridlayout_padding);
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		
		View fragmentLayout = inflater.inflate(R.layout.fragment_shopdistrict, container, false);
		thisView = fragmentLayout;
		
		
		return fragmentLayout;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		//Screen Tracker
		
		init();
	}
	
	@Override
	public void onResume(){
		super.onResume();
	}

	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}
	
	private void init() {
		if (debug) Log.i(TAG, "init" + currentPage);
		tapItemList = new ArrayList<TabItem>();
		tapItemList.add(new TabItem(R.drawable.shop_hki, getResString(R.string.myhkt_shop_hk), 0));
		tapItemList.add(new TabItem(R.drawable.shop_kl,  getResString(R.string.myhkt_shop_kln), 1));
		tapItemList.add(new TabItem(R.drawable.shop_nt,  getResString(R.string.myhkt_shop_nt), 2));
		tapItemList.add(new TabItem(-1, null, 5));
		fragmentManager = getChildFragmentManager();

		shopDistrictChildFragmentHK = new ShopDistrictChildFragment();
		shopDistrictChildFragmentKLN = new ShopDistrictChildFragment();
		shopDistrictChildFragmentNT = new ShopDistrictChildFragment();
		
		colWidth = (deviceWidth - extralinespace*2 ) / tapItemList.size(); 
		
		aq = new AAQuery(thisView);
		
		viewPager = (ViewPager)aq.id(R.id.shopdistrict_commonview).getView();
		shopDistrictPagerAdapter = new ShopDistrictPagerAdapter(getChildFragmentManager());
		viewPager.setAdapter(shopDistrictPagerAdapter);
		
		hktindicator = (HKTIndicator) aq.id(R.id.shopdistrict_indictaor).getView();
		aq.id(R.id.shopdistrict_indictaor).height(Math.max((int) getResources().getDimension(R.dimen.indicator_height) ,colWidth), false);
		hktindicator.setViewPager(viewPager);
		hktindicator.setCurrentItem(currentPage);
		hktindicator.initView(getActivity(), tapItemList, (int) getResources().getDimension(R.dimen.smalltext1size), getResources().getColor(R.color.hkt_textcolor),Color.TRANSPARENT, colWidth);
		hktindicator.setOnPageChangeListener(new OnPageChangeListener(){

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageSelected(int position) {
				//Refresh the MyProfLoginMainFragment when it is current 
				Utils.closeSoftKeyboard(getActivity());
				currentPage = position;
				if (position == 0) {				
					shopDistrictChildFragmentHK.refresh(0);
				}
				if (position == 1) {
					shopDistrictChildFragmentKLN.refresh(1);
				}
				if (position == 2) {
					shopDistrictChildFragmentNT.refresh(2);
				}
			}
		});
		
		selectPage(currentPage);
	}
	
	protected final void refreshData() {
		super.refreshData();
		
	}

	
	// View Pager Class
	private class ShopDistrictPagerAdapter extends FragmentStatePagerAdapter {
		private static final int NUM_PAGER_VIEWS = 3;

		public ShopDistrictPagerAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
		}

		@Override
		public int getCount() {
			return NUM_PAGER_VIEWS;
		}

		// Returns the fragment to display for that page
		@Override
		public Fragment getItem(int position) {
			if (debug) Log.i(TAG, "FS:" + position);
//			Bundle args = new Bundle();
			switch (position) {
			case 0: // Fragment # 0 - This will show FirstFragment
				return shopDistrictChildFragmentHK;
			case 1: // Fragment # 1 - This will show FirstFragment different title
				return shopDistrictChildFragmentKLN;
			case 2: // Fragment # 2 - This will show FirstFragment different title
				return shopDistrictChildFragmentNT;
			default:
				return null;
				//	            case 2:
				//	            	return new MembershipFragment();
				//	            case 3:
				//	            case 4:
				//	            case 5:
				//	            	return new MembershipFragment();
				//	            case 6:
				//	            case 7:
				//	            case 8:
				//	            case 9:
				//	            case 10:
				//	            	return new AsiaMilesFragment();
			}
		}
		
		@Override
		public Parcelable saveState()
		{
		    return null;
		}
	}
	
	private void selectPage(final int pos) {
		//force 1st page refresh: use runnable to make sure viewpager's view already instantiated
		viewPager.post(new Runnable(){
			@Override
		    public void run() {
				hktindicator.onPageSelected(pos);
		   	}
		});
	}


}
