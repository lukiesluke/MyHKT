package com.pccw.myhkt.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.BiifCra;
import com.pccw.dango.shared.cra.BinqCra;
import com.pccw.dango.shared.entity.Bill;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.g3entity.G3ServiceCallBarStatusDTO;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Tool;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.PPSActivity;
import com.pccw.myhkt.activity.TapNGoActivity;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.DetailBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.TwoBtnCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.mymob.MyMobileAccountHelper;
import com.pccw.myhkt.utils.Constant;
import com.pccw.myhkt.utils.FirebaseSetting;
import com.pccw.myhkt.utils.RuntimePermissionUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/************************************************************************
 * File : BillSumFragment.java
 * Desc : Bills and Summary
 * Name : BillSumFragment
 * by 	: 
 * Date : 
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 06/7/2017  Abdulmoiz Esmail	-Updated refresh methods, restrict getting of bills
 * 								if LOB is PCD and if bill agent show message, otherwise
 *  					 		get the bill	
 * 06/8/2017  Abdulmoiz Esmail  -Updated refresh methods, remove call GetPlanIMS AP, instead
 * 								get the PCD and billagt from callback
 * 15/6/2017  Abdulmoiz Esmail  -59316: PCD subscription under csl, setModuleId for PCD Plan & PCD Bill.
 * 16/6/2017  Abdulmoiz Esmail  -59684: Change Bill Payment reminder Alert Message Color
 *************************************************************************/

public class BillSumFragment extends BaseServiceFragment {
	private static BillSumFragment me;
	private View myView;
	private AAQuery aq;
	private SveeRec sveeRec = null;
	private RegInputItem 			inputName;
	private RegInputItem 			inputEmail;
	private RegInputItem 			inputMobNum;

	private LinearLayout			frame;
	private Button 					cancelBtn;
	private Button 					updateBtn;

	private String 					nameInput;
	private String 					emailInput;
	private String 					mobInput;
	private boolean isChangePwdMode = false;
	private String TAG = "BillSumFragment";
	private List<Bundle> 			bundleList;
	private List<Cell>				cellList;
	private CellViewAdapter 		cellViewAdapter;
	private FragmentTransaction ft;

	private String refNo;

	// Account Database Helper
	private SaveAccountHelper							saveAccountHelper;							// SQLite database for a new bill checkingF
	private MyMobileAccountHelper						myMobileAccountHelper;						// MyMobileAcct SQLite database

	private BiifCra biifCra;
	private BinqCra binqCra;
	private AddOnCra addOnCra;
	private String billDate;

	// Thin bill
	private boolean isThinBill = false;
	private int											clickBill;
	private int											chkBillcount;

	//payment method urls
	private final String payMethUrl1010Zh = "http://1010.com.hk/c/payment_method_e";
	private final String payMethUrl1010En = "http://1010.com.hk/e/payment_method_e";
	private final String payMethUrlIOIZh =  "http://1010.com.hk/c/payment_method_m";
	private final String payMethUrlIOIEn =  "http://1010.com.hk/e/payment_method_m";
	private final String payMethUrlO2FZh = "http://e.hkcsl.com/paymentmethodc";
	private final String payMethUrlO2FEn = "http://e.hkcsl.com/paymentmethodc-e";
	private final String payMethUrlMOBZh = "http://e.hkcsl.com/paymentmethod";
	private final String payMethUrlMOBEn = "http://e.hkcsl.com/paymentmethodh-e";
	private final String payMethUrlPCDZh = "https://cs.netvigator.com/bill/payment_methods_c.html";
	private final String payMethUrlPCDEn = "https://cs.netvigator.com/bill/payment_methods_e.html";
	private final String payMethUrlLTSZh = "http://www.pccw.com/Customer+Service/Consumer/Billing+Inquiry/Payment+Methods?language=zh_HK";
	private final String payMethUrlLTSEn = "http://www.pccw.com/Customer+Service/Consumer/Billing+Inquiry/Payment+Methods?language=en_US";
	private final String payMethUrlTVZh = "http://nowtv.now.com/myaccount?lang=zh";
	private final String payMethUrlTVEn = "http://nowtv.now.com/myaccount?&lang=en";
	private FirebaseSetting firebaseSetting;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_billsum, container, false);
		myView = fragmentLayout;
		initData();
		firebaseSetting = new FirebaseSetting(this.getContext());
		return fragmentLayout;
	}

	protected void initUI() {

	}

	/*
	 * Update: Moiz:060817: Check if the LOB is PCD and SubnRec.billagt is true
	 * if true, show the message bill by agent, otherwise doGetBill() 
	 */
	@Override
	protected final void refreshData() {

		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
			doGATracker();
			if(callback_main.getSubnRec().lob.equalsIgnoreCase("PCD") && callback_main.getSubnRec().isBillByAgent()){ 
				//Show message 
				showPcdBillByAgent();
			} else {
				if (((BillFragment)getParentFragment()).isBillSum) {
					if(getResources().getBoolean(R.bool.DEBUGGRQ) && !RuntimePermissionUtil.isWriteExternalStoragePermissionGranted(getActivity())) {
						if(ClnEnv.getPref(getActivity(), Constant.FILE_STORAGE_PERMISSION_DENIED, false)) {
							displayDialog(getActivity().getString(R.string.permission_denied_setting_enabled));
						} else {
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
								RuntimePermissionUtil.requestFileStoragePermissionForLogs(getActivity());
							}
						}
					} else {
						doGetBill();
					}
				}
			}
		}
	}

	/*
	 * Update: Moiz:060817: Check if the LOB is PCD and SubnRec.billagt is true
	 * if true, show the message bill by agent, otherwise doGetBill() 
	 */
	@Override
	public void refresh() {
		super.refresh();
		doGATracker();
		
		if(callback_main.getSubnRec().lob.equalsIgnoreCase("PCD") && callback_main.getSubnRec().isBillByAgent()){ 
			//show the message
//			Log.d(TAG, "Module Id: " + getResString(R.string.MODULE_PCD_BILL_4MOBCS));
//			callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_BILL_4MOBCS));
			showPcdBillByAgent();
		} else {
			if (((BillFragment)getParentFragment()).isBillSum) {
	//						if (callback_main.IsZombie() && (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F)) {
	//							initBasicUI();
	//							initBillList();
	//							cellViewAdapter.setView(frame, cellList);
	//						} else {
				doGetBill();
	//						}
			}
		}
	}

	protected void initData(){
		super.initData();
	}

	protected void initBasicUI() {
		bundleList = new ArrayList<Bundle>();
		cellList = new ArrayList<Cell>();
		cellViewAdapter = new CellViewAdapter(this);
		aq = new AAQuery(myView);

		aq.id(R.id.fragment_bill_layout).backgroundColorId(R.color.white);
		frame = (LinearLayout) aq.id(R.id.fragment_bill_listview).getView();

		// Header
		BigTextCell cell1;
		// switch(callback.getAcctAgent().getLobType()) {
		// case R.string.CONST_LOB_MOB:

		String accNo = Tool.formatAcctNum(callback_main.getAcctAgent().getAcctNum());
		// String accNo = Tool.formatAcctNum(callback_main.getAcctAgent().getAcctNum()) + (callback_main.IsZombie()?"*":"");
		cell1 = new BigTextCell(getResString(R.string.myhkt_summary_accountNo) + " ", accNo);
		cell1.setTopMargin(basePadding);
		cell1.setLeftMargin(basePadding);
		cell1.setRightMargin(basePadding);
		cell1.setContentColorId(R.color.hkt_textcolor);
		cellList.add(cell1);

		cellViewAdapter.setView(frame, cellList);
	}

	/***************
	 * Listeners
	 *
	 ******************/
	public void onUpdateClick(View v) {
		((BillFragment) getParentFragment()).billInfoLTSSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1;
		((BillFragment) getParentFragment()).openBillInfoFrag();
	}

	public void onPayClick(View v) {
		// Event Tracker
		String url = "";
		if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
			url = isZh ? payMethUrlLTSZh : payMethUrlLTSEn;
		} else if (callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_IMS) {
			url = isZh ? payMethUrlPCDZh : payMethUrlPCDEn;
		} else if (callback_main.getLob() == R.string.CONST_LOB_TV) {
			url = isZh ? payMethUrlTVZh : payMethUrlTVEn;
		} else if (callback_main.getLob() == R.string.CONST_LOB_MOB) {
			url = isZh ? payMethUrlMOBZh : payMethUrlMOBEn;
		} else if (callback_main.getLob() == R.string.CONST_LOB_1010) {
			url = isZh ? payMethUrl1010Zh : payMethUrl1010En;
		} else if (callback_main.getLob() == R.string.CONST_LOB_IOI) {
			url = isZh ? payMethUrlIOIZh : payMethUrlIOIEn;
		} else if (callback_main.getLob() == R.string.CONST_LOB_O2F) {
			url = isZh ? payMethUrlO2FZh : payMethUrlO2FEn;
		} else {
			//handling
		}

		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		startActivity(browserIntent);
	}

	public void onUpdatePayMethodClick(View v) {
		((BillFragment) getParentFragment()).billInfoLTSSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2;
		((BillFragment) getParentFragment()).openBillInfoFrag();
	}

	public void onPPSClick(View v) {
		if (debug) Log.e("PPS ONCLICK", "PPS ONCLICK TRACKER");
		// Event Tracker

		Intent intent = new Intent(this.getActivity(), PPSActivity.class);
		Bundle bundle = new Bundle();
		//				bundle.putSerializable("ACCTAGENT", callback_main.getAcctAgent());	
		bundle.putInt("LOB", callback_main.getLob());
		if (callback_main.getAcctAgent().getLobType() == R.string.CONST_LOB_LTS) {
			bundle.putInt("LTSTYPE", callback_main.getLtsType());
		}
		//insert amount
		switch (callback_main.getLob()) {
			case R.string.CONST_LOB_MOB:
			case R.string.CONST_LOB_IOI:
			case R.string.CONST_LOB_1010:
			case R.string.CONST_LOB_O2F:
				bundle.putInt("LOB", callback_main.getLob());
				bundle.putSerializable("ACCTNUM", callback_main.getAcctAgent().getAcctNum());
				if (binqCra.getOG3OutstandingBalanceDTO() != null) {
					bundle.putString("AMOUNT", Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal(), 2)));
				}
				break;
			case R.string.CONST_LOB_PCD:
			case R.string.CONST_LOB_TV:
			case R.string.CONST_LOB_IMS:
				if (binqCra.getOBillPreview() != null) {
					bundle.putString("ACCTNUM", binqCra.getOBillPreview().getReceiptAcctNo());
					bundle.putString("AMOUNT", String.format("$%s", binqCra.getOBillPreview().getCurrentBalance()));
				}
				break;
			case R.string.CONST_LOB_LTS:
				Bill[] billary = binqCra.getOBillList().getOBillAry();
				bundle.putString("ACCTNUM", callback_main.getAcctAgent().getAcctNum());
				if (billary.length > 0) {
					bundle.putString("AMOUNT", Utils.convertStringToPrice(billary[0].getInvAmt().toString()));
				}
				break;
		}

		intent.putExtras(bundle);
		startActivity(intent);
		me.getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
	}

	public void onSevenClick(View v) {
		/*
		// Event Tracker
		Tracker tracker = GoogleAnalytics.getInstance(me.getActivity()).getTracker(ClnEnv.getTrackerId(getActivity(), false));
		UncaughtExceptionHandler myHandler = new ExceptionReporter(tracker, GAServiceManager.getInstance(), Thread.getDefaultUncaughtExceptionHandler(), getActivity().getBaseContext());
		Thread.setDefaultUncaughtExceptionHandler(myHandler);
		tracker.send(MapBuilder
				.createEvent(Utils.getString(me.getActivity(), R.string.CONST_GA_CATEGORY_USERLV), Utils.getString(me.getActivity(), R.string.CONST_GA_ACTION_BTN), Utils.getString(me.getActivity(), R.string.CONST_GA_LABEL_711) + " (" + callback_main.getLobString() +")", null).build());

		String serviceType = "";
		String displayDate = "";
		double amtdouble = 0.0;
		if (binqCra != null) {
			switch(callback_main.getAcctAgent().getLobType()) {
			case R.string.CONST_LOB_MOB:
			case R.string.CONST_LOB_IOI:
				if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
					displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
				} else {
					displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
				}
				DialogHelper.create7ElevenDialog(me.getActivity(), callback_main.getLob(), Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_mob),
						displayDate, callback_main.getAcctAgent().getAcctNum(), Utils.getString(me.getActivity(), R.string.qrcode_bill_type_mobile),
						binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal());
				break;
			case R.string.CONST_LOB_PCD:
			case R.string.CONST_LOB_TV:
				serviceType = (callback_main.getLob() == R.string.CONST_LOB_PCD) ? Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_pcd) : Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_tv);
				displayDate = "";
				if (binqCra.getOBillPreview().getDisplayBillDate() != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
					displayDate = sdf.format(binqCra.getOBillPreview().getDisplayBillDate());
				} else {
					displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
				}
				// PCD Amount is in formatted string, strip , then convert back to double
				String amt = binqCra.getOBillPreview().getCurrentBalance().replaceAll("\\,", "");
				amtdouble = 0.0;
				try {
					amtdouble = Double.parseDouble(amt);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				DialogHelper.create7ElevenDialog(me.getActivity(), callback_main.getLob(), serviceType, displayDate, binqCra.getOBillPreview().getReceiptAcctNo(), 
						Utils.getString(me.getActivity(), R.string.qrcode_bill_type_pcd), amtdouble);
				break;
			case R.string.CONST_LOB_LTS:				
				Bill[] billary = (Bill[]) binqCra.getOBillList().getOBillAry();
				String billDt = "";
				String billAmt = "";
				if (billary.length > 0 && !billary[0].getInvDate().equalsIgnoreCase("")) {
					// Displaying the first bill info as summary
					billDt = Tool.formatDate(billary[0].getInvDate(), "dd/MM/yyyy");
					billAmt = billary[0].getInvAmt().toString();
				} else {
					billDt = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
				}
				// Bill List Amount became string in new MyHKT, converting to double for now, NEED CONFIRMATION // TODO
				amt = billAmt.replaceAll("\\,", "");
				amtdouble = 0.0;
				try {
					amtdouble = Double.parseDouble(amt);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				DialogHelper.create7ElevenDialog(me.getActivity(), callback_main.getLob(), Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_lts),
						billDt, callback_main.getAcctAgent().getAcctNum(), 
						Utils.getString(me.getActivity(), R.string.qrcode_bill_type_lts), amtdouble);
				break;
			case R.string.CONST_LOB_1010:
			case R.string.CONST_LOB_O2F:
				serviceType = (callback_main.getLob() == R.string.CONST_LOB_1010) ? Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_1010) : Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_mob);
				displayDate = "";
				if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
					displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
				} else {
					displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
				}
				DialogHelper.create7ElevenDialog(me.getActivity(), callback_main.getLob(), serviceType, displayDate, 
						callback_main.getAcctAgent().getAcctNum(), Utils.getString(me.getActivity(), R.string.qrcode_billtype_1010), amtdouble);
				break;
			}
		} */
	}

	public void onTapNGoClick(View v) {
		//		String merchCode = "";
		//		//merch code
		Intent intent = new Intent(this.getActivity(), TapNGoActivity.class);
		Bundle bundle = new Bundle();

		String serviceType = "";
		String displayDate = "";
		double amtdouble = 0.0;
		if (binqCra != null) {
			switch (callback_main.getAcctAgent().getLobType()) {
				case R.string.CONST_LOB_MOB:
				case R.string.CONST_LOB_IOI:
					if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
						displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
					} else {
						displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
					}
					amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
					//pass code
					bundle.putInt("LOB", callback_main.getLob());
					bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
					bundle.putString("ACCTNUM", callback_main.getAcctAgent().getAcctNum());
					bundle.putString("BILLDATE", displayDate);
					bundle.putString("BILLTYPE", Utils.getString(Objects.requireNonNull(me.getActivity()), R.string.qrcode_bill_type_mobile));
					bundle.putDouble("AMOUNT", amtdouble);

					// DialogHelper.create7ElevenDialog(me.getActivity(), callback_main.getLob(), Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_mob),
					// displayDate, callback_main.getAcctAgent().getAcctNum(), Utils.getString(me.getActivity(), R.string.qrcode_bill_type_mobile),
					// binqCra.getOG3OutstandingBalanceDTO().getCurrOSBal());
					break;
				case R.string.CONST_LOB_PCD:
				case R.string.CONST_LOB_TV:
					serviceType = (callback_main.getLob() == R.string.CONST_LOB_PCD) ? Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_pcd) : Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_tv);
					displayDate = "";
					if (binqCra.getOBillPreview().getDisplayBillDate() != null) {
						SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
						displayDate = sdf.format(binqCra.getOBillPreview().getDisplayBillDate());
					} else {
						displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
					}
					// PCD Amount is in formatted string, strip , then convert back to double
					String amt = binqCra.getOBillPreview().getCurrentBalance().replaceAll("\\,", "");
					amtdouble = 0.0;
					try {
						amtdouble = Double.parseDouble(amt);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}

					// Pass code
					bundle.putInt("LOB", callback_main.getLob());
					bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
					bundle.putString("ACCTNUM", binqCra.getOBillPreview().getReceiptAcctNo());
					bundle.putString("BILLDATE", displayDate);
					bundle.putString("BILLTYPE", Utils.getString(me.getActivity(), R.string.qrcode_bill_type_pcd));
					bundle.putDouble("AMOUNT", amtdouble);
					break;
				case R.string.CONST_LOB_LTS:
					Bill[] billary = binqCra.getOBillList().getOBillAry();
					String billDt = "";
					String billAmt = "";
					if (billary.length > 0 && !billary[0].getInvDate().equalsIgnoreCase("")) {
						// Displaying the first bill info as summary
						billDt = Tool.formatDate(billary[0].getInvDate(), "dd/MM/yyyy");
						billAmt = billary[0].getInvAmt().toString();
					} else {
						billDt = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
					}
					// Bill List Amount became string in new MyHKT, converting to double for now, NEED CONFIRMATION // TODO
					amt = billAmt.replaceAll("\\,", "");
					amtdouble = 0.0;
					try {
						amtdouble = Double.parseDouble(amt);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					//
					//pass code
					bundle.putInt("LOB", callback_main.getLob());
					bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
					bundle.putString("ACCTNUM", callback_main.getAcctAgent().getAcctNum());
					bundle.putString("BILLDATE", billDt);
					bundle.putString("BILLTYPE", Utils.getString(me.getActivity(), R.string.qrcode_bill_type_lts));
					bundle.putDouble("AMOUNT", amtdouble);
					break;
				case R.string.CONST_LOB_1010:
				case R.string.CONST_LOB_O2F:
					serviceType = (callback_main.getLob() == R.string.CONST_LOB_1010) ? Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_1010) : Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_mob);
					displayDate = "";
					if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
						displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
					} else {
						displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
					}

					amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
					//pass code
					bundle.putInt("LOB", callback_main.getLob());
					bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
					bundle.putString("ACCTNUM", callback_main.getAcctAgent().getAcctNum());
					bundle.putString("BILLDATE", displayDate);
					bundle.putString("BILLTYPE", Utils.getString(me.getActivity(), R.string.qrcode_billtype_1010));
					bundle.putDouble("AMOUNT", amtdouble);
					break;
			}
			if (amtdouble > (double) 0) {
				intent.putExtras(bundle);
				startActivity(intent);
				me.getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
			} else {
				displayDialog(getString(R.string.myhkt_tapngo_invalid));
			}
		}
	}

	//Bill list onClick
	public void onClickBillIcon(View v) {
		Bill bill = (Bill) v.getTag();
		if (bill != null) {
			Log.d("lwg", "Bill.getInvDate(): " + bill.getInvDate());
			Log.d("lwg", "Bill.getWithheld(): " + bill.getWithheld());
			isThinBill = bill.getWithheld().trim().length() > 0;
		}

		if (isThinBill) {
			DialogHelper.createSimpleDialog(me.getActivity(), getString(R.string.myhkt_holdbill));
		} else {
			clickBill = 0;
			chkBillcount = 0;
			billDate = Objects.requireNonNull(bill).getInvDate();
			doCheckBillStsLoop(billDate, false);
		}
	}

	private void doGetBill() {
		String loginId;
		// Account account;
		if (!ClnEnv.isLoggedIn()) {
			loginId = "";
		} else {
			loginId = ClnEnv.getQualSvee().getSveeRec().loginId;
		}

		BinqCra binqCra = new BinqCra();
		binqCra.setILoginId(loginId);
		binqCra.setISubnRec(callback_main.getSubnRec().copyMe());

		APIsManager.doGetBill(me, binqCra);
	}

	private void doCheckBillStsLoop(String billDate, Boolean isLastInLoop) {
		BinqCra chkStsBinqCra = new BinqCra();
		chkStsBinqCra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getQualSvee().getSveeRec().loginId : "");
		chkStsBinqCra.setISubnRec(callback_main.getSubnRec().copyMe());
		chkStsBinqCra.setIAcct(binqCra.getOBillList().getIAcct());
		chkStsBinqCra.setIBillDate(billDate);

		APIsManager.doCheckBillSts(me, chkStsBinqCra, isLastInLoop);
	}

	private void initMobBillSummary() {
		// Tracker
		// Tracker tracker = GoogleAnalytics.getInstance(getActivity()).getTracker(ClnEnv.getTrackerId(getActivity(), false));
		boolean isBillExist = false;
		String totalChg = "";
		String dueDate = null;
		String currentBal = "";

		//load data
		SmallTextCell cell2 = new SmallTextCell(getResString(R.string.myhkt_summary_mostrect), "");
		cell2.setLeftPadding(basePadding);
		cell2.setRightPadding(basePadding);
		if (callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV || callback_main.getLob() == R.string.CONST_LOB_IMS) {

		} else {
			cellList.add(cell2);
		}

		Cell cell1a = null; // receipt acct no (PCDTV)
		SmallTextCell cell3 = null; //total charge/ amt due(LTS)/ latest bill amt(PCDTV)/ no bill sum remarks
		SmallTextCell cell4 = null; //due date, bill month(LTS)
		SmallTextCell cell5 = null; //current bal, total acct bal(CSIM)
		SmallTextCell cell6 = null; //overdue amt(CSIM)
		SmallTextCell cell6a = null; //Service status (eg. "Suspended")
		SmallTextCell cell6b = null; //Service status remarks
//		DetailBtnCell cell7 = null; // payment method button
		SmallTextCell cell7 = null; // payment method button
		SmallTextCell cell7a = null; // Only show "Pay Bills: " 
		TwoBtnCell cell8 = null; // 7-11 and PPS button
		TwoBtnCell cell8a = null; // tap n go button
		DetailBtnCell cell9 = null; // Update bill address button 
		SmallTextCell cell9a = null; // Only show "Recent Bills: " 
		DetailBtnCell cell10 = null; //  Update payment method button

		String[] clickAry8;
		int[] resAry8;

		//mandatory cells
		Cell cellLine = new Cell(Cell.LINE); //seperator line
		cellLine.setLeftPadding(basePadding);
		cellLine.setRightPadding(basePadding);
		String[] clickAry7 = {"onPayClick"};
		String[] clickAry9 = {"onUpdateClick"}; //bill info button
		String[] clickAryEmpty = {""};

		if (!callback_main.IsZombie()) {
			cell9 = new DetailBtnCell(getResString(R.string.myhkt_summary_recentbill), getResString(R.string.comm_bill_billinfo_btn), getBtnImage(), clickAry9);
		} else {
			cell9a = new SmallTextCell(getResString(R.string.myhkt_summary_recentbill), "", R.color.black);
		}

		 if (isThinBill()) {
			 cell3 = new SmallTextCell(getResString(R.string.myhkt_holdbill), "");			
		 } else {			 
			//prepare cell by LOB
			switch(callback_main.getLob()) {
			case R.string.CONST_LOB_MOB:
			case R.string.CONST_LOB_IOI:
				callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_MOB_MM_BILL : R.string.MODULE_MOB_BILL));

				if (binqCra != null) {
					if (binqCra.getOG3OutstandingBalanceDTO() != null) {
						totalChg = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getCurrOSBal(), 2));
						cell3 = new SmallTextCell(getResString(R.string.bill_total_charge), totalChg);
						
						//If dute Date is null , dueDate item will not show		
						dueDate = binqCra.getOG3OutstandingBalanceDTO().getPymtDueDate() != null ?
								binqCra.getOG3OutstandingBalanceDTO().getPymtDueDate().toString() : "";
						if (dueDate != null && !dueDate.equals("")) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S", Locale.ENGLISH);
							Date d;
							try {
								d = sdf.parse(dueDate);
								sdf = new SimpleDateFormat("dd/MM/yyyy");
								dueDate = sdf.format(d);
							} catch (Exception e) {
								dueDate = getResString(R.string.summary_not_applicable);
							}
							cell4 = new SmallTextCell(getResString(R.string.summary_due_date), dueDate);
						}
						
						currentBal = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal(), 2));
						cell5 = new SmallTextCell(getResString(R.string.summary_current_balance), currentBal);
						
						//Service Status					
						if (binqCra.getOG3ServiceCallBarStatusWS() != null) {
							G3ServiceCallBarStatusDTO g3ServiceCallBarStatusDTO = binqCra.getOG3ServiceCallBarStatusWS().getG3ServiceCallBarStatusDTO();
							// Service Status
							if ("Y".equalsIgnoreCase(g3ServiceCallBarStatusDTO.getShowServiceStatus()) && g3ServiceCallBarStatusDTO != null) {
								if ("zh".equalsIgnoreCase(ClnEnv.getAppLocale(getActivity().getBaseContext()))) {
									cell6a = new SmallTextCell(getResString(R.string.summary_service_status), g3ServiceCallBarStatusDTO.getServiceStatusChi());
								} else {
									cell6a = new SmallTextCell(getResString(R.string.summary_service_status), g3ServiceCallBarStatusDTO.getServiceStatusEng());
								}
							}
							
							// Alert Message
							if ("Y".equalsIgnoreCase(g3ServiceCallBarStatusDTO.getHasAlertMessage()) && g3ServiceCallBarStatusDTO != null) {
								boolean isZh = "zh".equalsIgnoreCase(ClnEnv.getAppLocale(getActivity().getBaseContext()));
								if ("1".equalsIgnoreCase(g3ServiceCallBarStatusDTO.getAlertLevel())) {
									cell6b = new SmallTextCell(isZh ? g3ServiceCallBarStatusDTO.getAlertMessageChi() : g3ServiceCallBarStatusDTO.getAlertMessageEng(), "", R.color.blue_alert_message);
								} else if ("2".equalsIgnoreCase(g3ServiceCallBarStatusDTO.getAlertLevel())) {
									cell6b = new SmallTextCell(isZh ? g3ServiceCallBarStatusDTO.getAlertMessageChi() : g3ServiceCallBarStatusDTO.getAlertMessageEng(),"", R.color.red_alert_message);
								} else {
									cell6b = new SmallTextCell(isZh ? g3ServiceCallBarStatusDTO.getAlertMessageChi() : g3ServiceCallBarStatusDTO.getAlertMessageEng(),"", R.color.black);
								}
							}
						}
						//service status end
						
						clickAry8 = new String[]{"onPPSClick"/*, "onSevenClick"*/};
						resAry8 = new int[]{R.drawable.pps_small/*, R.drawable.seven_small*/};
						//cell7 = new DetailBtnCell(getResString(R.string.myhkt_summary_paybill), getResString(R.string.myhkt_summary_paymentmethod), getBtnImage(), clickAry7);
						cell7 = new SmallTextCell(getResString(R.string.myhkt_summary_paybill), "", R.color.black);
						cell8 = new TwoBtnCell("","", R.drawable.pps_small, 0, clickAry8);
					}
				}
				break;
			case R.string.CONST_LOB_1010:
			case R.string.CONST_LOB_O2F:
				if (callback_main.getAcctAgent().getLobType() == R.string.CONST_LOB_1010) {
					callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_101_MM_BILL : R.string.MODULE_101_BILL));
				} else {
					callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_O2F_MM_BILL : R.string.MODULE_O2F_BILL));
				}

				if (binqCra != null) {
					if (binqCra.getOG3OutstandingBalanceDTO() != null) {
						String overDueAmt = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getOverdueAmt(), 2));
						cell6 = new SmallTextCell(getResString(R.string.myhkt_summary_overdue_amount), overDueAmt);
						
						totalChg = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getCurrOSBal(), 2));
						cell3 = new SmallTextCell(getResString(R.string.csl_current_amount), totalChg);
						
						//If dute Date is null , dueDate item will not show		
						dueDate = binqCra.getOG3OutstandingBalanceDTO().getPymtDueDate() != null ?
								binqCra.getOG3OutstandingBalanceDTO().getPymtDueDate().toString() : "";
						if (dueDate != null && !dueDate.equals("")) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S", Locale.ENGLISH);
							Date d;
							try {
								d = sdf.parse(dueDate);
								sdf = new SimpleDateFormat("dd/MM/yyyy");
								dueDate = sdf.format(d);
							} catch (Exception e) {
								dueDate = getResString(R.string.summary_not_applicable);
							}
							cell4 = new SmallTextCell(getResString(R.string.summary_due_date), dueDate);
						}						
						
						currentBal = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal(), 2));						
						cell5 = new SmallTextCell(getResString(R.string.csl_total_balance), currentBal);
						//20170320 Combine Tap&Go button and PPS button on the same row  
						//						cell7 = new DetailBtnCell(getResString(R.string.myhkt_summary_paybill), getResString(R.string.myhkt_summary_paymentmethod), getBtnImage(), clickAry7);
						clickAry8 = new String[]{"onPPSClick"/*, "onTapNGoClick"*/};
						resAry8 = new int[]{R.drawable.pps_small};
						//no pps or update billinfo allowed for T / V account type  //postponed
						//						if (!("T".equals(callback_main.getAcctAgent().getAcctType()) || "V".equals(callback_main.getAcctAgent().getAcctType()))) {
						//cell7 = new DetailBtnCell(getResString(R.string.myhkt_summary_paybill), getResString(R.string.myhkt_summary_paymentmethod), getBtnImage(), clickAry7);
						cell7 = new SmallTextCell(getResString(R.string.myhkt_summary_paybill), "", R.color.black);
						cell8 = new TwoBtnCell("","", R.drawable.pps_small, 0, clickAry8);
					}
				}
				break;
			case R.string.CONST_LOB_PCD:
			case R.string.CONST_LOB_TV:
			case R.string.CONST_LOB_IMS:
				if (callback_main.getAcctAgent().getLobType() == R.string.CONST_LOB_PCD) {
					//IF LOB is PCD and isBillByAgent is true use MODULE_PCD_BILL_4MOBCS otherwise MODULE_PCD_BILL
					if(callback_main.getSubnRec().lob.equalsIgnoreCase("PCD") && callback_main.getSubnRec().isBillByAgent()){
						Log.d(TAG, "Module ID: " + getResString(R.string.MODULE_PCD_BILL_4MOBCS));
						callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_BILL_4MOBCS));
					} else {
						callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_BILL));
					}
				} else {
					callback_livechat.setModuleId(getResString(R.string.MODULE_TV_BILL));
				}

				if (binqCra != null) {
					String reciptacctNum = null;
					if (binqCra.getOBillPreview().getReceiptAcctNo() != null) {
						reciptacctNum = Tool.formatAcctNum(binqCra.getOBillPreview().getReceiptAcctNo());
						cell1a = new SmallTextCell(getResString(R.string.pcd_summary_recacct_num), reciptacctNum);

					}
					if (binqCra.getOBillPreview().getBillAmount() != null) {
						totalChg = Utils.convertStringToPrice(binqCra.getOBillPreview().getBillAmount());
						cell3 = new SmallTextCell(getResString(R.string.myhkt_summary_latestbill), totalChg);
					}
					if (binqCra.getOBillPreview().getCurrentBalance() != null) {
						currentBal = Utils.convertStringToPrice(binqCra.getOBillPreview().getCurrentBalance());
						cell5 = new SmallTextCell(getResString(R.string.pcd_summary_current_balance), currentBal);
						clickAry8 = new String[]{"onPPSClick"/*, "onSevenClick"*/};
						cell7 = new SmallTextCell(getResString(R.string.myhkt_summary_paybill), "", R.color.black);
						cell8 = new TwoBtnCell("","", R.drawable.pps_small, 0, clickAry8);	
					}
					dueDate = binqCra.getOBillPreview().getDueDate();

					if (dueDate != null) {
						if (dueDate.toUpperCase().indexOf("DUE") >= 0) {
							dueDate = Utils.getString(me.getActivity(), R.string.due_on_immediate);
						} else {
							if (dueDate.indexOf("T") >= 0) {
								dueDate = dueDate.substring(0, dueDate.indexOf("T"));
								dueDate = Utils.toDateString(dueDate, "yyyy-MM-dd", "dd/MM/yyyy");
							} else {
								// convert Date format in MyHKT
								dueDate = Utils.toDateString(dueDate, "dd-MMM-yyyy", "dd/MM/yyyy");
							}
						}
						cell4 = new SmallTextCell(getResString(R.string.summary_due_date), dueDate);
					}
				}
				break;
			case R.string.CONST_LOB_LTS:
				callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_BILL));

				Bill[] billary = binqCra.getOBillList().getOBillAry();
				if (billary.length > 0 && !billary[0].getDueDate().equalsIgnoreCase("") && !isThinBill()) {
					totalChg = Utils.convertStringToPrice(billary[0].getInvAmt().toString());
					String billMonth = toLocaleDateString(billary[0].getInvDate(), Utils.getString(me.getActivity(), R.string.input_date_format), Utils.getString(me.getActivity(), R.string.bill_date_format));

					cell3 = new SmallTextCell(getResString(R.string.myhkt_summary_amtdue), totalChg);
					cell4 = new SmallTextCell(getResString(R.string.myhkt_summary_billmonth), billMonth);

					String dueDateTxt = billary[0].getDueDate();
					if (dueDateTxt != null && !"".equalsIgnoreCase(dueDateTxt) && !"99999999".equalsIgnoreCase(dueDateTxt)) {
						//						viewholder.billsumlts_summary_due_date.setVisibility(View.VISIBLE);
						if (dueDateTxt.toUpperCase().indexOf("DUE") >= 0 || "00000000".equalsIgnoreCase(dueDateTxt)) {
							dueDateTxt = Utils.getString(me.getActivity(), R.string.due_on_immediate);
						} else {
							dueDateTxt = Utils.toDateString(dueDateTxt, "yyyyMMdd", "dd/MM/yyyy");
						}
						//						viewholder.billsumlts_summary_due_date.setText(Utils.getString(me.getActivity(), R.string.pcd_summary_due_date) + dueDateTxt);
						cell5 = new SmallTextCell(getResString(R.string.pcd_summary_due_date), dueDateTxt);
					}

					//cell7 = new DetailBtnCell(getResString(R.string.myhkt_summary_paybill), getResString(R.string.myhkt_summary_paymentmethod), getBtnImage(), clickAry7);
					cell7 = new SmallTextCell(getResString(R.string.myhkt_summary_paybill), "", R.color.black);
					clickAry8 = new String[]{"onPPSClick"/*, "onSevenClick"*/};
					cell8 = new TwoBtnCell("","", R.drawable.pps_small,0, clickAry8);
				}
				break;
			}
			
			//zombie handling for cell7 and cell10
			if (callback_main.IsZombie()) {
				cell7 = null;
				cell7a = new SmallTextCell(getResString(R.string.myhkt_summary_paybill), "", R.color.black);
				cell10 = null;
			}
		}

		//add cell
		if (cell1a != null){ cell1a.setLeftPadding(basePadding); cell1a.setRightPadding(basePadding); cellList.add(cell1a); }
		if (cell6 != null) { cell6.setLeftPadding(basePadding); cell6.setRightPadding(basePadding); cellList.add(cell6); }
		if (cell3 != null) { cell3.setLeftPadding(basePadding); cell3.setRightPadding(basePadding); cellList.add(cell3); }
		if (cell4 != null) { cell4.setLeftPadding(basePadding); cell4.setRightPadding(basePadding); cellList.add(cell4); }
		if (cell5 != null) { cell5.setLeftPadding(basePadding); cell5.setRightPadding(basePadding); cellList.add(cell5); }
		if (cell6a != null) { cell6a.setLeftPadding(basePadding); cell6a.setRightPadding(basePadding); cellList.add(cell6a); }
		if (cell6b != null) { cell6b.setLeftPadding(basePadding); cell6b.setRightPadding(basePadding); cellList.add(cell6b); }
		cellList.add(cellLine);
		if (cell7 != null) { cell7.setLeftPadding(basePadding); cell7.setRightPadding(basePadding); cellList.add(cell7); } 
		if (cell7a != null) { cell7a.setLeftPadding(basePadding); cell7a.setRightPadding(basePadding); cellList.add(cell7a); }
		if (cell8 != null) { cell8.setLeftPadding(basePadding); cell8.setRightPadding(basePadding); cellList.add(cell8); }
		if (cell8a != null) { cell8a.setLeftPadding(basePadding); cell8a.setRightPadding(basePadding); cellList.add(cell8a); }
		if (cell9 != null) { cell9.setLeftPadding(basePadding); cell9.setRightPadding(basePadding); cellList.add(cell9); }
		if (cell9a != null) { cell9a.setLeftPadding(basePadding); cell9a.setRightPadding(basePadding); cellList.add(cell9a); }
		if (cell10 != null){ cell10.setLeftPadding(basePadding); cell10.setRightPadding(basePadding); cellList.add(cell10); }

		initBillList();

		cellViewAdapter.setView(frame, cellList);
	}

	private void initBillList() {
		Cell cellBillPdf = null; // bill pdfs
		Cell cell11a = null; //bill remark 1
		Cell cell11b = null; //bill remark 2

		//init bill list
		Bill[] bills = null;
		int emptyBillDtCount = 0;

		if (binqCra != null) {
			bills = binqCra.getOBillList().getOBillAry();
		}

		if (bills != null && bills.length > 0) {
			List<Bill> billList = new ArrayList<Bill>();
			for (Bill bill : bills) {
				if (!bill.getInvDate().isEmpty()) {
					billList.add(bill);
				} else emptyBillDtCount++;
			}
			// Sort bill InvDate into desc.
//			Collections.sort(billList, (bill, t1) -> t1.getInvDate().compareTo(bill.getInvDate()));

			// Convert arrayList clickList into Array ref:
			// https://www.geeksforgeeks.org/arraylist-array-conversion-java-toarray-methods/
			// Init bill images
			int[] imageAry = setBillImages(isNewBill(), billList);
			cellBillPdf = new Cell(Cell.BTNS9, R.color.white, imageAry, billList);

			// Update new bill icon
			try {
				if (binqCra != null && binqCra.getOBillList().getOBillAry() != null && binqCra.getOBillList().getOBillAry().length > 0) {
					if (ClnEnv.isMyMobFlag()) {
						myMobileAccountHelper = MyMobileAccountHelper.getInstance(getActivity());
						if (callback_main.getAcctAgent().getLatest_bill() == null || "".equals(callback_main.getAcctAgent().getLatest_bill()) || callback_main.CompareLastBillDate(callback_main.getAcctAgent().getLatest_bill(), binqCra.getOBillList().getOBillAry()[0].getInvDate())) {
							myMobileAccountHelper.updateLastBillDt(callback_main.getAcctAgent().getSrvNum(), binqCra.getOBillList().getOBillAry()[0].getInvDate());
							callback_main.getAcctAgent().setLatest_bill(binqCra.getOBillList().getOBillAry()[0].getInvDate());
						}
					} else {
						// Update bill indicator
						if (callback_main.getAcctAgent().isLive() && binqCra != null) {
							saveAccountHelper = SaveAccountHelper.getInstance(getActivity());
							if (saveAccountHelper.getflagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), callback_main.getAcctAgent().getAcctNum())) {
								saveAccountHelper.updateLastBillDate(ClnEnv.getSessionLoginID(), callback_main.getAcctAgent().getAcctNum(), binqCra.getOBillList().getOBillAry()[0].getInvDate());
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (bills == null || emptyBillDtCount == bills.length) {
			//no bill
			if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
				cell11a = new SmallTextCell(getResString(R.string.BILTM_NOBILL), "");
				cell11a.setBgColorId(R.color.white);
			} else if (callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV || callback_main.getLob() == R.string.CONST_LOB_IMS) {
				cell11a = new SmallTextCell(getResString(R.string.BIIMM_NOBILL), "");
				cell11a.setBgColorId(R.color.white);
			} else if (callback_main.getLob() == R.string.CONST_LOB_MOB || callback_main.getLob() == R.string.CONST_LOB_IOI) {
				cell11a = new SmallTextCell(getResString(R.string.BIMBM_NOBILL), "");
				cell11a.setBgColorId(R.color.white);
			} else if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F) {
				cell11a = new SmallTextCell(getResString(R.string.BIMBM_NOBILL), "");
				cell11a.setBgColorId(R.color.white);
			}
		}

		if (callback_main.IsZombie()) {
			cell11b = new SmallTextCell(getResString(R.string.zombie_lastbills_housekeep), "", R.color.red);
			cell11b.setBgColorId(R.color.white);
		}

		if (cellBillPdf != null) {
			cellBillPdf.setLeftPadding(basePadding);
			cellBillPdf.setRightPadding(basePadding);
			cellList.add(cellBillPdf);
		}
		if (cell11a != null) {
			cell11a.setLeftPadding(basePadding);
			cell11a.setRightPadding(basePadding);
			cellList.add(cell11a);
		}
		if (cell11b != null) {
			cell11b.setLeftPadding(basePadding);
			cell11b.setRightPadding(basePadding);
			cellList.add(cell11b);
		}
	}

	protected final boolean isThinBill() {
		Bill[] billary = binqCra.getOBillList().getOBillAry();
		return billary.length > 0 && billary[0].getWithheld().trim().length() > 0;
	}

	private boolean isNewBill() {
		try {
			if (callback_main.isMyMobAcct()) {
				if (binqCra.getOBillList().getOBillAry() != null && binqCra.getOBillList().getOBillAry().length > 0) {
					//check new bill
					if (callback_main.getAcctAgent().getLatest_bill() == null || "".equalsIgnoreCase(callback_main.getAcctAgent().getLatest_bill())) {
						// SwitchBillIcon(true);
						return true;
					} else
						// SwitchBillIcon(true);
						// SwitchBillIcon(false);
						return callback_main.CompareLastBillDate(me.callback_main.getAcctAgent().getLatest_bill(), binqCra.getOBillList().getOBillAry()[0].getInvDate());
				}
			} else {
				if (ClnEnv.getPref(Objects.requireNonNull(me.getActivity()).getApplicationContext(), me.getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true) && !callback_main.IsZombie()) {
					// make sure the BillAry Not null and empty Array
					if (binqCra.getOBillList().getOBillAry() != null && binqCra.getOBillList().getOBillAry().length > 0) {
						try {
							// Check Database only if the bill date returned
							if (!"".equalsIgnoreCase(binqCra.getOBillList().getOBillAry()[0].getInvDate())) {
								me.saveAccountHelper = SaveAccountHelper.getInstance(me.getActivity());
								// Get lastBilldate from database
								String lastBillDate = me.saveAccountHelper.getDateByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), binqCra.getIAcct().getAcctNum());
								if (lastBillDate == null) {
									me.saveAccountHelper.insert(ClnEnv.getSessionLoginID(), binqCra.getIAcct().getAcctNum());
								} else if (callback_main.CompareLastBillDate(lastBillDate, binqCra.getOBillList().getOBillAry()[0].getInvDate())) {
									me.saveAccountHelper.updateFlag(ClnEnv.getSessionLoginID(), binqCra.getIAcct().getAcctNum());
								}

								//check new bill
								saveAccountHelper = SaveAccountHelper.getInstance(me.getActivity());
								return saveAccountHelper.getflagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), callback_main.getAcctAgent().getAcctNum());
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/**
	 *
	 * @param index get position from BillDate Array
	 * @param billList BillDate ArrayList
	 * @return If billDate string value is not empty return true otherwise false
	 */
	private boolean checkBillDate(int index, List<Bill> billList) {
		boolean value = false;
		try {
			value = billList.get(index).getInvDate().length() > 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return value;
	}

	private int[] setBillImages(boolean isNewBill, List<Bill> billList) {
		int[] imageAry = new int[billList.size()];

		boolean isBill1Show = checkBillDate(0, billList);
		boolean isBill2Show = checkBillDate(1, billList);
		boolean isBill3Show = checkBillDate(2, billList);

		if (ClnEnv.isMyMobFlag()) {
			if (callback_main.getLob() == R.string.CONST_LOB_1010) {
//				imageAry[0] = isBill1Show ? R.drawable.btn_bills_1010_new : R.drawable.btn_bills_1010;
//				imageAry[1] = isBill2Show ? R.drawable.btn_bills_1010 : -1;
//				imageAry[2] = isBill3Show ? R.drawable.btn_bills_1010 : -1;
				for (int i = 0; i < imageAry.length; i++) {
					try {
						if (i == 0) {
							imageAry[i] = !billList.get(i).getInvDate().isEmpty()
									? R.drawable.btn_bills_1010_new : R.drawable.btn_bills_1010;
						} else {
							imageAry[i] = !billList.get(i).getInvDate().isEmpty()
									? R.drawable.btn_bills_1010 : -1;
						}
					} catch (Exception e) {
						imageAry[i] = -1;
						e.printStackTrace();
					}
				}
			} else if (callback_main.getLob() == R.string.CONST_LOB_O2F || callback_main.getLob() == R.string.CONST_LOB_MOB) {
//				imageAry[0] = isBill1Show ? R.drawable.btn_bills_csl_new : R.drawable.btn_bills_csl;
//				imageAry[1] = isBill2Show ? R.drawable.btn_bills_csl : -1;
//				imageAry[2] = isBill3Show ? R.drawable.btn_bills_csl : -1;
				for (int i = 0; i < imageAry.length; i++) {
					try {
						if (i == 0) {
							imageAry[i] = !billList.get(i).getInvDate().isEmpty()
									? R.drawable.btn_bills_csl_new : R.drawable.btn_bills_csl;
						} else {
							imageAry[i] = !billList.get(i).getInvDate().isEmpty()
									? R.drawable.btn_bills_csl : -1;
						}
					} catch (Exception e) {
						imageAry[i] = -1;
						e.printStackTrace();
					}
				}
			}
		} else {
			for (int i = 0; i < imageAry.length; i++) {
				try {
					if (i == 0) {
						imageAry[i] = !billList.get(i).getInvDate().isEmpty()
								? R.drawable.btn_bills_new : R.drawable.btn_bills;
					} else {
						imageAry[i] = !billList.get(i).getInvDate().isEmpty()
								? R.drawable.btn_bills : -1;
					}
				} catch (Exception e) {
					imageAry[i] = -1;
					e.printStackTrace();
				}
			}
		}
		return imageAry;
	}

	//Button theme for update, billinfo, payment..
	private int getBtnImage() {
		int image = 0;
		if (ClnEnv.isMyMobFlag()) {
			if (callback_main.getLob() == R.string.CONST_LOB_1010) {
				image = R.drawable.btn_details_1010;
			} else if (callback_main.getLob() == R.string.CONST_LOB_O2F) {
				image = R.drawable.btn_details_csl;
			} else {
				image = R.drawable.btn_details_csl;
			}
		} else {
			image = R.drawable.btn_details;
		}
		return image;
	}

	private void startDownloadPdf() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
			if (binqCra != null) {
				String lang = isZh ? "zh" : "en";
				APIsManager.doDownloadPDF(me, binqCra, callback_main.getLobString(), callback_main.getSubnRec().cusNum, clickBill, lang);
			}
		} else if (RuntimePermissionUtil.isWriteExternalStoragePermissionGranted(getActivity())) {
			if (binqCra != null) {
				String lang = isZh ? "zh" : "en";
				APIsManager.doDownloadPDF(me, binqCra, callback_main.getLobString(), callback_main.getSubnRec().cusNum, clickBill, lang);
			}
		} else {
			if (ClnEnv.getPref(getActivity(), Constant.FILE_STORAGE_PERMISSION_DENIED, false)) {
				displayDialog(Objects.requireNonNull(getActivity()).getString(R.string.permission_denied_setting_enabled));
			} else {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					RuntimePermissionUtil.requestFileStoragePermission(getActivity());
				}
			}
		}
	}

	// Format BillList Date
	private String toLocaleDateString(String dateString, String patternString, String outPatternString) {
		SimpleDateFormat sdf = new SimpleDateFormat(patternString, Locale.US);
		Date d;

		try {
			d = sdf.parse(dateString);
			sdf = new SimpleDateFormat(outPatternString, Locale.US);
			return sdf.format(d);
		} catch (Exception e) {
			if (debug) Log.e("ServiceLTSActivity", "toLocaleDateString error: dateString=" + dateString + ", patternString=" + patternString + ", Exception:" + e.toString());
			return dateString;
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.BILL.equals(response.getActionTy())) {
			binqCra = (BinqCra) response.getCra();
			initBasicUI();
			initMobBillSummary();
		} else if (APIsManager.CHK_BILL.equals(response.getActionTy())) {
			// binqCra = (BinqCra) response.getCra();
			startDownloadPdf();
			// Download bill Analytics
			firebaseSetting.onBillDownload();
		} else if (APIsManager.AO_AUTH.equals(response.getActionTy())) {
			addOnCra = (AddOnCra) response.getCra();
			addOnCra.getOSubnRec().ivr_pwd = addOnCra.getISubnRec().ivr_pwd;

			callback_main.setSubscriptionRec(addOnCra.getOSubnRec());
			callback_main.getAcctAgent().setSubnRec(addOnCra.getOSubnRec());
			doGetBill();
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (APIsManager.CHK_BILL.equals(response.getActionTy()) && RC.BINQ_BINRDY.equalsIgnoreCase(response.getReply().getCode())) {
			if (me.chkBillcount < 10) {
				me.chkBillcount++;
				try {
					//					Log.i("BillCheck", "callbill"+me.chkBillcount);
					doCheckBillStsLoop(billDate, me.chkBillcount == 9);
				} catch (Exception e) {
					// fail to sleep
					me.chkBillcount = 10;
				}
			} else {
				me.displayDialog(Utils.getString(me.getActivity(), R.string.BINQ_BINRDY));
			}
			// General Error Message
		} else if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
			recallDoAuth(me, callback_main.getSubnRec().copyMe());
		} else {
			if (callback_main.getLob() == R.string.CONST_LOB_MOB || callback_main.getLob() == R.string.CONST_LOB_IOI || callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F) {
				DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqMobMdu(getActivity(), response.getReply().getCode(), callback_main.getLob()));
			} else if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
				DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqLtsMdu(getActivity(), response.getReply().getCode()));
			} else if (callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV || callback_main.getLob() == R.string.CONST_LOB_IMS) {
				DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqImsMdu(getActivity(), response.getReply().getCode()));
			} else {
				DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
			}
		}
		if (APIsManager.BILL.equals(response.getActionTy())) {
			initBasicUI();
			initMobBillSummary();
		}
	}

	private void doGATracker(){
		if (!(Utils.getString(getActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY).equals(callback_main.getTrackerId()) || 
				Utils.getString(getActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY).equals(callback_main.getTrackerId()) || 
				Utils.getString(getActivity(), R.string.CONST_SCRN_TVBILLSUMMARY).equals(callback_main.getTrackerId()) || 
				Utils.getString(getActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY).equals(callback_main.getTrackerId()))) {

			//tracker
			switch(callback_main.getLob()) {
			case R.string.CONST_LOB_MOB:
			case R.string.CONST_LOB_IOI:   
			case R.string.CONST_LOB_1010:
			case R.string.CONST_LOB_O2F:
				callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY));
				if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY));
				break;
			case R.string.CONST_LOB_PCD:
				callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY));
				if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY));
				break;
			case R.string.CONST_LOB_IMS:
				callback_main.setTrackerId(Utils.getString(getActivity(), R.string.GASrvIMSBillSumVC));
				if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.GASrvIMSBillSumVC));
				break;
			case R.string.CONST_LOB_TV:
				callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_TVBILLSUMMARY));
				if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_TVBILLSUMMARY));
				break;
			case R.string.CONST_LOB_LTS:
				callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY));
				if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY));
				break;
			}
		}
	}

	/* Update: Moiz: 060717
	 * This method will show the message PCD_BILL_UNDER_CSL and will be  
	 * called if the SubnRec.billagt is true and  the LOB is PCD.
	 * 060817: Half the basePadding
	 */
	private void showPcdBillByAgent() {
		bundleList = new ArrayList<Bundle>();
		cellList = new ArrayList<Cell>();
		cellViewAdapter = new CellViewAdapter(this);
		aq = new AAQuery(myView);

		aq.id(R.id.fragment_bill_layout).backgroundColorId(R.color.white);
		frame = (LinearLayout) aq.id(R.id.fragment_bill_listview).getView();

		SmallTextCell messageCell;
		messageCell = new SmallTextCell(getString(R.string.PCD_BILL_UNDER_CSL), "");
		messageCell.setTopMargin(basePadding/2);
		messageCell.setLeftMargin(basePadding/2); 
		messageCell.setRightMargin(basePadding/2);
		messageCell.setContentColorId(R.color.hkt_textcolor);
		cellList.add(messageCell);

		cellViewAdapter.setView(frame, cellList);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);

		if (requestCode == Constant.REQUEST_FILE_STORAGE_PERMISSION || requestCode == Constant.REQUEST_LOG_FILE_STORAGE_PERMISSION) {
			//Permission is granted
			if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				if(requestCode == Constant.REQUEST_FILE_STORAGE_PERMISSION) {
					startDownloadPdf();
				} else {
					doGetBill();
				}
			} else {

				displayDialog(getActivity().getString(R.string.permission_denied));

				//This will return false if the user tick the Don't ask again checkbox
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					if(!RuntimePermissionUtil.shouldShowRequestPermission(getActivity())) {
                        ClnEnv.setPref(getActivity().getApplicationContext(), Constant.FILE_STORAGE_PERMISSION_DENIED, true);
                    }
				}
			}
		}
	}
}
