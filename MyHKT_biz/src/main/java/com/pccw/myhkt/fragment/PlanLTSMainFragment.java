package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pccw.dango.shared.cra.PlanLtsCra;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.PlanLTS_TPFragment.OnPlanMainLtsListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.pageindicator.CirclePageIndicator;

/************************************************************************
 * File : PlanLTSFragment.java
 * Desc : LTS Plan
 * Name : PlanLTSFragment
 * by 	: Andy Wong
 * Date : 25/1/2015
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 18/1/2015  Andy Wong 		-First draft
 *************************************************************************/

public class PlanLTSMainFragment extends BaseServiceFragment implements OnPlanMainLtsListener {
	private PlanLTSMainFragment me;
	private AAQuery aq;

	private static final String TAG = "PlanLTSMainFragment";

	private int deviceWidth ;
	private int extralinespace;
	private int activeSubPage =0;

	private PlanLtsCra 			planLtsCra;
	private String 				returnCode = null;

	private PlanLTS_CPFragment 	planLTS_CPFragment;
	public PlanLTS_TPFragment	planLTS_TPFragment;
	public List<Fragment> fragmentList ;

	private CirclePageIndicator mIndicator;
	private PlanLTSPagerAdapter planLTSPagerAdapter;
	private ViewPager			viewPager;
	private OnPlanLtsListener callback_lts;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback_main = (OnServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}
		try {
			callback_lts = (OnPlanLtsListener) getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}
	}

	public interface OnPlanLtsListener {
		int getCurrentPlanPage();
		void setActiveSubView(int view);
		void displayChildview(int type);
	}

	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			planLTS_TPFragment = (PlanLTS_TPFragment) getChildFragmentManager().getFragment(savedInstanceState, "Frag1");
			planLTS_CPFragment = (PlanLTS_CPFragment) getChildFragmentManager().getFragment(savedInstanceState, "Frag2");
		} else {
			planLTS_TPFragment = new PlanLTS_TPFragment();
			planLTS_CPFragment  = new PlanLTS_CPFragment();
		}
		
//		Log.i(TAG, java.lang.System.identityHashCode(planLTS_TPFragment)+"");
//		Log.i(TAG, java.lang.System.identityHashCode(planLTS_CPFragment)+"");
		fragmentList = new ArrayList<Fragment>();
		fragmentList.add(planLTS_TPFragment);
		fragmentList.add(planLTS_CPFragment);		
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_plan_lts_main, container, false);
		aq = new AAQuery(fragmentLayout);
		initData();
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
		//Screen Tracker
	}

	protected void initData(){
		super.initData();
		activeSubPage = callback_lts.getCurrentPlanPage() == R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN ? 0 : 1;
	}

	protected void initUI(){
		planLTSPagerAdapter = new PlanLTSPagerAdapter(getChildFragmentManager(), fragmentList);
		viewPager = (ViewPager) aq.id(R.id.fragment_plan_lts_viewpager).getView();
		viewPager.setAdapter(planLTSPagerAdapter);		

		mIndicator = (CirclePageIndicator) aq.id(R.id.fragment_usage_indicator).getView();
		mIndicator.setViewPager(viewPager);
		viewPager.setOnPageChangeListener(new OnPageChangeListener(){

			@Override
			public void onPageScrollStateChanged(int arg0) {}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {}

			@Override
			public void onPageSelected(int arg0) {
				activeSubPage  = arg0;		
				mIndicator.setCurrentItem(arg0);
				Utils.closeSoftKeyboard(getActivity());
//				Log.i(TAG, java.lang.System.identityHashCode(planLTS_TPFragment)+"");
//				Log.i(TAG, java.lang.System.identityHashCode(planLTS_CPFragment)+"");
				if(activeSubPage == 0){
					callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_PLAN));
					planLTS_TPFragment.refresh();
				} else {
					callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_PLAN));
					planLTS_CPFragment.refresh();
				}
				setModuleId();
			}			
		});
		viewPager.setCurrentItem(getCurrentPlanPage());	
		mIndicator.setCurrentItem(getCurrentPlanPage());

	}


	//Refresh the data only when it is on current page
	public void refreshData(){	
		
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LTSPLAN && callback_lts.getCurrentPlanPage() == R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN){
			callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_PLAN));
			planLtsCra = null;
			//			activeSubPage = 0;
			initUI();			
		}	
	}

	public void refresh(){
		super.refresh();
//		Log.i(TAG, java.lang.System.identityHashCode(this)+"");
//		Log.i(TAG, java.lang.System.identityHashCode(planLTS_TPFragment)+"");
//		Log.i(TAG, java.lang.System.identityHashCode(planLTS_CPFragment)+"");
		planLtsCra = null;
		activeSubPage = 0;
		viewPager.setCurrentItem(activeSubPage);
		planLTS_TPFragment.refresh();
	}
	
	// View Pager Class
	private class PlanLTSPagerAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragmentList;
		public PlanLTSPagerAdapter(FragmentManager fragmentManager,List<Fragment> fragments ) {
			super(fragmentManager);
			fragmentList = fragments;
		}

		@Override
		public int getCount() {
			return fragmentList.size();
		}

		// Returns the fragment to display for that page
		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0: // Fragment # 0 - This will show FirstFragment
//				planLTS_TPFragment = new PlanLTS_TPFragment();
//				planLTS_CPFragment  = new PlanLTS_CPFragment();
				return planLTS_TPFragment;
//				return fragmentList.get(position);
			case 1: // Fragment # 1 - This will show FirstFragment different title
//				planLTS_TPFragment = new PlanLTS_TPFragment();
//				planLTS_CPFragment  = new PlanLTS_CPFragment();
//				return fragmentList.get(position);	
				return planLTS_CPFragment;
			default:
				return null;			
			}
		}
	}

	protected void cleanupUI() {

	}

	protected void setModuleId() {
		
	}
	@Override
	public PlanLtsCra getPlanltsCra() {		
		return planLtsCra;
	}

	@Override
	public void setPlanLtsCra(PlanLtsCra mPlanLtscra) {
		planLtsCra = mPlanLtscra;

	}

	@Override
	public int getCurrentPlanPage() {
		return activeSubPage;
	}
	
	

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		if (planLTS_TPFragment != null) {
			getChildFragmentManager().putFragment(outState, "Frag1",  planLTS_TPFragment);
		}

		if (planLTS_CPFragment != null) {
			getChildFragmentManager().putFragment(outState, "Frag2", planLTS_CPFragment);
		}

		super.onSaveInstanceState(outState);
	}
}

