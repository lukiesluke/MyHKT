package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Selection;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.adapter.EditServiceListViewAdapter;
import com.pccw.myhkt.adapter.EditServiceListViewAdapter.ListViewHolder;
import com.pccw.myhkt.adapter.EditServiceListViewAdapter.OnEditServiceListViewAdapterListener;
import com.pccw.myhkt.fragment.MyProfLoginMainFragment.OnMyProfLoginListener;
import com.pccw.myhkt.lib.swipelistview.BaseSwipeListViewListener;
import com.pccw.myhkt.lib.swipelistview.SwipeListView;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.RegInputItem;



public class MyProfServListFragment extends BaseFragment implements OnEditServiceListViewAdapterListener{
	private MyProfServListFragment me;
	private AAQuery aq;
	private View thisView;

	private SveeRec sveeRec = null;

	// CallBack
	private OnMyProfLoginListener  			callback;
	// List View
	private EditServiceListViewAdapter					editServiceListViewAdapter;

	private RegInputItem 			inputName;
	private RegInputItem 			inputEmail;
	private RegInputItem 			inputMobNum;

	private Button					myprofservlist_btn_alias;
	private Button 					myprofservlist_btn_cancel;
	private Button 					myprofservlist_btn_update;

	private SwipeListView 			myprofservlist_listview;

	private boolean isChangePwdMode = false;
	private String TAG = "MyProfServListFragment";

	private AcMainCra acMainCra 	= null;	


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			callback = (OnMyProfLoginListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnMyProfServListListener");
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_myprofservlist, container, false);
		thisView = fragmentLayout;
		initData();
		return fragmentLayout;
	}


	protected void initData() {
		super.initData();
		aq = new AAQuery(thisView);
	}

	protected void initUI() {

		//Container layout
		aq.marginpx(R.id.myprofservlist_layout, basePadding, 0, basePadding, 0);

		//Header layout
		aq.id(R.id.myprofservlist_header1).text(R.string.myhkt_myprofservlist_header1).textSize(AAQuery.getDefaultTextSize()-1);
		aq.id(R.id.myprofservlist_header2).text(R.string.myhkt_myprofservlist_header2).textSize(AAQuery.getDefaultTextSize()-1);
		//		aq.id(R.id.myprofservlist_header3).text(R.string.myhkt_myprofservlist_header3).textSize(AAQuery.getDefaultTextSize()-2);

		//button layout
		aq.id(R.id.myprofservlist_btn_cancel).clicked(this, "onClick");
		aq.id(R.id.myprofservlist_btn_update).clicked(this, "onClick");
		//		aq.id(R.id.myprofservlist_btn_alias).clicked(this, "onClick");

		//		aq.id(R.id.myprofservlist_btn_alias).text(R.string.myhkt_btn_alias).margin(padding_twocol, 0, padding_twocol, 0);
		aq.norm2TxtBtns(R.id.myprofservlist_btn_cancel, R.id.myprofservlist_btn_update, getResString(R.string.MYHKT_BTN_CANCEL), getResString(R.string.MYHKT_BTN_UPDATE));
		aq.marginpx(R.id.myprofservlist_btn_cancel, basePadding, 0, basePadding, 0);
		aq.marginpx(R.id.myprofservlist_btn_update, basePadding, 0, basePadding, 0);
		//		
		//Edit Display Name button
		Drawable image = getResources().getDrawable(R.drawable.btn_details);
		image.setBounds( 0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight() );
		aq.id(R.id.myprofservlist_btn_alias).getTextView().setCompoundDrawables(null, null, image, null);
		aq.id(R.id.myprofservlist_btn_alias).getTextView().setCompoundDrawablePadding(10);
		aq.id(R.id.myprofservlist_btn_alias).text(R.string.myhkt_btn_alias_noncapital).textColorId(R.color.hkt_buttonblue).clicked(this, "onClick");
		aq.id(R.id.myprofservlist_btn_alias).getTextView().setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
		aq.id(R.id.myprofservlist_btn_alias).textSize(AAQuery.getDefaultTextSize()-1);
		image = null;
		//		prepareAccData();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.myprofservlist_btn_alias:
			if (myprofservlist_listview.isOpened()) {
				myprofservlist_listview.closeOpenedItems();
			} else {
				for (int i = myprofservlist_listview.getFirstVisiblePosition(); i <= myprofservlist_listview.getLastVisiblePosition(); i++) {
					//					if (!serviceListViewAdapter.isZombie(i))
					myprofservlist_listview.openAnimate(i);
				}
			}
			break;
		case R.id.myprofservlist_btn_cancel:
			callback.closeActivity();
			break;
		case R.id.myprofservlist_btn_update:
			if (EditServiceListViewAdapter.checkSelected() > 0) {
				// doSvcAso
				//					progressDialogHelper.showProgressDialog(me.getActivity(), me.onstopHandler, false); //TODO
				// Prepare subnRecAry which are selected
				SubnRec[] subnRecAry = new SubnRec[ClnEnv.getQualSvee().getSubnRecAry().length];
				for (int i = 0; i < ClnEnv.getQualSvee().getSubnRecAry().length; i++) {
					subnRecAry[i] = ClnEnv.getQualSvee().getSubnRecAry()[i].copyMe();
					if (EditServiceListViewAdapter.isSelected[i]) {
						subnRecAry[i].assoc = "Y";
					} else {
						subnRecAry[i].assoc = "N";
					}
				}
				//					regnCra = new RegnCra();
				//					regnCra.setRegCustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
				//					regnCra.getRegCustRec().pwd = ClnEnv.getSessionPassword();
				//					regnCra.setSubnRecAry(subnRecAry);
				//
				//					regnGrq = new RegnGrq();
				//					regnGrq.actionTy = DoSvcAsoAsyncTask.actionTy;
				//					regnGrq.sessTok = ClnEnv.getSessTok();
				//					regnGrq.regnCra = regnCra;
				//
				//					doSvcAsoAsyncTask = new DoSvcAsoAsyncTask(getActivity().getApplicationContext(), callbackHandler).execute(regnGrq);

				acMainCra = new AcMainCra();
				acMainCra.setIChgPwd(false);
				acMainCra.setISubnRecAry(subnRecAry);
				acMainCra.setISveeRec(ClnEnv.getQualSvee().getSveeRec());
				acMainCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);

				APIsManager.doSvcAso(null, me, acMainCra);
				//				} else if (EditServiceListViewAdapter.checkSelected() > 20) {
				//					displayDialog(Utils.getString(me.getActivity(), R.string.RSVM_TOOMANY_ASO));
			} else {
				displayDialog(Utils.getString(me.getActivity(), R.string.RSVM_NO_SVCSEL));
			}
			break;
		}
	}

	public void updateAlias(String alias, SubnRec subnRec) {
		// doSvcAso
		// Prepare subnRecAry which are selected
		SubnRec[] subnRecAry = new SubnRec[ClnEnv.getQualSvee().getSubnRecAry().length];
		for (int i = 0; i < ClnEnv.getQualSvee().getSubnRecAry().length; i++) {
			subnRecAry[i] = ClnEnv.getQualSvee().getSubnRecAry()[i].copyMe();

			if (Utils.matchSubnRec(subnRecAry[i], subnRec)) {
				subnRecAry[i].alias = alias;
			}
		}

		AcMainCra acMainCra = new AcMainCra();
		acMainCra.setIChgPwd(false);
		acMainCra.getISveeRec().pwd = ClnEnv.getSessionPassword();
		acMainCra.setISubnRecAry(subnRecAry);
		acMainCra.setISveeRec(ClnEnv.getQualSvee().getSveeRec().copyMe());
		acMainCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);

		APIsManager.doSvcAso(null, me, acMainCra);
	}

	protected final void refreshData() {
		if (callback.getCurrentPage() == 1 ) {
			super.refreshData();
			refresh();
		}
	}

	/************************************
	 * 
	 * Read data
	 * 
	 ************************************/
	public final void refresh() {
		//Screen Tracker

		aq = new AAQuery(thisView);
		//		if (callback.getActiveSubview() == R.string.CONST_SELECTEDVIEW_MYPROFILE_SERVICELIST) {
		// Initialize ListView checking variables
		//			callback.setDataChanged(false);

		//Swipe List View
//		int deviceWidth = getResources().getDisplayMetrics().widthPixels;
		editServiceListViewAdapter = new EditServiceListViewAdapter(getActivity(), me);
		aq.id(R.id.myprofservlist_listview).getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		aq.id(R.id.myprofservlist_listview).getListView().setAdapter(editServiceListViewAdapter);
		myprofservlist_listview = getActivity().findViewById(R.id.myprofservlist_listview);
		myprofservlist_listview.setSwipeListViewListener( new EditServiceListSwipeListViewListener());
		myprofservlist_listview.setDeviceWidth(deviceWidth - basePadding*2);
		myprofservlist_listview.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
		myprofservlist_listview.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
		myprofservlist_listview.setSwipeActionRight(SwipeListView.SWIPE_ACTION_REVEAL);
		myprofservlist_listview.setOffsetLeft((deviceWidth - basePadding*2) * 5 / 6);
		myprofservlist_listview.setAnimationTime(0);
		myprofservlist_listview.setSwipeOpenOnLongPress(false);
	}

	class EditServiceListSwipeListViewListener extends BaseSwipeListViewListener{
		//		AdapterView<?> adapterView, View view, int position, long arg3
		@Override
		public void onClickFrontView(int position, View view) {
			ListViewHolder holder = (ListViewHolder) view.getTag();
			holder.adapter_editservicelist_checkBox.toggle();
			EditServiceListViewAdapter.isSelected[position] = holder.adapter_editservicelist_checkBox.isChecked();
			//			callback.setDataChanged(true);
		}


		@Override
		public void onDismiss(int[] reverseSortedPositions) {
			//			for (int position : reverseSortedPositions) {
			//				testData.remove(position);
			//			}
			//			mAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
		//		ClnEnv.getQualCust().copyFrom(logincra.getQualCust().copyMe()); //TODO
		//		if (me.callback.getActiveSubview() == R.string.CONST_SELECTEDVIEW_MYPROFILE_SERVICELIST) {
		//			me.updateDialog(Utils.getString(me.getActivity(), R.string.RSVM_DONE));
		//		}
		//update SubnRec
		AcMainCra acMainCra = (AcMainCra) response.getCra();
		ClnEnv.getQualSvee().setSveeRec(acMainCra.getOSveeRec());
		ClnEnv.getQualSvee().setSubnRecAry(acMainCra.getOSubnRecAry());

		DialogHelper.createSimpleDialog(getActivity(), Utils.getString(me.getActivity(), R.string.RSVM_DONE), 
				Utils.getString(me.getActivity(), R.string.btn_ok), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				//				refreshData();
				refresh();
			}
		});

		//		me.callback.setDataChanged(false);

		// Reset check RC_ALT error time
		//		me.errALT2nd = false;
		//		me.regnGrq = null;
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		AcMainCra acMainCra;
		try {
			acMainCra = (AcMainCra)response.getCra();
		} catch (Exception e) {
			acMainCra = null;
		}
		if (acMainCra != null) {
			//			if (!logincra.getReply().isEqual(Reply.RC_SUCC)) {
			//				if (logincra.getReply().isEqual(Reply.RC_IVSESS)) {
			//					// RC_IVSESS
			//					me.redirectDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), logincra.getReply().getCode()));
			//				} else if (logincra.getReply().isEqual(Reply.RC_ALT)) {
			//					// RC_ALT
			//					if (!me.errALT2nd) {
			//						// RC_ALT 1st time
			//						if (!ClnEnv.getSessionPassword().equalsIgnoreCase("") && !ClnEnv.getSessionLoginID().equalsIgnoreCase("")) {
			//							// try to login once, if have LoginID/Password
			//							// doHelo
			//							me.progressDialogHelper.showProgressDialog(me.getActivity(), me.onstopHandler, true, ProgressDialogHelper.dohello);
			//							HeloGrq helogrq = new HeloGrq();
			//							helogrq.actionTy = DoHeloAsyncTask.actionTy;
			//							helogrq.smphCra = new SmphCra();
			//							helogrq.smphCra.getISmphReq().chkSum = Utils.sha256(ClnEnv.getPref(me.getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", Utils
			//									.getString(me.getActivity(), R.string.CONST_SALT));
			//							helogrq.smphCra.getISmphReq().devId = ClnEnv.getPref(me.getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "");  // Device ID
			//							helogrq.smphCra.getISmphReq().devTy = "A"; 																							// Device Type: A for android, I for IOS
			//							helogrq.smphCra.getISmphReq().gni = ClnEnv.getPref(me.getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y"); // General Notification
			//																																												// ON/OFF
			//							if (ClnEnv.getPref(me.getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false) && ClnEnv.isLoggedIn()) {							// Bill Notificatiion "Y" if
			//								// password is saved
			//								helogrq.smphCra.getISmphReq().bni = "Y";
			//							} else {
			//								helogrq.smphCra.getISmphReq().bni = "N";
			//							}
			//							helogrq.smphCra.getISmphReq().lang = ClnEnv.getAppLocale(me.getActivity().getBaseContext());
			//							doHeloAsyncTask = new DoHeloAsyncTask(me.getActivity().getApplicationContext(), callbackHandler).execute(helogrq);
			//	
			//						} else {
			//							// redirect to Login, error message IVSESS, if no LoginID/Password
			//							me.redirectDialog(Utils.getString(me.getActivity(), R.string.DLGM_ABORT_IVSESS));
			//						}
			//					} else {
			//						// redirect to Login, error message IVSESS
			//						me.redirectDialog(Utils.getString(me.getActivity(), R.string.DLGM_ABORT_IVSESS));
			//					}
			//					me.errALT2nd = true;
			//				} else if (logincra.getReply().isEqual(Reply.RC_NOT_AUTH)) {
			//					// RC_NOT_AUTH
			//					me.displayDialog(Utils.getString(me.getActivity(), R.string.RSVM_NOT_AUTH));
			//					// Reset check RC_ALT error time
			//					me.errALT2nd = false;
			//					me.regnGrq = null;
			//				} else if (logincra.getReply().isEqual(Reply.RC_INACTIVE_RCUS)) {
			//					// RC_INACTIVE_RCUS
			//					me.displayDialog(Utils.getString(me.getActivity(), R.string.RSVM_INACT_RCUS));
			//					// Reset check RC_ALT error time
			//					me.errALT2nd = false;
			//					me.regnGrq = null;
			//				} else {
			//					// RC_IVDATA
			//					// RC_UXSVLTERR
			//					me.displayDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), logincra.getReply().getCode()));
			//					// Reset check RC_ALT error time
			//					me.errALT2nd = false;
			//					me.regnGrq = null;
			//				}
			//			}
		}

		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}
	}

	@Override
	public void displayEditDialog(String message, final int position) {
		if (alertDialog != null) {
			alertDialog.dismiss();
		}

		if (myprofservlist_listview != null) {
			if (myprofservlist_listview.isOpened()) {
				myprofservlist_listview.closeOpenedItems();
			}
		}

		AlertDialog.Builder builder = new Builder(me.getActivity());
		builder.setMessage(message);

		// Set an EditText view to get user input 
		final EditText input = new EditText(me.getActivity());
		input.setFilters(new InputFilter[] {new InputFilter.LengthFilter(getResources().getInteger(R.integer.CONST_MAX_ALIAS))});
		input.setSingleLine();
		input.setText(editServiceListViewAdapter.getAlias(position).toString());
		//edittext cursor reposition
		Selection.setSelection(input.getText(), input.length());
		builder.setView(input);

		builder.setNegativeButton(Utils.getString(me.getActivity(), R.string.btn_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				SubnRec subnRec = (SubnRec) editServiceListViewAdapter.getItem(position);
				updateAlias(value, subnRec);
			}
		});

		builder.setPositiveButton(Utils.getString(me.getActivity(), R.string.MYHKT_BTN_CANCEL), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});

		alertDialog = builder.create();
		alertDialog.show();

	}
}
