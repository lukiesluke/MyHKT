 package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.Account;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.mymob.model.MyMobAcctAgent;

/************************************************************************
 * File : BaseServiceFragment.java
 * Desc : Special base fragment for ServiceActivity related fragments
 * Name : BaseServiceFragment
 * by 	: Derek Tsui
 * Date : 28/01/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 28/01/2015 Derek Tsui 		-First draft
 *************************************************************************/

public class BaseServiceFragment extends BaseFragment {
	// CallBack
	protected OnServiceListener		callback_main;
	protected OnLiveChatListener 	callback_livechat;

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			callback_main = (OnServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}
		
		try {
			callback_livechat = (OnLiveChatListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnLiveChatListener");
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);		
		initPonBanner();
	}
	public interface OnLiveChatListener {
		void 	openLiveChat();
		void 	openLiveChat(String tempModuleId);
		void		setModuleId(String moduleId);
	}
	
	//Interface
	public interface OnServiceListener {
		Account 	getAccount();
		void 	setAccount(Account account);
		SubnRec 	getSubnRec();
		void 	setSubscriptionRec(SubnRec subnRec);
		void 	setActiveSubview(int index);
		int 		getActiveSubview();
		ApptCra  getApptCra();
		void 	setApptCra(ApptCra mApptCra);
		void 	displaySubview();
		int 	getLob();
		int 	getLtsType();
		String  getLobString();
		boolean 	IsZombie();
		boolean 	CompareLastBillDate(String lastBilldate, String newlastBilldate);
		boolean 	isMyMobAcct();
		MyMobAcctAgent getMyMobAcctAgent();
		AcctAgent getAcctAgent();
		int getCurrentServicePage();
		//TODO
        LnttAgent getLnttAgent();
		void 	 setLnttAgent(LnttAgent lnttAgent);
		LnttCra   getLnttCra();
		void 	 setLnttCra(LnttCra lnttCra);
		boolean  isLnttServiceClearable();
		void 	setIsLnttServiceClearable(Boolean isLnttServiceClearable);
		String 	 getNavTitle();
		String 	 getTrackerId();
		void 	 setTrackerId(String trackerId);
	}
	
	public void initPonBanner() {
		
	}

	//Refresh Data is called onresume
	//Refresh is called manually
	public void refresh() {
		if (debug) Log.i(this.getClass().toString() , "refresh"+"("+java.lang.System.identityHashCode(this)+")");		
	}
	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

}
