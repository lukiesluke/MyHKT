package com.pccw.myhkt.fragment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import androidx.fragment.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.activity.MainMenuActivity;
import com.pccw.myhkt.activity.SettingsActivity;

public class SlidingMenuFragment extends ListFragment{
	private SlidingMenu	slidingmenu;
	protected boolean		debug		= false;
	SlideMenuAdapter adapter;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.menu_list, null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		getListView().setDividerHeight(0);
		getListView().setDivider(null);
		debug = getResources().getBoolean(R.bool.DEBUG);
		Integer[] menuOptName;
		Integer[] menuOptImg;
		menuOptName = new Integer[]{ R.string.myhkt_LGIF_LOGIN, R.string.myhkt_settings, R.string.myhkt_about, R.string.myhkt_menu_tnc, R.string.myhkt_menu_quit};
		menuOptImg = new Integer[]{ R.drawable.ios7_login, R.drawable.settting, R.drawable.aboutmyhkt, R.drawable.ios7_tnc, R.drawable.icon_quit};

		adapter = new SlideMenuAdapter(getActivity());
		slidingmenu = ((MainMenuActivity) getActivity()).slidingmenu;

		for (int i = 0; i < menuOptName.length; i++) {
			adapter.add(new MenuOptionItem(menuOptName[i], menuOptImg[i]));
		}
		setBackground();
		setListAdapter(adapter);
	}
	
	public void setBackground(){
		this.getView().setBackgroundColor(this.getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.menu_bg_premier : R.color.hkt_slidemenu_blue));
	}

	private final void showContent() {
		if (slidingmenu != null) {
			slidingmenu.showContent();
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent;
		switch (position) {
			case 0:
				showContent();
				
				if (!ClnEnv.isLoggedIn()) {
					intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
					getActivity().startActivity(intent);
					getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
				} else {
					// Logout
					((MainMenuActivity) getActivity()).doLogout();
				}
				break;
			case 1:
				showContent();
				intent = new Intent(getActivity().getApplicationContext(), SettingsActivity.class);
				getActivity().startActivity(intent);
				getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
				break;
			case 2:
				displayAboutDialog();
				break;
			case 3:
				displayTNCSDialog();
				break;
			case 4:
			default:
				((MainMenuActivity) getActivity()).displayConfirmExitDialog();
				break;
//			case 5:
//			    intent = new Intent();
//				intent.setData(Uri.parse("theclub://com.pccw.theclub"));
//				startActivity(intent);
//				break;
		}
	}

	// About My HKT
	protected final void displayAboutDialog() {
//		AlertDialog.Builder builder = new Builder(getActivity());
//		builder.setTitle(Utils.getString(getActivity(), R.string.myhkt_about));
//		builder.setMessage(String.format("%s\n%s\n%s %s", Utils.getString(getActivity(), R.string.myhkt_about_appname), Utils.getString(getActivity(), R.string.myhkt_about_copyright), Utils.getString(getActivity(), R.string.myhkt_about_version), versionNum));
//		builder.setPositiveButton(Utils.getString(getActivity(), R.string.btn_ok), new OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//			}
//		});
//		builder.create().show();
		String versionNum = "";
		try {
			versionNum = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
			if (debug) {
				versionNum = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName + " (Build: " + getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode + ")";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String message = String.format("%s\n%s\n%s %s", Utils.getString(getActivity(), R.string.myhkt_about_appname), Utils.getString(getActivity(), R.string.myhkt_about_copyright), Utils.getString(getActivity(), R.string.myhkt_about_version), versionNum);
		String title = getResources().getString(R.string.myhkt_about);
		
		DialogHelper.createTitleDialog(getActivity(), title, message, getResources().getString(R.string.btn_ok));
	}

	public void refreshView(){
		Integer[] menuOptName;
		Integer[] menuOptImg;
		menuOptName = new Integer[]{ R.string.myhkt_LGIF_LOGIN, R.string.myhkt_settings, R.string.myhkt_about, R.string.myhkt_menu_tnc, R.string.myhkt_menu_quit};
		menuOptImg = new Integer[]{ R.drawable.ios7_login, R.drawable.settting, R.drawable.aboutmyhkt, R.drawable.ios7_tnc, R.drawable.icon_quit};

		adapter = new SlideMenuAdapter(getActivity());

		for (int i = 0; i < menuOptName.length; i++) {
			adapter.add(new MenuOptionItem(menuOptName[i], menuOptImg[i]));
		}

		setListAdapter(adapter);
	}
	// TNCS
	protected final void displayTNCSDialog() {
//		AlertDialog.Builder builder = new Builder(getActivity());
//
//		LinearLayout whiteDialogHeaderLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.dialog_white_header, null);
//		LinearLayout whiteDialogLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.dialog_white, null);
//		TextView dialog_title = (TextView) whiteDialogHeaderLayout.findViewById(R.id.dialog_white_title);
//		TextView dialog_detail = (TextView) whiteDialogLayout.findViewById(R.id.dialog_white_detail);
//
//		dialog_title.setText(Utils.getString(getActivity(), R.string.myhkt_menu_tnc));
//		dialog_detail.setText(Utils.getString(getActivity(), R.string.term_terms));
//
//		builder.setCustomTitle(whiteDialogHeaderLayout);
//		builder.setView(whiteDialogLayout);
//		builder.setInverseBackgroundForced(true);
//
//		builder.setPositiveButton(Utils.getString(getActivity(), R.string.btn_ok), new OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//			}
//		});
//		builder.create().show();
		String message = getResources().getString(R.string.term_terms);
		String title = getResources().getString(R.string.myhkt_menu_tnc);

		DialogHelper.createTitleDialog(getActivity(), title, message, getResources().getString(R.string.btn_ok));
	}

	private class MenuOptionItem {
		public int	tag;
		public int	iconRes;

		public MenuOptionItem(int tag, int iconRes) {
			this.tag = tag;
			this.iconRes = iconRes;
		}
	}

	public class SlideMenuAdapter extends ArrayAdapter<MenuOptionItem> {

		public SlideMenuAdapter(Context context) {
			super(context,0);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			try {
				ViewHolder holder;
				if (convertView == null) {
					holder = new ViewHolder();
					convertView = LayoutInflater.from(getContext()).inflate(R.layout.menu_list_row, null);
					
					holder.menu_list_row_icon = convertView.findViewById(R.id.menu_list_row_icon);
					holder.menu_list_row_title = convertView.findViewById(R.id.menu_list_row_title);
				} else {
					holder = (ViewHolder) convertView.getTag();
				}

				if (position == 0 && ClnEnv.isLoggedIn()) {
					holder.menu_list_row_title.setText(Utils.getString(getContext(), R.string.SHLF_LOGOUT));
					holder.menu_list_row_icon.setImageResource(R.drawable.ios7_logout);
				} else {
					holder.menu_list_row_icon.setImageResource(getItem(position).iconRes);
					holder.menu_list_row_title.setText(Utils.getString(getContext(), getItem(position).tag));
				}
				ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
				holder.menu_list_row_icon.setColorFilter(filter);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return convertView;
		}
	}
	
	public static class ViewHolder {
		ImageView	menu_list_row_icon;
		TextView	menu_list_row_title;
	}
}
