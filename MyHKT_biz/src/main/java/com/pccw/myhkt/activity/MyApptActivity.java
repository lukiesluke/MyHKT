package com.pccw.myhkt.activity;

import android.graphics.Point;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.Display;
import android.view.View;

import com.pccw.dango.shared.entity.ApptInfo;
import com.pccw.dango.shared.entity.SRApptInfo;
import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;

import com.pccw.myhkt.fragment.BaseMyApptFragment.OnMyApptListener;
import com.pccw.myhkt.fragment.MyApptListFragment;
import com.pccw.myhkt.fragment.MyApptUpdateFragment;
import com.pccw.myhkt.fragment.SRConfirmFragment;
import com.pccw.myhkt.fragment.SRCreationFragment;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;

/************************************************************************
 * File : MyApptActivity.java 
 * Desc : my appointment activity
 * Name : MyApptActivity
 * by : Derek Tsui 
 * Date : 01/03/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 01/03/2016 Derek Tsui 		-First draft
 *************************************************************************/

public class MyApptActivity extends BaseActivity implements OnMyApptListener {
	private AAQuery 	aq;

	// Fragment
	private FragmentManager				fragmentManager;
	protected int							activeSubview						= R.string.CONST_SELECTEDFRAG_MYAPPTLIST;	// Initial default subview
	//	protected int							activeSubview						= R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE;	// Initial default subview
	//	protected int							activeSubview						= R.string.CONST_SELECTEDFRAG_SRCREATION;	// Initial default subview
	//	protected int							activeSubview						= R.string.CONST_SELECTEDFRAG_SRCONFIRM;	// Initial default subview

	private Fragment						myApptListFragment		= null;
	private Fragment						myApptUpdateFragment    = null;

	//remove later
	private Fragment						srCreationFragment    = null;
	private Fragment						srConfirmFragment	  =	null;

	private SrvReq										srvReq;
	private SRApptInfo									srApptInfo			= null;
	private ApptInfo									apptInfo			= null;
	private String[]									timeslotAry			= null;
	
	private boolean isPause = false;



	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.activity_myappointment);
	}

	@Override
	protected final void onStart() {
		super.onStart();
		displaySubview();
	}
	
	@Override
	protected final void onPause() {
		super.onPause();
		isPause = true;
	}

	// Android Device Back Button Handling
	@Override
	public final void onBackPressed() {
		//			if (fragmentManager.getBackStackEntryCount() > 1) {
		//				popBackStack();
		//			} else {
		//				finish();
		//				overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		//			}
		if (activeSubview == R.string.CONST_SELECTEDFRAG_SRCONFIRM) {
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		} else if (activeSubview == R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE) {
			setActiveSubview(R.string.CONST_SELECTEDFRAG_MYAPPTLIST);
			fragmentBack();
		} else {
			fragmentBack();
		}
	}

	// Restore the Bundle data when re-built Activity
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		if (activeSubview == R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE) {
			srvReq = (SrvReq) savedInstanceState.getSerializable("SRVREQ");
			srApptInfo = (SRApptInfo) savedInstanceState.getSerializable("SRAPPTINFO");
		}
		super.onRestoreInstanceState(savedInstanceState);
	}

	// Saved data before Destroy Activity
	protected void onSaveInstanceState(Bundle outState) {
		if (activeSubview == R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE) {
			outState.putSerializable("SRVREQ", srvReq);
			outState.putSerializable("SRAPPTINFO", srApptInfo);
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onResume(){
		super.onResume();
		if (activeSubview == R.string.CONST_SELECTEDFRAG_MYAPPTLIST) {
			moduleId = getResources().getString(R.string.MODULE_MY_APPT);
		} else {
			if (SrvReq.OUT_PROD_EYE.equalsIgnoreCase(srvReq.getOutProd()) || SrvReq.OUT_PROD_VCE.equalsIgnoreCase(srvReq.getOutProd())) {
				moduleId = getResources().getString(R.string.MODULE_LTS_APPTUP);
			} else if (SrvReq.OUT_PROD_PCD.equalsIgnoreCase(srvReq.getOutProd())) {
				moduleId = getResources().getString(R.string.MODULE_PCD_APPTUP);
			} else if (SrvReq.OUT_PROD_VI.equalsIgnoreCase(srvReq.getOutProd())) {
				moduleId = getResources().getString(R.string.MODULE_TV_APPTUP);
			}
		}
	}

	public final void initUI2() {
		fragmentManager = getSupportFragmentManager();
		aq = new AAQuery(this);

		//navbar style
		aq.navBarBaseLayout(R.id.navbar_base_layout);
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.SHLF_MYAPPT));
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);

		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
	}

	public final void displaySubview() {
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out, R.anim.left_slide_in, R.anim.right_slide_out);
		if (!isPause) {
			switch (activeSubview) {
			case R.string.CONST_SELECTEDFRAG_MYAPPTLIST:
				myApptListFragment = new MyApptListFragment();
				transaction.replace(R.id.myappt_commonview, myApptListFragment);
				transaction.addToBackStack(null);
				break;
			case R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE:
				if (SrvReq.OUT_PROD_EYE.equalsIgnoreCase(srvReq.getOutProd()) || SrvReq.OUT_PROD_VCE.equalsIgnoreCase(srvReq.getOutProd())) {
					moduleId = getResources().getString(R.string.MODULE_LTS_APPTUP);
				} else if (SrvReq.OUT_PROD_PCD.equalsIgnoreCase(srvReq.getOutProd())) {
					moduleId = getResources().getString(R.string.MODULE_PCD_APPTUP);
				} else if (SrvReq.OUT_PROD_VI.equalsIgnoreCase(srvReq.getOutProd())) {
					moduleId = getResources().getString(R.string.MODULE_TV_APPTUP);
				}
				myApptUpdateFragment = new MyApptUpdateFragment();
				transaction.replace(R.id.myappt_commonview, myApptUpdateFragment);
				transaction.addToBackStack(null);
				break;
				//fake placement for testing only
			case R.string.CONST_SELECTEDFRAG_SRCREATION:
				srCreationFragment = new SRCreationFragment();
				transaction.replace(R.id.myappt_commonview, srCreationFragment);
				transaction.addToBackStack(null);
				break;
			case R.string.CONST_SELECTEDFRAG_SRCONFIRM:
				srConfirmFragment = new SRConfirmFragment();
				transaction.replace(R.id.myappt_commonview, srConfirmFragment);
				transaction.addToBackStack(null);
				break;
				//fake placement for testing only
			}
		}
		isPause = false;
		Utils.closeSoftKeyboard(this);
		transaction.commit();
	}


	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			//			onBackPressed();
			if (activeSubview == R.string.CONST_SELECTEDFRAG_SRCONFIRM) {
				finish();
				overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			} else if (activeSubview == R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE) {
				setActiveSubview(R.string.CONST_SELECTEDFRAG_MYAPPTLIST);
				fragmentBack();
			} else {
				fragmentBack();
			}

			break;
		}
	}

	@Override
	public void fragmentBack() {
		//clear saved data on back pressed
		setSrvReq(null);
		setApptInfo(null);

		if (fragmentManager.getBackStackEntryCount() > 1) {
			popBackStack();
		} else {
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		}
	}

	@Override
	public void popBackStack() {
		fragmentManager.popBackStack();
	}

	@Override
	public SubnRec getSubnRec() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSubscriptionRec(SubnRec subnRec) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setActiveSubview(int index) {
		activeSubview = index;
	}

	@Override
	public int getActiveSubview() {
		return activeSubview;
	}

	@Override
	public SrvReq getSrvReq() {
		return srvReq;
	}

	@Override
	public void setSrvReq(SrvReq srvReq) {
		this.srvReq = srvReq;
	}

	@Override
	public SRApptInfo getSrApptInfo() {
		return srApptInfo;
	}

	@Override
	public void setSrApptInfo(SRApptInfo srApptInfo) {
		this.srApptInfo = srApptInfo;
	}

	@Override
	public ApptInfo getApptInfo() {
		return apptInfo;
	}

	@Override
	public void setApptInfo(ApptInfo apptInfo) {
		this.apptInfo = apptInfo;
	}
}
