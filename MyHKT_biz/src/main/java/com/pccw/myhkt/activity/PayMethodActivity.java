package com.pccw.myhkt.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;

/************************************************************************
 * File : PayMethodActivity.java
 * Desc : Payment Method Web Page
 * Name : PayMethodActivity
 * by 	: Andy Wong
 * Date : 23/12/2015
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 23/12/2015 Andy Wong 		-First draft
 * 02/01/2016 Derek Tsui		-add logic, put life cycle code in order
 *************************************************************************/
public class PayMethodActivity extends BaseActivity{

	public final static String BUNDLEURL = "bundleurl";
	public final static String BUNDLETITLE = "bundletitle";
	private boolean					debug			= false;		
	private String title = null;

	private AAQuery aq;
	private WebView webView;
	private String TAG = this.getClass().getName();	
	private 			boolean		  isZh;
	
	private final String payMethUrl1010Zh = "http://1010.com.hk/c/payment_method_e";
	private final String payMethUrl1010En = "http://1010.com.hk/e/payment_method_e";
	private final String payMethUrlIOIZh =  "http://1010.com.hk/c/payment_method_m";
	private final String payMethUrlIOIEn =  "http://1010.com.hk/e/payment_method_m";
	private final String payMethUrlO2FZh = "http://e.hkcsl.com/paymentmethodc";
	private final String payMethUrlO2FEn = "http://e.hkcsl.com/paymentmethodc-e";
	private final String payMethUrlMOBZh = "http://e.hkcsl.com/paymentmethod";
	private final String payMethUrlMOBEn = "http://e.hkcsl.com/paymentmethodh-e";
	private final String payMethUrlPCDZh = "https://cs.netvigator.com/bill/payment_methods_c.html";
	private final String payMethUrlPCDEn = "https://cs.netvigator.com/bill/payment_methods_e.html";
	private final String payMethUrlLTSZh = "http://www.pccw.com/Customer+Service/Consumer/Billing+Inquiry/Payment+Methods?language=zh_HK";
	private final String payMethUrlLTSEn = "http://www.pccw.com/Customer+Service/Consumer/Billing+Inquiry/Payment+Methods?language=en_US";
	private final String payMethUrlTVZh = "http://nowtv.now.com/myaccount?lang=zh";
	private final String payMethUrlTVEn = "http://nowtv.now.com/myaccount?&lang=en";

	private AcctAgent acctAgent;
	private int ltsType = 0;
	private String lob = "";
	private String navbarText = "";
	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	

		debug = getResources().getBoolean(R.bool.DEBUG);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));		
		setContentView(R.layout.activity_paymethod);

		// Language indicator
        isZh = "zh".equalsIgnoreCase(Utils.getString(this, R.string.myhkt_lang));
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			acctAgent = (AcctAgent) bundle.getSerializable("ACCTAGENT");
			lob = acctAgent.getLob();
			ltsType = bundle.getInt("LTSTYPE");
			navbarText = acctAgent.getSrvNum();
		}
		setModuleID();
	}

	@Override
	protected final void onStart() {
		super.onStart();
		displayWebView(lob, navbarText);
	}

	public void onBackPressed() {
		webView.loadUrl("");
		finish();
		overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
	}

	protected void initUI2() {
		aq = new AAQuery(this);
		//navbar
		aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, 0, getLobIcon(), 0, acctAgent.getSubnRec().srvNum);
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.navBarTitle(R.id.navbar_title, title);
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");

		webView = aq.id(R.id.paymethod_webview).getWebView();
	}


	private void displayWebView(String lob, String navbarText) {
		if(webView == null) {
			initUI2();
		}
		aq.navBarTitle(R.id.navbar_title, navbarText);
		//		if ("LTS".equalsIgnoreCase(lob)) viewholder.paymethod_navbar_logo.setImageResource(R.drawable.lob_lts);
		//		if ("PCD".equalsIgnoreCase(lob)) viewholder.paymethod_navbar_logo.setImageResource(R.drawable.lob_pcd);
		//		if ("TV".equalsIgnoreCase(lob)) viewholder.paymethod_navbar_logo.setImageResource(R.drawable.lob_tv);
		//		if ("MOB".equalsIgnoreCase(lob)) {
		//			// Rex added change logo for MyMob
		//			if(ClnEnv.isMyMobFlag())
		//				viewholder.paymethod_navbar_logo.setImageResource(R.drawable.lob_csl_w);
		//			else
		//				viewholder.paymethod_navbar_logo.setImageResource(R.drawable.lob_csl);
		//			// Rex added
		//		}
		//		if ("1010".equalsIgnoreCase(lob)) viewholder.paymethod_navbar_logo.setImageResource(R.drawable.lob_1010);
		//		if ("O2F".equalsIgnoreCase(lob)) {
		//			// Rex added change logo for MyMob
		//			if(ClnEnv.isMyMobFlag())
		//				viewholder.paymethod_navbar_logo.setImageResource(R.drawable.lob_csl_w);
		//			else
		//				viewholder.paymethod_navbar_logo.setImageResource(R.drawable.lob_csl);
		//			// Rex added
		//		}
		//		aq.navBarDrawable(R.id.navbar_leftdraw, R.drawable.testfixline);
		webView.setVisibility(View.VISIBLE);
		webView.setWebViewClient(new WebViewOverrideUrl());
		webView.setVisibility(View.VISIBLE);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			webView.getSettings().setDisplayZoomControls(false);
		}
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		//		browser_webview.setWebViewClient(webview);


		String url = "";
		if ("LTS".equalsIgnoreCase(lob)) {			
			url = isZh ? payMethUrlLTSZh : payMethUrlLTSEn;		
		} else if ("PCD".equalsIgnoreCase(lob)) {
			url = isZh ? payMethUrlPCDZh : payMethUrlPCDEn;		
		} else if ("TV".equalsIgnoreCase(lob)) {
			url = isZh ? payMethUrlTVZh : payMethUrlTVEn;			
		} else if ("MOB".equalsIgnoreCase(lob)) {
			url = isZh ? payMethUrlMOBZh : payMethUrlMOBEn;		
		} else if ("101".equalsIgnoreCase(lob)) {
			url = isZh ? payMethUrl1010Zh : payMethUrl1010En;	
		} else if ("IOI".equalsIgnoreCase(lob)) {
			url = isZh ? payMethUrlIOIZh : payMethUrlIOIEn;	
		} else if ("O2F".equalsIgnoreCase(lob)) {
			url = isZh ? payMethUrlO2FZh : payMethUrlO2FEn;		
		} else {
			//handling
		}

		if (!"".equalsIgnoreCase(url)) webView.loadUrl(url);
//		String html = "<iframe src=\"http://docs.google.com/gview?url=https://cspuat.pccw.com/cs2/gen_cond_en.pdf&embedded=true\" style=\"width:600px; height:1200px;\" frameborder=\"0\"></iframe>";
//		webView.loadData(html, "text/html", "UTF-8");
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			onBackPressed();
			break;
		}
	}
	
	protected void setModuleID() {
		//Module id
		
		switch (acctAgent.getLobType()) {
		case R.string.CONST_LOB_LTS:
			moduleId = getResString(R.string.MODULE_LTS_BILL);
			break;
		case R.string.CONST_LOB_PCD:
			moduleId = getResString(R.string.MODULE_PCD_BILL);
			break;
		case R.string.CONST_LOB_TV:
			moduleId = getResString(R.string.MODULE_TV_BILL);
			break;
		case R.string.CONST_LOB_O2F:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_O2F_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_O2F_BILL);
			}
			break;
		case R.string.CONST_LOB_MOB:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_MOB_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_MOB_BILL);
			}
			break;
		case R.string.CONST_LOB_IOI:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_IOI_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_IOI_BILL);
			}
			break;
		case R.string.CONST_LOB_1010:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_101_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_101_BILL);
			}
			break;	
		}			
	}
	
	private final int getLobIcon() {
		int lobType = acctAgent.getLobType();
		if (lobType == R.string.CONST_LOB_1010 || lobType == R.string.CONST_LOB_IOI) {
			return R.drawable.logo_1010;
		} else if (lobType == R.string.CONST_LOB_MOB || lobType == R.string.CONST_LOB_O2F) {
			return R.drawable.logo_csl;
		} else if (lobType == R.string.CONST_LOB_LTS) {
			switch (ltsType) {
			case R.string.CONST_LTS_FIXEDLINE:
				return R.drawable.logo_fixedline_fixedline;
			case R.string.CONST_LTS_EYE:
				return R.drawable.logo_fixedline_eye;
			case R.string.CONST_LTS_IDD0060:
				return R.drawable.logo_fixedline_0060;
			case R.string.CONST_LTS_CALLINGCARD:
				return R.drawable.logo_fixedline_global;
			case R.string.CONST_LTS_ONECALL:
				return R.drawable.logo_fixedline_onecall;
			case R.string.CONST_LTS_ICFS:
				return R.drawable.logo_fixedline_icfs;
			default:
				return R.drawable.logo_fixedline;
			}
		} else if (lobType == R.string.CONST_LOB_PCD) {
			return R.drawable.logo_netvigator;
		} else if (lobType == R.string.CONST_LOB_TV) {
			return R.drawable.logo_now;
		}
		return 0;
	}

	//webview override url to avoid redirect at web browser outside the app
	private class WebViewOverrideUrl extends WebViewClient {
		boolean isReceiveError = false;

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
	        view.loadUrl(url);
	        return true;
		}
		
//	    @Override
//	    public void onLoadResource(WebView  view, String  url){
//
//	        if(url.endsWith(".pdf")){
//		    	Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//		    	startActivity(browserIntent);
//	        }
////	        if(url.endsWith(".pdf")){
////				Intent target = new Intent(Intent.ACTION_VIEW);
////				target.setDataAndType(Uri.parse(url), "application/pdf");
////				target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
////
////				//					    target = Intent.createChooser(target, "Open File");
////				try {
////					me.startActivity(target);
////				} catch (ActivityNotFoundException e) {
////					DialogHelper.createSimpleDialog(me, me.getString(R.string.myhkt_nopdfapp));
////				}
////	        }
//	    }

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			if (!isReceiveError) {
				view.setVisibility(View.VISIBLE);
			} else {
				view.setVisibility(View.GONE);
			}
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);

			isReceiveError = true;
			view.setVisibility(View.GONE);
			//			errorExitDialog(Utils.getString(me, R.string.LIVECHAT_UNAVAILABLE));
		}

//		@Override
//		public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//			handler.proceed(); // Ignore SSL certificate errors
//		}
	}


}
