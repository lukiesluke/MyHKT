package com.pccw.myhkt.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.entity.BcifRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.MyProfLoginMainFragment;
import com.pccw.myhkt.fragment.MyProfLoginMainFragment.OnMyProfLoginListener;
import com.pccw.myhkt.fragment.MyProfServListFragment;
import com.pccw.myhkt.fragment.ProfileContactFragment;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTIndicator;
import com.pccw.myhkt.listener.OnAccountDeleteListener;
import com.pccw.myhkt.model.TabItem;
import com.pccw.myhkt.utils.FirebaseSetting;

import java.util.ArrayList;
import java.util.List;

public class MyProfileActivity extends BaseActivity implements OnMyProfLoginListener, OnAccountDeleteListener {
    private boolean debug = false;
    private AAQuery aq;
    private SveeRec sveeRec = null;
    private BcifRec bcifRec = null; //Company Information
    private String TAG = this.getClass().getName();
    private List<TabItem> tapItemList;
    // Fragment
    private Fragment myProfLoginFragment = null;
    private Fragment myProfServListFragment = null;
    private Fragment myProfContactFragment = null;
    private HKTIndicator hktindicator;
    private ViewPager viewPager;
    private MyProfPagerAdapter myProfPagerAdapter;
    private int extralinespace;
    private int gridlayoutPadding = 0;
    private int deviceWidth = 0;
    private int colWidth = 0;
    public int currentPage = 0;
    private static final String SCREEN_PROFILE_INFO = "My Profile Info Screen";
    private static final String SCREEN_PROFILE_SERVICE_LIST = "My Profile Service List Screen";
    private static final String SCREEN_PROFILE_HKT = "My Profile HKT Screen";

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        me = this;

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        setContentView(R.layout.activity_myprofile);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        gridlayoutPadding = (int) getResources().getDimension(R.dimen.mainmenu_gridlayout_padding);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);

        if (ClnEnv.getLgiCra() != null) {
            sveeRec = ClnEnv.getLgiCra().getOQualSvee().getSveeRec();
            bcifRec = ClnEnv.getLgiCra().getOQualSvee().getBcifRec();
        }
        moduleId = getResString(R.string.MODULE_MYPROFILE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onResume() {
        super.onResume();
        moduleId = getResources().getString(R.string.MODULE_MYPROFILE);
        firebaseSetting.setScreenName(MyProfileActivity.class.getSimpleName(), SCREEN_PROFILE_INFO);
    }

    protected final void initUI2() {
        tapItemList = new ArrayList<TabItem>();
        tapItemList.add(new TabItem(R.drawable.myloginprofile, getResString(R.string.myhkt_myloginprofile_tabname), 0));
        tapItemList.add(new TabItem(R.drawable.myservicelist, getResString(R.string.myhkt_myservicelist_tabname), 1));
        tapItemList.add(new TabItem(R.drawable.mycontactinfo, getResString(R.string.myhkt_mycontactinfo_tabname), 2));
        tapItemList.add(new TabItem(-1, null, 5));

        colWidth = (deviceWidth - extralinespace * 2) / tapItemList.size();
        aq = new AAQuery(this);

        // navbar style
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_myprof_title));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        viewPager = (ViewPager) aq.id(R.id.activity_myprofile_view).getView();
        myProfPagerAdapter = new MyProfPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myProfPagerAdapter);

        firebaseSetting.onProfileEvent("Login_Profile");

        hktindicator = (HKTIndicator) aq.id(R.id.activity_myprofile_indictaor).getView();
        aq.id(R.id.activity_myprofile_indictaor).height(Math.max((int) getResources().getDimension(R.dimen.indicator_height), colWidth), false);
        hktindicator.setViewPager(viewPager);
        hktindicator.setCurrentItem(currentPage);
        hktindicator.initView(this, tapItemList, (int) getResources().getDimension(R.dimen.smalltext1size), getResources().getColor(R.color.hkt_textcolor), Color.TRANSPARENT, colWidth);
        hktindicator.setOnPageChangeListener(new OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                if (position == 0) {
                    firebaseSetting.setScreenName("MyProfLoginMainFragment", SCREEN_PROFILE_INFO);
                } else if (position == 1) {
                    firebaseSetting.setScreenName("MyProfServListFragment", SCREEN_PROFILE_SERVICE_LIST);
                } else {
                    firebaseSetting.setScreenName("ProfileContactFragment", SCREEN_PROFILE_HKT);
                }
                Utils.closeSoftKeyboard(me);
                // Refresh the MyProfLoginMainFragment when it is not current
                fragmentRefresh();
            }
        });
    }

    public void fragmentRefresh() {
        if (currentPage != 0) {
            ((MyProfLoginMainFragment) myProfLoginFragment).refresh();
        }
        if (currentPage == 1) {
            ((MyProfServListFragment) myProfServListFragment).refresh();
        }
        if (currentPage == 2) {
            ((ProfileContactFragment) myProfContactFragment).refresh();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                finish();
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                break;
        }
    }

    // interface
    public SveeRec getSveeRec() {
        return sveeRec;
    }

    public void setSveeRec(SveeRec sveeRec) {
        this.sveeRec = sveeRec;
    }

    public void closeActivity() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    @Override
    public void saveBcifRec(BcifRec bcifRec) {
        this.bcifRec = bcifRec;
    }

    @Override
    public BcifRec getBcifRec() {
        return bcifRec;
    }

    @Override
    public int getCurrentPage() {
        return currentPage;
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy())) {
                debugLog(TAG, "doHelo complete!!");
            } else if (APIsManager.RESET.equals(response.getActionTy())) {
                Toast.makeText(this, "Reset Success", Toast.LENGTH_SHORT)
                        .show();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
            }
        }
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        if (response != null) {
            if (APIsManager.RESET.equals(response.getActionTy())) {
                // TODO handle difference errorCode
            }

            // General Error Message
            if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                displayDialog(this, response.getMessage());
            } else {
                displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
            }
        }
    }

    @Override
    public void onAccountDeleted() {
        Log.d("lwg", "MyProfileActivity onAccountDeleted");
        ClnEnv.setPref(this.getApplicationContext(), this.getString(R.string.CONST_PREF_USERSAVELOGINID), false);
        ClnEnv.setPref(this.getApplicationContext(), this.getString(R.string.CONST_PREF_SAVELOGINID), false);
        ClnEnv.setPref(this.getApplicationContext(), this.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
        ClnEnv.setPref(this.getApplicationContext(), this.getString(R.string.CONST_PREF_LOGINID), "");
        ClnEnv.setEncPref(this.getApplicationContext(), "", this.getString(R.string.CONST_PREF_PASSWORD), "");
        ClnEnv.setPref(this.getApplicationContext(), this.getString(R.string.CONST_PREF_PREMIER_FLAG), false);
        ClnEnv.setSessionPremierFlag(false);
        ClnEnv.clear(me);

        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onFailedDeleted() {
// TODO handle difference errorCode
    }

    // View Pager Class
    private class MyProfPagerAdapter extends FragmentPagerAdapter {
        private static final int NUM_PAGER_VIEWS = 3;

        public MyProfPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return NUM_PAGER_VIEWS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            if (debug) Log.i(TAG, "FS:" + position);
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return myProfLoginFragment = new MyProfLoginMainFragment();
                // return new MembershipFragment();
                case 1: // Fragment # 1 - This will show FirstFragment different
                    return myProfServListFragment = new MyProfServListFragment();
                case 2: // Fragment # 2 - This will show FirstFragment different
                    return myProfContactFragment = new ProfileContactFragment();
                default:
                    return null;
            }
        }
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        closeActivity();
    }
}
