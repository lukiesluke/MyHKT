package com.pccw.myhkt.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;

import com.androidquery.AQuery;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegConfirmView;
import com.pccw.myhkt.lib.ui.RegImageButton;
import com.pccw.myhkt.model.SubService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/************************************************************************
 * File : RegConfirmActivity.java 
 * Desc : Reg Confirm 
 * Name : RegConfirmActivity
 * by : Andy Wong 
 * Date : 30/10/2015
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 30/10/2015 Andy Wong 		-First draft
 *************************************************************************/
public class RegAccInfoConfirmActivity extends BaseActivity {
    // Common Components
    private boolean debug = false;
    private static RegAccInfoConfirmActivity me;
    private String TAG = this.getClass().getName();
    private AQuery aq;
    private final int colMaxNum = 3;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int buttonPadding = 0;
    private int extralinespace = 0;
    private int regConfirmViewHeight = 0;
    private int greylineHeight = 0;
    private int basePadding = 0;
    private List<RegConfirmItem> regConfirmItemList;
    private RegImageButton regImageButon;
    private TextView textViewTc, textViewPri;

    private Button canceBtn;
    private Button confirmBtn;
    private HashMap<String, String> serviceMap;

    private CustRec custRec;
    private SveeRec sveeRec;
    private SubnRec subnRec;
    private AcMainCra acMainCra;
    private Boolean isHKID;

    private String domainURL = APIsManager.PRD_FTP;

    private static String errCode = "";
    private static boolean savePw = false;
    private static String loginId = "";
    private static String pwd = "";
    private static String newPwd = "";
    private static LgiCra lgicra = null;
    private static final String SCREEN_NAME = "Registration Account Info Confirm Screen";

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        me = this;
        debug = getResources().getBoolean(R.bool.DEBUG);

        acMainCra = (AcMainCra) this.getIntent().getExtras().getSerializable(RegAccInfoActivity.BUNDLE_ACMAIN);
        errCode = this.getIntent().getExtras().getString(RegAccInfoActivity.BUNDLE_ERRCODE);
        loginId = this.getIntent().getExtras().getString(RegAccInfoActivity.BUNDLE_LOGINID);
        savePw = this.getIntent().getExtras().getBoolean(RegAccInfoActivity.BUNDLE_SAVEPW);
        lgicra = (LgiCra) this.getIntent().getExtras().getSerializable(RegAccInfoActivity.BUNDLE_LOGINCRA);
        pwd = this.getIntent().getExtras().getString(RegAccInfoActivity.BUNDLE_PWD);
        newPwd = this.getIntent().getExtras().getString(RegAccInfoActivity.BUNDLE_NEWPWD);

        custRec = acMainCra.getICustRec();
        sveeRec = acMainCra.getISveeRec();
        subnRec = acMainCra.getISubnRec();
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));

        domainURL = ClnEnv.getPref(me, me.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
        setContentView(R.layout.activity_regconfirm);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        initData();
        super.onStart();
        setClickListener();
    }

    @Override
    protected final void onResume() {
        super.onResume();
        moduleId = getResources().getString(R.string.MODULE_REG);
        firebaseSetting.onScreenView(RegAccInfoConfirmActivity.this, SCREEN_NAME);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    private void initData() {
        //init the service map
        serviceMap = new HashMap<String, String>();
        serviceMap.put(SubnRec.LOB_PCD, Utils.getString(this, R.string.SHLM_LB_LOB_PCD));
        serviceMap.put(SubnRec.LOB_LTS, Utils.getString(this, R.string.SHLM_LB_LOB_LTS));
        serviceMap.put(SubnRec.LOB_TV, Utils.getString(this, R.string.SHLM_LB_LOB_TV));
        serviceMap.put(SubnRec.WLOB_X101, Utils.getString(this, R.string.SHLM_LB_LOB_1010));
        serviceMap.put(SubnRec.WLOB_CSL, Utils.getString(this, R.string.SHLM_LB_LOB_MOB));

        //* init the regconfirm list
        regConfirmItemList = new ArrayList<RegConfirmItem>();

        if (subnRec.lob.equals(SubnRec.LOB_TV)) {
            /* For TV, make sure srv_num is empty and acct_num has value*/
            regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_service, getResString(R.string.myhkt_confirm_head1), serviceMap.get(subnRec.lob) + "/" + subnRec.acctNum));
        } else {
            regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_service, getResString(R.string.myhkt_confirm_head1), serviceMap.get(subnRec.lob) + "/" + subnRec.srvNum));
        }

        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_hkid, custRec.docTy.equals(CustRec.TY_HKID) ? getResString(R.string.myhkt_confirm_hkid) : getResString(R.string.myhkt_confirm_passport), custRec.docNum));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_loginid, getResString(R.string.myhkt_confirm_loginid), sveeRec.loginId));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_nickname, getResString(R.string.myhkt_confirm_nickname), sveeRec.nickname));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_mobile_number_reset, getResString(R.string.myhkt_confirm_reset), sveeRec.ctMob));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        colWidth = (deviceWidth - (int) getResources().getDimension(R.dimen.extralinespace) * 2) / colMaxNum;
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        regConfirmViewHeight = (int) getResources().getDimension(R.dimen.reg_confirm_view_height);
        greylineHeight = (int) getResources().getDimension(R.dimen.greyline_height);

    }

    protected final void initUI2() {
        aq = new AQuery(this);
        AAQuery aaq = new AAQuery(this);
        aaq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aaq.navBarTitle(R.id.navbar_title, getResString(R.string.myhkt_reg_title));

        aq.id(R.id.navbar_button_left).clicked(this, "onBackPressed");
        aq.id(R.id.regconfirm_heading).text(getResString(R.string.myhkt_confirm_head2)).height(40);
        aq.id(R.id.regconfirm_heading).getTextView().setPadding(basePadding, extralinespace, basePadding, 0);

        regImageButon = (RegImageButton) aq.id(R.id.regconfirm_icon).getView();
        regImageButon.initViews(this, SubService.getResId(subnRec.lob), "", 0, 0, regConfirmViewHeight);
        regImageButon.setPadding(basePadding, extralinespace / 2, basePadding, extralinespace / 2);
        regImageButon.setBg(R.drawable.logo_container_gray);
        regImageButon.setVisibility(View.GONE);
        aq.id(R.id.regconfirm_view_service).visibility(View.GONE);

        setLine(R.id.regconfirm_line1);
        setLine(R.id.regconfirm_line2);
        setLine(R.id.regconfirm_line3);
        setLine(R.id.regconfirm_line4);
        setLine(R.id.regconfirm_line5);


        //init info list
        for (int itemCount = 0; itemCount < regConfirmItemList.size(); itemCount++) {

            RegConfirmView regConfirmView = (RegConfirmView) aq.id(regConfirmItemList.get(itemCount).res).getView();
            regConfirmView.initViews(this, regConfirmViewHeight, regConfirmItemList.get(itemCount).title, regConfirmItemList.get(itemCount).content);
            regConfirmView.setPadding(itemCount == 0 ? 0 : basePadding, 0, basePadding, 0);

        }

        aq.id(R.id.regconfirm_txt_agree).text(getResString(R.string.myhkt_confirm_ack1)).textColorId(R.color.hkt_ios_textcolor);
        aq.id(R.id.regconfirm_txt_tc).text(getResString(R.string.myhkt_confirm_ack2)).clicked(this, "onClick");
        aq.id(R.id.regconfirm_txt_pri).text(getResString(R.string.myhkt_confirm_ack3)).clicked(this, "onClick");

        textViewTc = aq.id(R.id.regconfirm_txt_tc).getTextView();
        textViewTc.setPaintFlags(textViewTc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textViewTc.setPadding(0, basePadding, 0, basePadding);
        textViewTc.setTextColor(this.getResources().getColor(R.color.hkt_ios_textcolor));

        textViewPri = aq.id(R.id.regconfirm_txt_pri).getTextView();
        textViewPri.setPaintFlags(textViewTc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textViewPri.setPadding(0, basePadding, 0, basePadding);
        textViewPri.setTextColor(this.getResources().getColor(R.color.hkt_ios_textcolor));

        aq.id(R.id.regconfirm_relative_layout).getView().setPadding(basePadding, extralinespace, basePadding, 0);


        canceBtn = (HKTButton) aq.id(R.id.regconfirm_btn_cancel).getView();
        canceBtn.setText(getResString(R.string.myhkt_confirm_cancel));
        LinearLayout.LayoutParams params_cancel = (LayoutParams) canceBtn.getLayoutParams();
        params_cancel.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params_cancel.rightMargin = buttonPadding;
        canceBtn.setLayoutParams(params_cancel);
        confirmBtn = (HKTButton) aq.id(R.id.regconfirm_btn_confirm).getView();
        confirmBtn.setText(getResString(R.string.myhkt_confirm_submit));
        LinearLayout.LayoutParams params_confirm = (LayoutParams) confirmBtn.getLayoutParams();
        params_confirm.leftMargin = buttonPadding;
        params_confirm.width = ViewGroup.LayoutParams.MATCH_PARENT;
        confirmBtn.setLayoutParams(params_confirm);
        confirmBtn.setEnabled(false);
        confirmBtn.setAlpha(0.2f);
        aq.id(R.id.regconfirm_checkbox).getCheckBox().setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    confirmBtn.setEnabled(false);
                    int m_color = Color.argb(200, 255, 255, 255);
                    confirmBtn.setAlpha(0.2f);
                } else {
                    confirmBtn.setEnabled(true);
                    confirmBtn.setAlpha(1.0f);
                }
            }

        });

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regconfirm_btn_confirm:
                if (aq.id(R.id.regconfirm_checkbox).isChecked()) {

                    Toast.makeText(v.getContext(), "Reg Success", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                    finish();
                } else {
                    DialogHelper.createSimpleDialog(RegAccInfoConfirmActivity.this, getResString(R.string.myhkt_confirm_tick));
                }
                break;
            case R.id.regconfirm_txt_tc: {
                navigateToBrowserActivity(isZh ? domainURL + "/mba/html/tnc_zh.htm" : domainURL + "/mba/html/tnc_en.htm",
                        getResString(R.string.myhkt_confirm_ack2));
                break;
            }
            case R.id.regconfirm_txt_pri: {
                navigateToBrowserActivity(isZh ? domainURL + "/mba/html/privacy_policy_zh.htm" : domainURL + "/mba/html/privacy_policy_en.htm",
                        getResString(R.string.myhkt_confirm_ack3));
                break;
            }
        }
    }

    private void navigateToBrowserActivity(String url, String title) {
        Intent intent = new Intent(getApplicationContext(), BrowserActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BrowserActivity.INTENTMODULEID, moduleId);
        bundle.putString(BrowserActivity.INTENTLINK, url);
        bundle.putString(BrowserActivity.INTENTTITLE, title);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    //Set onClickListener for the views
    public void setClickListener() {
        confirmBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (aq.id(R.id.regconfirm_checkbox).isChecked()) {
                    doRegister();
                } else {
                    DialogHelper.createSimpleDialog(RegAccInfoConfirmActivity.this, getResString(R.string.myhkt_confirm_tick));
                }
            }
        });

        canceBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setLine(int resId) {
        aq.id(resId).image(R.drawable.greyline).height(greylineHeight, false);
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        //
        //		Intent intent = new Intent(getApplicationContext(), RegBasicActivity.class);
        //		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //		startActivity(intent);
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private final void doRegister() {
        APIsManager.doAppendUserInfo(me, acMainCra);
    }

    public void debugLog(String tag, String msg) {
        if (debug) Log.i(tag, msg);
    }

    private final static boolean isReqSpecMsg4DocTy() {
        /* TODO - Remove when Not Necessary */
        /* Special Handling for 101 and O2F */

        String rLob;
        if (me.subnRec == null || me.custRec == null) {
            return false;
        }

        rLob = me.subnRec.lob;
        if (rLob.equals(SubnRec.LOB_101)) {
            return me.custRec.docTy == CustRec.TY_PASSPORT;
        } else if (rLob.equals(SubnRec.LOB_O2F)) {
            return me.custRec.docTy == CustRec.TY_PASSPORT;
        } else if (rLob.equals(SubnRec.LOB_MOB) || rLob.equals(SubnRec.LOB_IOI)) {
            if (me.subnRec.acctNum.trim().length() == SubnRec.CSL_ACCTNUM_LEN) {
                return me.custRec.docTy == CustRec.TY_PASSPORT;
            }
        }
        return (false);
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
        if (response != null) {
            if (APIsManager.ECLOSION.equals(response.getActionTy())) {
                AcMainCra mAcMainCra = (AcMainCra) response.getCra();
                //dialog redirect to mainmenu onclick
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVELOGINID), true);
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LOGINID), loginId);
            }

            if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
                ClnEnv.setSessionLoginID(loginId);
                ClnEnv.setSessionPassword(newPwd);
            } else if (errCode.equals(RC.FILL_SVEE4CLAV) || errCode.equals(RC.FILL_SVEE4CLUB) || errCode.equals(RC.FILL_SVEE_NOTNC) || errCode.equals(RC.FILL_SVEE4CLBL_NOTNC)) {
                if ("Y".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().pwdEff) && "V".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().state)) {
                    ClnEnv.setSessionLoginID(loginId);
                    ClnEnv.setSessionPassword(pwd);
                } else {
                    ClnEnv.setSessionLoginID(loginId);
                    ClnEnv.setSessionPassword(newPwd);
                }
            }
            if (savePw) {
                // Saving password implies saving loginid
                if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
                    ClnEnv.setEncPref(me.getApplicationContext(), loginId, me.getString(R.string.CONST_PREF_PASSWORD), newPwd);
                } else if (errCode.equals(RC.FILL_SVEE4CLAV) || errCode.equals(RC.FILL_SVEE4CLUB) || errCode.equals(RC.FILL_SVEE_NOTNC) || errCode.equals(RC.FILL_SVEE4CLBL_NOTNC)) {
                    if ("Y".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().pwdEff) && "V".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().state)) {
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
                        ClnEnv.setEncPref(me.getApplicationContext(), loginId, me.getString(R.string.CONST_PREF_PASSWORD), pwd);
                    } else {
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
                        ClnEnv.setEncPref(me.getApplicationContext(), loginId, me.getString(R.string.CONST_PREF_PASSWORD), newPwd);
                    }
                }
            } else {
                // Not saving password - remove the saved password
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
                ClnEnv.setEncPref(me.getApplicationContext(), "", me.getString(R.string.CONST_PREF_PASSWORD), "");
            }
            redirectMainMenuDialog(Utils.getString(me, R.string.SFRM_SUCCREG_4FILL));
        }
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        if (response != null) {
            if (APIsManager.ECLOSION.equals(response.getActionTy())) {
                AcMainCra mAcMainCra = (AcMainCra) response.getCra();
                //				if (mAcMainCra.getReply().isEqual(Reply.RC_IVSESS) || mAcMainCra.getReply().isEqual(Reply.RC_ALT)) {
                //					// RC_IVSESS
                //					// RC_ALT
                //					me.redirectDialog(Utils.getString(me, R.string.DLGM_ABORT_IVSESS));
                //				} else {
                // RC_IVDATA
                // RC_NOT_AUTH
                // RC_INACTIVE_RCUS
                // RC_UXSVLTERR
                DialogHelper.createSimpleDialog(me, InterpretRCManager.interpretRC_RegnMdu(me, mAcMainCra.getReply()));
                //					me.regnGrq = null;
                //				}
            } else {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    displayDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    ivSessDialog();
                } else {
                    DialogHelper.createSimpleDialog(me, InterpretRCManager.interpretRC_RegnMdu(me, response.getReply()));
                }
            }
        }
    }

    // redirect to login screen if RC_IVSESS
    protected final void redirectMainMenuDialog(String message) {
        AlertDialog.Builder builder = new Builder(me);
        builder.setMessage(message);
        builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                me.finish();
            }
        });
        builder.setOnCancelListener(new Dialog.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                Intent intent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                me.finish();
            }
        });
        builder.create().show();
    }

    public static class RegConfirmItem {
        String title;
        String content;
        int res;

        RegConfirmItem(int res, String title, String content) {
            this.res = res;
            this.title = title;
            this.content = content;
        }
    }
}