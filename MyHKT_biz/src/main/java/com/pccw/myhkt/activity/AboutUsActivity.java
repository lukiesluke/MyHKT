package com.pccw.myhkt.activity;

import java.lang.Thread.UncaughtExceptionHandler;

import androidx.appcompat.app.AlertDialog;
import android.app.Dialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegInputItem;


public class AboutUsActivity extends BaseActivity{
	private AAQuery 	aq;
	private SveeRec sveeRec = null;
	private String 	TAG = this.getClass().getName();

	private int 					extralinespace;
	private int 					gridlayoutPadding = 0;
	private int 					buttonPadding = 0;
	private int 					deviceWidth = 0;
	private int  					basePadding = 0;
	private int 					colWidth = 0;

	private String					loginId;
	private String					pwd;

	private RegInputItem 			regInputLoginId;
	private RegInputItem 			regInputActCode;

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		me = this;
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		setContentView(R.layout.activity_aboutus);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		gridlayoutPadding = (int) getResources().getDimension(R.dimen.mainmenu_gridlayout_padding);
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		
	}
	
	protected final void initUI2() {
		aq = new AAQuery(this);

		//navbar
		aq.navBarBaseLayout(R.id.navbar_base_layout);
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);	
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.MYHKT_ABOUT_US_TITLE));
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");		

		
		//Set the size for the mainmenu_btn_info
		Drawable drawable = getResources().getDrawable(R.drawable.about_us_premier);
		int drawableWidth = drawable.getIntrinsicWidth();
		int drawableHeight = drawable.getIntrinsicHeight();
		drawable = null;
		int viewWidth = deviceWidth - extralinespace * 2;
		int viewHeight = drawableHeight * viewWidth / drawableWidth;
		aq.id(R.id.aboutus_pic).height(viewHeight, false).width(viewWidth, false);	

		drawable = null;
		drawableWidth = 0;
		drawableHeight = 0;
		viewWidth = 0;
		viewHeight  = 0;		

	}

	@Override
	protected final void onResume() {
		super.onResume();
		moduleId = getResources().getString(R.string.MODULE_REG);
		
		// Screen Tracker
	}
	
	// Android Device Back Button Handling
	public final void onBackPressed() {
		finish();
	}
}
