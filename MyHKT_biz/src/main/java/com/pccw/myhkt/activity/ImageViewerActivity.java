package com.pccw.myhkt.activity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.BillList;
import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.wheat.shared.tool.Reply;

/************************************************************************
 * File : ImageViewerActivity.java
 * Desc : ImageViewerActivity
 * Name : ImageViewerActivity
 * by 	: Andy Wong
 * Date : 31/03/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 31/03/2015 Andy Wong 		-First draft
 *************************************************************************/

public class ImageViewerActivity extends BaseActivity implements OnAPIsListener {
	private boolean 			debug = false;
	private String 			TAG = this.getClass().getName();
	private AAQuery 			aq;

	//View
	private Activity 				me;
	private static int lob = 0;
	private static int type = 0;
	private static String navbarText = "";
	private String imageUrl = "";
	private int deviceWidth;
	private ImageView zoom ;

	private WebView webView;
	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		debug = getResources().getBoolean(R.bool.DEBUG);
		initData();

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			imageUrl = bundle.getString("IMAGEURL") != null ? bundle.getString("IMAGEURL") : "";
			lob = bundle.getInt("LOBICON");
			type = bundle.getInt("LOBTYPE");
			if (debug) Log.i(TAG, "i2" + lob + "/ " + type);
			navbarText = bundle.getString("NAVTEXT") != null ? bundle.getString("NAVTEXT") : "";
		}
		setContentView(R.layout.activity_imageviewer);
	}

	private void initData(){
		aq = new AAQuery(this);
		me = this;
	}

	@Override
	protected void initUI2(){
		//navbar
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);	
		if (lob > 0) {
			aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, 0 , Utils.getLobIcon(lob, 0) , 0, navbarText);
		} else {
			aq.navBarTitle(R.id.navbar_title,navbarText);	
		}
		
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");

		if (debug) Log.i(TAG, "i1");
		if (lob == R.string.CONST_LOB_LTS) {
			if (debug) Log.i(TAG, "i2");
			String rightText = Utils.getLtsTtypeRightText(this, type);
			if (debug) Log.i(TAG, "i2" + rightText + lob + "/ " + type);
			aq.id(R.id.navbar_righttext).text(rightText).textColorId(R.color.hkt_txtcolor_grey);
		}

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		String widthHeightAttr = "";
		String minScale = "";

		widthHeightAttr = "width='100%'";
		minScale = String.format("%f", (float)deviceWidth/(float)2362);

		//     	<html><meta name='viewport' content='width=320px,initial-scale=0.304826,minimum-scale=0.304826, maximum-scale=2.0' /><body bgcolor='#FFFFFF' style='margin:0px; height:100%;'><div style='width=100%; height:100%; display:table;'><img src = 'https://cspuat.pccw.com/cs/MD_2B_L2.png' width='100%' style="position:absolute; margin-top:auto;margin-bottom:auto;top:0;bottom:0;"></div></body></html>
		String data = String.format("<html><meta name='viewport' content='width=320px,initial-scale=%s,minimum-scale=%s, maximum-scale=2.0' /><body bgcolor='#FFFFFF' style='margin:0px; height:100%%;'><div style='width=100%%; height:100%%; display:table; vertical-align:middle;'><img style='position:absolute; margin-top:auto;margin-bottom:auto;top:0;bottom:0;' src = '%s' %s></div></body></html>"
				,minScale,minScale,imageUrl,widthHeightAttr);
		webView = aq.id(R.id.yourwebview).getWebView();
		//		   if (debug) Log.d("ImageViewerActivity", data);
		if (debug) Log.d("ImageViewerActivity", data);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setDisplayZoomControls(false);
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);
		webView.loadDataWithBaseURL("",data, "text/html","UTF-8", "");

		zoom = aq.id(R.id.imageviewer_icon_zoom).getImageView();
		//Init Zoom Icon Animation
		zoom.setVisibility(View.VISIBLE);
		zoom.bringToFront();
		aq.id(R.id.imageviewer_icon_zoom).image(R.drawable.icon_zoom_anim);
		final AnimationDrawable frameAnimation = (AnimationDrawable)zoom.getDrawable();
		frameAnimation.setCallback(zoom);
		frameAnimation.setVisible(true, true);
		frameAnimation.start();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				frameAnimation.stop();
				frameAnimation.setVisible(false, false);
				if (zoom != null)
					zoom.setVisibility(View.GONE);
			}
		}, 3000);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			onBackPressed();
			break;
		}
	}	
	
	@Override
	protected final void onResume() {
		super.onResume();
		// Update Locale	
		moduleId = getResources().getString(R.string.MODULE_LGIF);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
	}
}