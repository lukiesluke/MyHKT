package com.pccw.myhkt.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import com.google.gson.Gson;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.BinqCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.Account;
import com.pccw.dango.shared.entity.Bill;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnLiveChatListener;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnServiceListener;
import com.pccw.myhkt.fragment.BillFragment;
import com.pccw.myhkt.fragment.LnttFragment;
import com.pccw.myhkt.fragment.PlanLTSFragment;
import com.pccw.myhkt.fragment.PlanPCDFragment;
import com.pccw.myhkt.fragment.PlanTVFragment;
import com.pccw.myhkt.fragment.UsageFragment;
import com.pccw.myhkt.fragment.VasFragment;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTIndicator;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.model.TabItem;
import com.pccw.myhkt.mymob.model.MyMobAcctAgent;

import java.util.ArrayList;
import java.util.List;

/************************************************************************
 * File : ServiceActivity.java
 * Desc : Service Main Page
 * Name : ServiceActivity
 * by 	: Andy Wong
 * Date : 16/11/2015
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 16/11/2015 Andy Wong 		-First draft
 * 26/01/2016 Derek Tsui		-core functions update
 *************************************************************************/

public class ServiceActivity extends BaseActivity implements OnAPIsListener, OnServiceListener, OnLiveChatListener {
    private boolean debug = false;
    private String TAG = this.getClass().getName();
    private AAQuery aq;
    private final int colMaxNum = 3;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int buttonPadding = 0;
    private int extralinespace = 0;
    private int greylineHeight = 0;
    private int tabPos = 0;
    private int activeSubView;

    private int txtColor;
    private int textSize;
    private boolean isRememberPw = true;
    private String userid;
    private int bgColor = R.color.white;
    private String navBarTitle = "";

    private List<TabItem> tabItemList = new ArrayList<TabItem>();
    private List<Bundle> bundleList = new ArrayList<Bundle>();

    private HKTIndicator hktindicator;
    private ViewPager viewPager;

    //View
    private Activity me;

    private ServicePagerAdapter servicePagerAdapter;
    // Fragment
    private FragmentManager fragmentManager;
    private BillFragment billFragment;
    private UsageFragment usageFragment;
    private UsageFragment roamingFragment;
    private PlanTVFragment planTVFragment;
    private PlanPCDFragment planPCDFragment;
    private PlanLTSFragment planLtsFragment;
    private LnttFragment lnttFragment;
    private VasFragment vasFragment;

    protected String trackerId = "";

    protected SubnRec assocSubnRec;
    protected Account account = null;
    protected AcctAgent acctAgent = null; //MyMobList AcctAgent
    protected MyMobAcctAgent myMobAcctAgent = null; //Original AcctAgent

    //	protected int				activeSubview	= R.string.CONST_SELECTEDVIEW_BILLSUMMARY;	// Initial default subview
    protected int lob;
    protected String lobString;
    protected int ltsType;
    //	protected BupdCra			bupdCra			= null;
    protected BinqCra binqCra = null;

    protected Bill[] billary;

    // Line Test
    protected LnttCra lnttCra = null;
    protected ApptCra apptCra = null;
    protected LnttAgent lnttAgent = null;
    protected boolean showLnttResult = false;
    protected boolean isLnttServiceClearable = true;
    // Zombie account
    protected boolean isZombie = false;
    protected boolean isMyMobAcct = false;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        if (debug) Log.i("SERVICE ACTIVITY", "ONCREATE");
        lnttFragment = new LnttFragment();
        planLtsFragment = new PlanLTSFragment();
        usageFragment = new UsageFragment();

        Bundle bundle_usage = new Bundle();
        bundle_usage.putBoolean("islocal", true);
        usageFragment.setArguments(bundle_usage);
        roamingFragment = new UsageFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putBoolean("islocal", false);
        roamingFragment.setArguments(bundle1);

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));

        Bundle bundle = this.getIntent().getExtras();
        isMyMobAcct = bundle.getBoolean("ISMYMOBACCT", false);

        // We need to ensure the user is still logged on
        if (!ClnEnv.isLoggedIn() && !isMyMobAcct) {
            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
            return;
        }

        try {
            acctAgent = (AcctAgent) bundle.getSerializable("ACCTAGENT");

            if (bundle.containsKey("SUBNREC")) {
                assocSubnRec = (SubnRec) bundle.getSerializable("SUBNREC");
            } else {
                assocSubnRec = acctAgent.getSubnRec();
            }

            //derek
            if (debug) Log.i("serviceAct: ", acctAgent.getAcctNum());
            if (debug) Log.i("serviceActAccNum: ", assocSubnRec.acctNum);

            isZombie = !acctAgent.isLive();
            lob = getLob(assocSubnRec);
            lobString = getLobString(assocSubnRec);

            //set flag for 1010 theme in MyMob
            ClnEnv.setIs101Flag(lob == R.string.CONST_LOB_1010);

            if (lob == R.string.CONST_LOB_LTS) {
                ltsType = Utils.getLtsSrvType(assocSubnRec.tos, assocSubnRec.eyeGrp, assocSubnRec.priMob);
            }
            // If New Bill Indicator is enabled and unread bill flag = true, set Bill Detail to be default Screen
            //							try {
            //								if (bundle.getInt("CLICKBUTTON", 0) == R.id.idd_icon_rates && lob == R.string.CONST_LOB_LTS) {
            //									activeSubview = R.string.CONST_SELECTEDVIEW_IDDRATES;
            //								} else if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true) && account != null && !isMyMobAcct) {
            //									saveAccountHelper = SaveAccountHelper.getInstance(this);
            //									// Only update record if unread bill flag = true
            //									if (saveAccountHelper.getflagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), account.getAcctNum())) {
            //										activeSubview = R.string.CONST_SELECTEDVIEW_BILL;
            //									}
            //								}
            //							} catch (Exception e) {
            //								// fail in any Database error
            //							}

            //Check for the case that should not clear the lnttService
            if (debug) Log.i(TAG, "1LnttAngent" + new Gson().toJson(Utils.getPrefLnttAgent(this)));
//			if (lob == R.string.CONST_LOB_LTS || lob == R.string.CONST_LOB_PCD || lob == R.string.CONST_LOB_TV) {
            lnttAgent = Utils.getPrefLnttAgent(this);
            String lnttAgentStr = ClnEnv.getPref(this, this.getString(R.string.CONST_PREF_LNTT_AGENT), "");
            //Line test is running
            if (!lnttAgentStr.equals("") && lnttAgent != null && "".equalsIgnoreCase(lnttAgent.getEndTimestamp())) {
                this.setIsLnttServiceClearable(false);
            }
//				//Should not clear LnntAgent is not belong to this service 
            if (!lnttAgentStr.equals("") && lnttAgent != null && !(lnttAgent.getLnttSrvNum().equalsIgnoreCase(assocSubnRec.srvNum) && assocSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum))) {
                this.setIsLnttServiceClearable(false);
            }
//			}
            //								If LineTest returned Result, activeSubview == R.string.CONST_SELECTEDVIEW_TVLINETEST3;
            //				Assume Expiry checking at ServiceListActivity
            if (lob == R.string.CONST_LOB_LTS || lob == R.string.CONST_LOB_PCD || lob == R.string.CONST_LOB_TV) {
                lnttAgent = Utils.getPrefLnttAgent(this);
                if (lnttAgent.getLnttSrvNum().equalsIgnoreCase(assocSubnRec.srvNum) && assocSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum)) {
                    if (debug)
                        Log.i(TAG, lnttAgent.getEndTimestamp() + "/" + lnttAgent.getResultMsg());
                    if (!"".equalsIgnoreCase(lnttAgent.getResultMsg())) {
                        // If LineTest is imcompleted, Server returned Error/ Expired Result. Then redirected to Start LineTest and show Error Dialog
                        showLnttResult = false;
                        activeSubView = R.string.CONST_SELECTEDVIEW_LINETEST;
                        tabPos = 2;
                    } else if (!"".equalsIgnoreCase(lnttAgent.getEndTimestamp())) {
                        showLnttResult = true;
                        activeSubView = R.string.CONST_SELECTEDVIEW_LINETEST;
                        tabPos = 2;
                    }

                    // Reset the Notification flag to be false
                    ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);
                }
            }

            //display usage first if its open from MyMobile view
            if (ClnEnv.isMyMobFlag()) {
                if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_O2F) {
                    tabPos = 1;
                }
            }
            //			} else {
            //				// the requested account is from acctAry, we already have the account object
            //				isZombie = true;
            //				//				account = (Account) bundle.getSerializable("ACCOUNT");
            //				acctAgent = (AcctAgent) bundle.getSerializable("ACCTAGENT");
            //				lob = getLob(assocSubnRec);
            //
            //				// For zombie account we go straight to the bill detail screen
            //				//				activeSubview = R.string.CONST_SELECTEDVIEW_BILL;
            //			}
        } catch (Exception e) {
            e.printStackTrace();
            // App state unknown.
            // the safest way to handle this situation is to return to the caller.
            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        }

        //		switch (lob) {
        //			case R.string.CONST_LOB_LTS:
        //				setContentView(R.layout.activity_servicelts);
        //				break;
        //			case R.string.CONST_LOB_TV:
        //				setContentView(R.layout.activity_servicetv);
        //				break;
        //			case R.string.CONST_LOB_PCD:
        //				setContentView(R.layout.activity_servicepcd);
        //				break;
        //			case R.string.CONST_LOB_MOB:
        //				setContentView(R.layout.activity_servicemob);
        //				break;
        //			case R.string.CONST_LOB_1010:
        //			case R.string.CONST_LOB_O2F:
        //				setContentView(R.layout.activity_service1010);
        //				break;
        //			default:
        //				break;
        //		}


        //		try {
        //			initUI();
        //			displaySubview();
        //		} catch (Exception e) {
        //			// Fail-safe: return to the previous screen
        //			// whenever we have unexpected error when resuming
        //			e.printStackTrace();
        //			finish();
        //			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        //		}

        initData();
        setContentView(R.layout.activity_service);
    }

    protected void onRestart() {
        if (debug) Log.i(TAG, "onReStart" + callOnStart);
        Gson gson = new Gson();
        if (lnttAgent != null) {
            if (debug) Utils.showLog(TAG, gson.toJson(lnttAgent));
        }
        super.onRestart();
        if (debug) Log.i(TAG, "onReStart1" + callOnStart);
    }

    private void initData() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        greylineHeight = (int) getResources().getDimension(R.dimen.greyline_height);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        txtColor = getResources().getColor(R.color.hkt_txtcolor_grey);

        textSize = (int) getResources().getDimension(R.dimen.bodytextsize);
        userid = "";
        me = this;

        fragmentManager = getSupportFragmentManager();
        tabItemList = new ArrayList<TabItem>();
        TabItem tabItemBillSum = new TabItem(R.drawable.bill_n_summary, getResString(R.string.myhkt_title_billAndSummary), 0);
        tabItemBillSum.setPageConst(R.string.CONST_SELECTEDVIEW_BILLSUMMARY);
        tabItemList.add(tabItemBillSum);

        //Screen Tracker

        switch (lob) {
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
                //tracker

                break;
            case R.string.CONST_LOB_1010:
            case R.string.CONST_LOB_O2F:

                break;
            case R.string.CONST_LOB_PCD:
                //tracker

                break;
            case R.string.CONST_LOB_TV:
                //tracker
                break;
            case R.string.CONST_LOB_LTS:
                //tracker
                break;
        }
        //tracker part 2

        //Moiz Added: Empty Tab Views
        tabItemList.add(new TabItem(-1, null, 2));
        tabItemList.add(new TabItem(-1, null, 3));

        colWidth = (deviceWidth - basePadding * 2) / tabItemList.size();
        servicePagerAdapter = new ServicePagerAdapter(getSupportFragmentManager());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        super.onStart();
        //			 If there is Bill Message, activeSubView == R.string.CONST_SELECTEDVIEW_LTSBILL;
        //			 dialog with bill message will be displayed
        if (ClnEnv.getPushDataBill() != null) {
            if (ClnEnv.getPushDataBill().getAcctNum().equalsIgnoreCase(assocSubnRec.acctNum)) {

                //prepare custom message for bill dialog
                String fullMsg = "";
                String serviceNum = "";
                String acctNum = getString(R.string.MYHKT_PN_MSG_BILL_ACCNUM) + " ";
                String overdueAmt = getString(R.string.MYHKT_PN_MSG_BILL_AMT) + " ";
                String remark = getString(R.string.MYHKT_PN_MSG_BILL);

                //acct number
                if (assocSubnRec.acctNum != null && !"".equals(assocSubnRec.acctNum)) {
                    acctNum = acctNum + assocSubnRec.acctNum + "\n";
                    fullMsg = fullMsg + acctNum;
                }

                //service number
                int ltsType = 0;
                if (assocSubnRec.lob.equals(SubnRec.LOB_LTS)) {
                    ltsType = Utils.getLtsSrvType(assocSubnRec.tos, assocSubnRec.eyeGrp, assocSubnRec.priMob);
                    if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
                        int last4index = (assocSubnRec.srvNum.length() - 4) < 0 ? 0 : assocSubnRec.srvNum.length() - 4;
                        serviceNum = String.format("CARD%s", assocSubnRec.srvNum.substring(last4index)) + "\n";
                    } else {
                        serviceNum = assocSubnRec.srvNum + "\n";
                    }
                } else {
                    serviceNum = assocSubnRec.srvNum + "\n";
                }

                if (assocSubnRec.lob.equals(SubnRec.LOB_MOB) || assocSubnRec.lob.equals(SubnRec.LOB_101) ||
                        assocSubnRec.lob.equals(SubnRec.LOB_IOI) || assocSubnRec.lob.equals(SubnRec.LOB_O2F)) {
                    fullMsg = fullMsg + getString(R.string.LTTF_MOB_ACCTNUM) + " " + serviceNum;
                } else if (assocSubnRec.lob.equals(SubnRec.LOB_LTS)) {
                    fullMsg = fullMsg + getString(R.string.LTTF_LTS_ACCTNUM) + " " + serviceNum;
                } else if (assocSubnRec.lob.equals(SubnRec.LOB_TV)) {
//					fullMsg = fullMsg + getString(R.string.LTTF_TV_ACCTNUM) + " " + serviceNum; //removed to prevent account number showing twice (for TV only)
                } else if (assocSubnRec.lob.equals(SubnRec.LOB_PCD)) {
                    fullMsg = fullMsg + getString(R.string.LTTF_PCD_ACCTNUM) + " " + serviceNum;
                }

                //overdue amt TODO inserting fake value not finished yet
                if (ClnEnv.getPushDataBill().getMessage().contains("$")) {
                    String str = ClnEnv.getPushDataBill().getMessage();
                    String[] parts = str.split("\\$");
                    String part2 = parts[1];
                    String amt = part2.substring(0, part2.indexOf("."));
                    String amtDec = part2.substring(part2.indexOf("."), part2.indexOf(".") + 3);
                    String amount = "$" + amt + amtDec;
                    fullMsg = fullMsg + overdueAmt + amount + "\n";
                }

                //remark
                fullMsg = fullMsg + "\n" + remark;

                DialogHelper.createSimpleDialog(this, fullMsg);
            }
            // Reset the Notification flag to be false
            ClnEnv.getPushDataBill().clear();
            ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
        }
    }

    @Override
    protected final void onResume() {
        if (debug) Log.i("SERVICE ACTIVITY", "ONRESUME");
        super.onResume();
        // Update Locale
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onPause() {
        super.onPause();
    }

    @Override
    protected final void onDestroy() {
        super.onDestroy();
        ClnEnv.setIs101Flag(false);
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        closeActivity();
    }

    public void closeActivity() {
        if (this.isLnttServiceClearable) {
            Utils.clearLnttService(me);
        }
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    protected void initUI2() {
        aq = new AAQuery(this);

        //navbar
        int rightImageViewID = 0;
        int rightDrawDrawable = 0;
        String rightText = "";
        //Show only for non zombie and LTS
        if (lob == R.string.CONST_LOB_LTS && !isZombie) {
            rightText = Utils.getLtsTtypeRightText(this, ltsType);
        }
        //Add by Andy ZHOU for bug fix
        if (isZombie) {
            navBarTitle = "*" + assocSubnRec.acctNum + getResString(R.string.LABEL_EXPIRED);
        } else {
            navBarTitle = assocSubnRec.srvNum;
        }
        //		rightText = "";
        //		navBarTitle = "a60058422" ;
        //		lob = R.string.CONST_LOB_PCD;

        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, rightImageViewID, Utils.getLobIcon(lob, ltsType, assocSubnRec.tos, isZh), rightDrawDrawable, navBarTitle);
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");
        aq.id(R.id.navbar_righttext).text(rightText).textColorId(R.color.hkt_txtcolor_grey);

        activeSubView = tabItemList.get(tabPos).pageConst;
        viewPager = (ViewPager) aq.id(R.id.activity_myprofile_view).getView();
        if (viewPager.getAdapter() == null) {
            if (debug) Log.i(TAG, "onStartSet");
            viewPager.setAdapter(servicePagerAdapter);
            if (debug) Log.i(TAG, "onStartEnd");
            hktindicator = (HKTIndicator) aq.id(R.id.activity_myprofile_indictaor).getView();
            aq.id(R.id.activity_myprofile_indictaor).height(Math.max((int) getResources().getDimension(R.dimen.indicator_height), colWidth), false);
            if (ClnEnv.isMyMobFlag()) {   // MyMob's MyAccount Login default displaying "Usage & Plan" Screen
                if (lob == R.string.CONST_LOB_1010) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_1010USAGE;
                    setModuleId(getResString(R.string.MODULE_101_MM_PLAN));
                } else if (lob == R.string.CONST_LOB_O2F) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_1010USAGE;
                    setModuleId(getResString(R.string.MODULE_O2F_MM_PLAN));
                } else if (lob == R.string.CONST_LOB_MOB) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_MOBUSAGE;
                    setModuleId(getResString(R.string.MODULE_MOB_MM_PLAN));
                } else if (lob == R.string.CONST_LOB_IOI) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_MOBUSAGE;
                    setModuleId(getResString(R.string.MODULE_IOI_MM_PLAN));
                }
                tabPos = 1;
            }
            hktindicator.setViewPager(viewPager, tabPos);
            hktindicator.initView(this, tabItemList, (int) getResources().getDimension(R.dimen.smalltext1size), getResources().getColor(R.color.hkt_textcolor), Color.TRANSPARENT, colWidth);
            hktindicator.setOnPageChangeListener(new OnPageChangeListener() {

                @Override
                public void onPageScrollStateChanged(int arg0) {

                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void onPageSelected(int position) {
                    tabPos = position;
                    activeSubView = tabItemList.get(position).pageConst;
                    if (debug) Log.i(TAG, activeSubView + "/" + position);
                    Utils.closeSoftKeyboard(me);
                    fragmentRefresh(activeSubView);
                }
            });
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.navbar_button_right:
                //livechat
                break;
        }
    }

    public void fragmentRefresh(int avtiveSubView) {
        //		if (currentPage !=0){((MyProfLoginMainFragment)myProfLoginFragment).refresh();	}
        //		Log.i("ServiceActivity", "Sel page: " +tabPos + "/nLob: " + getLob()) ;
        //LTS, refresh the view to first page when it is not on current page
        if (lob == R.string.CONST_LOB_LTS) {
            //			if (activeSubView != R.string.CONST_SELECTEDVIEW_LTSPLAN ){
            //				if (planLtsFragment !=null) {
            //					planLtsFragment.refresh();
            //				}
            //			}
            //			if (activeSubView != R.string.CONST_SELECTEDVIEW_LINETEST ){
            //				if (ltsType ==  R.string.CONST_LTS_ONECALL || ltsType ==  R.string.CONST_LTS_ICFS || ltsType ==  R.string.CONST_LTS_CALLINGCARD ) {
            //				} else {
            //					if (lnttFragment !=null) {
            //						lnttFragment.refresh();
            //					}
            //				}
            //			}
        }
        switch (activeSubView) {
            case R.string.CONST_SELECTEDVIEW_BILLSUMMARY:
                if (billFragment != null) {
                    billFragment.refresh();
                }
                break;
            //Plan
            case R.string.CONST_SELECTEDVIEW_PCDPLAN:
                if (planPCDFragment != null) {
                    planPCDFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_TVPLAN:
                if (planTVFragment != null) {
                    planTVFragment.refreshData();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_LTSPLAN:
                if (planLtsFragment != null) {
                    planLtsFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_LINETEST:
            case R.string.CONST_SELECTEDVIEW_LINETEST1:
            case R.string.CONST_SELECTEDVIEW_LINETEST3:
                if (lnttFragment != null) {
                    lnttFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_MOBUSAGE:
            case R.string.CONST_SELECTEDVIEW_1010USAGE:
                if (lob == R.string.CONST_LOB_MOB) {
                    moduleId = getString(R.string.MODULE_MOB_PLAN);
                } else if (lob == R.string.CONST_LOB_IOI) {
                    moduleId = getString(R.string.MODULE_IOI_PLAN);
                } else if (lob == R.string.CONST_LOB_1010) {
                    moduleId = getString(R.string.MODULE_101_PLAN);
                } else if (lob == R.string.CONST_LOB_O2F) {
                    moduleId = getString(R.string.MODULE_O2F_PLAN);
                }
                if (usageFragment != null) {
                    usageFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_MOBROAMING:
            case R.string.CONST_SELECTEDVIEW_1010ROAMING:
                if (roamingFragment != null) {
                    roamingFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDFRAG_VAS:
                if (vasFragment != null) {
                    vasFragment.refreshData();
                }
                break;
            default:
        }
    }

    // View Pager Class
    private class ServicePagerAdapter extends FragmentPagerAdapter {

        public ServicePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            int pageCount = 0;
            for (TabItem tabItem : tabItemList) {
                if (tabItem.text != null && !tabItem.text.equals("")) {
                    pageCount++;
                }
            }
            return pageCount;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            //			Log.i(TAG, "FS:" + position);
            //			if (ltsType ==  R.string.CONST_LTS_ONECALL || ltsType ==  R.string.CONST_LTS_ICFS || ltsType ==  R.string.CONST_LTS_CALLINGCARD ) {
            //				tabItemList.add(new TabItem(-1, null, 2));
            //			} else {
            //				tabItemList.add(new TabItem(R.drawable.linetest, getResString(R.string.myhkt_title_livetest), 2));
            //			}

            switch (tabItemList.get(position).pageConst) {
                case R.string.CONST_SELECTEDVIEW_BILLSUMMARY:
                    billFragment = new BillFragment();
                    return billFragment;
                case R.string.CONST_SELECTEDVIEW_PCDPLAN:
                    planPCDFragment = new PlanPCDFragment();
                    return planPCDFragment;
                case R.string.CONST_SELECTEDVIEW_TVPLAN:
                    planTVFragment = new PlanTVFragment();
                    return planTVFragment;
                case R.string.CONST_SELECTEDVIEW_LTSPLAN:
                    //					planLtsFragment = new PlanLTSFragment();
                    return planLtsFragment;
                case R.string.CONST_SELECTEDVIEW_LINETEST:
                case R.string.CONST_SELECTEDVIEW_LINETEST1:
                case R.string.CONST_SELECTEDVIEW_LINETEST3:
                    //					lnttFragment = new LnttFragment();
                    return lnttFragment;
                case R.string.CONST_SELECTEDVIEW_MOBUSAGE:
                case R.string.CONST_SELECTEDVIEW_1010USAGE:
                    //				if (usageFragment == null) {
                    //					usageFragment = new UsageFragment();
                    //					Bundle bundle = new Bundle();
                    //					bundle.putBoolean("islocal", true);
                    //					usageFragment.setArguments(bundle);
                    //				}
                    return usageFragment;
                case R.string.CONST_SELECTEDVIEW_MOBROAMING:
                case R.string.CONST_SELECTEDVIEW_1010ROAMING:
//				roamingFragment = new UsageFragment();
//				Bundle bundle1 = new Bundle();
//				bundle1.putBoolean("islocal", false);
//				roamingFragment.setArguments(bundle1);
                    return roamingFragment;
                case R.string.CONST_SELECTEDFRAG_VAS:
                    vasFragment = new VasFragment();
                    return vasFragment;
                default:
                    return null;
            }
        }
    }

    // Restore the Bundle data when re-built Activity
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        //		assocSubnRec = (SubscriptionRec) savedInstanceState.getSerializable(SubscriptionRec.class.getSimpleName());
        //		isZombie = savedInstanceState.getBoolean("ISZOMBIE");
        //		account = (Account) savedInstanceState.getSerializable("ACCOUNT");
        lob = savedInstanceState.getInt("LOB", lob);
        //		bupdCra = (BupdCra) savedInstanceState.getSerializable("BUPDCRA");
        //		binqCra = (BinqCra) savedInstanceState.getSerializable("BINQCRA");
        ClnEnv.onRestoreInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    // Saved data before Destroy Activity
    protected void onSaveInstanceState(Bundle outState) {
        //		outState.putSerializable(SubscriptionRec.class.getSimpleName(), assocSubnRec);
        //		outState.putSerializable("ACCOUNT", account);
        //		outState.putInt("LOB", lob);
        //		outState.putSerializable("BUPDCRA", bupdCra);
        //		outState.putSerializable("BINQCRA", binqCra);
        outState.putBoolean("ISZOMBIE", isZombie);
        super.onSaveInstanceState(ClnEnv.onSaveInstanceState(outState));
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
    }

    private int getLob(SubnRec subnRec) {
        if (SubnRec.LOB_LTS.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_LTS;
        } else if (SubnRec.LOB_MOB.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_MOB;
        } else if (SubnRec.LOB_IOI.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_IOI;
        } else if (SubnRec.LOB_PCD.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_PCD;
        } else if (SubnRec.LOB_TV.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_TV;
        } else if (SubnRec.LOB_101.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_1010;
        } else if (SubnRec.LOB_O2F.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_O2F;
        } else if (SubnRec.WLOB_XMOB.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_MOB;
        } else if (SubnRec.LOB_IMS.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_IMS;
        }
        return 0;
    }

    private String getLobString(SubnRec subnRec) {
        if (SubnRec.LOB_LTS.equalsIgnoreCase(subnRec.lob)) {
            return "LTS";
        } else if (SubnRec.LOB_MOB.equalsIgnoreCase(subnRec.lob)) {
            return "MOB";
        } else if (SubnRec.WLOB_XMOB.equalsIgnoreCase(subnRec.lob)) {
            return "XMOB";
        } else if (SubnRec.LOB_IOI.equalsIgnoreCase(subnRec.lob)) {
            return "IOI";
        } else if (SubnRec.LOB_PCD.equalsIgnoreCase(subnRec.lob)) {
            return "PCD";
        } else if (SubnRec.LOB_TV.equalsIgnoreCase(subnRec.lob)) {
            return "TV";
        } else if (SubnRec.LOB_101.equalsIgnoreCase(subnRec.lob)) {
            return "101";
        } else if (SubnRec.LOB_O2F.equalsIgnoreCase(subnRec.lob)) {
            return "O2F";
        } else if (SubnRec.LOB_IMS.equalsIgnoreCase(subnRec.lob)) {
            return "IMS";
        }
        return "";
    }

    public SubnRec getAssocSubnRec() {
        return assocSubnRec;
    }

    public void setAssocSubnRec(SubnRec assocSubnRec) {
        this.assocSubnRec = assocSubnRec;
    }

    public Account getAccount() {
        // TODO Auto-generated method stub
        return account;
    }

    public void setAccount(Account account) {
        // TODO Auto-generated method stub
        this.account = account;
    }

    public SubnRec getSubnRec() {
        // TODO Auto-generated method stub
        return assocSubnRec;
    }

    public void setSubscriptionRec(SubnRec subnRec) {
        // TODO Auto-generated method stub

    }

    public void setActiveSubview(int index) {
        activeSubView = index;
    }

    public int getActiveSubview() {
        return activeSubView;
    }

    public void displaySubview() {
        // TODO Auto-generated method stub

    }

    public int getLob() {
        // TODO Auto-generated method stub
        return lob;
    }

    @Override
    public int getLtsType() {
        if (debug) Log.i(TAG, "getType" + ltsType);
        return ltsType;
    }

    public String getLobString() {
        return lobString;
    }

    public boolean IsZombie() {
        // TODO Auto-generated method stub
        return isZombie;
    }

    public boolean CompareLastBillDate(String lastBilldate, String newlastBilldate) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isMyMobAcct() {
        return isMyMobAcct;
    }

    public AcctAgent getAcctAgent() {
        // TODO Auto-generated method stub
        return acctAgent;
    }

    @Override
    public MyMobAcctAgent getMyMobAcctAgent() {
        return myMobAcctAgent;
    }

    @Override
    public int getCurrentServicePage() {
        return tabPos;
    }

    @Override
    public ApptCra getApptCra() {
        return apptCra;
    }

    @Override
    public void setApptCra(ApptCra mApptCra) {
        apptCra = mApptCra;
    }

    @Override
    public LnttAgent getLnttAgent() {
        return lnttAgent;
    }

    @Override
    public void setLnttAgent(LnttAgent mLnttAgent) {
        lnttAgent = mLnttAgent;
    }

    @Override
    public LnttCra getLnttCra() {
        return lnttCra;
    }

    @Override
    public void setLnttCra(LnttCra mLnttCra) {
        this.lnttCra = mLnttCra;
    }

    @Override
    public void openLiveChat() {
        super.openLiveChat();
    }

    @Override
    public String getNavTitle() {
        // TODO Auto-generated method stub
        return navBarTitle;
    }

    @Override
    public boolean isLnttServiceClearable() {
        if (debug) Log.i(TAG, "isLnttService Clearable = " + isLnttServiceClearable);
        return isLnttServiceClearable;
    }

    @Override
    public void setIsLnttServiceClearable(Boolean mIsLnttServiceClearable) {
        this.isLnttServiceClearable = mIsLnttServiceClearable;
        if (debug) Log.i(TAG, "Set isLnttService Clearable = " + mIsLnttServiceClearable);
        if (debug) Log.i(TAG, "LnttAngent" + new Gson().toJson(Utils.getPrefLnttAgent(this)));

    }

    @Override
    public String getTrackerId() {
        return trackerId;
    }

    @Override
    public void setTrackerId(String mtrackerId) {
        trackerId = mtrackerId;
    }


}