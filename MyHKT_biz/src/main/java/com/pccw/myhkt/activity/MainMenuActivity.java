package com.pccw.myhkt.activity;

import static com.pccw.myhkt.utils.Constant.USER_TYPE_PRD;
import static com.pccw.myhkt.utils.Constant.USER_TYPE_UAT;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.AppConfigManager;
import com.pccw.myhkt.AppConfigManager.AppConfigListener;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.LiveChatHelper;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.adapter.MainMenuGridViewAdapter;
import com.pccw.myhkt.fragment.SlidingMenuFragment;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.HomeButtonItem;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.mymob.activity.MyMobileActivity;
import com.pccw.myhkt.utils.DownloadService;
import com.pccw.myhkt.utils.FirebaseSetting;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/************************************************************************
 * File : MainMenu.java
 * Desc : Screen 4.1 - Main Menu 
 * Name : Main Menu Created
 * by 	: Vincent Fung 
 * Date : 14/10/2015
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 14/10/2015 Vincent Fung		- First draft
 * 30/10/2015 Andy Wong			- Change the home menu to grid view	
 * 26/11/2015 Derek Tsui		- Navbar and BaseActivity handling
 * 29/03/2016 Andy Wong			- To support status bar color, use activity + create sliding menu instead of slidingmenuactivity
 * 30/05/2017 Abdulmoiz Esmail  - Comment line 454 and 455 to remove the non premier buttons in the main menu
 * 01/06/2017 Abdulmoiz Esmail  - Repositioned the Non-premier menu buttons, update the SwitchApptIcon, set the index to 2. 
 * 29/09/2017 Abdulmoiz Esmail  - Added onclick on the banner image and navigate to browser with the saved URL from appconfig
 *************************************************************************/
public class MainMenuActivity extends FragmentActivity implements OnAPIsListener, AppConfigListener, DownloadService.DownloadServiceListener {
    // Common Components
    private boolean debug = false;
    private AAQuery aq;
    private SaveAccountHelper saveAccountHelper;            // SQLite database for a new bill checking
    public static AppConfigManager appConfigManager = null;
    private Calendar cal;
    public static MainMenuActivity me;
    private String clientVersion = "";
    private String TAG = this.getClass().getName();
    private final int colMaxNum = 4;
    private List<HomeButtonItem> homeItems;
    private int extralinespace;
    private int basePadding;
    private int logoPadding;
    private int gridlayoutPadding = 0;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private Activity act;
    protected Boolean isZh = false;
    private boolean isCheckedLnttResult = false;
    // FrameLayout as Container for activity
    protected FrameLayout frameContainer;
    private MAINMENU clickItem;
    // SlideMenu
    protected SlidingMenuFragment menuFrag;
    public SlidingMenu slidingmenu;
    private MainMenuGridViewAdapter mainMenuGridViewAdapter;
    private String moduleId;
    private Boolean isPremier = true;
    private FirebaseSetting firebaseSetting;

    private DownloadService downloadService = null;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aq = new AAQuery(this);

        firebaseSetting = new FirebaseSetting(this);
        debug = getResources().getBoolean(R.bool.DEBUG);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        initData();
        downloadService = DownloadService.getInstance(me, this);
        moduleId = getResources().getString(R.string.MODULE_HOME);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        isPremier = ClnEnv.getSessionPremierFlag();
        // init SlideMenu Content View, must be defined in onCreate
        //		setBehindContentView(R.layout.menu_frame);
        setContentView(!isPremier ? R.layout.activity_mainmenu : R.layout.activity_mainmenu_prem);
        try {
            clientVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        setStatusBarColor();
        initSlidingMenu();
        checkEnvironment();
    }

    private void checkEnvironment() {
        if (ClnEnv.getPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain).equals(APIsManager.UAT_domain)) {
            firebaseSetting.setUserEvent(USER_TYPE_UAT);
            firebaseSetting.setUATProperty();
        } else {
            firebaseSetting.setUserEvent(USER_TYPE_PRD);
            firebaseSetting.setPRDProperty();
        }
    }

    public void setStatusBarColor() {
        //Status Bar Color
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.getDecorView().setFitsSystemWindows(false);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.hkt_headingpremier : R.color.hkt_textcolor));
        }
    }

    public void initData() {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        gridlayoutPadding = (int) getResources().getDimension(R.dimen.mainmenu_gridlayout_padding);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        logoPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding_1);
        colWidth = (deviceWidth - extralinespace * 2 - (colMaxNum - 1) * gridlayoutPadding) / colMaxNum;
        act = this;
        me = this;
    }

    @Override
    public void setContentView(int layoutResID) {
        if (frameContainer != null) {
            frameContainer.removeAllViews();
        }
        frameContainer = new FrameLayout(this);
        LayoutParams layoutParam = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        frameContainer.addView(getLayoutInflater().inflate(layoutResID, null), layoutParam);
        super.setContentView(frameContainer, layoutParam);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    // Check LineTest Result when launch and relaunch MainMenu
    private final void checkLnttResultAndRedirect() {
        // Line Test Result completed, redirect to targetService
        if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false)) {
            LnttAgent lnttAgent = Utils.getPrefLnttAgent(me);
            String endTimestamp = lnttAgent.getEndTimestamp();
            if (!"".equals(lnttAgent.getResultMsg()) && lnttAgent.getResultMsg() != null && (lnttAgent.getLnttSrvNum() == null || lnttAgent.getLnttSrvNum().equals(""))) {
                //Http 404
                DialogHelper.createSimpleDialog(this, lnttAgent.getResultMsg());
                Utils.clearLnttService(me);
            } else {
                // Clear lineTest Data if loginID is not matched
                String loginID = lnttAgent.getLnttCra().getILoginId();
                if ((!"".equalsIgnoreCase(loginID) && !loginID.equalsIgnoreCase(ClnEnv.getSessionLoginID())) || "".equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
                    DialogHelper.createSimpleDialog(this, getResString(R.string.MYHKT_LT_ERR_ACC_NOT_FOUND));
                    Utils.clearLnttService(me);
                } else if (ClnEnv.isLoggedIn()) {
                    // Check subscriptionRec is valid in current ServiceList
                    boolean isValidSubnRec = false;
                    for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry())) {
                        if (rSubnRec.srvNum.equalsIgnoreCase(lnttAgent.getLnttSrvNum()) && rSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum)) {
                            isValidSubnRec = true;
                        }
                    }

                    if (!isValidSubnRec) {
                        DialogHelper.createSimpleDialog(this, getResString(R.string.MYHKT_LT_ERR_SUBS_NOT_FOUND));
                        Utils.clearLnttService(me);
                    } else if (Utils.isExpiredLnttResult(me, endTimestamp)) {
                        // Check the Line Test Result Expired Time
                        DialogHelper.createSimpleDialog(this, getResString(R.string.MYHKT_LT_ERR_EXPIRED));
                        Utils.clearLnttService(me);
                    } else if (!"".equalsIgnoreCase(lnttAgent.getLnttSrvNum())) {
                        Intent nextIntent = new Intent(getApplicationContext(), ServiceListActivity.class);
                        startActivity(nextIntent);
                    } else {
                        // UnExpected Case, clean data to handle it
                        Utils.clearLnttService(me);
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);
                    }
                }
            }
        }
    }

    //Check if bill message exist during Main Menu launch
    private final void checkBillMsgAndRedirect() {
        //demo to test the notification steps flow
        if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false)) {
            if (debug) Log.i(TAG, "check bill1");
            try {
                if (debug) Log.i(TAG, "check bill2");
                if (ClnEnv.getPushDataBill() != null) {
                    if (debug) Log.i(TAG, "check bill3");
                    String loginID = ClnEnv.getPushDataBill().getLoginId();
                    if ((!"".equalsIgnoreCase(loginID) && !ClnEnv.getSessionLoginID().equalsIgnoreCase(loginID)) || "".equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
                        billMsgLoginRedirect();
                        if (debug) Log.i(TAG, "NOT SAME ID");
                    } else if (ClnEnv.isLoggedIn()) {
                        // Check subscriptionRec is valid in current ServiceList
                        boolean isValidSubnRec = false;
                        String serviceNum = "";
                        for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry())) {
                            if (rSubnRec.acctNum.equalsIgnoreCase(ClnEnv.getPushDataBill().getAcctNum())) {
                                isValidSubnRec = true;
                                int ltsType = 0;

                                //service number for bill dialog TODO
                                if (rSubnRec.lob.equals(SubnRec.LOB_LTS)) {
                                    ltsType = Utils.getLtsSrvType(rSubnRec.tos, rSubnRec.eyeGrp, rSubnRec.priMob);
                                    if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
                                        int last4index = (rSubnRec.srvNum.length() - 4) < 0 ? 0 : rSubnRec.srvNum.length() - 4;
                                        serviceNum = String.format("CARD%s", rSubnRec.srvNum.substring(last4index)) + "\n";
                                    } else {
                                        serviceNum = rSubnRec.srvNum + "\n";
                                    }
                                } else {
                                    serviceNum = rSubnRec.srvNum + "\n";
                                }


                                if (rSubnRec.lob.equals(SubnRec.LOB_LTS) || rSubnRec.lob.equals(SubnRec.LOB_MOB) || rSubnRec.lob.equals(SubnRec.LOB_101) || rSubnRec.lob.equals(SubnRec.LOB_IOI) || rSubnRec.lob.equals(SubnRec.LOB_O2F)) {
                                    serviceNum = getString(R.string.LTTF_LTS_ACCTNUM) + " " + serviceNum;
                                } else if (rSubnRec.lob.equals(SubnRec.LOB_TV)) {
                                    serviceNum = getString(R.string.LTTF_TV_ACCTNUM) + " " + serviceNum;
                                } else if (rSubnRec.lob.equals(SubnRec.LOB_PCD)) {
                                    serviceNum = getString(R.string.LTTF_PCD_ACCTNUM) + " " + serviceNum;
                                }
                            }
                        }


                        if (!isValidSubnRec) {
                            if (debug) Log.i(TAG, "not valid");
                            Utils.clearBillMsgService(me);
                            billMsgRedirect(Utils.getString(me, R.string.MYHKT_PN_ERR_SUBS_NOT_FOUND));
                        } else if (!ClnEnv.getLoginId().equalsIgnoreCase(ClnEnv.getPushDataBill().getLoginId())) {
                            if (debug) Log.i(TAG, "not same loginid id");
                            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
                            billMsgLoginRedirect();
                        } else {
                            if (debug) Log.i(TAG, "ok");
                            billMsgRedirect(ClnEnv.getPushDataBill().getMessage());
                        }
                    }
                }
            } catch (Exception e) {
                //Prevent throw null pointer exception for BillPushData
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
                return;
            }
        }
    }

    private final void checkGenMsg() {
        if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), false)) {
            try {
                if (ClnEnv.getPushDataGen() != null) {
                    DialogHelper.createSimpleDialog(this, ClnEnv.getPushDataGen().getMessage());
                }
            } catch (Exception e) {
                //Prevent throw null pointer exception for BillPushData
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), false);
                return;
            }
            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), false);
        }
    }

    @Override
    protected final void onStart() {
        super.onStart();
        //Screen Tracker

        //Set MyMobileFlag false
        ClnEnv.setMyMobFlag(false);

        // Clear lineTest Data if loginID is not matched before autoLogin
        // if not matched, stop to auto-Login
        isCheckedLnttResult = true;
        checkLnttResultAndRedirect();
        checkBillMsgAndRedirect();
        checkGenMsg();
        //		initUI();

        // try auto-login
        if (!ClnEnv.isLoggedIn() && ClnEnv.getSessionLoginID().length() > 0 && ClnEnv.getSessionPassword().length() > 0) {
            SveeRec sveeRec = new SveeRec();
            sveeRec.loginId = ClnEnv.getSessionLoginID();
            sveeRec.pwd = ClnEnv.getSessionPassword();
            LgiCra lgiCra = new LgiCra();
            lgiCra.setISveeRec(sveeRec);
            //				APIsManager.doLogin(this, lgiCra, ClnEnv.isSessionSavePw());
//			APIsManager.doLoginTest(this, lgiCra, ClnEnv.isSessionSavePw());
            APIsManager.doLoginTest(this, lgiCra, ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false));
        }
    }

    @Override
    protected final void onResume() {
        super.onResume();
        initUI();
        // Update Locale
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        //setLiveChangeIcon(LiveChatHelper.getInstance(me, me).isPause);

        if (!ClnEnv.isLoggedIn()) {
            me.SwitchApptIcon(false);
        } else {
            me.SwitchApptIcon(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_FLAG), false));
        }

        // When user stay at MainMenu receiving LineTest Result,
        // it does not run onStart, so check LineTest result at onResume
        if (!isCheckedLnttResult) {
            checkLnttResultAndRedirect();
        }
        isCheckedLnttResult = false;

        updateLoginStatus();
    }

    private void setPermierLayout() {
        homeItems = new ArrayList<HomeButtonItem>();
        int textsize = (int) getResources().getDimension(R.dimen.smalltext1size);
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_account_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_ACCT), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.MYACCT));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_profile_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_PROFILE), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.MYPROFILE));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_appt_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_APPT), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.MYAPPT));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_mymobile_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_MYMOB), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.MYMOB));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_home_sol_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_SOLUTION), isZh ? textsize - 3 : textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.SOLUTION));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_news_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_NEWS), isZh ? textsize - 3 : textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.NEWS));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_club_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_CLUB), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.THECLUB));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_tap_go_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_TAPNGO), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.TAPNGO));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_dq_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_DQ), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.DIRECTINQ));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_idd_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_IDD), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.IDD));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_shop_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_SHOP), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.SHOPS));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_livechat_premier, Utils.getString(this, R.string.MYHKT_HOME_BTN_CONTACT), textsize, Color.parseColor("#00FFFFFF"), Color.parseColor("#606060"), MAINMENU.CONTACTUS));
        aq.id(R.id.mainmenu_label_welcome).getTextView().setPadding(0, logoPadding, 0, logoPadding);
        aq.id(R.id.mainmenu_label_welcome).textColorId(R.color.hkt_txtcolor_grey);
        aq.id(R.id.mainmenu_label_welcome).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header3size));

        mainMenuGridViewAdapter = new MainMenuGridViewAdapter(this, homeItems, colWidth);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setNumColumns(colMaxNum);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setHorizontalSpacing(gridlayoutPadding);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setVerticalSpacing(gridlayoutPadding);
        aq.id(R.id.mainmenu_gridlayout).adapter(mainMenuGridViewAdapter).itemClicked(this, "onHomeBtnClick");

        //Set the size for the mainmenu_btn_info
        int h = getResources().getDrawable(R.drawable.slogun2_premier).getIntrinsicHeight();
        Drawable drawable = getResources().getDrawable(R.drawable.home_btn_info_premier);
        int drawableWidth = drawable.getIntrinsicWidth();
        int drawableHeight = drawable.getIntrinsicHeight();
        drawable = null;
        int viewWidth = drawableWidth * h / drawableHeight;
        int viewHeight = h;
        aq.id(R.id.mainmenu_btn_info).height(viewHeight, false).width(viewWidth, false);

        drawable = null;
        drawableWidth = 0;
        drawableHeight = 0;
        viewWidth = 0;
        viewHeight = 0;
        aq.id(R.id.mainmenu_btn_info).clicked(this, "onClick");

        //Bot bar
        aq.id(R.id.mainmenu_pccwlogo1).image(0);
        aq.id(R.id.mainmenu_pccwlogo).image(0);
        aq.id(R.id.mainmenu_pccwlogo1).image(R.drawable.hktlogo);
        aq.id(R.id.mainmenu_pccwlogo).image(R.drawable.pccwlogo);

    }

    private void setNonPermierLayout() {
        //init menu buttons
        homeItems = new ArrayList<HomeButtonItem>();
        int textsize = (int) getResources().getDimension(R.dimen.smalltext1size);
        //homeItems.add(new HomeButtonItem(-1,"",textsize,Color.parseColor("#ECECEC"),Color.parseColor("#FFFFFF"), MAINMENU.NULL));
        //homeItems.add(new HomeButtonItem(-1,"",textsize,Color.parseColor("#DEE0EF"),Color.parseColor("#FFFFFF"), MAINMENU.NULL));
        homeItems.add(new HomeButtonItem(R.drawable.myaccount, Utils.getString(this, R.string.MYHKT_HOME_BTN_ACCT), textsize, Color.parseColor("#00417B"), Color.parseColor("#FFFFFF"), MAINMENU.MYACCT));
        homeItems.add(new HomeButtonItem(R.drawable.myprofile, Utils.getString(this, R.string.MYHKT_HOME_BTN_PROFILE), textsize, Color.parseColor("#303E5F"), Color.parseColor("#FFFFFF"), MAINMENU.MYPROFILE));
        //homeItems.add(new HomeButtonItem(R.drawable.myappointment,Utils.getString(this, R.string.MYHKT_HOME_BTN_APPT),textsize,Color.parseColor("#47ADF3"),Color.parseColor("#FFFFFF"), MAINMENU.MYAPPT));
        //homeItems.add(new HomeButtonItem(R.drawable.mymobile,Utils.getString(this, R.string.MYHKT_HOME_BTN_MYMOB),textsize,Color.parseColor("#EF9A0C"),Color.parseColor("#FFFFFF"), MAINMENU.MYMOB));
        homeItems.add(new HomeButtonItem(R.drawable.dq, Utils.getString(this, R.string.MYHKT_HOME_BTN_DQ), textsize, Color.parseColor("#5E7BCB"), Color.parseColor("#FFFFFF"), MAINMENU.DIRECTINQ));
        //homeItems.add(new HomeButtonItem(R.drawable.idd,Utils.getString(this, R.string.MYHKT_HOME_BTN_IDD),textsize,Color.parseColor("#2273BC"),Color.parseColor("#FFFFFF"), MAINMENU.IDD));
        homeItems.add(new HomeButtonItem(R.drawable.icon_theclub, Utils.getString(this, R.string.MYHKT_HOME_BTN_CLUB), textsize, Color.parseColor("#000000"), Color.parseColor("#FFFFFF"), MAINMENU.THECLUB));
        homeItems.add(new HomeButtonItem(R.drawable.tab_go, Utils.getString(this, R.string.MYHKT_HOME_BTN_TAPNGO), textsize, Color.parseColor("#AD342F"), Color.parseColor("#FFFFFF"), MAINMENU.TAPNGO));
        homeItems.add(new HomeButtonItem(R.drawable.shop, Utils.getString(this, R.string.MYHKT_HOME_BTN_SHOP), textsize, Color.parseColor("#0693B7"), Color.parseColor("#FFFFFF"), MAINMENU.SHOPS));
        homeItems.add(new HomeButtonItem(R.drawable.contactus, Utils.getString(this, R.string.MYHKT_HOME_BTN_CONTACT), textsize, Color.parseColor("#52708A"), Color.parseColor("#FFFFFF"), MAINMENU.CONTACTUS));
        aq.id(R.id.mainmenu_label_welcome).getTextView().setPadding(0, logoPadding, 0, logoPadding);
        aq.id(R.id.mainmenu_label_welcome).textColorId(R.color.hkt_txtcolor_grey);
        aq.id(R.id.mainmenu_label_welcome).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header3size));

        mainMenuGridViewAdapter = new MainMenuGridViewAdapter(this, homeItems, colWidth);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setNumColumns(colMaxNum);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setHorizontalSpacing(gridlayoutPadding);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setVerticalSpacing(gridlayoutPadding);
        aq.id(R.id.mainmenu_gridlayout).adapter(mainMenuGridViewAdapter).itemClicked(this, "onHomeBtnClick");

        //Set Banner to match the device width with same ratio
        Drawable drawable = this.getResources().getDrawable(R.drawable.menu_banner);

        int drawableWidth = drawable.getIntrinsicWidth();
        int drawableHeight = drawable.getIntrinsicHeight();
        int bannerWidth = deviceWidth - (int) getResources().getDimension(R.dimen.basePadding) * 2;
        int bannerHeight = (int) ((float) drawableHeight * (float) bannerWidth / (float) drawableWidth);
        //		aq.id(R.id.mainmenu_topbanner).image(R.drawable.menu_banner);
        aq.id(R.id.mainmenu_topbanner).height(bannerHeight, false);
        aq.id(R.id.mainmenu_topbanner).width(bannerWidth, false);

        ///Appconfig image //TODO derek
        if (!ClnEnv.isAppConfigDownloaded()) {
//            AppConfigManager am = AppConfigManager.getInstance(me);
//            am.setAppConfigListener(this);
//            am.downloadAppConfigJson();
            downloadService.downloadAppConfig();
            downloadService.downloadImageBanner();
        }

        File imgFile = new File(getResString(R.string.PATH) + "images/" + ClnEnv.getAppLocale(getBaseContext()) + "/main_top_banner.png");
        if (imgFile.exists()) {
            aq.id(R.id.mainmenu_topbanner).getImageView().setImageURI(Uri.fromFile(imgFile));
        } else {
            aq.id(R.id.mainmenu_topbanner).getImageView().setImageDrawable(drawable);
        }
        aq.id(R.id.mainmenu_topbanner).clicked(this, "onClick");

        drawable = null;
        drawableWidth = 0;
        drawableHeight = 0;
        bannerWidth = 0;
        bannerHeight = 0;

        //Bot bar
        aq.id(R.id.mainmenu_pccwlogo1).image(0);
        aq.id(R.id.mainmenu_pccwlogo).image(0);
        aq.id(R.id.mainmenu_pccwlogo1).image(R.drawable.hktlogo);
        aq.id(R.id.mainmenu_pccwlogo).image(R.drawable.pccwlogo);

    }

    private final void initUI() {
        aq = new AAQuery(this);
        if (isPremier != ClnEnv.getSessionPremierFlag()) {
            isPremier = ClnEnv.getSessionPremierFlag();
            setStatusBarColor();
            setContentView(!isPremier ? R.layout.activity_mainmenu : R.layout.activity_mainmenu_prem);
//			slidingmenu = new SlidingMenu(this);
//			slidingmenu.setShadowWidth(0);
//			slidingmenu.setShadowDrawable(null);
//			slidingmenu.setBehindOffset(Utils.dpToPx(85));
//			slidingmenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
//			slidingmenu.setFadeEnabled(false);
//			slidingmenu.setBehindScrollScale(0.0f);
            slidingmenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
            slidingmenu.setMenu(R.layout.menu_frame);
            // set Backgound Color to prevent white color edge problem when opening Menu instant
            slidingmenu.setBackgroundColor(getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.menu_bg_premier : R.color.menu_bg_grey));
            //Initialize sildingmenu
            FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
            menuFrag = new SlidingMenuFragment();
            transaction.replace(R.id.menu_frame, menuFrag);
            transaction.commit();

//			menuFrag.setBackground();
        } else {
            if (menuFrag != null) {
                menuFrag.refreshView();
            }
        }

//		if (isPremier) {

//		}

        //navbar style
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarTitle(R.id.navbar_title, getResString(R.string.app_name));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_slidemenu);
        setLiveChangeIcon(false);

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");
        //aq.id(R.id.navbar_button_right).clicked(this, "onClick");
        aq.id(R.id.navbar_button_right).visibility(View.GONE);
        isZh = "zh".equalsIgnoreCase(this.getResources().getString(R.string.myhkt_lang));

        //init menu buttons
        if (this.isPremier) {
            setPermierLayout();
        } else {
            setNonPermierLayout();
        }


        //Initialize sildingmenu
        //		FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        //				menuFrag = new SlidingMenuFragment();
        if (menuFrag != null) {
//			menuFrag.refreshView();
        }
        //		transaction.replace(R.id.menu_frame, menuFrag);
        //		transaction.commit();
        updateLoginStatus();


        //derek test
        //		AppConfigManager am = AppConfigManager.getInstance(me);
        //		String test = am.getCommonConfigByKey("livechat_consumer_flag");
        //		Toast.makeText(me, "livechat_consumer_flag = " + test, 4000).show();
        //derek test
    }

    public String getResString(int res) {
        return Utils.getString(this, res);
    }

    private final void initSlidingMenu() {

        //		slidingmenu = getSlidingMenu();
        slidingmenu = new SlidingMenu(this);
        slidingmenu.setShadowWidth(0);
        slidingmenu.setShadowDrawable(null);
        slidingmenu.setBehindOffset(Utils.dpToPx(85));
        slidingmenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        slidingmenu.setFadeEnabled(false);
        slidingmenu.setBehindScrollScale(0.0f);
        slidingmenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        slidingmenu.setMenu(R.layout.menu_frame);
        // set Backgound Color to prevent white color edge problem when opening Menu instant
        slidingmenu.setBackgroundColor(getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.menu_bg_premier : R.color.menu_bg_grey));

        //Initialize sildingmenu
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        menuFrag = null;
        menuFrag = new SlidingMenuFragment();
        transaction.replace(R.id.menu_frame, menuFrag);
        transaction.commit();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                if (debug) Log.i("MainMenu", "L Click");
                if (slidingmenu == null) {
                    if (debug) Log.i("MainMenu", "Menu is null");
                } else {
                    if (slidingmenu.getMenu() == null) {
                        if (debug) Log.i("MainMenu", "Menu ");
                    }
                    if (debug) Log.i("MainMenu", "" + slidingmenu.isMenuShowing());
                }
                slidingmenu.toggle();
                break;
            case R.id.navbar_button_right:
                //livechat
                //check login
                if (LiveChatHelper.isPause) {
                    LiveChatHelper.getInstance(act, act).showWebView(act, moduleId);
                } else if (ClnEnv.isLoggedIn()) {
                    //open live chat
                    if (!"Y".equalsIgnoreCase(ClnEnv.getChatIsMaintenance())) {
                        LiveChatHelper.getInstance(act, act).showWebView(act, moduleId);
                    } else {
                        DialogHelper.createSimpleDialog(me, getString(R.string.LIVECHAT_UNAVAILABLE));
                    }
                } else {
                    loginFirstDialog();
                }

                //derek for apppointment icon test, free to remove
                //			homeItems.get(4).setImageResource(R.drawable.myappointment_2);
                //			mainMenuGridViewAdapter.notifyDataSetChanged();
                break;
            case R.id.mainmenu_btn_info:
//			Log.i("TEST", "TEst");
                Intent intent;
//			intent = new Intent(getApplicationContext(), AboutUsActivity.class);
//			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			startActivity(intent);
//			overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);

                intent = new Intent(getApplicationContext(), BrowserActivity.class);
                intent.putExtra(BrowserActivity.INTENTLINK, isZh ? "http://www.hktpremier.com/hktpremier/tch/about/" : "http://www.hktpremier.com/hktpremier/eng/about/");
                intent.putExtra(BrowserActivity.INTENTTITLE, Utils.getString(this, R.string.MYHKT_ABOUT_US_TITLE));
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case R.id.mainmenu_topbanner:
                String url = isZh ? ClnEnv.getPref(getApplicationContext(), getString(R.string.CONT_BANNER_URL_ZH), "") : ClnEnv.getPref(getApplicationContext(), getString(R.string.CONT_BANNER_URL_ENG), "");
                if (!TextUtils.isEmpty(url)) {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intentBrowser);
                }
                break;
        }
    }

    public void onHomeBtnClick(AdapterView<?> parent, View v, int position, long id) {
        Intent intent;
        Bundle bundle;
        HomeButtonItem homeButtonItem = (HomeButtonItem) parent.getAdapter().getItem(position);

        switch (homeButtonItem.type) {
            case MYACCT:
                if (!ClnEnv.isLoggedIn()) {
                    loginFirstDialog(homeButtonItem.type);
                } else {
                    intent = new Intent(getApplicationContext(), ServiceListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                }
//                firebaseSetting.onMenuIconClickEvent("My_Account");
                break;
            case MYPROFILE:
                if (!ClnEnv.isLoggedIn()) {
                    loginFirstDialog(homeButtonItem.type);
                } else {
                    intent = new Intent(getApplicationContext(), MyProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                }
//                firebaseSetting.onMenuIconClickEvent("My_Profile");
                break;
            case MYAPPT:
                if (!ClnEnv.isLoggedIn()) {
                    loginFirstDialog(homeButtonItem.type);
                } else {
                    intent = new Intent(getApplicationContext(), MyApptActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                }
//                firebaseSetting.onMenuIconClickEvent("My_Appointment");
                break;
            case MYMOB:
                intent = new Intent(getApplicationContext(), MyMobileActivity.class);
                //			bundle = new Bundle();
                //			bundle.putBoolean("ISMYMOBEMPTY", isEmpty);
                //			intent.putExtras(bundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
//                firebaseSetting.onMenuIconClickEvent("My_Mobile");
                break;
            case IDD:
                intent = new Intent(getApplicationContext(), IDDActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                bundle = new Bundle();
                bundle.putBoolean(IDDActivity.BUNDLEIS0060, ClnEnv.isLoggedIn());
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
//                firebaseSetting.onMenuIconClickEvent("IDD_0060");
                break;
            case THECLUB:
                // Event Tracker
                //open the club app
                String url = "pccwtheclub://";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setData(Uri.parse(url));
                appurlDialog(i, false);
//                firebaseSetting.onMenuIconClickEvent("The_Club");
                break;
            case DIRECTINQ:
                intent = new Intent(getApplicationContext(), DirectoryInquiryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
//                firebaseSetting.onMenuIconClickEvent("Directory_Inquiries");
                break;
            case SHOPS:
                intent = new Intent(getApplicationContext(), ShopActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
//                firebaseSetting.onMenuIconClickEvent("Shops");
                break;
            case TAPNGO:
                // Event Tracker

                intent = getPackageManager().getLaunchIntentForPackage("com.hktpayment.tapngo");
                appurlDialog(intent, true);

                //testing activity remove later TODO
                //			intent = new Intent(getApplicationContext(), ImageViewerActivity.class);
                //			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //			bundle = new Bundle();
                //			bundle.putString("NAVTEXT" , "LOBTITLE");
                //			bundle.putInt("LOBICON", R.drawable.logo_fixedline_eye);
                //			bundle.putString("IMAGEURL", "http://static.tumblr.com/c4afa18619e7f4ba3a72cf869dd21ed8/eybk5yk/YDKmu2acr/tumblr_static_pikachu.full.997159.jpg");
                //			intent.putExtras(bundle);
                //			startActivity(intent);
                //			overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);

                //testing activity remove later TODO
                //			intent = new Intent(getApplicationContext(), RegAccInfoActivity.class);
                //			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //			bundle = new Bundle();
                //			bundle.putString("LOGINID" , "keith.kk.mak@pccw.com");
                //			bundle.putString("PWD", "abcd1234");
                //
                //			bundle.putString("SAVEPW", "abcd1234");
                //			bundle.putString("ERRCODE", "ERR");
                //			bundle.putSerializable("LOGINCRA", ClnEnv.getLgiCra());
                //			intent.putExtras(bundle);
                //			startActivity(intent);
                //			overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
//                firebaseSetting.onMenuIconClickEvent("TapNGo");
                break;
            case CONTACTUS:
                intent = new Intent(getApplicationContext(), ContactUsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
//                firebaseSetting.onMenuIconClickEvent("Contact_Us");
                break;
            case SOLUTION:
                intent = new Intent(getApplicationContext(), BrowserActivity.class);
                intent.putExtra(BrowserActivity.INTENTLINK, "http://www.hktpremier.com/hktpremier/eng/homesolutions/");
                intent.putExtra(BrowserActivity.INTENTTITLE, Utils.getString(this, R.string.MYHKT_HOME_BTN_SOLUTION));
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case NEWS:
//			intent = new Intent(getApplicationContext(), BrowserActivity.class);
//			intent.putExtra(BrowserActivity.INTENTLINK, isZh? "http://www.hktpremier.com/hktpremier/tch/whatsnew/" : "http://www.hktpremier.com/hktpremier/eng/whatsnew/");
//			intent.putExtra(BrowserActivity.INTENTTITLE, Utils.getString(this, R.string.MYHKT_HOME_BTN_NEWS));
//			startActivity(intent);
//			overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);			

                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(isZh ? "http://www.hktpremier.com/hktpremier/tch/whatsnew/" : "http://www.hktpremier.com/hktpremier/eng/whatsnew/"));
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            default:
                // Toast.makeText(this, "Clicked", Toast.LENGTH_LONG).show();
                break;
        }
    }

    // Login First Dialog
    protected final void loginFirstDialog() {
        //Without redirect
        loginFirstDialog(null);
    }

    protected final void loginFirstDialog(final MAINMENU clickItem) {
        OnClickListener onClickListener = new OnClickListener() {
            Bundle rbundle;

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                dialog.dismiss();
                intent = new Intent(act.getApplicationContext(), LoginActivity.class);
                rbundle = new Bundle();
                if (clickItem != null) {
                    rbundle.putSerializable("CLICKBUTTON", clickItem);
                    intent.putExtras(rbundle);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        };
        DialogHelper.createSimpleDialog(this, Utils.getString(this, R.string.myhkt_plslogin), Utils.getString(this, R.string.btn_ok), onClickListener);
    }

    public final void doLogout() {
        DialogHelper.createLogoutDialog(me, Utils.getString(me, R.string.myhkt_confirmlogout), Utils.getString(me, R.string.myhkt_btn_yes), Utils.getString(this, R.string.myhkt_btn_no));
    }

    public final void appurlDialog(final Intent i, boolean isTapNGo) {
        if (isTapNGo) {
            String message = i != null ? Utils.getString(me, R.string.TAPNGO_TRANSITION_MSG) : Utils.getString(me, R.string.TAPNGO_DOWNLOAD_MSG);
            DialogHelper.createSimpleDialog(me, message, Utils.getString(me, R.string.btn_ok), new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //check if TheClub app exist
                    if (i != null) {
                        //to app directly
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    } else {
                        //to market download page
                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//							intent.setData(Uri.parse("http://www.tapngo.com.hk/download/"));
                            intent.setData(Uri.parse("market://details?id=" + "com.hktpayment.tapngo"));
                            startActivity(intent);

                        } catch (ActivityNotFoundException e) {

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//							intent.setData(Uri.parse("http://www.tapngo.com.hk/download/"));
                            intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + "com.hktpayment.tapngo"));
                            startActivity(intent);
                        }
                    }
                }
            }, Utils.getString(this, R.string.btn_cancel));
        } else {
            String message = i.resolveActivity(me.getPackageManager()) != null ? Utils.getString(me, R.string.THECLUB_TRANSITION_MSG) : Utils.getString(me, R.string.THECLUB_DOWNLOAD_MSG);

            DialogHelper.createSimpleDialog(me, message, Utils.getString(me, R.string.btn_ok), new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //check if TheClub app exist
                    if (i.resolveActivity(me.getPackageManager()) != null) {
                        //to app directly
                        startActivity(i);
                    } else {
                        //to market download page
                        try {

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setData(Uri.parse("market://details?id=" + "com.pccw.theclub"));
                            startActivity(intent);

                        } catch (ActivityNotFoundException e) {

                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + "com.pccw.theclub"));
                            startActivity(intent);
                        }
                    }
                }
            }, Utils.getString(this, R.string.btn_cancel));
        }
    }

    // Confirm Exit
    public final void displayConfirmExitDialog() {
        OnClickListener onclick = new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ClnEnv.clear(me);
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
                finish();
            }
        };
        DialogHelper.createSimpleDialog(me, getResources().getString(R.string.quit_msg), getResources().getString(R.string.myhkt_btn_yes), onclick, getResources().getString(R.string.myhkt_btn_no));
    }

    protected final void billMsgLoginRedirect() {
        AlertDialog.Builder builder = new Builder(MainMenuActivity.this);
        builder.setMessage(Utils.getString(me, R.string.MYHKT_PN_ERR_ACC_NOT_FOUND));
        builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
            Bundle rbundle;

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                dialog.dismiss();
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                //temp (Redirecting options, will remove if confirmed)
                //				rbundle = new Bundle();
                //				rbundle.putInt("CLICKBUTTON", R.id.mainmenu_button_service);
                //				intent.putExtras(rbundle);
                ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
                ClnEnv.getPushDataBill().clear();
                //temp
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        });
        builder.create().show();
    }


    protected final void billMsgRedirect(String msg) {
        AlertDialog.Builder builder = new Builder(MainMenuActivity.this);
        builder.setMessage(msg);
        builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                dialog.dismiss();
                intent = new Intent(getApplicationContext(), ServiceListActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        });
        builder.create().show();
    }

    private final void updateLoginStatus() {
        if (!ClnEnv.isLoggedIn()) {
            // TODO: update login/logout button imageView
            //			aq.navBarButton(R.id.navbar_button_right, R.drawable.ios7_login);
            aq.id(R.id.mainmenu_label_welcome).text("");
        } else {
            //			aq.navBarButton(R.id.navbar_button_right, R.drawable.ios7_logout);

            String rGreet = "";
            cal = Calendar.getInstance();
            int rHr = cal.get(Calendar.HOUR_OF_DAY);

            // the exact logic copied from CSP
            if (rHr >= 5 && rHr < 12) {
                rGreet = Utils.getString(me, R.string.SHLF_MORNING);
            } else {
                if (rHr >= 12 && rHr < 18) {
                    rGreet = Utils.getString(me, R.string.SHLF_AFTERNOON);
                } else {
                    rGreet = Utils.getString(me, R.string.SHLF_NIGHT);
                }
            }
            aq.id(R.id.mainmenu_label_welcome).text(String.format("%s,\n%s", rGreet, ClnEnv.getQualSvee().getSveeRec().nickname));
            aq.id(R.id.mainmenu_label_welcome).textColorId(ClnEnv.getSessionPremierFlag() ? R.color.white : R.color.hkt_txtcolor_grey);
        }
    }

    public void setLiveChangeIcon(Boolean isPause) {
        if (isPause) {
            if (aq != null) {
                aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
                aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);
                ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#DB9200"), PorterDuff.Mode.MULTIPLY);
                aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
            }
        } else {
            if (aq != null) {
                aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
                aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);
                aq.id(R.id.navbar_button_right).getImageView().clearColorFilter();
                //Premier: white , Non-Premier : blue
                if (debug) Log.i("TAG", ClnEnv.getSessionPremierFlag() + "");
                if (!ClnEnv.getSessionPremierFlag()) {
                    ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#217EC0"), PorterDuff.Mode.MULTIPLY);
                    aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
                } else {
                    ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
                    aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
                }
            }
        }
    }

    private final void SwitchApptIcon(final boolean hasAppt) {
        mainMenuGridViewAdapter.notifyDataSetChanged();
        if (ClnEnv.getSessionPremierFlag()) {
            int apptIconIndex = getIndexByMenuType(MAINMENU.MYAPPT);
            if (homeItems != null && homeItems.get(apptIconIndex) != null) {
                if (hasAppt) {
                    if (ClnEnv.getSessionPremierFlag()) {
                        homeItems.get(apptIconIndex).setImageResource(R.drawable.home_btn_appt_new_premier);
                    } else {
                        homeItems.get(apptIconIndex).setImageResource(R.drawable.myappointment_2);
                    }
                } else {
                    if (ClnEnv.getSessionPremierFlag()) {
                        homeItems.get(apptIconIndex).setImageResource(R.drawable.home_btn_appt_premier);
                    } else {
                        homeItems.get(apptIconIndex).setImageResource(R.drawable.myappointment);
                    }
                }
                mainMenuGridViewAdapter.notifyDataSetChanged();
            }
        }
    }

    private int getIndexByMenuType(MAINMENU menuType) {

        for (int i = 0; i < homeItems.size(); i++) {
            if (homeItems.get(i).type == menuType) {
                return i;
            }
        }

        return -1;

    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        displayConfirmExitDialog();
    }

    public void onSuccess(APIsResponse response) {
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy())) {
                //				debugLog(TAG, "doHelo complete!!");
            } else if (APIsManager.LGI.equals(response.getActionTy())) {
                //AUTO login
                LgiCra lgiCra = (LgiCra) response.getCra();
                ClnEnv.setLgiCra(lgiCra);
                Gson gson = new Gson();
                String result = gson.toJson(lgiCra);
                if (debug) Utils.showLog(TAG, result);
                //				// Save Account Type
                //				ClnEnv.setPref(this, getString(R.string.CONST_PREF_PREMIER_FLAG), ClnEnv.getQualSvee().getCustRec().isPremier());
                //				ClnEnv.setSessionPremierFlag(ClnEnv.getQualSvee().getCustRec().isPremier());

                ClnEnv.setPref(this, getString(R.string.CONST_PREF_SAVELOGINID), true);
                ClnEnv.setPref(this, getString(R.string.CONST_PREF_LOGINID), ClnEnv.getSessionLoginID());
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
                ClnEnv.setEncPref(me.getApplicationContext(), ClnEnv.getSessionLoginID(), me.getString(R.string.CONST_PREF_PASSWORD), ClnEnv.getSessionPassword());
                // New Bill Indicator Process
                saveAccountHelper = SaveAccountHelper.getInstance(me);
                // Removed all records if last bill date is one year or earlier than current Date
                saveAccountHelper.RemoveRecordForInitial(lgiCra.getServerTS());
                // make sure the BillAry Not null and empty Array
                //				if (lgiCra.getOBillListAry() != null){
                //					for (BillList binqcra : lgiCra.getOBillListAry()) {
                //						if (binqcra.getOBillAry() != null && binqcra.getOBillAry().length > 0) {
                //							try {
                //								// Check Database only if the bill date returned
                //								if (!"".equalsIgnoreCase(binqcra.getOBillAry()[0].getInvDate())) {
                //									// Get lastBilldate from database
                //									String lastBillDate = saveAccountHelper.getDateByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                //									if (lastBillDate == null) {
                //										saveAccountHelper.insert(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                //									} else if (CompareLastBillDate(lastBillDate, binqcra.getOBillAry()[0].getInvDate())) {
                //										saveAccountHelper.updateFlag(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                //									}
                //								}
                //							} catch (Exception e) {
                //								e.printStackTrace();
                //							}
                //						}
                //					}
                //				}

                // Appointment Indicator Process
                // Appt Alert Icon
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), false);
                if (lgiCra.getOGnrlApptAry() != null) {
                    for (int i = 0; i < lgiCra.getOGnrlApptAry().length; i++) {
                        GnrlAppt gnrlAppt = lgiCra.getOGnrlApptAry()[i];
                        if (Utils.CompareDateAdd3(me, gnrlAppt.getApptStDT(), lgiCra.getServerTS(), getString(R.string.input_datetime_format))) {
                            // Have an appointment
                            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), true);
                            me.SwitchApptIcon(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_FLAG), false));
                            break;
                        }
                    }

                }

                // SR Array
                if (lgiCra.getOSrvReqAry() != null) {
                    for (int i = 0; i < lgiCra.getOSrvReqAry().length; i++) {
                        SrvReq srvReq = lgiCra.getOSrvReqAry()[i];
                        if (Utils.CompareDateAdd3(me, srvReq.getApptTS().getApptDate(), lgiCra.getServerTS(), getString(R.string.input_date_format))) {
                            // Have an appointment
                            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), true);
                            me.SwitchApptIcon(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_FLAG), false));
                            break;
                        }
                    }
                }

                //				RedirectTargetScreen();
                //				updateLoginStatus(); //remove

                int rl = ClnEnv.getQualSvee().getSubnRecAry().length;
                int rx, rlz;
                //live accounts
                for (rx = 0; rx < rl; rx++) {

                    SubnRec subnRec = ClnEnv.getQualSvee().getSubnRecAry()[rx];
                }

                //zombie accounts
                rlz = ClnEnv.getQualSvee().getZmSubnRecAry().length;
                for (rx = 0; rx < rlz; rx++) {
                    SubnRec subnRec = ClnEnv.getQualSvee().getZmSubnRecAry()[rx];
                }

                // Line Test Result completed, redirect to targetService
                me.checkLnttResultAndRedirect();
                // Get GCM Message after autologin
                me.checkBillMsgAndRedirect();

            } else if (APIsManager.LGO.equals(response.getActionTy())) {
                ClnEnv.clear(me);

                ClnEnv.setSessionPremierFlag(false);
                //				updateLoginStatus(); //remove
                //				me.setContentView(R.layout.activity_mainmenu);
                me.initUI();
            }

        }
        initUI();
    }

    public void onFail(APIsResponse response) {
        if (response != null) {
            if (APIsManager.LGO.equals(response.getActionTy())) {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    DialogHelper.createSimpleDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    BaseActivity.ivSessDialog();
                } else {
                    DialogHelper.createSimpleDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
                ClnEnv.clear(me);

                ClnEnv.setSessionPremierFlag(false);
                //				me.setContentView(R.layout.activity_mainmenu);
                me.initUI();
            } else if (APIsManager.LGI.equals(response.getActionTy())) {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    DialogHelper.createSimpleDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    BaseActivity.ivSessDialog();
                } else {
                    DialogHelper.createSimpleDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
                // Keeping the username and password in memory
                ClnEnv.setSessionLoginID("");
                ClnEnv.setSessionPassword("");
                ClnEnv.setSessionSavePw(false);
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
            }
        }
        initUI();
    }

    @Override
    public void onDlSuccess() {
        //		Toast.makeText(me, "download success", 5000).show();
        ClnEnv.setAppConfigDownloaded(true);
        File imgFile = new File(getResString(R.string.PATH) + "images/" + ClnEnv.getAppLocale(getBaseContext()) + "/main_top_banner.png");
        if (imgFile.exists()) {
            if (!ClnEnv.getSessionPremierFlag()) {
                aq.id(R.id.mainmenu_topbanner).getImageView().setImageURI(Uri.fromFile(imgFile));
            }
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == LiveChatHelper.mRequestCodeFilePicker) {
            if (resultCode == Activity.RESULT_OK) {
                if (intent != null) {
                    if (LiveChatHelper.mFileUploadCallbackFirst != null) {
                        LiveChatHelper.mFileUploadCallbackFirst.onReceiveValue(intent.getData());
                        LiveChatHelper.mFileUploadCallbackFirst = null;
                    } else if (LiveChatHelper.mFileUploadCallbackSecond != null) {
                        Uri[] dataUris;
                        try {
                            dataUris = new Uri[]{Uri.parse(intent.getDataString())};
                        } catch (Exception e) {
                            dataUris = null;
                        }

                        LiveChatHelper.mFileUploadCallbackSecond.onReceiveValue(dataUris);
                        LiveChatHelper.mFileUploadCallbackSecond = null;
                    }
                }
            } else {
                if (LiveChatHelper.mFileUploadCallbackFirst != null) {
                    LiveChatHelper.mFileUploadCallbackFirst.onReceiveValue(null);
                    LiveChatHelper.mFileUploadCallbackFirst = null;
                } else if (LiveChatHelper.mFileUploadCallbackSecond != null) {
                    LiveChatHelper.mFileUploadCallbackSecond.onReceiveValue(null);
                    LiveChatHelper.mFileUploadCallbackSecond = null;
                }
            }
        }
    }

    @Override
    public void onDownloadAppConfig() {
        downloadService.downloadAppConfig();
        Log.d("lwg", "downloadService.downloadAppConfig()");
    }

    @Override
    public void onDownloadSuccess(String filename) {
        if (DownloadService.APP_CONFIG.equals(filename)) {
            downloadService.downloadImageBanner();
            Log.d("lwg", "downloadService.downloadImageBanner()");
        } else {
            if (!this.isPremier) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setNonPermierLayout();
                    }
                });
            }
        }
    }
}