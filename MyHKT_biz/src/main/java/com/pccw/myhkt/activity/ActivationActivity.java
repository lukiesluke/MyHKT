package com.pccw.myhkt.activity;

import java.lang.Thread.UncaughtExceptionHandler;

import androidx.appcompat.app.AlertDialog;
import android.app.Dialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Point;
import android.os.Bundle;
import android.text.InputType;
import android.view.Display;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegInputItem;


public class ActivationActivity extends BaseActivity{
	private AAQuery 	aq;
	private SveeRec sveeRec = null;
	private String 	TAG = this.getClass().getName();

	private int 					extralinespace;
	private int 					gridlayoutPadding = 0;
	private int 					buttonPadding = 0;
	private int 					deviceWidth = 0;
	private int 					colWidth = 0;

	private String					loginId;
	private String					pwd;

	private RegInputItem 			regInputLoginId;
	private RegInputItem 			regInputActCode;

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		me = this;
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		setContentView(R.layout.activity_activation);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		gridlayoutPadding = (int) getResources().getDimension(R.dimen.mainmenu_gridlayout_padding);
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
		Bundle bundle = this.getIntent().getExtras();
		if (bundle != null) {
			try {
				loginId = bundle.getString("LOGINID");
				pwd = bundle.getString("PWD");
			} catch (Exception e) {
				// fail to extract object
			}
		}

	}
	// Restore the Bundle data when re-built Activity
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		loginId = savedInstanceState.getString("LOGINID");
		pwd = savedInstanceState.getString("PWD");
	}

	// Saved data before Destroy Activity
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("LOGINID", loginId);
		outState.putString("PWD", pwd);
	}

	HKTButton  canceBtn;
	HKTButton  confirmBtn;
	protected final void initUI2() {
		aq = new AAQuery(this);

		//navbar
		aq.navBarBaseLayout(R.id.navbar_base_layout);
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);	
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_reg_title));
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");		

		aq.normText(R.id.activity_activation_heading, getResString(R.string.SACF_SMSACTN), 2);
		aq.normText(R.id.activity_activation_label_intro, getResString(R.string.SACF_ITO));
		aq.id(R.id.activity_activation_label_intro).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
		regInputLoginId = aq.regInputItem(R.id.activity_activation_loginid , getResString(R.string.SACF_LOGIN_ID), "", loginId,  InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		regInputActCode = aq.regInputItem(R.id.activity_activation_actcode , getResString(R.string.SACF_ACT_CODE), "", "",  InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

		aq.id(R.id.activity_activation_botar).getView().setPadding(extralinespace, extralinespace, extralinespace, 0);
		
//		canceBtn = (HKTButton) aq.id(R.id.activity_activation_btn_cancel).getView();
//		canceBtn.setText(getResString(R.string.SACF_REGEN));
//		LinearLayout.LayoutParams params_cancel = (LayoutParams) canceBtn.getLayoutParams();
//		params_cancel.rightMargin =buttonPadding;
//		canceBtn.setLayoutParams(params_cancel);
//		aq.id(R.id.activity_activation_btn_cancel).clicked(this, "onClick");	
//		confirmBtn = (HKTButton) aq.id(R.id.activity_activation_btn_confirm).getView();
//		confirmBtn.setText(getResString(R.string.SACF_SUBMIT));
//		LinearLayout.LayoutParams params_confirm = (LayoutParams) canceBtn.getLayoutParams();
//		params_confirm.leftMargin =buttonPadding;
//		confirmBtn.setLayoutParams(params_confirm);
//		aq.id(R.id.activity_activation_btn_confirm).clicked(this, "onClick");	
		aq.norm2TxtBtns(R.id.activity_activation_btn_cancel, R.id.activity_activation_btn_confirm, getResString(R.string.SACF_REGEN), getResString(R.string.SACF_SUBMIT ));
		aq.id(R.id.activity_activation_btn_confirm).clicked(this, "onClick");
		aq.id(R.id.activity_activation_btn_cancel).clicked(this, "onClick");

	}

	@Override
	protected final void onResume() {
		super.onResume();
		moduleId = getResources().getString(R.string.MODULE_REG);
		
		// Screen Tracker
	}	

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			break;	
		case R.id.activity_activation_btn_cancel:
			Utils.closeSoftKeyboard(me, v);
			LgiCra lgiCra = new LgiCra();
			SveeRec svee = new SveeRec();
			svee.loginId = loginId;
			svee.pwd = pwd;
			lgiCra.setISveeRec(svee);
			APIsManager.doRegenActCode(me, lgiCra);
			break;
		case R.id.activity_activation_btn_confirm:
			Utils.closeSoftKeyboard(me, v);
			// Activation Code Validation
			if ((regInputActCode.getInput().toString().length() == 0) || (regInputActCode.getInput().length() > getResources().getInteger(R.integer.MAX_ACTCODE))) {
				DialogHelper.createSimpleDialog(me, getResString(R.string.SACM_ILACTCODE));
			} else {
				AcMainCra acMainCra = new AcMainCra();
				acMainCra.setIActCode(regInputActCode.getInput().toString());
				acMainCra.setILoginId(loginId);
				APIsManager.doSMSActivation(me, acMainCra);
			}
			break;
		}
	}



	public void onSuccess(APIsResponse response) {
		super.onSuccess(response);
		if (response != null) {
			if (APIsManager.RG_ACTCD.equals(response.getActionTy())) {
				DialogHelper.createSimpleDialog(me, getResString(R.string.SACM_REGEN_DONE));
			} else if  (APIsManager.SMSACT.equals(response.getActionTy())) {
				// Save the loginID, Password
				ClnEnv.setSessionLoginID(loginId);
				ClnEnv.setSessionPassword(pwd);
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVELOGINID), true);
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_PASSWORD), pwd);
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LOGINID), loginId);
				
				Dialog.OnClickListener dialogListener = new  Dialog.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(intent);
						overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
						finish();
					}
				};
				
				DialogHelper.createSimpleDialog(me, getResString(R.string.SACM_DONE), getResString(R.string.btn_ok), dialogListener);
			}
		}
	}

	public void onFail(APIsResponse response) {
		super.onFail(response);
		if (response != null) {
			
			if (APIsManager.RG_ACTCD.equals(response.getActionTy())) {
				LgiCra lgiCra = (LgiCra) response.getCra();
				displayDialog(this, InterpretRCManager.interpretRC_SmsActnMdu(this, lgiCra.getReply()));
			} else if  (APIsManager.SMSACT.equals(response.getActionTy())) {
				AcMainCra acMainCra = (AcMainCra) response.getCra();
				displayDialog(this,	InterpretRCManager.interpretRC_SmsActnMdu(this, acMainCra.getReply()));
			} else {
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					displayDialog(this, response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					ivSessDialog();
				} else {
					displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
				}
				
			}
		
		}
	}

	// Android Device Back Button Handling
	public final void onBackPressed() {
		finish();
	}
}
