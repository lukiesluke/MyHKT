package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.TextView;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.entity.BcifRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.PopOverInputView;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.wheat.shared.tool.Reply;

/************************************************************************
 * File : RegCompanyInfoActivity.java 
 * Desc : Register Company Information
 * Name : RegCompanyInfoActivity
 * by 	: Abdulmoiz Esmail  
 * Date : 1/12/2017
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 31/12/2017 Abdulmoiz Esmail 		-First draft
 *************************************************************************/
public class RegCompanyInfoActivity extends BaseActivity {
    // Common Components
    private boolean debug = false;
    private String TAG = this.getClass().getName();
    private AAQuery aq;
    private final int colMaxNum = 3;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int buttonPadding = 0;
    private int basePadding = 0;
    private int extralinespace = 0;
    private int txtColor;
    private int regInputHeight;
    private Boolean isFirstTime = true;
    private int btnWidth = 0;

    private RegInputItem regInputCompName;
    private RegInputItem regInputPCName;
    private RegInputItem regInputPCTelNum;
    private RegInputItem regInputPCMobileNum;
    private RegInputItem regInputPCEmail;
    private RegInputItem regInputPCJobTitle;
    private RegInputItem regInputSCName;
    private RegInputItem regInputSCTelNum;
    private RegInputItem regInputSCMobileNum;
    private RegInputItem regInputSCEmail;
    private RegInputItem regInputSCJobTitle;

    private QuickAction mQAIndustry;
    private QuickAction mQAStaffNum;
    private QuickAction mQAHKBranches;
    private QuickAction mQABranches; //branches outside HK
    private QuickAction mQAPrimaryTitle;
    private QuickAction mQASecondaryTitle;

    ActionItem[] aIINdustry;
    ActionItem[] aITitles;
    ActionItem[] aIBranches;
    private PopOverInputView regComIndustryPopOver;
    private PopOverInputView regComStaffNumPopOver;
    private PopOverInputView regComHKBranchPopOver;
    private PopOverInputView regComBrachePopOver;
    private PopOverInputView regComPTitlePopOver;
    private PopOverInputView regComSTitlePopOver;
    private static final String SCREEN_NAME = "Registration Company Info Screen";
    private static final String[] INDUSTRY_VALUES = {
            "D_INDUST_OPT_ASSOC",
            "D_INDUST_OPT_BANK",
            "D_INDUST_OPT_PROF_SRVCS",
            "D_INDUST_OPT_CONSTRCT",
            "D_INDUST_OPT_EDU_SRVCS",
            "D_INDUST_OPT_INVSTMNT",
            "D_INDUST_OPT_GOVT",
            "D_INDUST_OPT_HEALTH",
            "D_INDUST_OPT_HOTEL",
            "D_INDUST_OPT_INSURNCE",
            "D_INDUST_OPT_MANUF",
            "D_INDUST_OPT_PERSNL_SRVS",
            "D_INDUST_OPT_REAL_ESTATE",
            "D_INDUST_OPT_RESTRNT",
            "D_INDUST_OPT_RETAIL",
            "D_INDUST_OPT_TELECOM",
            "D_INDUST_OPT_TRADE",
            "D_INDUST_OPT_TRANSPO_LOGC",
            //"D_INDUST_OPT_TRAVEL_AGNCY",
            "D_INDUST_OPT_UTILITY",
            "D_INDUST_OPT_WHOLSALE",
            "D_INDUST_OPT_OTHRS"};

    private static final String[] IS_BRANCH_OS_HK_VALUES = {"Y", "N"};
    private static final String[] TITLES_VALUES = {BcifRec.TTL_MR, BcifRec.TTL_MRS, BcifRec.TTL_MR};
    private BcifRec bcifRec = new BcifRec();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        if (debug) Log.i(TAG, "onCreate");

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        isZh = !ClnEnv.getAppLocale(this).equals(Utils.getString(this, R.string.CONST_LOCALE_EN));
        setContentView(R.layout.activity_reg_company_info);
        initData();
    }

    private void initData() {
        me = this;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        extralinespace = (int) getResources().getDimension(R.dimen.smalltext2size);
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        regInputHeight = (int) getResources().getDimension(R.dimen.reg_input_height);
        colWidth = (deviceWidth - basePadding * 4 + buttonPadding * 2) / colMaxNum;
        btnWidth = (deviceWidth - basePadding * 4) / 2;
        txtColor = getResources().getColor(R.color.hkt_txtcolor_grey);
    }

    @Override
    protected final void initUI2() {
        aq = new AAQuery(this);

        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_reg_title));
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        aq.id(R.id.com_info_title_textview).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(getResources().getColor(R.color.black)).text(getResString(R.string.comm_cominfo_title));
        aq.id(R.id.com_info_title_textview).getView().setPadding(basePadding, 0, basePadding, 0);

        aq.id(R.id.regservice_textview1).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(txtColor).text(getResString(R.string.comm_cominfo_des));
        aq.id(R.id.regservice_textview1).getView().setPadding(basePadding, 0, basePadding, 0);

        aq.id(R.id.regbasic_primary_contact).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(getResources().getColor(R.color.black)).text(getResString(R.string.comm_cominfo_pricontact));
        aq.id(R.id.regbasic_primary_contact).getView().setPadding(basePadding, 0, basePadding, 0);

        aq.id(R.id.regbasic_secondary_contact).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(getResources().getColor(R.color.black)).text(getResString(R.string.comm_cominfo_seccontact));
        aq.id(R.id.regbasic_secondary_contact).getView().setPadding(basePadding, 0, basePadding, 0);

        regInputCompName = aq.regInputItem(R.id.regbasic_input_comp_name, getResString(R.string.comm_cominfo_company), getResString(R.string.myhkt_myprof_nickname_hint), "", 20);


        //Primary contacts
        regInputPCName = aq.regInputItem(R.id.regbasic_primary_input_name, getResString(R.string.MYHKT_DQ_LBL_NAME), getResString(R.string.myhkt_myprof_nickname_hint), "", 20);
        aq.id(R.id.regbasic_primary_input_name).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        regInputPCTelNum = aq.regInputItem(R.id.regbasic_primary_input_tel_num, getResString(R.string.comm_cominfo_phone), getResString(R.string.myhkt_myprof_mobnum_hint), "",
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
        aq.id(R.id.regbasic_primary_input_tel_num).getView().setPadding(basePadding, extralinespace, basePadding, 0);
        regInputPCMobileNum = aq.regInputItem(R.id.regbasic_primary_input_mobile_num, getResString(R.string.comm_cominfo_mobile), getResString(R.string.myhkt_myprof_mobnum_hint), "",
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
        aq.id(R.id.regbasic_primary_input_mobile_num).getView().setPadding(basePadding, extralinespace, basePadding, 0);
        regInputPCEmail = aq.regInputItem(R.id.regbasic_primary_input_email_add, getResString(R.string.comm_cominfo_email), getResString(R.string.myhkt_myprof_email_hint), "",
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS, getResources().getInteger(R.integer.CONST_MAX_LOGINID));
        aq.id(R.id.regbasic_primary_input_email_add).getView().setPadding(basePadding, extralinespace, basePadding, 0);
        regInputPCJobTitle = aq.regInputItem(R.id.regbasic_primary_input_job_title, getResString(R.string.comm_cominfo_jobtitle), getResString(R.string.myhkt_myprof_nickname_hint), "", 20);
        aq.id(R.id.regbasic_primary_input_email_add).getView().setPadding(basePadding, extralinespace, basePadding, 0);


        //Secondary contacts
        aq.normTextGrey(R.id.regbasic_secondary_title_txt, getResString(R.string.comm_cominfo_name_title));
        regInputSCName = aq.regInputItem(R.id.regbasic_secondary_input_name, getResString(R.string.MYHKT_DQ_LBL_NAME), getResString(R.string.myhkt_myprof_nickname_hint), "", 20);
        aq.id(R.id.regbasic_secondary_input_name).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        regInputSCTelNum = aq.regInputItem(R.id.regbasic_secondary_input_tel_num, getResString(R.string.comm_cominfo_phone), getResString(R.string.myhkt_myprof_mobnum_hint), "",
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
        aq.id(R.id.regbasic_secondary_input_tel_num).getView().setPadding(basePadding, extralinespace, basePadding, 0);
        regInputSCMobileNum = aq.regInputItem(R.id.regbasic_secondary_input_mobile_num, getResString(R.string.comm_cominfo_mobile), getResString(R.string.myhkt_myprof_mobnum_hint), "",
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
        aq.id(R.id.regbasic_secondary_input_mobile_num).getView().setPadding(basePadding, extralinespace, basePadding, 0);
        regInputSCEmail = aq.regInputItem(R.id.regbasic_secondary_input_email_add, getResString(R.string.comm_cominfo_email), getResString(R.string.myhkt_myprof_email_hint), "",
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS, getResources().getInteger(R.integer.CONST_MAX_LOGINID));
        aq.id(R.id.regbasic_secondary_input_email_add).getView().setPadding(basePadding, extralinespace, basePadding, 0);
        regInputSCJobTitle = aq.regInputItem(R.id.regbasic_secondary_input_job_title, getResString(R.string.comm_cominfo_jobtitle), getResString(R.string.myhkt_myprof_nickname_hint), "", 20);
        aq.id(R.id.regbasic_secondary_input_email_add).getView().setPadding(basePadding, extralinespace, basePadding, 0);
        //TODO popover

        //Next button
        aq.normTxtBtn(R.id.regservice_btn_next, getResString(R.string.REGF_NEXT), btnWidth);
        aq.id(R.id.regservice_btn_next).clicked(this, "onClick");

        regComIndustryPopOver = aq.popOverInputView(R.id.regbasic_industry_spinner, getResString(R.string.comm_cominfo_select_default));
        regComStaffNumPopOver = aq.popOverInputView(R.id.regbasic_num_staff_spinner, getResString(R.string.comm_cominfo_select_default));
        regComHKBranchPopOver = aq.popOverInputView(R.id.regbasic_num_hk_branch_spinner, getResString(R.string.comm_cominfo_select_default));
        regComBrachePopOver = aq.popOverInputView(R.id.regbasic_num_branch_spinner, getResString(R.string.comm_cominfo_select_default));
        regComPTitlePopOver = aq.popOverInputView(R.id.regbasic_primary_title_spinner, getResString(R.string.comm_cominfo_select_default));
        regComSTitlePopOver = aq.popOverInputView(R.id.regbasic_secondary_title_spinner, getResString(R.string.comm_cominfo_select_default));

        //Industry
        aq.id(R.id.regbasic_industry_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_industry_txt, getResString(R.string.comm_cominfo_industry));
        aq.layoutWeight(R.id.regbasic_industry_txt, 1);
        TextView textView2 = aq.id(R.id.regbasic_industry_txt).getTextView();
        textView2.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_industry_spinner, 1);

        //Staff Num
        aq.id(R.id.regbasic_num_staff_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_num_staff_txt, getResString(R.string.comm_cominfo_staffnum));
        aq.layoutWeight(R.id.regbasic_num_staff_txt, 1);
        TextView textView3 = aq.id(R.id.regbasic_num_staff_txt).getTextView();
        textView3.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_num_staff_spinner, 1);

        //HK Branches
        aq.id(R.id.regbasic_num_hk_branch_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_num_hk_branch_txt, getResString(R.string.comm_cominfo_branchnum));
        aq.layoutWeight(R.id.regbasic_num_hk_branch_txt, 1);
        TextView textView4 = aq.id(R.id.regbasic_num_hk_branch_txt).getTextView();
        textView4.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_num_hk_branch_spinner, 1);

        //Outside HK Branch
        aq.id(R.id.regbasic_num_branch_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_num_branch_txt, getResString(R.string.comm_cominfo_branchhk));
        aq.layoutWeight(R.id.regbasic_num_branch_txt, 1);
        TextView textView5 = aq.id(R.id.regbasic_num_branch_txt).getTextView();
        textView5.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_num_branch_spinner, 1);

        aq.id(R.id.regbasic_primary_title_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_primary_title_txt, getResString(R.string.comm_cominfo_name_title));
        aq.layoutWeight(R.id.regbasic_primary_title_txt, 1);
        TextView textView6 = aq.id(R.id.regbasic_primary_title_txt).getTextView();
        textView6.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_primary_title_spinner, 1);


        aq.id(R.id.regbasic_secondary_title_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_secondary_title_txt, getResString(R.string.comm_cominfo_name_title));
        aq.layoutWeight(R.id.regbasic_secondary_title_txt, 1);
        TextView textView7 = aq.id(R.id.regbasic_secondary_title_txt).getTextView();
        textView7.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        aq.layoutWeight(R.id.regbasic_secondary_title_spinner, 1);
        setQuickAction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        firebaseSetting.onScreenView(RegCompanyInfoActivity.this, SCREEN_NAME);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.regbasic_industry_spinner:
                mQAIndustry.show(v, aq.id(R.id.regbasic_industry_spinner).getView());
                mQAIndustry.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_num_staff_spinner:
                mQAStaffNum.show(v, aq.id(R.id.regbasic_num_staff_spinner).getView());
                mQAStaffNum.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_num_hk_branch_spinner:
                mQAHKBranches.show(v, aq.id(R.id.regbasic_num_hk_branch_spinner).getView());
                mQAHKBranches.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_num_branch_spinner:
                mQABranches.show(v, aq.id(R.id.regbasic_industry_spinner).getView());
                mQABranches.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_primary_title_spinner:
                mQAPrimaryTitle.show(v, aq.id(R.id.regbasic_primary_title_spinner).getView());
                mQAPrimaryTitle.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regbasic_secondary_title_spinner:
                mQASecondaryTitle.show(v, aq.id(R.id.regbasic_secondary_title_spinner).getView());
                mQASecondaryTitle.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.regservice_btn_next:
                saveCompanyInfo();

                break;
        }
    }

    private void setQuickAction() {

        aIINdustry = new ActionItem[21];
        aIINdustry[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_ind_asso));
        aIINdustry[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_ind_bank));
        aIINdustry[2] = new ActionItem(2, getString(R.string.comm_cominfo_select_ind_business));
        aIINdustry[3] = new ActionItem(3, getString(R.string.comm_cominfo_select_ind_const));
        aIINdustry[4] = new ActionItem(4, getString(R.string.comm_cominfo_select_ind_edu));
        aIINdustry[5] = new ActionItem(5, getString(R.string.comm_cominfo_select_ind_finance));
        aIINdustry[6] = new ActionItem(6, getString(R.string.comm_cominfo_select_ind_gov));
        aIINdustry[7] = new ActionItem(7, getString(R.string.comm_cominfo_select_ind_health));
        aIINdustry[8] = new ActionItem(8, getString(R.string.comm_cominfo_select_ind_hotel));
        aIINdustry[9] = new ActionItem(9, getString(R.string.comm_cominfo_select_ind_insurance));
        aIINdustry[10] = new ActionItem(10, getString(R.string.comm_cominfo_select_ind_manuf));
        aIINdustry[11] = new ActionItem(11, getString(R.string.comm_cominfo_select_ind_personal));
        aIINdustry[12] = new ActionItem(12, getString(R.string.comm_cominfo_select_ind_estate));
        aIINdustry[13] = new ActionItem(13, getString(R.string.comm_cominfo_select_ind_restaurant));
        aIINdustry[14] = new ActionItem(14, getString(R.string.comm_cominfo_select_ind_retail));
        aIINdustry[15] = new ActionItem(15, getString(R.string.comm_cominfo_select_ind_IT));
        aIINdustry[16] = new ActionItem(16, getString(R.string.comm_cominfo_select_ind_trade));
        aIINdustry[17] = new ActionItem(17, getString(R.string.comm_cominfo_select_ind_transport));
        aIINdustry[18] = new ActionItem(18, getString(R.string.comm_cominfo_select_ind_utility));
        aIINdustry[19] = new ActionItem(19, getString(R.string.comm_cominfo_select_ind_wholesale));
        aIINdustry[20] = new ActionItem(20, getString(R.string.comm_cominfo_select_ind_other));


        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        int screenWidth = wm.getDefaultDisplay().getWidth();

        mQAIndustry = new QuickAction(this, screenWidth / 2, 0);
        for (int i = 0; i < aIINdustry.length; i++) {
            mQAIndustry.addActionItem(aIINdustry[i]);
        }

        //setup the action item click listener
        mQAIndustry.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComIndustryPopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });

        aq.id(R.id.regbasic_industry_spinner).clicked(this, "onClick");

        ActionItem[] aINumberStaff = new ActionItem[6];
        aINumberStaff[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_staffnum_1));
        aINumberStaff[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_staffnum_2));
        aINumberStaff[2] = new ActionItem(2, getString(R.string.comm_cominfo_select_staffnum_3));
        aINumberStaff[3] = new ActionItem(3, getString(R.string.comm_cominfo_select_staffnum_4));
        aINumberStaff[4] = new ActionItem(4, getString(R.string.comm_cominfo_select_staffnum_5));
        aINumberStaff[5] = new ActionItem(5, getString(R.string.comm_cominfo_select_staffnum_6));

        mQAStaffNum = new QuickAction(this, screenWidth / 2, 0);
        for (int i = 0; i < aINumberStaff.length; i++) {
            mQAStaffNum.addActionItem(aINumberStaff[i]);
        }
        //setup the action item click listener
        mQAStaffNum.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComStaffNumPopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });
        //aq.popOverInputView(R.id.regbasic_num_staff_spinner,aINumberStaff[0].getTitle());
        aq.id(R.id.regbasic_num_staff_spinner).clicked(this, "onClick");

        ActionItem[] aIHkBranches = new ActionItem[4];
        aIHkBranches[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_branchnum_1));
        aIHkBranches[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_branchnum_2));
        aIHkBranches[2] = new ActionItem(2, getString(R.string.comm_cominfo_select_branchnum_3));
        aIHkBranches[3] = new ActionItem(3, getString(R.string.comm_cominfo_select_branchnum_4));

        mQAHKBranches = new QuickAction(this, screenWidth / 2, 0);
        for (int i = 0; i < aIHkBranches.length; i++) {
            mQAHKBranches.addActionItem(aIHkBranches[i]);
        }

        aIBranches = new ActionItem[2];
        aIBranches[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_yes));
        aIBranches[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_no));

        mQABranches = new QuickAction(this, screenWidth / 2, 0);
        for (int i = 0; i < aIBranches.length; i++) {
            mQABranches.addActionItem(aIBranches[i]);
        }
        //setup the action item click listener
        mQAHKBranches.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComHKBranchPopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });
        //setup the action item click listener
        mQABranches.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComBrachePopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });
        aq.id(R.id.regbasic_num_hk_branch_spinner).clicked(this, "onClick");
        aq.id(R.id.regbasic_num_branch_spinner).clicked(this, "onClick");

        aITitles = new ActionItem[3];
        aITitles[0] = new ActionItem(0, getString(R.string.comm_cominfo_select_title_mr));
        aITitles[1] = new ActionItem(1, getString(R.string.comm_cominfo_select_title_mrs));
        aITitles[2] = new ActionItem(2, getString(R.string.comm_cominfo_select_title_ms));

        mQAPrimaryTitle = new QuickAction(this, screenWidth / 2, 0);
        for (int i = 0; i < aITitles.length; i++) {
            mQAPrimaryTitle.addActionItem(aITitles[i]);
        }

        mQASecondaryTitle = new QuickAction(this, screenWidth / 2, 0);
        for (int i = 0; i < aITitles.length; i++) {
            mQASecondaryTitle.addActionItem(aITitles[i]);
        }

        //setup the action item click listener
        mQAPrimaryTitle.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComPTitlePopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });
        mQASecondaryTitle.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                regComSTitlePopOver.setText(quickAction.getActionItem(position).getTitle());
            }
        });

        aq.id(R.id.regbasic_primary_title_spinner).clicked(this, "onClick");
        aq.id(R.id.regbasic_secondary_title_spinner).clicked(this, "onClick");

    }

    private void saveCompanyInfo() {

        bcifRec.custNm = regInputCompName.getInput();
        bcifRec.industry = isValidSpinSelected(regComIndustryPopOver.getText()) ? INDUSTRY_VALUES[getIndex(aIINdustry, regComIndustryPopOver.getText())] : "";
        bcifRec.nuStaff = isValidSpinSelected(regComStaffNumPopOver.getText()) ? regComStaffNumPopOver.getText() : "";
        bcifRec.nuPresence = isValidSpinSelected(regComHKBranchPopOver.getText()) ? regComHKBranchPopOver.getText() : "";
        bcifRec.xborder = isValidSpinSelected(regComBrachePopOver.getText()) ? IS_BRANCH_OS_HK_VALUES[getIndex(aIBranches, regComBrachePopOver.getText())] : "";
        //primary contact
        bcifRec.priCtName = regInputPCName.getInput();
        bcifRec.priCtTitle = isValidSpinSelected(regComPTitlePopOver.getText()) ? TITLES_VALUES[getIndex(aITitles, regComPTitlePopOver.getText())] : "";
        bcifRec.priCtMail = regInputPCEmail.getInput();
        bcifRec.priCtJobtitle = regInputPCJobTitle.getInput();
        bcifRec.priCtTel = regInputPCTelNum.getInput();
        bcifRec.priCtMob = regInputPCMobileNum.getInput();
        //secondary contact
        bcifRec.secCtName = regInputPCName.getInput();
        bcifRec.secCtTitle = isValidSpinSelected(regComSTitlePopOver.getText()) ? TITLES_VALUES[getIndex(aITitles, regComSTitlePopOver.getText())] : "";
        bcifRec.secCtMail = regInputSCEmail.getInput();
        bcifRec.secCtJobtitle = regInputSCJobTitle.getInput();
        bcifRec.secCtTel = regInputSCTelNum.getInput();
        bcifRec.secCtMob = regInputSCMobileNum.getInput();

        Reply reply = bcifRec.verifyCustNm();

        if (reply.isSucc()) reply = bcifRec.verifyIndustry();
        if (reply.isSucc()) reply = bcifRec.verifyNuStaff();
        if (reply.isSucc()) reply = bcifRec.verifyNuPresence();
        if (reply.isSucc()) reply = bcifRec.verifyXBorder();
        //primary contact
        if (reply.isSucc()) reply = bcifRec.verifyPriCtName();
        if (reply.isSucc()) reply = bcifRec.verifyPriCtTitle();
        if (reply.isSucc()) reply = bcifRec.verifyPriCtMail();
        if (reply.isSucc()) reply = bcifRec.verifyPriCtJobTitle();
        if (reply.isSucc()) reply = bcifRec.verifyPriCtTel();
        if (reply.isSucc()) reply = bcifRec.verifyPriCtMob();

        if (reply.isSucc()) reply = bcifRec.verifySecCtName();
        if (reply.isSucc()) reply = bcifRec.verifySecCtTitle();
        if (reply.isSucc()) reply = bcifRec.verifySecCtMail();
        if (reply.isSucc()) reply = bcifRec.verifySecCtJobTitle();
        if (reply.isSucc()) reply = bcifRec.verifySecCtTel();
        if (reply.isSucc()) reply = bcifRec.verifySecCtMob();


        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_AcMainMdu(this, reply));
        } else {
            Bundle bundle = getIntent().getExtras();
            bundle.putSerializable(RegBasicActivity.BUNDLE_COMPANY_INFO_REC, bcifRec);


            Intent intent;
            intent = new Intent(getApplicationContext(), RegConfirmActivity.class);
            intent.putExtras(bundle); //Pass bundle from regBasiActivity
            startActivity(intent);
            overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        }


    }

    private boolean isValidSpinSelected(String selected) {
        return !selected.equalsIgnoreCase(getResString(R.string.comm_cominfo_select_default));
    }

    private int getIndex(ActionItem[] actionItems, String text) {

        for (int i = 0; i < actionItems.length; i++) {
            if (actionItems[i].getTitle().equals(text)) {
                return i;
            }
        }
        return -1;
    }
}
