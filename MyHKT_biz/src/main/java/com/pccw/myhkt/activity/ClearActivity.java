package com.pccw.myhkt.activity;



import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;



/************************************************************************
 File       : AboutUsActivity.java
 Desc       : My Exclusive Benefits
 Name       : BenefitsActivity
 Created by : Vincent Fung
 Date       : 06/11/2013

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 06/11/2013  Vincent Fung		- First draft for demo
 14/02/2014  Derek Tsui			- added google analytics screen/event tracking
*************************************************************************/

public class ClearActivity extends Activity {
	// Common Components
	private boolean					debug				= false;									// toggle debug logs
	private static ClearActivity	me;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		me = this;
		if (getIntent().getExtras() != null && getIntent().getExtras().getString("FLAG").equals("PLAY")) {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=com.pccw.biz.myhkt"));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			try {
				startActivity(intent);
			} catch (Exception e) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.pccw.biz.myhkt" )));
			}
			finish();
		} else {
		    finish();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		//Screen Tracker
		
	}


}