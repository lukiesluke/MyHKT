package com.pccw.myhkt.activity;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;

import com.hktpayment.tapngosdk.TapNGoPayResult;
import com.hktpayment.tapngosdk.TapNGoPayment;
import com.hktpayment.tapngosdk.TapNGoPaymentActivity;
import com.hktpayment.tapngosdk.TapNGoSdkSettings;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.LiveChatHelper;
import com.pccw.myhkt.Tool;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.RegImageButton;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.model.SubService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

	public class TapNGoActivity extends TapNGoPaymentActivity {
		private static boolean isTapnGoStarted = false;
		
		//UAT
		
		private static final String APP_ID_LTS_UAT = "5982833503";
		private static final String APP_ID_PCD_UAT = "9195370636";
		private static final String APP_ID_TV_UAT = "9574656243";
		private static final String APP_ID_MOB_UAT = "5396499863";
		private static final String APP_ID_101_UAT = "7991681490";
		
		private static final String API_KEY_LTS_UAT = "kcWfrXWMpD1UaNVmpHmdUa+D2LN/d1VoHQYSn0pqpL++jxoCIvUiPhCFSMVwZP/Ao8j5UiGvlQKNhzacsWtZHA==";
		private static final String API_KEY_PCD_UAT = "Lubpn0CHqmA9OLnyb0y3s1r6/9yv26QyjxXS8W6k+1I+PMG3SXYjvc3l6buGJNjSaYLQZZB2Kee8t7wQTMTGBg==";
		private static final String API_KEY_TV_UAT  = "YUm7h13DDAx3eOPPg7v+HMtDLb/aYgyjLvpaITcaB3JjnCPkO5J+oejatFZssvhzARRKQ068tdA0VBzm0HChAA==";
		private static final String API_KEY_MOB_UAT = "fEH4mQMXP886QIF+WzPoOt8Cc/rBdYsb7Ch/7BMpkEWtDBbWOP2fIpgCQ6fERfA/YgyFSeTy5ISO4EjfoTdTYA==";
		private static final String API_KEY_101_UAT = "odGXLNF8uoqwqyiF4PO6/JPnqbP55RZ8jfyNjPxVpA40TbOSQuuFlktajxD08ROg0F2kRZYBIxIepkJu/RwI6Q==";
		
		private static final String PUBLIC_KEY_LTS_UAT = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAlqLt/wSBz0qWMBYsSTAfUrbpxrVGyBNtORWAoLA9Iq/Gx/N3YXXBSxcPFFAjOZBuO0yd6Am5khWTMWuer1BWlOcDKHyS8JkliBnRf4btB8uKXRClUiVOgY84TqYVwxpp/HSe6Lz0hwGBR0/bcZTwT3hBYonSpfQ2vqZP/Rri4jgVOTQyX3y5cqkNDdmope/VZuQZW+L+87CQErlxlBV/x9L8KTOa59lVQGkJgUDN5YJ/BbVawt8kS8fW8ixUYQURHNOnC1U+0Ug9pRB6kvBshk2XSf7/9olPH9xRmInMUbsCzQ65D4+XAaz9RJ5XFTDN/n91/PKldurCdDUrifCtcMirbxSVhjjHPMQ3Pkw1r9IpWH2h4YNT129UdczMc9OW90AgH8ZdsDQm3ZrpyChE2CY29yYrpDrghOnbF+wc25KXuaezB9QTfHeYyNq6ZlZgVIFcJXs7/Jbxw78ww+hoQ69okaHQb03knpC05PsEi6LR4XAxtYWeZao6tB8AGpViX1VsbJAjiCU8/oULr5GgYP1wh1+oo3d35/EAplYCe/f5CwllRg+Pe0iu1Q6hiv9y6Z9ZkF9tYBKuScX6QpB9N+JNLLU6GafzmVuqfd1VhX4bcIklBWkrPQMw8nyHCdBu3qL4DzkjQk8F5uI5m1sXInNqr5aK/WC+jWKl400GcfcCAwEAAQ==";
		private static final String PUBLIC_KEY_PCD_UAT = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAtym6bTWzLACj2vSeR8SBd+bbpycTJ821vIdpBjipMGniBqGk2ujI17ij9B1+dLm+2PSsZNMJVpN5HxOn+tPB2mKjAeERMsDuRIzzuqi+Q6KAT00ItVAekXqYqfiZMrGJG890NLYAOCckzYbGkm9skcUZmALFubeFh0WduuEu2o6omqwo0EIqMf1SaXCddAU4HXLJ9D3dhtwbo3SSScPfuMrrxKmWmwZya4WlB7QoBtGu+HK9T9zvWKe5ZYJ1bEEVAmhHKrO+2v+p1h2+JbZHbvFQTnoLsg4J5DJIByODVRve6OLzWhDyoV/BLjQiXJD3Ov8Ci6uEYX5NDM7FN0r+WC6voeETBjMUZDujJJUQNoDZMGrDFmYdjvylns/q0YGNXIIRDLM+qYP4WF0NRFOACNhTDgcc+eIT1j3n4xxJ+G+7myREjNolHVSe3ySRpLJ/PM0dobKO82y4SlBPl7yT7UKfWCUOaksDdmnyu85gk2LVeXCE7Qb3v+aEl8HQS43/uFatQWuPnxlDZPXkTcxXQO7+5WhyPBD2KQmZr0KP1sTjV9MFc2hHdmlptZuBKKB7YGoQvUVJRD/jHfGSXiKytBgSCogzAHUEc8zV+++lGi8BxP82PjWfcomy/FWLC4sOTdf94o/GVyNX/Xw8fPGttWCzZhXmG5hHANBaZs5twT0CAwEAAQ==";
		private static final String PUBLIC_KEY_TV_UAT  = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAlDyPJ38emupwee7azYECM+i65IXbB2BhPvm0rsj3waJOPW4F86D8jfyXdmsFK11o3fSae6UpXsCosLWQ1TnDkNoy02jSxWNuOhkuzYDWMBgS+l74lHOVAJMAryIPJuYxg0zXlilIsOUF6/aor6SsBa7uxQd3n1tSEYPnwSlSguRjrbG9vUFed3MikvhhOTLcU6pDy+RztTMQ/3jkdtvu6zmJBpQLDTehZU/4Hm7xMz25yFES6yau6QDLQ0wcD3Wmtkb48D2zGBV52Z13liULxbt8gfQ3Rgilk8kkJNwHsTMlMc63sRe2F0DMlq2lJl3iGwjmQ05oqeW2SdBI0EqWbY/t3EUJJltURnWNITcDAIgVM2R0NSoO3Gw2qBHvZYBPJtH8h8dfm875s5OnM94xZQ0mXzZqaM7+TlnQnAk+QIIEPFrZIZXdCqjycMEr/fhEd+NI8ekXb6qD+cLjoFsQ78hl6KZmOEfG70wKJx5+7Bqy++2MW3K6DwUV7imAlnmfJuZPTMcKZog1yFo+hgLQv8Hp1MBKVCbYbFeRAmBaKfmPq98Kh6wDg+3uoVuwwm6NseTL0sMUUmA/WJvckTG6whgt0bT5kTvEQHJzAr6WuZPiHewOcFzSNGMPIspEmd2RIpg2215dhqoxt06wduqAnnRfELWWSRuhQFmEpO3ysXcCAwEAAQ==";
		private static final String PUBLIC_KEY_MOB_UAT = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAjVDIemwfx//QP81cpTu9EZI/CeG9GFy8a7w7oXgrbKj6OwN+l4b9sZ4+u/COzP9mgPdhLAU/20VOh8tig8wOLCqvrrWXWbkCplRQCIqVfOkoSvtRWUYOdVh3oObWhmw9P8tzMAIyKUnfbhqfDxI/h/4yenWN8ZOCBlQGP/AjEF1tm3H7gJnexaP2JjDokCW6MnvG8lZt7uWd+wkXiyk9VaJLC0fiP0anxT034ECg5gC8dkGQHk6RneKqrdDOAqjml7+fT29QS+544+2wRZPvvI5Hm+PElpycT70uibPKHEJrK50l1xCye/tpoe8JjV59uNQpPZZbQyB55ZQFtEYktHadPIwqxGN/0kOqYuiPZgKdMcO6XNvVpi7cbcIN3chAhi+XFPP4v++qfjXJqvmVwKT3dumqHPg0rRWtJz+BbggSB3zpNtSRrLMiZlc646nCFZS67vVFgU6oRFkmJYFI765HwBkYJhZ8a1esbxY/q9TjbxElcub7EkYOzJm1REhzHG8xnu/zevc2rZhpuzEVXBYfNWXlfyrQoK+GKlhwGyn8NSCFBKX43FXhTgYkrU/MUf0Ksog91BLpBQa4gmZk4vp46GJMrR88kTiZYc7OVS9/qIKou3/KbyYueTVK38O5yU+QvYK65ckOn1bgkmt9CpGgGzhQ5AqDj3FPGHyGeFUCAwEAAQ==";
		private static final String PUBLIC_KEY_101_UAT = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAmjZe9mqikNiyaVnnldaWlo6rIIaQxRkV8cm7k4sQtSq7FhmutEPPAMw4hKx9mk/pLLFNQARwvBu186ohKX/eOI8dFMNo2nJX+kMXekdUFlfUe/MHUbvitqE7n+lQPyez9qd/qMUiaM1HWk3JRZ9rco2VRALsDdi2OaG1cYmeH9T3a95lUMks+JcoR+DPzz2wSPUm548xDXMIZJhK/qngwVsmx6N3rDsdNnVJZiXI8h4uMWJ1r9ekhYFfLgxyyUZRah1MhM8xhiKxkFVayIyxGV6G2tKIBN9qL+Vj02ju75qBX0CIp51egUCw8O8XgK/odELLgbP+V59y5yFsNVmjRFHRuplgcn41zgp77cd5zlwQYWU02ot/4iJAqQ4jPweFQXv7rJ3r6VvPqs8ZE1GsgIfGXk/7qAnan9QAKYmCw5kOZLdWCaLcvKHuHpn/ifaTgMlrhWek1dyb+EnD1fEpOHc3GZow9aQcBMJnH8cVcilw4cJ31FEYvf+8KpS15tISvjGR7v9B0e4x5N7W5FmNwgQcDI0yOzWVkXtmjcWmZdY0sd10qq+ukJVloR0d41XNvprUmrqafbD6QGoAo9YcJvYEVp8AozXsPouFy28NiyNkTSZbNhqCOYJbtzUeBTTO88YXKNz8+C/9hZEooJo0A6U++Go8FBUAdPFwH0MGt48CAwEAAQ==";
		
		//PRD 			
		private static final String APP_ID_LTS_PRD = "9628142264";
		private static final String APP_ID_PCD_PRD = "8468955844";
		private static final String APP_ID_TV_PRD = "8748951986";
		private static final String APP_ID_MOB_PRD = "1021485156";
		private static final String APP_ID_101_PRD = "9860080693";
		
		private static final String API_KEY_LTS_PRD = "4ILhPSOM/5OfAzzMKfZMChTslpUi4W0bw5h9gmvIde2tvdfm1/eXre9laramK5CWcucR58lNj44/sKaWQKJ0UA==";
		private static final String API_KEY_PCD_PRD = "3qCda5DZQkago46uglDibS4gsou7h7AdUNwHVRPIOzw5UDNdP51h1tqCWhqsDp9EqyfySIqpoaXdyoIpAya+wA==";
		private static final String API_KEY_TV_PRD  = "UZDyf24R9cZp0OfU0+tpJnhbKeHVs/BsNBm9rG8wwqLnyy1DqWFagchSapiVqbOGZZMiD8QMMa1V7NEqU0Mpiw==";
		private static final String API_KEY_MOB_PRD = "fEMTnm5mMkAAiSSLbFQZD5ZVQgzPregadWO9TjzEESJw/EXf1J+cCaQ6Og4GvF6ZuXLbvCVp2tNcuVzuQN3K+w==";
		private static final String API_KEY_101_PRD = "UoWGJxXrzac9hnnZ81yx3XBGk42EpU7CYuamcJzp5rwnEIGbxc7pbBdKPDTrnAigSavxqqmF6d6qthUuvyaoxg==";
		
		private static final String PUBLIC_KEY_LTS_PRD = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAqpBz+wSfyb/qNBs9rt7sArrQfIw9qWFg28set2Wa0SFbEY8y+XT7U19edvO9/DCkNUSnSdoDsuiAwrfGNTD7RSs7Q4uU+tfqkkWbWHKpgH4jNfsgdYvB/TyXFKHRKuhJoPc4TLcQ+Pwm8weSp7kwWbpi+I7xYvfe37RiyoBlQilJh8DkL5pV0zCBHCTh4qGDQEjX9m83RsZJkFCELK/9AwHf/P1DzXIQG/od0uFi41+Ap4BJB8Gd3aMxCKmRs1z0zmm8ahsm7K3HWQsf4taka0KvF8QdoZb9XlNrwvPtIXm/8hydhqaXJIE4UoBkt7XdidYFIuFqfupa6XCs7IQiD+38mLNVQxW/q/k9f0j/QS/ZYwctuyX6HxEmVpoYmWFZ6YscKbLZMAIjaui4xuFCSP6vn6HqZGXw9v3CTp8xxoUSJaA+sG0yFq7WnK8eRTbBLJ5re4+khqFbUhpNtASrsoi+1xEEbyxdcd5UZlE/w7F9zvAqfRS5DGqFMd7YCTe4buOubLe+CgDGskBgFeNm6W9o0ZO3C7FApR+Oep1WtKdpFPATAHYlSlDU1xd4yt3KZoB80iGUZHRRSPpbDFHxrAgxOb/1t5u+isagicAvbELRjhUo9/GnlDPwmoFN1H6HHS/E5cr4/7bGElXylEWamNQ1ttUy2MokZ7kRgpypmtkCAwEAAQ==";
		private static final String PUBLIC_KEY_PCD_PRD = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA+WeMCR79G+TcUu/3VqsI377Lpj9zQhO82OqdqJMNRYLoE0YBYXD7vucKQr31wSQQJ+guHL7ptU0yPbfern0eAQrBvtlq+UgOJWbK2BpIYKIc2prskuTMMAH9tefCBvw6E6nCFkFiyZqzAgRksILGyD82BjXHIgENfR8raU75Y5TvaQ9nBBXHS47xM6SXwCz0r2bMtLShYrM3wbhIorSOO1l8cRSczZtN6544yegTO7bEPa9WSVpFEX43am8+G0hArG72Ol1VbgCoYW18CST0fAeg1uKi7DmCUNN36qGXfblB+Sfe403J0FI/B2ZZ++C5HT/LM6+MwNJyLX1meCMHeThGWAvQot8xCpUqUU4Bcc9ea4FrByVCvI/YxksWXIHmHx0e6GMRNOu6Jm70RBBAG2WxEQtsDqc8x4+euzauWRNZApYe5vIArMZsJIoXtAZZDituKt4IiX611muSwdhwcGTpKkztYZ6JFPozBxWinXJiE0WRfgYg0j0XdFheBEMvDlAGNqrsDiMR35wcioJKpxqmgfWLufAhk8oCEo1rU+hytCsXSytPvqOOiOlN6V4avcD4WWpRy8QgVuSXwCTZps1a4sh+X9BG9sNwLfPdL8GIVGgS0hi7+tivxGa5/iJIPcM3REc+Hp+0UVl6pU83dFPK/J8OhxX56J/h/4Fsi3UCAwEAAQ==";
		private static final String PUBLIC_KEY_TV_PRD  = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAjWfecBmPnAZHIjAE5dc2AHsR2wXD8gweTe7HiU8hE9k385qBQV6czDZAOdUQK2PMUUz7ENXed0HbYWCNmlOQKJjnT8I0Lmv5Sgz5kJC0IkXIFRcQQUT0rNq3dSEI9whWWRuuZXtkLqN7CTRgaZgBCR8KIs/oUxDTDUyesV8hW9jOklJhsn9lEsKt16kUKi2sk9AW6SNuSiDCnpfbgKankJ8LBSsM8RTrfHGwRnb9WST/rHN3bPomiwmxWjbAIjF2J21OHg79DvWBQ/ICMMC/sSW5MASwJEyaxLvrdopKbIbeI3x527n1TG7ppGfDvHnC7zlXWLVtC8L17nQqKZyOsl5bvHHQNy7mUEf09WCiipERc8VysQlv2qhVP+EjAHqF9N82BgDWQVR/OiTOA8Wn/tzCFX4vzmonFKsluL4h8Z3NReviVFmO+MFvMVmQJGDRA918v8LEEJ7iUdAS8k3D1Xszw6TNhlKGrbf2ITJ2m8yCubxxv/k4fipPaAewveMm9wWPo0Hnahw4ghi/y6DkPZ0AbbU+pzYpIOVDLE8DV0hcBcWYGKTb5O0UkCjjQYlw6Ls7zdv5yPnccBeTY9AAXcDUxOv/hh19cwL4SQ9LVCRyquBA1jl43gavqqFXK/0RrInS4KWZGXxWQ4Ue7+R5XcZ8CtCEnoWAVeXBrSripA8CAwEAAQ==";
		private static final String PUBLIC_KEY_MOB_PRD = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0JBgCEWQw7rL8W+Zg2Jdipf2l8CA+xMIRyRu3H4kgQzcvBJTVRvRkhWbAA7w2gw7rC/iUUPuRSSPNLu06XOytILrJ+q7kJxG8CxTl4JSNYuFd8Za24wWEKWel0rvpZIcvIpGv569gOjCWB6lal5GZ1NwzvrL0q2c/2aksVzaaYjE+AhhNtnFlBzOETwMuRP/1mJM66BO78W3BTuYROOPjxGDKPUprwZ3ZAXNhiLgA5dQQsaexuXNWHYitLl2kEQAJsJKzz1A5rZsk2kbX5kmCFw4dvtf+9G6s9FJDLuPKxp6WftRvJgOPMpb6pidcgyra+RE81jkrWgzoTE7ORc1JKxu0a9hlEiuvFZUJPKjiwdXjd5PlThw9gdi1Oi2X6jQ1T95RT/gzogb9eaQk9br03OoJa2kbTO82BC/m4XQzagdUGRIcA8wIenrRYBhloMNNP4MZNH/C3/8Z1YRufJRWbMZUfMvwQB9dqmhUcEFuAD0lKsT2LTcZdveLU+dd1eDBAcEWtWsELYxKI4+o+SW9lxk1QatfEzboa8KZIco6FDhFgs4q45XHQ2rQ+mY3EAQG1/MIlItRYFLyGuk22W7ah3hcVuc3pkuo5137QxrHaOxZggHcpUpRT8lFbNWpAMJzbBwKP1mOMfTMsgMRXEQ+P7JmbpjDYEBJL37OoG/6X8CAwEAAQ==";
		private static final String PUBLIC_KEY_101_PRD = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAga87MDGKLEhQB/urrCGCE929rvwFmd1L0Y18c0C/3kSze29kyuw7VYpkMy//LbnZaIo2kTtjx2yPHhxrcsYmp+pt6ASkpDrNkZROiPmms42cseEqA8Di6TXzW32u1WMpwRLLlX7/GOTHBZAhnRfSzvc9YpM+dARipuvaVpQtjTcZVmFfdl77HApURLg3HnLPJ1touj7FSrRqi33OrFWfH0+dpreAAGJXmWfySuPrjNGuaanDiA8hqiMAsgTIzpkwJP+u16QTIr2rQDX+QdcInnHObu7E2Hfg+W5YfUNlQX5gkP5GrB5+gNaricfPnou09jkXuXN2EEm7Az9jFv7TDcXakv51cShxdYKU091MibVtmFChBrGdSMsMr7SfqDV2F+b4Zefgu0TMI0lKUJl+lgJM/Q2X/vQSvUgRbVDvC8fzmycLMoNQpaiZecX2yOBnr29sl+M+kMA8lPF+D/68BnkS47IQvdjaMfxFmxpe0/V2YZAMtf+hGHJQd49/LGM1AtPnqucP1IpRH+XspFZo7YAIJKnN/WAw4Adi/ibzW63VucDyxXIWQfa1phb2c8NJ5BIwEpNyDv+3ciogtUt/+XP/byPyFYFC2+OliUiXX2QF7P2xTB/yurvjzxeE8OxnB5tfYRStm1o8zcXlKVcMdwKaUf5UStSwm0lDu4l9ixcCAwEAAQ==";
		
		private static String API_KEY_LTS = API_KEY_LTS_PRD;
		private static String API_KEY_PCD = API_KEY_PCD_PRD;
		private static String API_KEY_TV  = API_KEY_TV_PRD;
		private static String API_KEY_MOB = API_KEY_MOB_PRD;
		private static String API_KEY_101 = API_KEY_101_PRD;
		
		private static String PUBLIC_KEY_LTS = PUBLIC_KEY_LTS_PRD;
		private static String PUBLIC_KEY_PCD = PUBLIC_KEY_PCD_PRD;
		private static String PUBLIC_KEY_TV  = PUBLIC_KEY_TV_PRD;
		private static String PUBLIC_KEY_MOB = PUBLIC_KEY_MOB_PRD;
		private static String PUBLIC_KEY_101 = PUBLIC_KEY_101_PRD;	
		
		private static String APP_ID_LTS = APP_ID_LTS_PRD;
		private static String APP_ID_PCD = APP_ID_PCD_PRD;
		private static String APP_ID_TV  = APP_ID_TV_PRD;
		private static String APP_ID_MOB = APP_ID_MOB_PRD;
		private static String APP_ID_101 = APP_ID_101_PRD;
		
		private static final String APP_ID = "5396499863";
		private static final String API_KEY = "fEH4mQMXP886QIF+WzPoOt8Cc/rBdYsb7Ch/7BMpkEWtDBbWOP2fIpgCQ6fERfA/YgyFSeTy5ISO4EjfoTdTYA==";
		private static final String PUBLIC_KEY = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAjVDIemwfx//QP81cpTu9EZI/CeG9GFy8a7w7oXgrbKj6OwN+l4b9sZ4+u/COzP9mgPdhLAU/20VOh8tig8wOLCqvrrWXWbkCplRQCIqVfOkoSvtRWUYOdVh3oObWhmw9P8tzMAIyKUnfbhqfDxI/h/4yenWN8ZOCBlQGP/AjEF1tm3H7gJnexaP2JjDokCW6MnvG8lZt7uWd+wkXiyk9VaJLC0fiP0anxT034ECg5gC8dkGQHk6RneKqrdDOAqjml7+fT29QS+544+2wRZPvvI5Hm+PElpycT70uibPKHEJrK50l1xCye/tpoe8JjV59uNQpPZZbQyB55ZQFtEYktHadPIwqxGN/0kOqYuiPZgKdMcO6XNvVpi7cbcIN3chAhi+XFPP4v++qfjXJqvmVwKT3dumqHPg0rRWtJz+BbggSB3zpNtSRrLMiZlc646nCFZS67vVFgU6oRFkmJYFI765HwBkYJhZ8a1esbxY/q9TjbxElcub7EkYOzJm1REhzHG8xnu/zevc2rZhpuzEVXBYfNWXlfyrQoK+GKlhwGyn8NSCFBKX43FXhTgYkrU/MUf0Ksog91BLpBQa4gmZk4vp46GJMrR88kTiZYc7OVS9/qIKou3/KbyYueTVK38O5yU+QvYK65ckOn1bgkmt9CpGgGzhQ5AqDj3FPGHyGeFUCAwEAAQ==";
		private static final String MER_TRADE_NO = "A3275001564360518160914152212";  
		private static final double totalPrice = 100;
		private static final String currency = "HKD";
		private static final String notifyUrl = "";   
		
		// Common Components
		private boolean 	debug = true;
		private String 	TAG = this.getClass().getName();
		public static TapNGoActivity	me;
		private 		AAQuery 		aq;
		private 		Boolean 		isZh;
		private final int 			colMaxNum = 3;
		private int 					deviceWidth = 0;
		private int					colWidth = 0;
		private int 					buttonPadding = 0;
		private int 					basePadding = 0;
		private int 					extralinespace = 0;
		private List<RegImageButton> 	regImageButtons;
		List<SubService>				serviceLists ;
		private int					txtColor;
		private int 					regInputHeight;
		private Boolean 				isFirstTime = true;
		private int 					btnWidth = 0;

		
		//payment info
		private int 	lob;
		private String srvNum;
		private String refNo;
		private String merchCode;
		private Double amount;
		private String acctNum;
		private String billType;
		private String billDate;
		private String appId;
		private String apiKey;
		private String publicKey;
		
		//Base Activity START
		//----------------------------------------------------------------------
		protected String moduleId;
		public String getModuleId() {
			return moduleId;
		}
		protected Boolean isLiveChatShown = true;

		//--------------------------------------------------------------------------
		//Base Activity END
		
		@Override
		public final void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);	
			
			TapNGoSdkSettings.setSandboxMode(ClnEnv.getPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain) != APIsManager.PRD_domain);
			
			ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
			isZh = !ClnEnv.getAppLocale(this).equals(Utils.getString(this, R.string.CONST_LOCALE_EN));
			setContentView(R.layout.activity_tapngo);
			initData();	
			
			Bundle bundle = this.getIntent().getExtras();
			if (bundle != null) {
				try {
//					refNo = bundle.getString("REFNO");
//					merchCode = bundle.getString("MERCHID");
					srvNum = bundle.getString("SRVNUM") != null ? bundle.getString("SRVNUM") : "";
					lob = bundle.getInt("LOB");
					amount = bundle.getDouble("AMOUNT"); //
					acctNum = bundle.getString("ACCTNUM");
					billType = bundle.getString("BILLTYPE");
					billDate = bundle.getString("BILLDATE");
					loadData(acctNum, amount);
				} catch (Exception e) {
					// fail to extract object
				}
			} else {
				loadData(acctNum, amount);
			}
			
		}
		
		@Override
		public final void onStart() {
			super.onStart();
			initNavBar();
			initUI();
		}
		
		@Override
		public final void onResume() {
			super.onResume();
			if (isTapnGoStarted) {
				isTapnGoStarted = false;
				onBackPressed();
			}
		}
		
		
		private void initData(){
			me = this;
			Display display = getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			deviceWidth = size.x;
			basePadding = (int) getResources().getDimension(R.dimen.basePadding); 
			extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);		
			buttonPadding =  (int) getResources().getDimension(R.dimen.reg_logo_padding);
			regInputHeight = (int) getResources().getDimension(R.dimen.reg_input_height);		
			colWidth =  (deviceWidth - basePadding * 4 + buttonPadding * 2) / colMaxNum;
			btnWidth = 	(deviceWidth - basePadding * 4)/2;
			txtColor = getResources().getColor(R.color.hkt_txtcolor_grey);
			//* init the regservice list
			serviceLists = new ArrayList<SubService>();
			serviceLists.add(new SubService(R.drawable.lob_pcd_plain, SubnRec.LOB_PCD));
			serviceLists.add(new SubService(R.drawable.lob_lts_plain, SubnRec.LOB_LTS));
			serviceLists.add(new SubService(R.drawable.lob_tv_plain, SubnRec.LOB_TV));
			serviceLists.add(new SubService(R.drawable.lob_1010_plain, SubnRec.WLOB_X101));
			serviceLists.add(new SubService(R.drawable.lob_csl_plain, SubnRec.WLOB_CSL));

			//For validation
			String mobPfx = ClnEnv.getPref(this, "mobPfx", "51,52,53,54,55,56,57,59,6,9,84,85,86,87,89");
			String ltsPfx = ClnEnv.getPref(this, "ltsPfx", "2,3,81,82,83");
			DirNum.getInstance(mobPfx, ltsPfx);
//			subnRec = new SubnRec();
		}
		
		protected final void initUI() {
			aq = new AAQuery(this);		

			aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
			if (lob > 0) {
				aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, 0 , Utils.getLobIcon(lob, 0) , 0, srvNum);
			} else {
				aq.navBarTitle(R.id.navbar_title, srvNum);	
			}
			aq.id(R.id.navbar_button_left).clicked(this, "onClick");

			aq.normTxtBtn(R.id.tapngo_btn_next, getResources().getString(R.string.btn_continue), btnWidth);
			aq.id(R.id.tapngo_btn_next).clicked(this, "onClick");
			aq.id(R.id.tapngo_btn_next).visibility(View.VISIBLE);

		}
		
		private final void loadData(String mAcctNum, double mAmount) {
			aq = new AAQuery(this);	
			String acctnum = "";
			String amount = "";
			if (mAcctNum != null || mAcctNum.trim().length() != 0) {
				acctnum = Tool.formatAcctNum(mAcctNum);
			}
			amount = Utils.convertStringToPrice(Utils.convertDoubleToString(mAmount,2));
			aq.id(R.id.tapngo_acctnum).text(acctnum);
			aq.id(R.id.tapngo_amount).text(amount);
		}
		
		private final void prepareTapNGoPayInfo() {
			//Ref No. / Trade No.
			SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			String datetime = sdf.format(new Date());
			String billtype = "";
			if (lob == R.string.CONST_LOB_MOB || lob == R.string.CONST_LOB_IOI) {
				billtype = getResources().getString(R.string.qrcode_bill_type_mobile);
			} else if (lob == R.string.CONST_LOB_PCD || lob == R.string.CONST_LOB_TV) {
				billtype = getResources().getString(R.string.qrcode_bill_type_pcd);
			} else if (lob == R.string.CONST_LOB_O2F || lob == R.string.CONST_LOB_1010) {
				billtype = getResources().getString(R.string.qrcode_billtype_1010);
			} else if (lob == R.string.CONST_LOB_LTS) {
				billtype = getResources().getString(R.string.qrcode_bill_type_lts);
			}
//			
			boolean isPRD = ClnEnv.getPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain) == APIsManager.PRD_domain;
			API_KEY_LTS = isPRD ? API_KEY_LTS_PRD : API_KEY_LTS_UAT;
			API_KEY_PCD = isPRD ? API_KEY_PCD_PRD : API_KEY_PCD_UAT;
			API_KEY_TV  = isPRD ? API_KEY_TV_PRD : API_KEY_TV_UAT;
			API_KEY_MOB = isPRD ? API_KEY_MOB_PRD : API_KEY_MOB_UAT;
			API_KEY_101 = isPRD ? API_KEY_101_PRD : API_KEY_101_UAT;
			
			PUBLIC_KEY_LTS = isPRD ? PUBLIC_KEY_LTS_PRD : PUBLIC_KEY_LTS_UAT;
			PUBLIC_KEY_PCD = isPRD ? PUBLIC_KEY_PCD_PRD : PUBLIC_KEY_PCD_UAT;
			PUBLIC_KEY_TV  = isPRD ? PUBLIC_KEY_TV_PRD : PUBLIC_KEY_TV_UAT;
			PUBLIC_KEY_MOB = isPRD ? PUBLIC_KEY_MOB_PRD : PUBLIC_KEY_MOB_UAT;
			PUBLIC_KEY_101 = isPRD ? PUBLIC_KEY_101_PRD : PUBLIC_KEY_101_UAT;
			
			APP_ID_LTS = isPRD ? APP_ID_LTS_PRD : APP_ID_LTS_UAT;
			APP_ID_PCD = isPRD ? APP_ID_PCD_PRD : APP_ID_PCD_UAT;
			APP_ID_TV  = isPRD ? APP_ID_TV_PRD 	: APP_ID_TV_UAT; 
			APP_ID_MOB = isPRD ? APP_ID_MOB_PRD : APP_ID_MOB_UAT;
			APP_ID_101 = isPRD ? APP_ID_101_PRD : APP_ID_101_UAT; 
			
			refNo = "A" + billType + Tool.formatAcctNum14(acctNum) + datetime;
//			Toast.makeText(me, refNo, 5000).show();

			//App ID
			if (lob == R.string.CONST_LOB_MOB || lob == R.string.CONST_LOB_O2F) {
				appId = APP_ID_MOB;
				apiKey = API_KEY_MOB;
				publicKey = PUBLIC_KEY_MOB;
			} else if (lob == R.string.CONST_LOB_PCD) {
				appId = APP_ID_PCD;
				apiKey = API_KEY_PCD;
				publicKey = PUBLIC_KEY_PCD;
			} else if (lob == R.string.CONST_LOB_TV) {
				appId = APP_ID_TV;
				apiKey = API_KEY_TV;
				publicKey = PUBLIC_KEY_TV;
			} else if (lob == R.string.CONST_LOB_IOI || lob == R.string.CONST_LOB_1010) {
				appId = APP_ID_101;
				apiKey = API_KEY_101;
				publicKey = PUBLIC_KEY_101;
			} else if (lob == R.string.CONST_LOB_LTS) {
				appId = APP_ID_LTS;
				apiKey = API_KEY_LTS;
				publicKey = PUBLIC_KEY_LTS;
			}
		}
		
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.tapngo_btn_next:
				//check if app installed\
				final Intent i;
				i = getPackageManager().getLaunchIntentForPackage("com.hktpayment.tapngo");
				String message = i != null ? Utils.getString(me, R.string.TAPNGO_TRANSITION_MSG) : Utils.getString(me, R.string.TAPNGO_DOWNLOAD_MSG);
				DialogHelper.createSimpleDialog(me, message, 
						Utils.getString(me, R.string.btn_ok), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						//check if TheClub app exist
						if (i != null) {
//							//make payment with app redirect
							//prepare reference no and appid
							prepareTapNGoPayInfo();
							
							//doPayment()
							doSinglePayment(refNo, amount, "", "");
							isTapnGoStarted = true;
//							private void doSinglePayment(String tradeNo, double price, String remark, String url) {     
						} else {
							//to market download page
							try {
								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//								intent.setData(Uri.parse("http://www.tapngo.com.hk/download/"));
								intent.setData(Uri.parse("market://details?id="+ "com.hktpayment.tapngo"));
								startActivity(intent);					

							} catch (ActivityNotFoundException e) {

								Intent intent = new Intent(Intent.ACTION_VIEW);
								intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//								intent.setData(Uri.parse("http://www.tapngo.com.hk/download/"));
								intent.setData(Uri.parse("http://play.google.com/store/apps/details?id="+"com.hktpayment.tapngo"));
								startActivity(intent);
							}					
						}
					}
				},Utils.getString(this, R.string.btn_cancel));
				break;
			case R.id.navbar_button_left:
				onBackPressed();
				break;
//			case R.id.regbasic_lang_spin:
//				mQuickAction.show(v, aq.id(R.id.regbasic_lang_spin).getView());
//				mQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
//				break;
			}
		}

		
		// Android Device Back Button Handling
		public final void onBackPressed() {
			finish();
		}

		private void doSinglePayment(String tradeNo, double price, String remark, String url) {
//			TapNGoPayment payment = new TapNGoPayment(APP_ID, API_KEY, PUBLIC_KEY);
//			payment.setSinglePayment(MER_TRADE_NO, totalPrice, currency, remark, notifyUrl);
			TapNGoPayment payment = new TapNGoPayment(appId, apiKey, publicKey);
			payment.setSinglePayment(tradeNo, Utils.convertDoubleToString(price, 2), currency, remark, url);
			doPayment(payment);
		}

//		private void doRecurrentPayment() {
//			TapNGoPayment payment = new TapNGoPayment(APP_ID, API_KEY, PUBLIC_KEY);
//			payment.setRecurrentPayment(MER_TRADE_NO, currency, remark);
//			doPayment(payment); 
//		}  
//		
//		private void doSingleRecurrentPayment() {
//		    TapNGoPayment payment = new TapNGoPayment(APP_ID, API_KEY, PUBLIC_KEY);
//		    payment.setSingleAndRecurrentPayment(MER_TRADE_NO, totalPrice, currency, remark, notifyUrl);
//		    doPayment(payment);
//		}
		


		@Override
		protected void onPaymentFail(TapNGoPayResult arg0) {
			Log.e("", "on payment fail");
			Log.e("", arg0.getMessage());
		}

		@Override
		protected void onPaymentSuccess(TapNGoPayResult arg0) {
			// TODO Auto-generated method stub
			Log.e("", "on payment success");
			Log.e("", arg0.getMessage());
		}

		//--------------------------------------------------------------------------------------
		//BaseActivity Settings   START
		//--------------------------------------------------------------------------------------
		public final void initNavBar() {
			aq = new AAQuery(this);
			aq.navBarBaseLayout(R.id.navbar_base_layout);
			LiveChatHelper.getInstance(me,me);
			if (LiveChatHelper.isPause || isLiveChatShown ) {
				aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
			} else {
				aq.id(R.id.navbar_button_right).visibility(View.INVISIBLE);	
			}

			aq.id(R.id.navbar_button_right).clicked(new View.OnClickListener(){

				@Override
				public void onClick(View v) {
					//login required
					if (LiveChatHelper.isPause) {
						openLiveChat();
					} else if (moduleId == getResources().getString(R.string.MODULE_CONTACT_US) && !ClnEnv.isLoggedIn()) {
						loginFirstDialog(MAINMENU.CONTACTUS);
					} else if (moduleId == getResources().getString(R.string.MODULE_LTS_IDD) && !ClnEnv.isLoggedIn()) {
						loginFirstDialog(MAINMENU.IDD);
					} else if (moduleId == getResources().getString(R.string.MODULE_MM_MAIN) && !ClnEnv.isLoggedIn()) {
						loginFirstDialog(MAINMENU.MYMOB);
					} else if (moduleId == getResources().getString(R.string.MODULE_SETTING) && !ClnEnv.isLoggedIn()) {
						loginFirstDialog(MAINMENU.SETTING);
					} else {
						openLiveChat();
					}					
				}			
			});
		}
		
		public void openLiveChat(){
			// Event Tracker
			
			//open live chat
			if (!"Y".equalsIgnoreCase(ClnEnv.getChatIsMaintenance())) {
				LiveChatHelper.getInstance(me, me).showWebView(me, moduleId);
			} else {
				DialogHelper.createSimpleDialog(me, getString(R.string.LIVECHAT_UNAVAILABLE));
			}
		}
		
		public void setLiveChangeIcon(Boolean isPause){
			TapNGoPaymentActivity a ;
			
			if (isPause) {
				if (aq != null) {				
					aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
					aq.navBarButton(R.id.navbar_button_right,R.drawable.chat);
					ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#DB9200"), PorterDuff.Mode.MULTIPLY);				
					aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
				}			
			} else {
				if (aq != null) {
					//				aq.navBarButton(R.id.navbar_button_right,R.drawable.livechat_small);
					//				aq.id(R.id.navbar_button_right).getImageView().setColorFilter(null);
					if (isLiveChatShown) {
						aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
					} else {
						aq.id(R.id.navbar_button_right).visibility(View.INVISIBLE);	
					}
					aq.navBarButton(R.id.navbar_button_right,R.drawable.chat);
					aq.id(R.id.navbar_button_right).getImageView().clearColorFilter();
					//Premier: white , Non-Premier : blue
					//Prtimrt : 
					if (!ClnEnv.getSessionPremierFlag()) {
						ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#217EC0"), PorterDuff.Mode.MULTIPLY);
						aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
					} else {
						ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
						aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
					}
				}
			}
		}
		
		// Login First Dialog
		protected final void loginFirstDialog(final MAINMENU clickItem) {
			OnClickListener onClickListener =  new OnClickListener() {
				Bundle	rbundle;
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent;
					dialog.dismiss();
					intent = new Intent(getApplicationContext(), LoginActivity.class);
					rbundle = new Bundle();
					rbundle.putSerializable("CLICKBUTTON", clickItem);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					intent.putExtras(rbundle);
					finish();
					startActivity(intent);
					overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
				}
			};
			DialogHelper.createSimpleDialog(this, 
					Utils.getString(this, R.string.myhkt_plslogin), 
					Utils.getString(this, R.string.btn_ok), 
					onClickListener); 
		}
		//--------------------------------------------------------------------------------------
		//BaseActivity Settings    END
		//--------------------------------------------------------------------------------------
	}

