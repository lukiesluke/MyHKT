package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRButton;
import com.pccw.myhkt.lib.ui.LRButton.OnLRButtonClickListener;
import com.pccw.myhkt.lib.ui.RegImageButton;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.model.SubService;
import com.pccw.myhkt.utils.FirebaseSetting;
import com.pccw.wheat.shared.tool.Reply;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/************************************************************************
 * File : RegServiceActivity.java 
 * Desc : Reg Menu 
 * Name : RegServiceActivity
 * by : Andy Wong 
 * Date : 30/10/2015
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 30/10/2015 Andy Wong 		-First draft
 *************************************************************************/
public class RegBasicActivity extends BaseActivity implements OnLRButtonClickListener {
    // Common Components
    private boolean debug = false;
    private String TAG = this.getClass().getName();
    private AAQuery aq;
    private final int colMaxNum = 3;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int buttonPadding = 0;
    private int basePadding = 0;
    private int extralinespace = 0;
    private List<RegImageButton> regImageButtons;
    List<SubService> serviceLists;
    private int txtColor;
    private int regInputHeight;
    private Boolean isFirstTime = true;
    private int btnWidth = 0;
    private LRButton lrButton;
    private LinearLayout infoContainer;
    private LinearLayout serviceContainer;
    private RegInputItem regInputHKBR;
    private RegInputItem regInputAccNum;
    private RegInputItem regInputLoginId;
    private RegInputItem regInputPw;
    private RegInputItem regInputRePw;
    private RegInputItem regInputNickname;
    private RegInputItem regInputMobileReset;
    private Boolean isZh;
    //Save data
    private int regServicePos = -1;
    private String regHkbr = "";
    private String regAccNum = "";
    private String regLoginId = "";
    private String regPw = "";
    private String regRePw = "";
    private String regNickname = "";
    private String regMobileReset = "";
    private String subscriptionTOS = "";
    private SubnRec subnRec = new SubnRec();
    private CustRec custRec = new CustRec();
    private SveeRec sveeRec = new SveeRec();
    public final static String BUNDLE_CUSTREC = "BUNDLE_CUSTREC";
    public final static String BUNDLE_SVEEREC = "BUNDLE_SVEEREC";
    public final static String BUNDLE_SUBNREC = "BUNDLE_SUBNREC";
    public final static String BUNDLE_SERVICERES = "BUNDLE_SERVICERES";
    public final static String BUNDLE_COMPANY_INFO_REC = "BUNDLE_COMPANY_INFO_REC";
    private QuickAction mQuickAction;
    private static final String SCREEN_NAME = "Registration Basic Screen";

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        isZh = !ClnEnv.getAppLocale(this).equals(Utils.getString(this, R.string.CONST_LOCALE_EN));
        setContentView(R.layout.activity_regbasic);
        initData();
        if (savedInstanceState != null) {
            regHkbr = savedInstanceState.getString("REGHKBR");
            regAccNum = savedInstanceState.getString("REGMOBILE");
            regLoginId = savedInstanceState.getString("REGLOGINID");
            regPw = savedInstanceState.getString("REGPW");
            regRePw = savedInstanceState.getString("REGREPW");
            regNickname = savedInstanceState.getString("REGNICKNAME");
            regMobileReset = savedInstanceState.getString("REGMOBILERESET");
        }
    }

    private void initData() {
        me = this;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        regInputHeight = (int) getResources().getDimension(R.dimen.reg_input_height);
        colWidth = (deviceWidth - basePadding * 4 + buttonPadding * 2) / colMaxNum;
        btnWidth = (deviceWidth - basePadding * 4) / 2;
        txtColor = getResources().getColor(R.color.hkt_txtcolor_grey);
        //* init the regservice list
        serviceLists = new ArrayList<SubService>();
        serviceLists.add(new SubService(R.drawable.lob_lts_tel, SubnRec.LOB_LTS));
        serviceLists.add(new SubService(R.drawable.lob_ims, SubnRec.LOB_IMS));
        serviceLists.add(new SubService(R.drawable.lob_pcd, SubnRec.LOB_PCD));
        serviceLists.add(new SubService(R.drawable.lob_lts_ocm, SubnRec.LOB_LTS));
        serviceLists.add(new SubService(R.drawable.lob_tv_plain, SubnRec.LOB_TV));

        //For validation
        String mobPfx = ClnEnv.getPref(this, "mobPfx", "51,52,53,54,55,56,57,59,6,9,84,85,86,87,89");
        String ltsPfx = ClnEnv.getPref(this, "ltsPfx", "2,3,81,82,83");
        DirNum.getInstance(mobPfx, ltsPfx);
        subnRec = new SubnRec();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onResume() {
        super.onResume();
        moduleId = getResources().getString(R.string.MODULE_REG);

        // Update Locale
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        firebaseSetting.onScreenView(RegBasicActivity.this, SCREEN_NAME);
    }

    private void refreshView() {
        aq.navBarTitle(R.id.navbar_title, getResString(R.string.myhkt_reg_title));
        aq.id(R.id.regservice_textview).text(getResString(R.string.REGF_HEADER1));
        aq.id(R.id.regservice_textview1).text(getResString(R.string.REGF_HEADER2));
        //regInputHKID.setTexts("", getResString(R.string.REGF_FB_DOC_NUM));
        setAccNum(serviceLists.get(regServicePos).getLob(), true);
        lrButton.setTexts(getResString(R.string.REGF_HKID_NO), getResString(R.string.REGF_PASSPORT_NO));
        aq.id(R.id.regbasic_txt).text(getResString(R.string.REGF_HEADER3));
        aq.id(R.id.regbasic_txt1).text(getResString(R.string.REGF_HEADER4));
        regInputLoginId.setTexts(getResString(R.string.REGF_EMAIL_TITLE), getResString(R.string.myhkt_myprof_email_hint));
        regInputPw.setTexts(getResString(R.string.REGF_PWD), getResString(R.string.REGF_PWD_HINT));
        regInputRePw.setTexts(getResString(R.string.REGF_CFM_PWD), "");
        regInputNickname.setTexts(getResString(R.string.REGF_NICK_NAME), getResString(R.string.REGF_NICK_NAME));
        regInputMobileReset.setTexts(getResString(R.string.REGF_CT_MOB), getResString(R.string.myhkt_myprof_mobnum_hint));
        aq.normTextGrey(R.id.regbasic_lang_txt, getResString(R.string.PAMF_LANG));
        aq.id(R.id.regservice_btn_next).text(getResString(R.string.REGF_NEXT));
        setPopover();

        //Commercial
        regInputHKBR.setTexts("", getResString(R.string.comm_reg_hkbr_ph));

        // Dynamic Generate Service Menu
        refreshServiceMenu();
    }

    protected void refreshServiceMenu() {
        serviceContainer = (LinearLayout) aq.id(R.id.regacc_service_container).getView();
        serviceContainer.setGravity(Gravity.CENTER);
        serviceContainer.removeAllViews();
        serviceContainer.setPadding(0, 0, 0, extralinespace * 2);
        regImageButtons = new ArrayList<RegImageButton>();

        int buttonPadding1 = (int) getResources().getDimension(R.dimen.reg_logo_padding_1);

        // Dynamic Generate Service Menu
        LinearLayout rowLayout = new LinearLayout(this);
        for (int itemCount = 0; itemCount < serviceLists.size(); itemCount++) {
            if (itemCount % colMaxNum == 0) {
                rowLayout = new LinearLayout(this);
                LayoutParams param = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                rowLayout.setGravity(Gravity.CENTER);
                rowLayout.setLayoutParams(param);
                rowLayout.setPadding(basePadding - buttonPadding, 0, basePadding - buttonPadding, 0);
                serviceContainer.addView(rowLayout);
            }
            RegImageButton regImageBtn = new RegImageButton(this);
            regImageBtn.setButtonPadding(buttonPadding1);
            regImageBtn.initViews(this, serviceLists.get(itemCount).getImageRes(), "", 0, 0, colWidth - buttonPadding * 2);
            rowLayout.addView(regImageBtn);
            LinearLayout.LayoutParams buttonParam = (LinearLayout.LayoutParams) regImageBtn.getLayoutParams();
            buttonParam.setMargins(buttonPadding, buttonPadding, buttonPadding, buttonPadding);
            regImageBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (RegImageButton regImageBtn : regImageButtons) {
                        regImageBtn.setSelect(regImageBtn.equals(v));
                        if (regImageBtn.equals(v)) {
                            regServicePos = regImageButtons.indexOf(regImageBtn);
                        }
                    }
                    subscriptionTOS = "";
                    if (regServicePos == 3) {
                        subscriptionTOS = SubnRec.TOS_LTS_OCM;
                    }
                    setAccNum(serviceLists.get(regServicePos).getLob(), true);
                    aq.id(R.id.regservice_btn_next).visibility(regServicePos < 0 ? View.GONE : View.VISIBLE);
                    infoContainer.setVisibility(View.VISIBLE);
                    if (infoContainer.getVisibility() != View.VISIBLE) {
                        infoContainer.setVisibility(View.VISIBLE);
                    }
                }
            });
            regImageButtons.add(regImageBtn);
        }

        if (regImageButtons != null && regServicePos >= 0) {
            regImageButtons.get(regServicePos).setSelect(true);
            //			setAccNum(serviceLists.get(regServicePos).getLob(), false);
        }
    }

    @Override
    protected final void initUI2() {
        aq = new AAQuery(this);

        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_reg_title));
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        //Header
        aq.id(R.id.regservice_textview).width(LayoutParams.MATCH_PARENT).textSize(12).textColor(txtColor).text(getResString(R.string.REGF_HEADER1));
        aq.id(R.id.regservice_textview).getView().setPadding(basePadding, extralinespace, basePadding, 0);

        aq.id(R.id.regservice_textview1).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(txtColor).text(getResString(R.string.REGF_HEADER2));
        aq.id(R.id.regservice_textview1).getView().setPadding(basePadding, 0, basePadding, 0);

        // Dynamic Generate Service Menu
        refreshServiceMenu();

        infoContainer = (LinearLayout) aq.id(R.id.regbasic_container).getView();
        aq.id(R.id.regbasic_container).visibility(regServicePos < 0 ? View.GONE : View.VISIBLE);

        aq.line(R.id.regbasic_line);

        //SubHeader
        TextView textView = aq.id(R.id.regbasic_txt).getTextView();
        aq.id(R.id.regbasic_txt).text(getResString(R.string.REGF_HEADER3)).textColor(txtColor);
        textView.setPadding(basePadding, extralinespace, basePadding, extralinespace);

        //ID Left Right Button
        lrButton = (LRButton) aq.id(R.id.regbasic_lrBtn).getView();
        lrButton.initViews(this, getResString(R.string.REGF_HKID_NO), getResString(R.string.REGF_PASSPORT_NO));
        lrButton.setPadding(basePadding, 0, basePadding, extralinespace);

        //HKBR
        regInputHKBR = aq.regInputItem(R.id.regbasic_input_hkbr, getResString(R.string.comm_reg_hkbr), getResString(R.string.comm_reg_hkbr_ph), regHkbr);

        //HKID input
//		regInputHKID = aq.regInputItem(R.id.regbasic_input_hkid, "", getResString(R.string.REGF_FB_DOC_NUM), regHkid, 
//				this.getResources().getInteger(R.integer.CONST_MAX_IDDOC));
        //Acc num input
        regInputAccNum = aq.regInputItem(R.id.regbasic_input_accnum, getResString(R.string.REGF_CT_MOB), "", regAccNum,
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));

        TextView textView1 = aq.id(R.id.regbasic_txt1).getTextView();
        aq.id(R.id.regbasic_txt1).text(getResString(R.string.REGF_HEADER4)).textColor(txtColor);
        textView1.setPadding(basePadding, extralinespace, basePadding, extralinespace);
        //		textView1.setVisibility(View.GONE);

        //Login ID
        regInputLoginId = aq.regInputItem(R.id.regbasic_input_loginid, getResString(R.string.REGF_EMAIL_TITLE), getResString(R.string.myhkt_myprof_email_hint), regLoginId,
                InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS, getResources().getInteger(R.integer.CONST_MAX_LOGINID));
        //Password
        regInputPw = aq.regInputItem(R.id.regbasic_input_pw, getResString(R.string.REGF_PWD), getResString(R.string.REGF_PWD_HINT), regPw,
                InputType.TYPE_TEXT_VARIATION_PASSWORD, getResources().getInteger(R.integer.CONST_MAX_PASSWORD));
        //Re-type pw
        regInputRePw = aq.regInputItem(R.id.regbasic_input_repw, getResString(R.string.REGF_CFM_PWD), "", regRePw,
                InputType.TYPE_TEXT_VARIATION_PASSWORD, getResources().getInteger(R.integer.CONST_MAX_PASSWORD));
        //Nickname
        regInputNickname = aq.regInputItem(R.id.regbasic_input_nickname, getResString(R.string.REGF_NICK_NAME), getResString(R.string.REGF_NICK_NAME_HINT), regNickname,
                getResources().getInteger(R.integer.CONST_MAX_NICKNAME));
        //Contact Mob
        regInputMobileReset = aq.regInputItem(R.id.regbasic_input_mobile_reset, getResString(R.string.REGF_CT_MOB), getResString(R.string.myhkt_myprof_mobnum_hint), regMobileReset,
                InputType.TYPE_CLASS_PHONE, getResources().getInteger(R.integer.CONST_MAX_MOBNUM));

        //Lang
        aq.id(R.id.regbasic_lang_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        aq.normTextGrey(R.id.regbasic_lang_txt, getResString(R.string.PAMF_LANG));
        aq.layoutWeight(R.id.regbasic_lang_txt, 1);
        TextView textView2 = aq.id(R.id.regbasic_lang_txt).getTextView();
        textView2.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        //		aq.spinText(R.id.regbasic_lang_spin, Utils.getString(this, R.string.myhkt_lang).equalsIgnoreCase("en")? Utils.getString(this, R.string.btn_eng) : Utils.getString(this, R.string.btn_chi));
        aq.layoutWeight(R.id.regbasic_lang_spin, 1);

        setPopover();

        aq.normTxtBtn(R.id.regservice_btn_next, getResString(R.string.REGF_NEXT), btnWidth);
        aq.id(R.id.regservice_btn_next).clicked(this, "onClick");
        aq.id(R.id.regservice_btn_next).visibility(regServicePos < 0 ? View.GONE : View.VISIBLE);

        if (regImageButtons != null && regServicePos >= 0) {
            regImageButtons.get(regServicePos).setSelect(true);
            setAccNum(serviceLists.get(regServicePos).getLob(), false);
        }

        //		APIsManager.doHelo(this);
    }

    public void setPopover() {
        ActionItem[] actionItem = new ActionItem[2];
        actionItem[0] = new ActionItem(0, "中文");
        actionItem[1] = new ActionItem(1, "ENGLISH");

        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        int screenWidth = wm.getDefaultDisplay().getWidth();
        mQuickAction = new QuickAction(this, screenWidth / 2, 0);
        for (int i = 0; i < actionItem.length; i++) {
            mQuickAction.addActionItem(actionItem[i]);
        }

        //setup the action item click listener
        mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int position, int actionId) {
                if (position == 1) {
                    isZh = false;
                    ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LOCALE), getString(R.string.CONST_LOCALE_EN));
                    ClnEnv.updateUILocale(me, getString(R.string.CONST_LOCALE_EN));
                }

                if (position == 0) {
                    isZh = true;
                    ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LOCALE), getString(R.string.CONST_LOCALE_ZH));
                    ClnEnv.updateUILocale(me, getResources().getString(R.string.CONST_LOCALE_ZH));
                }

                aq.id(R.id.regbasic_lang_spin).text(quickAction.getActionItem(position).getTitle());
                refreshView();
            }
        });

        aq.popOverInputView(R.id.regbasic_lang_spin, actionItem[isZh ? 0 : 1].getTitle());
        aq.id(R.id.regbasic_lang_spin).clicked(this, "onClick");

    }

    //set acc num according lob
    public void setAccNum(String lob, boolean clearExisting) {
        if (clearExisting)
            regInputAccNum.setEditText("");
        regInputAccNum.setKeyListener(DigitsKeyListener.getInstance());
        if (lob.equals(SubnRec.LOB_PCD)) {
            regInputAccNum.setMaxLength(getResources().getInteger(R.integer.CONST_MAX_PCDID));
            regInputAccNum.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            regInputAccNum.setTexts(Utils.getString(me, R.string.REGM_ASO_SVN_PCD), Utils.getString(me, R.string.comm_reg_srv_ims_ph));
            //			regInputAccNum.setKeyListener(DigitsKeyListener.getInstance("abcdefghijklmnopqrstuvwxyz1234567890 "));
        }
        if (lob.equals(SubnRec.LOB_LTS)) {
            regInputAccNum.setMaxLength(getResources().getInteger(R.integer.CONST_MAX_PHONENUM));
            regInputAccNum.setInputType(InputType.TYPE_CLASS_PHONE);
            regInputAccNum.setText(Utils.getString(me, R.string.REGM_ASO_SVN_LTS));
            regInputAccNum.setTexts(getString(R.string.comm_cominfo_phone), getString(R.string.comm_reg_tel_num_ph));
        }
        if (lob.equals(SubnRec.LOB_TV)) {
            regInputAccNum.setMaxLength(getResources().getInteger(R.integer.CONST_MAX_PCDTVACCNUM));
            regInputAccNum.setInputType(InputType.TYPE_CLASS_PHONE);
            regInputAccNum.setTexts(Utils.getString(me, R.string.REGM_ASO_SVN_TV), Utils.getString(me, R.string.comm_reg_srv_ims_ph));
        }
        if (lob.equals(SubnRec.WLOB_X101)) {
            regInputAccNum.setMaxLength(getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
            regInputAccNum.setInputType(InputType.TYPE_CLASS_PHONE);
            regInputAccNum.setText(Utils.getString(me, R.string.REGF_CT_MOB));
        }
        if (lob.equals(SubnRec.WLOB_CSL)) {
            regInputAccNum.setMaxLength(getResources().getInteger(R.integer.CONST_MAX_MOBNUM));
            regInputAccNum.setInputType(InputType.TYPE_CLASS_PHONE);
            regInputAccNum.setText(Utils.getString(me, R.string.REGF_CT_MOB));
        }
        if (lob.equals(SubnRec.LOB_IMS)) {
            regInputAccNum.setMaxLength(getResources().getInteger(R.integer.CONST_MAX_PCDTVACCNUM));
            regInputAccNum.setInputType(InputType.TYPE_CLASS_PHONE);
            regInputAccNum.setTexts(Utils.getString(me, R.string.comm_reg_srv_ims), Utils.getString(me, R.string.comm_reg_srv_ims_ph));
        }

    }

    public void gridviewitemClicked(AdapterView<?> parent, View v, int position, long id) {
        for (int i = 0; i < 6; i++) {
            RegImageButton regImageButton = (RegImageButton) parent.getChildAt(i);
            regImageButton.setSelect(position == i);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regservice_btn_next:
                checkInput();
                break;
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.regbasic_lang_spin:
                mQuickAction.show(v, aq.id(R.id.regbasic_lang_spin).getView());
                mQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
        }
    }


    private void checkInput() {
        //TODO
        Utils.closeSoftKeyboard(this, (View) serviceContainer.getParent());
//		if (regServicePos < 0) {
//			DialogHelper.createSimpleDialog(this, "Please choose one subscribed service");
//			return;
//		}

        String lob = serviceLists.get(regServicePos).getLob();
//		regHkid = regInputHKID.getInput();
        regHkbr = regInputHKBR.getInput();
        regAccNum = regInputAccNum.getInput();
        regLoginId = regInputLoginId.getInput();
        regPw = regInputPw.getInput();
        regRePw = regInputRePw.getInput();
        regNickname = regInputNickname.getInput();
        regMobileReset = regInputMobileReset.getInput();

        sveeRec = new SveeRec();
        sveeRec.loginId = regLoginId;
        sveeRec.pwd = regPw;
        //TODO
        sveeRec.ctMail = regLoginId;
        sveeRec.ctMob = regMobileReset;
        sveeRec.nickname = regNickname;
        sveeRec.mobAlrt = "Y";
        sveeRec.lang = Utils.getString(me, R.string.myhkt_lang).equalsIgnoreCase("en") ? SveeRec.LG_EN : SveeRec.LG_ZH;

        subnRec.lob = lob;
        custRec = new CustRec();
        custRec.docTy = CustRec.TY_HKBR;//lrButton.isLeftClick() ? CustRec.TY_HKID : CustRec.TY_PASSPORT;
//		custRec.docNum = regHkid;
        custRec.docNum = regHkbr;
        custRec.docNum = custRec.docNum.trim().toUpperCase(Locale.US);
        if (subnRec.lob.equals(SubnRec.LOB_TV) || subnRec.lob.equals(SubnRec.LOB_IMS)) {
            /* For TV, make sure srv_num is empty and acct_num has value*/
            subnRec.acctNum = regAccNum;
            subnRec.srvNum = "";
        } else {
            subnRec.acctNum = "";
            subnRec.srvNum = regAccNum;
        }
        subnRec.tos = "";
        if (subnRec.lob == SubnRec.LOB_LTS && !TextUtils.isEmpty(subscriptionTOS)) {
            subnRec.tos = subscriptionTOS;
        }

        //Start verify
        Reply reply = new Reply();
        //HKID, doc type

        reply = custRec.verifyBaseInput();
        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_RegnMdu(me, reply, subnRec, custRec));
            aq.id(R.id.regservice_scrollView).getView().scrollTo(0, (int) lrButton.getY() + ((View) lrButton.getParent()).getTop());
            return;
        }
        // service type , mobile
        reply = subnRec.verify4Registration();
        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_RegnMdu(me, reply, subnRec, custRec));
            aq.id(R.id.regservice_scrollView).getView().scrollTo(0, (int) regInputAccNum.getY() + ((View) regInputAccNum.getParent()).getTop());
            return;
        }
        reply = sveeRec.verifyLoginId();
        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_RegnMdu(me, reply, subnRec, custRec));
            aq.id(R.id.regservice_scrollView).getView().scrollTo(0, (int) regInputLoginId.getY() + ((View) regInputLoginId.getParent()).getTop());
            return;
        }
        reply = sveeRec.verifyPwd();
        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_RegnMdu(me, reply, subnRec, custRec));
            aq.id(R.id.regservice_scrollView).getView().scrollTo(0, (int) regInputPw.getY() + ((View) regInputPw.getParent()).getTop());
            return;
        }
        if (!regPw.equals(regRePw)) {
            DialogHelper.createSimpleDialog(this, Utils.getString(this, R.string.REGM_IVCFMPWD));
            aq.id(R.id.regservice_scrollView).getView().scrollTo(0, (int) regInputRePw.getY() + ((View) regInputRePw.getParent()).getTop());
            return;
        }

        reply = sveeRec.verifyNickName(false);
        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_RegnMdu(me, reply, subnRec, custRec));
            aq.id(R.id.regservice_scrollView).getView().scrollTo(0, (int) regInputNickname.getY() + ((View) regInputNickname.getParent()).getTop());
            return;
        }

        reply = sveeRec.verifyCtMob(false);
        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_RegnMdu(me, reply, subnRec, custRec));
            aq.id(R.id.regservice_scrollView).getView().scrollTo(0, (int) regInputMobileReset.getY() + ((View) regInputMobileReset.getParent()).getTop());
            return;
        }

        reply = sveeRec.verifyBaseInput();
        if (!reply.isSucc()) {
            DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_RegnMdu(me, reply, subnRec, custRec));
            return;
        }

        DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
            Bundle rbundle;

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                Bundle bundle = new Bundle();
                bundle.putSerializable(BUNDLE_SVEEREC, sveeRec);
                bundle.putSerializable(BUNDLE_CUSTREC, custRec);
                bundle.putSerializable(BUNDLE_SUBNREC, subnRec);
                //intent = new Intent(getApplicationContext(), RegConfirmActivity.class);
                intent = new Intent(getApplicationContext(), RegCompanyInfoActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        };

        AlertDialog.Builder builder = new Builder(this);
        final TextView message = new TextView(this);
        message.setPadding(basePadding, basePadding, basePadding, basePadding);
        message.setText(Html.fromHtml(Utils.getString(this, R.string.MYHKT_PICS)));
        message.setAutoLinkMask(Linkify.ALL);
        //		  message.setAutoLinkMask(Linkify.ALL);
        message.setMovementMethod(LinkMovementMethod.getInstance());
        //		  message.setMovementMethod(LinkMovementMethod.getInstance());
        //		builder.setMessage(Html.fromHtml(Utils.getString(this, R.string.MYHKT_PICS)));
        builder.setView(message);
        builder.setCancelable(false);

        //To Align to the layout of IOS, will exchange the position of positive button and negative button
        builder.setNegativeButton(Utils.getString(this, R.string.btn_accept), onPositiveClickListener);
        builder.setPositiveButton(Utils.getString(this, R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        //			clearNotificationData();
        //
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        finish();
    }

    @Override
    protected final void onPause() {
        super.onPause();

    }

    @Override
    protected final void onStop() {
        super.onStop();
        //		cleanupUI();
    }

//		protected void cleanupUI() {
//			regImageButtons = null;
//			lrButton= null ; 
//			infoContainer= null ;
//			serviceContainer= null ;
//			regInputHKID= null ;
//			regInputAccNum= null ;
//			regInputLoginId= null ;
//			regInputPw= null ;
//			regInputRePw= null ;
//			regInputNickname= null ;
//			regInputMobileReset= null ;
//	
//		}

    // Restore the Bundle data when re-built Activity
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (debug) Log.i(TAG, "load state");
        super.onRestoreInstanceState(savedInstanceState);
        //		regHkid = savedInstanceState.getString("REGHKID");
        //		regAccNum = savedInstanceState.getString("REGMOBILE");
        //		regLoginId = savedInstanceState.getString("REGLOGINID");
        //		regPw = savedInstanceState.getString("REGPW");
        //		regRePw = savedInstanceState.getString("REGREPW");
        //		regNickname = savedInstanceState.getString("REGNICKNAME");
        //		regMobileReset = savedInstanceState.getString("REGMOBILERESET");
    }

    // Saved data before Destroy Activity
    protected void onSaveInstanceState(Bundle outState) {
        if (debug) Log.i(TAG, "save state");

        super.onSaveInstanceState(outState);
        //TODO may need to remake as they should be in
//		regHkid 	= regInputHKID.getInput();
        regHkbr = regInputHKBR.getInput();
        regAccNum = regInputAccNum.getInput();
        regLoginId = regInputLoginId.getInput();
        regPw = regInputPw.getInput();
        regRePw = regInputRePw.getInput();
        regNickname = regInputNickname.getInput();
        regMobileReset = regInputMobileReset.getInput();
        if (debug) Log.i(TAG, regServicePos + "/" + regHkbr);
//		outState.putString("REGHKID", regHkid);
        outState.putString("REGHKBR", regHkbr);
        outState.putString("REGMOBILE", regAccNum);
        outState.putString("REGLOGINID", regLoginId);
        outState.putString("REGPW", regPw);
        outState.putString("REGREPW", regRePw);
        outState.putString("REGNICKNAME", regNickname);
        outState.putString("REGMOBILERESET", regMobileReset);

    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
    }

    @Override
    public void onLRBtnLeftClick() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onLRBtnRightClick() {
        // TODO Auto-generated method stub

    }
}