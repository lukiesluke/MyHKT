package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.wheat.shared.tool.Reply;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ResetPwdActivity extends BaseActivity {
    private static final String SCREEN_NAME = "Reset Password Screen";
    // Common Components.
    private static ResetPwdActivity me;
    private boolean debug = false;
    private final String TAG = this.getClass().getName();

    private String resetpwd;
    private boolean savepwd;
    private SveeRec sveeRec = null;
    private String errCode;
    private LgiCra lgiCra;
    private SaveAccountHelper saveAccountHelper;            // SQLite database for a new bill checking

    //UI
    private AAQuery aq;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        me = this;
        debug = getResources().getBoolean(R.bool.DEBUG);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            resetpwd = bundle.getString("RESETPWD");
            savepwd = bundle.getBoolean("SAVEPWD");
            sveeRec = (SveeRec) bundle.getSerializable("SVEEREC");
            errCode = bundle.getString("ERRCODE");
            lgiCra = (LgiCra) bundle.getSerializable("LOGINCRA");
        }
        setContentView(R.layout.activity_resetpwd);
    }


    @Override
    protected final void onResume() {
        super.onResume();
        moduleId = getResources().getString(R.string.MODULE_FORGET_PWD);
        // Update Locale
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        firebaseSetting.onScreenView(ResetPwdActivity.this, SCREEN_NAME);
    }

    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    protected void initUI2() {
        aq = new AAQuery(this);
        //navbar
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_resetpwd));
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        //Initialize the container
        aq.id(R.id.resetpwd_frame).backgroundColorId(R.color.white);
        aq.marginpx(R.id.resetpwd_frame, basePadding, 0, basePadding, 0);
        aq.id(R.id.resetpwd_frame).getView().setPadding(0, extralinespace / 2, 0, 0);

        aq.marginpx(R.id.resetpwd_heading, basePadding, 0, basePadding, 0);
        aq.normText(R.id.resetpwd_heading, getString(R.string.RSPF_RESETPWD));
        aq.marginpx(R.id.resetpwd_heading, basePadding, 0, 0, extralinespace);

        if (RC.INITPWD_SVEE.equalsIgnoreCase(errCode)) {
            aq.normText(R.id.resetpwd_heading, getString(R.string.RSPF_INITPWD));
        } else {
            aq.normText(R.id.resetpwd_heading, getString(R.string.RSPF_RESETPWD));
        }

        aq.normEditText(R.id.resetpwd_pwd_etxt, "", getString(R.string.REGF_PWD_HINT));
        aq.id(R.id.resetpwd_pwd_etxt).getEditText().setTransformationMethod(new PasswordTransformationMethod());
        aq.normEditText(R.id.resetpwd_retypepwd_etxt, "", "");
        aq.id(R.id.resetpwd_retypepwd_etxt).getEditText().setTransformationMethod(new PasswordTransformationMethod());


        //limit password input length
        int maxlength = 16;
        InputFilter[] fileter = new InputFilter[1];
        fileter[0] = new InputFilter.LengthFilter(maxlength);
        aq.id(R.id.resetpwd_pwd_etxt).getEditText().setFilters(fileter);
        aq.id(R.id.resetpwd_retypepwd_etxt).getEditText().setFilters(fileter);

        aq.marginpx(R.id.resetpwd_pwd_txt, basePadding, 0, basePadding, 0);
        aq.marginpx(R.id.resetpwd_retypepwd_txt, basePadding, 0, basePadding, 0);
        aq.marginpx(R.id.resetpwd_pwd_etxt, basePadding, 0, basePadding, extralinespace);
        aq.marginpx(R.id.resetpwd_retypepwd_etxt, basePadding, 0, basePadding, extralinespace);

        //button layout
        aq.norm2TxtBtns(R.id.resetpwd_btn_cancel, R.id.resetpwd_btn_confirm, getResString(R.string.MYHKT_BTN_CANCEL), getResString(R.string.MYHKT_BTN_CONFIRM));
        aq.id(R.id.resetpwd_btn_cancel).clicked(this, "onClick");
        aq.id(R.id.resetpwd_btn_confirm).clicked(this, "onClick");
    }

    // Restore the Bundle data when re-built Activity
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        sveeRec = (SveeRec) savedInstanceState.getSerializable("SVEEREC");
        resetpwd = savedInstanceState.getString("RESETPWD");
        savepwd = savedInstanceState.getBoolean("SAVEPWD");
        super.onRestoreInstanceState(savedInstanceState);
    }

    // Saved data before Destroy Activity
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("SVEEREC", sveeRec);
        outState.putString("RESETPWD", resetpwd);
        outState.putBoolean("SAVEPWD", savepwd);
        super.onSaveInstanceState(outState);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.resetpwd_btn_cancel:
                onBackPressed();
                break;
            case R.id.resetpwd_btn_confirm:
                InputMethodManager inputManager;
                try {
                    inputManager = (InputMethodManager) me.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    inputManager = null;
                } catch (Exception e) {
                    // fail to hide the keyboard
                }

                // check input and call interpretRC(...) for error message
                if (!resetpwd.equals("") && sveeRec != null) {
                    Reply reply = new Reply();
                    sveeRec.pwd = aq.id(R.id.resetpwd_pwd_etxt).getText().toString();
                    reply = sveeRec.verifyPwd();
                    if (!reply.isSucc()) {
                        //					displayDialog(interpretRC(reply));
                        DialogHelper.createSimpleDialog(me, interpretRC(reply));
                    } else if (aq.id(R.id.resetpwd_pwd_etxt).getText().toString().equals(resetpwd)) {
                        DialogHelper.createSimpleDialog(me, getString(R.string.RSPM_CANTSAME));
                    } else if (!aq.id(R.id.resetpwd_pwd_etxt).getText().toString().equals(aq.id(R.id.resetpwd_retypepwd_etxt).getText().toString())) {
                        DialogHelper.createSimpleDialog(me, getString(R.string.PAMM_IVCFMPWD));
                    } else {

                        AcMainCra acMainCra = new AcMainCra();
                        acMainCra.setILoginId(this.lgiCra.getOQualSvee().getSveeRec().loginId);
                        acMainCra.setIChgPwd(true);
                        acMainCra.setIOrigPwd(resetpwd);
                        acMainCra.setISveeRec(sveeRec);

                        APIsManager.doResetPwd(me, acMainCra);
                    }
                } else {
                    if (debug) Log.d("ResetPwdActivity", "resetpwd is empty or regCustRec = null.");
                }
                break;
        }
    }

    private final String interpretRC(Reply rRC) {
        //		if (rRC.isEqual(RC.RC_RCUS_NAPWD)) { return Utils.getString(me, R.string.REGM_NAPWD); }
        if (rRC.getCode().equals(RC.SVEE_NLPWD)) {
            return Utils.getString(me, R.string.REGM_ILPWD);
        }
        if (rRC.getCode().equals(RC.SVEE_IVPWDCBN)) {
            return Utils.getString(me, R.string.REGM_IVPWDCBN);
        }

        return ClnEnv.getRPCErrMsg(me.getApplicationContext(), rRC.getCode());
    }

    private final boolean CompareLastBillDate(String lastBilldate, String newlastBilldate) {
        SimpleDateFormat inputdf = new SimpleDateFormat("yyyyMMdd", Locale.US);
        Date lastBillDate, newlastBillDate;
        try {
            lastBillDate = inputdf.parse(lastBilldate);
            newlastBillDate = inputdf.parse(newlastBilldate);

            if (lastBillDate.compareTo(newlastBillDate) < 0) {
                return true;
            }
        } catch (Exception e) {
            // fail to convert date
            e.printStackTrace();
        }
        return false;
    }

    // Compare T-n Days
    private final boolean CompareDateAdd3(String apptcsdt, String gettodaydt) {
        SimpleDateFormat inputdf = new SimpleDateFormat(Utils.getString(me, R.string.input_datetime_format), Locale.US);
        Date apptcsdtDate, gettodaydtDate;
        try {
            apptcsdtDate = inputdf.parse(apptcsdt);
            gettodaydtDate = inputdf.parse(gettodaydt);

            // Reset Time = 00:00 before compare
            Calendar cal = Calendar.getInstance();
            cal.setTime(apptcsdtDate);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            apptcsdtDate = cal.getTime();

            cal.setTime(gettodaydtDate);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            gettodaydtDate = cal.getTime();

            cal.setTime(gettodaydtDate);
            cal.add(Calendar.DATE, ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_DAYS), 3));
            gettodaydtDate = cal.getTime();

            if (apptcsdtDate.compareTo(gettodaydtDate) <= 0) return true;
        } catch (Exception e) {
            // fail to convert day
            return false;
        }
        return false;
    }

    // redirect to MainMenuActivity screen if reset
    protected final void redirectMainMenuDialog(String message) {
        DialogHelper.createSimpleDialog(me, message, getString(R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
            }
        });
    }

    @Override
    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
        if (response != null) {
            if (APIsManager.RESET.equals(response.getActionTy())) {
                // Reset Password Success and Login again
                // Updated Password
                ClnEnv.setSessionPassword(aq.id(R.id.resetpwd_pwd_etxt).getText().toString());
                ClnEnv.setSessionLoginID(sveeRec.loginId);

                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVELOGINID), true);
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LOGINID), sveeRec.loginId);

                if (me.savepwd) {
                    // Saving password implies saving loginid
                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
                    ClnEnv.setEncPref(me.getApplicationContext(), sveeRec.loginId, me.getString(R.string.CONST_PREF_PASSWORD), aq.id(R.id.resetpwd_pwd_etxt).getText().toString());
                } else {
                    // Not saving password - remove the saved password
                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
                    ClnEnv.setEncPref(me.getApplicationContext(), "", me.getString(R.string.CONST_PREF_PASSWORD), "");
                }
                me.redirectMainMenuDialog(Utils.getString(me, R.string.RSPM_RESETDONE));
            }
        }
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        if (response != null) {
            if (APIsManager.RESET.equals(response.getActionTy())) {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    displayDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    ivSessDialog();
                } else {
                    displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
            }
        }
    }
}
