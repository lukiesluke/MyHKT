package com.pccw.myhkt.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.RequiresApi;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.TextBtnCell;
import com.pccw.myhkt.cell.model.TextImageBtnCell;
import com.pccw.myhkt.cell.model.TwoTextBtnCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.utils.Constant;
import com.pccw.myhkt.utils.FirebaseSetting;
import com.pccw.myhkt.utils.RuntimePermissionUtil;

import java.util.ArrayList;
import java.util.List;

/************************************************************************
 * File : ContactUsActivity.java
 * Desc : Contact list
 * Name : ContactUsActivity
 * by 	: 
 * Date :
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 03/07/2018  Abdulmoiz Esmail	-Updated business tab, replace the content from
 * 								UAT Comments - 3rd Round (20180302v1)	
 *************************************************************************/
public class ContactUsActivity extends BaseActivity {
    //List Data
    private static final String SCREEN_NAME = "Contact Us Page";
    List<String> listCol1Title;
    List<Integer> listCol2ButtonResId;
    List<String> listCol2Content;
    List<String> listCol2SubTitle;
    CellViewAdapter cellViewAdapter;
    LinearLayout frame;
    // Common Components
    private boolean debug = false;
    private final String TAG = this.getClass().getName();
    //UI
    private AAQuery aq;
    private List<Cell> cellList;
    /*
     * showType Definition
     * 0 : Consumer
     * 1 : HKT Premier
     * 2 : Business
     */
    private int showType = 0;
    private int svPos = 0;
    private int deviceWidth;
    private int extralinespace;
    private int colWidth;
    private int sColWidth;
    //Property of text
    private float textSize;
    private int textColor;
    private int textColorDisabled;
    private Drawable callIcon;
    private Drawable emailIcon;
    private Drawable eformIcon;
    private Drawable whatsappIcon;
    private String phoneNum;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        isZh = !ClnEnv.getAppLocale(this).equals(Utils.getString(this, R.string.CONST_LOCALE_EN));

        setContentView(R.layout.activity_contactus);
        initData();
        if (savedInstanceState != null) {
            showType = savedInstanceState.getInt("showType");
            svPos = savedInstanceState.getInt("svPos");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        moduleId = getResources().getString(R.string.MODULE_CONTACT_US);
        firebaseSetting.onScreenView(ContactUsActivity.this, SCREEN_NAME);
    }

    // Android Device Back Button Handling
    @Override
    public final void onBackPressed() {
        // clearNotificationData();
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private void setDrawableBounds(Drawable drawable, boolean small) {
        int bh = (int) getResources().getDimension(R.dimen.buttonblue_height);
        int padding_twocol = (int) getResources().getDimension(R.dimen.padding_twocol);
        int height = drawable.getIntrinsicHeight();
        int width = drawable.getIntrinsicWidth();
        int imageHeight = small ? bh / 3 : bh - padding_twocol * 3;
        int imageWidth = width * (imageHeight) / height;
        drawable.setBounds(0, 0, imageWidth, imageHeight);
    }

    private void initData() {
        me = this;
        Display display = me.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        textSize = getResources().getDimension(R.dimen.bodytextsize);
        textColor = R.color.hkt_txtcolor_grey;
        textColorDisabled = R.color.hkt_txtcolor_grey_disable;
        colWidth = (deviceWidth - basePadding * 2) / 4;
        sColWidth = (deviceWidth - basePadding * 2) * 2 / 9;
        showType = 0;
        cellViewAdapter = new CellViewAdapter(me);

        ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#187DC3"), PorterDuff.Mode.SRC_IN);

        callIcon = getResources().getDrawable(R.drawable.phone_small);
        callIcon.setColorFilter(filter);
        setDrawableBounds(callIcon, false);

        emailIcon = getResources().getDrawable(R.drawable.btn_mail);
        emailIcon.setColorFilter(filter);
        setDrawableBounds(emailIcon, true);

        eformIcon = getResources().getDrawable(R.drawable.eform_small);
        eformIcon.setColorFilter(filter);
        setDrawableBounds(eformIcon, false);

        whatsappIcon = getResources().getDrawable(R.drawable.whatsapp_small);
        whatsappIcon.setColorFilter(filter);
        setDrawableBounds(whatsappIcon, false);
    }

    @Override
    final protected void initUI2() {
        aq = new AAQuery(this);
        frame = (LinearLayout) aq.id(R.id.cu_frame2).getView();

        //navbar
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_cu_title));
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        //Initialize the container
        aq.id(R.id.cu_frame).backgroundColorId(R.color.white);
        aq.marginpx(R.id.cu_frame, basePadding, 0, basePadding, 0);
        aq.id(R.id.cu_frame).getView().setPadding(0, basePadding, 0, 0);

        //Top tab bar
        aq.marginpx(R.id.cu_buttonsframe, 0, 0, 0, extralinespace);
        int[] buttonResIds = new int[]{R.id.cu_button0, R.id.cu_button1, R.id.cu_button2};
        int[] stringsResIds = new int[]{R.string.myhkt_cu_conButtonTitle, R.string.SUPPORT_PREMIER, R.string.myhkt_cu_busButtonTitle};
        for (int j = 0; j < 3; j++) {
            aq.id(buttonResIds[j]).height((int) getResources().getDimension(R.dimen.button_height), false).textColorId(textColor).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            aq.id(buttonResIds[j]).text(stringsResIds[j]);
            aq.id(buttonResIds[j]).clicked(this, "onClick");
        }

        aq.id(R.id.cu_button1).visibility(View.GONE);
        if (ClnEnv.isLoggedIn()) {
            if (ClnEnv.getSessionPremierFlag()) {
                // Login and Premier
                showType = 1;
                aq.id(R.id.cu_button0).visibility(View.GONE);

                //Screen Tracker
            } else {
                // Login and Non Premier
                showType = 0;
                aq.id(R.id.cu_button1).visibility(View.GONE);
            }
        }
        //Show the list
        prepareListData();
        refreshView();
        aq.id(R.id.cu_button2).click();
    }

    private void refreshView() {
        setBtns();
        aq.id(R.id.cu_sv).getView().scrollTo(0, 0);
        cellList = new ArrayList<>();

        for (int i = 0; i < listCol1Title.size(); i++) {
            String title = listCol1Title.get(i);
            if (debug) Utils.showLog(TAG, "Sub" + listCol2SubTitle.get(i) + "Title");
            if (listCol2Content.get(i).isEmpty() && !(title.equals(getResString(R.string.support_biz_e_enquiry_eform)) || title.equals(getResString(R.string.support_biz_e_enquiry_whatsapp)))) {
                //Add facebook Button
                if (listCol2ButtonResId.get(i) == R.drawable.ic_facebook) {
                    OnClickListener fbImageClicked = new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String fburl = "https://www.facebook.com/pccwcs";
                            Intent fbintent = new Intent(Intent.ACTION_VIEW);
                            fbintent.setData(Uri.parse(fburl));
                            startActivity(fbintent);
                        }
                    };

                    TextImageBtnCell fbImageCell = new TextImageBtnCell(fbImageClicked, listCol1Title.get(i), listCol2ButtonResId.get(i));
                    fbImageCell.setTopMargin(0);
                    fbImageCell.setLeftPadding(basePadding);
                    fbImageCell.setRightPadding(basePadding);
                    fbImageCell.setBotMargin(0);
                    cellList.add(fbImageCell);
                } else {
                    BigTextCell bigTextCell = new BigTextCell(listCol1Title.get(i), "");
                    bigTextCell.setTopMargin(0);
                    bigTextCell.setBotMargin(0);
                    bigTextCell.setLeftMargin(basePadding);
                    bigTextCell.setRightMargin(basePadding);
                    if (!listCol2SubTitle.get(i).isEmpty()) {
                        bigTextCell.setTitleColorId(R.color.hkt_txtcolor_grey);
                        bigTextCell.setTitleSizeDelta(0);
                    } else {
                        bigTextCell.setTitleTypeface(Typeface.BOLD);
                    }
                    cellList.add(bigTextCell);
                }

            } else {
                OnClickListener detailClicked;
                final int pos = i;
                String btnTextName;
                String content = listCol2Content.get(i);
                String subTitle = listCol2SubTitle.get(i);

                if (!title.isEmpty()) {
                    SmallTextCell smallTextCell = new SmallTextCell(title, "");
                    smallTextCell.setLeftPadding(basePadding);
                    smallTextCell.setRightPadding(basePadding);
                    cellList.add(smallTextCell);
                }

                if (content.contains("@")) {
                    btnTextName = getResString(R.string.btn_send);
                    detailClicked = new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_SENDTO);
                            intent.setData(Uri.fromParts(
                                    "mailto", listCol2Content.get(pos), null)); // only email apps should handle this
                            //						    intent.putExtra(Intent.EXTRA_EMAIL, listCol2ButtonText.get(pos));
                            //						    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                            if (intent.resolveActivity(me.getPackageManager()) != null) {
                                //to app directly
                                startActivity(intent);
                            } else {
                                DialogHelper.createSimpleDialog(me, "No app can perform this action");
                            }
                        }
                    };
                } else if (content.equals(getResString(R.string.SUB_TITLE_LIVE_CHAT))) {
                    btnTextName = getResString(R.string.MYHKT_BTN_LIVECHAT);
                    detailClicked = new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!ClnEnv.isLoggedIn()) {
                                loginFirstDialog(MAINMENU.CONTACTUS);
                            } else {
                                openLiveChat();
                            }

                        }
                    };
                } else if (title.equals(getResString(R.string.support_biz_e_enquiry_eform))) {
                    btnTextName = getResString(R.string.btn_eform);
                    detailClicked = v -> {
                        firebaseSetting.onContactButtonTap(FirebaseSetting.EVENT_CONTACT_US_E_FORM, "e-Form");
                        String url = isZh ? "https://www.hkt-sme.com/contact-us/" : "https://www.hkt-sme.com/en/contact-us/";
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(intentBrowser);
                    };
                } else if (title.equals(getResString(R.string.support_biz_e_enquiry_whatsapp))) {
                    btnTextName = getResString(R.string.btn_whatsapp);
                    detailClicked = v -> {
                        firebaseSetting.onContactButtonTap(FirebaseSetting.EVENT_CONTACT_US_WHATSAPP, "WhatsApp");
                        Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://hktcslchl.veserve.cc/url/59f07fb95fa"));
                        startActivity(intentBrowser);
                    };
                } else {
                    btnTextName = getResString(R.string.btn_call_now);
                    detailClicked = new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                String phoneNumber = listCol2Content.get(pos);
                                if (phoneNumber.equalsIgnoreCase("10088")
                                        || phoneNumber.equalsIgnoreCase("1833111")) {
                                    firebaseSetting.onContactButtonTap(FirebaseSetting.EVENT_CONTACT_US + phoneNumber, phoneNumber);
                                }
                                callPhone(phoneNumber);
                            }
                        }
                    };
                }
                if (subTitle.isEmpty()) {
                    TextBtnCell textBtnCell = new TextBtnCell(detailClicked, content, btnTextName, listCol2ButtonResId.get(i));
                    if (content.equals(getResString(R.string.SUB_TITLE_LIVE_CHAT))) {
                        textBtnCell.setTitleSizeDelta(-4);
                    } else {
                        textBtnCell.setTitleSizeDelta(-2);
                    }

                    textBtnCell.setTitleColorId(R.color.hkt_textcolor);
                    textBtnCell.setTitleGravity(Gravity.TOP | Gravity.LEFT);
                    textBtnCell.setTitleHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                    textBtnCell.setBtnWidth((int) getResources().getDimension(R.dimen.buttonblue_width));
                    Integer ButtonId = listCol2ButtonResId.get(i);
                    if (ButtonId == R.drawable.phone_small) {
                        textBtnCell.setDrawable(callIcon);
                    } else if (ButtonId == R.drawable.btn_mail) {
                        textBtnCell.setDrawable(emailIcon);
                    } else if (ButtonId == R.drawable.eform_small) {
                        textBtnCell.setDrawable(eformIcon);
                    } else if (ButtonId == R.drawable.whatsapp_small) {
                        textBtnCell.setDrawable(whatsappIcon);
                    }
                    cellList.add(textBtnCell);
                } else {
                    TwoTextBtnCell textBtnCell = new TwoTextBtnCell(detailClicked, subTitle, content, btnTextName, listCol2ButtonResId.get(i));
                    textBtnCell.setContentSizeDelta(-2);
                    textBtnCell.setTitleSizeDelta(0);
                    textBtnCell.setContentColorId(R.color.hkt_textcolor);
                    textBtnCell.setTopMargin(extralinespace);
                    textBtnCell.setBotMargin(extralinespace);
                    textBtnCell.setBtnWidth((int) getResources().getDimension(R.dimen.buttonblue_width));
                    if (listCol2ButtonResId.get(i) == R.drawable.phone_small) {
                        textBtnCell.setDrawable(callIcon);
                    } else if (listCol2ButtonResId.get(i) == R.drawable.btn_mail) {
                        textBtnCell.setDrawable(emailIcon);
                    }

                    cellList.add(textBtnCell);
                }
            }

            // if (listCol2SubTitle.get(i).equals("") || (i !=listCol1Title.size()-1 && listCol2ButtonResId.get(i+1) ==-1 )) {
            if (i != listCol1Title.size() - 1 && listCol2ButtonResId.get(i + 1) == -1 && listCol2SubTitle.get(i + 1).isEmpty()) {
                Cell cellLine = new Cell(Cell.LINE);
                cellLine.setTopPadding(extralinespace);
                cellLine.setLeftPadding(basePadding);
                cellLine.setRightPadding(basePadding);
                cellLine.setBottompadding(extralinespace);
                cellLine.setBotMargin(extralinespace);
                cellList.add(cellLine);
            }
        }
        cellViewAdapter.setView(frame, cellList, svPos);
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        switch (showType) {
            //Consumer
            case 0:
                listCol1Title = new ArrayList<String>();
                listCol2ButtonResId = new ArrayList<Integer>();
                listCol2Content = new ArrayList<String>();
                listCol2SubTitle = new ArrayList<String>();

                //  //Live Chat Group
                //  listCol1Title.add(getResString(R.string.BTN_LIVE_CHAT));
                //  listCol2ButtonResId.add(-1);
                //  listCol2Content.add("");
                //  listCol2SubTitle.add("");
                //
                //  listCol1Title.add(getResString(R.string.TITLE_LIVE_CHAT));
                //  listCol2ButtonResId.add(R.drawable.livechat_small);
                //  listCol2Content.add(getResString(R.string.SUB_TITLE_LIVE_CHAT));
                //  listCol2SubTitle.add("");

                //HotLines Group
                listCol1Title.add(getResString(R.string.support_hotlines));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_consumer_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_consumer_hotline_no));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.myhkt_cu_tvTitle) + " " + getResString(R.string.support_consumer_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_consumer_tv_hotline_no));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.myhkt_cu_cslTitle) + " " + getResString(R.string.support_consumer_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_mobile_hotline_no));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.myhkt_cu_1010Title) + " " + getResString(R.string.support_consumer_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_mobile_hotline_no_1010));
                listCol2SubTitle.add("");

                //Sales Hotline Group
                listCol1Title.add(getResString(R.string.support_sales_hotline_title));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_hotline));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_sales_hotline_no));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_sales_csl_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_sales_csl_hotline_no));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.myhkt_cu_1010Title) + " " + getResString(R.string.support_sales_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_sales_1010_hotline_no));
                listCol2SubTitle.add("");

                //Email Inq Group
                listCol1Title.add(getResString(R.string.support_email_enquiries));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_consumer_email));
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.support_consumer_email_info));
                listCol2SubTitle.add("");

                //NETVIGATOR
                listCol1Title.add(getResString(R.string.myhkt_cu_pcdTitle));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_pcdTitle));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_pcdemail_cs));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_GI));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_pcdemail_ts));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_TS));

                //Now TV
                listCol1Title.add(getResString(R.string.myhkt_cu_tvTitle));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_tvTitle));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_tvemail_cs));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_GI));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_tvemail_ts));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_TS));

                //Other
                listCol1Title.add(getResString(R.string.support_consumer_others));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_consumer_others_fb) + "\n" + getResString(R.string.support_consumer_others_fbinfo));
                listCol2ButtonResId.add(R.drawable.ic_facebook);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                break;
            //HKT Premier
            case 1:
                listCol1Title = new ArrayList<String>();
                listCol2ButtonResId = new ArrayList<Integer>();
                listCol2Content = new ArrayList<String>();
                listCol2SubTitle = new ArrayList<String>();

                if (ClnEnv.isLoggedIn() && ClnEnv.getSessionPremierFlag()) {
                    //Live Chat Group
                    listCol1Title.add(getResString(R.string.BTN_LIVE_CHAT));
                    listCol2ButtonResId.add(-1);
                    listCol2Content.add("");
                    listCol2SubTitle.add("");

                    listCol1Title.add(getResString(R.string.TITLE_LIVE_CHAT));
                    listCol2ButtonResId.add(R.drawable.livechat_small);
                    listCol2Content.add(getResString(R.string.SUB_TITLE_LIVE_CHAT));
                    listCol2SubTitle.add("");
                }

                //HotLines Group
                listCol1Title.add(getResString(R.string.support_hotlines));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.SUPPORT_PREMIER_HOTLINE_TITLE));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.SUPPORT_PREMIER_HOTLINE_NO));
                listCol2SubTitle.add("");

                //Email Inq Group
                listCol1Title.add(getResString(R.string.support_email_enquiries));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.SUPPORT_PREMIER_HOTLINE_TITLE));
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.SUPPORT_PREMIER_EMAIL_CS));
                listCol2SubTitle.add("");

                break;
            //Business
            case 2:
                listCol1Title = new ArrayList<String>();
                listCol2ButtonResId = new ArrayList<Integer>();
                listCol2Content = new ArrayList<String>();
                listCol2SubTitle = new ArrayList<String>();

                //Title: e-Enquiry
                listCol1Title.add(getResString(R.string.support_biz_e_enquiry));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                //e-Form
                listCol1Title.add(getResString(R.string.support_biz_e_enquiry_eform));
                listCol2ButtonResId.add(R.drawable.eform_small);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                //Whatsapp
                listCol1Title.add(getResString(R.string.support_biz_e_enquiry_whatsapp));
                listCol2ButtonResId.add(R.drawable.whatsapp_small);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                //Title: HotLines Group
                listCol1Title.add(getResString(R.string.support_biz_customer_service_title));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                //bus customer hotline
                listCol1Title.add(getResString(R.string.support_biz_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_biz_hotline_no));
                listCol2SubTitle.add("");

                //one comm hotline
                listCol1Title.add(getResString(R.string.support_biz_customer_one_comm_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_biz_customer_one_comm_no));
                listCol2SubTitle.add("");

                //Title: Email Inq Group
                listCol1Title.add(getResString(R.string.support_email_enquiries));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                //one comm/bus net/now tv
                listCol1Title.add(getResString(R.string.support_biz_customer_email_one_comm_title));
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.support_biz_customer_email_one_comm_info));
                listCol2SubTitle.add("");

                //1010/csl
                listCol1Title.add(getResString(R.string.support_biz_email));
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.support_biz_email_info));
                listCol2SubTitle.add("");
                break;
        }
    }

    public void onClick(View v) {
        //Screen Tracker

        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.cu_button0:
                //tracker
                Utils.closeSoftKeyboard(me, v);
                showType = 0;
                prepareListData();
                refreshView();
                break;
            case R.id.cu_button1:
                //tracker
                Utils.closeSoftKeyboard(me, v);
                showType = 1;
                prepareListData();
                refreshView();
                break;
            case R.id.cu_button2:
                //tracker
                Utils.closeSoftKeyboard(me, v);
                showType = 2;
                prepareListData();
                refreshView();
                break;
        }
    }

    private void setBtns() {
        aq.id(R.id.cu_button0).background(showType == 0 ? R.drawable.hkt_cubtnleft_bg_sel : R.drawable.hkt_cubtnleft_bg_not_sel);
        aq.id(R.id.cu_button0).textColorId(showType == 0 ? textColor : textColorDisabled);
        aq.id(R.id.cu_button1).background(showType == 1 ? R.drawable.hkt_cubtnmiddle_bg_sel : R.drawable.hkt_cubtnmiddle_bg_not_sel);
        aq.id(R.id.cu_button1).textColorId(showType == 1 ? textColor : textColorDisabled);
        aq.id(R.id.cu_button2).background(showType == 2 ? R.drawable.hkt_cubtnright_bg_sel : R.drawable.hkt_cubtnright_bg_not_sel);
        aq.id(R.id.cu_button2).textColorId(showType == 2 ? textColor : textColorDisabled);
    }


    // Saved data before Destroy Activity
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        svPos = aq.id(R.id.cu_sv).getView().getScrollY();
        outState.putInt("showType", showType);
        outState.putInt("svPos", svPos);
    }

    //MOIZ: 4/1/18 - Check if the permission granted,  before  dialing the phone number
    @SuppressLint("MissingPermission")
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void callPhone(String phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            this.phoneNum = phoneNumber;
            if (RuntimePermissionUtil.isActionCallPermissionGranted(this)) {
                String num = phoneNum;
                if (num.contains("(")) {
                    num = num.substring(0, num.indexOf("(") - 1);
                }
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
                startActivity(intent);

            } else {
                if (ClnEnv.getPref(this, Constant.CALL_PHONE_PERMISSION_DENIED, false)) {
                    displayDialog(this, getString(R.string.permission_denied_setting_enabled));
                } else {
                    RuntimePermissionUtil.requestCallPhonePermission(this);
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constant.REQUEST_CALL_PHONE_PERMISSION) {
            //Permission is granted
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && !TextUtils.isEmpty(phoneNum)) {
                String num = phoneNum;
                if (num.contains("(")) {
                    num = num.substring(0, num.indexOf("(") - 1);
                }
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
                startActivity(intent);
            } else {
                displayDialog(this, getString(R.string.permission_denied));

                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowCallPhoneRequestPermission(this)) {
                    ClnEnv.setPref(getApplicationContext(), Constant.CALL_PHONE_PERMISSION_DENIED, true);
                }
            }
        }
    }
}
