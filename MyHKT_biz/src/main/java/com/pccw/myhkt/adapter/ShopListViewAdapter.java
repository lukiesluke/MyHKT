package com.pccw.myhkt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;

public class ShopListViewAdapter extends BaseAdapter{

	// ShopRec
	// Assume the input array has been processed
	private final ShopRec[]	shopRec;
	private final boolean	isZh;
	public Context			context;
	public LayoutInflater	inflater;

	public ShopListViewAdapter(Context context, ShopRec[] sr) {
		super();
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		shopRec = sr;

		// Language indicator
        isZh = "zh".equalsIgnoreCase(Utils.getString(context, R.string.myhkt_lang));
	}

	public int getCount() {
		if (shopRec == null) return 0;
		return shopRec.length;
	}

	public Object getItem(int position) {
		if (position >= 0 && position < shopRec.length) {
			return shopRec[position];
		} else {
			return null;
		}
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		try {
			ViewHolder holder;

			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.adapter_shoplist, null);

				holder.adapter_shoplist_logo = convertView.findViewById(R.id.adapter_shoplist_logo);
				holder.adapter_shoplist_header = convertView.findViewById(R.id.adapter_shoplist_header);
				// 20121218 Vincent Add a subheading to separate the District
				holder.adapter_shoplist_separator = convertView.findViewById(R.id.adapter_shoplist_separator);
				
				holder.adapter_shoplist_address = convertView.findViewById(R.id.adapter_shoplist_address);
//				holder.adapter_shoplist_perhr = (TextView) convertView.findViewById(R.id.adapter_shoplist_perhr);
//				holder.adapter_shoplist_bushr = (TextView) convertView.findViewById(R.id.adapter_shoplist_bushr);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			// Shop type - PCCWHKT Shop or SmartLiving or CSC?
			//new icon by shop type
			int height = context.getResources().getInteger(R.integer.shop_icon_height);
		if ("S".equalsIgnoreCase(shopRec[position].shop_ty)) {
			holder.adapter_shoplist_logo.setImageBitmap(Utils.scaleImage(context, R.drawable.shop_hkt, height, height));
		} else if ("D".equalsIgnoreCase(shopRec[position].shop_ty)) {
			holder.adapter_shoplist_logo.setImageBitmap(Utils.scaleImage(context, R.drawable.shop_csl, height, height));
		} else if ("1".equalsIgnoreCase(shopRec[position].shop_ty)) {
			holder.adapter_shoplist_logo.setImageBitmap(Utils.scaleImage(context, R.drawable.shop_1010, height, height));
		} else if ("L".equalsIgnoreCase(shopRec[position].shop_ty)) {
			holder.adapter_shoplist_logo.setImageBitmap(Utils.scaleImage(context, R.drawable.shop_smart, height, height));
		} else if ("I".equalsIgnoreCase(shopRec[position].shop_ty)) {
			holder.adapter_shoplist_logo.setImageBitmap(Utils.scaleImage(context, R.drawable.shop_iot, height, height));
		} else {
			holder.adapter_shoplist_logo.setImageBitmap(Utils.scaleImage(context, R.drawable.shop_cs, height, height));
		}

// commented original version for testing
//			if (shopRec[position].shop_ty.equalsIgnoreCase(ShopRec.TY_SHOP)) {
//				holder.adapter_shoplist_logo.setImageResource(R.drawable.shop_pccw_hkt);
//			} else if (shopRec[position].shop_ty.equalsIgnoreCase(ShopRec.TY_SMARTLIVING)) {
//				holder.adapter_shoplist_logo.setImageResource(R.drawable.shop_smartliving);
//			} else {
//				holder.adapter_shoplist_logo.setImageResource(R.drawable.shop_cs);
//			}

			holder.adapter_shoplist_header.setText((isZh) ? shopRec[position].desn_zh : shopRec[position].desn_en);
			holder.adapter_shoplist_address.setText((isZh) ? shopRec[position].addr_zh : shopRec[position].addr_en);
//			holder.adapter_shoplist_perhr.setText((isZh) ? shopRec[position].bz_hr_zh : shopRec[position].bz_hr_en);
			
			
			// 20121218 Vincent Add a subheading to separate the District
			if (showSeparator(position)) {
				holder.adapter_shoplist_separator.setText((isZh) ? shopRec[position].district_zh : shopRec[position].district_en);
				holder.adapter_shoplist_separator.setVisibility(View.VISIBLE);
			} else {
				holder.adapter_shoplist_separator.setVisibility(View.GONE);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	public static class ViewHolder {
		ImageView	adapter_shoplist_logo;
		TextView	adapter_shoplist_header;
		// 20121218 Vincent Add a subheading to separate the District
		TextView	adapter_shoplist_separator;
		TextView 	adapter_shoplist_perhr;
		TextView 	adapter_shoplist_address;
		TextView	adapter_shoplist_bushr;
	}

	private final boolean showSeparator(int position) {
		if (position == 0) { return true; }

		// solely compare the district value of the former element
		try {
			if (shopRec[position].district_en.equalsIgnoreCase(shopRec[position - 1].district_en)) { return false; }
		} catch (Exception e) {
		}

		return true;
	}
}
