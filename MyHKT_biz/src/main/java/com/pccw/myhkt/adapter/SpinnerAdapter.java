package com.pccw.myhkt.adapter;

/************************************************************************
File       : IDDDestListViewAdapter.java
Desc       : Destination list for IDD Rates search function
Name       : IDDDestListViewAdapter
Created by : Derek Tsui
Date       : 23/01/2014

Change History:
Date       Modified By        	Description
---------- ----------------   	-------------------------------
23/01/2014 Derek Tsui       	- First draft

*************************************************************************/

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pccw.biz.myhkt.R;

public class SpinnerAdapter extends BaseAdapter {
	
	public 		  Context				context;
	public 		  LayoutInflater		inflater;
	private 	  int					itemHeight;
	private       List<String> 			lists;
	//Filterable
	
	
	public SpinnerAdapter(Context context, List<String> lists) {
		super();
		this.context = context;		
		this.lists = lists;
	}
	
	@Override
	public int getCount() {
		if (lists == null) return 0;
		return lists.size();
	}

	@Override
	public Object getItem(int position) {
		if (position >= 0 && position < lists.size()) {
			return lists.get(position);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView textView = new TextView(context);
		int edittextpadding = (int)context.getResources().getDimension(R.dimen.edittextpadding);
		textView.setTextColor(context.getResources().getColor(R.color.hkt_textcolor));
		textView.setHeight((int) context.getResources().getDimension(R.dimen.reg_input_height)/2);
//		textView.setBackgroundResource(R.drawable.hkt_edittextblue_bg);
//		super.id(id).getSpinner()..setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getContext().getResources().getDimension(R.dimen.bodytextsize));
//		
		textView.setText(lists.get(position));
		Drawable drawable = context.getResources().getDrawable(R.drawable.btn_arrowdown);
		drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
		textView.setCompoundDrawables(null, null, drawable, null);
		
		textView.setPadding(edittextpadding, 0, edittextpadding, 0);
		textView.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		
		return textView;
	}
	
}
