package com.pccw.myhkt.adapter;

/************************************************************************
File       : ServiceListViewAdapter.java
Desc       : Subscription List
Name       : ServiceListViewAdapter
Created by : Derek Tsui
Date       : 24/12/2015

Change History:
Date       Modified By        	Description
---------- ----------------   	-------------------------------
24/12/2015 Derek Tsui			- First draft
28/12/2015 Andy Wong			- First draft
25/01/2015 Derek Tsui			- base functions updated (sorting, acctagent)
 *************************************************************************/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pccw.dango.shared.entity.LtrsRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.ServiceListActivity;
import com.pccw.myhkt.lib.swipelistview.SwipeListView;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.model.SubService;

public class ServiceListViewAdapter extends BaseAdapter {
	boolean					debug			= false;

	// ServiceListArray
	private AcctAgent		acctAgentAry[]	= null;
	private SaveAccountHelper saveAccountHelper;
	private LnttAgent lnttAgent;
	private boolean isStartedAnimationIcon = false;
	private Timer	timer = new Timer();
	private String TAG = "ServiceListViewAdapter";

	public Activity			context;
	public LayoutInflater	inflater;

	private static boolean 	isIDD 		   = false;
	public static final int IDDRATE = 2;
	private  boolean isZh = false;

	private int logoH, logoW;
	// CallBack
	private OnServiceListViewAdapterListener			callback;

	public interface OnServiceListViewAdapterListener {
		void displayEditDialog(String message, final int position);
		void showRemark(boolean show);
	}

	public ServiceListViewAdapter(Fragment frag, int parentOnClickId, SwipeListView mSwipeListView) throws Exception {
		super();
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			callback = (OnServiceListViewAdapterListener) frag;
		} catch (ClassCastException e) {
			throw new ClassCastException(frag.toString() + " must implement OnServiceListViewAdapterListener");
		}

        isIDD = parentOnClickId == IDDRATE;

		this.context = frag.getActivity();
		isZh = !ClnEnv.getAppLocale(context).equalsIgnoreCase("en");
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		debug = context.getResources().getBoolean(R.bool.DEBUG);
		prepareAcctAgent();
		lnttAgent = Utils.getPrefLnttAgent(context);


		//		Drawable drawable = context.getResources().getDrawable(R.drawable.logo_fixedline_fixedline);
		//		logoH = drawable.getIntrinsicHeight();
		//		logoW = drawable.getIntrinsicWidth();

		BitmapFactory.Options option = new BitmapFactory.Options();
		option.inJustDecodeBounds = true;
		option.inPurgeable = true;
		Bitmap bm =  BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_fixedline_fixedline, option);
		logoH = option.outHeight;
		logoW = option.outWidth;
	}

	public ServiceListViewAdapter(Activity context, int parentOnClickId, SwipeListView mSwipeListView) throws Exception {
		super();
		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			callback = (OnServiceListViewAdapterListener) context;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement OnServiceListViewAdapterListener");
		}
        isIDD = parentOnClickId == IDDRATE;
		isZh = !ClnEnv.getAppLocale(context).equalsIgnoreCase("en");
		this.context = context;		
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		debug = context.getResources().getBoolean(R.bool.DEBUG);
		prepareAcctAgent();
		lnttAgent = Utils.getPrefLnttAgent(context);
		BitmapFactory.Options option = new BitmapFactory.Options();
		option.inJustDecodeBounds = true;
		option.inPurgeable = true;
		Bitmap bm =  BitmapFactory.decodeResource(context.getResources(), R.drawable.logo_fixedline_fixedline, option);
		logoH = option.outHeight;
		logoW = option.outWidth;
	}

	public final int getCount() {
		if (acctAgentAry == null) { return 0; }
		return acctAgentAry.length;
	}

	// We need to return the correct Account object from either assocSubnAry / AcctAry
	public final Object getItem(int position) {
		//		if (position >= acctAgentAry.length) { return null; }
		//
		//		if (acctAgentAry[position].getAcctX() == -1) {
		//			// the requested item is from SubnRecAry (Active)
		//			// Return type is SubscriptionRec
		//			return ClnEnv.getQualSvee().getSubnRecAry()[acctAgentAry[position].getSubnRecX()];
		//		} else if (acctAgentAry[position].getSubnRecX() == -1) {
		//			// the requested item is from AcctAry (Zombie)
		//			// Return type is Account
		//			return ClnEnv.getQualSvee().getZmSubnRecAry()[acctAgentAry[position].getAcctX()].deriveAcct();
		//		} else {
		//			return null;
		//		}
		return acctAgentAry[position];
	}

	public final Object getSubnRecItem(int position) {
		return acctAgentAry[position].getSubnRec();
	}

	// We need to know if the requested position is from assocSubnAry / AcctAry
	public final boolean isItemAssocSubnAry(int position) {
		if (position >= acctAgentAry.length) {
			return false;
		} else {
			return (acctAgentAry[position].getAcctX() == -1);
		}
	}

	public final long getItemId(int position) {
		return position;
	}

	public final String getLOB(int position) {
		return  (position >= acctAgentAry.length) ? null : acctAgentAry[position].getLob();
	}

	public final boolean isZombie(int position) {
		return !acctAgentAry[position].isLive();
	}

	public final View getView(final int position, View convertView, ViewGroup parent) {
		try {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.adapter_servicelist, null);

				holder.adapter_servicelist_logo = convertView.findViewById(R.id.adapter_servicelist_logo);
				holder.adapter_servicelist_header_alias = convertView.findViewById(R.id.adapter_servicelist_header_alias);
				holder.adapter_servicelist_header = convertView.findViewById(R.id.adapter_servicelist_header);
				holder.adapter_servicelist_ltslabelImage = convertView.findViewById(R.id.adapter_servicelist_ltslabelImage);
				holder.adapter_servicelist_ltslabel = convertView.findViewById(R.id.adapter_servicelist_ltslabel);
				holder.adapter_servicelist_star = convertView.findViewById(R.id.adapter_servicelist_star);
				holder.adapter_servicelist_lnttind = convertView.findViewById(R.id.adapter_servicelist_lnttind);

				//SwipeListView
				holder.adapter_servicelist_edit = convertView.findViewById(R.id.adapter_servicelist_edit);
				LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.adapter_servicelist_edit.getLayoutParams();
				int deviceWidth = context.getResources().getDisplayMetrics().widthPixels;
				params.width = deviceWidth * 1 / 6;	
				holder.adapter_servicelist_edit.setLayoutParams(params);
				//				holder.adapter_servicelist_edit.setOnTouchListener(new CCMCHoverButton(context, R.string.CONST_BTN_ALIAS, false));

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			//			holder.adapter_servicelist_ltslabel.setVisibility(View.GONE);
			holder.adapter_servicelist_ltslabelImage.setVisibility(View.GONE);
			holder.adapter_servicelist_star.setVisibility(View.GONE);
			holder.adapter_servicelist_lnttind.setVisibility(View.GONE);

			if (acctAgentAry[position].getLob().equals(SubnRec.LOB_LTS)) {
//				switch (acctAgentAry[position].getLtsType()) {
//				case R.string.CONST_LTS_FIXEDLINE:
//					holder.adapter_servicelist_logo.setImageResource(R.drawable.logo_fixedline);
//					break;
//				case R.string.CONST_LTS_EYE:
//					holder.adapter_servicelist_logo.setImageResource(R.drawable.logo_fixedline);
//					break;
//				case R.string.CONST_LTS_IDD0060:
//					holder.adapter_servicelist_logo.setImageResource(R.drawable.logo_iddservices);
//					break;
//				case R.string.CONST_LTS_CALLINGCARD:
//					holder.adapter_servicelist_logo.setImageResource(R.drawable.logo_iddservices);
//					break;
//				case R.string.CONST_LTS_ONECALL:
//					holder.adapter_servicelist_logo.setImageResource(R.drawable.logo_iddservices);
//					break;
//				case R.string.CONST_LTS_ICFS:
//					holder.adapter_servicelist_logo.setImageResource(R.drawable.logo_iddservices);
//					break;
//				default:
//					holder.adapter_servicelist_logo.setImageResource(R.drawable.logo_fixedline);
//					break;
//				}
				if(acctAgentAry[position].getSubnRec().tos.equals(SubnRec.TOS_LTS_OCM)){
					holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_lts_ocm);
				} else {
					holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_lts_tel);
				}
			} else if (acctAgentAry[position].getLob().equals(SubnRec.LOB_MOB)) {
				holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_csl_plain);			
			} else if (acctAgentAry[position].getLob().equals(SubnRec.LOB_IOI)) {
				holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_1010_plain);
			} else if (acctAgentAry[position].getLob().equals(SubnRec.LOB_PCD)) {
				holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_pcd_plain);
			} else if (acctAgentAry[position].getLob().equals(SubnRec.LOB_TV)) {
				holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_tv_plain);
			} else if (acctAgentAry[position].getLob().equals(SubnRec.LOB_101)) {
				holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_1010_plain);
			} else if (acctAgentAry[position].getLob().equals(SubnRec.LOB_O2F)) {
				holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_csl_plain);
			} else if (acctAgentAry[position].getLob().equals(SubnRec.LOB_IMS)) {
				holder.adapter_servicelist_logo.setImageResource(R.drawable.lob_ims);
			}
			//			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.adapter_servicelist_logo.getLayoutParams();
			//			params.height = logoH;
			//			params.width = logoW;
			//			Log.i(TAG, logoH + "/" +logoW );
			//			holder.adapter_servicelist_logo.setLayoutParams(params);
			//			holder.adapter_servicelist_logo.setScaleType(ScaleType.FIT_CENTER);

			if (acctAgentAry[position].getAlias() != null && !"".equalsIgnoreCase(acctAgentAry[position].getAlias())) {
				holder.adapter_servicelist_header_alias.setVisibility(View.VISIBLE);
				holder.adapter_servicelist_header_alias.setText(acctAgentAry[position].getAlias());
			} else {
				holder.adapter_servicelist_header_alias.setText("");
				holder.adapter_servicelist_header_alias.setVisibility(View.GONE);
			}

			if (acctAgentAry[position].isLive()) {
				// active account
				if (acctAgentAry[position].getLob().equals(SubnRec.LOB_LTS) && (acctAgentAry[position].getLtsType() == R.string.CONST_LTS_CALLINGCARD)) {
					int last4index = (acctAgentAry[position].getSrvNum().length() - 4) < 0 ? 0 : acctAgentAry[position].getSrvNum().length() - 4;
					holder.adapter_servicelist_header.setText(String.format("CARD%s", acctAgentAry[position].getSrvNum().substring(last4index)));
				} else {
					holder.adapter_servicelist_header.setText(acctAgentAry[position].getSrvNum());
				}

				saveAccountHelper = SaveAccountHelper.getInstance(context);
				//new bill indicator enabled for for C-sim as well.  v1.2 2015-1-19
				//				if (!acctAgentAry[position].lob.equals(SubnRec.LOB_101) && !acctAgentAry[position].lob.equals(SubnRec.LOB_O2F) && ClnEnv.getPref(context.getApplicationContext(), context.getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true) && saveAccountHelper.getflagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), acctAgentAry[position].acctNum)) {
				if (ClnEnv.getPref(context.getApplicationContext(), context.getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true) && saveAccountHelper.getflagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), acctAgentAry[position].getAcctNum())) {
					holder.adapter_servicelist_star.setVisibility(View.VISIBLE);
				} else {
					holder.adapter_servicelist_star.setVisibility(View.GONE);
				}

				if (lnttAgent.getLnttSrvNum().equalsIgnoreCase(acctAgentAry[position].getSrvNum()) && lnttAgent.getLnttCra().getISubnRec().acctNum.equalsIgnoreCase(acctAgentAry[position].getAcctNum())){
					if (acctAgentAry[position].getLob().equals(SubnRec.LOB_LTS) && !(acctAgentAry[position].getLtsType() == R.string.CONST_LTS_FIXEDLINE || acctAgentAry[position].getLtsType() == R.string.CONST_LTS_EYE)) {
						// Filtered disable to LineTest LtsType, Only allow Fixedline & Eye to LineTest
						holder.adapter_servicelist_lnttind.setVisibility(View.GONE);
					} else if (!"".equalsIgnoreCase(lnttAgent.getEndTimestamp()) && Utils.isExpiredLnttResult(context, lnttAgent.getEndTimestamp())) {
						// Check Expired Line Test Result, after Line Test is completed
						holder.adapter_servicelist_lnttind.setVisibility(View.GONE);
						Utils.clearLnttService(context);
					} else {
						if (!"".equalsIgnoreCase(lnttAgent.getResultMsg())) {
							holder.adapter_servicelist_lnttind.setImageResource(R.drawable.icon_linetest_warning);
						} else {
							if (!"".equalsIgnoreCase(lnttAgent.getEndTimestamp())) {
								String lnttIndd = "";
								// Show the Lntt Indicator if LineTest is success
								//If both indicator is false show warning icon
								if (lnttAgent.getLnttCra().isIBBNwInd()) {
									lnttIndd = lnttAgent.getLnttCra().getOLtrsRec().modem;
								} else if (lnttAgent.getLnttCra().isIFixLnInd()) {
									lnttIndd = lnttAgent.getLnttCra().getOLtrsRec().voice;
								}								
								// Display Modem Result
								if (lnttIndd.equals(LtrsRec.RLT_GOOD)) {
									// Show result image : IMG_MANG
									holder.adapter_servicelist_lnttind.setImageResource(R.drawable.icon_linetest_success);
								} else if (lnttIndd.equals(LtrsRec.RLT_OUT_SYNC)) {
									// Show result image : IMG_MANR;
									holder.adapter_servicelist_lnttind.setImageResource(R.drawable.icon_linetest_fail);
								} else {
									// Show result image : IMG_MANY;
									holder.adapter_servicelist_lnttind.setImageResource(R.drawable.icon_linetest_warning);
								}
							} else {
								//								holder.adapter_servicelist_lnttind.setBackgroundResource(R.drawable.icon_linetest_progress_anim);
								//								final AnimationDrawable animationDrawable = (AnimationDrawable)holder.adapter_servicelist_lnttind.getBackground();
								holder.adapter_servicelist_lnttind.setImageResource(R.drawable.icon_linetest_progress_anim);
								final AnimationDrawable animationDrawable = (AnimationDrawable)holder.adapter_servicelist_lnttind.getDrawable();

								//								timer.cancel();
								//								timer.purge();
								if (!isStartedAnimationIcon) {
									holder.adapter_servicelist_lnttind.post(
											new Runnable(){
												@Override
												public void run() {
													animationDrawable.start();
													isStartedAnimationIcon = true;
												}
											});
									//									try {
									//										timer = new Timer();
									//										timer.schedule(new TimerTask() {
									//											@Override
									//											public void run() {
									//												if (animationDrawable != null) {
									//													animationDrawable.start();
									//													isStartedAnimationIcon = true;
									//												}
									//											}
									//										}, 100);
									//									} catch (Exception e) {
									//										e.printStackTrace();
									//										timer.cancel();
									//										timer.purge();
									//									}
								}
							}
						}
						holder.adapter_servicelist_star.setVisibility(View.GONE);
						holder.adapter_servicelist_lnttind.setVisibility(View.VISIBLE);
					}
				} else {
					holder.adapter_servicelist_lnttind.setVisibility(View.GONE);
				}			


				// CSP-CR2013011 - Adding IDD service & Onecall at CS Portal
				if (acctAgentAry[position].getLob().equals(SubnRec.LOB_LTS) && acctAgentAry[position].isLive()) {
					holder.adapter_servicelist_header.setText(holder.adapter_servicelist_header.getText() + "  ");
					holder.adapter_servicelist_ltslabel.setVisibility(View.VISIBLE);
					holder.adapter_servicelist_ltslabel.setTextColor(context.getResources().getColor(R.color.hkt_txtcolor_grey));
					//					holder.adapter_servicelist_ltslabelImage.setVisibility(View.VISIBLE);
					switch (acctAgentAry[position].getLtsType()) {
					case R.string.CONST_LTS_FIXEDLINE:
						//							holder.adapter_servicelist_ltslabelImage.setImageResource(R.drawable.logo_fixedline_residential);
						holder.adapter_servicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_fixedline));
						break;
					case R.string.CONST_LTS_EYE:
						//													if(ClnEnv.getAppLocale(context).equalsIgnoreCase("en"))
						//														holder.adapter_servicelist_ltslabelImage.setImageResource(R.drawable.logo_eye);
						//													else {
						//														holder.adapter_servicelist_ltslabelImage.setImageResource(R.drawable.logo_eye_zh);
						//													}
						holder.adapter_servicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_eye));
						break;
					case R.string.CONST_LTS_IDD0060:
						//							holder.adapter_servicelist_ltslabelImage.setImageResource(R.drawable.logo_fixedline_0060);
						holder.adapter_servicelist_ltslabel.setText( Utils.getString(context ,R.string.myhkt_lts_idd0060));
						break;
					case R.string.CONST_LTS_CALLINGCARD:
						holder.adapter_servicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_callingcard));
						break;
					case R.string.CONST_LTS_ONECALL:
						//							holder.adapter_servicelist_ltslabelImage.setImageResource(R.drawable.logo_fixedline_onecall);
						holder.adapter_servicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_onecall));
						break;
					case R.string.CONST_LTS_ICFS:
						holder.adapter_servicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_icfs));
						break;
					default:
						holder.adapter_servicelist_ltslabel.setVisibility(View.GONE);
						break;
					}
				} else {
					holder.adapter_servicelist_ltslabel.setText("");
					holder.adapter_servicelist_ltslabel.setVisibility(View.GONE);
					holder.adapter_servicelist_ltslabelImage.setVisibility(View.GONE);
				}
			} else {
				// zombie account
				holder.adapter_servicelist_ltslabel.setVisibility(View.GONE);
				holder.adapter_servicelist_ltslabelImage.setVisibility(View.GONE);
				holder.adapter_servicelist_header.setText(String.format("*%s", acctAgentAry[position].getAcctNum()+context.getString(R.string.LABEL_EXPIRED)));
			}

//			if(isZombie(position)){
//				holder.adapter_servicelist_ltslabel.setVisibility(View.VISIBLE);
//				holder.adapter_servicelist_ltslabel.setText(context.getString(R.string.LABEL_EXPIRED));
//			}
			//onclick listeners for each view
			holder.adapter_servicelist_edit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					callback.displayEditDialog(Utils.getString(context, R.string.MYMOB_PLZ_INPUT_ALIAS), position);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

		return convertView;
	}

	public final String getAlias(int position) {
		if (position >= acctAgentAry.length) { return null; }
		return acctAgentAry[position].getAlias();
	}

	public final static class ViewHolder {
		ImageView	adapter_servicelist_logo;
		TextView	adapter_servicelist_header_alias;
		TextView	adapter_servicelist_header;
		TextView	adapter_servicelist_ltslabel;
		ImageView 	adapter_servicelist_ltslabelImage;
		ImageView	adapter_servicelist_star;
		ImageView	adapter_servicelist_lnttind;
		Button 		adapter_servicelist_edit;
	}

	List rAcctAgentLst;
	//There are two list:
	//SubnRecAry (Active List)
	//S
	//
	//		if (acctAgentAry[position].acctX == -1) {
	// the requested item is from SubnRecAry (Active)
	// the requested item is from AcctAry (Zombie)
	// Return type is Account
	private final void prepareAcctAgent() {
		AcctAgent rAcctAgent;

		int rx, ri, rl, rlz;

		rAcctAgentLst = new ArrayList();

		// copy all SubnRec to acctAgent, but add ASSOC AcctAgent to list ONLY
		rl = ClnEnv.getQualSvee().getSubnRecAry().length;

		//live accounts
		for (rx = 0; rx < rl; rx++) {
			rAcctAgent = new AcctAgent();
			rAcctAgent.setLob(ClnEnv.getQualSvee().getSubnRecAry()[rx].lob);
			rAcctAgent.setSrvNum(ClnEnv.getQualSvee().getSubnRecAry()[rx].srvNum);
			rAcctAgent.setCusNum(ClnEnv.getQualSvee().getSubnRecAry()[rx].cusNum);
			rAcctAgent.setAcctNum(ClnEnv.getQualSvee().getSubnRecAry()[rx].acctNum);
			rAcctAgent.setSrvId(ClnEnv.getQualSvee().getSubnRecAry()[rx].srvId);
			rAcctAgent.setSysTy(ClnEnv.getQualSvee().getSubnRecAry()[rx].systy);
			rAcctAgent.setSubnRecX(rx);
			rAcctAgent.setAcctX(-1);
			rAcctAgent.setLobType(Utils.getLobType(rAcctAgent.getLob()));
			rAcctAgent.setAlias(ClnEnv.getQualSvee().getSubnRecAry()[rx].alias.trim());
			rAcctAgent.setLive(true);
			rAcctAgent.setAssoc("Y".equalsIgnoreCase(ClnEnv.getQualSvee().getSubnRecAry()[rx].assoc));
			rAcctAgent.setSubnRec(ClnEnv.getQualSvee().getSubnRecAry()[rx]);

			// CSP-CR2013011 - Adding IDD service & Onecall at CS Portal
			if (rAcctAgent.getLob().equals(SubnRec.LOB_LTS)) {
				rAcctAgent.setLtsType(Utils.getLtsSrvType(ClnEnv.getQualSvee().getSubnRecAry()[rx].tos, ClnEnv.getQualSvee().getSubnRecAry()[rx].eyeGrp, ClnEnv.getQualSvee().getSubnRecAry()[rx].priMob));
				//rAcctAgent.ltsType = Utils.getLtsSrvTypeDemo(ClnEnv.getQualCust().getAssocSubnRecAry()[rx].srv_num);
			}

			//			if (rAcctAgent.isAssoc()) {
			if(isIDD) {
				//For IDD Service List
				//One Call, ICFS, and Calling Card are excluded from the service list
				if (rAcctAgent.getLtsType() == R.string.CONST_LTS_FIXEDLINE || rAcctAgent.getLtsType() == R.string.CONST_LTS_IDD0060 ||
						rAcctAgent.getLtsType() == R.string.CONST_LTS_EYE)
					if (rAcctAgent.isAssoc())
						rAcctAgentLst.add(rAcctAgent);
			} else {
				//For Original Service List	
				if (rAcctAgent.isAssoc())
					rAcctAgentLst.add(rAcctAgent);
			}
			//			}

		}

		if (!isIDD) {
			/* Copy all Zombie Acct to AcctAgentAry [For Original Service List]*/
			rl = ClnEnv.getQualSvee().getZmSubnRecAry().length;

			for (rx = 0; rx < rl; rx++) {
				rAcctAgent = new AcctAgent();
				rAcctAgent.setLob(ClnEnv.getQualSvee().getZmSubnRecAry()[rx].deriveAcct().getLob());
                rAcctAgent.setSrvNum("");
				rAcctAgent.setCusNum(ClnEnv.getQualSvee().getZmSubnRecAry()[rx].deriveAcct().getCusNum());
				rAcctAgent.setAcctNum(ClnEnv.getQualSvee().getZmSubnRecAry()[rx].deriveAcct().getAcctNum());
				rAcctAgent.setSrvId("");
				rAcctAgent.setSysTy(ClnEnv.getQualSvee().getZmSubnRecAry()[rx].deriveAcct().getSysTy());
				rAcctAgent.setLive(false);
				rAcctAgent.setSubnRecX(-1);
				rAcctAgent.setAcctX(rx);
				rAcctAgent.setLobType(Utils.getLobType(rAcctAgent.getLob()));
				//					rAcctAgent.setAlias(ClnEnv.getQualSvee().getZmSubnRecAry()[rx].alias.trim());
//				rAcctAgent.setAssoc("Y".equalsIgnoreCase(ClnEnv.getQualSvee().getZmSubnRecAry()[rx].assoc) ? true : false);
				rAcctAgent.setSubnRec(ClnEnv.getQualSvee().getZmSubnRecAry()[rx]);

//				if (rAcctAgent.isAssoc())
					rAcctAgentLst.add(rAcctAgent);
			}
		}
		acctAgentAry = (AcctAgent[]) rAcctAgentLst.toArray(new AcctAgent[0]);

		//show remark if zombie account exists
		checkZombie:
			for (int i = 0; i < acctAgentAry.length; i++) {
				if (!acctAgentAry[i].isLive()) {
					callback.showRemark(true);
					break checkZombie;
				}
			}

		//Sort the list
		Arrays.sort(acctAgentAry, new Comparator<AcctAgent>() {
			public int compare(AcctAgent rA, AcctAgent rB) {
				int rLiveA;
				int rLiveB;
				int rx, ri, rl;

				rLiveA = (rA.isLive()) ? 0 : 1;
				rLiveB = (rB.isLive()) ? 0 : 1;

				if (getOrder(rA) == getOrder(rB)) {
					rx = 0;
				} else if (getOrder(rA)> getOrder(rB)) {
					rx = 1;
				} else {
					rx = -1;
				}

				if (rx == 0) rx = rLiveA - rLiveB;
				if (rx == 0) {
					if (rA.isLive()) {
						if (rx == 0) rx = rA.getSrvNum().compareTo(rB.getSrvNum());
					}
				}
				if (rx == 0) rx = rA.getCusNum().compareTo(rB.getCusNum());
				if (rx == 0) rx = rA.getAcctNum().compareTo(rB.getAcctNum());
				if (rx == 0) rx = rA.getSrvId().compareTo(rB.getSrvId());

				return (rx);
			}
		});
	}

	// Use for ordering
	private final int getOrder(AcctAgent accAgent){
		String lob = accAgent.getLob();
		int ltsType = accAgent.getLtsType();
		//with wild card lob
		if (SubnRec.LOB_LTS.equalsIgnoreCase(lob)) {
			switch(ltsType) {
			case R.string.CONST_LTS_FIXEDLINE:
				return 0;

			case R.string.CONST_LTS_EYE:
				return 0;

			case R.string.CONST_LTS_IDD0060:
				return 1;
				
			case R.string.CONST_LTS_CALLINGCARD:
				return 1;

			case R.string.CONST_LTS_ONECALL:
				return 0;

			case R.string.CONST_LTS_ICFS:
				return 1;
			default:
				return 0;
			}
		}
		else if (SubnRec.LOB_101.equalsIgnoreCase(lob)) return 2;
		else if (SubnRec.LOB_IOI.equalsIgnoreCase(lob)) return 2;
		else if (SubnRec.LOB_MOB.equalsIgnoreCase(lob)) return 3;
		else if (SubnRec.LOB_O2F.equalsIgnoreCase(lob)) return 5;
		else if (SubnRec.LOB_PCD.equalsIgnoreCase(lob)) return 6;
		else if (SubnRec.LOB_TV.equalsIgnoreCase(lob)) return 7;
		else return 7;		
	}
}