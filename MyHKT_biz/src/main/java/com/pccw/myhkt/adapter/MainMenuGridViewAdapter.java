package com.pccw.myhkt.adapter;

/************************************************************************
File       : IDDDestListViewAdapter.java
Desc       : Destination list for IDD Rates search function
Name       : IDDDestListViewAdapter
Created by : Derek Tsui
Date       : 23/01/2014

Change History:
Date       Modified By        	Description
---------- ----------------   	-------------------------------
23/01/2014 Derek Tsui       	- First draft

*************************************************************************/

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.HomeImageButton;
import com.pccw.myhkt.model.HomeButtonItem;

public class MainMenuGridViewAdapter extends BaseAdapter {
	
	public 		  Context				context;
	public 		  LayoutInflater		inflater;
	public 		  List<HomeButtonItem>	homeItems	;
	private final boolean				isZh;
	private 	  int					itemHeight;
	private 	  Boolean 				isPremier = false;
	//Filterable
	
	
	public Boolean getIsPermier() {
		return isPremier;
	}

	public void setIsPerimer(Boolean isPerimer) {
		this.isPremier = isPerimer;
	}

	public MainMenuGridViewAdapter(Context context, List<HomeButtonItem>homeItems, int itemHeight) {
		super();
		this.context = context;
		this.itemHeight = itemHeight;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.homeItems = homeItems;

		// Language indicator
        isZh = "zh".equalsIgnoreCase(Utils.getString(context, R.string.myhkt_lang));
		
		isPremier = ClnEnv.getSessionPremierFlag();
	}
	
	@Override
	public int getCount() {
		if (homeItems == null) return 0;
		return homeItems.size();
	}

	@Override
	public Object getItem(int position) {
		if (position >= 0 && position < homeItems.size()) {
			return homeItems.get(position);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		HomeImageButton homeButton = new HomeImageButton(parent.getContext());
		homeButton.initViews(parent.getContext(), homeItems.get(position).image, homeItems.get(position).text, homeItems.get(position).textSize, homeItems.get(position).txtColor, homeItems.get(position).bgcolor, itemHeight, isZh, isPremier);
//		try {
//			if (convertView == null) {
//				holder = new ViewHolder();
//				convertView = inflater.inflate(R.layout.adapter_idddestlist, null);
//	
//				holder.adapter_idddestlist_right_circle = (ImageView) convertView.findViewById(R.id.adapter_idddestlist_right_circle);
//				holder.adapter_idddestlist_dest = (TextView) convertView.findViewById(R.id.adapter_idddestlist_dest);
//				holder.adapter_idddestlist_destext = (TextView) convertView.findViewById(R.id.adapter_idddestlist_destext);
//	
//				convertView.setTag(holder);
//			} else {
//				holder = (ViewHolder) convertView.getTag();
//			}
//			
//				holder.adapter_idddestlist_dest.setText((isZh) ? destination[position].chiDestName : destination[position].engDestName);
//				holder.adapter_idddestlist_destext.setText((isZh) ? destination[position].chiDestNameExt : destination[position].engDestNameExt);
//				holder.adapter_idddestlist_destext.setVisibility(View.VISIBLE);
//				
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		homeButton.setEnabled(homeItems.get(position).image == -1);
		homeButton.setFocusable(homeItems.get(position).image == -1);
		homeButton.setEnabled(homeItems.get(position).image == -1);
		return homeButton;
	}
	
}
