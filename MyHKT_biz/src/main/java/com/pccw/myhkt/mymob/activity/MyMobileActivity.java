package com.pccw.myhkt.mymob.activity;

import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.mymob.MyMobileAccountHelper;
import com.pccw.myhkt.mymob.fragment.BaseMyMobFragment.OnMyMobListener;
import com.pccw.myhkt.mymob.fragment.MyMobAddAccFragment;
import com.pccw.myhkt.mymob.fragment.MyMobServiceListFragment;
import com.pccw.myhkt.mymob.model.EditCra;

public class MyMobileActivity extends BaseActivity implements OnMyMobListener{
	private AAQuery 	aq;
	private boolean 	debug = false;
	
	// Fragment
	private FragmentManager				fragmentManager;
//	protected int							activeSubview		= R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC;	// Initial default subview
	protected int							activeSubview		= R.string.CONST_SELECTEDFRAG_MYMOB_SERVICELIST;	// Initial default subview

	private Fragment						myMobAddAccFragment			= null;
	private Fragment						myMobServiceListFragment    = null;
	
	private AddOnCra addOnCra = null;
	
	private boolean isAutoLogin = false;
	private AcctAgent selectedAcctAgent = null;
	private EditCra editCra = null;
	private boolean isEditMode = false;
	
	private AcctAgent					recallAcctAgent         = null;
	private String						apiKey; 	//record apiKey for login redirection (asyncId)
	private boolean					isSavePwd 				= false; //record savepassword setting for login redirection
	private boolean					isLogin 				= false; //redirect that came from my mob service list login call
	private boolean					pwdDlgIsSavePwd			= false;
	private String						pwdDlgPwd				= "";
	private boolean					isPwHintUpdated			= false;
	private SubnRec					ioSubscriptionRec 		= null;
	private AddOnCra 				addonCra;
	
	private String 					actionTy = null;
	private String 					TAG = "MyMobileActivity";
	
	@Override
	public final void onCreate(Bundle savedInstanceState) {
		isLiveChatShown = false;
		ClnEnv.setMyMobFlag(true);
		super.onCreate(savedInstanceState);	
		debug = getResources().getBoolean(R.bool.DEBUG);
		setActiveSubview(R.string.CONST_SELECTEDFRAG_MYMOB_SERVICELIST);
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
//			parentOnClickId = bundle.getInt("CLICKBUTTON", 0);
			recallAcctAgent = (AcctAgent) bundle.getSerializable("SELECTACCTAGENT");
			actionTy = bundle.getString("ACTIONTY");
			isSavePwd = bundle.getBoolean("ISSAVEPWD");
			isLogin = bundle.getBoolean("ISMYMOBLOGIN");

			if (recallAcctAgent != null) setRecallAcctAgent(recallAcctAgent);
//			if (actionTy != null) setActionTy(actionTy);		
			if (debug) Log.i(TAG, "Start Type " + actionTy);
			activeSubview = isLogin ? R.string.CONST_SELECTEDFRAG_MYMOB_SERVICELIST : R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC;
			
			if (isLogin) {
				setAutoLogin(true);
			}
		
			setSavePwd(isSavePwd);
		}
		
		
		setContentView(R.layout.activity_mymobile);
	}	
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		me = this;
		Bundle bundle = intent.getExtras();
		if (bundle != null) {
//			localSessTok = ClnEnv.getSessTok();
			recallAcctAgent = (AcctAgent) bundle.getSerializable("SELECTACCTAGENT");
			actionTy = bundle.getString("ACTIONTY");
			isSavePwd = bundle.getBoolean("ISSAVEPWD");
			if (debug) Log.i(TAG, "intent Type " + actionTy);
			if (recallAcctAgent != null) setRecallAcctAgent(recallAcctAgent);
			if (actionTy != null) setActionTy(actionTy);
			setSavePwd(isSavePwd);
		}			
	}
	
	@Override
	protected final void onStart() {
		super.onStart();	
		
		//Screen Tracker
		displaySubview();	
	}
	
	// Android Device Back Button Handling
	@Override
	public final void onBackPressed() {
		setEditMode(false);
		if (activeSubview == R.string.CONST_SELECTEDFRAG_MYMOB_SERVICELIST) {
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		} else if (activeSubview == R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC) {
			if (isEmptyList()) {
				finish();
				overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			} else {
				setActiveSubview(R.string.CONST_SELECTEDFRAG_MYMOB_SERVICELIST);
				fragmentBack();
			}
		} else {
			fragmentBack();
		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		ClnEnv.setMyMobFlag(true);
		moduleId = getResources().getString(R.string.MODULE_MM_MAIN);
	}
	
	public final void initUI2() {
		fragmentManager = getSupportFragmentManager();
		aq = new AAQuery(this);
		
		//navbar style
		aq.navBarBaseLayout(R.id.navbar_base_layout);
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.MYMOB_NAV_TITLE), R.color.hkt_textcolor_orange);
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);
		
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
	}
	
	
	public final void displaySubview() {
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		Fragment frag = fragmentManager.findFragmentById(R.id.mymob_commonview);
		transaction.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out, R.anim.left_slide_in, R.anim.right_slide_out);
			switch (activeSubview) {
				case R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC:
					if (!(frag instanceof MyMobAddAccFragment)) {
						myMobAddAccFragment = new MyMobAddAccFragment();
						fragmentManager.findFragmentById(R.id.mymob_commonview);					
						transaction.replace(R.id.mymob_commonview, myMobAddAccFragment);
						transaction.addToBackStack(null);					
					}
					break;
				case R.string.CONST_SELECTEDFRAG_MYMOB_SERVICELIST:
					myMobServiceListFragment = new MyMobServiceListFragment();
					transaction.replace(R.id.mymob_commonview, myMobServiceListFragment);
					transaction.addToBackStack(null);
					break;
			}
			Utils.closeSoftKeyboard(this);
		transaction.commit();
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			setEditMode(false);
			if (activeSubview == R.string.CONST_SELECTEDFRAG_MYMOB_SERVICELIST) {
				finish();
				overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			} else if (activeSubview == R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC) {
				if (isEmptyList()) {
					finish();
					overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
				} else {
					setActiveSubview(R.string.CONST_SELECTEDFRAG_MYMOB_SERVICELIST);
					fragmentBack();
				}
			} else {
				fragmentBack();
			}
			break;
		}
	}
	
	public boolean isEmptyList() {
		MyMobileAccountHelper myMobileAccountHelper = MyMobileAccountHelper.getInstance(me);
		int sizetest = myMobileAccountHelper.getList().size();
		try {
            return !ClnEnv.isMyMobMyAccExist() && sizetest == 0;
		} catch (Exception e) {
			e.printStackTrace();
			return true;
		}
	}
	
	@Override
	public void setActiveSubview(int index) {
		activeSubview = index;
	}

	@Override
	public int getActiveSubview() {
		return activeSubview;
	}

	@Override
	public void popBackStack() {
		fragmentManager.popBackStack();
	}

	@Override
	public void fragmentBack() {
		setEditMode(false);
		if (fragmentManager.getBackStackEntryCount() > 1) {
			popBackStack();
		} else {
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		}
	}
	
	public void showLiveChatIcon() {
		isLiveChatShown = true;
		initNavBar();
	}
	
	public void hideLiveChatIcon() {
		isLiveChatShown = false;
		initNavBar();
	}


	@Override
	public boolean isAutoLogin() {
		return isAutoLogin;
	}
	@Override
	public void setAutoLogin(boolean isAutoLogin) {
		this.isAutoLogin = isAutoLogin;
	}
	@Override
	public void setSelectedAcctAgent(AcctAgent acctAgent) {
		this.selectedAcctAgent = acctAgent;
	}

	@Override
	public AcctAgent getSelectedAcctAgent() {
		return selectedAcctAgent;
	}
	
	@Override
	public AddOnCra getAddOnCra() {
		return addOnCra;
	}
	
	@Override
	public void setAddOnCra(AddOnCra addOnCra) {
		this.addOnCra = addOnCra;
	}
	@Override
	public EditCra getEditCra() {
		return editCra;
	}
	@Override
	public void setEditCra(EditCra editCra) {
		this.editCra = editCra;
	}
	@Override
	public boolean isEditMode() {
		return isEditMode;
	}
	@Override
	public void setEditMode(boolean isEditMode) {
		this.isEditMode = isEditMode;
	}
	
	public AcctAgent getRecallAcctAgent() {
		return recallAcctAgent;
	}

	public void setRecallAcctAgent(AcctAgent recallAcctAgent) {
		this.recallAcctAgent = recallAcctAgent;
	}

	public boolean isSavePwd() {
		return isSavePwd;
	}

	public void setSavePwd(boolean isSavePwd) {
		this.isSavePwd = isSavePwd;
	}

	public boolean isPwdDlgIsSavePwd() {
		return pwdDlgIsSavePwd;
	}

	public void setPwdDlgIsSavePwd(boolean pwdDlgIsSavePwd) {
		this.pwdDlgIsSavePwd = pwdDlgIsSavePwd;
	}

	public String getPwdDlgPwd() {
		return pwdDlgPwd;
	}

	public void setPwdDlgPwd(String pwdDlgPwd) {
		this.pwdDlgPwd = pwdDlgPwd;
	}

	public boolean isPwHintUpdated() {
		return isPwHintUpdated;
	}

	public void setPwHintUpdated(boolean isPwHintUpdated) {
		this.isPwHintUpdated = isPwHintUpdated;
	}

	public String getActionTy() {
		return actionTy;
	}

	public void setActionTy(String actionTy) {
		this.actionTy = actionTy;
	}

	public SubnRec getIoSubscriptionRec() {
		return ioSubscriptionRec;
	}

	public void setIoSubscriptionRec(SubnRec ioSubscriptionRec) {
		this.ioSubscriptionRec = ioSubscriptionRec;
	}

	@Override
	public void killActivity() {
		finish();
	}

	@Override
	public void onActivityBackPress() {
		this.onBackPressed();
		
	}


}
