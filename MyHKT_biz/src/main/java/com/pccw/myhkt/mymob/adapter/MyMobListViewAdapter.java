package com.pccw.myhkt.mymob.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;

import android.app.Activity;
import android.content.Context;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.swipelistview.SwipeListView;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.mymob.MyMobileAccountHelper;

public class MyMobListViewAdapter extends BaseAdapter {
	boolean					debug			= false;

	// ServiceListArray
	private AcctAgent		acctAgentAry[]	= null;

	public static final int TYPE_ITEM = 0;
	public static final int TYPE_SEPARATOR = 1;
	private ArrayList<String> mData = new ArrayList<String>();
	private TreeSet<Integer> sectionHeader = null;
	
//	private int myMobListCount	=0;
//	private int myAcctListCount = 0;
	
	public Activity			context;
	public LayoutInflater	inflater;
	private MyMobileAccountHelper myMobileAccountHelper;
	private SaveAccountHelper saveAccountHelper;
	
	//Rex test alias START
//	private String			mymob_alias[]			= null;

	// CallBack
	private OnMyMobListAdapterListener			callback;
	
	public interface OnMyMobListAdapterListener  {
		void displayAliasDialog(String message, final int position);
		void editAccountInfo(String alias, String mob_num, String password, int pos);
	}
	
	public MyMobListViewAdapter(Fragment frag, SwipeListView mSwipeListView) throws Exception {
//	public MyMobListViewAdapter(Activity context, int parentOnClickId, SwipeListView mSwipeListView) throws Exception {
		super();

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			callback = (OnMyMobListAdapterListener) frag;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement OnMyMobHKTListAdapterListener");
		}
		
		this.context = frag.getActivity();
//		this.mSwipeListView = mSwipeListView ;
				
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		debug = context.getResources().getBoolean(R.bool.DEBUG);
		myMobileAccountHelper = MyMobileAccountHelper.getInstance(context);
		prepareAcctAgent();
	}

	public final int getCount() {
		if (acctAgentAry == null ) { return 0; }
		return acctAgentAry.length ;
	}
	
	public AcctAgent[] getAcctAgentList () {	
		return acctAgentAry;
	}
	
	public void addItem(final String item) {
		mData.add(item);
		notifyDataSetChanged();
	}

	public void addSectionHeaderItem(final String item) {
		mData.add(item);
		sectionHeader.add(mData.size() - 1);
		notifyDataSetChanged();
	}

	@Override
	public int getItemViewType(int position) {
		return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}
	
	public final String getAlias(int position) {
		if (position >= acctAgentAry.length) { return null; }
		return acctAgentAry[position].getAlias();
	}
	
	// We need to return the correct Account object from either assocSubnAry / AcctAry
	public final Object getItem2(int position) {
		if (position >= acctAgentAry.length) { return null; }

		return acctAgentAry[position];
	}
	
	public final Object getItem(int position) {
//		if (position >= acctAgentAry.length) { return null; }
		
		// 1<= position <= SubscriptionRec length
		// the requested item is from MyAccount (Active) 
		// Return type is SubscriptionRec
//		if (sectionHeader.contains(position)) { 
//			return null;
//		} else if (acctAgentAry[position].isMyMob()) {
//			return acctAgentAry[position];
//		} else {
//			return ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[acctAgentAry[position].getSubnRecX()];
//			return ClnEnv.getQualCust().getAssocSubnRecAry()[acctAgentAry[position].getSubnRecX()];
//		}
		return acctAgentAry[position];
	}
	
	public final Object getSubnRecItem(int position) {
		return acctAgentAry[position].getSubnRec();
	}
	

	// We need to know if the requested position is from assocSubnAry / AcctAry
	public final boolean isItemAssocSubnAry(int position) {
		return !acctAgentAry[position].isMyMob();
//		if (position >= acctAgentAry.length) {
//			
//			return false;
//		} else {
//			return (acctAgentAry[position].acctX == -1);
//		}
	}
	
	public final boolean hasMyMobAcct() {
		for (int i=0 ; i < acctAgentAry.length ; i++) {
			if (getItemViewType(i) == TYPE_ITEM && acctAgentAry[i].isMyMob()) {
				return true;
			}
		}
		
		return false;
	}

	public final long getItemId(int position) {
		return position;
	}

	public final String getLOB(int position) {
		if (position >= acctAgentAry.length) { return null; }
		if(debug) Log.d("MyMobListAdadpter", "selected lob:"+acctAgentAry[position].getLob());
		return acctAgentAry[position].getLob();
	}

	public final View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		holder = new ViewHolder();
		int rowType = getItemViewType(position);

		if (convertView == null) {
			holder = new ViewHolder();
			switch (rowType) {
				case TYPE_ITEM:
					convertView = inflater.inflate(R.layout.adapter_mymob_accountlist, null);
					holder.adapter_accountlist_alias = convertView.findViewById(R.id.adapter_mymob_accountlist_alias);
					holder.adapter_accountlist_mobnum = convertView.findViewById(R.id.adapter_mymob_accountlist_mobnum);
					holder.adapter_accountlist_logo = convertView.findViewById(R.id.adapter_mymob_accountlist_logo);
					holder.adapter_accountlist_star = convertView.findViewById(R.id.adapter_mymob_accountlist_star);
					holder.adapter_accountlist_frontviewLayout = convertView.findViewById(R.id.adapter_mymob_accountlist_front);
					// Set SwipeListView
					holder.adapter_accountlist_btn_edit = convertView.findViewById(R.id.adapter_mymob_accountlist_edit);
					break;
				case TYPE_SEPARATOR:
					convertView = inflater.inflate(R.layout.view_mymob_list_header, null);
					convertView.setClickable(false);
					break;
			}
	
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		switch (rowType) {
			case TYPE_ITEM:
				holder.adapter_accountlist_star.setVisibility(View.GONE);
				LinearLayout.LayoutParams params = (LayoutParams) holder.adapter_accountlist_btn_edit.getLayoutParams();
				int deviceWidth = context.getResources().getDisplayMetrics().widthPixels;
				params.width = deviceWidth * 1 / 6;
				Gson gson = new Gson();
				if (debug) Log.i("ServiceList", params.width+"");
				if (debug) Log.i("ServiceList1",gson.toJson(params));
				holder.adapter_accountlist_btn_edit.setLayoutParams(params);
				// Set layout for MyMobile item
				if (acctAgentAry[position].isMyMob()) {
					String lob = acctAgentAry[position].getLob();
					if (lob.equals(SubnRec.LOB_101) || lob.equals(SubnRec.LOB_IOI)) {
						holder.adapter_accountlist_logo.setImageResource(R.drawable.lob_1010_plain);
					} else {
						holder.adapter_accountlist_logo.setImageResource(R.drawable.lob_csl_plain);
					}

					holder.adapter_accountlist_btn_edit.setBackgroundColor(context.getResources().getColor(R.color.mymob_orange));
					holder.adapter_accountlist_btn_edit.setText(R.string.MYMOB_BTN_EDIT);
//					holder.adapter_accountlist_btn_edit.setOnTouchListener(new CCMCHoverButton(context, R.string.CONST_BTN_EDIT, false));
					holder.adapter_accountlist_btn_edit.setTag(position);
					holder.adapter_accountlist_btn_edit.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							callback.editAccountInfo(acctAgentAry[position].getAlias(), acctAgentAry[position].getSrvNum(), acctAgentAry[position].getPassword(), position);
						}
					});

					//hide alias if empty
					if (acctAgentAry[position].getAlias() != null && !"".equalsIgnoreCase(acctAgentAry[position].getAlias())) {
						holder.adapter_accountlist_alias.setVisibility(View.VISIBLE);
						holder.adapter_accountlist_alias.setText(acctAgentAry[position].getAlias());
					} else {
						holder.adapter_accountlist_alias.setText("");
						holder.adapter_accountlist_alias.setVisibility(View.GONE);
					}
					holder.adapter_accountlist_mobnum.setText(acctAgentAry[position].getSrvNum());
				} else {
					// Set layout for MyAccount item
					holder.adapter_accountlist_btn_edit.setBackgroundColor(context.getResources().getColor(R.color.btn_alias));
					holder.adapter_accountlist_btn_edit.setText(R.string.MYMOB_BTN_ALIAS);
//					holder.adapter_accountlist_btn_edit.setOnTouchListener(new CCMCHoverButton(context, R.string.CONST_BTN_ALIAS, false));
					holder.adapter_accountlist_btn_edit.setTag(position);
					
					if (acctAgentAry[position].getAlias() != null && !"".equalsIgnoreCase(acctAgentAry[position].getAlias())) {
						holder.adapter_accountlist_alias.setVisibility(View.VISIBLE);
						holder.adapter_accountlist_alias.setText(acctAgentAry[position].getAlias());
					} else {
						holder.adapter_accountlist_alias.setText("");
						holder.adapter_accountlist_alias.setVisibility(View.GONE);
					}
					
					if (acctAgentAry[position].getLob().equals(SubnRec.LOB_101)) {
						holder.adapter_accountlist_logo.setImageResource(R.drawable.lob_1010_plain);
					} else {
						holder.adapter_accountlist_logo.setImageResource(R.drawable.lob_csl_plain);
					}
					holder.adapter_accountlist_mobnum.setText(acctAgentAry[position].getSrvNum());
					

					
					saveAccountHelper = SaveAccountHelper.getInstance(context);
					try {
						
						//if (!acctAgentAry[position].lob.equals(SubscriptionRec.LOB_101) && !acctAgentAry[position].lob.equals(SubscriptionRec.LOB_O2F) && ClnEnv.getPref(context.getApplicationContext(), context.getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true) && saveAccountHelper.getflagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), acctAgentAry[position].acctNum)) {
						if (ClnEnv.getPref(context.getApplicationContext(), context.getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true) && saveAccountHelper.getflagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), acctAgentAry[position].getAcctNum())) {	
							holder.adapter_accountlist_star.setVisibility(View.VISIBLE);
						} else {
							holder.adapter_accountlist_star.setVisibility(View.GONE);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					//onclick listeners for each view
					holder.adapter_accountlist_btn_edit.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							callback.displayAliasDialog(Utils.getString(context, R.string.MYMOB_PLZ_INPUT_ALIAS),position);
						}
					});
				}

				break;
			case TYPE_SEPARATOR:
		//		if (debug) Log.d("MyMobListAdadpter", "pos type isMMob:" + position + "/" + rowType + "/" + acctAgentAry[position].isMyMob);
				if (acctAgentAry[position].isMyMob()) {
					holder.adapter_accountlist_header = convertView.findViewById(R.id.mymob_list_headertext);
					holder.adapter_accountlist_header.setText(R.string.MYMOB_OTHERLIST_TITLE);
					holder.adapter_accountlist_header.setTextColor(context.getResources().getColor(R.color.mymob_orange));
				} else {
					//TODO: Please define the MyAccount Layout Style
				}
				break;
		}
		return convertView;
	}

	public final static class ViewHolder {
		TextView 	adapter_accountlist_header;
		ImageView	adapter_accountlist_logo;
		ImageView	adapter_accountlist_star;
		TextView	adapter_accountlist_alias;
		TextView	adapter_accountlist_mobnum;
		Button 		adapter_accountlist_btn_edit;
		RelativeLayout adapter_accountlist_frontviewLayout;
	}
	
	public int getAccountCount() {
		return acctAgentAry.length;
	}
	
	private final void prepareAcctAgent() {
		sectionHeader = new TreeSet<Integer>();
		AcctAgent rAcctAgent;
		List<AcctAgent> rAcctAgentLst;
		int rx, /*ri,*/ rl;

		rAcctAgentLst = new ArrayList<AcctAgent>();
		
		if (ClnEnv.isLoggedIn()) {
			// copy all SubnRec to acctAgent, but add ASSOC AcctAgent to list ONLY
//			rl = ClnEnv.getQualCust().getAssocSubnRecAry().length;
			rl = ClnEnv.getQualSvee().getSubnRecAry().length;
			
			for (rx = 0; rx < rl; rx++) {
				//If MyAccount has data, add header first
				// Adding MOB/NE/O2F/101 service only for MyMobile
				if ((SubnRec.LOB_MOB.equalsIgnoreCase(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].lob)) ||	
					(SubnRec.LOB_IOI.equalsIgnoreCase(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].lob)) ||
					(SubnRec.LOB_101.equalsIgnoreCase(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].lob)) ||
					(SubnRec.LOB_O2F.equalsIgnoreCase(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].lob))) {
						//header text
						if (rAcctAgentLst.size() == 0) {
							rAcctAgent = new AcctAgent();
							rAcctAgent.setHeader(true);
							rAcctAgent.setMyMob(false);
							rAcctAgentLst.add(rAcctAgent);
							sectionHeader.add(0);
						}
						
						//services
						rAcctAgent = new AcctAgent();
//						rAcctAgent.lob = ClnEnv.getQualCust().getAssocSubnRecAry()[rx].lob;
//						rAcctAgent.srvNum = ClnEnv.getQualCust().getAssocSubnRecAry()[rx].srv_num;
//						rAcctAgent.cusNum = ClnEnv.getQualCust().getAssocSubnRecAry()[rx].cus_num;
//						rAcctAgent.acctNum = ClnEnv.getQualCust().getAssocSubnRecAry()[rx].acct_num;
//						rAcctAgent.srvId = ClnEnv.getQualCust().getAssocSubnRecAry()[rx].srv_id;
//						rAcctAgent.sysTy = ClnEnv.getQualCust().getAssocSubnRecAry()[rx].systy;
//						rAcctAgent.live = true; /* Live Subscription/Account */
//						rAcctAgent.subnRecX = rx;
//						rAcctAgent.acctX = -1;
//						rAcctAgent.isMyMob = false;
//						rAcctAgent.isHeader = false;
//						rAcctAgent.lobType = getLobType(rAcctAgent.lob);
//						rAcctAgent.alias = ClnEnv.getQualCust().getAssocSubnRecAry()[rx].alias.trim();
						//derek start
						rAcctAgent.setLob(ClnEnv.getQualSvee().getSubnRecAry()[rx].lob);
						rAcctAgent.setSrvNum(ClnEnv.getQualSvee().getSubnRecAry()[rx].srvNum);
						rAcctAgent.setCusNum(ClnEnv.getQualSvee().getSubnRecAry()[rx].cusNum);
						rAcctAgent.setAcctNum(ClnEnv.getQualSvee().getSubnRecAry()[rx].acctNum);
						rAcctAgent.setSrvId(ClnEnv.getQualSvee().getSubnRecAry()[rx].srvId);
						rAcctAgent.setSysTy(ClnEnv.getQualSvee().getSubnRecAry()[rx].systy);
						rAcctAgent.setSubnRecX(rx);
						rAcctAgent.setAcctX(-1);
						rAcctAgent.setLobType(Utils.getLobType(rAcctAgent.getLob()));
						rAcctAgent.setAlias(ClnEnv.getQualSvee().getSubnRecAry()[rx].alias.trim());
						rAcctAgent.setLive(true);
						rAcctAgent.setAssoc("Y".equalsIgnoreCase(ClnEnv.getQualSvee().getSubnRecAry()[rx].assoc));
						rAcctAgent.setSubnRec(ClnEnv.getQualSvee().getSubnRecAry()[rx]);
						rAcctAgent.setMyMob(false);
						rAcctAgent.setHeader(false);
						//derek end
						if (rAcctAgent.isAssoc())
						rAcctAgentLst.add(rAcctAgent);
				}
			}
			
			acctAgentAry = rAcctAgentLst.toArray(new AcctAgent[0]);

			//Sort the list
			Arrays.sort(acctAgentAry, new Comparator<AcctAgent>() {
				public int compare(AcctAgent rA, AcctAgent rB) {
					int rLiveA;
					int rLiveB;
					int rx, ri, rl;

					if (rA.isHeader() || rB.isHeader()) {
						rx = 0;
					} else {
						rLiveA = (rA.isLive()) ? 0 : 1;
						rLiveB = (rB.isLive()) ? 0 : 1;
						
						if (getOrder(rA.getLob()) == getOrder(rB.getLob())) {
							rx = 0;
						} else if (getOrder(rA.getLob())> getOrder(rB.getLob())) {
							rx = 1;
						} else {
							rx = -1;
						}
							
						if (rx == 0) rx = rLiveA - rLiveB;
						if (rx == 0) {
							if (rA.isLive()) {
								if (rx == 0) rx = rA.getSrvNum().compareTo(rB.getSrvNum());
							}
						}
						if (rx == 0) rx = rA.getCusNum().compareTo(rB.getCusNum());
						if (rx == 0) rx = rA.getAcctNum().compareTo(rB.getAcctNum());
						if (rx == 0) rx = rA.getSrvId().compareTo(rB.getSrvId());
					}


					
					return (rx);
				}
			});
		}
		
		// Prepare MyMobile
		rAcctAgentLst = new ArrayList<AcctAgent>();
		AcctAgent[] acctAgentAryTemp = null;
		// get MyMobile account records from local DB
		ArrayList<HashMap<String, ?>> accountList = myMobileAccountHelper.getList();
				
		//If has records in local DB
		if (accountList.size() > 0) {
			Boolean isFirstItem = true;
			if (debug) Log.d("MyMobileActivity", accountList.size() + " records in DB");
			for (int i = 0; i < accountList.size(); i++) {
				Boolean isValidToAdd = true;
				//Check if MyMobile account duplicate with MyAccount ,if duplicate set addflag = 0
				if (acctAgentAry != null) {
					for (int j = 1; j < acctAgentAry.length; j++) {
						if (acctAgentAry[j].getSrvNum().equals(accountList.get(i).get("MOBNUM"))) {
							isValidToAdd = false;
						}
					}
				}
				
				if (isValidToAdd) {
					if (isFirstItem) {
						rAcctAgent = new AcctAgent();
						rAcctAgent.setHeader(true);
						rAcctAgent.setMyMob(true);
						rAcctAgentLst.add(rAcctAgent);
						sectionHeader.add((sectionHeader.size() > 0) ? acctAgentAry.length : 0);
						isFirstItem = false;
					}
					
					rAcctAgent = new AcctAgent();
//					rAcctAgent.isMyMob = true;
//					rAcctAgent.srvNum = (String) accountList.get(i).get("MOBNUM");
//					rAcctAgent.alias = (String) accountList.get(i).get("ALIAS");
//					rAcctAgent.password = (String) accountList.get(i).get("MOBPWD");
//					rAcctAgent.lob = (String) accountList.get(i).get("LOB");
//					rAcctAgent.latest_bill = (String) accountList.get(i).get("LATEST_BILL");
//					rAcctAgent.isSms = ("Y".equals((String) accountList.get(i).get("SMS_FLAG"))) ? true : false;
//					rAcctAgent.cardType = (String) accountList.get(i).get("CARDTYPE");
					
					//derek start
					rAcctAgent.setMyMob(true);
					rAcctAgent.setSrvNum((String) accountList.get(i).get("MOBNUM"));
					rAcctAgent.setAlias((String) accountList.get(i).get("ALIAS"));
					String decPassword = (String) accountList.get(i).get("MOBPWD");
					if (decPassword != null) {
						decPassword = ClnEnv.getDecString(context, rAcctAgent.getSrvNum(), decPassword);
					}
					rAcctAgent.setPassword(decPassword);
					rAcctAgent.setLob((String) accountList.get(i).get("LOB"));
					rAcctAgent.setLatest_bill((String) accountList.get(i).get("LATEST_BILL"));
					rAcctAgent.setSms(false);
					rAcctAgent.setCardType((String) accountList.get(i).get("CARDTYPE"));
					//derek end
					rAcctAgentLst.add(rAcctAgent);

				}
			}
		}
				
		/*If has records in rAcctAgentLst 
		* concat arrays with MyMobile and MyAccount 
		*/
		if (acctAgentAry == null) {
			acctAgentAry = rAcctAgentLst.toArray(new AcctAgent[0]);
		} else {
			acctAgentAryTemp = rAcctAgentLst.toArray(new AcctAgent[0]);
			acctAgentAry = Utils.concatArray(acctAgentAry, acctAgentAryTemp);
		}

		if(debug) Log.d("MyMobListAdadpter", "section 1 array size:"+(acctAgentAry.length));
	}
	
	// Use for ordering
	private final int getOrder(String lob){
		//with wild card lob
		if (SubnRec.LOB_LTS.equalsIgnoreCase(lob)) return 0;
		else if (SubnRec.LOB_101.equalsIgnoreCase(lob)) return 1;
		else if (SubnRec.LOB_IOI.equalsIgnoreCase(lob)) return 1;
		else if (SubnRec.LOB_MOB.equalsIgnoreCase(lob)) return 2;
		else if (SubnRec.LOB_O2F.equalsIgnoreCase(lob)) return 4;
		else if (SubnRec.LOB_PCD.equalsIgnoreCase(lob)) return 5;
		else if (SubnRec.LOB_TV.equalsIgnoreCase(lob)) return 6;
		else return 7;		
	}
	
	public final static int getLobType(String lob) {
		if (SubnRec.LOB_LTS.equalsIgnoreCase(lob)) return R.string.CONST_LOB_LTS;
		else if (SubnRec.LOB_101.equalsIgnoreCase(lob)) return R.string.CONST_LOB_1010;
		else if (SubnRec.LOB_IOI.equalsIgnoreCase(lob)) return R.string.CONST_LOB_IOI;
		else if (SubnRec.LOB_MOB.equalsIgnoreCase(lob)) return R.string.CONST_LOB_MOB;
		else if (SubnRec.LOB_O2F.equalsIgnoreCase(lob)) return R.string.CONST_LOB_O2F;
		else if (SubnRec.LOB_PCD.equalsIgnoreCase(lob)) return R.string.CONST_LOB_PCD;
		else if (SubnRec.LOB_TV.equalsIgnoreCase(lob)) return R.string.CONST_LOB_TV;
		else return 0;
	}

//	public final class AcctAgent implements Serializable{
//		private static final long	serialVersionUID	= 2393246116792643181L;
//		//MyAccount Key
//		private int lobType;
//		private String	lob;
//		private String	srvNum;
//		private String	cusNum;
//		private String	acctNum;
//		private String	srvId;
//		private String	sysTy;
//		private String	alias;
//		private boolean	live;
//		private int		subnRecX;
//		private int		acctX;
//		private int		ltsType;	// CR2013011
//		
//		//MyMobile key
//		private String	password;
//		private String	cardType;
//		private boolean	isHeader;
//		private boolean isMyMob = false;
//		private String latest_bill;
//		private boolean isSms = false;
//		
//		public int getLobType() {
//			return lobType;
//		}
//		public void setLobType(int lobType) {
//			this.lobType = lobType;
//		}
//		public String getLob() {
//			return lob;
//		}
//		public void setLob(String lob) {
//			this.lob = lob;
//		}
//		public String getSrvNum() {
//			return srvNum;
//		}
//		public void setSrvNum(String srvNum) {
//			this.srvNum = srvNum;
//		}
//		public String getCusNum() {
//			return cusNum;
//		}
//		public void setCusNum(String cusNum) {
//			this.cusNum = cusNum;
//		}
//		public String getAcctNum() {
//			return acctNum;
//		}
//		public void setAcctNum(String acctNum) {
//			this.acctNum = acctNum;
//		}
//		public String getSrvId() {
//			return srvId;
//		}
//		public void setSrvId(String srvId) {
//			this.srvId = srvId;
//		}
//		public String getSysTy() {
//			return sysTy;
//		}
//		public void setSysTy(String sysTy) {
//			this.sysTy = sysTy;
//		}
//		public String getAlias() {
//			return alias;
//		}
//		public void setAlias(String alias) {
//			this.alias = alias;
//		}
//		public boolean isLive() {
//			return live;
//		}
//		public void setLive(boolean live) {
//			this.live = live;
//		}
//		public int getSubnRecX() {
//			return subnRecX;
//		}
//		public void setSubnRecX(int subnRecX) {
//			this.subnRecX = subnRecX;
//		}
//		public int getAcctX() {
//			return acctX;
//		}
//		public void setAcctX(int acctX) {
//			this.acctX = acctX;
//		}
//		public int getLtsType() {
//			return ltsType;
//		}
//		public void setLtsType(int ltsType) {
//			this.ltsType = ltsType;
//		}
//		public String getCardType() {
//			return cardType;
//		}
//		public void setCardType(String cardType) {
//			this.cardType = cardType;
//		}
//		public boolean isHeader() {
//			return isHeader;
//		}
//		public void setHeader(boolean isHeader) {
//			this.isHeader = isHeader;
//		}
//		public String getLatest_bill() {
//			return latest_bill;
//		}
//		public void setLatest_bill(String latest_bill) {
//			this.latest_bill = latest_bill;
//		}
//		public String getPassword() {
//			return password;
//		}
//		public void setPassword(String password) {
//			this.password = password;
//		}
//		public boolean isSms() {
//			return isSms;
//		}
//		public void setSms(boolean isSms) {
//			this.isSms = isSms;
//		}
//	}
}
