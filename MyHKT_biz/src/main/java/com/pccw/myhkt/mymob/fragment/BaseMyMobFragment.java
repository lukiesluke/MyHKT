package com.pccw.myhkt.mymob.fragment;

import android.app.Activity;

import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.fragment.BaseFragment;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnLiveChatListener;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.mymob.model.EditCra;

public class BaseMyMobFragment extends BaseFragment {
	// CallBack
	protected OnMyMobListener		callback_main;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			callback_main = (OnMyMobListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnMyMobListener");
		}
	}
	
	//Interface
	public interface OnMyMobListener {
		void 	setActiveSubview(int index);
		int 	getActiveSubview();
		void 	displaySubview();
		void 	popBackStack();
		void 	fragmentBack();
		void 	onActivityBackPress();
		boolean isEmptyList();
		void setAutoLogin(boolean isAutoLogin);
		boolean isAutoLogin();
		void setSelectedAcctAgent(AcctAgent acctAgent);
		AcctAgent getSelectedAcctAgent();
		AddOnCra getAddOnCra();
		void setAddOnCra(AddOnCra addOnCra);
		EditCra getEditCra();
		void setEditCra(EditCra editCra);
		boolean isEditMode();
		void setEditMode(boolean isEditMode);
		AcctAgent getRecallAcctAgent();

		void setRecallAcctAgent(AcctAgent recallAcctAgent);

		boolean isSavePwd();

		void setSavePwd(boolean isSavePwd);

		boolean isPwdDlgIsSavePwd();

		void setPwdDlgIsSavePwd(boolean pwdDlgIsSavePwd);

		String getPwdDlgPwd();

		void setPwdDlgPwd(String pwdDlgPwd);

		boolean isPwHintUpdated();

		void setPwHintUpdated(boolean isPwHintUpdated);
		String getActionTy();
		void setActionTy(String actionTy);
		SubnRec getIoSubscriptionRec();

		void setIoSubscriptionRec(SubnRec ioSubscriptionRec);
		void killActivity();
		
		void hideLiveChatIcon();
		void showLiveChatIcon();
	}
	
	@Override
	public void onSuccess(APIsResponse response) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(APIsResponse response) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
