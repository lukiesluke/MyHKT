package com.pccw.myhkt.cell.model;

import com.pccw.biz.myhkt.R;

import android.util.Log;
import android.view.View.OnClickListener;

public class TextImageBtnCell extends Cell{

	private OnClickListener onClickListener = null;
	
	public TextImageBtnCell(String title, int drawId, String[] clickArray) {
		super();
		type = Cell.TEXTIMGBTN;
		
		titleColorId = R.color.hkt_txtcolor_grey;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;
		
		this.title = title;
		this.clickArray = clickArray;
		this.draw = drawId;
	}
	
	public TextImageBtnCell(OnClickListener onClickListener, String title, int drawId) {
		super();
		type = Cell.TEXTIMGBTN;
		
		titleColorId = R.color.hkt_txtcolor_grey;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;
		
		this.title = title;
		this.onClickListener = onClickListener;
		this.draw = drawId;
	}
	
	public OnClickListener getOnClickListener() {
		return onClickListener;
	}

	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}
}