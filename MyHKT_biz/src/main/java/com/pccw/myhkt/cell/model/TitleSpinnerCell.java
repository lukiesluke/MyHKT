package com.pccw.myhkt.cell.model;

import android.view.View.OnClickListener;

import com.pccw.biz.myhkt.R;

public class TitleSpinnerCell extends Cell{

	
	private Boolean isArrowShown = false;
	private OnClickListener onClick;
	
	public Boolean getIsArrowShown() {
		return isArrowShown;
	}

	public void setIsArrowShown(Boolean isArrowShown) {
		this.isArrowShown = isArrowShown;
	}

	public TitleSpinnerCell(String title, String content, OnClickListener onClick) {
		super();
		type = Cell.TITLESPINNER;
		this.title = title;
		this.content = content;
		this.onClick = onClick;
		
		titleColorId = R.color.black;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;	
		isArrowShown = false;
	}
	
	public OnClickListener getOnClick() {
		return onClick;
	}

	public void setOnClick(OnClickListener onClick) {
		this.onClick = onClick;
	}
}