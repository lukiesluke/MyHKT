package com.pccw.myhkt.cell.model;

import com.pccw.biz.myhkt.R;

import android.util.Log;

public class BigTextCell extends Cell{

	
	private Boolean isArrowShown = false;

	public BigTextCell(String title, String content) {
		super();
		type = Cell.BIGTEXT1;
		this.title = title;
		this.content = content;
		titleColorId = R.color.black;
		contentColorId = R.color.hkt_txtcolor_grey;
		titleSizeDelta = 4;
		contentSizeDelta = 4;	
		isArrowShown = false;
	}

	public Boolean getIsArrowShown() {
		return isArrowShown;
	}

	public void setIsArrowShown(Boolean isArrowShown) {
		this.isArrowShown = isArrowShown;
	}
	

}