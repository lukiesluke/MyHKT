package com.pccw.myhkt.cell.model;

import com.pccw.biz.myhkt.R;

import android.util.Log;

public class LineTest extends Cell{

	// Bundle name	
	private int rightResId;

	public LineTest(String title, String content,int draw, int rightResId) {
		super();
		//Default setting
		type = Cell.LINETEST;
		titleColorId = R.color.hkt_textcolor;
		contentColorId = R.color.black;		
		
		this.title = title;
		this.content = content;
		this.draw = draw;
		this.rightResId = rightResId;		
	}
	
	public int getRightResId() {
		return rightResId;
	}

	public void setRightResId(int rightResId) {
		this.rightResId = rightResId;
	}
}