package com.pccw.myhkt.cell.model;

import android.view.View.OnClickListener;

import com.pccw.biz.myhkt.R;

public class SpinnerTextCell extends Cell{

	private OnClickListener onClickListener;
	
	
	public OnClickListener getOnClickListener() {
		return onClickListener;
	}


	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}


	public SpinnerTextCell(String title, String[] clickArray) {
		super();
		type = Cell.SPINNERTEXT;
		
		titleColorId = R.color.hkt_txtcolor_grey;
		titleSizeDelta = 0;
		
		this.title = title;
		this.clickArray = clickArray;
	}
}