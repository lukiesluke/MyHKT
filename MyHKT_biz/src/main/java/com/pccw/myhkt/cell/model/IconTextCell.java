package com.pccw.myhkt.cell.model;

import com.pccw.biz.myhkt.R;

import android.util.Log;

public class IconTextCell extends Cell {

	//ICONTEXT
	public IconTextCell(int draw, String title) {
		super();
		type = Cell.ICONTEXT;
		this.title = title;
		this.draw = draw;
		titleColorId = R.color.black;
		titleSizeDelta = 0;
		this.cellHeight = 0;
	}

	
	

}