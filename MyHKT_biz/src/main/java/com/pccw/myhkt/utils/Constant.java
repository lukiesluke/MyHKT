package com.pccw.myhkt.utils;

/**
 * Created by AMoiz Esmail on 02/11/2017.
 */

public class Constant {
    public static final int REQUEST_FILE_STORAGE_PERMISSION = 1115;
    public static final int REQUEST_CALL_PHONE_PERMISSION = 1116;
    public static final int REQUEST_LOCATION_PERMISSION = 1117;
    public static final int REQUEST_LOCATION_PERMISSION_ENABLED = 1118;
    public static final int REQUEST_LOCATION_PERMISSION_DISABLED = 1119;
    public static final int REQUEST_LOG_FILE_STORAGE_PERMISSION = 1120;
    public static final String LOCATION_PERMISSION_DENIED = "com.pccw.com.location.permission_denied";
    public static final String CALL_PHONE_PERMISSION_DENIED = "com.pccw.com.call_phone.permission_denied";
    public static final String ACTION_LOCATION_REQUEST_GRANTED = "com.pccw.com.location.request_granted";
    public static final String FILE_STORAGE_PERMISSION_DENIED = "com.pccw.com.permission_denied";
    public static final String USER_TYPE_UAT = "UAT";
    public static final String USER_TYPE_PRD = "PRD";
    public static final String USER_TYPE_OS = "Android";
    public static final String OS_ENVIRONMENT = "OS";
    public static final String DEVICE_MANUFACTURE = "device_manufacture";
    public static final String DEVICE_BRAND = "device_brand";
    public static final String DEVICE_MODEL = "device_model";
}
