package com.pccw.myhkt;

import android.app.Activity;
import android.app.IntentService;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.XmlDom;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.BaseCraEx;
import com.pccw.dango.shared.cra.BiifCra;
import com.pccw.dango.shared.cra.BinqCra;
import com.pccw.dango.shared.cra.CtacCra;
import com.pccw.dango.shared.cra.DeleteCra;
import com.pccw.dango.shared.cra.DeleteRequest;
import com.pccw.dango.shared.cra.DqryCra;
import com.pccw.dango.shared.cra.HeloCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.cra.PlanImsCra;
import com.pccw.dango.shared.cra.PlanLtsCra;
import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.cra.PlanTvCra;
import com.pccw.dango.shared.cra.ShopCra;
import com.pccw.dango.shared.cra.SpssCra;
import com.pccw.dango.shared.entity.Bill;
import com.pccw.dango.shared.entity.SpssRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.fragment.DeleteAccountFragment;
import com.pccw.myhkt.model.DQAreas;
import com.pccw.myhkt.model.Destination;
import com.pccw.myhkt.model.IDDCity;
import com.pccw.myhkt.model.IDDCodesNTimes;
import com.pccw.myhkt.model.IDDCountry;
import com.pccw.myhkt.model.Rate;
import com.pccw.myhkt.model.SRPlan;
import com.pccw.myhkt.model.VersionCheck;
import com.pccw.wheat.shared.tool.Reply;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class APIsManager {
    private static boolean debug;
    private static boolean debugGrq;
    private static boolean debugCra;

    private static Gson gson = new Gson();
    private static Gson gson2 = new GsonBuilder().serializeNulls().create();

    public static List<Cookie> mCookies;

    public static ProgressManager progressManager = null;

    // Error Code
    public static final String HTTPSTATUS_NOT_200 = "HTTPSTATUS_NOT_200";
    public static final String JSON_RETURN_NULL = "JSON_RETURN_NULL";
    public static final String NULL_OBJECT_CRA = "NULL_OBJECT_CRA";

    private static String domainURL = "https://cspuat.pccw.com/cs2";
    private static String domainFTP = "";
    private static String salt = "";

    //IDD 0060
    public static String UAT_shop_host = "http://sbreg.0060everywhere.com/";
    public static String PRD_shop_host = "http://shop.pccwmobile.com/";
    //	public static String PRD_shop_host = "http://sbreg.0060everywhere.com/";

    public static String UAT_domain = "https://cspuat.pccw.com/cs2";
    //		public static String UAT_domain = "http://10.87.120.207:8080/dango"; //internal UAT
    public static String PRD_domain = "https://customerservice.pccw.com/myhkt"; //pilot version fake PRD

    public static String UAT_FTP = "https://cspuat.pccw.com";
    public static String PRD_FTP = "https://customerservice.pccw.com";


    //the club
    public static final String THECLUB_HOST = "https://uat.theclub.com.hk";

    //SALT (for checksum number)
    public static String UAT_salt = "1234567890";
    public static String PRD_salt = "134552de84";

    //live chat
    public static String UAT_livechat = "https://livechat4.pccw.com/chat/m/index.jsp";
    public static String PRD_livechat = "https://livechat2.pccw.com/chat/m/index.jsp";

    //Google Analytics Tracker
    public static String UAT_tracker1 = "UA-47326648-7";
    public static String UAT_tracker2 = "UA-47326648-9";
    public static String UAT_mm_tracker1 = "UA-60320759-2";
    public static String UAT_mm_tracker2 = "UA-60320759-4";

    public static String PRD_tracker1 = "UA-47326648-6";
    public static String PRD_tracker2 = "UA-47326648-8";
    public static String PRD_mm_tracker1 = "UA-60320759-1";
    public static String PRD_mm_tracker2 = "UA-60320759-3";

    //	public static final String THECLUB_HOST = "https://www.theclub.com.hk";

    public static final String HELO = "HELO";               /* MA for Helo                          */
    public static final String LGI = "LGI";                /* MA for Login                         */
    public static final String LGO = "LGO";                /* MA for Logout                        */
    public static final String RG_ACTCD = "RG_ACTCD";           /* MA for Regenerate Act Code           */
    public static final String P4_RCALL = "P4_RCALL";           /* MA for Preparation for Recall        */
    public static final String RC_CRED = "RC_CRED";            /* MA for Recall Credential             */
    public static final String SIGNUP = "SIGNUP";             /* MA for Signup                        */
    public static final String SMSACT = "SMSACT";             /* MA for SMS Activation                */
    public static final String RESET = "RESET";              /* MA for Reset Password                */
    public static final String PLAN_LTS = "PLAN_LTS";           /* MA for MyPlan (LTS)                  */
    public static final String PLAN_IMS = "PLAN_IMS";           /* MA for MyPlan (IMS)                  */
    public static final String PLAN_TV = "PLAN_TV";            /* MA for MyPlan (TV)                   */
    public static final String PLAN_MOB = "PLAN_MOB";           /* MA for MyPlan (MOB)                  */
    public static final String READ_CTAC = "READ_CTAC";          /* MA for Read Contact Info             */
    public static final String UPD_CTAC = "UPD_CTAC";           /* MA for Update Contact Info           */
    public static final String DEL_ACCOUNT = "APITY_APP_DEL_LOGINID";

    public static final String BILL = "BILL";               /* MA for Bill                          */
    public static final String CHK_BILL = "CHK_BILL";           /* MA for Check Bill                    */
    public static final String READ_BIIF = "READ_BIIF";          /* MA for Read Billing Info             */
    public static final String UPD_BIIF = "UPD_BIIF";           /* MA for Update Billing Info           */
    public static final String APPT = "APPT";               /* MA for Appointment                   */
    public static final String SHOP_BYA = "SHOP_BYA";           /* MA for Shop (Read By Area)           */
    public static final String SHOP_BYGC = "SHOP_BYGC";          /* MA for Shop (Read By GC)             */
    public static final String SHOP = "SHOP";               /* MA for Shop (Read By Area)           */
    public static final String DQRY = "DQRY";               /* MA for Directory Query               */
    public static final String ACMAIN = "ACMAIN";             /* MA for Account Maintenance           */
    public static final String SVCASO = "SVCASO";             /* MA for Associated Account List       */
    public static final String SVCASO4SUBN = "SVCASO4SUBN";        /* MA for Associated Account List       */
    public static final String RTRT = "RTRT";                /* MA for Line Test (Reset purpose)     */
    public static final String SPSS_UPD = "SPSS_UPD";           /* MA for Smart Phone (Update)          */
    public static final String TPUP_HIST = "TPUP_HIST";          /* MA for Get Topup History             */
    public static final String TPUP = "TPUP";               /* MA for Get Topup                     */
    public static final String AO_ADDON = "AO_ADDON";           /* MA for Add-on Add on                 */
    public static final String AO_AUTH = "AO_AUTH";            /* MA for Add-on Authentication         */

    public static final String REQ_UPDTOK4LTSPI = "REQ_UPDTOK4LTSPI";    /* MA for Update LTS biifCra            */
    public static final String PAR_LOB = "p0";
    public static final String PAR_SYSTY = "p1";
    public static final String PAR_CUS_NUM = "p2";
    public static final String PAR_ACCT_NUM = "p3";
    public static final String PAR_INV_DATE = "p4";
    public static final String PAR_LANG = "p5";

    public static final String SPSS = "SPSS";               /* MA for Smart Phone                   */
    public static final String LNTT = "LNTT";               /* MA for Line Test                     */
    public static final String SR_CRT = "SR_CRT";             /* MA for SR Creation                   */
    public static final String SR_UPD = "SR_UPD";             /* MA for SR Update                     */
    public static final String SR_CLS = "SR_CLS";             /* MA for SR Closure                    */
    public static final String SR_ENQ = "SR_ENQ";             /* MA for SR Enquiry                    */
    public static final String SR_ENQ_NP = "SR_ENQ_NP";          /* MA for SR Enquiry (No Pending)       */
    public static final String SR_AVA_TS = "SR_AVA_TS";          /* MA for SR Available Timeslot         */
    public static final String ECLOSION = "ECLOSION";

    //Custom make Action type
    public static final String IDDCTY = "IDDCTY";             /* MA for IDD Country list	            */
    public static final String IDDCODE = "IDDCODE";            /*    for IDD Code Result   			*/
    public static final String IDDRATEDEST = "IDDRATEDEST";
    public static final String IDDRATERESULT = "IDDRATERESULT";

    public static void onDeleteAccountRequest(DeleteAccountFragment fragment, String logId, int rid, String sessionToken, APIsManager.OnAPIsListener callback) {
        Context context = fragment.getContext();
        domainURL = ClnEnv.getPref(context, Objects.requireNonNull(context).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/delLoginId";

        DeleteRequest deleteRequest = new DeleteRequest();
        deleteRequest.setIApi(DEL_ACCOUNT);
        deleteRequest.setiLoginId(logId);
        deleteRequest.setiSveeRid(rid);
        deleteRequest.setIGwtGud(sessionToken);

        APIsRequest<DeleteRequest> apiRequest = new APIsRequest<>(url, DEL_ACCOUNT);
        apiRequest.setiCra(deleteRequest);

        deleteAccount(fragment, DeleteRequest.class, apiRequest, callback);
    }

    public static <T extends BaseCraEx> void deleteAccount(final Fragment fragment, final Class<T> typeClass, final APIsRequest<T> apiRequest, APIsManager.OnAPIsListener callback) {
        final AQuery aq = new AQuery(fragment.getActivity());
        FragmentActivity fragmentActivity = fragment.getActivity();

        try {
            progressManager.showProgressDialog(fragment.getActivity());
            String jstr = gson.toJson(apiRequest.getiCra());
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<>();
            params.put(AQuery.POST_ENTITY, entity);

            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<JSONObject>() {
                APIsResponse response;
                DeleteCra cra;

                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    response = new APIsResponse();
                    response.setActionTy(DEL_ACCOUNT);
                    cra = new DeleteCra();
                    try {
                        cra = gson.fromJson(object.toString(), DeleteCra.class);
                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(fragmentActivity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } else {
                            if ("RC_SUCC".equalsIgnoreCase(cra.getReply().getCode())) {
                                response.setReply(cra.getReply());
                                response.setMessage(ClnEnv.getRPCErrMsg(fragmentActivity, status.getCode(), status.getMessage()));
                                callback.onSuccess(response);
                            } else {
                                response.setMessage(ClnEnv.getRPCErrMsg(fragmentActivity, cra.getReply().getCode()));
                                response.setReply(cra.getReply());
                                callback.onFail(response);
                            }
                        }
                        progressManager.dismissProgressDialog(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("lwg", "Exception inner: " + e.getMessage());
                        try {
                            response.setMessage(ClnEnv.getRPCErrMsg(fragmentActivity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    progressManager.dismissProgressDialog(true);
                }
            };

            // Here we are setting the cookie info.
            for (Cookie cookie : mCookies) {
                ajaxCallback.cookie(cookie.getName(), cookie.getValue());
            }
            aq.ajax(apiRequest.getPath(), params, JSONObject.class, ajaxCallback);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("lwg", "Exception end: " + e.getMessage());
            progressManager.dismissProgressDialog(true);
        }
    }

    public interface OnAPIsListener {
        void onSuccess(APIsResponse response) throws Exception;

        void onFail(APIsResponse response) throws Exception;
    }

    private static final void initDebugMode(Context context) {
        debug = context.getResources().getBoolean(R.bool.DEBUG);
        debugGrq = context.getResources().getBoolean(R.bool.DEBUGGRQ);
        debugCra = context.getResources().getBoolean(R.bool.DEBUGCRA);
    }

    //		public static final void doHelo(final Activity activity) {
    //			initDebugMode(activity);
    //			final String TAGNAME = "doHelo";
    //			AQuery aq = new AQuery(activity);
    //			String url = domainURL +"/ma/helo";
    //			HeloCra heloCra = new HeloCra();
    //			heloCra.setIApi(HELO);
    //
    //			//including spssCra on every dohelo
    //			SpssCra spssCra = new SpssCra();
    //			spssCra.setICkSum(Utils.sha256(ClnEnv.getPref(activity.getApplicationContext(), activity.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", Utils.getString(activity, R.string.CONST_SALT)));
    //
    //			if (ClnEnv.isLoggedIn())  {
    //				spssCra.setISpssRec(ClnEnv.getLgiCra().getISpssRec());
    //				spssCra.setILoginId(ClnEnv.getLoginId());
    //			} else {
    //				//if not doing login or save login
    //					spssCra.getISpssRec().devTy = SpssRec.TY_ANDROID;
    //					spssCra.getISpssRec().devId = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GCM_REGID), "");
    ////					spssCra.getISpssRec().devId = ClnEnv.getDeviceID(activity);
    //					spssCra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
    //					spssCra.getISpssRec().gni = ClnEnv.getPref(activity.getApplicationContext(), activity.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
    ////					spssCra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";
    //			}
    //			heloCra.setISpssCra(spssCra);
    //
    //			String jstr = gson.toJson(heloCra);
    //			if (debugGrq) ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
    //			if (debugCra) Log.d(TAGNAME, String.format("%s %s",  TAGNAME, "request sent:") + jstr);
    //
    //			try {
    //				HttpEntity entity = new StringEntity(jstr, "UTF-8");
    //				Map<String, Object> params = new HashMap<String, Object>();
    //				params.put(AQuery.POST_ENTITY, entity);
    //				aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {
    //
    //					OnAPIsListener 	callback;
    //					APIsResponse 	response;
    //					HeloCra 		cra;
    //
    //					@Override
    //					public void callback(String url, JSONObject json, AjaxStatus status) {
    //						mCookies = status.getCookies();
    //						try {
    //							try {
    //								callback = (OnAPIsListener) activity;
    //							} catch (ClassCastException e) {
    //								throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
    //							}
    //
    //							response = new APIsResponse();
    //							response.setActionTy(HELO);
    //							cra = new HeloCra();
    //							if (status.getCode() != 200) {
    //								response.setReply(HTTPSTATUS_NOT_200);
    //								response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
    //								callback.onFail(response);
    //							} else if (json == null) {
    //								// TODO: httppost response null Object from api
    //								response.setReply(JSON_RETURN_NULL);
    //								response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
    //								callback.onFail(response);
    //							} else {
    //								String resultStr = json.toString();
    //								if (debugCra) ClnEnv.toLogFile(String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
    //								if (debugCra) Log.d(TAGNAME, String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);
    //
    //								try {
    //									Type collectionType = new TypeToken<HeloCra>() {}.getType();
    //									cra = gson.fromJson(json.toString(), collectionType);
    //								} catch (Exception e) {
    //									if (debug) Log.e(TAGNAME+"/gson.fromJson error:", e.getMessage());
    //									e.printStackTrace();
    //								}
    //
    //								if (cra == null || !cra.getReply().isSucc()) {
    //									if (cra == null) {
    //										cra = new HeloCra();
    //										cra.setReply(NULL_OBJECT_CRA);
    //									}
    //									response.setReply(cra.getReply());
    //									response.setCra(cra);
    //									callback.onFail(response);
    //								} else {
    //									ClnEnv.setHeloCra(cra);
    //									response.setReply(cra.getReply());
    //									response.setCra(cra);
    //									callback.onSuccess(response);
    //								}
    //							}
    //						} catch (Exception e) {
    //							if (debugCra) Log.d(TAGNAME, e.toString());
    //						}
    //					}
    //				});
    //			} catch (Exception e) {
    //				e.toString();
    //			}
    //		}


    //	public static final void doHelo(final FragmentActivity activity) {
    //	initDebugMode(activity);
    //	final String TAGNAME = "doHelo";
    //	AQuery aq = new AQuery(activity);
    //	String url = domainURL +"/ma/helo";
    //	HeloCra heloCra = new HeloCra();
    //	heloCra.setIApi(HELO);
    //
    //	SpssRec spssRec = new SpssRec();
    //	if (ClnEnv.getPref(activity, activity.getResources().getString(R.string.CONST_PREF_SAVEPASSWORD), false) && ClnEnv.isLoggedIn()) {
    //		//Bill Notificatiion "Y" if password is saved
    //		spssRec.bni = "Y";
    //	} else {
    //		spssRec.bni = "N";
    //	}
    //	spssRec.gni = aq.id(R.id.settings_checkbox_noti).isChecked() ? "Y" : "N";
    //	spssRec.lang = ClnEnv.getAppLocale( activity.getBaseContext());
    //	spssRec.devId =	ClnEnv.getPref(activity,  activity.getResources().getString(R.string.CONST_PREF_GCM_REGID), "");
    //	SpssCra spssCra = new SpssCra();
    //	spssCra.setISpssRec(spssRec);
    //	heloCra.setISpssCra(spssCra);
    //
    //
    //	String jstr = gson.toJson(heloCra);
    //	if (debugGrq) ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
    //	if (debugCra) Log.d(TAGNAME, String.format("%s %s",  TAGNAME, "request sent:") + jstr);
    //
    //	try {
    //		HttpEntity entity = new StringEntity(jstr, "UTF-8");
    //		Map<String, Object> params = new HashMap<String, Object>();
    //		params.put(AQuery.POST_ENTITY, entity);
    //		aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {
    //
    //			OnAPIsListener 	callback;
    //			APIsResponse 	response;
    //			HeloCra 		cra;
    //
    //			@Override
    //			public void callback(String url, JSONObject json, AjaxStatus status) {
    //				mCookies = status.getCookies();
    //				try {
    //					try {
    //						callback = (OnAPIsListener) activity;
    //					} catch (ClassCastException e) {
    //						throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
    //					}
    //
    //					response = new APIsResponse();
    //					response.setActionTy(HELO);
    //					cra = new HeloCra();
    //					if (status.getCode() != 200) {
    //						response.setReply(HTTPSTATUS_NOT_200);
    //						response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
    //						callback.onFail(response);
    //					} else if (json == null) {
    //						// TODO: httppost response null Object from api
    //						response.setReply(JSON_RETURN_NULL);
    //						response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
    //						callback.onFail(response);
    //					} else {
    //						String resultStr = json.toString();
    //						if (debugCra) ClnEnv.toLogFile(String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
    //						if (debugCra) Log.d(TAGNAME, String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);
    //
    //						try {
    //							Type collectionType = new TypeToken<HeloCra>() {}.getType();
    //							cra = gson.fromJson(json.toString(), collectionType);
    //						} catch (Exception e) {
    //							if (debug) Log.e(TAGNAME+"/gson.fromJson error:", e.getMessage());
    //							e.printStackTrace();
    //						}
    //
    //						if (cra == null || !cra.getReply().isSucc()) {
    //							if (cra == null) {
    //								cra = new HeloCra();
    //								cra.setReply(NULL_OBJECT_CRA);
    //							}
    //							response.setReply(cra.getReply());
    //							response.setCra(cra);
    //							callback.onFail(response);
    //						} else {
    //							ClnEnv.setHeloCra(cra);
    //							response.setReply(cra.getReply());
    //							response.setCra(cra);
    //							callback.onSuccess(response);
    //						}
    //					}
    //				} catch (Exception e) {
    //					if (debugCra) Log.d(TAGNAME, e.toString());
    //				}
    //			}
    //		});
    //	} catch (Exception e) {
    //		e.toString();
    //	}
    //}


    //	public static final void doHelo(final Fragment fragment) {
    //		final String TAGNAME = "doHelo";
    //		AAQuery aq = new AAQuery(fragment.getActivity());
    //		String url = domainURL +"/ma/helo";
    //		HeloCra heloCra = new HeloCra();
    //		heloCra.setIApi(HELO);
    //
    //		String jstr = gson.toJson(heloCra);
    //		if (debugGrq) ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
    //		if (debugCra) Log.d(TAGNAME, String.format("%s %s",  TAGNAME, "request sent:") + jstr);
    //
    //		try {
    //			HttpEntity entity = new StringEntity(jstr, "UTF-8");
    //			Map<String, Object> params = new HashMap<String, Object>();
    //			params.put(AQuery.POST_ENTITY, entity);
    //
    //			aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {
    //
    //				OnAPIsListener 	callback;
    //				APIsResponse 	response;
    //				HeloCra 		cra;
    //
    //				@Override
    //				public void callback(String url, JSONObject json, AjaxStatus status) {
    //					mCookies = status.getCookies();
    //					try {
    //						try {
    //							callback = (OnAPIsListener) fragment;
    //						} catch (ClassCastException e) {
    //							throw new ClassCastException(fragment.toString() + " must implement OnAPIsListener");
    //						}
    //
    //						response = new APIsResponse();
    //						response.setActionTy(HELO);
    //						cra = new HeloCra();
    //						if (status.getCode() != 200) {
    //							response.setReply(HTTPSTATUS_NOT_200);
    //							response.setMessage(ClnEnv.getRPCErrMsg(fragment.getActivity(), status.getCode(), status.getMessage()));
    //							callback.onFail(response);
    //						} else if (json == null) {
    //							// TODO: httppost response null Object from api
    //							response.setReply(JSON_RETURN_NULL);
    //							response.setMessage(ClnEnv.getRPCErrMsg(fragment.getActivity(), status.getCode(), status.getMessage()));
    //							callback.onFail(response);
    //						} else {
    //							String resultStr = json.toString();
    //							if (debugCra) ClnEnv.toLogFile(String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
    //							if (debugCra) Log.d(TAGNAME, String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);
    //
    //							try {
    //								Type collectionType = new TypeToken<HeloCra>() {}.getType();
    //								cra = gson.fromJson(json.toString(), collectionType);
    //							} catch (Exception e) {
    //								if (debug) Log.e(TAGNAME+"/gson.fromJson error:", e.getMessage());
    //								e.printStackTrace();
    //							}
    //
    //							if (cra == null || !cra.getReply().isSucc()) {
    //								if (cra == null) {
    //									cra = new HeloCra();
    //									cra.setReply(NULL_OBJECT_CRA);
    //								}
    //								response.setReply(cra.getReply());
    //								response.setCra(cra);
    //								callback.onFail(response);
    //							} else {
    //								ClnEnv.setHeloCra(cra);
    //								response.setReply(cra.getReply());
    //								response.setCra(cra);
    //								callback.onSuccess(response);
    //							}
    //						}
    //					} catch (Exception e) {
    //						if (debugCra) Log.d(TAGNAME, e.toString());
    //					}
    //				}
    //			});
    //		} catch (Exception e) {
    //			e.toString();
    //		}
    //	}

    public static final void doLogin(final FragmentActivity activity, LgiCra rLgiCra, boolean isSavePwd) {
        final String TAGNAME = "doLogin";
        AQuery aq = new AQuery(activity);
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/lgi";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(LGI);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        lgicra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        lgicra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");
//		lgicra.getISpssRec().devId = "";

        //		lgicra.getISpssRec().devId = ClnEnv.getDeviceID(activity);
        lgicra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        lgicra.getISpssRec().gni = ClnEnv.getPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
        lgicra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";


        String jstr = gson.toJson(lgicra);
        if (debugGrq) ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
        if (debugCra) Log.d(TAGNAME, String.format("%s %s", TAGNAME, "request sent:") + jstr);

        try {
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(AQuery.POST_ENTITY, entity);


            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<JSONObject>() {
                OnAPIsListener callback;
                APIsResponse response;
                LgiCra cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    //        	          cookies = status.getCookies(); // Save the cookies in every requests you made
                    try {
                        try {
                            callback = (OnAPIsListener) activity;
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
                        }

                        response = new APIsResponse();
                        response.setActionTy(LGI);
                        cra = new LgiCra();
                        if (status.getCode() != 200) {
                            // TODO: return ERROR Message for HTTPSTATUS Not 200
                            if (debugCra)
                                Log.d(TAGNAME, "HTTPSTATUS_NOT_200 : Status : " + status.getCode() + ", Error : " + status.getError());
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            //	            		//derek force success for testing only START
                            //	            		String resultStr = "{\"serverTS\":\"20151130155041\",\"iGwtGud\":\"\",\"iOrigPwd\":\"\",\"oDuct\":null,\"oSrvReqAry\":[],\"iRecallLgiId\":false,\"oTaSubnRec\":{\"rev\":0,\"createTs\":\"\",\"netLoginId\":\"\",\"custRid\":0,\"systy\":\"\",\"instAdr\":\"\",\"eyeGrp\":\"\",\"sipSubr\":\"\",\"wipCust\":\"\",\"storeTy\":\"\",\"lastupdTs\":\"\",\"datCd\":\"\",\"indTv\":\"\",\"lastupdPsn\":\"\",\"lob\":\"\",\"tos\":\"\",\"indPcd\":\"\",\"auth_ph\":null,\"tariff\":\"\",\"indEye\":\"\",\"createPsn\":\"\",\"cusNum\":\"\",\"assoc\":\"\",\"fsa\":\"\",\"acct_ty\":null,\"alias\":\"\",\"acctNum\":\"\",\"ind2l2b\":\"\",\"domainTy\":\"\",\"srvNum\":\"\",\"bsn\":\"\",\"rid\":0,\"refFsa2l2b\":\"\",\"srvId\":\"\",\"priMob\":\"\",\"aloneDialup\":\"\",\"ivr_pwd\":null},\"oBomCustAry\":[{\"cusNum\":\"9500602\",\"lob\":\"PCD\",\"acctNum\":\"1200094879\",\"sysTy\":\"IMS\"}],\"iSveeRec\":{\"loginId\":\"\",\"rev\":0,\"staffId\":\"\",\"secAns\":\"\",\"ctMail\":\"\",\"status\":\"\",\"nickname\":\"\",\"createTs\":\"\",\"teamCd\":\"\",\"state\":\"\",\"custRid\":0,\"lastupdTs\":\"\",\"lang\":\"\",\"lastupdPsn\":\"\",\"ctMob\":\"\",\"salesChnl\":\"\",\"pwdEff\":\"\",\"pwd\":\"\",\"promoOpt\":\"\",\"regnActn\":\"\",\"rid\":0,\"stateUpTs\":\"\",\"secQus\":0,\"mobAlrt\":\"\",\"createPsn\":\"\"},\"oQualSvee\":{\"zmSubnRecAry\":[],\"subnRecAry\":[{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"b60070059\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"N\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60070059\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200094879\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"b60070059\",\"bsn\":\" \",\"rid\":44775,\"refFsa2l2b\":\" \",\"srvId\":\"60070059\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60069429\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"N\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60069429\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200101993\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"a60069429\",\"bsn\":\" \",\"rid\":44776,\"refFsa2l2b\":\" \",\"srvId\":\"60069429\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60069005\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"N\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60069005\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200102006\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"a60069005\",\"bsn\":\" \",\"rid\":44777,\"refFsa2l2b\":\" \",\"srvId\":\"60069005\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60069009\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"N\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60069009\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200121942\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"a60069009\",\"bsn\":\" \",\"rid\":44778,\"refFsa2l2b\":\" \",\"srvId\":\"60069009\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60071827\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"Y\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60071827\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200124349\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"a60071827\",\"bsn\":\" \",\"rid\":44779,\"refFsa2l2b\":\" \",\"srvId\":\"60071827\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60071827\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"Y\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"TV\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60071827\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200124352\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"1200124352\",\"bsn\":\" \",\"rid\":44780,\"refFsa2l2b\":\" \",\"srvId\":\"60071827\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null}],\"custRec\":{\"docNum\":\"88888891\",\"phylum\":\"CSUM\",\"rev\":0,\"status\":\"A\",\"rid\":7503,\"createTs\":\"20151127164938\",\"premier\":\" \",\"lastupdTs\":\"20151127164938\",\"docTy\":\"PASS\",\"createPsn\":\"dldsbat\",\"lastupdPsn\":\"dldsbat\"},\"bcifRec\":{\"priCtJobtitle\":\"\",\"rev\":0,\"custNm\":\"\",\"secCtMail\":\"\",\"createTs\":\"\",\"custRid\":0,\"secCtTel\":\"\",\"lastupdTs\":\"\",\"priCtMob\":\"\",\"priCtMail\":\"\",\"xborder\":\"\",\"lastupdPsn\":\"\",\"secCtMob\":\"\",\"nuStaff\":\"\",\"secCtName\":\"\",\"priCtTel\":\"\",\"nuPresence\":\"\",\"priCtName\":\"\",\"priCtTitle\":\"\",\"secCtTitle\":\"\",\"industry\":\"\",\"createPsn\":\"\",\"secCtJobtitle\":\"\"},\"sveeRec\":{\"loginId\":\"keith.kk.mak@pccw.com\",\"rev\":1,\"staffId\":\" \",\"secAns\":\"\",\"ctMail\":\"keith.kk.mak@pccw.com\",\"status\":\"A\",\"nickname\":\"KM\",\"createTs\":\"20151130103801\",\"teamCd\":\" \",\"state\":\"A\",\"custRid\":7503,\"lastupdTs\":\"20151130103824\",\"lang\":\"zh\",\"lastupdPsn\":\"!UNKN\",\"ctMob\":\"66230738\",\"salesChnl\":\" \",\"pwdEff\":\"Y\",\"pwd\":\"\",\"promoOpt\":\"N\",\"regnActn\":\"REGUSER\",\"rid\":3,\"stateUpTs\":\"20151130103824\",\"secQus\":0,\"mobAlrt\":\"Y\",\"createPsn\":\"!UNKN\"}},\"oGnrlApptAry\":[],\"iSpssRec\":{\"rev\":0,\"devTy\":\"\",\"dervRid\":0,\"rid\":0,\"bni\":\"\",\"sveeRid\":0,\"lastupdTs\":\"\",\"tcId\":\"\",\"gni\":\"\",\"devId\":\"\",\"tcSess\":\"\",\"lang\":\"\",\"lastupdPsn\":\"\"},\"oNxUrl\":\"\",\"iMyFrm\":\"\",\"iCustRec\":{\"docNum\":\"\",\"phylum\":\"\",\"rev\":0,\"status\":\"\",\"rid\":0,\"createTs\":\"\",\"premier\":\"\",\"lastupdTs\":\"\",\"docTy\":\"\",\"createPsn\":\"\",\"lastupdPsn\":\"\"},\"oHgg\":\"\",\"oAcctAry\":[{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200094879\"}],\"oChatTok\":\"e32ca1045577bcd7b0fc68ccb90e92f3d3d620d9a63b0a284a333fdce7073812\",\"reply\":{\"cargo\":null,\"code\":\"RC_SUCC\"},\"iApi\":\"\",\"oBillListAry\":[{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200094879\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200101993\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200102006\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200121942\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200124349\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"TV\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200124352\"}}],\"oSoGud\":\"\",\"iRecallPwd\":false,\"iNaSubnRec\":{\"rev\":0,\"createTs\":\"\",\"netLoginId\":\"\",\"custRid\":0,\"systy\":\"\",\"instAdr\":\"\",\"eyeGrp\":\"\",\"sipSubr\":\"\",\"wipCust\":\"\",\"storeTy\":\"\",\"lastupdTs\":\"\",\"datCd\":\"\",\"indTv\":\"\",\"lastupdPsn\":\"\",\"lob\":\"\",\"tos\":\"\",\"indPcd\":\"\",\"auth_ph\":null,\"tariff\":\"\",\"indEye\":\"\",\"createPsn\":\"\",\"cusNum\":\"\",\"assoc\":\"\",\"fsa\":\"\",\"acct_ty\":null,\"alias\":\"\",\"acctNum\":\"\",\"ind2l2b\":\"\",\"domainTy\":\"\",\"srvNum\":\"\",\"bsn\":\"\",\"rid\":0,\"refFsa2l2b\":\"\",\"srvId\":\"\",\"priMob\":\"\",\"aloneDialup\":\"\",\"ivr_pwd\":null}}";
                            //	            		Type collectionType = new TypeToken<LgiCra>() {}.getType();
                            //	            		cra = gson.fromJson(resultStr, collectionType);
                            //	            		ClnEnv.setLgiCra(cra);
                            //	            		response.setReply(cra.getReply());
                            //            			response.setCra(cra);
                            //	            			callback.onSuccess(response);
                            //	            		//derek force success for testing only END
                            callback.onFail(response);
                        } else if (json == null) {
                            // TODO: httppost response null Object from api
                            if (debugCra) Log.d(TAGNAME, JSON_RETURN_NULL);
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } else {
                            String resultStr = json.toString();
                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Log.d(TAGNAME, String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                Type collectionType = new TypeToken<LgiCra>() {
                                }.getType();
                                cra = gson.fromJson(json.toString(), collectionType);
                            } catch (Exception e) {
                                if (debug) Log.e(TAGNAME, "/gson.fromJson error:" + e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || !cra.getReply().isSucc()) {
                                if (cra == null) {
                                    cra = new LgiCra();
                                    cra.setReply(NULL_OBJECT_CRA);
                                }
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onFail(response);
                            } else {
                                ClnEnv.setLgiCra(cra);
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onSuccess(response);
                            }
                        }
                    } catch (Exception e) {
                        if (debugCra) Log.d(TAGNAME, e.toString());
                    }
                }

            };

            for (Cookie cookie : mCookies) {
                ajaxCallback.cookie(cookie.getName(), cookie.getValue());
                //Here we are setting the cookie info.
            }

            aq.ajax(url, params, JSONObject.class, ajaxCallback);
        } catch (Exception e) {
            e.toString();
        }
    }

    public static final void doLogout(final FragmentActivity activity) {
        final String TAGNAME = "doLogout";
        AQuery aq = new AQuery(activity);
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/lgo";

        LgiCra lgicra = new LgiCra();
        lgicra.setIApi(LGO);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        //		lgicra.setI
        //
        APIsRequest<LgiCra> apiRequest = new APIsRequest<LgiCra>(url, LGO);
        apiRequest.setiCra(lgicra);

        doAction(activity, null, LgiCra.class, apiRequest);
    }

    public static final void doRegister(final FragmentActivity activity, AcMainCra rAcMainCra, boolean isSavePwd) {
        final String TAGNAME = "doRegister";
        AQuery aq = new AQuery(activity);
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/signup";
        AcMainCra acMainCra = rAcMainCra.copyMe();
        acMainCra.setIApi(SIGNUP);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        //		acMainCra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        //		acMainCra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");
        //		acMainCra.getISpssRec().devId = ClnEnv.getDeviceID(activity);
        //		acMainCra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        //		acMainCra.getISpssRec().gni = ClnEnv.getPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), "Y");
        //		acMainCra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<AcMainCra>(url, SIGNUP);
        apiRequest.setiCra(acMainCra);

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHelo(activity, null, AcMainCra.class, apiRequest);

        //		String jstr = gson.toJson(acMainCra);
        //		if (debugGrq) ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, acMainCra.getClass().getSimpleName(), "request sent"), jstr);
        //		if (debugCra) Log.d(TAGNAME, String.format("%s %s %s",  TAGNAME, acMainCra.getClass().getSimpleName(), "request sent:") + jstr);
        //
        //		try {
        //			HttpEntity entity = new StringEntity(jstr, "UTF-8");
        //			Map<String, Object> params = new HashMap<String, Object>();
        //			params.put(AQuery.POST_ENTITY, entity);
        //
        //			AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<JSONObject>() {
        //
        //				OnAPIsListener 	callback;
        //				APIsResponse 	response;
        //				AcMainCra 		cra;
        //
        //				@Override
        //				public void callback(String url, JSONObject json, AjaxStatus status) {
        //					try {
        //						try {
        //							callback = (OnAPIsListener) activity;
        //						} catch (ClassCastException e) {
        //							throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
        //						}
        //
        //						response = new APIsResponse();
        //						response.setActionTy(SIGNUP);
        //						cra = new AcMainCra();
        //						if (status.getCode() != 200) {
        //							// TODO: return ERROR Message for HTTPSTATUS Not 200
        //							if (debug) Log.d(TAGNAME, "HTTPSTATUS_NOT_200 : Status : "+ status.getCode() + ", Error : "+status.getError());
        //							response.setReply(HTTPSTATUS_NOT_200);
        //							response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
        //							callback.onFail(response);
        //						} else if (json == null) {
        //							// TODO: httppost response null Object from api
        //							if (debug) Log.d(TAGNAME, JSON_RETURN_NULL);
        //							response.setReply(JSON_RETURN_NULL);
        //							response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
        //							callback.onFail(response);
        //						} else {
        //							String resultStr = json.toString();
        //							if (debugCra) ClnEnv.toLogFile(String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
        //							if (debugCra) Log.d(TAGNAME, String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);
        //
        //							try {
        //								Type collectionType = new TypeToken<AcMainCra>() {}.getType();
        //								cra = gson.fromJson(json.toString(), collectionType);
        //							} catch (Exception e) {
        //								if (debug) Log.e(TAGNAME+"/gson.fromJson error:", e.getMessage());
        //								e.printStackTrace();
        //							}
        //
        //							if (cra == null || !cra.getReply().isSucc()) {
        //								if (cra == null) {
        //									cra = new AcMainCra();
        //									cra.setReply(NULL_OBJECT_CRA);
        //								}
        //								response.setReply(cra.getReply());
        //								response.setCra(cra);
        //								callback.onFail(response);
        //							} else {
        //								response.setReply(cra.getReply());
        //								response.setCra(cra);
        //								callback.onSuccess(response);
        //							}
        //						}
        //					} catch (Exception e) {
        //						if (debugCra) Log.d(TAGNAME, e.toString());
        //					}
        //				}
        //			};
        //
        //			for (Cookie cookie : mCookies) {
        //				ajaxCallback.cookie(cookie.getName(), cookie.getValue());
        //				//Here we are setting the cookie info.
        //			}
        //
        //			aq.ajax(url, params, JSONObject.class, ajaxCallback);
        //		} catch (Exception e) {
        //			e.toString();
        //		}
    }

    public static final void doPrepare4Recall(final FragmentActivity activity, LgiCra rLgiCra) {
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/prepRecall";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(P4_RCALL);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        lgicra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        lgicra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");
        lgicra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        lgicra.getISpssRec().gni = ClnEnv.getPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
        //		lgicra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";

        APIsRequest<LgiCra> apiRequest = new APIsRequest<LgiCra>(url, P4_RCALL);
        apiRequest.setiCra(lgicra);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        doHelo(activity, null, LgiCra.class, apiRequest);
    }

    public static final void doRecallIDOrPwd(final FragmentActivity activity, LgiCra rLgiCra) {
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/recallCred";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(RC_CRED);
        lgicra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<LgiCra> apiRequest = new APIsRequest<LgiCra>(url, RC_CRED);
        apiRequest.setiCra(lgicra);

        //		doAction(activity, null, LgiCra.class, apiRequest);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        doHelo(activity, null, LgiCra.class, apiRequest);
    }

    public static final void doResetPwd(final FragmentActivity activity, AcMainCra rAcMainCra) {
        final String TAGNAME = "doResetPwd";

        AQuery aq = new AQuery(activity);
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/reset";

        AcMainCra acMainCra = rAcMainCra.copyMe();
        acMainCra.setIApi(RESET);
        //		acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<AcMainCra>(url, RESET);
        apiRequest.setiCra(acMainCra);

        doHelo(activity, null, AcMainCra.class, apiRequest);
        //		if (debugGrq) ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, acMainCra.getClass().getSimpleName(), "request sent"), jstr);
        //		if (debugCra) Log.d(TAGNAME, String.format("%s %s %s",  TAGNAME, acMainCra.getClass().getSimpleName(), "request sent:") + jstr);
        //
        //		try {
        //			HttpEntity entity = new StringEntity(jstr, "UTF-8");
        //			Map<String, Object> params = new HashMap<String, Object>();
        //			params.put(AQuery.POST_ENTITY, entity);
        //
        //			AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<JSONObject>() {
        //
        //				OnAPIsListener 	callback;
        //				APIsResponse 	response;
        //				AcMainCra 		cra;
        //
        //				@Override
        //				public void callback(String url, JSONObject json, AjaxStatus status) {
        //					try {
        //						try {
        //							callback = (OnAPIsListener) fragment;
        //						} catch (ClassCastException e) {
        //							throw new ClassCastException(fragment.toString() + " must implement OnAPIsListener");
        //						}
        //
        //						response = new APIsResponse();
        //						response.setActionTy(RESET);
        //						cra = new AcMainCra();
        //						if (status.getCode() != 200) {
        //							// TODO: return ERROR Message for HTTPSTATUS Not 200
        //							if (debug) Log.d(TAGNAME, "HTTPSTATUS_NOT_200 : Status : "+ status.getCode() + ", Error : "+status.getError());
        //							response.setReply(HTTPSTATUS_NOT_200);
        //							response.setMessage(ClnEnv.getRPCErrMsg(fragment.getActivity(), status.getCode(), status.getMessage()));
        //							callback.onFail(response);
        //						} else if (json == null) {
        //							// TODO: httppost response null Object from api
        //							if (debug) Log.d(TAGNAME, JSON_RETURN_NULL);
        //							response.setReply(JSON_RETURN_NULL);
        //							response.setMessage(ClnEnv.getRPCErrMsg(fragment.getActivity(), status.getCode(), status.getMessage()));
        //							callback.onFail(response);
        //						} else {
        //							String resultStr = json.toString();
        //							if (debugCra) ClnEnv.toLogFile(String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
        //							if (debugCra) Log.d(TAGNAME, String.format("%s %s %s",  TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);
        //
        //							try {
        //								Type collectionType = new TypeToken<AcMainCra>() {}.getType();
        //								cra = gson.fromJson(json.toString(), collectionType);
        //							} catch (Exception e) {
        //								if (debug) Log.e(TAGNAME+"/gson.fromJson error:", e.getMessage());
        //								e.printStackTrace();
        //							}
        //
        //							if (cra == null || !cra.getReply().isSucc()) {
        //								if (cra == null) {
        //									cra = new AcMainCra();
        //									cra.setReply(NULL_OBJECT_CRA);
        //								}
        //								response.setReply(cra.getReply());
        //								response.setCra(cra);
        //								callback.onFail(response);
        //							} else {
        //								response.setReply(cra.getReply());
        //								response.setCra(cra);
        //								callback.onSuccess(response);
        //							}
        //						}
        //					} catch (Exception e) {
        //						if (debugCra) Log.d(TAGNAME, e.toString());
        //					}
        //				}
        //			};
        //
        //			for (Cookie cookie : mCookies) {
        //				ajaxCallback.cookie(cookie.getName(), cookie.getValue());
        //				//Here we are setting the cookie info.
        //			}
        //
        //			aq.ajax(url, params, JSONObject.class, ajaxCallback);
        //		} catch (Exception e) {
        //			e.toString();
        //		}
    }

    public static final void doChangePwd(final Fragment fragment, AcMainCra rAcMainCra) {
        final String TAGNAME = "doChangePwd";
        AQuery aq = new AQuery(fragment.getActivity());
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/acMain";
        AcMainCra acMainCra = rAcMainCra.copyMe();
        acMainCra.setIApi(ACMAIN);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        //acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        //		acMainCra.setIChgPwd(true);

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<AcMainCra>(url, ACMAIN);
        apiRequest.setiCra(acMainCra);

        doAction(fragment.getActivity(), fragment, AcMainCra.class, apiRequest);
    }

    public static final void doUpdProfile(final FragmentActivity activity, final Fragment fragment, AcMainCra rAcMainCra) {
        final String TAGNAME = "doUpdProfile";
        Context cxt = activity == null ? fragment.getActivity() : activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/acMain";
        AcMainCra acMainCra = rAcMainCra.copyMe();
        acMainCra.setIApi(ACMAIN);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIChgPwd(false);
        //acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<AcMainCra>(url, ACMAIN);
        apiRequest.setiCra(acMainCra);


        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);

        doAction(activity, fragment, AcMainCra.class, apiRequest);
    }

    public static final void doUpdCtInfo(final Fragment fragment, CtacCra rCtacCra) {
        final String TAGNAME = "doUpdCtInfo";
        AQuery aq = new AQuery(fragment.getActivity());
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/updCtac";
        CtacCra ctacCra = rCtacCra.copyMe();
        ctacCra.setIApi(UPD_CTAC);
        ctacCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<CtacCra> apiRequest = new APIsRequest<CtacCra>(url, UPD_CTAC);
        apiRequest.setiCra(ctacCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);

        doAction(fragment.getActivity(), fragment, CtacCra.class, apiRequest);
    }

    public static final void doReadCtInfo(final Fragment fragment, CtacCra rCtacCra) {

        final String TAGNAME = "doReadCtInfo";
        AQuery aq = new AQuery(fragment.getActivity());
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/readCtac";

        CtacCra ctacCra = rCtacCra.copyMe();
        ctacCra.setIApi(READ_CTAC);
        ctacCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<CtacCra> apiRequest = new APIsRequest<CtacCra>(url, READ_CTAC);
        apiRequest.setiCra(ctacCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);

        doAction(fragment.getActivity(), fragment, CtacCra.class, apiRequest);
    }

    public static final void doGetShop(final Fragment fragment, ShopCra shopcra) {
        ///start
        final String TAGNAME = "doGetShop";
        AQuery aq = new AQuery(fragment.getActivity());
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/shopByArea";
        shopcra.setIApi(SHOP_BYA);
        //		shopcra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<ShopCra> apiRequest = new APIsRequest<ShopCra>(url, SHOP_BYA);
        apiRequest.setiCra(shopcra);

        // 2nd request
        progressManager = ProgressManager.getInstance(fragment.getActivity());
        progressManager.showProgressDialog(fragment.getActivity());
        doHeloOptional(fragment.getActivity(), fragment, ShopCra.class, apiRequest);

    }

    public static final void doLoginTest(final FragmentActivity activity, LgiCra rLgiCra, boolean isSavePwd) {
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/lgi";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(LGI);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        lgicra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        lgicra.getISpssRec().devId = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GCM_REGID), "");
        //		lgicra.getISpssRec().devId = ClnEnv.getDeviceID(activity);
        lgicra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        lgicra.getISpssRec().gni = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
        lgicra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";

        APIsRequest<LgiCra> apiRequest = new APIsRequest<LgiCra>(url, LGI);
        apiRequest.setiCra(lgicra);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        doHelo(activity, null, LgiCra.class, apiRequest);
    }


    public static final void doRegen(final Fragment frag, LgiCra rLgiCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/regenActCd";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(RG_ACTCD);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        lgicra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        lgicra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");
        //		lgicra.getISpssRec().devId = ClnEnv.getDeviceID(activity);
        lgicra.getISpssRec().lang = ClnEnv.getAppLocale(frag.getActivity());

        APIsRequest<LgiCra> apiRequest = new APIsRequest<LgiCra>(url, LGI);
        apiRequest.setiCra(lgicra);

        doHelo(frag.getActivity(), frag, LgiCra.class, apiRequest);

    }

    /*******************************
     *
     * DQ
     */
    public static final void doReadDistrict(FragmentActivity activity) {
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        HttpPost httpost = new HttpPost(String.format("%s%s", domainURL, "/dq_districts.txt"));
        String url = String.format("%s%s", "https://customerservice.pccw.com/cs/", "/dq_districts.txt");

        //		String url = domainURL +"/ma/regenActCd";
        DQAreas dqAreas = new DQAreas();

        APIsRequest<DQAreas> apiRequest = new APIsRequest<DQAreas>(url, LGI);
        apiRequest.setiCra(dqAreas);

        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);

        doHeloOptional(activity, null, DQAreas.class, apiRequest);
    }

    public static final void doDearDqry(FragmentActivity activity, DqryCra mDqryCra) {
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/dqry";
        DqryCra dqryCra = mDqryCra.copyMe();
        dqryCra.setIApi(DQRY);

        APIsRequest<DqryCra> apiRequest = new APIsRequest<DqryCra>(url, DQRY);
        apiRequest.setiCra(dqryCra);

        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);

        doHeloOptional(activity, null, DqryCra.class, apiRequest);
    }

    // get XML Response
    // IDD
    // Method 1: AQuery Method - Reference : http://code.google.com/p/android-query/wiki/AsyncAPI
    // light Weight version - may exist memory problem if XML too large
    public static final void doGetIDDCountries(final Fragment frag) {
        final String countryUrl = "http://www.pccw.com/staticfiles/PCCWCorpsite/EForms/IDDCodes/countrylist.xml";
        final String webviewurl = "http://www.pccw.com/Customer+Service/Directory+Inquiries/International+Telephone+Numbers/IDD+Codes+%26+Local+Times+Worldwide?language=zh_HK";
        ////original
        final AQuery aq = new AQuery(frag.getActivity());
        if (debug)
            Log.d("getCountryList", String.format("%s %s", "doGetIDDCountries2", "request sent:") + countryUrl);
        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());


        final AjaxCallback<XmlDom> cb = new AjaxCallback<XmlDom>() {
            @Override
            public void callback(String url, XmlDom xml, AjaxStatus status) {

                OnAPIsListener callback;
                APIsResponse response;
                Boolean isFailCase = false;
                HashMap<String, Object> hashMap = new HashMap<String, Object>();
                response = new APIsResponse();
                response.setActionTy(IDDCTY);
//				Log.i("Teseting", "Status" +status.getTime().toGMTString() +TextUtils.join(", ",status.getHeaders()));
//				Log.i("Teseting", "XML" + xml.text());
                try {
                    try {
                        callback = (OnAPIsListener) frag;
                    } catch (ClassCastException e) {
                        throw new ClassCastException(frag.toString() + " must implement OnAPIsListener");
                    }

                    if (status.getCode() != 200) {
                        response.setReply(HTTPSTATUS_NOT_200);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else {
                        //return success check if conurty list is null or not
                        if (xml != null && xml.tags("country") != null && !xml.tags("country").isEmpty()) {
                            List<XmlDom> countries = xml.tags("country");
                            List<IDDCity> cities;
                            List<IDDCountry> countriesList = new ArrayList<IDDCountry>();
                            for (XmlDom country : countries) {
                                IDDCountry countryItem = new IDDCountry();
                                countryItem.setEname(country.attr("ename"));
                                countryItem.setCname(country.attr("cname"));
                                countryItem.setVal(country.attr("val"));
                                if (debug)
                                    Log.d("getCountryList", String.format("%s %s", "GETCOUNTRYLIST", "countryItem :" + countryItem.getEname()));

                                cities = new ArrayList<IDDCity>();
                                for (XmlDom city : country.children("city")) {
                                    IDDCity cityItem = new IDDCity();
                                    cityItem.setEname(city.attr("ename"));
                                    cityItem.setCname(city.attr("cname"));
                                    cityItem.setVal(city.attr("val"));
                                    cities.add(cityItem);

                                    if (debug)
                                        Log.d("getCountryList", String.format("%s %s", "GETCOUNTRYLIST", "cityItem :" + cityItem.getEname()));
                                }
                                countryItem.setCity(cities);
                                countriesList.add(countryItem);
                            }

                            if (countriesList == null || countriesList.size() == 0) {
                                //if failed to retrive countrylist, exit this page
                                iddCountryError(frag);
                            } else {
                                response.setReply(Reply.getSucc());
                                response.setCra(countriesList);
                                callback.onSuccess(response);
                            }
                        } else {
                            //if failed to retrive countrylist, exit this page
                            iddCountryError(frag);
                        }
                    }
                    // dismiss
                    //	            	progressManager = ProgressManager.getInstance(activity);
                    progressManager.dismissProgressDialog(isFailCase);
                } catch (Exception e) {
                    progressManager.dismissProgressDialog(true);
                    if (debugCra) Log.d("", e.toString());
                }
            }
        };
        ////		cb.url(url).type(XmlDom.class).weakHandler(frag.getActivity(), "jsonCb");
        //		cb.header("Content-Type", "application/json; charset=utf-8");
        //		cb.header("Accept", "*/*");
        //		cb.header("Accept-Encoding", "gzip, deflate, sdch");
        //		cb.header("Accept-Language", "zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4");
        //		cb.header("Cache-Control", "no-cache");
        //		cb.header("Host", "www.pccw.com");
        //		cb.header("Pragma", "no-cache");
        //		cb.header("Proxy-Connection", "keep-alive");
        //		cb.header("X-Requested-With", "XMLHttpRequest");
        //


        //Load webview first, on webview finish loading, call

        WebView webview = new WebView(frag.getActivity());
        //        webview=(WebView)findViewById(R.id.webview1);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebChromeClient(new WebChromeClient());
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                aq.ajax(countryUrl, XmlDom.class, 1, cb);
//				view.clearCache(true);
//				view.clearHistory();
//				view.destroy();
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                aq.ajax(countryUrl, XmlDom.class, 1, cb);
                view.clearCache(true);
                view.clearHistory();
                view.destroy();
            }
            //        	    @Override
            //        	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //        	        // TODO Auto-generated method stub
            //        	        view.loadUrl(webviewurl);
            //        	        return true;
            //        	    }
        });
        webview.loadUrl(webviewurl);
    }

    private static void iddCountryError(final Fragment frag) {
        DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                frag.getActivity().finish();
                frag.getActivity().overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
            }
        };
        progressManager.dismissProgressDialog(true);
        DialogHelper.createSimpleDialog(frag.getActivity(), frag.getActivity().getString(R.string.myhkt_countrylist_failed), frag.getActivity().getString(R.string.btn_ok), onPositiveClickListener);
    }


    public static final void doGetIDDCodeResult(final Fragment frag, HashMap<String, String> hmSelectedCodeDetail) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String domain = "http://www.pccw.com/vgn-ext-templating/PCCWCorpsite/Template/EForm/IDDCodeResult.jsp?";
        //		String domain = domainURL+"/vgn-ext-templating/PCCWCorpsite/Template/EForm/IDDCodeResult.jsp?";
        //		String params = "iddCodeDetail.local_v1=USA&iddCodeDetail.local_v2=Boston&iddCodeDetail.dest_v1=China&iddCodeDetail.dest_v2=HONG KONG&iddCodeDetail.hr=18&iddCodeDetail.minute=00";
        //
        //		HashMap<String,String> hm = hmSelectedCodeDetail;
        String str = "";
        String local_v1 = hmSelectedCodeDetail.get("local_v1");//"China"; //country calling from
        String local_v2 = hmSelectedCodeDetail.get("local_v2");//"HONG KONG"; //city calling from
        String dest_v1 = hmSelectedCodeDetail.get("dest_v1");//"Denmark"; //country calling from
        String dest_v2 = hmSelectedCodeDetail.get("dest_v2");//"Copenhagen"; //city calling to
        String hr = hmSelectedCodeDetail.get("hr");//"11"; // hour calling from
        String min = hmSelectedCodeDetail.get("min");//"52"; // minute calling from
        str = str + "iddCodeDetail.local_v1=" + urlEncode(local_v1);
        str = str + "&iddCodeDetail.local_v2=" + urlEncode(local_v2);
        str = str + "&iddCodeDetail.dest_v1=" + urlEncode(dest_v1);
        str = str + "&iddCodeDetail.dest_v2=" + urlEncode(dest_v2);
        str = str + "&iddCodeDetail.hr=" + urlEncode(hr);
        str = str + "&iddCodeDetail.minute=" + urlEncode(min);

        String url = String.format("%s%s", domain, str);

        AQuery aq = new AQuery(frag.getActivity());
        if (debug)
            Log.d("doGetIDDCodeResult2", String.format("%s %s", "doGetIDDCodeResult2", "request sent:") + url);
        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        aq.ajax(url, XmlDom.class, new AjaxCallback<XmlDom>() {
            @Override
            public void callback(String url, XmlDom xml, AjaxStatus status) {
                OnAPIsListener callback;
                APIsResponse response;
                Boolean isFailCase = false;
                response = new APIsResponse();
                response.setActionTy(IDDCODE);
                try {
                    try {
                        callback = (OnAPIsListener) frag;

                    } catch (ClassCastException e) {
                        throw new ClassCastException(frag.toString() + " must implement OnAPIsListener");
                    }

                    if (status.getCode() != 200) {
                        response.setReply(HTTPSTATUS_NOT_200);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else if (xml == null) {
                        // TODO: httppost response null Object from api
                        response.setReply(JSON_RETURN_NULL);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else {

                        XmlDom country = xml;
                        IDDCodesNTimes codeNTime = new IDDCodesNTimes();
                        if (debug)
                            Log.d("doGetIDDCodeResult2", String.format("%s %s", "doGetIDDCodeResult2", "response receive2:") + xml.toString());
                        //				Log.d("doGetIDDCodeResult2", String.format("%s %s",  "doGetIDDCodeResult2", "response receive: Country Tag") + xml.tag("country").toString());

                        codeNTime.setCountryId(country.text("countryId"));
                        codeNTime.setCountryName(country.text("countryName"));
                        codeNTime.setCountryCode(country.text("countryCode"));
                        codeNTime.setAreaCode(country.text("areaCode"));
                        codeNTime.setTimeDiff(country.text("timeDiff"));
                        codeNTime.setTime(country.text("time"));
                        codeNTime.setCountryName2(country.text("countryName2"));
                        codeNTime.setHr(country.text("hr"));
                        codeNTime.setMin(country.text("min"));
                        codeNTime.setTimeDiff_dest(country.text("timeDiff_dest"));
                        codeNTime.setTimeDiff_local(country.text("timeDiff_local"));
                        codeNTime.setOtherInfo(country.text("otherInfo"));
                        if (debug)
                            Log.d("doGetIDDCodeResult2", String.format("%s %s", "doGetIDDCodeResult2", "codeNTime :" + codeNTime.getAreaCode()));
                        if (codeNTime == null) {
                            response.setReply(NULL_OBJECT_CRA);
                            response.setCra(codeNTime);
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            response.setReply(Reply.getSucc());
                            response.setCra(codeNTime);
                            callback.onSuccess(response);
                        }
                    }
                    // dismiss
                    //	            	progressManager = ProgressManager.getInstance(activity);
                    progressManager.dismissProgressDialog(isFailCase);
                } catch (Exception e) {
                    progressManager.dismissProgressDialog(true);
                    if (debugCra) Log.d("", e.toString());
                }
            }
        });
    }

    private static String IDDRateDestEndpoint = "IDD0060WS/rs/idd0060/destinations";

    public static final void doGetIDDRateDest(final Fragment frag) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);

        String domain = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_SHOPHOST), APIsManager.PRD_shop_host);
        String url = String.format("%s%s", domain, IDDRateDestEndpoint);
        AQuery aq = new AQuery(frag.getActivity());
        if (debug)
            Log.d("doGetIDDRateDest", String.format("%s %s", "doGetIDDRateDest", "request sent:") + url);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());
        aq.ajax(url, XmlDom.class, new AjaxCallback<XmlDom>() {
            @Override
            public void callback(String url, XmlDom xml, AjaxStatus status) {
                OnAPIsListener callback;
                APIsResponse response;
                Boolean isFailCase = false;
                response = new APIsResponse();
                response.setActionTy(IDDRATEDEST);
                try {
                    try {
                        callback = (OnAPIsListener) frag;

                    } catch (ClassCastException e) {
                        throw new ClassCastException(frag.toString() + " must implement OnAPIsListener");
                    }

                    if (status.getCode() != 200) {
                        response.setReply(HTTPSTATUS_NOT_200);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else if (xml == null) {
                        // TODO: httppost response null Object from api
                        response.setReply(JSON_RETURN_NULL);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else {
                        if (debug)
                            Utils.showLog("doGetIDDRateDest", String.format("%s %s", "doGetIDDRateDest", "response receive:") + xml.toString());

                        List<XmlDom> nlDestination = xml.children("Destination");
                        List<Destination> destinationList = new ArrayList<Destination>();
                        for (int i = 0; i < nlDestination.size(); i++) {
                            Destination destination = new Destination();
                            destination.engDestName = nlDestination.get(i).text("engDestName");
                            destination.engDestNameExt = nlDestination.get(i).text("engDestNameExt");
                            destination.chiDestName = nlDestination.get(i).text("chiDestName");
                            destination.chiDestNameExt = nlDestination.get(i).text("chiDestNameExt");
                            destinationList.add(destination);
                        }


                        if (destinationList == null) {
                            response.setReply(NULL_OBJECT_CRA);
                            response.setCra(destinationList);
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            response.setReply(Reply.getSucc());
                            response.setCra(destinationList);
                            callback.onSuccess(response);
                        }
                    }
                    // dismiss
                    //	            	progressManager = ProgressManager.getInstance(activity);
                    progressManager.dismissProgressDialog(isFailCase);
                } catch (Exception e) {
                    progressManager.dismissProgressDialog(true);
                    if (debugCra) Log.d("", e.toString());
                }
            }
        });


    }


    private static String IDDRateResultEndpoint = "IDD0060WS/rs/idd0060/customer";

    public static final void doGetIDRateResult(final Fragment frag, HashMap<String, String> hmSelectedDestDetail) {
        String domain = ClnEnv.getPref(frag.getActivity(), frag.getActivity().getString(R.string.CONST_PREF_SHOPHOST), APIsManager.PRD_shop_host);

        String str = "";
        String customernum = hmSelectedDestDetail.get("customernum");//"China"; //country calling from
        String servicenum = hmSelectedDestDetail.get("servicenum");//"HONG KONG"; //city calling from
        String srvtype = hmSelectedDestDetail.get("srvtype");//"Denmark"; //country calling from
        String dest = hmSelectedDestDetail.get("dest");//"Copenhagen"; //city calling to
        String destext = hmSelectedDestDetail.get("destext");//"11"; // hour calling from
        if (destext == null) {
            destext = "";
        }
        str = str + "/" + urlEncode(customernum);
        str = str + "/srps?servicenum=" + urlEncode(servicenum);
        str = str + "&srvtype=" + urlEncode(srvtype);
        str = str + "&dest=" + urlEncode(dest);
        str = str + "&destext=" + urlEncode(destext);
        String url = String.format("%s%s%s", domain, IDDRateResultEndpoint, str);
//		String url = String.format("%s%s%s", UAT_domain,IDDRateResultEndpoint, str);
        AQuery aq = new AQuery(frag.getActivity());
        if (debug)
            Log.d("doGetIDRateResult", String.format("%s %s", "doGetIDRateResult", "request sent:") + url);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());
        try {
            aq.ajax(url, XmlDom.class, new AjaxCallback<XmlDom>() {
                @Override
                public void callback(String url, XmlDom xml, AjaxStatus status) {
                    OnAPIsListener callback;
                    APIsResponse response;
                    Boolean isFailCase = false;
                    response = new APIsResponse();
                    response.setActionTy(IDDRATERESULT);
                    try {
                        try {
                            callback = (OnAPIsListener) frag;

                        } catch (ClassCastException e) {
                            throw new ClassCastException(frag.toString() + " must implement OnAPIsListener");
                        }

                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            String msg = null;
                            int statusCode = status.getCode();
                            if (status.getError() != null) {
                                XmlDom xD = new XmlDom(status.getError());
                                statusCode = -1;
                                msg = xD.getElement().getElementsByTagName("errorCode").item(0).getTextContent();
                            } else {
                                msg = status.getMessage();
                            }
                            response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), statusCode, msg));
                            callback.onFail(response);

                            isFailCase = true;
                        } else if (xml == null) {
                            // TODO: httppost response null Object from api
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            if (debug)
                                Log.d("doGetIDRateResult", String.format("%s %s", "doGetIDRateResult", "response receive:") + xml.toString());

                            SRPlan srPlan = new SRPlan();
                            srPlan.serviceType = xml.text("serviceType");
                            srPlan.engDestName = xml.text("engDestName");
                            srPlan.engDestNameExt = xml.text("engDestNameExt");
                            srPlan.chiDestName = xml.text("chiDestName");
                            srPlan.chiDestNameExt = xml.text("chiDestNameExt");

                            List<XmlDom> rates = xml.tag("rates").children("Rate");
                            List<Rate> rateList = new ArrayList<Rate>();
                            for (int i = 0; i < rates.size(); i++) {
                                Rate rate = new Rate();
                                rate.timeZone = rates.get(i).text("timeZone");
                                rate.chiTimeZone = rates.get(i).text("chiTimeZone");
                                rate.timeZoneRemarks = rates.get(i).text("timeZoneRemarks");
                                rate.chiTimeZoneRemarks = rates.get(i).text("chiTimeZoneRemarks");
                                rate.rate = rates.get(i).text("rate");
                                rateList.add(rate);
                            }
                            srPlan.rateList = (ArrayList<Rate>) rateList;

                            if (srPlan == null) {
                                response.setReply(NULL_OBJECT_CRA);
                                response.setCra(srPlan);
                                callback.onFail(response);
                                isFailCase = true;
                            } else {
                                response.setReply(Reply.getSucc());
                                response.setCra(srPlan);
                                callback.onSuccess(response);
                            }
                        }
                        // dismiss
                        //	            	progressManager = ProgressManager.getInstance(activity);
                        progressManager.dismissProgressDialog(isFailCase);
                    } catch (Exception e) {
                        progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            });
        } catch (Exception e) {
            progressManager.dismissProgressDialog(true);
        }
    }

    private static String urlEncode(String url) {
        String encodedurl = "";
        try {
            encodedurl = URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodedurl;
    }


    public static final void doGetPlanMob(Fragment frag, PlanMobCra mPlanMobCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/planMob";
        PlanMobCra planMobCra = mPlanMobCra.copyMe();
        planMobCra.setIApi(PLAN_MOB);
        planMobCra.setIGwtGud(ClnEnv.getSessTok());
        APIsRequest<PlanMobCra> apiRequest = new APIsRequest<PlanMobCra>(url, PLAN_MOB);
        apiRequest.setiCra(planMobCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanMobCra.class, apiRequest);
    }


    public static final void doRegenActCode(FragmentActivity act, LgiCra mLgiCra) {
        Context cxt = act;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/regenActCd";
        LgiCra lgiCra = mLgiCra.copyMe();
        lgiCra.setIApi(RG_ACTCD);
        APIsRequest<LgiCra> apiRequest = new APIsRequest<LgiCra>(url, RG_ACTCD);
        apiRequest.setiCra(lgiCra);

        progressManager = ProgressManager.getInstance(act);
        progressManager.showProgressDialog(act);

        doAction(act, null, LgiCra.class, apiRequest);

    }

    public static final void doSMSActivation(FragmentActivity act, AcMainCra mAcMainCra) {
        Context cxt = act;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/smsAct";
        AcMainCra acMainCra = mAcMainCra.copyMe();
        acMainCra.setIApi(SMSACT);
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call
        APIsRequest<AcMainCra> apiRequest = new APIsRequest<AcMainCra>(url, SMSACT);
        apiRequest.setiCra(acMainCra);

        progressManager = ProgressManager.getInstance(act);
        progressManager.showProgressDialog(act);

        doAction(act, null, AcMainCra.class, apiRequest);
    }

    public static final void doTopupHist(Fragment frag, PlanMobCra mPlanMobCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/tpupHist";
        PlanMobCra planMobCra = mPlanMobCra.copyMe();
        planMobCra.setIApi(TPUP_HIST);
        planMobCra.setIGwtGud(ClnEnv.getSessTok());
        planMobCra.setITopupActn(PlanMobCra.TP_ACTN_I);
        APIsRequest<PlanMobCra> apiRequest = new APIsRequest<PlanMobCra>(url, TPUP_HIST);
        apiRequest.setiCra(planMobCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanMobCra.class, apiRequest);
    }

    public static final void doTopupItem(Fragment frag, PlanMobCra mPlanMobCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/tpup";
        PlanMobCra planMobCra = mPlanMobCra.copyMe();
        planMobCra.setIApi(TPUP);
        planMobCra.setIGwtGud(ClnEnv.getSessTok());
        planMobCra.setITopupActn(PlanMobCra.TP_ACTN_I);
        APIsRequest<PlanMobCra> apiRequest = new APIsRequest<PlanMobCra>(url, TPUP);
        apiRequest.setiCra(planMobCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanMobCra.class, apiRequest);
    }

    public static final void doTopup(Fragment frag, PlanMobCra mPlanMobCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/tpup";
        PlanMobCra planMobCra = mPlanMobCra.copyMe();
        planMobCra.setIApi(TPUP);
        planMobCra.setIGwtGud(ClnEnv.getSessTok());
        planMobCra.setITopupActn(PlanMobCra.TP_ACTN_C);
        APIsRequest<PlanMobCra> apiRequest = new APIsRequest<PlanMobCra>(url, TPUP);
        apiRequest.setiCra(planMobCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanMobCra.class, apiRequest);
    }

    /****************************
     *
     * Plan
     *
     ****************************/
    public static final void doGetPlanIMS(Fragment frag, PlanImsCra mPlanImsCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/planIms";
        PlanImsCra planImsCra = mPlanImsCra.copyMe();
        planImsCra.setIApi(PLAN_IMS);
        planImsCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<PlanImsCra> apiRequest = new APIsRequest<PlanImsCra>(url, PLAN_IMS);
        apiRequest.setiCra(planImsCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanImsCra.class, apiRequest);
    }

    public static final void doGetPlanLts(Fragment frag, PlanLtsCra mPlanLtsCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/planLts";
        PlanLtsCra planLtsCra = mPlanLtsCra.copyMe();
        planLtsCra.setIApi(PLAN_LTS);
        planLtsCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<PlanLtsCra> apiRequest = new APIsRequest<PlanLtsCra>(url, PLAN_LTS);
        apiRequest.setiCra(planLtsCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanLtsCra.class, apiRequest);
    }

    public static final void doEnquireSR4NoPend(Fragment frag, ApptCra mApptCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/srEnqNP";
        ApptCra apptCra = mApptCra.copyMe();
        apptCra.setIApi(SR_ENQ_NP);
        apptCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<ApptCra> apiRequest = new APIsRequest<ApptCra>(url, SR_ENQ_NP);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, ApptCra.class, apiRequest);
    }

    /*************************************
     *
     * Line Test
     *
     * ***********************************/
    public static final void doSubmitLntt(IntentService service, LnttCra mLnttCra) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/lntt";
        LnttCra apptCra = mLnttCra.copyMe();
        apptCra.setIApi(LNTT);
        apptCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<LnttCra> apiRequest = new APIsRequest<LnttCra>(url, LNTT);
        apiRequest.setiCra(apptCra);

        doAction(null, null, service, LnttCra.class, apiRequest, true);
    }

    public static final void doResetModem(IntentService service, LnttCra mLnttCra) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/rtrt";
        LnttCra apptCra = mLnttCra.copyMe();
        apptCra.setIApi(RTRT);
        apptCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<LnttCra> apiRequest = new APIsRequest<LnttCra>(url, RTRT);
        apiRequest.setiCra(apptCra);

        doAction(null, null, service, LnttCra.class, apiRequest, true);
    }

    public static final void doGetAvailableTimeSlot(final Fragment frag, ApptCra mApptCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/srAvaTs";
        ApptCra apptCra = mApptCra.copyMe();
        apptCra.setIApi(SR_AVA_TS);
        apptCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<ApptCra> apiRequest = new APIsRequest<ApptCra>(url, SR_AVA_TS);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, ApptCra.class, apiRequest);
    }


    public static final void doCheckUpdate(final IntentService service) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
        String url = String.format("%s%s", domainURL, "/mba/biz/data/myhkt_version_android.txt");
        AQuery aq = new AQuery(service.getApplicationContext());
        try {
            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<JSONObject>() {
                OnAPIsListener callback;
                APIsResponse response;
                VersionCheck cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    Boolean isFailCase = false;
                    //	            	mCookies = status.getCookies();
                    try {
                        try {
                            callback = (OnAPIsListener) service;

                        } catch (ClassCastException e) {
                            throw new ClassCastException(service.toString() + " must implement OnAPIsListener");
                        }

                        response = new APIsResponse();
                        //						response.setActionTy(apiRequest.getActionTy());

                        try {

                        } catch (Exception e) {
                            e.toString();
                        }
                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(service.getBaseContext(), status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } else if (json == null) {
                            // TODO: httppost response null Object from api
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(service.getBaseContext(), status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } else {
                            String resultStr = json.toString();

                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s", "VersionCheck", "response received"), resultStr);
                            if (debugCra)
                                Log.d("VersionCheck", String.format("%s %s", "VersionCheck", "response received:") + resultStr);

                            try {
                                cra = gson.fromJson(resultStr, VersionCheck.class);
                                response.setCra(cra);
                                callback.onSuccess(response);
                            } catch (Exception e) {
                                if (debug)
                                    Log.e("VersionCheck", "/gson.fromJson error:" + e.getMessage());
                                response.setReply("Unexpected Error");
                                response.setMessage(ClnEnv.getRPCErrMsg(service.getBaseContext(), "Unexpected Error"));
                                callback.onFail(response);
                                e.printStackTrace();
                            }

                        }
                    } catch (Exception e) {
                        progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            };
            //			for (Cookie cookie : mCookies) {
            //				ajaxCallback.cookie(cookie.getName(), cookie.getValue());
            //				//Here we are setting the cookie info.
            //			}
            aq.ajax(url, null, JSONObject.class, ajaxCallback);

        } catch (Exception e) {
            e.toString();
            e.toString();
            OnAPIsListener callback;

            try {
                if (service != null) {
                    callback = (OnAPIsListener) service;

                    APIsResponse response;
                    response = new APIsResponse();
                    response.setReply("Unexpected Error");
                    response.setMessage(ClnEnv.getRPCErrMsg(service.getBaseContext(), "Unexpected Error"));
                    callback.onFail(response);
                }
            } catch (ClassCastException e2) {
                throw new ClassCastException(service.toString() + " must implement OnAPIsListener");
            } catch (Exception e3) {
                if (debugCra) Log.d("", e3.toString());
            }
        }
    }

    public static final void doAppendUserInfo(FragmentActivity act, AcMainCra mAcMainCra) {
        Context cxt = act;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/eclosion";
        AcMainCra acMainCra = mAcMainCra.copyMe();
        acMainCra.setIApi(ECLOSION);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<AcMainCra>(url, ECLOSION);
        apiRequest.setiCra(acMainCra);

        doAction(act, null, AcMainCra.class, apiRequest);
    }

    public static final <T extends BaseCraEx> void doHeloOptional(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest) {
        if (ClnEnv.getSessTok() != null && !"".equals(ClnEnv.getSessTok())) {
            doAction(activity, frag, typeClass, apiRequest);
        } else {
            doHelo(activity, frag, typeClass, apiRequest);
        }
    }

    public static final <T extends BaseCraEx> void doHeloOptional(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest, final Boolean isLastInLoop) {
        if (ClnEnv.getSessTok() != null && !"".equals(ClnEnv.getSessTok())) {
            doAction(activity, frag, typeClass, apiRequest, isLastInLoop);
        } else {
            doHelo(activity, frag, typeClass, apiRequest, isLastInLoop);
        }
    }

    public static final <T extends BaseCraEx> void doAction(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest) {
        doAction(activity, frag, null, typeClass, apiRequest, true);
    }

    public static final <T extends BaseCraEx> void doAction(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest, final Boolean isLastInLoop) {
        doAction(activity, frag, null, typeClass, apiRequest, isLastInLoop);
    }

    private static final <T extends BaseCraEx> void doAction(final FragmentActivity activity, final Fragment frag, final IntentService service, final Class<T> typeClass, final APIsRequest<T> apiRequest, final Boolean isLastinLoop) {
        if (service != null) {
            initDebugMode(service.getApplicationContext());
        } else if (frag != null) {
            initDebugMode(Objects.requireNonNull(frag.getActivity()));
        } else {
            initDebugMode(activity);
        }
        AQuery aq = new AQuery(service != null ? service.getApplicationContext() : activity);
        String url = apiRequest.getPath();
        T icra = apiRequest.getiCra();
        icra.setIGwtGud(ClnEnv.getSessTok());

        String jstr;
        if (icra instanceof CtacCra || icra instanceof AcMainCra) {
            jstr = gson2.toJson(icra);
        } else {
            jstr = gson.toJson(icra);
        }
        progressManager.checkNull();
        if (debugGrq)
            ClnEnv.toLogFile(String.format("%s %s", apiRequest.getActionTy(), "request sent"), jstr);
        if (debugCra)
            Utils.showLog((service != null ? service.getClass().toString() : (frag != null ? frag.getClass().toString() : activity.getClass().toString())) + ">>" + apiRequest.getActionTy(), url + "/" + String.format("%s %s", apiRequest.getActionTy(), "request sent:") + jstr);

        try {
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(AQuery.POST_ENTITY, entity);
            progressManager.checkNull();
            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<JSONObject>() {
                OnAPIsListener callback;
                APIsResponse response;
                T cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    boolean isFailCase = false;
                    Context cxt = null;
                    progressManager.checkNull();
                    //				    mCookies = status.getCookies();
                    try {
                        try {
                            if (service != null) {
                                callback = (OnAPIsListener) service;
                                cxt = service.getBaseContext();
                            } else if (frag != null) {
                                callback = (OnAPIsListener) frag;
                                cxt = frag.getActivity();
                            } else {
                                callback = (OnAPIsListener) activity;
                                cxt = activity;
                            }
                        } catch (ClassCastException e) {
                            throw new ClassCastException((service != null ? service.toString() : cxt.toString()) + " must implement OnAPIsListener");
                        }
                        progressManager.checkNull();
                        response = new APIsResponse();
                        response.setActionTy(apiRequest.getActionTy());
                        try {
                            cra = typeClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            if (status.getCode() == 500) {
                                if (ClnEnv.isLoggedIn()) {
                                    if (debug) Log.i("APIManager", "Login 500");
                                    response.getReply().setCode(RC.TCSESS_MM);
                                } else {
                                    if (debug) Log.i("APIManager", "No Login 500");
                                    response.getReply().setCode(RC.TCSESS_EXP);
                                }
                                ClnEnv.setHeloCra(null);
                                isFailCase = true;
                                progressManager.dismissProgressDialog(isFailCase);
                                callback.onFail(response);
                            } else {
                                response.setMessage(ClnEnv.getRPCErrMsg(cxt, status.getCode(), status.getMessage()));
                                callback.onFail(response);
                                isFailCase = true;
                            }
                        } else if (json == null) {
                            // TODO: httppost response null Object from api
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(cxt, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            progressManager.checkNull();
                            String resultStr = json.toString();
                            //for API testing, modify if needed. TODO
                            resultStr = sampleData(cxt, apiRequest, resultStr);

                            //special string handling for DQ
                            if (DQRY.equalsIgnoreCase(apiRequest.getActionTy()))
                                resultStr = resultStr.replaceAll("簕", "滙").replaceAll("𥝲", "鰂").replaceAll("𡧛", "邨").replaceAll("𦲁", "劵");

                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s %s", apiRequest.getActionTy(), cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Utils.showLog(apiRequest.getActionTy(), String.format("%s %s %s", apiRequest.getActionTy(), cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                cra = gson.fromJson(resultStr, typeClass); //debug
                            } catch (Exception e) {
                                if (debug)
                                    Log.e(apiRequest.getActionTy() + "/gson.fromJson error:", e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || !cra.getReply().isSucc()) {
                                if (cra == null) {
                                    try {
                                        cra = typeClass.newInstance();
                                        cra.setReply(NULL_OBJECT_CRA);
                                    } catch (Exception e) {
                                        e.toString();
                                    }
                                }
                                //Except the following case , other need to turn RC_ALT to RC_RC_TCESS_MM
                                //Force Relogin
                                //1		.Read Profile contact
                                //2		.Update Profile contact
                                //3		.Update Profile / Change PWD
                                //4		.Update Alias /Service List on Profile Page
                                //5		.Update Alias on Service List
                                if (RC.ALT.equals(cra.getReply().getCode())) {
                                    if (cra.getIApi().equals(APIsManager.READ_CTAC) ||
                                            cra.getIApi().equals(APIsManager.UPD_CTAC) ||
                                            cra.getIApi().equals(APIsManager.ACMAIN) ||
                                            cra.getIApi().equals(APIsManager.SVCASO) ||
                                            cra.getIApi().equals(APIsManager.SVCASO4SUBN)) {

                                    } else {
                                        cra.setReply(RC.TCSESS_MM);
                                    }
                                }
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onFail(response);
                                isFailCase = true;
                                progressManager.dismissProgressDialog(isFailCase);
                            } else {
                                progressManager.checkNull();
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onSuccess(response);
                            }
                        }
                        progressManager.checkNull();
                        // dismiss
                        // rogressManager = ProgressManager.getInstance(activity);
                        if (APIsManager.AO_ADDON.equals(response.getActionTy()) && !isFailCase) {
                            progressManager.popAsyncCount();
                        } else if (APIsManager.CHK_BILL.equals(response.getActionTy()) && isFailCase) {
                            if (isLastinLoop) {
                                progressManager.dismissProgressDialog(isFailCase);
                            } else {
                                progressManager.popAsyncCount();
                            }
                        } else {
                            progressManager.dismissProgressDialog(isFailCase);
                        }
                    } catch (Exception e) {
                        progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            };
            if (mCookies != null && mCookies.size() > 0) {
                for (Cookie cookie : mCookies) {
                    ajaxCallback.cookie(cookie.getName(), cookie.getValue());
                    //Here we are setting the cookie info.
                }
            }
            ajaxCallback.timeout(60000);
            aq.ajax(url, params, JSONObject.class, ajaxCallback);

        } catch (Exception e) {
            progressManager.dismissProgressDialog(true);
            OnAPIsListener callback;

            try {
                if (service != null) {
                    callback = (OnAPIsListener) service;
                } else if (frag != null) {
                    callback = (OnAPIsListener) frag;
                } else {
                    callback = (OnAPIsListener) activity;
                }

                APIsResponse response;
                response = new APIsResponse();
                response.setActionTy(apiRequest.getActionTy());
                response.setReply(e.toString());
                response.setMessage(ClnEnv.getRPCErrMsg(activity, e.toString()));
                callback.onFail(response);
            } catch (ClassCastException e2) {
                throw new ClassCastException((service != null ? service.toString() : activity.toString()) + " must implement OnAPIsListener");
            } catch (Exception e3) {
                if (debugCra) Log.d("", e3.toString());
            }
            progressManager.dismissProgressDialog(true);
        }
    }

    public static final <T extends BaseCraEx> void doHelo(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest) {
        doHelo(activity, frag, typeClass, apiRequest, true);
    }

    public static final <T extends BaseCraEx> void doHelo(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest, final Boolean isLastInLoop) {
        Context cxt = activity != null ? activity : frag.getActivity();
        final AQuery aq = new AQuery(activity);
        // doHelo first
        final String TAGNAME = HELO;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/helo";
        HeloCra heloCra = new HeloCra();
        heloCra.setIApi(HELO);

        //including spssCra on every dohelo
        SpssCra spssCra = new SpssCra();

        spssCra.setICkSum(Utils.sha256(ClnEnv.getPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt)));
        if (ClnEnv.isLoggedIn() || typeClass == LgiCra.class) {
            if (typeClass == LgiCra.class) {
                LgiCra lgicra = (LgiCra) apiRequest.getiCra();
                spssCra.setISpssRec(lgicra.getISpssRec());
                spssCra.setILoginId(lgicra.getISveeRec().loginId);
            } else if (ClnEnv.isLoggedIn()) {
                spssCra.setISpssRec(ClnEnv.getLgiCra().getISpssRec());
                spssCra.setILoginId(ClnEnv.getLoginId());
            }
        } else {
            //if not doing login or save login
            spssCra.getISpssRec().devTy = SpssRec.TY_ANDROID;
            spssCra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");

            //				spssCra.getISpssRec().devId = ClnEnv.getDeviceID(cxt);
            spssCra.getISpssRec().lang = ClnEnv.getAppLocale(cxt);
            spssCra.getISpssRec().gni = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
            //				spssCra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";
        }
        heloCra.setISpssCra(spssCra);

        String jstr = gson.toJson(heloCra);
        if (debugGrq) ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
        if (debugCra) Log.d(TAGNAME, String.format("%s %s", TAGNAME, "request sent:") + jstr);

        try {
            // 1st request
            progressManager.showProgressDialog(activity);
            // 1st call doHelo
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(AQuery.POST_ENTITY, entity);

            aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                OnAPIsListener callback;
                APIsResponse response;
                HeloCra cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    Boolean isFailCase = false;
                    mCookies = status.getCookies();
                    try {
                        try {
                            if (frag != null) {
                                callback = (OnAPIsListener) frag;
                            } else {
                                callback = (OnAPIsListener) activity;
                            }
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
                        }

                        response = new APIsResponse();
                        response.setActionTy(HELO);
                        cra = new HeloCra();
                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else if (json == null) {
                            // TODO: httppost response null Object from api
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            String resultStr = json.toString();
                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Log.d(TAGNAME, String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                Type collectionType = new TypeToken<HeloCra>() {
                                }.getType();
                                cra = gson.fromJson(json.toString(), collectionType);
                            } catch (Exception e) {
                                if (debug) Log.e(TAGNAME, "/gson.fromJson error:" + e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || !cra.getReply().isSucc()) {
                                if (cra == null) {
                                    cra = new HeloCra();
                                    cra.setReply(NULL_OBJECT_CRA);
                                }
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onFail(response);
                                isFailCase = true;
                            } else {
                                //set mobile number prefix
                                String mobPfx = cra.getOClnCfg().getProp().get("CLN_MOB_PFX");
                                String ltsPfx = cra.getOClnCfg().getProp().get("CLN_LTS_PFX");
                                if (mobPfx != null && mobPfx.length() > 0)
                                    ClnEnv.setPref(activity, "mobPfx", mobPfx);
                                if (ltsPfx != null && ltsPfx.length() > 0)
                                    ClnEnv.setPref(activity, "ltsPfx", ltsPfx);

                                ClnEnv.setHeloCra(cra);
                                // 2nd call
                                doAction(activity, frag, typeClass, apiRequest, isLastInLoop);
                                //	            			aq.ajax(apiRequest.getPath(), params2, JSONObject.class, ajaxCallback);
                                // continue the second Call
								/*	            			response.setReply(cra.getReply());
	            			response.setCra(cra);
	            			callback.onSuccess(response);*/
                            }
                        }
                        // dismiss
                        //	            	progressManager = ProgressManager.getInstance(activity);
                        progressManager.dismissProgressDialog(isFailCase);

                    } catch (Exception e) {
                        progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            });
        } catch (Exception e1) {
            e1.toString();
            // dismiss
            //        	progressManager = ProgressManager.getInstance(activity);
            progressManager.dismissProgressDialog(true);
        }
    }

    //Settings
    public static final void doUpdSmph(final FragmentActivity activity, SpssCra rSpssCra) {
        final String TAGNAME = "doUpdSmph";
        Context cxt = activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/spssUpd";
        SpssCra spsscra = rSpssCra.copyMe();
        spsscra.setIApi(SPSS_UPD);

        spsscra.setIGwtGud(ClnEnv.getSessTok());
        spsscra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getSessionLoginID() : "");
        spsscra.setICkSum(Utils.sha256(ClnEnv.getPref(activity.getApplicationContext(), activity.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt)));
//		spsscra.setICkSum(Utils.sha256("" + "A", ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt)));

        spsscra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        spsscra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");
//		spsscra.getISpssRec().devId = "";
        spsscra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        //please note that bni and gni are defined before this call.

        APIsRequest<SpssCra> apiRequest = new APIsRequest<SpssCra>(url, SPSS_UPD);
        apiRequest.setiCra(spsscra);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        if (ClnEnv.isLoggedIn()) {
            doHeloOptional(activity, null, SpssCra.class, apiRequest);
        } else {
            doHelo(activity, null, SpssCra.class, apiRequest);
        }
    }

    //MyMob verify
    public static final void doVerify(final Fragment frag, AddOnCra rAddonCra) {
        final String TAGNAME = "doVerify";
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/aoAddOn";
        AddOnCra addoncra = rAddonCra.copyMe();
        addoncra.setIApi(AO_ADDON);

        APIsRequest<AddOnCra> apiRequest = new APIsRequest<AddOnCra>(url, AO_ADDON);
        apiRequest.setiCra(addoncra);
        if (debug) Log.i("ADD ACCOUNT", "ADD ACCOUNT");
        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);

        doHeloOptional(null, frag, AddOnCra.class, apiRequest);
    }

    //MyMob Authen
    public static final void doAuthen(final Fragment frag, AddOnCra rAddonCra) {
        final String TAGNAME = "doAuthen";
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/aoAuth";
        AddOnCra addoncra = rAddonCra.copyMe();
        addoncra.setISms(false);
        addoncra.setIApi(AO_AUTH);

        APIsRequest<AddOnCra> apiRequest = new APIsRequest<AddOnCra>(url, AO_AUTH);
        apiRequest.setiCra(addoncra);
        if (debug) Log.i("AUTHEN", "AUTHEN");
        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(null, frag, AddOnCra.class, apiRequest);
    }

    public static final void doSvcAso(final FragmentActivity activity, final Fragment fragment, AcMainCra racMainCra) {
        final String TAGNAME = "doSvcAso";
        Context cxt = activity == null ? fragment.getActivity() : activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/svcAso";
        AcMainCra acMainCra = racMainCra.copyMe();
        acMainCra.setIApi(SVCASO);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<AcMainCra>(url, SVCASO);
        apiRequest.setiCra(acMainCra);

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(activity, fragment, AcMainCra.class, apiRequest);
    }

    public static final void doSvcAso4Subn(final FragmentActivity activity, final Fragment fragment, AcMainCra racMainCra) {
        final String TAGNAME = "doSvcAso4Subn";
        Context cxt = activity == null ? fragment.getActivity() : activity;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/svcAso4Subn";
        AcMainCra acMainCra = racMainCra.copyMe();
        acMainCra.setIApi(SVCASO4SUBN);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<AcMainCra>(url, SVCASO4SUBN);
        apiRequest.setiCra(acMainCra);

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(activity, fragment, AcMainCra.class, apiRequest);
    }

    public static final void doGetBill(final Fragment fragment, BinqCra rbinqCra) {
        final String TAGNAME = "doGetBill";
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/bill";
        BinqCra binqCra = rbinqCra.copyMe();
        binqCra.setIApi(BILL);
        binqCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BinqCra> apiRequest = new APIsRequest<BinqCra>(url, BILL);
        apiRequest.setiCra(binqCra);

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(fragment.getActivity(), fragment, BinqCra.class, apiRequest);
    }

    public static final void doGetBillInfo(final Fragment fragment, BiifCra rbiifCra) {
        final String TAGNAME = "doGetBillInfo";
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/readBiif";
        BiifCra biifCra = rbiifCra.copyMe();
        biifCra.setIApi(READ_BIIF);
        biifCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BiifCra> apiRequest = new APIsRequest<BiifCra>(url, READ_BIIF);
        apiRequest.setiCra(biifCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(fragment.getActivity(), fragment, BiifCra.class, apiRequest);
    }

    public static final void doUpdatePayMeth(final Fragment fragment, BiifCra rbiifCra) {
        final String TAGNAME = "doUpdatePayMeth";
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/reqUpdTok4LtsPi";
        BiifCra biifCra = rbiifCra.copyMe();
        biifCra.setIApi(REQ_UPDTOK4LTSPI);
        biifCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BiifCra> apiRequest = new APIsRequest<BiifCra>(url, REQ_UPDTOK4LTSPI);
        apiRequest.setiCra(biifCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(fragment.getActivity(), fragment, BiifCra.class, apiRequest);
    }


    public static final void doGetShopByGC(final Fragment fragment, ShopCra rShopCra) {
        final String TAGNAME = "doGetShopByGC";
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/shopByGC";
        ShopCra shopCra = rShopCra.copyMe();
        shopCra.setIApi(SHOP_BYGC);
        //		shopCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<ShopCra> apiRequest = new APIsRequest<ShopCra>(url, SHOP_BYGC);
        apiRequest.setiCra(shopCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ShopCra.class, apiRequest);
    }

    public static final void doUpdBillinfo(final Fragment fragment, BiifCra rbiifCra) {
        final String TAGNAME = "doUpdBillinfo";
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/updBiif";

        BiifCra biifCra = rbiifCra.copyMe();
        biifCra.setIApi(UPD_BIIF);
        biifCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BiifCra> apiRequest = new APIsRequest<BiifCra>(url, UPD_BIIF);
        apiRequest.setiCra(biifCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(fragment.getActivity(), fragment, BiifCra.class, apiRequest);
    }


    public static final void doShowBill(final Fragment fragment, BiifCra rbiifCra) {
        final String TAGNAME = "doShowBill";
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/svlt/showBill";
    }

    public static final void doGetPlanTv(final Fragment fragment, PlanTvCra rplanTvCra) {
        final String TAGNAME = "doGetPlanTv";
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/planTv";
        PlanTvCra planTvCra = rplanTvCra.copyMe();
        planTvCra.setIApi(PLAN_TV);
        //		shopCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<PlanTvCra> apiRequest = new APIsRequest<PlanTvCra>(url, PLAN_TV);
        apiRequest.setiCra(planTvCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, PlanTvCra.class, apiRequest);
    }

    public static final void doCheckBillSts(final Fragment fragment, BinqCra rbinqCra, Boolean isLastInLoop) {
        final String TAGNAME = "doCheckBillSts";
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/chkBill";
        BinqCra binqCra = rbinqCra.copyMe();
        binqCra.setIApi(CHK_BILL);
        //		shopCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BinqCra> apiRequest = new APIsRequest<BinqCra>(url, CHK_BILL);
        apiRequest.setiCra(binqCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, BinqCra.class, apiRequest, isLastInLoop);
    }

    public static final void doDownloadPDF(final Fragment fragment, BinqCra rbinqCra, String lobString, String cusNum, int clickBill, String lang) {
        final Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String filePath; // = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            filePath = cxt.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath();
        } else {
            filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        }
        String endpoint = "/svlt/showBill";
        AQuery aq = new AQuery(fragment.getActivity());

        ///testing
        String urlFormat = "?p0=%s&p1=%s&p2=%s&p3=%s&p4=%s&p5=%s";
        String filenameFormat = "%s-%s.pdf";

        Bill[] billary = rbinqCra.getOBillList().getOBillAry();

        String iLobString = "".equals(lobString) ? " " : lobString;
        String systy = "".equals(billary[clickBill].getSysTy()) ? " " : billary[clickBill].getSysTy();
        String iCusNum = "".equals(cusNum) ? " " : cusNum;
        String iAcctNum = "".equals(billary[clickBill].getAcctNum()) ? " " : billary[clickBill].getAcctNum();
        String iInvDate = "".equals(billary[clickBill].getInvDate()) ? " " : billary[clickBill].getInvDate();
        String iLang = "".equals(lang) ? " " : lang;

        String url = String.format(urlFormat, iLobString, systy, iCusNum, iAcctNum, iInvDate, iLang);
        url = url.replaceAll(" ", "%20");
        url = String.format("%s%s%s", domainURL, endpoint, url);
        String filename = String.format(filenameFormat, billary[clickBill].getAcctNum(), billary[clickBill].getInvDate());

        //dummy pdf data for testing
        //		String 	url  = "http://www.orimi.com/pdf-test.pdf";
        //		String filename = "test.pdf";

        //set saved file destination
        final File target = new File(filePath, filename);

        //AJAX callback
        AjaxCallback<File> ajaxCallback = new AjaxCallback<File>() {
            @Override
            public void callback(String url, File file, AjaxStatus status) {
                OnAPIsListener callback;
                APIsResponse response;
                try {
                    try {
                        progressManager = ProgressManager.getInstance(cxt);
                        progressManager.showProgressDialog(cxt);
                        callback = (OnAPIsListener) fragment;
                    } catch (ClassCastException e) {
                        throw new ClassCastException(fragment.getActivity().toString() + " must implement OnAPIsListener");
                    }
                    // mCookies = status.getCookies();
                    response = new APIsResponse();
                    if (status.getCode() != 200) {
                        if (status.getCode() == 500) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(cxt, -1, "HTTP " + status.getCode()));
                            callback.onFail(response);
                        } else {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(cxt, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        }
                    } else {
                        if (file != null) {
                            ////open pdf
                            if (file.exists()) {
                                try {
                                    Uri uri = FileProvider.getUriForFile(cxt, cxt.getApplicationContext()
                                            .getPackageName(), file);

                                    Intent target = new Intent(Intent.ACTION_VIEW);
                                    target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    target.setDataAndType(uri, "application/pdf");
                                    target = Intent.createChooser(target, "Choose PDF File Viewer");
                                    fragment.getActivity().startActivity(target);
                                } catch (ActivityNotFoundException e) {
                                    // Instruct the user to install a PDF reader here, or something
                                    DialogHelper.createSimpleDialog(fragment.getActivity(), fragment.getString(R.string.myhkt_nopdfapp));
                                }
                            } else {
                                DialogHelper.createSimpleDialog(fragment.getActivity(), fragment.getString(R.string.myhkt_pdfnf));
                            }
                        } else {
                            DialogHelper.createSimpleDialog(fragment.getActivity(), fragment.getString(R.string.myhkt_pdfnf));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (progressManager != null) {
                    progressManager.dismissProgressDialog();
                }
            }
        };

        //set cookies
        for (Cookie cookie : mCookies) {
            ajaxCallback.cookie(cookie.getName(), cookie.getValue());
            //Here we are setting the cookie info.
        }
        aq.download(url, target, ajaxCallback);
    }


    public static final void doGetAppointment(final Fragment fragment, ApptCra rapptCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        if (debug) Log.i("API", "doGetAppointment");
        final String TAGNAME = "doGetAppointment";
        String url = domainURL + "/ma/appt";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(APPT);
        //		shopCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<ApptCra> apiRequest = new APIsRequest<ApptCra>(url, APPT);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ApptCra.class, apiRequest);
    }

    public static final void doCreateSR(final Fragment fragment, ApptCra rapptCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        final String TAGNAME = "doCreateSR";
        String url = domainURL + "/ma/srCrt";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(SR_CRT);

        APIsRequest<ApptCra> apiRequest = new APIsRequest<ApptCra>(url, SR_CRT);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ApptCra.class, apiRequest);
    }

    public static final void doUpdateSR(final Fragment fragment, ApptCra rapptCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        final String TAGNAME = "doUpdateSR";
        String url = domainURL + "/ma/srUpd";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(SR_UPD);

        APIsRequest<ApptCra> apiRequest = new APIsRequest<ApptCra>(url, SR_UPD);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ApptCra.class, apiRequest);
    }

    public static final void doCancelSR(final Fragment fragment, ApptCra rapptCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        final String TAGNAME = "doCancelSR";
        String url = domainURL + "/ma/srCls";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(SR_CLS);

        APIsRequest<ApptCra> apiRequest = new APIsRequest<ApptCra>(url, SR_CLS);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ApptCra.class, apiRequest);
    }

    //	public static final void doGetAppConfigJson(final Activity activity) {
    //		final AQuery aq = new AQuery(activity);
    //		// doHelo first
    //		final String TAGNAME = "doGetAppConfigJson";
    //		String url = THECLUB_HOST + "/loyalty-static/static/data/app_config.json";
    //
    ////		HeloCra heloCra = new HeloCra();
    ////		heloCra.setIApi(HELO);
    //
    ////		String jstr = gson.toJson(heloCra);
    //		if (debugGrq) ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), "");
    //		if (debugCra) Log.d(TAGNAME, String.format("%s %s",  TAGNAME, "request sent:") + "");
    //
    //		try {
    //			// 1st request
    ////			progressManager.showProgressDialog(activity);
    //			// 1st call doHelo
    ////			HttpEntity entity = new StringEntity(jstr, "UTF-8");
    //			Map<String, Object> params = new HashMap<String, Object>();
    ////			params.put(AQuery., entity);
    //
    //			aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {
    //
    //				OnAPIsListener 	callback;
    //				APIsResponse 	response;
    ////				HeloCra 		cra;
    //
    //				@Override
    //				public void callback(String url, JSONObject json, AjaxStatus status) {
    //					Boolean isFailCase = false;
    //					mCookies = status.getCookies();
    //					try {
    //						try {
    //							callback = (OnAPIsListener) activity;
    //						} catch (ClassCastException e) {
    //							throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
    //						}
    //						response = new APIsResponse();
    ////						response.setActionTy(HELO);
    ////						cra = new HeloCra();
    //						if (status.getCode() != 200) {
    //							response.setReply(HTTPSTATUS_NOT_200);
    //							response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
    //							callback.onFail(response);
    //							isFailCase = true;
    //						} else if (json == null) {
    //							// TODO: httppost response null Object from api
    //							response.setReply(JSON_RETURN_NULL);
    //							response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
    //							callback.onFail(response);
    //							isFailCase = true;
    //						} else {
    //							String resultStr = json.toString();
    //
    //					        JSONObject jsonObject;
    //					        String appurl_myhkt = "";
    //					        String appurl_call_theclub = "";
    //					        String myhkt_mymob_hint_update = "";
    //					        String myhkt_premier_flag = "";
    //					        String myhkt_consumer_flag = "";
    //							try {
    //								jsonObject = new JSONObject(resultStr);
    //								appurl_myhkt = jsonObject.getString("appurl_myhkt");
    //								appurl_call_theclub = jsonObject.getString("appurl_call_theclub");
    //								myhkt_mymob_hint_update = jsonObject.getString("myhkt_mymob_hint_update");
    //								myhkt_premier_flag = jsonObject.getString("myhkt_premier_flag");
    //								myhkt_consumer_flag = jsonObject.getString("myhkt_consumer_flag");
    //							} catch (JSONException e) {
    //								e.printStackTrace();
    //								appurl_myhkt = "N";
    //								appurl_call_theclub = "N";
    //								myhkt_mymob_hint_update = "";
    //								myhkt_premier_flag = "";
    //								myhkt_consumer_flag = "";
    //							}
    //
    //							AppConfig appconfigObj = new AppConfig();
    //							appconfigObj.setAppurl_myhkt(appurl_myhkt);
    //							appconfigObj.setAppurl_call_theclub(appurl_call_theclub);
    //							appconfigObj.setMyhkt_mymob_hint_update(myhkt_mymob_hint_update);
    //							appconfigObj.setMyhkt_premier_flag(myhkt_premier_flag);
    //							appconfigObj.setMyhkt_consumer_flag(myhkt_consumer_flag);
    //
    //							if (debugCra) ClnEnv.toLogFile(String.format("%s %s %s",  TAGNAME, "app_config.json", "response received"), resultStr);
    //							if (debugCra) Log.d(TAGNAME, String.format("%s %s %s",  TAGNAME, "app_config.json", "response received:") + resultStr);
    //
    //	            			response.setCra(appconfigObj);
    //	            			callback.onSuccess(response);
    //						}
    //						// dismiss
    //						//	            	progressManager = ProgressManager.getInstance(activity);
    ////						progressManager.dismissProgressDialog(isFailCase);
    //					} catch (Exception e) {
    ////						progressManager.dismissProgressDialog(true);
    //						if (debugCra) Log.e("", e.toString());
    //					}
    //				}
    //			});
    //		} catch (Exception e1) {
    //			e1.toString();
    //			// dismiss
    //			//        	progressManager = ProgressManager.getInstance(activity);
    ////			progressManager.dismissProgressDialog(true);
    //		}
    //	}

    private static <T extends BaseCraEx> String sampleData(Context ctx, APIsRequest<T> apiRequest, String result) {
        //dummy json for API testing
//		if (PLAN_IMS.equalsIgnoreCase(apiRequest.getActionTy())) {
//		return "{   \"iLoginId\": \"yuem702@outlook.com\",   \"iSubnRec\": {     \"rid\": 133224,     \"custRid\": 31452,     \"cusNum\": \"6300344\",     \"acctNum\": \"1200596511\",     \"srvId\": \"60109204\",     \"srvNum\": \"pbill002\",     \"bsn\": \" \",     \"fsa\": \"60109204\",     \"lob\": \"PCD\",     \"wipCust\": \"N\",     \"sipSubr\": \" \",     \"netLoginId\": \"pbill002\",     \"aloneDialup\": \"N\",     \"eyeGrp\": \" \",     \"ind2l2b\": \"N\",     \"refFsa2l2b\": \" \",     \"indPcd\": \"Y\",     \"indTv\": \"Y\",     \"indEye\": \"N\",     \"domainTy\": \"N\",     \"tos\": \"IMS\",     \"datCd\": \" \",     \"tariff\": \" \",     \"priMob\": \" \",     \"systy\": \"IMS\",     \"alias\": \" \",     \"assoc\": \"Y\",     \"createTs\": \"20170303112523\",     \"createPsn\": \"acspbat\",     \"lastupdTs\": \"20170303112523\",     \"lastupdPsn\": \"acspbat\",     \"rev\": 0,     \"instAdr\": \"FT A3 1/F VP15 BLDG, HUNG HOM KOWLOON\",     \"storeTy\": \"\",     \"ivr_pwd\": \"\",     \"auth_ph\": \"\",     \"inMipMig\": false,     \"acct_ty\": \"\"   },   \"oMthFee\": {     \"srvCd\": \"\",     \"StDt\": \"\",     \"EnDt\": \"\",     \"fee\": \"\",     \"desnEn\": \"\",     \"desnZh\": \"\",     \"longDesn1En\": \"\",     \"longDesn2En\": \"\",     \"longDesn1Zh\": \"\",     \"longDesn2Zh\": \"\",     \"tncEn\": \"\",     \"tncZh\": \"\",     \"usg\": \"\",     \"usgStDt\": \"\",     \"usgEnDt\": \"\",     \"usgUnt\": \"\",     \"srvCmmtPrd\": \"\",     \"srvPenalty\": \"\",     \"srvCmmtRmn\": \"\",     \"srvEnt\": \"\",     \"rebtEn\": \"\",     \"rebtZh\": \"\",     \"rebtAmt\": \"\",     \"monthlyTermRate\": \"\",     \"m2mRate\": \"\",     \"plnTy\": \"\",     \"plnSubTy\": \"\",     \"cntrStDt\": \"\",     \"cntrEnDt\": \"\",     \"cntrDur\": \"\",     \"cmmtInd\": \"\",     \"seasInd\": \"\",     \"suppInd\": \"\",     \"summInd\": \"\"   },   \"oVasAry\": [],   \"oIncAry\": [],   \"oCommAry\": [],   \"oPonCvg\": false,   \"iApi\": \"PLAN_IMS\",   \"iGwtGud\": \"915590BB02D348B3\",   \"iMyFrm\": \"\",   \"oHgg\": \"\",   \"oNxUrl\": \"\",   \"oDuct\": null,   \"reply\": {     \"code\": \"RC_UXPERR\",     \"cargo\": null   },   \"serverTS\": \"20170327103918\" }"; 		
//		}
        //check bill
        //		if (CHK_BILL.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return "{\"iAcct\":{\"acctNum\":\"\",\"cusNum\":\"\",\"sysTy\":\"\",\"lob\":\"\",\"live\":false,\"custRid\":0},\"iBillDate\":\"20161021\",\"iLoginId\":\"derekkc@gmail.com\",\"iSubnRec\":{\"acctNum\":\"75100138564625\",\"acct_ty\":\"\",\"alias\":\" \",\"aloneDialup\":\" \",\"assoc\":\"Y\",\"auth_ph\":\"\",\"bsn\":\" \",\"createPsn\":\"pcspbat\",\"createTs\":\"20141208091100\",\"cusNum\":\"11405824\",\"wipCust\":\"N\",\"datCd\":\" \",\"domainTy\":\" \",\"eyeGrp\":\" \",\"fsa\":\" \",\"tos\":\"3G\",\"ind2l2b\":\" \",\"indEye\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"instAdr\":\"\",\"ivr_pwd\":\"\",\"lastupdPsn\":\"!NOPERSON\",\"lastupdTs\":\"20141211100059\",\"lob\":\"MOB\",\"netLoginId\":\" \",\"priMob\":\" \",\"refFsa2l2b\":\" \",\"tariff\":\" \",\"systy\":\"MOB\",\"sipSubr\":\"N\",\"srvId\":\"115839843\",\"srvNum\":\"54109128\",\"storeTy\":\"\",\"rid\":11899874,\"rev\":0,\"inMipMig\":false,\"custRid\":6150162},\"oBillList\":{\"iAcct\":{\"acctNum\":\"\",\"cusNum\":\"\",\"sysTy\":\"\",\"lob\":\"\",\"live\":false,\"custRid\":0},\"oBillAry\":[]},\"oBillPreview\":{\"noBillMsgFlg\":false,\"noBillFlg\":false},\"oG3OutstandingBalanceDTO\":{\"overdueAmt\":0.0,\"currOSBal\":0.0,\"totalOSBal\":0.0},\"oG3ServiceCallBarStatusWS\":{\"g3ServiceCallBarStatusDTO\":{\"g3AccountInformationDTO\":{},\"g3PaymentRejectStatusDTO\":{},\"g3SubExtInfoDTO\":{}}},\"oSummUrl\":\"\",\"iApi\":\"CHK_BILL\",\"iGwtGud\":\"CACA549C340D4E65\",\"iMyFrm\":\"\",\"oHgg\":\"\",\"oNxUrl\":\"\",\"reply\":{\"code\":\"RC_BINQ_BINRDY\"},\"serverTS\":\"\"}";
        //		}

        //RegAccInfo livechat01@pccw.com / a12345  HKID B000002(2)
//								if (LGI.equalsIgnoreCase(apiRequest.getActionTy())) {
//							     return "{     \"serverTS\": \"20160825193107\",     \"iGwtGud\": \"\",     \"iOrigPwd\": \"\",     \"oDuct\": null,     \"oSrvReqAry\": [             ],     \"iRecallLgiId\": false,     \"iNaCra\": {         \"serverTS\": \"\",         \"iGwtGud\": \"\",         \"oHgg\": \"\",         \"oDuct\": null,         \"reply\": {             \"cargo\": null,             \"code\": \"RC_SUCC\"         },         \"iApi\": \"\",         \"oNxUrl\": \"\",         \"iMyFrm\": \"\"     },     \"oTaSubnRec\": {         \"rev\": 0,         \"createTs\": \"\",         \"netLoginId\": \"\",         \"custRid\": 0,         \"systy\": \"\",         \"instAdr\": \"\",         \"eyeGrp\": \"\",         \"sipSubr\": \"\",         \"wipCust\": \"\",         \"storeTy\": \"\",         \"lastupdTs\": \"\",         \"datCd\": \"\",         \"indTv\": \"\",         \"lastupdPsn\": \"\",         \"lob\": \"\",         \"tos\": \"\",         \"indPcd\": \"\",         \"auth_ph\": \"\",         \"tariff\": \"\",         \"indEye\": \"\",         \"createPsn\": \"\",         \"cusNum\": \"\",         \"assoc\": \"\",         \"fsa\": \"\",         \"acct_ty\": \"\",         \"alias\": \"\",         \"acctNum\": \"\",         \"ind2l2b\": \"\",         \"domainTy\": \"\",         \"inMipMig\": false,         \"srvNum\": \"\",         \"bsn\": \"\",         \"rid\": 0,         \"refFsa2l2b\": \"\",         \"srvId\": \"\",         \"priMob\": \"\",         \"aloneDialup\": \"\",         \"ivr_pwd\": \"\"     },     \"iSveeRec\": {         \"loginId\": \"\",         \"rev\": 0,         \"staffId\": \"\",         \"secAns\": \"\",         \"ctMail\": \"\",         \"status\": \"\",         \"nickname\": \"\",         \"createTs\": \"\",         \"teamCd\": \"\",         \"state\": \"\",         \"custRid\": 0,         \"lastupdTs\": \"\",         \"lang\": \"\",         \"lastupdPsn\": \"\",         \"ctMob\": \"\",         \"salesChnl\": \"\",         \"pwdEff\": \"\",         \"pwd\": \"\",         \"promoOpt\": \"\",         \"regnActn\": \"\",         \"rid\": 0,         \"stateUpTs\": \"\",         \"secQus\": 0,         \"mobAlrt\": \"\",         \"createPsn\": \"\"     },     \"oQualSvee\": {         \"zmSubnRecAry\": [                     ],         \"subnRecAry\": [                     ],         \"careRec\": {             \"hitCnt\": 0,             \"bipt\": \"\",             \"rev\": 0,             \"biptUpTs\": \"\",             \"custNm\": \"\",             \"careUpTs\": \"\",             \"createTs\": \"\",             \"care\": \"\",             \"custRid\": 0,             \"ageGrp\": \"\",             \"lastupdTs\": \"\",             \"createPsn\": \"\",             \"lastupdPsn\": \"\"         },         \"bomCustAry\": [                     ],         \"custRec\": {             \"docNum\": \"\",             \"phylum\": \"\",             \"rev\": 0,             \"status\": \"\",             \"rid\": 0,             \"createTs\": \"\",             \"premier\": \"\",             \"lastupdTs\": \"\",             \"docTy\": \"\",             \"createPsn\": \"\",             \"lastupdPsn\": \"\"         },         \"bcifRec\": {             \"priCtJobtitle\": \"\",             \"rev\": 0,             \"custNm\": \"\",             \"secCtMail\": \"\",             \"createTs\": \"\",             \"custRid\": 0,             \"secCtTel\": \"\",             \"lastupdTs\": \"\",             \"priCtMob\": \"\",             \"priCtMail\": \"\",             \"xborder\": \"\",             \"lastupdPsn\": \"\",             \"secCtMob\": \"\",             \"nuStaff\": \"\",             \"secCtName\": \"\",             \"priCtTel\": \"\",             \"nuPresence\": \"\",             \"priCtName\": \"\",             \"priCtTitle\": \"\",             \"secCtTitle\": \"\",             \"industry\": \"\",             \"createPsn\": \"\",             \"secCtJobtitle\": \"\"         },         \"sveeRec\": {             \"loginId\": \"livechat01@pccw.com\",             \"rev\": 0,             \"staffId\": \"000000\",             \"secAns\": \"\",             \"ctMail\": \"livechat01@pccw.com\",             \"status\": \"A\",             \"nickname\": \" \",             \"createTs\": \"20160817161613\",             \"teamCd\": \"000000\",             \"state\": \"L2\",             \"custRid\": 4207,             \"lastupdTs\": \"20160817161613\",             \"lang\": \"zh\",             \"lastupdPsn\": \"T1475912\",             \"ctMob\": \"62897639\",             \"salesChnl\": \"PCD/TV CS\",             \"pwdEff\": \"Y\",             \"pwd\": \"\",             \"promoOpt\": \"I\",             \"regnActn\": \"REGUSER_LARVAT\",             \"rid\": 821,             \"stateUpTs\": \"20160817161613\",             \"secQus\": 0,             \"mobAlrt\": \"Y\",             \"createPsn\": \"T1475912\"         }     },     \"oGnrlApptAry\": [             ],     \"iSpssRec\": {         \"rev\": 0,         \"devTy\": \"\",         \"dervRid\": 0,         \"rid\": 0,         \"bni\": \"\",         \"sveeRid\": 0,         \"lastupdTs\": \"\",         \"tcId\": \"\",         \"gni\": \"\",         \"devId\": \"\",         \"tcSess\": \"\",         \"lang\": \"\",         \"lastupdPsn\": \"\"     },     \"oNxUrl\": \"\",     \"iMyFrm\": \"\",     \"iCustRec\": {         \"docNum\": \"\",         \"phylum\": \"\",         \"rev\": 0,         \"status\": \"\",         \"rid\": 0,         \"createTs\": \"\",         \"premier\": \"\",         \"lastupdTs\": \"\",         \"docTy\": \"\",         \"createPsn\": \"\",         \"lastupdPsn\": \"\"     },     \"oHgg\": \"\",     \"oAcctAry\": [             ],     \"oTaCra\": {         \"serverTS\": \"\",         \"iGwtGud\": \"\",         \"oHgg\": \"\",         \"oDuct\": null,         \"reply\": {             \"cargo\": null,             \"code\": \"RC_SUCC\"         },         \"iApi\": \"\",         \"oNxUrl\": \"\",         \"iMyFrm\": \"\"     },     \"oChatTok\": \"\",     \"reply\": {         \"cargo\": null,         \"code\": \"RC_FILL_SVEE4CLUB\"     },     \"iApi\": \"\",     \"oBillListAry\": [             ],     \"oSoGud\": \"0bf60dfd22c062b4bddad1f41094b7896ee7b7c5bc5ebb723b5664d6c761049f\",     \"iRecallPwd\": false,     \"iNaSubnRec\": {         \"rev\": 0,         \"createTs\": \"\",         \"netLoginId\": \"\",         \"custRid\": 0,         \"systy\": \"\",         \"instAdr\": \"\",         \"eyeGrp\": \"\",         \"sipSubr\": \"\",         \"wipCust\": \"\",         \"storeTy\": \"\",         \"lastupdTs\": \"\",         \"datCd\": \"\",         \"indTv\": \"\",         \"lastupdPsn\": \"\",         \"lob\": \"\",         \"tos\": \"\",         \"indPcd\": \"\",         \"auth_ph\": \"\",         \"tariff\": \"\",         \"indEye\": \"\",         \"createPsn\": \"\",         \"cusNum\": \"\",         \"assoc\": \"\",         \"fsa\": \"\",         \"acct_ty\": \"\",         \"alias\": \"\",         \"acctNum\": \"\",         \"ind2l2b\": \"\",         \"domainTy\": \"\",         \"inMipMig\": false,         \"srvNum\": \"\",         \"bsn\": \"\",         \"rid\": 0,         \"refFsa2l2b\": \"\",         \"srvId\": \"\",         \"priMob\": \"\",         \"aloneDialup\": \"\",         \"ivr_pwd\": \"\"     } }";
//								}
        //lts plan
        //				if (PLAN_LTS.equalsIgnoreCase(apiRequest.getActionTy())) {
        //				return 	"{     \"serverTS\": \"20160712122052\",     \"iSubnRec\": {         \"rev\": 0,         \"createTs\": \"20151127163423\",         \"netLoginId\": \" \",         \"custRid\": 2033,         \"systy\": \"DRG\",         \"instAdr\": \"G/F HK11 BLDG, CENTRAL DISTRICT HONG KONG\",         \"eyeGrp\": \" \",         \"sipSubr\": \" \",         \"wipCust\": \"X\",         \"storeTy\": \"\",         \"lastupdTs\": \"20151127163423\",         \"datCd\": \"DEL\",         \"indTv\": \" \",         \"lastupdPsn\": \"dldsbat\",         \"lob\": \"LTS\",         \"tos\": \"TEL\",         \"indPcd\": \" \",         \"auth_ph\": null,         \"tariff\": \"R\",         \"indEye\": \"N\",         \"createPsn\": \"dldsbat\",         \"cusNum\": \"98000405\",         \"assoc\": \"Y\",         \"fsa\": \" \",         \"acct_ty\": null,         \"alias\": \" \",         \"acctNum\": \"98000405000339\",         \"ind2l2b\": \" \",         \"domainTy\": \" \",         \"srvNum\": \"24105533\",         \"bsn\": \" \",         \"rid\": 11550,         \"refFsa2l2b\": \" \",         \"srvId\": \"100034243\",         \"priMob\": \" \",         \"aloneDialup\": \" \",         \"ivr_pwd\": null     },     \"iGwtGud\": \"E461DA41C000439D\",     \"oCallPlanAry\": [             ],     \"oHgg\": \"\",     \"oDuct\": null,     \"iLoginId\": \"ocs01@pccw.com\",     \"oIAdr\": \"\",     \"reply\": {         \"cargo\": null,         \"code\": \"RC_LTSA_CPLN_11_17\"     },     \"iApi\": \"PLAN_LTS\",     \"oTermPlanAry\": [             ],     \"oNxUrl\": \"\",     \"iMyFrm\": \"\" }";
        //global calling card with call plan ocs01@pccw.com /a12345
        //	return	"{\"serverTS\":\"20161121122609\",\"iSubnRec\":{\"rev\":0,\"createTs\":\"20151127163423\",\"netLoginId\":\" \",\"custRid\":2033,\"systy\":\"DRG\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"X\",\"storeTy\":\"\",\"lastupdTs\":\"20151127163423\",\"datCd\":\"NCR\",\"indTv\":\" \",\"lastupdPsn\":\"dldsbat\",\"lob\":\"LTS\",\"tos\":\"ONC\",\"indPcd\":\" \",\"auth_ph\":\"\",\"tariff\":\"B\",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"98000405\",\"assoc\":\"Y\",\"fsa\":\" \",\"acct_ty\":\"\",\"alias\":\" \",\"acctNum\":\"98000405000141\",\"ind2l2b\":\" \",\"domainTy\":\" \",\"inMipMig\":false,\"srvNum\":\"81000168\",\"bsn\":\" \",\"rid\":11548,\"refFsa2l2b\":\" \",\"srvId\":\"100072045\",\"priMob\":\" \",\"aloneDialup\":\" \",\"ivr_pwd\":\"\"},\"iGwtGud\":\"0E26DBEE7DB64CC1\",\"oCallPlanAry\":[{\"srvCd\":\"UM+2\",\"StDt\":\"\",\"EnDt\":\"\",\"fee\":\"+000000160.00\",\"desnEn\":\"TERMPLANUM+2\",\"desnZh\":\"TERMPLANUM+2(ZH)\",\"longDesn1En\":\"TermPlanminutesapplytocallingthebelowdestinationsfromHongKongviaregisteredmobilenumber: \",\"longDesn2En\":\"FixedlineandmobilenumberofChina/USA/CanadaandfixedlinenumberofAustraliaandUK\",\"longDesn1Zh\":\"TermPlanminutesapplytocallingthebelowdestinationsfromHongKongviaregisteredmobilenumber: (zh)\",\"longDesn2Zh\":\"FixedlineandmobilenumberofChina/USA/CanadaandfixedlinenumberofAustraliaandUK(zh)\",\"tncEn\":\"Planminutesareofferedonaper-minutebasis.Anyunusedminutescannotbecarriedforwardtosubsequentmonth.PlanminutesdonotapplytocallsmadefromPCCWmobilerechargeableSIMcard,            PCCWHelloprepaidSIMcardandcallsmadetotelephonenumberswithprefixes\u002756\u0027,            \u00277\u0027,            \u00278\u0027and\u00279\u0027intheUK.\",\"tncZh\":\"Planminutesareofferedonaper-minutebasis.Anyunusedminutescannotbecarriedforwardtosubsequentmonth.PlanminutesdonotapplytocallsmadefromPCCWmobilerechargeableSIMcard,            PCCWHelloprepaidSIMcardandcallsmadetotelephonenumberswithprefixes\u002756\u0027,            \u00277\u0027,            \u00278\u0027and\u00279\u0027intheUK.(zh)\",\"usg\":\"+000000000.00\",\"usgStDt\":\"20130401\",\"usgEnDt\":\"00000000\",\"usgUnt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"00160\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"CM\",\"plnSubTy\":\"1\",\"cntrStDt\":\"20120301\",\"cntrEnDt\":\"20120401\",\"cntrDur\":\"01\",\"cmmtInd\":\"\",\"seasInd\":\"\",\"suppInd\":\"\",\"summInd\":\"\"},{\"srvCd\":\"UM8(\",\"StDt\":\"20120501\",\"EnDt\":\"00000000\",\"fee\":\"+000000140.00\",\"desnEn\":\"TERMPLANUM8(\",\"desnZh\":\"TERMPLANUM8((ZH)\",\"longDesn1En\":\"TermPlanminutesapplytocallingthebelowdestinationsfromHongKongviaregisteredmobilenumber: \",\"longDesn2En\":\"FixedlineandmobilenumberofChina/USA/CanadaandfixedlinenumberofAustraliaandUK\",\"longDesn1Zh\":\"TermPlanminutesapplytocallingthebelowdestinationsfromHongKongviaregisteredmobilenumber:  (zh)\",\"longDesn2Zh\":\"FixedlineandmobilenumberofChina/USA/CanadaandfixedlinenumberofAustraliaandUK(zh)\",\"tncEn\":\"TermPlanminutesareofferedonaper-minutebasis.Anyunusedminutescannotbecarriedforwardtosubsequentmonth.PlanminutesdonotapplytocallsmadefromPCCWmobilerechargeableSIMcard,            PCCWHelloprepaidSIMcardandcallsmadetotelephonenumberswithprefixes\u002756\u0027,            \u00277\u0027,            \u00278\u0027and\u00279\u0027intheUK.\",\"tncZh\":\"TermPlanminutesareofferedonaper-minutebasis.Anyunusedminutescannotbecarriedforwardtosubsequentmonth.PlanminutesdonotapplytocallsmadefromPCCWmobilerechargeableSIMcard,            PCCWHelloprepaidSIMcardandcallsmadetotelephonenumberswithprefixes\u002756\u0027,            \u00277\u0027,            \u00278\u0027and\u00279\u0027intheUK.(zh)\",\"usg\":\"+000000000.00\",\"usgStDt\":\"20130401\",\"usgEnDt\":\"20130430\",\"usgUnt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"00140\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"CM\",\"plnSubTy\":\"1\",\"cntrStDt\":\"20120501\",\"cntrEnDt\":\"20120601\",\"cntrDur\":\"01\",\"cmmtInd\":\"\",\"seasInd\":\"\",\"suppInd\":\"\",\"summInd\":\"\"}],\"oHgg\":\"\",\"oDuct\":null,\"iLoginId\":\"ocs01@pccw.com\",\"oIAdr\":\"\",\"reply\":{\"cargo\":null,\"code\":\"RC_SUCC\"},\"iApi\":\"PLAN_LTS\",\"oTermPlanAry\":[],\"oNxUrl\":\"\",\"iMyFrm\":\"\"}";

        //derek
        //		return "{\"serverTS\":\"20161121155940\",\"iSubnRec\":{\"rev\":0,\"createTs\":\"20141212025800\",\"netLoginId\":\" \",\"custRid\":6150162,\"systy\":\"DRG\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"X\",\"storeTy\":\"\",\"lastupdTs\":\"20150316031043\",\"datCd\":\"MOB\",\"indTv\":\" \",\"lastupdPsn\":\"pcspbat\",\"lob\":\"LTS\",\"tos\":\"MOB\",\"indPcd\":\" \",\"auth_ph\":\"\",\"tariff\":\"R\",\"indEye\":\"N\",\"createPsn\":\"pcspbat\",\"cusNum\":\"24094258\",\"assoc\":\"Y\",\"fsa\":\" \",\"acct_ty\":\"\",\"alias\":\" \",\"acctNum\":\"24094258000125\",\"ind2l2b\":\" \",\"domainTy\":\" \",\"inMipMig\":false,\"srvNum\":\"54109128\",\"bsn\":\" \",\"rid\":11912275,\"refFsa2l2b\":\" \",\"srvId\":\"115844215\",\"priMob\":\" \",\"aloneDialup\":\" \",\"ivr_pwd\":\"\"},\"iGwtGud\":\"52008086A8314BA8\",\"oCallPlanAry\":[{\"srvCd\":\"UM+2\",\"StDt\":\"\",\"EnDt\":\"\",\"fee\":\"+000000160.00\",\"desnEn\":\"TERMPLANUM+2\",\"desnZh\":\"TERMPLANUM+2(ZH)\",\"longDesn1En\":\"TermPlanminutesapplytocallingthebelowdestinationsfromHongKongviaregisteredmobilenumber: \",\"longDesn2En\":\"FixedlineandmobilenumberofChina/USA/CanadaandfixedlinenumberofAustraliaandUK\",\"longDesn1Zh\":\"TermPlanminutesapplytocallingthebelowdestinationsfromHongKongviaregisteredmobilenumber: (zh)\",\"longDesn2Zh\":\"FixedlineandmobilenumberofChina/USA/CanadaandfixedlinenumberofAustraliaandUK(zh)\",\"tncEn\":\"Planminutesareofferedonaper-minutebasis.Anyunusedminutescannotbecarriedforwardtosubsequentmonth.PlanminutesdonotapplytocallsmadefromPCCWmobilerechargeableSIMcard,            PCCWHelloprepaidSIMcardandcallsmadetotelephonenumberswithprefixes\u002756\u0027,            \u00277\u0027,            \u00278\u0027and\u00279\u0027intheUK.\",\"tncZh\":\"Planminutesareofferedonaper-minutebasis.Anyunusedminutescannotbecarriedforwardtosubsequentmonth.PlanminutesdonotapplytocallsmadefromPCCWmobilerechargeableSIMcard,            PCCWHelloprepaidSIMcardandcallsmadetotelephonenumberswithprefixes\u002756\u0027,            \u00277\u0027,            \u00278\u0027and\u00279\u0027intheUK.(zh)\",\"usg\":\"+000000000.00\",\"usgStDt\":\"20130401\",\"usgEnDt\":\"00000000\",\"usgUnt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"00160\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"CM\",\"plnSubTy\":\"1\",\"cntrStDt\":\"20120301\",\"cntrEnDt\":\"20120401\",\"cntrDur\":\"01\",\"cmmtInd\":\"\",\"seasInd\":\"\",\"suppInd\":\"\",\"summInd\":\"\"},{\"srvCd\":\"UM8(\",\"StDt\":\"20120501\",\"EnDt\":\"00000000\",\"fee\":\"+000000140.00\",\"desnEn\":\"TERMPLANUM8(\",\"desnZh\":\"TERMPLANUM8((ZH)\",\"longDesn1En\":\"TermPlanminutesapplytocallingthebelowdestinationsfromHongKongviaregisteredmobilenumber: \",\"longDesn2En\":\"FixedlineandmobilenumberofChina/USA/CanadaandfixedlinenumberofAustraliaandUK\",\"longDesn1Zh\":\"TermPlanminutesapplytocallingthebelowdestinationsfromHongKongviaregisteredmobilenumber:  (zh)\",\"longDesn2Zh\":\"FixedlineandmobilenumberofChina/USA/CanadaandfixedlinenumberofAustraliaandUK(zh)\",\"tncEn\":\"TermPlanminutesareofferedonaper-minutebasis.Anyunusedminutescannotbecarriedforwardtosubsequentmonth.PlanminutesdonotapplytocallsmadefromPCCWmobilerechargeableSIMcard,            PCCWHelloprepaidSIMcardandcallsmadetotelephonenumberswithprefixes\u002756\u0027,            \u00277\u0027,            \u00278\u0027and\u00279\u0027intheUK.\",\"tncZh\":\"TermPlanminutesareofferedonaper-minutebasis.Anyunusedminutescannotbecarriedforwardtosubsequentmonth.PlanminutesdonotapplytocallsmadefromPCCWmobilerechargeableSIMcard,            PCCWHelloprepaidSIMcardandcallsmadetotelephonenumberswithprefixes\u002756\u0027,            \u00277\u0027,            \u00278\u0027and\u00279\u0027intheUK.(zh)\",\"usg\":\"+000000000.00\",\"usgStDt\":\"20130401\",\"usgEnDt\":\"20130430\",\"usgUnt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"00140\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"CM\",\"plnSubTy\":\"1\",\"cntrStDt\":\"20120501\",\"cntrEnDt\":\"20120601\",\"cntrDur\":\"01\",\"cmmtInd\":\"\",\"seasInd\":\"\",\"suppInd\":\"\",\"summInd\":\"\"}],\"oHgg\":\"\",\"oDuct\":null,\"iLoginId\":\"derekkc@gmail.com\",\"oIAdr\":\"\",\"reply\":{\"cargo\":null,\"code\":\"RC_SUCC\"},\"iApi\":\"PLAN_LTS\",\"oTermPlanAry\":[],\"oNxUrl\":\"\",\"iMyFrm\":\"\"}";

        //					return "{     \"serverTS\": \"20160712113453\",     \"iSubnRec\": {         \"rev\": 0,         \"createTs\": \"20151127163349\",         \"netLoginId\": \" \",         \"custRid\": 1325,         \"systy\": \"DRG\",         \"instAdr\": \"FT 5 G/F, 88 YUNG SHU WAN MAIN ST, LAMMA ISLAND HONG KONG\",         \"eyeGrp\": \" \",         \"sipSubr\": \" \",         \"wipCust\": \"N\",         \"storeTy\": \"\",         \"lastupdTs\": \"20151127163349\",         \"datCd\": \"DEL\",         \"indTv\": \" \",         \"lastupdPsn\": \"dldsbat\",         \"lob\": \"LTS\",         \"tos\": \"TEL\",         \"indPcd\": \" \",         \"auth_ph\": null,         \"tariff\": \"R\",         \"indEye\": \"N\",         \"createPsn\": \"dldsbat\",         \"cusNum\": \"24000019\",         \"assoc\": \"Y\",         \"fsa\": \" \",         \"acct_ty\": null,         \"alias\": \" \",         \"acctNum\": \"24000019000570\",         \"ind2l2b\": \" \",         \"domainTy\": \" \",         \"srvNum\": \"20124030\",         \"bsn\": \" \",         \"rid\": 3325,         \"refFsa2l2b\": \" \",         \"srvId\": \"100132351\",         \"priMob\": \" \",         \"aloneDialup\": \" \",         \"ivr_pwd\": null     },     \"iGwtGud\": \"344B618334A9499F\",     \"oCallPlanAry\": [],     \"oHgg\": \"\",     \"oDuct\": null,     \"iLoginId\": \"asdfg12345@pccw.com\",     \"oIAdr\": \"\",     \"reply\": {         \"cargo\": null,         \"code\": \"RC_LTSA_CPLN_11_17\"     },     \"iApi\": \"PLAN_LTS\",     \"oTermPlanAry\": [         {             \"srvEnt\": \"\",             \"usg\": \"\",             \"tncEn\": \"\",             \"longDesn2Zh\": \"\",             \"monthlyTermRate\": \"128\",             \"srvCmmtRmn\": \"\",             \"longDesn2En\": \"\",             \"tncZh\": \"\",             \"longDesn1En\": \"Residential Telephone Line Service - Premium Service Plan\",             \"srvPenalty\": \"\",             \"rebtZh\": \"\",             \"desnEn\": \"\",             \"srvCmmtPrd\": \"24\",             \"cntrDur\": \"\",             \"suppInd\": \"\",             \"plnSubTy\": \"\",             \"plnTy\": \"\",             \"usgEnDt\": \"\",             \"usgUnt\": \"\",             \"StDt\": \"20160114\",             \"longDesn1Zh\": \"家居電話線服務 - 禮品服務計劃\",             \"rebtAmt\": \"\",             \"m2mRate\": \"\",             \"cmmtInd\": \"\",             \"rebtEn\": \"\",             \"srvCd\": \"1ST9\",             \"fee\": \"\",             \"usgStDt\": \"\",             \"summInd\": \"\",             \"desnZh\": \"\",             \"EnDt\": \"20180113\",             \"seasInd\": \"\",             \"cntrEnDt\": \"\",             \"cntrStDt\": \"\"         }     ],     \"oNxUrl\": \"\",     \"iMyFrm\": \"\" }";
        //				}

        //csim zombie
        //		if (LGI.equalsIgnoreCase(apiRequest.getActionTy())) {
        //	return		"{     \"iCustRec\": {         \"createPsn\": \"\",         \"createTs\": \"\",         \"docNum\": \"\",         \"docTy\": \"\",         \"lastupdPsn\": \"\",         \"lastupdTs\": \"\",         \"phylum\": \"\",         \"premier\": \"\",         \"status\": \"\",         \"rid\": 0,         \"rev\": 0     },     \"iNaCra\": {         \"iApi\": \"\",         \"iGwtGud\": \"\",         \"iMyFrm\": \"\",         \"oHgg\": \"\",         \"oNxUrl\": \"\",         \"reply\": {             \"code\": \"RC_SUCC\"         },         \"serverTS\": \"\"     },     \"iNaSubnRec\": {         \"acctNum\": \"\",         \"alias\": \"\",         \"aloneDialup\": \"\",         \"assoc\": \"\",         \"bsn\": \"\",         \"createPsn\": \"\",         \"createTs\": \"\",         \"cusNum\": \"\",         \"wipCust\": \"\",         \"datCd\": \"\",         \"domainTy\": \"\",         \"eyeGrp\": \"\",         \"fsa\": \"\",         \"ind2l2b\": \"\",         \"indEye\": \"\",         \"indPcd\": \"\",         \"indTv\": \"\",         \"instAdr\": \"\",         \"lastupdPsn\": \"\",         \"lastupdTs\": \"\",         \"lob\": \"\",         \"netLoginId\": \"\",         \"priMob\": \"\",         \"refFsa2l2b\": \"\",         \"tos\": \"\",         \"tariff\": \"\",         \"sipSubr\": \"\",         \"srvId\": \"\",         \"srvNum\": \"\",         \"storeTy\": \"\",         \"systy\": \"\",         \"rid\": 0,         \"rev\": 0,         \"custRid\": 0     },     \"iOrigPwd\": \"\",     \"oTaSubnRec\": {         \"acctNum\": \"\",         \"alias\": \"\",         \"aloneDialup\": \"\",         \"assoc\": \"\",         \"bsn\": \"\",         \"createPsn\": \"\",         \"createTs\": \"\",         \"cusNum\": \"\",         \"wipCust\": \"\",         \"datCd\": \"\",         \"domainTy\": \"\",         \"eyeGrp\": \"\",         \"fsa\": \"\",         \"ind2l2b\": \"\",         \"indEye\": \"\",         \"indPcd\": \"\",         \"indTv\": \"\",         \"instAdr\": \"\",         \"lastupdPsn\": \"\",         \"lastupdTs\": \"\",         \"lob\": \"\",         \"netLoginId\": \"\",         \"priMob\": \"\",         \"refFsa2l2b\": \"\",         \"tos\": \"\",         \"tariff\": \"\",         \"sipSubr\": \"\",         \"srvId\": \"\",         \"srvNum\": \"\",         \"storeTy\": \"\",         \"systy\": \"\",         \"rid\": 0,         \"rev\": 0,         \"custRid\": 0     },     \"oTaCra\": {         \"iApi\": \"\",         \"iGwtGud\": \"\",         \"iMyFrm\": \"\",         \"oHgg\": \"\",         \"oNxUrl\": \"\",         \"reply\": {             \"code\": \"RC_SUCC\"         },         \"serverTS\": \"\"     },     \"iSpssRec\": {         \"bni\": \"\",         \"tcSess\": \"\",         \"devId\": \"\",         \"devTy\": \"\",         \"gni\": \"\",         \"lang\": \"\",         \"lastupdPsn\": \"\",         \"lastupdTs\": \"\",         \"tcId\": \"\",         \"rid\": 0,         \"sveeRid\": 0,         \"rev\": 0,         \"dervRid\": 0     },     \"iSveeRec\": {         \"createPsn\": \"\",         \"createTs\": \"\",         \"ctMail\": \"\",         \"ctMob\": \"\",         \"teamCd\": \"\",         \"lang\": \"\",         \"lastupdPsn\": \"\",         \"lastupdTs\": \"\",         \"loginId\": \"\",         \"mobAlrt\": \"\",         \"nickname\": \"\",         \"promoOpt\": \"\",         \"pwd\": \"\",         \"pwdEff\": \"\",         \"regnActn\": \"\",         \"status\": \"\",         \"stateUpTs\": \"\",         \"salesChnl\": \"\",         \"secAns\": \"\",         \"state\": \"\",         \"staffId\": \"\",         \"secQus\": 0,         \"rid\": 0,         \"rev\": 0,         \"custRid\": 0     },     \"oAcctAry\": [         {             \"acctNum\": \"05054861\",             \"cusNum\": \" \",             \"sysTy\": \"CSL\",             \"lob\": \"O2F\",             \"live\": false,             \"custRid\": 7503         },         {             \"acctNum\": \"1200094879\",             \"cusNum\": \"9500602\",             \"sysTy\": \"IMS\",             \"lob\": \"PCD\",             \"live\": true,             \"custRid\": 7503         },         {             \"acctNum\": \"1200101993\",             \"cusNum\": \"9500602\",             \"sysTy\": \"IMS\",             \"lob\": \"PCD\",             \"live\": true,             \"custRid\": 7503         },         {             \"acctNum\": \"1200102006\",             \"cusNum\": \"9500602\",             \"sysTy\": \"IMS\",             \"lob\": \"PCD\",             \"live\": true,             \"custRid\": 7503         },         {             \"acctNum\": \"1200121942\",             \"cusNum\": \"9500602\",             \"sysTy\": \"IMS\",             \"lob\": \"PCD\",             \"live\": true,             \"custRid\": 7503         },         {             \"acctNum\": \"1200124349\",             \"cusNum\": \"9500602\",             \"sysTy\": \"IMS\",             \"lob\": \"PCD\",             \"live\": true,             \"custRid\": 7503         },         {             \"acctNum\": \"1200124352\",             \"cusNum\": \"9500602\",             \"sysTy\": \"IMS\",             \"lob\": \"TV\",             \"live\": true,             \"custRid\": 7503         },         {             \"acctNum\": \"75100133007187\",             \"cusNum\": \"10004631\",             \"sysTy\": \"MOB\",             \"lob\": \"MOB\",             \"live\": false,             \"custRid\": 7503         }     ],     \"oBillListAry\": [         {             \"iAcct\": {                 \"acctNum\": \"05054861\",                 \"cusNum\": \" \",                 \"sysTy\": \"CSL\",                 \"lob\": \"O2F\",                 \"live\": false,                 \"custRid\": 7503             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"acctNum\": \"1200094879\",                 \"cusNum\": \"9500602\",                 \"sysTy\": \"IMS\",                 \"lob\": \"PCD\",                 \"live\": true,                 \"custRid\": 7503             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"acctNum\": \"1200101993\",                 \"cusNum\": \"9500602\",                 \"sysTy\": \"IMS\",                 \"lob\": \"PCD\",                 \"live\": true,                 \"custRid\": 7503             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"acctNum\": \"1200102006\",                 \"cusNum\": \"9500602\",                 \"sysTy\": \"IMS\",                 \"lob\": \"PCD\",                 \"live\": true,                 \"custRid\": 7503             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"acctNum\": \"1200121942\",                 \"cusNum\": \"9500602\",                 \"sysTy\": \"IMS\",                 \"lob\": \"PCD\",                 \"live\": true,                 \"custRid\": 7503             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"acctNum\": \"1200124349\",                 \"cusNum\": \"9500602\",                 \"sysTy\": \"IMS\",                 \"lob\": \"PCD\",                 \"live\": true,                 \"custRid\": 7503             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"acctNum\": \"1200124352\",                 \"cusNum\": \"9500602\",                 \"sysTy\": \"IMS\",                 \"lob\": \"TV\",                 \"live\": true,                 \"custRid\": 7503             },             \"oBillAry\": []         }     ],     \"oChatTok\": \"cfae808d19eca736acdc8376ca2b0e52a32df2f2a9c2cc0f93bc25c35f2ed4c2\",     \"oGnrlApptAry\": [],     \"oQualSvee\": {         \"bcifRec\": {             \"createPsn\": \"\",             \"createTs\": \"\",             \"custNm\": \"\",             \"xborder\": \"\",             \"industry\": \"\",             \"lastupdPsn\": \"\",             \"lastupdTs\": \"\",             \"nuPresence\": \"\",             \"nuStaff\": \"\",             \"priCtJobtitle\": \"\",             \"priCtMail\": \"\",             \"priCtMob\": \"\",             \"priCtName\": \"\",             \"priCtTel\": \"\",             \"priCtTitle\": \"\",             \"secCtTitle\": \"\",             \"secCtJobtitle\": \"\",             \"secCtMail\": \"\",             \"secCtMob\": \"\",             \"secCtName\": \"\",             \"secCtTel\": \"\",             \"rev\": 0,             \"custRid\": 0         },         \"bomCustAry\": [             {                 \"acctNum\": \"05054861\",                 \"cusNum\": \" \",                 \"lob\": \"O2F\",                 \"sysTy\": \"CSL\"             },             {                 \"acctNum\": \"1200094879\",                 \"cusNum\": \"9500602\",                 \"lob\": \"PCD\",                 \"sysTy\": \"IMS\"             }         ],         \"custRec\": {             \"createPsn\": \"dldsbat\",             \"createTs\": \"20151127164938\",             \"docNum\": \"88888891\",             \"docTy\": \"PASS\",             \"lastupdPsn\": \"dldsbat\",             \"lastupdTs\": \"20151127164938\",             \"phylum\": \"CSUM\",             \"premier\": \" \",             \"status\": \"A\",             \"rid\": 7503,             \"rev\": 0         },         \"subnRecAry\": [             {                 \"acctNum\": \"1200094879\",                 \"alias\": \"VAS, INC, CMT\",                 \"aloneDialup\": \"N\",                 \"assoc\": \"Y\",                 \"bsn\": \" \",                 \"createPsn\": \"dldsbat\",                 \"createTs\": \"20151127164938\",                 \"cusNum\": \"9500602\",                 \"wipCust\": \"N\",                 \"datCd\": \" \",                 \"domainTy\": \"N\",                 \"eyeGrp\": \" \",                 \"fsa\": \"60070059\",                 \"ind2l2b\": \"N\",                 \"indEye\": \"N\",                 \"indPcd\": \"Y\",                 \"indTv\": \"N\",                 \"instAdr\": \"FT 568 856/F ALICE GDN BLK 120, SHA TIN NEW TERRITORIES\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"lastupdTs\": \"20160602151811\",                 \"lob\": \"PCD\",                 \"netLoginId\": \"b60070059\",                 \"priMob\": \" \",                 \"refFsa2l2b\": \" \",                 \"tos\": \"IMS\",                 \"tariff\": \" \",                 \"sipSubr\": \" \",                 \"srvId\": \"60070059\",                 \"srvNum\": \"b60070059\",                 \"storeTy\": \"\",                 \"systy\": \"IMS\",                 \"rid\": 44775,                 \"rev\": 9,                 \"custRid\": 7503             },             {                 \"acctNum\": \"1200101993\",                 \"alias\": \"Vas\",                 \"aloneDialup\": \"N\",                 \"assoc\": \"Y\",                 \"bsn\": \" \",                 \"createPsn\": \"dldsbat\",                 \"createTs\": \"20151127164938\",                 \"cusNum\": \"9500602\",                 \"wipCust\": \"N\",                 \"datCd\": \" \",                 \"domainTy\": \"N\",                 \"eyeGrp\": \" \",                 \"fsa\": \"60069429\",                 \"ind2l2b\": \"N\",                 \"indEye\": \"N\",                 \"indPcd\": \"Y\",                 \"indTv\": \"N\",                 \"instAdr\": \"FT 96 996/F ROLLOUT HSE 3, CHCH, WAN CHAI HONG KONG\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"lastupdTs\": \"20160602151653\",                 \"lob\": \"PCD\",                 \"netLoginId\": \"a60069429\",                 \"priMob\": \" \",                 \"refFsa2l2b\": \" \",                 \"tos\": \"IMS\",                 \"tariff\": \" \",                 \"sipSubr\": \" \",                 \"srvId\": \"60069429\",                 \"srvNum\": \"a60069429\",                 \"storeTy\": \"\",                 \"systy\": \"IMS\",                 \"rid\": 44776,                 \"rev\": 56,                 \"custRid\": 7503             },             {                 \"acctNum\": \"1200102006\",                 \"alias\": \"All none\",                 \"aloneDialup\": \"N\",                 \"assoc\": \"Y\",                 \"bsn\": \" \",                 \"createPsn\": \"dldsbat\",                 \"createTs\": \"20151127164938\",                 \"cusNum\": \"9500602\",                 \"wipCust\": \"N\",                 \"datCd\": \" \",                 \"domainTy\": \"N\",                 \"eyeGrp\": \" \",                 \"fsa\": \"60069005\",                 \"ind2l2b\": \"N\",                 \"indEye\": \"N\",                 \"indPcd\": \"Y\",                 \"indTv\": \"N\",                 \"instAdr\": \"FT 11 111/F TEST PRIORITY (JOB CP6362), CHCH, WAN CHAI HONG KONG\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"lastupdTs\": \"20160617105401\",                 \"lob\": \"PCD\",                 \"netLoginId\": \"a60069005\",                 \"priMob\": \" \",                 \"refFsa2l2b\": \" \",                 \"tos\": \"IMS\",                 \"tariff\": \" \",                 \"sipSubr\": \" \",                 \"srvId\": \"60069005\",                 \"srvNum\": \"a60069005\",                 \"storeTy\": \"\",                 \"systy\": \"IMS\",                 \"rid\": 44777,                 \"rev\": 38,                 \"custRid\": 7503             },             {                 \"acctNum\": \"1200121942\",                 \"alias\": \"Vas\",                 \"aloneDialup\": \"N\",                 \"assoc\": \"Y\",                 \"bsn\": \" \",                 \"createPsn\": \"dldsbat\",                 \"createTs\": \"20151127164938\",                 \"cusNum\": \"9500602\",                 \"wipCust\": \"N\",                 \"datCd\": \" \",                 \"domainTy\": \"N\",                 \"eyeGrp\": \" \",                 \"fsa\": \"60069009\",                 \"ind2l2b\": \"N\",                 \"indEye\": \"N\",                 \"indPcd\": \"Y\",                 \"indTv\": \"N\",                 \"instAdr\": \"FT 909 999/F VP15 BLDG, HUNG HOM KOWLOON\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"lastupdTs\": \"20160602151641\",                 \"lob\": \"PCD\",                 \"netLoginId\": \"a60069009\",                 \"priMob\": \" \",                 \"refFsa2l2b\": \" \",                 \"tos\": \"IMS\",                 \"tariff\": \" \",                 \"sipSubr\": \" \",                 \"srvId\": \"60069009\",                 \"srvNum\": \"a60069009\",                 \"storeTy\": \"\",                 \"systy\": \"IMS\",                 \"rid\": 44778,                 \"rev\": 55,                 \"custRid\": 7503             },             {                 \"acctNum\": \"1200124349\",                 \"alias\": \"IMSB UNUCESS\",                 \"aloneDialup\": \"N\",                 \"assoc\": \"Y\",                 \"bsn\": \" \",                 \"createPsn\": \"dldsbat\",                 \"createTs\": \"20151127164938\",                 \"cusNum\": \"9500602\",                 \"wipCust\": \"N\",                 \"datCd\": \" \",                 \"domainTy\": \"N\",                 \"eyeGrp\": \" \",                 \"fsa\": \"60071827\",                 \"ind2l2b\": \"N\",                 \"indEye\": \"N\",                 \"indPcd\": \"Y\",                 \"indTv\": \"Y\",                 \"instAdr\": \"FT 23 234/F VP11 BLDG, KWUN TONG KOWLOON\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"lastupdTs\": \"20160602151729\",                 \"lob\": \"PCD\",                 \"netLoginId\": \"a60071827\",                 \"priMob\": \" \",                 \"refFsa2l2b\": \" \",                 \"tos\": \"IMS\",                 \"tariff\": \" \",                 \"sipSubr\": \" \",                 \"srvId\": \"60071827\",                 \"srvNum\": \"a60071827\",                 \"storeTy\": \"\",                 \"systy\": \"IMS\",                 \"rid\": 44779,                 \"rev\": 75,                 \"custRid\": 7503             },             {                 \"acctNum\": \"1200124352\",                 \"alias\": \"UXPERR\",                 \"aloneDialup\": \"N\",                 \"assoc\": \"Y\",                 \"bsn\": \" \",                 \"createPsn\": \"dldsbat\",                 \"createTs\": \"20151127164938\",                 \"cusNum\": \"9500602\",                 \"wipCust\": \"N\",                 \"datCd\": \" \",                 \"domainTy\": \"N\",                 \"eyeGrp\": \" \",                 \"fsa\": \"60071827\",                 \"ind2l2b\": \"N\",                 \"indEye\": \"N\",                 \"indPcd\": \"Y\",                 \"indTv\": \"Y\",                 \"instAdr\": \"FT 23 234/F VP11 BLDG, KWUN TONG KOWLOON\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"lastupdTs\": \"20160602152810\",                 \"lob\": \"TV\",                 \"netLoginId\": \"a60071827\",                 \"priMob\": \" \",                 \"refFsa2l2b\": \" \",                 \"tos\": \"IMS\",                 \"tariff\": \" \",                 \"sipSubr\": \" \",                 \"srvId\": \"60071827\",                 \"srvNum\": \"1200124352\",                 \"storeTy\": \"\",                 \"systy\": \"IMS\",                 \"rid\": 44780,                 \"rev\": 56,                 \"custRid\": 7503             }         ],         \"sveeRec\": {             \"createPsn\": \"!UNKN\",             \"createTs\": \"20151130103801\",             \"ctMail\": \"vincent.wm.fung@pccw.com\",             \"ctMob\": \"66230739\",             \"teamCd\": \" \",             \"lang\": \"en\",             \"lastupdPsn\": \"keith.kk.mak@pccw.com\",             \"lastupdTs\": \"20160620151033\",             \"loginId\": \"keith.kk.mak@pccw.com\",             \"mobAlrt\": \"Y\",             \"nickname\": \"KM12370aaa\",             \"promoOpt\": \"N\",             \"pwd\": \"\",             \"pwdEff\": \"Y\",             \"regnActn\": \"REGUSER\",             \"status\": \"A\",             \"stateUpTs\": \"20151130103824\",             \"salesChnl\": \" \",             \"secAns\": \"\",             \"state\": \"A\",             \"staffId\": \" \",             \"secQus\": 0,             \"rid\": 3,             \"rev\": 310,             \"custRid\": 7503         },         \"zmSubnRecAry\": [             {                 \"acctNum\": \"75100133007187\",                 \"alias\": \" \",                 \"aloneDialup\": \" \",                 \"assoc\": \"Y\",                 \"bsn\": \" \",                 \"createPsn\": \"acspbat\",                 \"createTs\": \"20160520123508\",                 \"cusNum\": \"10004631\",                 \"wipCust\": \"N\",                 \"datCd\": \" \",                 \"domainTy\": \" \",                 \"eyeGrp\": \" \",                 \"fsa\": \" \",                 \"ind2l2b\": \" \",                 \"indEye\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"instAdr\": \"\",                 \"lastupdPsn\": \"acspbat\",                 \"lastupdTs\": \"20160520123508\",                 \"lob\": \"MOB\",                 \"netLoginId\": \" \",                 \"priMob\": \" \",                 \"refFsa2l2b\": \" \",                 \"tos\": \" \",                 \"tariff\": \" \",                 \"sipSubr\": \"N\",                 \"srvId\": \"~ZOMBIE\",                 \"srvNum\": \" \",                 \"storeTy\": \"\",                 \"systy\": \"MOB\",                 \"rid\": 90499,                 \"rev\": 0,                 \"custRid\": 7503             }, 			{                 \"acctNum\": \"05054861\",                 \"alias\": \" \",                 \"aloneDialup\": \" \",                 \"assoc\": \"Y\",                 \"bsn\": \" \",                 \"createPsn\": \"acspbat\",                 \"createTs\": \"20160520123508\",                 \"cusNum\": \"10004631\",                 \"wipCust\": \"N\",                 \"datCd\": \" \",                 \"domainTy\": \" \",                 \"eyeGrp\": \" \",                 \"fsa\": \" \",                 \"ind2l2b\": \" \",                 \"indEye\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"instAdr\": \"\",                 \"lastupdPsn\": \"acspbat\",                 \"lastupdTs\": \"20160520123508\",                 \"lob\": \"O2F\",                 \"netLoginId\": \" \",                 \"priMob\": \" \",                 \"refFsa2l2b\": \" \",                 \"tos\": \" \",                 \"tariff\": \" \",                 \"sipSubr\": \"N\",                 \"srvId\": \"~ZOMBIE\",                 \"srvNum\": \" \",                 \"storeTy\": \"\",                 \"systy\": \"CSL\",                 \"rid\": 90499,                 \"rev\": 0,                 \"custRid\": 7503 			}         ]     },     \"oSoGud\": \"\",     \"oSrvReqAry\": [],     \"iRecallPwd\": false,     \"iRecallLgiId\": false,     \"iApi\": \"\",     \"iGwtGud\": \"\",     \"iMyFrm\": \"\",     \"oHgg\": \"\",     \"oNxUrl\": \"\",     \"reply\": {         \"code\": \"RC_SUCC\"     },     \"serverTS\": \"20160621190154\" }";
        //		}
        //					if (PLAN_MOB.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return		"{\"iBstUpHistEnDt\":\"\",\"iBstUpHistStDt\":\"\",\"iBstUpOff\":{},\"iLoginId\":\"csimtest001@abc.com\",\"iSubnRec\":{\"acctNum\":\"90657073\",\"alias\":\"lkjhgfdsaqwertyu\",\"aloneDialup\":\" \",\"assoc\":\"Y\",\"bsn\":\" \",\"createPsn\":\"acspbat\",\"createTs\":\"20160317164713\",\"cusNum\":\" \",\"wipCust\":\" \",\"datCd\":\" \",\"domainTy\":\" \",\"eyeGrp\":\" \",\"fsa\":\" \",\"ind2l2b\":\" \",\"indEye\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"instAdr\":\"\",\"lastupdPsn\":\"csimtest001@abc.com\",\"lastupdTs\":\"20160525113653\",\"lob\":\"O2F\",\"netLoginId\":\" \",\"priMob\":\" \",\"refFsa2l2b\":\" \",\"tos\":\"CSL\",\"tariff\":\" \",\"sipSubr\":\" \",\"srvId\":\"60530731\",\"srvNum\":\"60530731\",\"storeTy\":\"\",\"systy\":\"CSL\",\"rid\":90021,\"rev\":7,\"custRid\":26608},\"iTopupActn\":\"\",\"iTopupItem\":{\"displayOrder\":0},\"oBomBillEnDt\":\"\",\"oBomBillStDt\":\"\",\"oEntExpDT\":\"20160602000000\",\"oG3BoostUpOffer1DTOAry\":[],\"oG3RechargeHistDTOAry\":[{\"rechargeAmount\":\"50\",\"rechargeValueInMB\":\"1024\",\"transactionDateTime\":\"20160526000000\"}],\"oMobUsage\":{\"usgLastUpdTs\":\"\",\"g3AcctBomNextBillDateDTO\":{},\"g3DisplayServiceItemResultDTO\":{\"displayServiceItemList\":[{\"allowTopup\":\"N\",\"asOfDateChi\":\"最新紀錄\",\"asOfDateEng\":\"Latest Update\",\"planType\":\"BASIC\",\"region\":\"LOCAL\",\"serviceType\":\"Local Entitlement-based Data Plan\",\"showInMainPage\":\"Y\",\"titleLevel1Chi\":\"本地流動數據\",\"titleLevel1Eng\":\"Local Mobile Data\",\"titleLevel2Chi\":\"已用用量\",\"titleLevel2Eng\":\"Total Used\",\"usageBar\":{\"showBar\":\"Y\",\"colorB\":0,\"colorG\":204,\"colorR\":0,\"barPercent\":33},\"usageDescription\":{\"showWarningIcon\":\"N\",\"textChi\":\"尚餘: 33% (2324MB)\",\"textEng\":\"33% (2324MB) Remaining\",\"textColorG\":0,\"textColorR\":0,\"textColorB\":0},\"displayOrder\":0}]},\"g3GetUnbilledDailyRoamWifiResultDTO\":{},\"g3SummUsageQuotaDTO\":{\"asOfDate\":\"20160531\",\"dataAsOfDate\":\"20160601\",\"iddVoiceUsage\":\"0\",\"localBBUnit\":\"KB\",\"localBBUsage\":\"0\",\"localSMSInterUsage\":\"0\",\"localVoiceInterUsage\":\"0\",\"mobileLocalDataUnit\":\"GB\",\"mobileLocalDataUsage\":\"4.74\",\"mobileRoamingDataUnit\":\"KB\",\"mobileRoamingDataUsage\":\"0\",\"overseasSMSUsage\":\"0\",\"primaryFlag\":\"N\",\"roamBBUnit\":\"KB\",\"roamBBUsage\":\"0\",\"roamSMSUsage\":\"0\"},\"g3UsageQuotaDTO\":{\"asOfDate\":\"20160531\",\"dataAsOfDate\":\"20160601\",\"iddVoiceUsage\":\"0\",\"localBBUnit\":\"KB\",\"localBBUsage\":\"0\",\"localSMSInterUsage\":\"0\",\"localVoiceInterUsage\":\"0\",\"mobileLocalDataUnit\":\"GB\",\"mobileLocalDataUsage\":\"4.74\",\"mobileRoamingDataUnit\":\"KB\",\"mobileRoamingDataUsage\":\"0\",\"overseasSMSUsage\":\"0\",\"primaryFlag\":\"P\",\"roamBBUnit\":\"KB\",\"roamBBUsage\":\"0\",\"roamSMSUsage\":\"0\"},\"iSMS\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lSMSIter\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lSMSItra\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lVceIter\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lVceItra\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lVdoIter\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lVdoItra\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"localData\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"moov\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"nowSports\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"nowTv\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"optVasAry\":[],\"ratePlan\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"entBased\":false},\"oMthlyPkgMB\":\"6144\",\"oTtlEntMB\":\"7168\",\"oVbpCustTy\":\"3\",\"iApi\":\"PLAN_MOB\",\"iGwtGud\":\"3DF3758D621C4CFE\",\"iMyFrm\":\"\",\"oHgg\":\"\",\"oNxUrl\":\"\",\"reply\":{\"code\":\"RC_SUCC\"},\"serverTS\":\"20160531205846\"}";

        //				return "{\"iBstUpHistEnDt\":\"\",\"iBstUpHistStDt\":\"\",\"iBstUpOff\":{},\"iLoginId\":\"csimtest001@abc.com\",\"iSubnRec\":{\"acctNum\":\"90657073\",\"alias\":\"lkjhgfdsaqwertyu\",\"aloneDialup\":\" \",\"assoc\":\"Y\",\"bsn\":\" \",\"createPsn\":\"acspbat\",\"createTs\":\"20160317164713\",\"cusNum\":\" \",\"wipCust\":\" \",\"datCd\":\" \",\"domainTy\":\" \",\"eyeGrp\":\" \",\"fsa\":\" \",\"ind2l2b\":\" \",\"indEye\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"instAdr\":\"\",\"lastupdPsn\":\"csimtest001@abc.com\",\"lastupdTs\":\"20160525113653\",\"lob\":\"O2F\",\"netLoginId\":\" \",\"priMob\":\" \",\"refFsa2l2b\":\" \",\"tos\":\"CSL\",\"tariff\":\" \",\"sipSubr\":\" \",\"srvId\":\"60530731\",\"srvNum\":\"60530731\",\"storeTy\":\"\",\"systy\":\"CSL\",\"rid\":90021,\"rev\":7,\"custRid\":26608},\"iTopupActn\":\"\",\"iTopupItem\":{\"displayOrder\":0},\"oBomBillEnDt\":\"\",\"oBomBillStDt\":\"\",\"oEntExpDT\":\"\",\"oG3BoostUpOffer1DTOAry\":[],\"oG3RechargeHistDTOAry\":[],\"oMobUsage\":{\"usgLastUpdTs\":\"\",\"g3AcctBomNextBillDateDTO\":{},\"g3DisplayServiceItemResultDTO\":{},\"g3GetUnbilledDailyRoamWifiResultDTO\":{},\"g3SummUsageQuotaDTO\":{},\"g3UsageQuotaDTO\":{},\"iSMS\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lSMSIter\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lSMSItra\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lVceIter\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lVceItra\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lVdoIter\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"lVdoItra\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"localData\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"moov\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"nowSports\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"nowTv\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"optVasAry\":[],\"ratePlan\":{\"EnDt\":\"\",\"StDt\":\"\",\"cmmtInd\":\"\",\"cntrDur\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\",\"desnEn\":\"\",\"desnZh\":\"\",\"fee\":\"\",\"longDesn1En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2En\":\"\",\"longDesn2Zh\":\"\",\"m2mRate\":\"\",\"monthlyTermRate\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"rebtAmt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"seasInd\":\"\",\"srvCd\":\"\",\"srvCmmtPrd\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"srvPenalty\":\"\",\"summInd\":\"\",\"suppInd\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgEnDt\":\"\",\"usgStDt\":\"\",\"usgUnt\":\"\"},\"entBased\":false},\"oMthlyPkgMB\":\"\",\"oTtlEntMB\":\"\",\"oVbpCustTy\":\"\",\"iApi\":\"PLAN_MOB\",\"iGwtGud\":\"40CF5709CD444C21\",\"iMyFrm\":\"\",\"oHgg\":\"\",\"oNxUrl\":\"\",\"reply\":{\"code\":\"RC_TIMEOUT\"},\"serverTS\":\"20160531211006\"}";
        //				}


        //				if (APPT.equalsIgnoreCase(apiRequest.getActionTy())) {
        //				return "{     \"serverTS\": \"20160531143258\",     \"iSubnRec\": {         \"rev\": 0,         \"createTs\": \"\",         \"netLoginId\": \"\",         \"custRid\": 0,         \"systy\": \"\",         \"instAdr\": \"\",         \"eyeGrp\": \"\",         \"sipSubr\": \"\",         \"wipCust\": \"\",         \"storeTy\": \"\",         \"lastupdTs\": \"\",         \"datCd\": \"\",         \"indTv\": \"\",         \"lastupdPsn\": \"\",         \"lob\": \"\",         \"tos\": \"\",         \"indPcd\": \"\",         \"auth_ph\": null,         \"tariff\": \"\",         \"indEye\": \"\",         \"createPsn\": \"\",         \"cusNum\": \"\",         \"assoc\": \"\",         \"fsa\": \"\",         \"acct_ty\": null,         \"alias\": \"\",         \"acctNum\": \"\",         \"ind2l2b\": \"\",         \"domainTy\": \"\",         \"srvNum\": \"\",         \"bsn\": \"\",         \"rid\": 0,         \"refFsa2l2b\": \"\",         \"srvId\": \"\",         \"priMob\": \"\",         \"aloneDialup\": \"\",         \"ivr_pwd\": null     },     \"iGwtGud\": \"33551702E0BD4B7F\",     \"oDuct\": null,     \"oSrvReqAry\": [],     \"iLoginId\": \"ncr01@pccw.com\",     \"iSRApptInfo\": {         \"apptTS\": {             \"apptDT\": \"\",             \"apptTmslot\": \"\",             \"apptDate\": \"\"         },         \"apptTSAry\": [],         \"prodId\": \"\",         \"refNum\": \"\",         \"distCd\": \"\",         \"areaCd\": \"\",         \"prodTy\": \"\",         \"srvTy\": \"\",         \"srvNum\": \"\"     },     \"oGnrlApptAry\": [         {             \"ocId\": \"21738169\",             \"srvAry\": [                 \"TEL\"             ],             \"iAdr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"drgOrdNumAry\": [                 \"106073528001\"             ],             \"srvBndy\": \"83764\",             \"apptStDT\": \"20160607140000\",             \"showDtls\": true,             \"apptEnDT\": \"20160603160000\"         }     ],     \"oPendSrvReq\": {         \"assocSubnRec\": {             \"rev\": 0,             \"createTs\": \"\",             \"netLoginId\": \"\",             \"custRid\": 0,             \"systy\": \"\",             \"instAdr\": \"\",             \"eyeGrp\": \"\",             \"sipSubr\": \"\",             \"wipCust\": \"\",             \"storeTy\": \"\",             \"lastupdTs\": \"\",             \"datCd\": \"\",             \"indTv\": \"\",             \"lastupdPsn\": \"\",             \"lob\": \"\",             \"tos\": \"\",             \"indPcd\": \"\",             \"auth_ph\": null,             \"tariff\": \"\",             \"indEye\": \"\",             \"createPsn\": \"\",             \"cusNum\": \"\",             \"assoc\": \"\",             \"fsa\": \"\",             \"acct_ty\": null,             \"alias\": \"\",             \"acctNum\": \"\",             \"ind2l2b\": \"\",             \"domainTy\": \"\",             \"srvNum\": \"\",             \"bsn\": \"\",             \"rid\": 0,             \"refFsa2l2b\": \"\",             \"srvId\": \"\",             \"priMob\": \"\",             \"aloneDialup\": \"\",             \"ivr_pwd\": null         },         \"enSpkr\": \"\",         \"autoSRInd\": \"\",         \"updTy\": \"\",         \"srInfo\": {             \"flatCd\": \"\",             \"floorCd\": \"\",             \"fsa\": \"\",             \"bsn\": \"\",             \"mdmRst\": \"\",             \"custAdr\": \"\",             \"ponCvg\": \"\",             \"ctNum\": \"\",             \"srvBndy\": \"\",             \"ponLine\": \"\",             \"ctNm\": \"\"         },         \"ctNmTtl\": \"\",         \"ctNm\": \"\",         \"srvNum\": \"\",         \"apptTS\": {             \"apptDT\": \"\",             \"apptTmslot\": \"\",             \"apptDate\": \"\"         },         \"extNum\": \"\",         \"smsLang\": \"\",         \"trunkNum\": \"\",         \"srvNumTy\": \"\",         \"ctNum\": \"\",         \"refNum\": \"\",         \"outProd\": \"\",         \"reportNum\": \"\",         \"allowUpdInd\": \"\",         \"apptInfo\": {             \"apptTS\": {                 \"apptDT\": \"\",                 \"apptTmslot\": \"\",                 \"apptDate\": \"\"             },             \"apptTSAry\": [],             \"prodId\": \"\",             \"refNum\": \"\",             \"distCd\": \"\",             \"areaCd\": \"\",             \"prodTy\": \"\",             \"srvTy\": \"\",             \"srvNum\": \"\"         }     },     \"oNxUrl\": \"\",     \"iMyFrm\": \"\",     \"iCustRec\": {         \"docNum\": \"A133138(0)\",         \"phylum\": \"CSUM\",         \"rev\": 1323,         \"status\": \"A\",         \"rid\": 1323,         \"createTs\": \"20151127163349\",         \"premier\": \" \",         \"lastupdTs\": \"20151127163349\",         \"docTy\": \"HKID\",         \"createPsn\": \"dldsbat\",         \"lastupdPsn\": \"dldsbat\"     },     \"iSrvReq\": {         \"assocSubnRec\": {             \"rev\": 0,             \"createTs\": \"\",             \"netLoginId\": \"\",             \"custRid\": 0,             \"systy\": \"\",             \"instAdr\": \"\",             \"eyeGrp\": \"\",             \"sipSubr\": \"\",             \"wipCust\": \"\",             \"storeTy\": \"\",             \"lastupdTs\": \"\",             \"datCd\": \"\",             \"indTv\": \"\",             \"lastupdPsn\": \"\",             \"lob\": \"\",             \"tos\": \"\",             \"indPcd\": \"\",             \"auth_ph\": null,             \"tariff\": \"\",             \"indEye\": \"\",             \"createPsn\": \"\",             \"cusNum\": \"\",             \"assoc\": \"\",             \"fsa\": \"\",             \"acct_ty\": null,             \"alias\": \"\",             \"acctNum\": \"\",             \"ind2l2b\": \"\",             \"domainTy\": \"\",             \"srvNum\": \"\",             \"bsn\": \"\",             \"rid\": 0,             \"refFsa2l2b\": \"\",             \"srvId\": \"\",             \"priMob\": \"\",             \"aloneDialup\": \"\",             \"ivr_pwd\": null         },         \"enSpkr\": \"\",         \"autoSRInd\": \"\",         \"updTy\": \"\",         \"srInfo\": {             \"flatCd\": \"\",             \"floorCd\": \"\",             \"fsa\": \"\",             \"bsn\": \"\",             \"mdmRst\": \"\",             \"custAdr\": \"\",             \"ponCvg\": \"\",             \"ctNum\": \"\",             \"srvBndy\": \"\",             \"ponLine\": \"\",             \"ctNm\": \"\"         },         \"ctNmTtl\": \"\",         \"ctNm\": \"\",         \"srvNum\": \"\",         \"apptTS\": {             \"apptDT\": \"\",             \"apptTmslot\": \"\",             \"apptDate\": \"\"         },         \"extNum\": \"\",         \"smsLang\": \"\",         \"trunkNum\": \"\",         \"srvNumTy\": \"\",         \"ctNum\": \"\",         \"refNum\": \"\",         \"outProd\": \"\",         \"reportNum\": \"\",         \"allowUpdInd\": \"\",         \"apptInfo\": {             \"apptTS\": {                 \"apptDT\": \"\",                 \"apptTmslot\": \"\",                 \"apptDate\": \"\"             },             \"apptTSAry\": [],             \"prodId\": \"\",             \"refNum\": \"\",             \"distCd\": \"\",             \"areaCd\": \"\",             \"prodTy\": \"\",             \"srvTy\": \"\",             \"srvNum\": \"\"         }     },     \"oHgg\": \"\",     \"oBomApptAry\": [         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21585686\",             \"acctNum\": \"\",             \"drgOrdNum\": \"100071581001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150731140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20150731160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690016\",             \"srvBndy\": \"81709\",             \"grpNum\": \"1002752900\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21587172\",             \"acctNum\": \"\",             \"drgOrdNum\": \"100069213001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150731140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20150731160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036000115\",             \"srvBndy\": \"83764\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21587404\",             \"acctNum\": \"\",             \"drgOrdNum\": \"100068979001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150731140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20150731160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000034013567\",             \"srvBndy\": \"83764\",             \"grpNum\": \"1002756500\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21608103\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069217001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150831140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20150831160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"FT 1 1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000035656246\",             \"srvBndy\": \"83764\",             \"grpNum\": \"1002794300\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21607892\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069449001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150831160000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20150831180000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690037\",             \"srvBndy\": \"83764\",             \"grpNum\": \"1002793400\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21620097\",             \"acctNum\": \"\",             \"drgOrdNum\": \"100071170001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150925180000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20150925200000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"Y\",             \"Adr\": \"1/F, EYE 2 VILLA BLK 10, EYE 2 VLG, 123 EYE TWO ROAD, NORTH POINT, HONG KONG\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690605\",             \"srvBndy\": \"228380\",             \"grpNum\": \"1002817500\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21612340\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109071098001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150930140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20150930160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690648\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21613606\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069245001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150930140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20150930160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036680732\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21615239\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069477001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150930140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20150930160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690118\",             \"srvBndy\": \"81709\",             \"grpNum\": \"1002812700\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21612199\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069225001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150930140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20150930160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690372\",             \"srvBndy\": \"83764\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21612214\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069084001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150930140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20150930160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690742\",             \"srvBndy\": \"83764\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21615217\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109067692001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20150930140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20150930160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690326\",             \"srvBndy\": \"83764\",             \"grpNum\": \"1002812600\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21611213\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069743001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151006140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151006160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036000842\",             \"srvBndy\": \"83764\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21607582\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069553001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151013140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151013160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036680197\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21613798\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069316001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151013140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20151013160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"7/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690353\",             \"srvBndy\": \"81709\",             \"grpNum\": \"1002809700\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21623104\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069627001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151013140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20151013160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690100\",             \"srvBndy\": \"81709\",             \"grpNum\": \"1002825500\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21610522\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069367001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151013140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20151013160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690703\",             \"srvBndy\": \"83764\",             \"grpNum\": \"1002803100\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21608767\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109072211001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151014140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151014160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000034013572\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21609366\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109070820001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151019140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20151019160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036680759\",             \"srvBndy\": \"81709\",             \"grpNum\": \"1002800600\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21609791\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109070840001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151019140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20151019160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690586\",             \"srvBndy\": \"83764\",             \"grpNum\": \"1002801500\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21612007\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109070910001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151019160000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151019180000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036680034\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21610315\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069165001\",             \"grpTy\": \"EYE3\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151024140000\",             \"datCd\": \"NCR\",             \"apptEnDT\": \"20151024160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036680220\",             \"srvBndy\": \"81709\",             \"grpNum\": \"1002802700\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21611903\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069397001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151028140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151028160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036000395\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21613282\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109071118001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151028140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151028160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690207\",             \"srvBndy\": \"83764\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21612188\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109071088001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151030140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151030160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036000333\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21613514\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109067602001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151031140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151031160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036000818\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21617714\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109067742001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151031140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151031160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690339\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21613167\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109069773001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151119140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151119160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036690029\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21615103\",             \"acctNum\": \"\",             \"drgOrdNum\": \"109067682001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20151119140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20151119160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000036680691\",             \"srvBndy\": \"81709\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21738169\",             \"acctNum\": \"\",             \"drgOrdNum\": \"106073528001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20160601140000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20160601160000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"1/F, BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN, NEW TERRITORIES\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000039911099\",             \"srvBndy\": \"83764\",             \"grpNum\": \"\"         }     ],     \"iEnqSRTy\": \"\",     \"iSubnRecAry\": [         {             \"rev\": 0,             \"createTs\": \"20151127163349\",             \"netLoginId\": \" \",             \"custRid\": 1323,             \"systy\": \"DRG\",             \"instAdr\": \"1/F BLK 2 GREENDOT CT, 9-11 SAI SHA ROAD, SHA TIN NEW TERRITORIES\",             \"eyeGrp\": \" \",             \"sipSubr\": \" \",             \"wipCust\": \"N\",             \"storeTy\": \"\",             \"lastupdTs\": \"20151127163349\",             \"datCd\": \"DEL\",             \"indTv\": \" \",             \"lastupdPsn\": \"dldsbat\",             \"lob\": \"LTS\",             \"tos\": \"TEL\",             \"indPcd\": \" \",             \"auth_ph\": null,             \"tariff\": \"R\",             \"indEye\": \"N\",             \"createPsn\": \"dldsbat\",             \"cusNum\": \"23001005\",             \"assoc\": \"Y\",             \"fsa\": \" \",             \"acct_ty\": null,             \"alias\": \" \",             \"acctNum\": \"23001005011594\",             \"ind2l2b\": \" \",             \"domainTy\": \" \",             \"srvNum\": \"36680763\",             \"bsn\": \" \",             \"rid\": 3318,             \"refFsa2l2b\": \" \",             \"srvId\": \"100151143\",             \"priMob\": \" \",             \"aloneDialup\": \" \",             \"ivr_pwd\": null         },         {             \"rev\": 0,             \"createTs\": \"20151127163349\",             \"netLoginId\": \" \",             \"custRid\": 1323,             \"systy\": \"DRG\",             \"instAdr\": \"1/F BLK 1 GREENFIELD CT, 9-11 SHATIN WAI ROAD, SHA TIN NEW TERRITORIES\",             \"eyeGrp\": \"1002805500\",             \"sipSubr\": \" \",             \"wipCust\": \"N\",             \"storeTy\": \"\",             \"lastupdTs\": \"20151127163349\",             \"datCd\": \"NCR\",             \"indTv\": \" \",             \"lastupdPsn\": \"dldsbat\",             \"lob\": \"LTS\",             \"tos\": \"TEL\",             \"indPcd\": \" \",             \"auth_ph\": null,             \"tariff\": \"R\",             \"indEye\": \"Y\",             \"createPsn\": \"dldsbat\",             \"cusNum\": \"23001005\",             \"assoc\": \"Y\",             \"fsa\": \" \",             \"acct_ty\": null,             \"alias\": \" \",             \"acctNum\": \"23001005015190\",             \"ind2l2b\": \" \",             \"domainTy\": \" \",             \"srvNum\": \"34013534\",             \"bsn\": \" \",             \"rid\": 3319,             \"refFsa2l2b\": \" \",             \"srvId\": \"100151188\",             \"priMob\": \" \",             \"aloneDialup\": \" \",             \"ivr_pwd\": null         }     ],     \"reply\": {         \"cargo\": null,         \"code\": \"RC_SUCC\"     },     \"iApi\": \"APPT\",     \"oSRApptInfo\": {         \"apptTS\": {             \"apptDT\": \"\",             \"apptTmslot\": \"\",             \"apptDate\": \"\"         },         \"apptTSAry\": [],         \"prodId\": \"\",         \"refNum\": \"\",         \"distCd\": \"\",         \"areaCd\": \"\",         \"prodTy\": \"\",         \"srvTy\": \"\",         \"srvNum\": \"\"     } }";
        //				}
        //				if (PLAN_MOB.equalsIgnoreCase(apiRequest.getActionTy())) {
        //					return "{ 	\"iLoginId\": \"keith.kk.mak@pccw.com\", 	\"iSubnRec\": { 		\"tos\": \"\", 		\"alias\": \"\", 		\"ref_fsa_2l2b\": \"\", 		\"lastupd_ts\": \"\", 		\"assoc\": \"\", 		\"ind_2l2b\": \"\", 		\"systy\": \"\", 		\"domain_ty\": \"\", 		\"cus_num\": \"\", 		\"create_psn\": \"\", 		\"acct_ty\": \"\", 		\"wip_cust\": \"\", 		\"bsn\": \"\", 		\"fsa\": \"\", 		\"ivr_pwd\": \"\", 		\"ind_eye\": \"\", 		\"lastupd_psn\": \"\", 		\"tariff\": \"\", 		\"net_login_id\": \"\", 		\"eye_grp\": \"\", 		\"dat_cd\": \"\", 		\"eff_enddate\": \"\", 		\"lob\": \"MOB\", 		\"ind_pcd\": \"\", 		\"create_ts\": \"\", 		\"srv_id\": \"\", 		\"inst_addr\": \"\", 		\"pri_login_ind\": \"\", 		\"acct_num\": \"\", 		\"auth_ph\": \"\", 		\"line_status\": \"\", 		\"pri_mob\": \"\", 		\"store_ty\": \"\", 		\"cust_rid\": 0, 		\"rev\": 0, 		\"rid\": 0, 		\"sip_subr\": \"\", 		\"eff_begdate\": \"\", 		\"srv_num\": \"\", 		\"ind_tv\": \"\" 	}, 	\"oMobUsage\": { 		\"ratePlan\": { 			\"srvCd\": \"MRP0001143\", 			\"fee\": \"389.0\", 			\"desnEn\": \"MonthlyMobileServiceFee\", 			\"desnZh\": \"基本通話月費\", 			\"longDesn1En\": \"\", 			\"longDesn2En\": \"\", 			\"longDesn1Zh\": \"\", 			\"longDesn2Zh\": \"\", 			\"tncEn\": \"\", 			\"tncZh\": \"\", 			\"usg\": \"\", 			\"usgStDt\": \"\", 			\"usgEnDt\": \"\", 			\"usgUnt\": \"\", 			\"StDt\": \"\", 			\"EnDt\": \"\", 			\"srvCmmtPrd\": \"\", 			\"srvPenalty\": \"\", 			\"srvCmmtRemn\": \"\", 			\"srvEnt\": \"\", 			\"rebtEn\": \"\", 			\"rebtZh\": \"\", 			\"rebtAmt\": \"\", 			\"monthlyTermRate\": \"\", 			\"m2mRate\": \"\", 			\"plnTy\": \"\", 			\"plnSubTy\": \"\", 			\"cntrStDt\": \"\", 			\"cntrEnDt\": \"\", 			\"cntrDur\": \"\", 			\"cmmtInd\": \"\", 			\"seasInd\": \"\", 			\"suppInd\": \"\", 			\"summInd\": \"\" 		}, 		\"g3DisplayServiceItemResultDTO\": { 			\"acctType\": \"nornal\", 			\"rtnCode\": \"0\", 			\"rtnMsg\": \"Operationsucceeded\", 			\"displayServiceItemList\": [{ 				\"titleLevel1Chi\": \"本地流動數據\", 				\"additionalMessage\": {  				}, 				\"titleLevel1Eng\": \"Local Mobile Data\", 				\"allowTopup\": \"Y\", 				\"quotaTopupInfo\": { 					\"initialQuotaInKB\": 5242880, 					\"quotaValidityPeriod\": \"B\", 					\"pcrfServiceName\": \"Local_Quota_3G_1\", 					\"pcrfServiceType\": \"LocalTopupPlan\", 					\"topupQuotaName\": \"Quota_LocalQuota3G1\", 					\"displayNameInEnglish\": \"LocalMobileData\" 				}, 				\"asOfDateEng\": \"Latest update\", 				\"show\": \"Y\", 				\"usageBar\": { 					\"showBar\": \"Y\", 					\"barPercent\": 100, 					\"colorB\": 0, 					\"colorG\": 204, 					\"colorR\": 0 				}, 				\"otherVAS\": \"N\", 				\"showInMainPage\": \"Y\", 				\"displayOrder\": 10, 				\"serviceType\": \"LocalTopupPlan\", 				\"region\": \"LOCAL\", 				\"planType\": \"BASIC\", 				\"usageDescription\": { 					\"textColorG\": 0, 					\"showWarningIcon\": \"N\", 					\"textColorR\": 0, 					\"textChi\": \"尚餘: 100%(5, 120MB)\", 					\"textEng\": \"100%(5, 120MB)Remaining\", 					\"textColorB\": 0, 					\"htmlChi\": \"尚餘: 100%(5, 120MB)\", 					\"htmlEng\": \"100%(5,120MB)Remaining\" 				}, 				\"asOfDateChi\": \"最新紀錄\", 				\"refInfo\": { 					\"quotaServiceItemList\": [  					] 				}, 				\"usageType\": \"QUOTA-BASED\" 			}, { 				\"titleLevel1Chi\": \"漫遊流動數據\", 				\"titleLevel2Chi\": \"日費計劃\", 				\"additionalMessage\": {  				}, 				\"titleLevel1Eng\": \"Roaming Mobile Data\", 				\"titleLevel2Eng\": \"Day Pass\", 				\"allowTopup\": \"N\", 				\"asOfDateEng\": \"\", 				\"quotaTopupInfo\": { 					\"initialQuotaInKB\": 20480, 					\"quotaValidityPeriod\": \"D\", 					\"pcrfServiceName\": \"Roaming_QuotaDayP_3G_3\", 					\"pcrfServiceType\": \"RoamingAll-In-OneDayPass\", 					\"topupQuotaName\": \"Quota_RoamQuotaDP3G3\", 					\"displayNameInEnglish\": \"RoamingMobileData\" 				}, 				\"show\": \"Y\", 				\"usageBar\": { 					\"showBar\": \"N\", 					\"barPercent\": 100, 					\"colorB\": 0, 					\"colorG\": 204, 					\"colorR\": 0 				}, 				\"showInMainPage\": \"Y\", 				\"displayOrder\": 10, 				\"serviceType\": \"RoamingAll-In-OneDayPass\", 				\"region\": \"ROAMING\", 				\"planType\": \"BASIC\", 				\"usageDescription\": { 					\"textColorG\": 0, 					\"showWarningIcon\": \"N\", 					\"textColorR\": 0, 					\"textChi\": \"尚餘0%(0MB)\", 					\"textEng\": \"0%(0MB)Remaining\", 					\"textColorB\": 0, 					\"htmlChi\": \"尚餘0%(0MB)\", 					\"htmlEng\": \"0%(0MB)Remaining\" 				}, 				\"asOfDateChi\": \"\", 				\"refInfo\": { 					\"quotaServiceItemList\": [  					] 				}, 				\"usageType\": \"QUOTA-BASED\" 			}, { 				\"allowTopup\": \"N\", 				\"region\": \"ROAMING\", 				\"asOfDateEng\": \"\", 				\"quotaTopupInfo\": {  				}, 				\"additionalMessage\": {  				}, 				\"showInMainPage\": \"Y\", 				\"refInfo\": { 					\"entitlementUsedUsage\": \"0\", 					\"entitlementValue\": \"60\", 					\"quotaServiceItemList\": [  					], 					\"entitlementUnit\": \"mins\" 				}, 				\"otherVAS\": \"N\", 				\"displayOrder\": 10000, 				\"planType\": \"OTHER\", 				\"show\": \"Y\", 				\"usageDescription\": { 					\"textColorG\": 0, 					\"showWarningIcon\": \"N\", 					\"textColorR\": 0, 					\"textChi\": \"已用用量: 0分鐘|可享用量: 0分鐘\", 					\"textEng\": \"Used: 0mins|Entitled: 0mins\", 					\"textColorB\": 0, 					\"htmlChi\": \"已用用量: 0分鐘|可享用量: 0分鐘\", 					\"htmlEng\": \"Used: 0mins|Entitled: 0mins\" 				}, 				\"usageType\": \"ENTITLEMENT-BASED\", 				\"serviceType\": \"RoamingWIFIDayPass\", 				\"titleLevel1Chi\": \"WiFi漫遊\", 				\"usageBar\": { 					\"showBar\": \"N\", 					\"barPercent\": 100, 					\"colorB\": 0, 					\"colorG\": 204, 					\"colorR\": 0 				}, 				\"titleLevel2Chi\": \"日費計劃\", 				\"titleLevel1Eng\": \"Roaming WiFi\", 				\"titleLevel2Eng\": \"Day Pass\", 				\"asOfDateChi\": \"\" 			}] 		}, 		\"g3UsageQuotaDTO\": { 			\"overseasSMSEntitlement\": \"0\", 			\"moovUsage\": \"0\", 			\"overseasSMSUsage\": \"0\", 			\"localVideoIntraUnit\": \"mins\", 			\"localSMSIntraEntitlement\": \"Unlimit\", 			\"emailEntitlement\": \"0\", 			\"localMMSIntraEntitlement\": \"300\", 			\"moovEntitlement\": \"Unlimit\", 			\"localVoiceIntraUsage\": \"0\", 			\"overseasMMSUsage\": \"0\", 			\"localSMSInterEntitlement\": \"0\", 			\"localMMSIntraUsage\": \"0\", 			\"emailUsage\": \"0\", 			\"localVoicePeakOffpeakBasedInd\": \"N\", 			\"localVoiceInterUsage\": \"0\", 			\"overseasMMSEntitlement\": \"0\", 			\"localVideoIntraUsage\": \"0\", 			\"localVoiceIntraEntitlement\": \"0\", 			\"localMMSInterEntitlement\": \"0\", 			\"localSMSInterUsage\": \"0\", 			\"localVoiceInterUnit\": \"mins\", 			\"nowTVEntitlement\": \"Unlimit\", 			\"localVoiceInterEntitlement\": \"Unlimit\", 			\"localVideoInterUsage\": \"0\", 			\"localVideoIntraEntitlement\": \"300\", 			\"localVoicePeakEntitlement\": \"0\", 			\"localVoiceOffPeakUsage\": \"0\", 			\"nowSportsUsage\": \"0\", 			\"localMMSInterUsage\": \"0\", 			\"localVideoInterEntitlement\": \"0\", 			\"nonMobileUsageEntitlementRtnCode\": \"0\", 			\"localVideoInterUnit\": \"mins\", 			\"localVoicePeakUnit\": \"mins\", 			\"localVoiceOffPeakEntitlement\": \"0\", 			\"nowTVUsage\": \"0\", 			\"nonMobileUsageEntitlementRtnMsg\": \"\", 			\"nowSportsUnit\": \"mins\", 			\"nowSportsEntitlement\": \"0\", 			\"moovUnit\": \"mins\", 			\"localVoiceIntraUnit\": \"mins\", 			\"nowTVUnit\": \"mins\", 			\"localVoicePeakUsage\": \"0\", 			\"asOfDate\": \"20160128\", 			\"localSMSIntraUsage\": \"0\" 		}, 		\"g3SummUsageQuotaDTO\": {  		}, 		\"g3AcctBomNextBillDateDTO\": { 			\"result\": {  			}, 			\"acctNum\": \"\", 			\"bomNextBillDate\": \"10/02/2016\", 			\"bomStartBillDate\": \"10/01/2016\" 		} 	}, 	\"iApi\": \"PLAN_MOB\", 	\"serverTS\": \"20160127121212\", 	\"reply\": { 		\"code\": \"RC_SUCC\", 		\"cargo\": null 	} }";
        //					}

        //AUTH success
        //		if (AO_AUTH.equalsIgnoreCase(apiRequest.getActionTy())) {

        //		return "{\"iLoginId\":\"\",\"iSubnRec\":{\"rid\":1,\"custRid\":7503,\"cusNum\":\"10004631\",\"acctNum\":\"73001278660919\",\"srvId\":\"100121382\",\"srvNum\":\"98238921\",\"bsn\":\" \",\"fsa\":\" \",\"lob\":\"MOB\",\"wipCust\":\"N\",\"sipSubr\":\"N\",\"netLoginId\":\" \",\"aloneDialup\":\" \",\"eyeGrp\":\" \",\"ind2l2b\":\" \",\"refFsa2l2b\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"indEye\":\" \",\"domainTy\":\" \",\"tos\":\"3G\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"MOB\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127162542\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20151127162542\",\"lastupdPsn\":\"dldsbat\",\"rev\":0,\"instAdr\":\"\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"iSms\":true,\"oSubnRec\":{\"rid\":1,\"custRid\":7503,\"cusNum\":\"10004631\",\"acctNum\":\"73001278660919\",\"srvId\":\"100121382\",\"srvNum\":\"98238921\",\"bsn\":\" \",\"fsa\":\" \",\"lob\":\"XMOB\",\"wipCust\":\"N\",\"sipSubr\":\"N\",\"netLoginId\":\" \",\"aloneDialup\":\" \",\"eyeGrp\":\" \",\"ind2l2b\":\" \",\"refFsa2l2b\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"indEye\":\" \",\"domainTy\":\" \",\"tos\":\"3G\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"MOB\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127162542\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20151127162542\",\"lastupdPsn\":\"dldsbat\",\"rev\":0,\"instAdr\":\"\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"iApi\":\"\",\"iGwtGud\":\"\",\"iMyFrm\":\"\",\"oHgg\":\"\",\"oNxUrl\":\"\",\"oDuct\":null,\"reply\":{\"code\":\"RC_SUCC\"},\"serverTS\":\"20151229153146\"}";
        //AUTH   H INDIV
        //		return "{\"iLoginId\":\"\",\"iSubnRec\":{\"rid\":1,\"custRid\":7503,\"cusNum\":\"10004631\",\"acctNum\":\"73001278660919\",\"srvId\":\"100121382\",\"srvNum\":\"98238921\",\"bsn\":\" \",\"fsa\":\" \",\"lob\":\"MOB\",\"wipCust\":\"N\",\"sipSubr\":\"N\",\"netLoginId\":\" \",\"aloneDialup\":\" \",\"eyeGrp\":\" \",\"ind2l2b\":\" \",\"refFsa2l2b\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"indEye\":\" \",\"domainTy\":\" \",\"tos\":\"3G\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"MOB\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127162542\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20151127162542\",\"lastupdPsn\":\"dldsbat\",\"rev\":0,\"instAdr\":\"\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"iSms\":true,\"oSubnRec\":{\"rid\":1,\"custRid\":7503,\"cusNum\":\"10004631\",\"acctNum\":\"73001278660919\",\"srvId\":\"100121382\",\"srvNum\":\"98238921\",\"bsn\":\" \",\"fsa\":\" \",\"lob\":\"XMOB\",\"wipCust\":\"N\",\"sipSubr\":\"N\",\"netLoginId\":\" \",\"aloneDialup\":\" \",\"eyeGrp\":\" \",\"ind2l2b\":\" \",\"refFsa2l2b\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"indEye\":\" \",\"domainTy\":\" \",\"tos\":\"3G\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"MOB\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127162542\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20151127162542\",\"lastupdPsn\":\"dldsbat\",\"rev\":0,\"instAdr\":\"\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"iApi\":\"\",\"iGwtGud\":\"\",\"iMyFrm\":\"\",\"oHgg\":\"\",\"oNxUrl\":\"\",\"oDuct\":null,\"reply\":{\"code\":\"RC_AO_INDIV_H\"},\"serverTS\":\"20151229153146\"}";
        //	}
        //		if (AO_ADDON.equalsIgnoreCase(apiRequest.getActionTy())) {

        //ADD  success
        //			return "{\"iLoginId\":\"\",\"iSubnRec\":{\"rid\":1,\"custRid\":7503,\"cusNum\":\"10004631\",\"acctNum\":\"73001278660919\",\"srvId\":\"100121382\",\"srvNum\":\"98238921\",\"bsn\":\" \",\"fsa\":\" \",\"lob\":\"MOB\",\"wipCust\":\"N\",\"sipSubr\":\"N\",\"netLoginId\":\" \",\"aloneDialup\":\" \",\"eyeGrp\":\" \",\"ind2l2b\":\" \",\"refFsa2l2b\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"indEye\":\" \",\"domainTy\":\" \",\"tos\":\"3G\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"MOB\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127162542\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20151127162542\",\"lastupdPsn\":\"dldsbat\",\"rev\":0,\"instAdr\":\"\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"iSms\":true,\"oSubnRec\":{\"rid\":1,\"custRid\":7503,\"cusNum\":\"10004631\",\"acctNum\":\"73001278660919\",\"srvId\":\"100121382\",\"srvNum\":\"98238921\",\"bsn\":\" \",\"fsa\":\" \",\"lob\":\"XMOB\",\"wipCust\":\"N\",\"sipSubr\":\"N\",\"netLoginId\":\" \",\"aloneDialup\":\" \",\"eyeGrp\":\" \",\"ind2l2b\":\" \",\"refFsa2l2b\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"indEye\":\" \",\"domainTy\":\" \",\"tos\":\"3G\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"MOB\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127162542\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20151127162542\",\"lastupdPsn\":\"dldsbat\",\"rev\":0,\"instAdr\":\"\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"iApi\":\"\",\"iGwtGud\":\"\",\"iMyFrm\":\"\",\"oHgg\":\"\",\"oNxUrl\":\"\",\"oDuct\":null,\"reply\":{\"code\":\"RC_SUCC\"},\"serverTS\":\"20151229153146\"}";
        //ADD   H INDIV
        //		return "{\"iLoginId\":\"\",\"iSubnRec\":{\"rid\":1,\"custRid\":7503,\"cusNum\":\"10004631\",\"acctNum\":\"73001278660919\",\"srvId\":\"100121382\",\"srvNum\":\"98238921\",\"bsn\":\" \",\"fsa\":\" \",\"lob\":\"MOB\",\"wipCust\":\"N\",\"sipSubr\":\"N\",\"netLoginId\":\" \",\"aloneDialup\":\" \",\"eyeGrp\":\" \",\"ind2l2b\":\" \",\"refFsa2l2b\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"indEye\":\" \",\"domainTy\":\" \",\"tos\":\"3G\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"MOB\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127162542\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20151127162542\",\"lastupdPsn\":\"dldsbat\",\"rev\":0,\"instAdr\":\"\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"iSms\":true,\"oSubnRec\":{\"rid\":1,\"custRid\":7503,\"cusNum\":\"10004631\",\"acctNum\":\"73001278660919\",\"srvId\":\"100121382\",\"srvNum\":\"98238921\",\"bsn\":\" \",\"fsa\":\" \",\"lob\":\"XMOB\",\"wipCust\":\"N\",\"sipSubr\":\"N\",\"netLoginId\":\" \",\"aloneDialup\":\" \",\"eyeGrp\":\" \",\"ind2l2b\":\" \",\"refFsa2l2b\":\" \",\"indPcd\":\" \",\"indTv\":\" \",\"indEye\":\" \",\"domainTy\":\" \",\"tos\":\"3G\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"MOB\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127162542\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20151127162542\",\"lastupdPsn\":\"dldsbat\",\"rev\":0,\"instAdr\":\"\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"iApi\":\"\",\"iGwtGud\":\"\",\"iMyFrm\":\"\",\"oHgg\":\"\",\"oNxUrl\":\"\",\"oDuct\":null,\"reply\":{\"code\":\"RC_AO_INDIV_H\"},\"serverTS\":\"20151229153146\"}";
        //		}
        //		if (LGI.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return "{   \"iCustRec\": {     \"rid\": 0,     \"docTy\": \"\",     \"docNum\": \"\",     \"premier\": \"\",     \"phylum\": \"\",     \"status\": \"\",     \"createTs\": \"\",     \"createPsn\": \"\",     \"lastupdTs\": \"\",     \"lastupdPsn\": \"\",     \"rev\": 0   },   \"iSveeRec\": {     \"rid\": 0,     \"loginId\": \"\",     \"pwd\": \"\",     \"nickname\": \"\",     \"ctMail\": \"\",     \"ctMob\": \"\",     \"mobAlrt\": \"\",     \"lang\": \"\",     \"secQus\": 0,     \"secAns\": \"\",     \"custRid\": 0,     \"status\": \"\",     \"state\": \"\",     \"stateUpTs\": \"\",     \"regnActn\": \"\",     \"promoOpt\": \"\",     \"pwdEff\": \"\",     \"salesChnl\": \"\",     \"teamCd\": \"\",     \"staffId\": \"\",     \"createTs\": \"\",     \"createPsn\": \"\",     \"lastupdTs\": \"\",     \"lastupdPsn\": \"\",     \"rev\": 0   },   \"iSpssRec\": {     \"rid\": 0,     \"dervRid\": 0,     \"sveeRid\": 0,     \"tcId\": \"\",     \"tcSess\": \"\",     \"devId\": \"\",     \"devTy\": \"\",     \"gni\": \"\",     \"bni\": \"\",     \"lang\": \"\",     \"lastupdTs\": \"\",     \"lastupdPsn\": \"\",     \"rev\": 0   },   \"iNaSubnRec\": {     \"rid\": 0,     \"custRid\": 0,     \"cusNum\": \"\",     \"acctNum\": \"\",     \"srvId\": \"\",     \"srvNum\": \"\",     \"bsn\": \"\",     \"fsa\": \"\",     \"lob\": \"\",     \"wipCust\": \"\",     \"sipSubr\": \"\",     \"netLoginId\": \"\",     \"aloneDialup\": \"\",     \"eyeGrp\": \"\",     \"ind2l2b\": \"\",     \"refFsa2l2b\": \"\",     \"indPcd\": \"\",     \"indTv\": \"\",     \"indEye\": \"\",     \"domainTy\": \"\",     \"tos\": \"\",     \"datCd\": \"\",     \"tariff\": \"\",     \"priMob\": \"\",     \"systy\": \"\",     \"alias\": \"\",     \"assoc\": \"\",     \"createTs\": \"\",     \"createPsn\": \"\",     \"lastupdTs\": \"\",     \"lastupdPsn\": \"\",     \"rev\": 0,     \"instAdr\": \"\",     \"storeTy\": \"\",     \"ivr_pwd\": null,     \"auth_ph\": null,     \"acct_ty\": null   },   \"iNaCra\": {     \"iApi\": \"\",     \"iGwtGud\": \"\",     \"iMyFrm\": \"\",     \"oHgg\": \"\",     \"oNxUrl\": \"\",     \"oDuct\": null,     \"reply\": {       \"code\": \"RC_SUCC\",       \"cargo\": null     },     \"serverTS\": \"\"   },   \"iOrigPwd\": \"\",   \"iRecallLgiId\": false,   \"iRecallPwd\": false,   \"oQualSvee\": {     \"custRec\": {       \"rid\": 0,       \"docTy\": \"\",       \"docNum\": \"\",       \"premier\": \"\",       \"phylum\": \"\",       \"status\": \"\",       \"createTs\": \"\",       \"createPsn\": \"\",       \"lastupdTs\": \"\",       \"lastupdPsn\": \"\",       \"rev\": 0     },     \"sveeRec\": {       \"rid\": 30,       \"loginId\": \"ivan20095x@gmail.com\",       \"pwd\": \"\",       \"nickname\": \"ivan\",       \"ctMail\": \"stephanie.kh.cheung@pccw.com\",       \"ctMob\": \"53359899\",       \"mobAlrt\": \"Y\",       \"lang\": \"zh\",       \"secQus\": 0,       \"secAns\": \"\",       \"custRid\": 1302,       \"status\": \"A\",       \"state\": \"A\",       \"stateUpTs\": \"20151215150533\",       \"regnActn\": \"REGUSER\",       \"promoOpt\": \"N\",       \"pwdEff\": \"N\",       \"salesChnl\": \" \",       \"teamCd\": \" \",       \"staffId\": \" \",       \"createTs\": \"20151215150308\",       \"createPsn\": \"!UNKN\",       \"lastupdTs\": \"20160321160530\",       \"lastupdPsn\": \"!UNKN\",       \"rev\": 57     },     \"bcifRec\": {       \"custRid\": 0,       \"custNm\": \"\",       \"industry\": \"\",       \"nuStaff\": \"\",       \"nuPresence\": \"\",       \"xborder\": \"\",       \"priCtName\": \"\",       \"priCtTitle\": \"\",       \"priCtMail\": \"\",       \"priCtJobtitle\": \"\",       \"priCtTel\": \"\",       \"priCtMob\": \"\",       \"secCtName\": \"\",       \"secCtTitle\": \"\",       \"secCtMail\": \"\",       \"secCtJobtitle\": \"\",       \"secCtTel\": \"\",       \"secCtMob\": \"\",       \"createTs\": \"\",       \"createPsn\": \"\",       \"lastupdTs\": \"\",       \"lastupdPsn\": \"\",       \"rev\": 0     },     \"subnRecAry\": [],     \"zmSubnRecAry\": []   },   \"oAcctAry\": [],   \"oBomCustAry\": [],   \"oSrvReqAry\": [],   \"oGnrlApptAry\": [],   \"oBillListAry\": [],   \"oChatTok\": \"\",   \"oSoGud\": \"a8777acf4011f793ffc8b4521295b688176c60f255b5dc00e9d252271c747378\",   \"oTaSubnRec\": {     \"rid\": 0,     \"custRid\": 0,     \"cusNum\": \"\",     \"acctNum\": \"\",     \"srvId\": \"\",     \"srvNum\": \"\",     \"bsn\": \"\",     \"fsa\": \"\",     \"lob\": \"\",     \"wipCust\": \"\",     \"sipSubr\": \"\",     \"netLoginId\": \"\",     \"aloneDialup\": \"\",     \"eyeGrp\": \"\",     \"ind2l2b\": \"\",     \"refFsa2l2b\": \"\",     \"indPcd\": \"\",     \"indTv\": \"\",     \"indEye\": \"\",     \"domainTy\": \"\",     \"tos\": \"\",     \"datCd\": \"\",     \"tariff\": \"\",     \"priMob\": \"\",     \"systy\": \"\",     \"alias\": \"\",     \"assoc\": \"\",     \"createTs\": \"\",     \"createPsn\": \"\",     \"lastupdTs\": \"\",     \"lastupdPsn\": \"\",     \"rev\": 0,     \"instAdr\": \"\",     \"storeTy\": \"\",     \"ivr_pwd\": null,     \"auth_ph\": null,     \"acct_ty\": null   },   \"oTaCra\": {     \"iApi\": \"\",     \"iGwtGud\": \"\",     \"iMyFrm\": \"\",     \"oHgg\": \"\",     \"oNxUrl\": \"\",     \"oDuct\": null,     \"reply\": {       \"code\": \"RC_SUCC\",       \"cargo\": null     },     \"serverTS\": \"\"   },   \"iApi\": \"\",   \"iGwtGud\": \"\",   \"iMyFrm\": \"\",   \"oHgg\": \"\",   \"oNxUrl\": \"\",   \"oDuct\": null,   \"reply\": {     \"code\": \"RC_RESET_SVEE\",     \"cargo\": null   },   \"serverTS\": \"20160329103313\" }";
        //		}
        //		if (APPT.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return "{   \"iLoginId\": \"keith.kk.mak@pccw.com\",   \"iCustRec\": {     \"rid\": 7503,     \"docTy\": \"PASS\",     \"docNum\": \"88888891\",     \"premier\": \" \",     \"phylum\": \"CSUM\",     \"status\": \"A\",     \"createTs\": \"20151127164938\",     \"createPsn\": \"dldsbat\",     \"lastupdTs\": \"20151127164938\",     \"lastupdPsn\": \"dldsbat\",     \"rev\": 0   },   \"iSubnRec\": {     \"rid\": 0,     \"custRid\": 0,     \"cusNum\": \"\",     \"acctNum\": \"\",     \"srvId\": \"\",     \"srvNum\": \"\",     \"bsn\": \"\",     \"fsa\": \"\",     \"lob\": \"\",     \"wipCust\": \"\",     \"sipSubr\": \"\",     \"netLoginId\": \"\",     \"aloneDialup\": \"\",     \"eyeGrp\": \"\",     \"ind2l2b\": \"\",     \"refFsa2l2b\": \"\",     \"indPcd\": \"\",     \"indTv\": \"\",     \"indEye\": \"\",     \"domainTy\": \"\",     \"tos\": \"\",     \"datCd\": \"\",     \"tariff\": \"\",     \"priMob\": \"\",     \"systy\": \"\",     \"alias\": \"\",     \"assoc\": \"\",     \"createTs\": \"\",     \"createPsn\": \"\",     \"lastupdTs\": \"\",     \"lastupdPsn\": \"\",     \"rev\": 0,     \"instAdr\": \"\",     \"storeTy\": \"\",     \"ivr_pwd\": null,     \"auth_ph\": null,     \"acct_ty\": null   },   \"iSubnRecAry\": [     {       \"rid\": 1,       \"custRid\": 7503,       \"cusNum\": \"10004631\",       \"acctNum\": \"73001278660919\",       \"srvId\": \"100121382\",       \"srvNum\": \"98238921\",       \"bsn\": \" \",       \"fsa\": \" \",       \"lob\": \"MOB\",       \"wipCust\": \"N\",       \"sipSubr\": \"N\",       \"netLoginId\": \" \",       \"aloneDialup\": \" \",       \"eyeGrp\": \" \",       \"ind2l2b\": \" \",       \"refFsa2l2b\": \" \",       \"indPcd\": \" \",       \"indTv\": \" \",       \"indEye\": \" \",       \"domainTy\": \" \",       \"tos\": \"3G\",       \"datCd\": \" \",       \"tariff\": \" \",       \"priMob\": \" \",       \"systy\": \"MOB\",       \"alias\": \" \",       \"assoc\": \"Y\",       \"createTs\": \"20151127162542\",       \"createPsn\": \"dldsbat\",       \"lastupdTs\": \"20160128180226\",       \"lastupdPsn\": \"keith.kk.mak@pccw.com\",       \"rev\": 37,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     },     {       \"rid\": 99987705,       \"custRid\": 7503,       \"cusNum\": \" \",       \"acctNum\": \"09757477\",       \"srvId\": \"88000004\",       \"srvNum\": \"88000004\",       \"bsn\": \" \",       \"fsa\": \" \",       \"lob\": \"O2F\",       \"wipCust\": \"N\",       \"sipSubr\": \"N\",       \"netLoginId\": \" \",       \"aloneDialup\": \" \",       \"eyeGrp\": \" \",       \"ind2l2b\": \" \",       \"refFsa2l2b\": \" \",       \"indPcd\": \" \",       \"indTv\": \" \",       \"indEye\": \" \",       \"domainTy\": \" \",       \"tos\": \" \",       \"datCd\": \" \",       \"tariff\": \" \",       \"priMob\": \" \",       \"systy\": \"CSL\",       \"alias\": \" \",       \"assoc\": \"Y\",       \"createTs\": \"20160201113243\",       \"createPsn\": \"KM\",       \"lastupdTs\": \"20160201113243\",       \"lastupdPsn\": \"KM\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     },     {       \"rid\": 99887705,       \"custRid\": 7503,       \"cusNum\": \" \",       \"acctNum\": \"90657073\",       \"srvId\": \"60530731\",       \"srvNum\": \"60530731\",       \"bsn\": \" \",       \"fsa\": \" \",       \"lob\": \"O2F\",       \"wipCust\": \"N\",       \"sipSubr\": \"N\",       \"netLoginId\": \" \",       \"aloneDialup\": \" \",       \"eyeGrp\": \" \",       \"ind2l2b\": \" \",       \"refFsa2l2b\": \" \",       \"indPcd\": \" \",       \"indTv\": \" \",       \"indEye\": \" \",       \"domainTy\": \" \",       \"tos\": \" \",       \"datCd\": \" \",       \"tariff\": \" \",       \"priMob\": \" \",       \"systy\": \"CSL\",       \"alias\": \" \",       \"assoc\": \"Y\",       \"createTs\": \"20160201113243\",       \"createPsn\": \"KM\",       \"lastupdTs\": \"20160201113243\",       \"lastupdPsn\": \"KM\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     },     {       \"rid\": 34258,       \"custRid\": 7503,       \"cusNum\": \"5100345\",       \"acctNum\": \"1068249749\",       \"srvId\": \"60058422\",       \"srvNum\": \"a60058422\",       \"bsn\": \"20228391\",       \"fsa\": \"60058422\",       \"lob\": \"PCD\",       \"wipCust\": \"N\",       \"sipSubr\": \" \",       \"netLoginId\": \"a60058422\",       \"aloneDialup\": \"N\",       \"eyeGrp\": \" \",       \"ind2l2b\": \"N\",       \"refFsa2l2b\": \" \",       \"indPcd\": \"Y\",       \"indTv\": \"N\",       \"indEye\": \"N\",       \"domainTy\": \"N\",       \"tos\": \"IMS\",       \"datCd\": \" \",       \"tariff\": \" \",       \"priMob\": \" \",       \"systy\": \"IMS\",       \"alias\": \" \",       \"assoc\": \"Y\",       \"createTs\": \"20151127164833\",       \"createPsn\": \"dldsbat\",       \"lastupdTs\": \"20151127164833\",       \"lastupdPsn\": \"dldsbat\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     }   ],   \"iSrvReq\": {     \"srvNum\": \"\",     \"srvNumTy\": \"\",     \"reportNum\": \"\",     \"trunkNum\": \"\",     \"extNum\": \"\",     \"refNum\": \"\",     \"outProd\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"ctNmTtl\": \"\",     \"ctNm\": \"\",     \"ctNum\": \"\",     \"allowUpdInd\": \"\",     \"autoSRInd\": \"\",     \"enSpkr\": \"\",     \"smsLang\": \"\",     \"srInfo\": {       \"custAdr\": \"\",       \"flatCd\": \"\",       \"floorCd\": \"\",       \"srvBndy\": \"\",       \"ctNum\": \"\",       \"ctNm\": \"\",       \"ponLine\": \"\",       \"ponCvg\": \"\",       \"mdmRst\": \"\",       \"bsn\": \"\",       \"fsa\": \"\"     },     \"apptInfo\": {       \"refNum\": \"\",       \"prodTy\": \"\",       \"srvTy\": \"\",       \"areaCd\": \"\",       \"distCd\": \"\",       \"srvNum\": \"\",       \"prodId\": \"\",       \"apptTS\": {         \"apptDate\": \"\",         \"apptTmslot\": \"\",         \"apptDT\": \"\"       },       \"apptTSAry\": []     },     \"updTy\": \"\",     \"assocSubnRec\": {       \"rid\": 0,       \"custRid\": 0,       \"cusNum\": \"\",       \"acctNum\": \"\",       \"srvId\": \"\",       \"srvNum\": \"\",       \"bsn\": \"\",       \"fsa\": \"\",       \"lob\": \"\",       \"wipCust\": \"\",       \"sipSubr\": \"\",       \"netLoginId\": \"\",       \"aloneDialup\": \"\",       \"eyeGrp\": \"\",       \"ind2l2b\": \"\",       \"refFsa2l2b\": \"\",       \"indPcd\": \"\",       \"indTv\": \"\",       \"indEye\": \"\",       \"domainTy\": \"\",       \"tos\": \"\",       \"datCd\": \"\",       \"tariff\": \"\",       \"priMob\": \"\",       \"systy\": \"\",       \"alias\": \"\",       \"assoc\": \"\",       \"createTs\": \"\",       \"createPsn\": \"\",       \"lastupdTs\": \"\",       \"lastupdPsn\": \"\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     }   },   \"iEnqSRTy\": \"\",   \"iSRApptInfo\": {     \"refNum\": \"\",     \"prodTy\": \"\",     \"srvTy\": \"\",     \"areaCd\": \"\",     \"distCd\": \"\",     \"srvNum\": \"\",     \"prodId\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"apptTSAry\": []   },   \"oGnrlApptAry\": [],   \"oBomApptAry\": [],   \"oSrvReqAry\": [     {       \"srvNum\": \"60058422\",       \"srvNumTy\": \"FSA\",       \"reportNum\": \"8567\",       \"trunkNum\": \"\",       \"extNum\": \"\",       \"refNum\": \"\",       \"outProd\": \"PCD\",       \"apptTS\": {         \"apptDate\": \"20160605\",         \"apptTmslot\": \"18\",         \"apptDT\": \"07-Mar 18:00-20:00\"       },       \"ctNmTtl\": \"\",       \"ctNm\": \"MISS CHAN LING LING\",       \"ctNum\": \"63520372\",       \"allowUpdInd\": \"Y\",       \"autoSRInd\": \"N\",       \"enSpkr\": \"\",       \"smsLang\": \"\",       \"srInfo\": {         \"custAdr\": \"FT 15 12/F YCK AM01 BLDG, SHA TIN NEW TERRITORIES\",         \"flatCd\": \"\",         \"floorCd\": \"\",         \"srvBndy\": \"\",         \"ctNum\": \"\",         \"ctNm\": \"\",         \"ponLine\": \"\",         \"ponCvg\": \"N\",         \"mdmRst\": \"\",         \"bsn\": \"\",         \"fsa\": \"\"       },       \"apptInfo\": {         \"refNum\": \"\",         \"prodTy\": \"I\",         \"srvTy\": \"SR\",         \"areaCd\": \"NT\",         \"distCd\": \"222\",         \"srvNum\": \"20228391\",         \"prodId\": \"NU1R\",         \"apptTS\": {           \"apptDate\": \"\",           \"apptTmslot\": \"\",           \"apptDT\": \"\"         },         \"apptTSAry\": []       },       \"updTy\": \"\",       \"assocSubnRec\": {         \"rid\": 34258,         \"custRid\": 7503,         \"cusNum\": \"5100345\",         \"acctNum\": \"1068249749\",         \"srvId\": \"60058422\",         \"srvNum\": \"a60058422\",         \"bsn\": \"20228391\",         \"fsa\": \"60058422\",         \"lob\": \"PCD\",         \"wipCust\": \"N\",         \"sipSubr\": \" \",         \"netLoginId\": \"a60058422\",         \"aloneDialup\": \"N\",         \"eyeGrp\": \" \",         \"ind2l2b\": \"N\",         \"refFsa2l2b\": \" \",         \"indPcd\": \"Y\",         \"indTv\": \"N\",         \"indEye\": \"N\",         \"domainTy\": \"N\",         \"tos\": \"IMS\",         \"datCd\": \" \",         \"tariff\": \" \",         \"priMob\": \" \",         \"systy\": \"IMS\",         \"alias\": \" \",         \"assoc\": \"Y\",         \"createTs\": \"20151127164833\",         \"createPsn\": \"dldsbat\",         \"lastupdTs\": \"20151127164833\",         \"lastupdPsn\": \"dldsbat\",         \"rev\": 0,         \"instAdr\": \"\",         \"storeTy\": \"\",         \"ivr_pwd\": null,         \"auth_ph\": null,         \"acct_ty\": null       }     }   ],   \"oSRApptInfo\": {     \"refNum\": \"\",     \"prodTy\": \"\",     \"srvTy\": \"\",     \"areaCd\": \"\",     \"distCd\": \"\",     \"srvNum\": \"\",     \"prodId\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"apptTSAry\": []   },   \"oPendSrvReq\": {     \"srvNum\": \"\",     \"srvNumTy\": \"\",     \"reportNum\": \"\",     \"trunkNum\": \"\",     \"extNum\": \"\",     \"refNum\": \"\",     \"outProd\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"ctNmTtl\": \"\",     \"ctNm\": \"\",     \"ctNum\": \"\",     \"allowUpdInd\": \"\",     \"autoSRInd\": \"\",     \"enSpkr\": \"\",     \"smsLang\": \"\",     \"srInfo\": {       \"custAdr\": \"\",       \"flatCd\": \"\",       \"floorCd\": \"\",       \"srvBndy\": \"\",       \"ctNum\": \"\",       \"ctNm\": \"\",       \"ponLine\": \"\",       \"ponCvg\": \"\",       \"mdmRst\": \"\",       \"bsn\": \"\",       \"fsa\": \"\"     },     \"apptInfo\": {       \"refNum\": \"\",       \"prodTy\": \"\",       \"srvTy\": \"\",       \"areaCd\": \"\",       \"distCd\": \"\",       \"srvNum\": \"\",       \"prodId\": \"\",       \"apptTS\": {         \"apptDate\": \"\",         \"apptTmslot\": \"\",         \"apptDT\": \"\"       },       \"apptTSAry\": []     },     \"updTy\": \"\",     \"assocSubnRec\": {       \"rid\": 0,       \"custRid\": 0,       \"cusNum\": \"\",       \"acctNum\": \"\",       \"srvId\": \"\",       \"srvNum\": \"\",       \"bsn\": \"\",       \"fsa\": \"\",       \"lob\": \"\",       \"wipCust\": \"\",       \"sipSubr\": \"\",       \"netLoginId\": \"\",       \"aloneDialup\": \"\",       \"eyeGrp\": \"\",       \"ind2l2b\": \"\",       \"refFsa2l2b\": \"\",       \"indPcd\": \"\",       \"indTv\": \"\",       \"indEye\": \"\",       \"domainTy\": \"\",       \"tos\": \"\",       \"datCd\": \"\",       \"tariff\": \"\",       \"priMob\": \"\",       \"systy\": \"\",       \"alias\": \"\",       \"assoc\": \"\",       \"createTs\": \"\",       \"createPsn\": \"\",       \"lastupdTs\": \"\",       \"lastupdPsn\": \"\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     }   },   \"iApi\": \"APPT\",   \"iGwtGud\": \"722EB6B5E8694474\",   \"iMyFrm\": \"\",   \"oHgg\": \"\",   \"oNxUrl\": \"\",   \"oDuct\": null,   \"reply\": {     \"code\": \"RC_SUCC\",     \"cargo\": null   },   \"serverTS\": \"20160601190000\" }";
        //		}
        //		if (APPT.equalsIgnoreCase(apiRequest.getActionTy())) {
        //	return		"{     \"serverTS\": \"20160520115359\",     \"iSubnRec\": {         \"rev\": 0,         \"createTs\": \"\",         \"netLoginId\": \"\",         \"custRid\": 0,         \"systy\": \"\",         \"instAdr\": \"\",         \"eyeGrp\": \"\",         \"sipSubr\": \"\",         \"wipCust\": \"\",         \"storeTy\": \"\",         \"lastupdTs\": \"\",         \"datCd\": \"\",         \"indTv\": \"\",         \"lastupdPsn\": \"\",         \"lob\": \"\",         \"tos\": \"\",         \"indPcd\": \"\",         \"auth_ph\": null,         \"tariff\": \"\",         \"indEye\": \"\",         \"createPsn\": \"\",         \"cusNum\": \"\",         \"assoc\": \"\",         \"fsa\": \"\",         \"acct_ty\": null,         \"alias\": \"\",         \"acctNum\": \"\",         \"ind2l2b\": \"\",         \"domainTy\": \"\",         \"srvNum\": \"\",         \"bsn\": \"\",         \"rid\": 0,         \"refFsa2l2b\": \"\",         \"srvId\": \"\",         \"priMob\": \"\",         \"aloneDialup\": \"\",         \"ivr_pwd\": null     },     \"iGwtGud\": \"59B15762F5934BD2\",     \"oDuct\": null,     \"oSrvReqAry\": [         {             \"assocSubnRec\": {                 \"rev\": 4,                 \"createTs\": \"20151127164826\",                 \"netLoginId\": \"asr4\",                 \"custRid\": 4539,                 \"systy\": \"IMS\",                 \"instAdr\": \"MF WTS EXCH R ADSL BLDG, FUNG TAK STREET, WONG TAI SIN KOWLOON\",                 \"eyeGrp\": \" \",                 \"sipSubr\": \" \",                 \"wipCust\": \"N\",                 \"storeTy\": \"\",                 \"lastupdTs\": \"20160516104039\",                 \"datCd\": \" \",                 \"indTv\": \"Y\",                 \"lastupdPsn\": \"uatsr1@gmail.com\",                 \"lob\": \"PCD\",                 \"tos\": \"IMS\",                 \"indPcd\": \"Y\",                 \"auth_ph\": null,                 \"tariff\": \" \",                 \"indEye\": \"N\",                 \"createPsn\": \"dldsbat\",                 \"cusNum\": \"6700423\",                 \"assoc\": \"Y\",                 \"fsa\": \"60078659\",                 \"acct_ty\": null,                 \"alias\": \"fran\",                 \"acctNum\": \"1200194526\",                 \"ind2l2b\": \"N\",                 \"domainTy\": \"N\",                 \"srvNum\": \"asr4\",                 \"bsn\": \"80895196\",                 \"rid\": 33157,                 \"refFsa2l2b\": \" \",                 \"srvId\": \"60078659\",                 \"priMob\": \" \",                 \"aloneDialup\": \"N\",                 \"ivr_pwd\": null             },             \"enSpkr\": \"\",             \"autoSRInd\": \"Y\",             \"updTy\": \"\",             \"srInfo\": {                 \"flatCd\": \"\",                 \"floorCd\": \"\",                 \"fsa\": \"\",                 \"bsn\": \"\",                 \"mdmRst\": \"\",                 \"custAdr\": \"MF WTS EXCH RADSL BLDG, FUNG TAK STREET, WONG TAI SIN KOWLOON\",                 \"ponCvg\": \"Y\",                 \"ctNum\": \"\",                 \"srvBndy\": \"\",                 \"ponLine\": \"\",                 \"ctNm\": \"\"             },             \"ctNmTtl\": \"\",             \"ctNm\": \"29FEB\",             \"srvNum\": \"60078659\",             \"apptTS\": {                 \"apptDT\": \"20-May 10:00-13:00\",                 \"apptTmslot\": \"AM\",                 \"apptDate\": \"20160520\"             },             \"extNum\": \"\",             \"smsLang\": \"\",             \"trunkNum\": \"\",             \"srvNumTy\": \"FSA\",             \"ctNum\": \"28834567\",             \"refNum\": \"\",             \"outProd\": \"PCD\",             \"reportNum\": \"8564\",             \"allowUpdInd\": \"N\",             \"apptInfo\": {                 \"apptTS\": {                     \"apptDT\": \"\",                     \"apptTmslot\": \"\",                     \"apptDate\": \"\"                 },                 \"apptTSAry\": [                                     ],                 \"prodId\": \"PN08\",                 \"refNum\": \"\",                 \"distCd\": \"21\",                 \"areaCd\": \"KN\",                 \"prodTy\": \"I\",                 \"srvTy\": \"SR\",                 \"srvNum\": \"80895196\"             }         }     ],     \"iLoginId\": \"uatsr1@gmail.com\",     \"iSRApptInfo\": {         \"apptTS\": {             \"apptDT\": \"\",             \"apptTmslot\": \"\",             \"apptDate\": \"\"         },         \"apptTSAry\": [                     ],         \"prodId\": \"\",         \"refNum\": \"\",         \"distCd\": \"\",         \"areaCd\": \"\",         \"prodTy\": \"\",         \"srvTy\": \"\",         \"srvNum\": \"\"     },     \"oGnrlApptAry\": [             ],     \"oPendSrvReq\": {         \"assocSubnRec\": {             \"rev\": 0,             \"createTs\": \"\",             \"netLoginId\": \"\",             \"custRid\": 0,             \"systy\": \"\",             \"instAdr\": \"\",             \"eyeGrp\": \"\",             \"sipSubr\": \"\",             \"wipCust\": \"\",             \"storeTy\": \"\",             \"lastupdTs\": \"\",             \"datCd\": \"\",             \"indTv\": \"\",             \"lastupdPsn\": \"\",             \"lob\": \"\",             \"tos\": \"\",             \"indPcd\": \"\",             \"auth_ph\": null,             \"tariff\": \"\",             \"indEye\": \"\",             \"createPsn\": \"\",             \"cusNum\": \"\",             \"assoc\": \"\",             \"fsa\": \"\",             \"acct_ty\": null,             \"alias\": \"\",             \"acctNum\": \"\",             \"ind2l2b\": \"\",             \"domainTy\": \"\",             \"srvNum\": \"\",             \"bsn\": \"\",             \"rid\": 0,             \"refFsa2l2b\": \"\",             \"srvId\": \"\",             \"priMob\": \"\",             \"aloneDialup\": \"\",             \"ivr_pwd\": null         },         \"enSpkr\": \"\",         \"autoSRInd\": \"\",         \"updTy\": \"\",         \"srInfo\": {             \"flatCd\": \"\",             \"floorCd\": \"\",             \"fsa\": \"\",             \"bsn\": \"\",             \"mdmRst\": \"\",             \"custAdr\": \"\",             \"ponCvg\": \"\",             \"ctNum\": \"\",             \"srvBndy\": \"\",             \"ponLine\": \"\",             \"ctNm\": \"\"         },         \"ctNmTtl\": \"\",         \"ctNm\": \"\",         \"srvNum\": \"\",         \"apptTS\": {             \"apptDT\": \"\",             \"apptTmslot\": \"\",             \"apptDate\": \"\"         },         \"extNum\": \"\",         \"smsLang\": \"\",         \"trunkNum\": \"\",         \"srvNumTy\": \"\",         \"ctNum\": \"\",         \"refNum\": \"\",         \"outProd\": \"\",         \"reportNum\": \"\",         \"allowUpdInd\": \"\",         \"apptInfo\": {             \"apptTS\": {                 \"apptDT\": \"\",                 \"apptTmslot\": \"\",                 \"apptDate\": \"\"             },             \"apptTSAry\": [                             ],             \"prodId\": \"\",             \"refNum\": \"\",             \"distCd\": \"\",             \"areaCd\": \"\",             \"prodTy\": \"\",             \"srvTy\": \"\",             \"srvNum\": \"\"         }     },     \"oNxUrl\": \"\",     \"iMyFrm\": \"\",     \"iCustRec\": {         \"docNum\": \"E885244(1)\",         \"phylum\": \"CSUM\",         \"rev\": 4539,         \"status\": \"A\",         \"rid\": 4539,         \"createTs\": \"20151127164826\",         \"premier\": \" \",         \"lastupdTs\": \"20151127164826\",         \"docTy\": \"HKID\",         \"createPsn\": \"dldsbat\",         \"lastupdPsn\": \"dldsbat\"     },     \"iSrvReq\": {         \"assocSubnRec\": {             \"rev\": 0,             \"createTs\": \"\",             \"netLoginId\": \"\",             \"custRid\": 0,             \"systy\": \"\",             \"instAdr\": \"\",             \"eyeGrp\": \"\",             \"sipSubr\": \"\",             \"wipCust\": \"\",             \"storeTy\": \"\",             \"lastupdTs\": \"\",             \"datCd\": \"\",             \"indTv\": \"\",             \"lastupdPsn\": \"\",             \"lob\": \"\",             \"tos\": \"\",             \"indPcd\": \"\",             \"auth_ph\": null,             \"tariff\": \"\",             \"indEye\": \"\",             \"createPsn\": \"\",             \"cusNum\": \"\",             \"assoc\": \"\",             \"fsa\": \"\",             \"acct_ty\": null,             \"alias\": \"\",             \"acctNum\": \"\",             \"ind2l2b\": \"\",             \"domainTy\": \"\",             \"srvNum\": \"\",             \"bsn\": \"\",             \"rid\": 0,             \"refFsa2l2b\": \"\",             \"srvId\": \"\",             \"priMob\": \"\",             \"aloneDialup\": \"\",             \"ivr_pwd\": null         },         \"enSpkr\": \"\",         \"autoSRInd\": \"\",         \"updTy\": \"\",         \"srInfo\": {             \"flatCd\": \"\",             \"floorCd\": \"\",             \"fsa\": \"\",             \"bsn\": \"\",             \"mdmRst\": \"\",             \"custAdr\": \"\",             \"ponCvg\": \"\",             \"ctNum\": \"\",             \"srvBndy\": \"\",             \"ponLine\": \"\",             \"ctNm\": \"\"         },         \"ctNmTtl\": \"\",         \"ctNm\": \"\",         \"srvNum\": \"\",         \"apptTS\": {             \"apptDT\": \"\",             \"apptTmslot\": \"\",             \"apptDate\": \"\"         },         \"extNum\": \"\",         \"smsLang\": \"\",         \"trunkNum\": \"\",         \"srvNumTy\": \"\",         \"ctNum\": \"\",         \"refNum\": \"\",         \"outProd\": \"\",         \"reportNum\": \"\",         \"allowUpdInd\": \"\",         \"apptInfo\": {             \"apptTS\": {                 \"apptDT\": \"\",                 \"apptTmslot\": \"\",                 \"apptDate\": \"\"             },             \"apptTSAry\": [                             ],             \"prodId\": \"\",             \"refNum\": \"\",             \"distCd\": \"\",             \"areaCd\": \"\",             \"prodTy\": \"\",             \"srvTy\": \"\",             \"srvNum\": \"\"         }     },     \"oHgg\": \"\",     \"oBomApptAry\": [         {             \"clone\": false,             \"loginId\": \"\",             \"ocId\": \"21674889\",             \"acctNum\": \"\",             \"drgOrdNum\": \"100070485001\",             \"grpTy\": \"\",             \"srvTy\": \"TEL\",             \"apptStDT\": \"20160604090000\",             \"datCd\": \"DEL\",             \"apptEnDT\": \"20160131130000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"FT 23 23F, SUNRISE CT, 4244 TUNG LO WAN ROAD, CAUSEWAY BAY, HONG KONG\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"000023322134\",             \"srvBndy\": \"228936\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"curlyclub41\",             \"ocId\": \"21674373\",             \"acctNum\": \"1200194526\",             \"drgOrdNum\": \"\",             \"grpTy\": \"\",             \"srvTy\": \"IMS\",             \"apptStDT\": \"20160131090000\",             \"datCd\": \"IMS\",             \"apptEnDT\": \"20160131130000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"123F, SUNRISE CT, 4244 TUNG LO WAN ROAD, CAUSEWAY BAY, HONG KONG\",             \"peInd\": \"N\",             \"Adr\": \"22F, SUNRISE CT, 42-44 TUNG LO WAN ROAD, CAUSEWAY BAY, HONG KONG\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"60101228\",             \"srvBndy\": \"228936\",             \"grpNum\": \"\"         },         {             \"clone\": false,             \"loginId\": \"pcd60101242\",             \"ocId\": \"21674889\",             \"acctNum\": \"1200487666\",             \"drgOrdNum\": \"\",             \"grpTy\": \"\",             \"srvTy\": \"IMS\",             \"apptStDT\": \"20160131090000\",             \"datCd\": \"IMS\",             \"apptEnDT\": \"20160131130000\",             \"tvAcctNum\": \"\",             \"pwApptEnDT\": \"\",             \"oAdr\": \"\",             \"peInd\": \"N\",             \"Adr\": \"FT 23 23F, SUNRISE CT, 42-44 TUNG LO WAN ROAD, CAUSEWAY BAY, HONG KONG\",             \"pwApptStDT\": \"\",             \"ordTy\": \"I\",             \"srvId\": \"60101242\",             \"srvBndy\": \"228936\",             \"grpNum\": \"\"         }     ],     \"iEnqSRTy\": \"\",     \"iSubnRecAry\": [         {             \"rev\": 4,             \"createTs\": \"20151127164826\",             \"netLoginId\": \"asr4\",             \"custRid\": 4539,             \"systy\": \"IMS\",             \"instAdr\": \"MF WTS EXCH R-ADSL BLDG, FUNG TAK STREET, WONG TAI SIN KOWLOON\",             \"eyeGrp\": \" \",             \"sipSubr\": \" \",             \"wipCust\": \"N\",             \"storeTy\": \"\",             \"lastupdTs\": \"20160516104039\",             \"datCd\": \" \",             \"indTv\": \"Y\",             \"lastupdPsn\": \"uatsr1@gmail.com\",             \"lob\": \"PCD\",             \"tos\": \"IMS\",             \"indPcd\": \"Y\",             \"auth_ph\": null,             \"tariff\": \" \",             \"indEye\": \"N\",             \"createPsn\": \"dldsbat\",             \"cusNum\": \"6700423\",             \"assoc\": \"Y\",             \"fsa\": \"60078659\",             \"acct_ty\": null,             \"alias\": \"fran\",             \"acctNum\": \"1200194526\",             \"ind2l2b\": \"N\",             \"domainTy\": \"N\",             \"srvNum\": \"asr4\",             \"bsn\": \"80895196\",             \"rid\": 33157,             \"refFsa2l2b\": \" \",             \"srvId\": \"60078659\",             \"priMob\": \" \",             \"aloneDialup\": \"N\",             \"ivr_pwd\": null         },         {             \"rev\": 2,             \"createTs\": \"20151127164826\",             \"netLoginId\": \"asr4\",             \"custRid\": 4539,             \"systy\": \"IMS\",             \"instAdr\": \"MF WTS EXCH R-ADSL BLDG, FUNG TAK STREET, WONG TAI SIN KOWLOON\",             \"eyeGrp\": \" \",             \"sipSubr\": \" \",             \"wipCust\": \"N\",             \"storeTy\": \"\",             \"lastupdTs\": \"20160513164537\",             \"datCd\": \" \",             \"indTv\": \"Y\",             \"lastupdPsn\": \"uatsr1@gmail.com\",             \"lob\": \"TV\",             \"tos\": \"IMS\",             \"indPcd\": \"Y\",             \"auth_ph\": null,             \"tariff\": \" \",             \"indEye\": \"N\",             \"createPsn\": \"dldsbat\",             \"cusNum\": \"6700423\",             \"assoc\": \"Y\",             \"fsa\": \"60078659\",             \"acct_ty\": null,             \"alias\": \"aliashh\",             \"acctNum\": \"1200194539\",             \"ind2l2b\": \"N\",             \"domainTy\": \"N\",             \"srvNum\": \"1200194539\",             \"bsn\": \"80895196\",             \"rid\": 33158,             \"refFsa2l2b\": \" \",             \"srvId\": \"60078659\",             \"priMob\": \" \",             \"aloneDialup\": \"N\",             \"ivr_pwd\": null         }     ],     \"reply\": {         \"cargo\": null,         \"code\": \"RC_SUCC\"     },     \"iApi\": \"APPT\",     \"oSRApptInfo\": {         \"apptTS\": {             \"apptDT\": \"\",             \"apptTmslot\": \"\",             \"apptDate\": \"\"         },         \"apptTSAry\": [                     ],         \"prodId\": \"\",         \"refNum\": \"\",         \"distCd\": \"\",         \"areaCd\": \"\",         \"prodTy\": \"\",         \"srvTy\": \"\",         \"srvNum\": \"\" }";

        //		if (PLAN_MOB.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/hsim_mobusage.json");
        //		}
        //		//Appointment case (ltstest01@abc.com , 37551223);
        //		if (LNTT.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/lntt_rs_report.json");
        //		}
        //		if (SR_ENQ_NP.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/enq_sr.json");
        //		}
        //		if (SR_AVA_TS.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/sr_ava_ts.json");
        //		}
        //		if (SR_CRT.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/sr_crt.json");
        //		}
        //				if (PLAN_MOB.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/hsim_mobusage.json");
        //					return "{\"serverTS\":\"20160923102025\",\"iSubnRec\":{\"rev\":0,\"createTs\":\"\",\"netLoginId\":\"\",\"custRid\":0,\"systy\":\"MOB\",\"instAdr\":\"\",\"eyeGrp\":\"\",\"sipSubr\":\"\",\"wipCust\":\"\",\"storeTy\":\"AO\",\"lastupdTs\":\"\",\"datCd\":\"\",\"indTv\":\"\",\"lastupdPsn\":\"\",\"lob\":\"XMOB\",\"tos\":\"3G\",\"indPcd\":\"\",\"auth_ph\":\"4baefdd1d26d0f5b729e21e71b3e2803d11d21524c1ccc304fc47f8ac88a69a6\",\"tariff\":\"\",\"indEye\":\"\",\"createPsn\":\"\",\"cusNum\":\"10954444\",\"assoc\":\"\",\"fsa\":\"\",\"acct_ty\":\"C\",\"alias\":\"\",\"acctNum\":\"75100096687984\",\"ind2l2b\":\"\",\"domainTy\":\"\",\"inMipMig\":false,\"srvNum\":\"92741281\",\"bsn\":\"\",\"rid\":0,\"refFsa2l2b\":\"\",\"srvId\":\"\",\"priMob\":\"\",\"aloneDialup\":\"\",\"ivr_pwd\":\"\"},\"iGwtGud\":\"86CEFDF53E85411E\",\"iBstUpHistEnDt\":\"\",\"oDuct\":null,\"iLoginId\":\"\",\"oVbpCustTy\":\"\",\"oNxUrl\":\"\",\"iMyFrm\":\"\",\"oContrEndDt\":\"\",\"oG3BoostUpOffer1DTOAry\":[],\"iBstUpHistStDt\":\"\",\"oHgg\":\"\",\"oMobUsage\":{\"lVdoIter\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"nowTv\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"lVceIter\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"localData\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"lVceItra\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"moov\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"ratePlan\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"Monthly Mobile Service Fee\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"MRP0001207\",\"fee\":\"438.0\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"基本通話月費\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"g3DisplayServiceItemResultDTO\":{\"rtnMsg\":\"\",\"displayServiceItemList\":[{\"region\":\"LOCAL\",\"asOfDateEng\":\"Latest update\",\"usageBar\":{\"colorG\":204,\"colorR\":0,\"showBar\":\"Y\",\"colorB\":0,\"barPercent\":100},\"titleLevel1Eng\":\"Local Mobile Data\",\"otherVAS\":\"Y\",\"titleLevel2Display\":null,\"usageType\":\"QUOTA-BASED\",\"show\":\"Y\",\"titleLevel2Eng\":null,\"serviceType\":\"Local Topup Plan\",\"usageDescription\":{\"consumedInMB\":\"0\",\"textColorG\":0,\"showWarningIcon\":\"N\",\"remainingInMB\":\"5,320\",\"textDisplay\":null,\"htmlChi\":\"尚餘: 100% (5,320MB)\",\"wapEntitleDisplay\":null,\"initialInMB\":\"5,120\",\"textColorR\":0,\"textChi\":\"尚餘: 100% (5,320MB)\",\"htmlEng\":\"100% (5,320MB)Remaining\",\"textColorB\":0,\"textEng\":\"100% (5,320MB) Remaining\"},\"titleLevel1Chi\":\"本地流動數據\",\"planType\":\"BASIC\",\"refInfo\":{\"entitlementUsedUsage\":null,\"entitlementUnit\":null,\"roamingWiFiInScopeUsageInMins\":null,\"roamingWiFiFreeEntitlementInMins\":null,\"entitlementValue\":null,\"entitlementserviceType\":null,\"quotaServiceItemList\":[]},\"showInMainPage\":\"Y\",\"titleLevel1Display\":null,\"quotaTopupInfo\":{\"pcrfServiceType\":\"Local Topup Plan\",\"quotaValidityPeriod\":\"B\",\"displayNameInEnglish\":\"Local Mobile Data\",\"pcrfServiceName\":\"Local_Quota_3G_1\",\"topupQuotaName\":\"Quota_LocalQuota3G1\",\"initialQuotaInKB\":5242880},\"additionalMessage\":null,\"displayOrder\":10,\"titleLevel2Chi\":null,\"asOfDateChi\":\"最新紀錄\",\"allowTopup\":\"Y\",\"asOfDateDisplay\":null},{\"region\":\"LOCAL\",\"asOfDateEng\":\"Latest update\",\"usageBar\":{\"colorG\":204,\"colorR\":0,\"showBar\":\"Y\",\"colorB\":0,\"barPercent\":100},\"titleLevel1Eng\":\"Smart Apps Data Package\",\"otherVAS\":null,\"titleLevel2Display\":null,\"usageType\":\"QUOTA-BASED\",\"show\":\"Y\",\"titleLevel2Eng\":null,\"serviceType\":\"Smart Apps Data Package\",\"usageDescription\":{\"consumedInMB\":\"0\",\"textColorG\":0,\"showWarningIcon\":\"N\",\"remainingInMB\":\"61,440\",\"textDisplay\":null,\"htmlChi\":\"尚餘: 100% (61,440MB)\",\"wapEntitleDisplay\":null,\"initialInMB\":\"61,440\",\"textColorR\":0,\"textChi\":\"尚餘: 100% (61,440MB)\",\"htmlEng\":\"100% (61,440MB)Remaining\",\"textColorB\":0,\"textEng\":\"100% (61,440MB) Remaining\"},\"titleLevel1Chi\":\"Smart Apps 數據組合\",\"planType\":\"VAS\",\"refInfo\":{\"entitlementUsedUsage\":null,\"entitlementUnit\":null,\"roamingWiFiInScopeUsageInMins\":null,\"roamingWiFiFreeEntitlementInMins\":null,\"entitlementValue\":null,\"entitlementserviceType\":null,\"quotaServiceItemList\":[]},\"showInMainPage\":\"N\",\"titleLevel1Display\":null,\"quotaTopupInfo\":{\"pcrfServiceType\":\"Smart Apps Data Package\",\"quotaValidityPeriod\":\"B\",\"displayNameInEnglish\":\"Smart Apps Data Package\",\"pcrfServiceName\":\"Local_VAS5_Q1\",\"topupQuotaName\":\"Quota_LocalVAS5Q1\",\"initialQuotaInKB\":62914560},\"additionalMessage\":null,\"displayOrder\":5030,\"titleLevel2Chi\":null,\"asOfDateChi\":\"最新紀錄\",\"allowTopup\":\"N\",\"asOfDateDisplay\":null},{\"region\":\"ROAMING\",\"asOfDateEng\":\"\",\"usageBar\":{\"colorG\":204,\"colorR\":0,\"showBar\":\"N\",\"colorB\":0,\"barPercent\":100},\"titleLevel1Eng\":\"Roaming Mobile Data–Day Pass\",\"otherVAS\":null,\"titleLevel2Display\":null,\"usageType\":\"QUOTA-BASED\",\"show\":\"Y\",\"titleLevel2Eng\":\"Day Pass\",\"serviceType\":\"Roaming All-In-One DayPass\",\"usageDescription\":{\"consumedInMB\":\"0\",\"textColorG\":0,\"showWarningIcon\":\"N\",\"remainingInMB\":\"10\",\"textDisplay\":null,\"htmlChi\":\"尚餘 0% (0MB) \",\"wapEntitleDisplay\":null,\"initialInMB\":\"10\",\"textColorR\":0,\"textChi\":\"尚餘 0% (0MB) \",\"htmlEng\":\"0% (0MB) Remaining\",\"textColorB\":0,\"textEng\":\"0% (0MB) Remaining\"},\"titleLevel1Chi\":\"漫遊流動數據–日費計劃\",\"planType\":\"BASIC\",\"refInfo\":{\"entitlementUsedUsage\":null,\"entitlementUnit\":null,\"roamingWiFiInScopeUsageInMins\":null,\"roamingWiFiFreeEntitlementInMins\":null,\"entitlementValue\":null,\"entitlementserviceType\":null,\"quotaServiceItemList\":[]},\"showInMainPage\":\"Y\",\"titleLevel1Display\":null,\"quotaTopupInfo\":{\"pcrfServiceType\":\"Roaming All-In-One DayPass\",\"quotaValidityPeriod\":\"D\",\"displayNameInEnglish\":\"Roaming Mobile Data\",\"pcrfServiceName\":\"Roaming_QuotaDayP_3G_4\",\"topupQuotaName\":\"Quota_RoamQuotaDP3G4\",\"initialQuotaInKB\":10240},\"additionalMessage\":null,\"displayOrder\":10,\"titleLevel2Chi\":\"日費計劃\",\"asOfDateChi\":\"\",\"allowTopup\":\"N\",\"asOfDateDisplay\":null},{\"region\":\"ROAMING\",\"asOfDateEng\":\"\",\"usageBar\":{\"colorG\":204,\"colorR\":0,\"showBar\":\"N\",\"colorB\":0,\"barPercent\":100},\"titleLevel1Eng\":\"Roaming WiFi–Day Pass\",\"otherVAS\":\"N\",\"titleLevel2Display\":null,\"usageType\":\"ENTITLEMENT-BASED\",\"show\":\"Y\",\"titleLevel2Eng\":\"Day Pass\",\"serviceType\":\"Roaming WIFI Day Pass\",\"usageDescription\":{\"consumedInMB\":null,\"textColorG\":0,\"showWarningIcon\":\"N\",\"remainingInMB\":null,\"textDisplay\":null,\"htmlChi\":\"已用用量: 0分鐘 | 可享用量: 0分鐘\",\"wapEntitleDisplay\":null,\"initialInMB\":null,\"textColorR\":0,\"textChi\":\"已用用量: 0分鐘 | 可享用量: 0分鐘\",\"htmlEng\":\"Used: 0mins | Entitled: 0mins\",\"textColorB\":0,\"textEng\":\"Used: 0mins | Entitled: 0mins\"},\"titleLevel1Chi\":\"WiFi漫遊–日費計劃\",\"planType\":\"OTHER\",\"refInfo\":{\"entitlementUsedUsage\":\"0\",\"entitlementUnit\":\"mins\",\"roamingWiFiInScopeUsageInMins\":null,\"roamingWiFiFreeEntitlementInMins\":null,\"entitlementValue\":\"60\",\"entitlementserviceType\":null,\"quotaServiceItemList\":null},\"showInMainPage\":\"Y\",\"titleLevel1Display\":null,\"quotaTopupInfo\":null,\"additionalMessage\":null,\"displayOrder\":10000,\"titleLevel2Chi\":\"日費計劃\",\"asOfDateChi\":\"\",\"allowTopup\":\"N\",\"asOfDateDisplay\":null}],\"rtnCode\":\"0\",\"acctType\":\"nornal\"},\"lSMSIter\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"g3AcctBomNextBillDateDTO\":{\"bomNextBillDate\":\"20161007\",\"result\":null,\"bomStartBillDate\":\"20160907\",\"acctNum\":\"\"},\"g3UsageQuotaDTO\":{\"localVoicePeakEntitlement\":\"\",\"mobileDataRtnMsg\":null,\"asOfDate\":\"22/09/2016\",\"primaryFlag\":null,\"overseasMMSEntitlement\":\"\",\"mobileLocalDataQuota\":null,\"localMMSIntraEntitlement\":\"300\",\"mobileRoamingDataUnit\":null,\"localVideoInterEntitlement\":\"\",\"localVoiceInterUnit\":\"mins\",\"localSMSIntraEntitlement\":\"Unlimit\",\"moovUnit\":\"mins\",\"localVoiceInterEntitlement\":\"Unlimit\",\"localDataQuotaBaseInd\":null,\"localBBUnit\":null,\"localVoicePeakUnit\":\"mins\",\"mobileLocalDataRemaining\":null,\"localVideoInterUnit\":\"mins\",\"localBBUsage\":null,\"mobileRoamingDataQuota\":null,\"roamingDataQuotaBaseInd\":null,\"localVideoIntraUsage\":\"0\",\"emailUnit\":null,\"localSMSInterUsage\":\"\",\"nonMobileUsageEntitlementRtnCode\":\"0\",\"localSMSInterEntitlement\":\"\",\"roamBBUsage\":null,\"emailEntitlement\":\"\",\"localVideoIntraUnit\":\"mins\",\"mobileRoamingDataUsage\":null,\"mobileLocalDataInitialQuota\":null,\"emailUsage\":\"\",\"localVideoInterUsage\":\"\",\"moovUsage\":\"0\",\"localVoiceOffPeakUsage\":\"\",\"roamBBUnit\":null,\"roamSMSUsage\":null,\"dataAsOfDate\":null,\"localVideoIntraEntitlement\":\"300\",\"localVoiceOffPeakUnit\":\"mins\",\"overseasMMSUsage\":\"\",\"localMMSInterEntitlement\":\"\",\"mobileLocalDataUnit\":null,\"localMMSInterUnit\":null,\"nowSportsUnit\":\"mins\",\"localMMSIntraUnit\":null,\"moovEntitlement\":\"Unlimit\",\"localMMSIntraUsage\":\"0\",\"nowTVEntitlement\":\"Unlimit\",\"nonMobileUsageEntitlementRtnMsg\":\"\",\"mobileLocalDataUsedInPercent\":null,\"nowTVUnit\":\"mins\",\"mobileLocalDataUsage\":null,\"localVoicePeakUsage\":\"\",\"overseasMMSUnit\":null,\"localVoicePeakOffpeakBasedInd\":\"N\",\"localMMSInterUsage\":\"\",\"mobileRoamingDataInitialQuota\":null,\"localVoiceIntraUnit\":\"mins\",\"nowSportsEntitlement\":\"\",\"iddVoiceUnit\":null,\"mobileLocalDataRemainingInMB\":null,\"localDataQtaName\":null,\"nxtBilCutoffDt\":null,\"localVoiceIntraUsage\":\"\",\"mobileLocalDataRemainingInPercent\":null,\"iddVoiceUsage\":null,\"overseasSMSUsage\":\"\",\"localVoiceIntraEntitlement\":\"\",\"localVoiceOffPeakEntitlement\":\"\",\"roamBBEntitlement\":null,\"iddVoiceEntitlement\":null,\"roamSMSEntitlement\":null,\"localBBEntitlement\":null,\"nowTVUsage\":\"0\",\"localSMSIntraUsage\":\"4\",\"nowSportsUsage\":\"\",\"localVoiceInterUsage\":\"0\",\"mobileDataRtnCd\":null,\"roamingDataQtaName\":null,\"overseasSMSEntitlement\":\"\"},\"usgLastUpdTs\":\"\",\"iSMS\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"nowSports\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"lSMSItra\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"entBased\":false,\"g3GetUnbilledDailyRoamWifiResultDTO\":{\"rtnMsg\":\"\",\"g3UnbilledDailyRoamWifiDTOArr\":[{\"unit\":\"Min\",\"startDate\":\"23/09/2016\",\"inScopeUsedUsage\":\"0\",\"freeEntitle\":\"60\",\"usedUsage\":\"0\",\"lastUpdDate\":\"\",\"serviceType\":\"Roaming_Wifi_Day_Pass\",\"callType\":\"\"}],\"resultRefCode\":null,\"rtnCd\":\"0\"},\"lVdoItra\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"optVasAry\":[{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"Viu Premium Service (Data Pass)\",\"srvCmmtPrd\":\"12\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2016-04-05\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"39.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"39.0\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"Viu升級會員服務 (Data Pass)\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"GOTV Monthly Pass\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2015-07-09\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"59.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"GOTV 月費計劃\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"WeChat Data Package\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2015-03-02\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"8.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"WeChat 數據組合\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"Smart Apps Data Package\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2013-12-10\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"200.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"Smart Apps數據組合\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"EasyCare: 300 requests\n- thereafter charge $0.5/request\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2013-09-16\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"28.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"關心.易: 300次查詢\n- 額外用量費用$0.5/查詢\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"SMS BOXX: Basic\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2013-08-23\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"8.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"SMS BOXX: 基本\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"}],\"g3SummUsageQuotaDTO\":{\"localVoicePeakEntitlement\":null,\"mobileDataRtnMsg\":null,\"asOfDate\":null,\"primaryFlag\":null,\"overseasMMSEntitlement\":null,\"mobileLocalDataQuota\":null,\"localMMSIntraEntitlement\":null,\"mobileRoamingDataUnit\":null,\"localVideoInterEntitlement\":null,\"localVoiceInterUnit\":null,\"localSMSIntraEntitlement\":null,\"moovUnit\":null,\"localVoiceInterEntitlement\":null,\"localDataQuotaBaseInd\":null,\"localBBUnit\":null,\"localVoicePeakUnit\":null,\"mobileLocalDataRemaining\":null,\"localVideoInterUnit\":null,\"localBBUsage\":null,\"mobileRoamingDataQuota\":null,\"roamingDataQuotaBaseInd\":null,\"localVideoIntraUsage\":null,\"emailUnit\":null,\"localSMSInterUsage\":null,\"nonMobileUsageEntitlementRtnCode\":null,\"localSMSInterEntitlement\":null,\"roamBBUsage\":null,\"emailEntitlement\":null,\"localVideoIntraUnit\":null,\"mobileRoamingDataUsage\":null,\"mobileLocalDataInitialQuota\":null,\"emailUsage\":null,\"localVideoInterUsage\":null,\"moovUsage\":null,\"localVoiceOffPeakUsage\":null,\"roamBBUnit\":null,\"roamSMSUsage\":null,\"dataAsOfDate\":null,\"localVideoIntraEntitlement\":null,\"localVoiceOffPeakUnit\":null,\"overseasMMSUsage\":null,\"localMMSInterEntitlement\":null,\"mobileLocalDataUnit\":null,\"localMMSInterUnit\":null,\"nowSportsUnit\":null,\"localMMSIntraUnit\":null,\"moovEntitlement\":null,\"localMMSIntraUsage\":null,\"nowTVEntitlement\":null,\"nonMobileUsageEntitlementRtnMsg\":null,\"mobileLocalDataUsedInPercent\":null,\"nowTVUnit\":null,\"mobileLocalDataUsage\":null,\"localVoicePeakUsage\":null,\"overseasMMSUnit\":null,\"localVoicePeakOffpeakBasedInd\":null,\"localMMSInterUsage\":null,\"mobileRoamingDataInitialQuota\":null,\"localVoiceIntraUnit\":null,\"nowSportsEntitlement\":null,\"iddVoiceUnit\":null,\"mobileLocalDataRemainingInMB\":null,\"localDataQtaName\":null,\"nxtBilCutoffDt\":null,\"localVoiceIntraUsage\":null,\"mobileLocalDataRemainingInPercent\":null,\"iddVoiceUsage\":null,\"overseasSMSUsage\":null,\"localVoiceIntraEntitlement\":null,\"localVoiceOffPeakEntitlement\":null,\"roamBBEntitlement\":null,\"iddVoiceEntitlement\":null,\"roamSMSEntitlement\":null,\"localBBEntitlement\":null,\"nowTVUsage\":null,\"localSMSIntraUsage\":null,\"nowSportsUsage\":null,\"localVoiceInterUsage\":null,\"mobileDataRtnCd\":null,\"roamingDataQtaName\":null,\"overseasSMSEntitlement\":null}},\"oTtlEntMB\":\"\",\"iTopupItem\":{\"region\":null,\"asOfDateEng\":null,\"usageBar\":null,\"titleLevel1Eng\":null,\"otherVAS\":null,\"titleLevel2Display\":null,\"usageType\":null,\"show\":null,\"titleLevel2Eng\":null,\"serviceType\":null,\"usageDescription\":null,\"titleLevel1Chi\":null,\"planType\":null,\"refInfo\":null,\"showInMainPage\":null,\"titleLevel1Display\":null,\"quotaTopupInfo\":null,\"additionalMessage\":null,\"displayOrder\":0,\"titleLevel2Chi\":null,\"asOfDateChi\":null,\"allowTopup\":null,\"asOfDateDisplay\":null},\"oG3RechargeHistDTOAry\":[],\"reply\":{\"cargo\":null,\"code\":\"RC_AO_PSPH_FAIL\"},\"iApi\":\"PLAN_MOB\",\"iTopupActn\":\"\",\"oEntExpDT\":\"\",\"oMthlyPkgMB\":\"\",\"iBstUpOff\":{\"boostUpOfferId\":null,\"rechargeValue\":null,\"displayDescription\":null,\"rechargeAmount\":null,\"quotaName\":null,\"engDescription\":null,\"valPeriod\":null,\"chiDescription\":null,\"rechargeAmountDisplay\":null,\"rechargeValueInMB\":null}}";

        //				return "{\"serverTS\":\"20160923102025\",\"iSubnRec\":{\"rev\":0,\"createTs\":\"\",\"netLoginId\":\"\",\"custRid\":0,\"systy\":\"MOB\",\"instAdr\":\"\",\"eyeGrp\":\"\",\"sipSubr\":\"\",\"wipCust\":\"\",\"storeTy\":\"AO\",\"lastupdTs\":\"\",\"datCd\":\"\",\"indTv\":\"\",\"lastupdPsn\":\"\",\"lob\":\"XMOB\",\"tos\":\"3G\",\"indPcd\":\"\",\"auth_ph\":\"4baefdd1d26d0f5b729e21e71b3e2803d11d21524c1ccc304fc47f8ac88a69a6\",\"tariff\":\"\",\"indEye\":\"\",\"createPsn\":\"\",\"cusNum\":\"10954444\",\"assoc\":\"\",\"fsa\":\"\",\"acct_ty\":\"C\",\"alias\":\"\",\"acctNum\":\"75100096687984\",\"ind2l2b\":\"\",\"domainTy\":\"\",\"inMipMig\":false,\"srvNum\":\"92741281\",\"bsn\":\"\",\"rid\":0,\"refFsa2l2b\":\"\",\"srvId\":\"\",\"priMob\":\"\",\"aloneDialup\":\"\",\"ivr_pwd\":\"\"},\"iGwtGud\":\"86CEFDF53E85411E\",\"iBstUpHistEnDt\":\"\",\"oDuct\":null,\"iLoginId\":\"\",\"oVbpCustTy\":\"\",\"oNxUrl\":\"\",\"iMyFrm\":\"\",\"oContrEndDt\":\"\",\"oG3BoostUpOffer1DTOAry\":[],\"iBstUpHistStDt\":\"\",\"oHgg\":\"\",\"oMobUsage\":{\"lVdoIter\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"nowTv\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"lVceIter\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"localData\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"lVceItra\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"moov\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"ratePlan\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"Monthly Mobile Service Fee\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"MRP0001207\",\"fee\":\"438.0\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"基本通話月費\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"g3DisplayServiceItemResultDTO\":{\"rtnMsg\":\"\",\"displayServiceItemList\":[{\"region\":\"LOCAL\",\"asOfDateEng\":\"Latest update\",\"usageBar\":{\"colorG\":204,\"colorR\":0,\"showBar\":\"Y\",\"colorB\":0,\"barPercent\":100},\"titleLevel1Eng\":\"Local Mobile Data\",\"otherVAS\":\"Y\",\"titleLevel2Display\":null,\"usageType\":\"QUOTA-BASED\",\"show\":\"Y\",\"titleLevel2Eng\":null,\"serviceType\":\"Local Topup Plan\",\"usageDescription\":{\"consumedInMB\":\"0\",\"textColorG\":0,\"showWarningIcon\":\"N\",\"remainingInMB\":\"5,320\",\"textDisplay\":null,\"htmlChi\":\"尚餘: 100% (5,320MB)\",\"wapEntitleDisplay\":null,\"initialInMB\":\"5,120\",\"textColorR\":0,\"textChi\":\"尚餘: 100% (5,320MB)\",\"htmlEng\":\"100% (5,320MB)Remaining\",\"textColorB\":0,\"textEng\":\"100% (5,320MB) Remaining\"},\"titleLevel1Chi\":\"本地流動數據\",\"planType\":\"BASIC\",\"refInfo\":{\"entitlementUsedUsage\":null,\"entitlementUnit\":null,\"roamingWiFiInScopeUsageInMins\":null,\"roamingWiFiFreeEntitlementInMins\":null,\"entitlementValue\":null,\"entitlementserviceType\":null,\"quotaServiceItemList\":[]},\"showInMainPage\":\"Y\",\"titleLevel1Display\":null,\"quotaTopupInfo\":{\"pcrfServiceType\":\"Local Topup Plan\",\"quotaValidityPeriod\":\"B\",\"displayNameInEnglish\":\"Local Mobile Data\",\"pcrfServiceName\":\"Local_Quota_3G_1\",\"topupQuotaName\":\"Quota_LocalQuota3G1\",\"initialQuotaInKB\":5242880},\"additionalMessage\":null,\"displayOrder\":10,\"titleLevel2Chi\":null,\"asOfDateChi\":\"最新紀錄\",\"allowTopup\":\"Y\",\"asOfDateDisplay\":null},{\"region\":\"LOCAL\",\"asOfDateEng\":\"Latest update\",\"usageBar\":{\"colorG\":204,\"colorR\":0,\"showBar\":\"Y\",\"colorB\":0,\"barPercent\":100},\"titleLevel1Eng\":\"Smart Apps Data Package\",\"otherVAS\":null,\"titleLevel2Display\":null,\"usageType\":\"QUOTA-BASED\",\"show\":\"Y\",\"titleLevel2Eng\":null,\"serviceType\":\"Smart Apps Data Package\",\"usageDescription\":{\"consumedInMB\":\"0\",\"textColorG\":0,\"showWarningIcon\":\"N\",\"remainingInMB\":\"61,440\",\"textDisplay\":null,\"htmlChi\":\"尚餘: 100% (61,440MB)\",\"wapEntitleDisplay\":null,\"initialInMB\":\"61,440\",\"textColorR\":0,\"textChi\":\"尚餘: 100% (61,440MB)\",\"htmlEng\":\"100% (61,440MB)Remaining\",\"textColorB\":0,\"textEng\":\"100% (61,440MB) Remaining\"},\"titleLevel1Chi\":\"Smart Apps 數據組合\",\"planType\":\"VAS\",\"refInfo\":{\"entitlementUsedUsage\":null,\"entitlementUnit\":null,\"roamingWiFiInScopeUsageInMins\":null,\"roamingWiFiFreeEntitlementInMins\":null,\"entitlementValue\":null,\"entitlementserviceType\":null,\"quotaServiceItemList\":[]},\"showInMainPage\":\"N\",\"titleLevel1Display\":null,\"quotaTopupInfo\":{\"pcrfServiceType\":\"Smart Apps Data Package\",\"quotaValidityPeriod\":\"B\",\"displayNameInEnglish\":\"Smart Apps Data Package\",\"pcrfServiceName\":\"Local_VAS5_Q1\",\"topupQuotaName\":\"Quota_LocalVAS5Q1\",\"initialQuotaInKB\":62914560},\"additionalMessage\":null,\"displayOrder\":5030,\"titleLevel2Chi\":null,\"asOfDateChi\":\"最新紀錄\",\"allowTopup\":\"N\",\"asOfDateDisplay\":null},{\"region\":\"ROAMING\",\"asOfDateEng\":\"\",\"usageBar\":{\"colorG\":204,\"colorR\":0,\"showBar\":\"N\",\"colorB\":0,\"barPercent\":100},\"titleLevel1Eng\":\"Roaming Mobile Data–Day Pass\",\"otherVAS\":null,\"titleLevel2Display\":null,\"usageType\":\"QUOTA-BASED\",\"show\":\"Y\",\"titleLevel2Eng\":\"Day Pass\",\"serviceType\":\"Roaming All-In-One DayPass\",\"usageDescription\":{\"consumedInMB\":\"0\",\"textColorG\":0,\"showWarningIcon\":\"N\",\"remainingInMB\":\"10\",\"textDisplay\":null,\"htmlChi\":\"尚餘 0% (0MB) \",\"wapEntitleDisplay\":null,\"initialInMB\":\"10\",\"textColorR\":0,\"textChi\":\"尚餘 0% (0MB) \",\"htmlEng\":\"0% (0MB) Remaining\",\"textColorB\":0,\"textEng\":\"0% (0MB) Remaining\"},\"titleLevel1Chi\":\"漫遊流動數據–日費計劃\",\"planType\":\"BASIC\",\"refInfo\":{\"entitlementUsedUsage\":null,\"entitlementUnit\":null,\"roamingWiFiInScopeUsageInMins\":null,\"roamingWiFiFreeEntitlementInMins\":null,\"entitlementValue\":null,\"entitlementserviceType\":null,\"quotaServiceItemList\":[]},\"showInMainPage\":\"Y\",\"titleLevel1Display\":null,\"quotaTopupInfo\":{\"pcrfServiceType\":\"Roaming All-In-One DayPass\",\"quotaValidityPeriod\":\"D\",\"displayNameInEnglish\":\"Roaming Mobile Data\",\"pcrfServiceName\":\"Roaming_QuotaDayP_3G_4\",\"topupQuotaName\":\"Quota_RoamQuotaDP3G4\",\"initialQuotaInKB\":10240},\"additionalMessage\":null,\"displayOrder\":10,\"titleLevel2Chi\":\"日費計劃\",\"asOfDateChi\":\"\",\"allowTopup\":\"N\",\"asOfDateDisplay\":null},{\"region\":\"ROAMING\",\"asOfDateEng\":\"\",\"usageBar\":{\"colorG\":204,\"colorR\":0,\"showBar\":\"N\",\"colorB\":0,\"barPercent\":100},\"titleLevel1Eng\":\"Roaming WiFi–Day Pass\",\"otherVAS\":\"N\",\"titleLevel2Display\":null,\"usageType\":\"ENTITLEMENT-BASED\",\"show\":\"Y\",\"titleLevel2Eng\":\"Day Pass\",\"serviceType\":\"Roaming WIFI Day Pass\",\"usageDescription\":{\"consumedInMB\":null,\"textColorG\":0,\"showWarningIcon\":\"N\",\"remainingInMB\":null,\"textDisplay\":null,\"htmlChi\":\"已用用量: 0分鐘 | 可享用量: 0分鐘\",\"wapEntitleDisplay\":null,\"initialInMB\":null,\"textColorR\":0,\"textChi\":\"已用用量: 0分鐘 | 可享用量: 0分鐘\",\"htmlEng\":\"Used: 0mins | Entitled: 0mins\",\"textColorB\":0,\"textEng\":\"Used: 0mins | Entitled: 0mins\"},\"titleLevel1Chi\":\"WiFi漫遊–日費計劃\",\"planType\":\"OTHER\",\"refInfo\":{\"entitlementUsedUsage\":\"0\",\"entitlementUnit\":\"mins\",\"roamingWiFiInScopeUsageInMins\":null,\"roamingWiFiFreeEntitlementInMins\":null,\"entitlementValue\":\"60\",\"entitlementserviceType\":null,\"quotaServiceItemList\":null},\"showInMainPage\":\"Y\",\"titleLevel1Display\":null,\"quotaTopupInfo\":null,\"additionalMessage\":null,\"displayOrder\":10000,\"titleLevel2Chi\":\"日費計劃\",\"asOfDateChi\":\"\",\"allowTopup\":\"N\",\"asOfDateDisplay\":null}],\"rtnCode\":\"0\",\"acctType\":\"nornal\"},\"lSMSIter\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"g3AcctBomNextBillDateDTO\":{\"bomNextBillDate\":\"20161007\",\"result\":null,\"bomStartBillDate\":\"20160907\",\"acctNum\":\"\"},\"g3UsageQuotaDTO\":{\"localVoicePeakEntitlement\":\"\",\"mobileDataRtnMsg\":null,\"asOfDate\":\"22/09/2016\",\"primaryFlag\":null,\"overseasMMSEntitlement\":\"\",\"mobileLocalDataQuota\":null,\"localMMSIntraEntitlement\":\"300\",\"mobileRoamingDataUnit\":null,\"localVideoInterEntitlement\":\"\",\"localVoiceInterUnit\":\"mins\",\"localSMSIntraEntitlement\":\"Unlimit\",\"moovUnit\":\"mins\",\"localVoiceInterEntitlement\":\"Unlimit\",\"localDataQuotaBaseInd\":null,\"localBBUnit\":null,\"localVoicePeakUnit\":\"mins\",\"mobileLocalDataRemaining\":null,\"localVideoInterUnit\":\"mins\",\"localBBUsage\":null,\"mobileRoamingDataQuota\":null,\"roamingDataQuotaBaseInd\":null,\"localVideoIntraUsage\":\"0\",\"emailUnit\":null,\"localSMSInterUsage\":\"\",\"nonMobileUsageEntitlementRtnCode\":\"0\",\"localSMSInterEntitlement\":\"\",\"roamBBUsage\":null,\"emailEntitlement\":\"\",\"localVideoIntraUnit\":\"mins\",\"mobileRoamingDataUsage\":null,\"mobileLocalDataInitialQuota\":null,\"emailUsage\":\"\",\"localVideoInterUsage\":\"\",\"moovUsage\":\"0\",\"localVoiceOffPeakUsage\":\"\",\"roamBBUnit\":null,\"roamSMSUsage\":null,\"dataAsOfDate\":null,\"localVideoIntraEntitlement\":\"300\",\"localVoiceOffPeakUnit\":\"mins\",\"overseasMMSUsage\":\"\",\"localMMSInterEntitlement\":\"\",\"mobileLocalDataUnit\":null,\"localMMSInterUnit\":null,\"nowSportsUnit\":\"mins\",\"localMMSIntraUnit\":null,\"moovEntitlement\":\"Unlimit\",\"localMMSIntraUsage\":\"0\",\"nowTVEntitlement\":\"Unlimit\",\"nonMobileUsageEntitlementRtnMsg\":\"\",\"mobileLocalDataUsedInPercent\":null,\"nowTVUnit\":\"mins\",\"mobileLocalDataUsage\":null,\"localVoicePeakUsage\":\"\",\"overseasMMSUnit\":null,\"localVoicePeakOffpeakBasedInd\":\"N\",\"localMMSInterUsage\":\"\",\"mobileRoamingDataInitialQuota\":null,\"localVoiceIntraUnit\":\"mins\",\"nowSportsEntitlement\":\"\",\"iddVoiceUnit\":null,\"mobileLocalDataRemainingInMB\":null,\"localDataQtaName\":null,\"nxtBilCutoffDt\":null,\"localVoiceIntraUsage\":\"\",\"mobileLocalDataRemainingInPercent\":null,\"iddVoiceUsage\":null,\"overseasSMSUsage\":\"\",\"localVoiceIntraEntitlement\":\"\",\"localVoiceOffPeakEntitlement\":\"\",\"roamBBEntitlement\":null,\"iddVoiceEntitlement\":null,\"roamSMSEntitlement\":null,\"localBBEntitlement\":null,\"nowTVUsage\":\"0\",\"localSMSIntraUsage\":\"4\",\"nowSportsUsage\":\"\",\"localVoiceInterUsage\":\"0\",\"mobileDataRtnCd\":null,\"roamingDataQtaName\":null,\"overseasSMSEntitlement\":\"\"},\"usgLastUpdTs\":\"\",\"iSMS\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"nowSports\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"lSMSItra\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"entBased\":false,\"g3GetUnbilledDailyRoamWifiResultDTO\":{\"rtnMsg\":\"\",\"g3UnbilledDailyRoamWifiDTOArr\":[{\"unit\":\"Min\",\"startDate\":\"23/09/2016\",\"inScopeUsedUsage\":\"0\",\"freeEntitle\":\"60\",\"usedUsage\":\"0\",\"lastUpdDate\":\"\",\"serviceType\":\"Roaming_Wifi_Day_Pass\",\"callType\":\"\"}],\"resultRefCode\":null,\"rtnCd\":\"0\"},\"lVdoItra\":{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},\"optVasAry\":[{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"Viu Premium Service (Data Pass)\",\"srvCmmtPrd\":\"12\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2016-04-05\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"39.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"39.0\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"Viu升級會員服務 (Data Pass)\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"GOTV Monthly Pass\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2015-07-09\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"59.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"GOTV 月費計劃\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"WeChat Data Package\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2015-03-02\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"8.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"WeChat 數據組合\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"Smart Apps Data Package\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2013-12-10\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"200.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"Smart Apps數據組合\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"EasyCare: 300 requests\n- thereafter charge $0.5/request\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2013-09-16\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"28.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"關心.易: 300次查詢\n- 額外用量費用$0.5/查詢\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"},{\"srvEnt\":\"\",\"usg\":\"\",\"tncEn\":\"\",\"longDesn2Zh\":\"\",\"monthlyTermRate\":\"\",\"srvCmmtRmn\":\"\",\"longDesn2En\":\"\",\"tncZh\":\"\",\"longDesn1En\":\"\",\"srvPenalty\":\"\",\"rebtZh\":\"\",\"desnEn\":\"SMS BOXX: Basic\",\"srvCmmtPrd\":\"\",\"cntrDur\":\"\",\"suppInd\":\"\",\"plnSubTy\":\"\",\"plnTy\":\"\",\"usgEnDt\":\"\",\"usgUnt\":\"\",\"StDt\":\"2013-08-23\",\"longDesn1Zh\":\"\",\"rebtAmt\":\"\",\"m2mRate\":\"8.0\",\"cmmtInd\":\"\",\"rebtEn\":\"\",\"srvCd\":\"\",\"fee\":\"\",\"usgStDt\":\"\",\"summInd\":\"\",\"desnZh\":\"SMS BOXX: 基本\",\"EnDt\":\"\",\"seasInd\":\"\",\"cntrEnDt\":\"\",\"cntrStDt\":\"\"}],\"g3SummUsageQuotaDTO\":{\"localVoicePeakEntitlement\":null,\"mobileDataRtnMsg\":null,\"asOfDate\":null,\"primaryFlag\":null,\"overseasMMSEntitlement\":null,\"mobileLocalDataQuota\":null,\"localMMSIntraEntitlement\":null,\"mobileRoamingDataUnit\":null,\"localVideoInterEntitlement\":null,\"localVoiceInterUnit\":null,\"localSMSIntraEntitlement\":null,\"moovUnit\":null,\"localVoiceInterEntitlement\":null,\"localDataQuotaBaseInd\":null,\"localBBUnit\":null,\"localVoicePeakUnit\":null,\"mobileLocalDataRemaining\":null,\"localVideoInterUnit\":null,\"localBBUsage\":null,\"mobileRoamingDataQuota\":null,\"roamingDataQuotaBaseInd\":null,\"localVideoIntraUsage\":null,\"emailUnit\":null,\"localSMSInterUsage\":null,\"nonMobileUsageEntitlementRtnCode\":null,\"localSMSInterEntitlement\":null,\"roamBBUsage\":null,\"emailEntitlement\":null,\"localVideoIntraUnit\":null,\"mobileRoamingDataUsage\":null,\"mobileLocalDataInitialQuota\":null,\"emailUsage\":null,\"localVideoInterUsage\":null,\"moovUsage\":null,\"localVoiceOffPeakUsage\":null,\"roamBBUnit\":null,\"roamSMSUsage\":null,\"dataAsOfDate\":null,\"localVideoIntraEntitlement\":null,\"localVoiceOffPeakUnit\":null,\"overseasMMSUsage\":null,\"localMMSInterEntitlement\":null,\"mobileLocalDataUnit\":null,\"localMMSInterUnit\":null,\"nowSportsUnit\":null,\"localMMSIntraUnit\":null,\"moovEntitlement\":null,\"localMMSIntraUsage\":null,\"nowTVEntitlement\":null,\"nonMobileUsageEntitlementRtnMsg\":null,\"mobileLocalDataUsedInPercent\":null,\"nowTVUnit\":null,\"mobileLocalDataUsage\":null,\"localVoicePeakUsage\":null,\"overseasMMSUnit\":null,\"localVoicePeakOffpeakBasedInd\":null,\"localMMSInterUsage\":null,\"mobileRoamingDataInitialQuota\":null,\"localVoiceIntraUnit\":null,\"nowSportsEntitlement\":null,\"iddVoiceUnit\":null,\"mobileLocalDataRemainingInMB\":null,\"localDataQtaName\":null,\"nxtBilCutoffDt\":null,\"localVoiceIntraUsage\":null,\"mobileLocalDataRemainingInPercent\":null,\"iddVoiceUsage\":null,\"overseasSMSUsage\":null,\"localVoiceIntraEntitlement\":null,\"localVoiceOffPeakEntitlement\":null,\"roamBBEntitlement\":null,\"iddVoiceEntitlement\":null,\"roamSMSEntitlement\":null,\"localBBEntitlement\":null,\"nowTVUsage\":null,\"localSMSIntraUsage\":null,\"nowSportsUsage\":null,\"localVoiceInterUsage\":null,\"mobileDataRtnCd\":null,\"roamingDataQtaName\":null,\"overseasSMSEntitlement\":null}},\"oTtlEntMB\":\"\",\"iTopupItem\":{\"region\":null,\"asOfDateEng\":null,\"usageBar\":null,\"titleLevel1Eng\":null,\"otherVAS\":null,\"titleLevel2Display\":null,\"usageType\":null,\"show\":null,\"titleLevel2Eng\":null,\"serviceType\":null,\"usageDescription\":null,\"titleLevel1Chi\":null,\"planType\":null,\"refInfo\":null,\"showInMainPage\":null,\"titleLevel1Display\":null,\"quotaTopupInfo\":null,\"additionalMessage\":null,\"displayOrder\":0,\"titleLevel2Chi\":null,\"asOfDateChi\":null,\"allowTopup\":null,\"asOfDateDisplay\":null},\"oG3RechargeHistDTOAry\":[],\"reply\":{\"cargo\":null,\"code\":\"RC_SUCC\"},\"iApi\":\"PLAN_MOB\",\"iTopupActn\":\"\",\"oEntExpDT\":\"\",\"oMthlyPkgMB\":\"\",\"iBstUpOff\":{\"boostUpOfferId\":null,\"rechargeValue\":null,\"displayDescription\":null,\"rechargeAmount\":null,\"quotaName\":null,\"engDescription\":null,\"valPeriod\":null,\"chiDescription\":null,\"rechargeAmountDisplay\":null,\"rechargeValueInMB\":null}}";
        //				}
        //			return "{   \"iLoginId\": \"keith.kk.mak@pccw.com\",   \"iCustRec\": {     \"rid\": 7503,     \"docTy\": \"PASS\",     \"docNum\": \"88888891\",     \"premier\": \" \",     \"phylum\": \"CSUM\",     \"status\": \"A\",     \"createTs\": \"20151127164938\",     \"createPsn\": \"dldsbat\",     \"lastupdTs\": \"20151127164938\",     \"lastupdPsn\": \"dldsbat\",     \"rev\": 0   },   \"iSubnRec\": {     \"rid\": 0,     \"custRid\": 0,     \"cusNum\": \"\",     \"acctNum\": \"\",     \"srvId\": \"\",     \"srvNum\": \"\",     \"bsn\": \"\",     \"fsa\": \"\",     \"lob\": \"\",     \"wipCust\": \"\",     \"sipSubr\": \"\",     \"netLoginId\": \"\",     \"aloneDialup\": \"\",     \"eyeGrp\": \"\",     \"ind2l2b\": \"\",     \"refFsa2l2b\": \"\",     \"indPcd\": \"\",     \"indTv\": \"\",     \"indEye\": \"\",     \"domainTy\": \"\",     \"tos\": \"\",     \"datCd\": \"\",     \"tariff\": \"\",     \"priMob\": \"\",     \"systy\": \"\",     \"alias\": \"\",     \"assoc\": \"\",     \"createTs\": \"\",     \"createPsn\": \"\",     \"lastupdTs\": \"\",     \"lastupdPsn\": \"\",     \"rev\": 0,     \"instAdr\": \"\",     \"storeTy\": \"\",     \"ivr_pwd\": null,     \"auth_ph\": null,     \"acct_ty\": null   },   \"iSubnRecAry\": [     {       \"rid\": 1,       \"custRid\": 7503,       \"cusNum\": \"10004631\",       \"acctNum\": \"73001278660919\",       \"srvId\": \"100121382\",       \"srvNum\": \"98238921\",       \"bsn\": \" \",       \"fsa\": \" \",       \"lob\": \"MOB\",       \"wipCust\": \"N\",       \"sipSubr\": \"N\",       \"netLoginId\": \" \",       \"aloneDialup\": \" \",       \"eyeGrp\": \" \",       \"ind2l2b\": \" \",       \"refFsa2l2b\": \" \",       \"indPcd\": \" \",       \"indTv\": \" \",       \"indEye\": \" \",       \"domainTy\": \" \",       \"tos\": \"3G\",       \"datCd\": \" \",       \"tariff\": \" \",       \"priMob\": \" \",       \"systy\": \"MOB\",       \"alias\": \" \",       \"assoc\": \"Y\",       \"createTs\": \"20151127162542\",       \"createPsn\": \"dldsbat\",       \"lastupdTs\": \"20160128180226\",       \"lastupdPsn\": \"keith.kk.mak@pccw.com\",       \"rev\": 37,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     },     {       \"rid\": 99987705,       \"custRid\": 7503,       \"cusNum\": \" \",       \"acctNum\": \"09757477\",       \"srvId\": \"88000004\",       \"srvNum\": \"88000004\",       \"bsn\": \" \",       \"fsa\": \" \",       \"lob\": \"O2F\",       \"wipCust\": \"N\",       \"sipSubr\": \"N\",       \"netLoginId\": \" \",       \"aloneDialup\": \" \",       \"eyeGrp\": \" \",       \"ind2l2b\": \" \",       \"refFsa2l2b\": \" \",       \"indPcd\": \" \",       \"indTv\": \" \",       \"indEye\": \" \",       \"domainTy\": \" \",       \"tos\": \" \",       \"datCd\": \" \",       \"tariff\": \" \",       \"priMob\": \" \",       \"systy\": \"CSL\",       \"alias\": \" \",       \"assoc\": \"Y\",       \"createTs\": \"20160201113243\",       \"createPsn\": \"KM\",       \"lastupdTs\": \"20160201113243\",       \"lastupdPsn\": \"KM\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     },     {       \"rid\": 99887705,       \"custRid\": 7503,       \"cusNum\": \" \",       \"acctNum\": \"90657073\",       \"srvId\": \"60530731\",       \"srvNum\": \"60530731\",       \"bsn\": \" \",       \"fsa\": \" \",       \"lob\": \"O2F\",       \"wipCust\": \"N\",       \"sipSubr\": \"N\",       \"netLoginId\": \" \",       \"aloneDialup\": \" \",       \"eyeGrp\": \" \",       \"ind2l2b\": \" \",       \"refFsa2l2b\": \" \",       \"indPcd\": \" \",       \"indTv\": \" \",       \"indEye\": \" \",       \"domainTy\": \" \",       \"tos\": \" \",       \"datCd\": \" \",       \"tariff\": \" \",       \"priMob\": \" \",       \"systy\": \"CSL\",       \"alias\": \" \",       \"assoc\": \"Y\",       \"createTs\": \"20160201113243\",       \"createPsn\": \"KM\",       \"lastupdTs\": \"20160201113243\",       \"lastupdPsn\": \"KM\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     },     {       \"rid\": 34258,       \"custRid\": 7503,       \"cusNum\": \"5100345\",       \"acctNum\": \"1068249749\",       \"srvId\": \"60058422\",       \"srvNum\": \"a60058422\",       \"bsn\": \"20228391\",       \"fsa\": \"60058422\",       \"lob\": \"PCD\",       \"wipCust\": \"N\",       \"sipSubr\": \" \",       \"netLoginId\": \"a60058422\",       \"aloneDialup\": \"N\",       \"eyeGrp\": \" \",       \"ind2l2b\": \"N\",       \"refFsa2l2b\": \" \",       \"indPcd\": \"Y\",       \"indTv\": \"N\",       \"indEye\": \"N\",       \"domainTy\": \"N\",       \"tos\": \"IMS\",       \"datCd\": \" \",       \"tariff\": \" \",       \"priMob\": \" \",       \"systy\": \"IMS\",       \"alias\": \" \",       \"assoc\": \"Y\",       \"createTs\": \"20151127164833\",       \"createPsn\": \"dldsbat\",       \"lastupdTs\": \"20151127164833\",       \"lastupdPsn\": \"dldsbat\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     }   ],   \"iSrvReq\": {     \"srvNum\": \"\",     \"srvNumTy\": \"\",     \"reportNum\": \"\",     \"trunkNum\": \"\",     \"extNum\": \"\",     \"refNum\": \"\",     \"outProd\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"ctNmTtl\": \"\",     \"ctNm\": \"\",     \"ctNum\": \"\",     \"allowUpdInd\": \"\",     \"autoSRInd\": \"\",     \"enSpkr\": \"\",     \"smsLang\": \"\",     \"srInfo\": {       \"custAdr\": \"\",       \"flatCd\": \"\",       \"floorCd\": \"\",       \"srvBndy\": \"\",       \"ctNum\": \"\",       \"ctNm\": \"\",       \"ponLine\": \"\",       \"ponCvg\": \"\",       \"mdmRst\": \"\",       \"bsn\": \"\",       \"fsa\": \"\"     },     \"apptInfo\": {       \"refNum\": \"\",       \"prodTy\": \"\",       \"srvTy\": \"\",       \"areaCd\": \"\",       \"distCd\": \"\",       \"srvNum\": \"\",       \"prodId\": \"\",       \"apptTS\": {         \"apptDate\": \"\",         \"apptTmslot\": \"\",         \"apptDT\": \"\"       },       \"apptTSAry\": []     },     \"updTy\": \"\",     \"assocSubnRec\": {       \"rid\": 0,       \"custRid\": 0,       \"cusNum\": \"\",       \"acctNum\": \"\",       \"srvId\": \"\",       \"srvNum\": \"\",       \"bsn\": \"\",       \"fsa\": \"\",       \"lob\": \"\",       \"wipCust\": \"\",       \"sipSubr\": \"\",       \"netLoginId\": \"\",       \"aloneDialup\": \"\",       \"eyeGrp\": \"\",       \"ind2l2b\": \"\",       \"refFsa2l2b\": \"\",       \"indPcd\": \"\",       \"indTv\": \"\",       \"indEye\": \"\",       \"domainTy\": \"\",       \"tos\": \"\",       \"datCd\": \"\",       \"tariff\": \"\",       \"priMob\": \"\",       \"systy\": \"\",       \"alias\": \"\",       \"assoc\": \"\",       \"createTs\": \"\",       \"createPsn\": \"\",       \"lastupdTs\": \"\",       \"lastupdPsn\": \"\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     }   },   \"iEnqSRTy\": \"\",   \"iSRApptInfo\": {     \"refNum\": \"\",     \"prodTy\": \"\",     \"srvTy\": \"\",     \"areaCd\": \"\",     \"distCd\": \"\",     \"srvNum\": \"\",     \"prodId\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"apptTSAry\": []   },   \"oGnrlApptAry\": [],   \"oBomApptAry\": [],   \"oSrvReqAry\": [     {       \"srvNum\": \"60058422\",       \"srvNumTy\": \"FSA\",       \"reportNum\": \"8567\",       \"trunkNum\": \"\",       \"extNum\": \"\",       \"refNum\": \"\",       \"outProd\": \"PCD\",       \"apptTS\": {         \"apptDate\": \"20160307\",         \"apptTmslot\": \"09\",         \"apptDT\": \"07-Mar 18:00-20:00\"       },       \"ctNmTtl\": \"\",       \"ctNm\": \"MISS CHAN LING LING\",       \"ctNum\": \"63520372\",       \"allowUpdInd\": \"Y\",       \"autoSRInd\": \"N\",       \"enSpkr\": \"\",       \"smsLang\": \"\",       \"srInfo\": {         \"custAdr\": \"FT 15 12/F YCK AM01 BLDG, SHA TIN NEW TERRITORIES\",         \"flatCd\": \"\",         \"floorCd\": \"\",         \"srvBndy\": \"\",         \"ctNum\": \"\",         \"ctNm\": \"\",         \"ponLine\": \"\",         \"ponCvg\": \"N\",         \"mdmRst\": \"\",         \"bsn\": \"\",         \"fsa\": \"\"       },       \"apptInfo\": {         \"refNum\": \"\",         \"prodTy\": \"I\",         \"srvTy\": \"SR\",         \"areaCd\": \"NT\",         \"distCd\": \"222\",         \"srvNum\": \"20228391\",         \"prodId\": \"NU1R\",         \"apptTS\": {           \"apptDate\": \"\",           \"apptTmslot\": \"\",           \"apptDT\": \"\"         },         \"apptTSAry\": []       },       \"updTy\": \"\",       \"assocSubnRec\": {         \"rid\": 34258,         \"custRid\": 7503,         \"cusNum\": \"5100345\",         \"acctNum\": \"1068249749\",         \"srvId\": \"60058422\",         \"srvNum\": \"a60058422\",         \"bsn\": \"20228391\",         \"fsa\": \"60058422\",         \"lob\": \"PCD\",         \"wipCust\": \"N\",         \"sipSubr\": \" \",         \"netLoginId\": \"a60058422\",         \"aloneDialup\": \"N\",         \"eyeGrp\": \" \",         \"ind2l2b\": \"N\",         \"refFsa2l2b\": \" \",         \"indPcd\": \"Y\",         \"indTv\": \"N\",         \"indEye\": \"N\",         \"domainTy\": \"N\",         \"tos\": \"IMS\",         \"datCd\": \" \",         \"tariff\": \" \",         \"priMob\": \" \",         \"systy\": \"IMS\",         \"alias\": \" \",         \"assoc\": \"Y\",         \"createTs\": \"20151127164833\",         \"createPsn\": \"dldsbat\",         \"lastupdTs\": \"20151127164833\",         \"lastupdPsn\": \"dldsbat\",         \"rev\": 0,         \"instAdr\": \"\",         \"storeTy\": \"\",         \"ivr_pwd\": null,         \"auth_ph\": null,         \"acct_ty\": null       }     }   ],   \"oSRApptInfo\": {     \"refNum\": \"\",     \"prodTy\": \"\",     \"srvTy\": \"\",     \"areaCd\": \"\",     \"distCd\": \"\",     \"srvNum\": \"\",     \"prodId\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"apptTSAry\": []   },   \"oPendSrvReq\": {     \"srvNum\": \"\",     \"srvNumTy\": \"\",     \"reportNum\": \"\",     \"trunkNum\": \"\",     \"extNum\": \"\",     \"refNum\": \"\",     \"outProd\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"ctNmTtl\": \"\",     \"ctNm\": \"\",     \"ctNum\": \"\",     \"allowUpdInd\": \"\",     \"autoSRInd\": \"\",     \"enSpkr\": \"\",     \"smsLang\": \"\",     \"srInfo\": {       \"custAdr\": \"\",       \"flatCd\": \"\",       \"floorCd\": \"\",       \"srvBndy\": \"\",       \"ctNum\": \"\",       \"ctNm\": \"\",       \"ponLine\": \"\",       \"ponCvg\": \"\",       \"mdmRst\": \"\",       \"bsn\": \"\",       \"fsa\": \"\"     },     \"apptInfo\": {       \"refNum\": \"\",       \"prodTy\": \"\",       \"srvTy\": \"\",       \"areaCd\": \"\",       \"distCd\": \"\",       \"srvNum\": \"\",       \"prodId\": \"\",       \"apptTS\": {         \"apptDate\": \"\",         \"apptTmslot\": \"\",         \"apptDT\": \"\"       },       \"apptTSAry\": []     },     \"updTy\": \"\",     \"assocSubnRec\": {       \"rid\": 0,       \"custRid\": 0,       \"cusNum\": \"\",       \"acctNum\": \"\",       \"srvId\": \"\",       \"srvNum\": \"\",       \"bsn\": \"\",       \"fsa\": \"\",       \"lob\": \"\",       \"wipCust\": \"\",       \"sipSubr\": \"\",       \"netLoginId\": \"\",       \"aloneDialup\": \"\",       \"eyeGrp\": \"\",       \"ind2l2b\": \"\",       \"refFsa2l2b\": \"\",       \"indPcd\": \"\",       \"indTv\": \"\",       \"indEye\": \"\",       \"domainTy\": \"\",       \"tos\": \"\",       \"datCd\": \"\",       \"tariff\": \"\",       \"priMob\": \"\",       \"systy\": \"\",       \"alias\": \"\",       \"assoc\": \"\",       \"createTs\": \"\",       \"createPsn\": \"\",       \"lastupdTs\": \"\",       \"lastupdPsn\": \"\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     }   },   \"iApi\": \"APPT\",   \"iGwtGud\": \"722EB6B5E8694474\",   \"iMyFrm\": \"\",   \"oHgg\": \"\",   \"oNxUrl\": \"\",   \"oDuct\": null,   \"reply\": {     \"code\": \"RC_SUCC\",     \"cargo\": null   },   \"serverTS\": \"20160302161944\" }";
        //		}
        //		if (SR_AVA_TS.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return "{   \"iLoginId\": \"keith.kk.mak@pccw.com\",   \"iCustRec\": {     \"rid\": 1,     \"docTy\": \"HKID\",     \"docNum\": \"A004898(7)\",     \"premier\": \" \",     \"phylum\": \"CSUM\",     \"status\": \"A\",     \"createTs\": \"20151127162542\",     \"createPsn\": \"dldsbat\",     \"lastupdTs\": \"20151127162542\",     \"lastupdPsn\": \"dldsbat\",     \"rev\": 0   },   \"iSubnRec\": {     \"rid\": 0,     \"custRid\": 0,     \"cusNum\": \"\",     \"acctNum\": \"\",     \"srvId\": \"\",     \"srvNum\": \"\",     \"bsn\": \"\",     \"fsa\": \"\",     \"lob\": \"\",     \"wipCust\": \"\",     \"sipSubr\": \"\",     \"netLoginId\": \"\",     \"aloneDialup\": \"\",     \"eyeGrp\": \"\",     \"ind2l2b\": \"\",     \"refFsa2l2b\": \"\",     \"indPcd\": \"\",     \"indTv\": \"\",     \"indEye\": \"\",     \"domainTy\": \"\",     \"tos\": \"\",     \"datCd\": \"\",     \"tariff\": \"\",     \"priMob\": \"\",     \"systy\": \"\",     \"alias\": \"\",     \"assoc\": \"\",     \"createTs\": \"\",     \"createPsn\": \"\",     \"lastupdTs\": \"\",     \"lastupdPsn\": \"\",     \"rev\": 0,     \"instAdr\": \"\",     \"storeTy\": \"\",     \"ivr_pwd\": null,     \"auth_ph\": null,     \"acct_ty\": null   },   \"iSubnRecAry\": [],   \"iSrvReq\": {     \"srvNum\": \"\",     \"srvNumTy\": \"\",     \"reportNum\": \"\",     \"trunkNum\": \"\",     \"extNum\": \"\",     \"refNum\": \"\",     \"outProd\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"ctNmTtl\": \"\",     \"ctNm\": \"\",     \"ctNum\": \"\",     \"allowUpdInd\": \"\",     \"autoSRInd\": \"B\",     \"enSpkr\": \"\",     \"smsLang\": \"\",     \"srInfo\": {       \"custAdr\": \"FT 15 12/F YCK AM01 BLDG\nSHA TIN NEW TERRITORIES\",       \"flatCd\": \"\",       \"floorCd\": \"\",       \"srvBndy\": \"\",       \"ctNum\": \"\",       \"ctNm\": \"\",       \"ponLine\": \"N\",       \"ponCvg\": \"N\",       \"mdmRst\": \"\",       \"bsn\": \"\",       \"fsa\": \"60058422\"     },     \"apptInfo\": {       \"refNum\": \"\",       \"prodTy\": \"I\",       \"srvTy\": \"SR\",       \"areaCd\": \"NT\",       \"distCd\": \"222\",       \"srvNum\": \"20228391\",       \"prodId\": \"NU1R\",       \"apptTS\": {         \"apptDate\": \"\",         \"apptTmslot\": \"\",         \"apptDT\": \"\"       },       \"apptTSAry\": []     },     \"updTy\": \"\",     \"assocSubnRec\": {       \"rid\": 0,       \"custRid\": 0,       \"cusNum\": \"\",       \"acctNum\": \"\",       \"srvId\": \"\",       \"srvNum\": \"\",       \"bsn\": \"\",       \"fsa\": \"\",       \"lob\": \"\",       \"wipCust\": \"\",       \"sipSubr\": \"\",       \"netLoginId\": \"\",       \"aloneDialup\": \"\",       \"eyeGrp\": \"\",       \"ind2l2b\": \"\",       \"refFsa2l2b\": \"\",       \"indPcd\": \"\",       \"indTv\": \"\",       \"indEye\": \"\",       \"domainTy\": \"\",       \"tos\": \"\",       \"datCd\": \"\",       \"tariff\": \"\",       \"priMob\": \"\",       \"systy\": \"\",       \"alias\": \"\",       \"assoc\": \"\",       \"createTs\": \"\",       \"createPsn\": \"\",       \"lastupdTs\": \"\",       \"lastupdPsn\": \"\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     }   },   \"iEnqSRTy\": \"\",   \"iSRApptInfo\": {     \"refNum\": \"\",     \"prodTy\": \"I\",     \"srvTy\": \"SR\",     \"areaCd\": \"NT\",     \"distCd\": \"222\",     \"srvNum\": \"20228391\",     \"prodId\": \"NU1R\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"apptTSAry\": []   },   \"oGnrlApptAry\": [],   \"oBomApptAry\": [],   \"oSrvReqAry\": [],   \"oSRApptInfo\": {     \"refNum\": \"\",     \"prodTy\": \"\",     \"srvTy\": \"\",     \"areaCd\": \"\",     \"distCd\": \"\",     \"srvNum\": \"\",     \"prodId\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"apptTSAry\": [       {         \"apptDate\": \"20160318\",         \"apptTmslot\": \"AM\",         \"apptDT\": \"18-Mar 10:00-13:00\"       },       {         \"apptDate\": \"20160318\",         \"apptTmslot\": \"14\",         \"apptDT\": \"18-Mar 14:00-16:00\"       },       {         \"apptDate\": \"20160318\",         \"apptTmslot\": \"16\",         \"apptDT\": \"18-Mar 16:00-18:00\"       },       {         \"apptDate\": \"20160318\",         \"apptTmslot\": \"18\",         \"apptDT\": \"18-Mar 18:00-20:00\"       },       {         \"apptDate\": \"20160318\",         \"apptTmslot\": \"20\",         \"apptDT\": \"18-Mar 20:00-22:00\"       },       {         \"apptDate\": \"20160319\",         \"apptTmslot\": \"AM\",         \"apptDT\": \"19-Mar 10:00-13:00\"       },       {         \"apptDate\": \"20160319\",         \"apptTmslot\": \"14\",         \"apptDT\": \"19-Mar 14:00-16:00\"       },       {         \"apptDate\": \"20160319\",         \"apptTmslot\": \"16\",         \"apptDT\": \"19-Mar 16:00-18:00\"       },       {         \"apptDate\": \"20160319\",         \"apptTmslot\": \"18\",         \"apptDT\": \"19-Mar 18:00-20:00\"       }     ]   },   \"oPendSrvReq\": {     \"srvNum\": \"\",     \"srvNumTy\": \"\",     \"reportNum\": \"\",     \"trunkNum\": \"\",     \"extNum\": \"\",     \"refNum\": \"\",     \"outProd\": \"\",     \"apptTS\": {       \"apptDate\": \"\",       \"apptTmslot\": \"\",       \"apptDT\": \"\"     },     \"ctNmTtl\": \"\",     \"ctNm\": \"\",     \"ctNum\": \"\",     \"allowUpdInd\": \"\",     \"autoSRInd\": \"\",     \"enSpkr\": \"\",     \"smsLang\": \"\",     \"srInfo\": {       \"custAdr\": \"\",       \"flatCd\": \"\",       \"floorCd\": \"\",       \"srvBndy\": \"\",       \"ctNum\": \"\",       \"ctNm\": \"\",       \"ponLine\": \"\",       \"ponCvg\": \"\",       \"mdmRst\": \"\",       \"bsn\": \"\",       \"fsa\": \"\"     },     \"apptInfo\": {       \"refNum\": \"\",       \"prodTy\": \"\",       \"srvTy\": \"\",       \"areaCd\": \"\",       \"distCd\": \"\",       \"srvNum\": \"\",       \"prodId\": \"\",       \"apptTS\": {         \"apptDate\": \"\",         \"apptTmslot\": \"\",         \"apptDT\": \"\"       },       \"apptTSAry\": []     },     \"updTy\": \"\",     \"assocSubnRec\": {       \"rid\": 0,       \"custRid\": 0,       \"cusNum\": \"\",       \"acctNum\": \"\",       \"srvId\": \"\",       \"srvNum\": \"\",       \"bsn\": \"\",       \"fsa\": \"\",       \"lob\": \"\",       \"wipCust\": \"\",       \"sipSubr\": \"\",       \"netLoginId\": \"\",       \"aloneDialup\": \"\",       \"eyeGrp\": \"\",       \"ind2l2b\": \"\",       \"refFsa2l2b\": \"\",       \"indPcd\": \"\",       \"indTv\": \"\",       \"indEye\": \"\",       \"domainTy\": \"\",       \"tos\": \"\",       \"datCd\": \"\",       \"tariff\": \"\",       \"priMob\": \"\",       \"systy\": \"\",       \"alias\": \"\",       \"assoc\": \"\",       \"createTs\": \"\",       \"createPsn\": \"\",       \"lastupdTs\": \"\",       \"lastupdPsn\": \"\",       \"rev\": 0,       \"instAdr\": \"\",       \"storeTy\": \"\",       \"ivr_pwd\": null,       \"auth_ph\": null,       \"acct_ty\": null     }   },   \"iApi\": \"SR_AVA_TS\",   \"iGwtGud\": \"F01299F25BF44E7C\",   \"iMyFrm\": \"\",   \"oHgg\": \"\",   \"oNxUrl\": \"\",   \"oDuct\": null,   \"reply\": {     \"code\": \"RC_SUCC\",     \"cargo\": null   },   \"serverTS\": \"20160317162007\" }";
        //		}
        //				if (PLAN_TV.equalsIgnoreCase(apiRequest.getActionTy()))  {
        //					return "{\"iLoginId\":\"keith.kk.mak@pccw.com\",\"iSubnRec\":{\"rid\":44780,\"custRid\":7503,\"cusNum\":\"9500602\",\"acctNum\":\"1200124352\",\"srvId\":\"60071827\",\"srvNum\":\"1200124352\",\"bsn\":\" \",\"fsa\":\"60071827\",\"lob\":\"TV\",\"wipCust\":\"N\",\"sipSubr\":\" \",\"netLoginId\":\"a60071827\",\"aloneDialup\":\"N\",\"eyeGrp\":\" \",\"ind2l2b\":\"N\",\"refFsa2l2b\":\" \",\"indPcd\":\"Y\",\"indTv\":\"Y\",\"indEye\":\"N\",\"domainTy\":\"N\",\"tos\":\"IMS\",\"datCd\":\" \",\"tariff\":\" \",\"priMob\":\" \",\"systy\":\"IMS\",\"alias\":\" \",\"assoc\":\"Y\",\"createTs\":\"20151127164938\",\"createPsn\":\"dldsbat\",\"lastupdTs\":\"20160203101700\",\"lastupdPsn\":\"keith.kk.mak@pccw.com\",\"rev\":15,\"instAdr\":\"FT 23 234/F VP11 BLDG, KWUN TONG KOWLOON\",\"storeTy\":\"\",\"ivr_pwd\":null,\"auth_ph\":null,\"acct_ty\":null},\"oPlanAry\":[{\"srvCd\":\"\",\"fee\":\"\",\"desnEn\":\"Special Bonus Pack\",\"desnZh\":\"\",\"longDesn1En\":\"\",\"longDesn2En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2Zh\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgStDt\":\"\",\"usgEnDt\":\"\",\"unit\":\"\",\"StDt\":\"20151226\",\"EnDt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"\",\"plnSubTy\":\"\",\"cntrStDt\":\"\",\"cntrEnDt\":\"\",\"cntrDur\":\"\",\"cmmtInd\":\"N\",\"seasInd\":\"N\",\"suppInd\":\"N\",\"summInd\":\"Y\"},{\"srvCd\":\"\",\"fee\":\"\",\"desnEn\":\"Special Bonus Pack for Mass\",\"desnZh\":\"\",\"longDesn1En\":\"\",\"longDesn2En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2Zh\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgStDt\":\"\",\"usgEnDt\":\"\",\"unit\":\"\",\"StDt\":\"\",\"EnDt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"\",\"plnSubTy\":\"\",\"cntrStDt\":\"\",\"cntrEnDt\":\"\",\"cntrDur\":\"\",\"cmmtInd\":\"Y\",\"seasInd\":\"N\",\"suppInd\":\"Y\",\"summInd\":\"N\"},{\"srvCd\":\"\",\"fee\":\"\",\"desnEn\":\"24M Sports Flexi Combo\",\"desnZh\":\"\",\"longDesn1En\":\"\",\"longDesn2En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2Zh\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgStDt\":\"\",\"usgEnDt\":\"\",\"unit\":\"\",\"StDt\":\"20151226\",\"EnDt\":\"20171203\",\"srvCmmtPrd\":\"23\",\"srvPenalty\":\"5838\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"\",\"plnSubTy\":\"\",\"cntrStDt\":\"\",\"cntrEnDt\":\"\",\"cntrDur\":\"\",\"cmmtInd\":\"N\",\"seasInd\":\"N\",\"suppInd\":\"N\",\"summInd\":\"Y\"},{\"srvCd\":\"\",\"fee\":\"\",\"desnEn\":\"Sports Pack\",\"desnZh\":\"\",\"longDesn1En\":\"\",\"longDesn2En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2Zh\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgStDt\":\"\",\"usgEnDt\":\"\",\"unit\":\"\",\"StDt\":\"\",\"EnDt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"\",\"plnSubTy\":\"\",\"cntrStDt\":\"\",\"cntrEnDt\":\"\",\"cntrDur\":\"\",\"cmmtInd\":\"Y\",\"seasInd\":\"N\",\"suppInd\":\"Y\",\"summInd\":\"N\"},{\"srvCd\":\"\",\"fee\":\"\",\"desnEn\":\"Knowledge Pack\",\"desnZh\":\"\",\"longDesn1En\":\"\",\"longDesn2En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2Zh\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgStDt\":\"\",\"usgEnDt\":\"\",\"unit\":\"\",\"StDt\":\"\",\"EnDt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"\",\"plnSubTy\":\"\",\"cntrStDt\":\"\",\"cntrEnDt\":\"\",\"cntrDur\":\"\",\"cmmtInd\":\"Y\",\"seasInd\":\"N\",\"suppInd\":\"Y\",\"summInd\":\"N\"},{\"srvCd\":\"\",\"fee\":\"\",\"desnEn\":\"Junior Pack\",\"desnZh\":\"\",\"longDesn1En\":\"\",\"longDesn2En\":\"\",\"longDesn1Zh\":\"\",\"longDesn2Zh\":\"\",\"tncEn\":\"\",\"tncZh\":\"\",\"usg\":\"\",\"usgStDt\":\"\",\"usgEnDt\":\"\",\"unit\":\"\",\"StDt\":\"\",\"EnDt\":\"\",\"srvCmmtPrd\":\"\",\"srvPenalty\":\"\",\"srvCmmtRmn\":\"\",\"srvEnt\":\"\",\"rebtEn\":\"\",\"rebtZh\":\"\",\"rebtAmt\":\"\",\"monthlyTermRate\":\"\",\"m2mRate\":\"\",\"plnTy\":\"\",\"plnSubTy\":\"\",\"cntrStDt\":\"\",\"cntrEnDt\":\"\",\"cntrDur\":\"\",\"cmmtInd\":\"Y\",\"seasInd\":\"N\",\"suppInd\":\"Y\",\"summInd\":\"N\"}],\"oNowDollBal\":\"\",\"oNowDollExp\":\"\",\"oPonCvg\":false,\"iApi\":\"PLAN_TV\",\"iGwtGud\":\"5EF3C31997F04B94\",\"iMyFrm\":\"\",\"oHgg\":\"\",\"oNxUrl\":\"\",\"oDuct\":null,\"reply\":{\"code\":\"RC_SUCC\",\"cargo\":null},\"serverTS\":\"20160216123059\"}";
        //				}
        //		if (LGI.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return "{     \"iCustRec\": {         \"rid\": 0,         \"docTy\": \"\",         \"docNum\": \"\",         \"premier\": \"\",         \"phylum\": \"\",         \"status\": \"\",         \"createTs\": \"\",         \"createPsn\": \"\",         \"lastupdTs\": \"\",         \"lastupdPsn\": \"\",         \"rev\": 0     },     \"iSveeRec\": {         \"rid\": 0,         \"loginId\": \"\",         \"pwd\": \"***\",         \"nickname\": \"\",         \"ctMail\": \"\",         \"ctMob\": \"\",         \"mobAlrt\": \"\",         \"lang\": \"\",         \"secQus\": 0,         \"secAns\": \"\",         \"custRid\": 0,         \"status\": \"\",         \"state\": \"\",         \"stateUpTs\": \"\",         \"regnActn\": \"\",         \"promoOpt\": \"\",         \"pwdEff\": \"\",         \"salesChnl\": \"\",         \"teamCd\": \"\",         \"staffId\": \"\",         \"createTs\": \"\",         \"createPsn\": \"\",         \"lastupdTs\": \"\",         \"lastupdPsn\": \"\",         \"rev\": 0     },     \"iSpssRec\": {         \"rid\": 0,         \"dervRid\": 0,         \"sveeRid\": 0,         \"tcId\": \"\",         \"tcSess\": \"\",         \"devId\": \"\",         \"devTy\": \"\",         \"gni\": \"\",         \"bni\": \"\",         \"lang\": \"\",         \"lastupdTs\": \"\",         \"lastupdPsn\": \"\",         \"rev\": 0     },     \"iNaSubnRec\": {         \"rid\": 0,         \"custRid\": 0,         \"cusNum\": \"\",         \"acctNum\": \"\",         \"srvId\": \"\",         \"srvNum\": \"\",         \"bsn\": \"\",         \"fsa\": \"\",         \"lob\": \"\",         \"wipCust\": \"\",         \"sipSubr\": \"\",         \"netLoginId\": \"\",         \"aloneDialup\": \"\",         \"eyeGrp\": \"\",         \"ind2l2b\": \"\",         \"refFsa2l2b\": \"\",         \"indPcd\": \"\",         \"indTv\": \"\",         \"indEye\": \"\",         \"domainTy\": \"\",         \"tos\": \"\",         \"datCd\": \"\",         \"tariff\": \"\",         \"priMob\": \"\",         \"systy\": \"\",         \"alias\": \"\",         \"assoc\": \"\",         \"createTs\": \"\",         \"createPsn\": \"\",         \"lastupdTs\": \"\",         \"lastupdPsn\": \"\",         \"rev\": 0,         \"instAdr\": \"\",         \"storeTy\": \"\",         \"ivr_pwd\": null,         \"auth_ph\": null,         \"acct_ty\": null     },     \"iNaCra\": {         \"iApi\": \"\",         \"iGwtGud\": \"\",         \"iMyFrm\": \"\",         \"oHgg\": \"\",         \"oNxUrl\": \"\",         \"oDuct\": null,         \"reply\": {             \"code\": \"RC_SUCC\",             \"cargo\": null         },         \"serverTS\": \"\"     },     \"iOrigPwd\": \"\",     \"iRecallLgiId\": false,     \"iRecallPwd\": false,     \"oQualSvee\": {         \"custRec\": {             \"rid\": 7503,             \"docTy\": \"PASS\",             \"docNum\": \"88888891\",             \"premier\": \" \",             \"phylum\": \"CSUM\",             \"status\": \"A\",             \"createTs\": \"20151127164938\",             \"createPsn\": \"dldsbat\",             \"lastupdTs\": \"20151127164938\",             \"lastupdPsn\": \"dldsbat\",             \"rev\": 0         },         \"sveeRec\": {             \"rid\": 3,             \"loginId\": \"keith.kk.mak@pccw.com\",             \"pwd\": \"***\",             \"nickname\": \"KM12363\",             \"ctMail\": \"keith.kk.mak@pccw.com\",             \"ctMob\": \"56666669\",             \"mobAlrt\": \"Y\",             \"lang\": \"zh\",             \"secQus\": 0,             \"secAns\": \"\",             \"custRid\": 7503,             \"status\": \"A\",             \"state\": \"A\",             \"stateUpTs\": \"20151130103824\",             \"regnActn\": \"REGUSER\",             \"promoOpt\": \"N\",             \"pwdEff\": \"Y\",             \"salesChnl\": \" \",             \"teamCd\": \" \",             \"staffId\": \" \",             \"createTs\": \"20151130103801\",             \"createPsn\": \"!UNKN\",             \"lastupdTs\": \"2016021281154480226\",             \"lastupdPsn\": \"keith.kk.mak@pccw.com\",             \"rev\": 889         },         \"bcifRec\": {             \"custRid\": 0,             \"custNm\": \"\",             \"industry\": \"\",             \"nuStaff\": \"\",             \"nuPresence\": \"\",             \"xborder\": \"\",             \"priCtName\": \"\",             \"priCtTitle\": \"\",             \"priCtMail\": \"\",             \"priCtJobtitle\": \"\",             \"priCtTel\": \"\",             \"priCtMob\": \"\",             \"secCtName\": \"\",             \"secCtTitle\": \"\",             \"secCtMail\": \"\",             \"secCtJobtitle\": \"\",             \"secCtTel\": \"\",             \"secCtMob\": \"\",             \"createTs\": \"\",             \"createPsn\": \"\",             \"lastupdTs\": \"\",             \"lastupdPsn\": \"\",             \"rev\": 0         },         \"subnRecAry\": [             {                 \"rid\": 1,                 \"custRid\": 7503,                 \"cusNum\": \"10004631\",                 \"acctNum\": \"73001278660919\",                 \"srvId\": \"100121382\",                 \"srvNum\": \"11111111\",                 \"bsn\": \" \",                 \"fsa\": \" \",                 \"lob\": \"LTS\",                 \"wipCust\": \"N\",                 \"sipSubr\": \"N\",                 \"netLoginId\": \" \",                 \"aloneDialup\": \" \",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \" \",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"indEye\": \" \",                 \"domainTy\": \" \",                 \"tos\": \"MOB\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"MOB\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127162542\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 16,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             }, 			{                 \"rid\": 1,                 \"custRid\": 7503,                 \"cusNum\": \"10004631\",                 \"acctNum\": \"73001278660919\",                 \"srvId\": \"100121382\",                 \"srvNum\": \"22222222\",                 \"bsn\": \" \",                 \"fsa\": \" \",                 \"lob\": \"LTS\",                 \"wipCust\": \"N\",                 \"sipSubr\": \"N\",                 \"netLoginId\": \" \",                 \"aloneDialup\": \" \",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \" \",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"indEye\": \" \",                 \"domainTy\": \" \",                 \"tos\": \"ITS\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"MOB\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127162542\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 16,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             }, 			{                 \"rid\": 1,                 \"custRid\": 7503,                 \"cusNum\": \"10004631\",                 \"acctNum\": \"73001278660919\",                 \"srvId\": \"100121382\",                 \"srvNum\": \"33333333\",                 \"bsn\": \" \",                 \"fsa\": \" \",                 \"lob\": \"LTS\",                 \"wipCust\": \"N\",                 \"sipSubr\": \"N\",                 \"netLoginId\": \" \",                 \"aloneDialup\": \" \",                 \"eyeGrp\": \"00000000\",                 \"ind2l2b\": \" \",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"indEye\": \" \",                 \"domainTy\": \" \",                 \"tos\": \"TEL\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"MOB\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127162542\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 16,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             }, 			{                 \"rid\": 1,                 \"custRid\": 7503,                 \"cusNum\": \"10004631\",                 \"acctNum\": \"73001278660919\",                 \"srvId\": \"100121382\",                 \"srvNum\": \"44444444\",                 \"bsn\": \" \",                 \"fsa\": \" \",                 \"lob\": \"LTS\",                 \"wipCust\": \"N\",                 \"sipSubr\": \"N\",                 \"netLoginId\": \" \",                 \"aloneDialup\": \" \",                 \"eyeGrp\": \"\",                 \"ind2l2b\": \" \",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"indEye\": \" \",                 \"domainTy\": \" \",                 \"tos\": \"TEL\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \"O\",                 \"systy\": \"MOB\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127162542\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 16,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             }, 			{                 \"rid\": 1,                 \"custRid\": 7503,                 \"cusNum\": \"10004631\",                 \"acctNum\": \"73001278660919\",                 \"srvId\": \"100121382\",                 \"srvNum\": \"55555555\",                 \"bsn\": \" \",                 \"fsa\": \" \",                 \"lob\": \"LTS\",                 \"wipCust\": \"N\",                 \"sipSubr\": \"N\",                 \"netLoginId\": \" \",                 \"aloneDialup\": \" \",                 \"eyeGrp\": \"\",                 \"ind2l2b\": \" \",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"indEye\": \" \",                 \"domainTy\": \" \",                 \"tos\": \"TEL\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \"I\",                 \"systy\": \"MOB\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127162542\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 16,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             }, 			{                 \"rid\": 1,                 \"custRid\": 7503,                 \"cusNum\": \"10004631\",                 \"acctNum\": \"73001278660919\",                 \"srvId\": \"100121382\",                 \"srvNum\": \"55555555\",                 \"bsn\": \" \",                 \"fsa\": \" \",                 \"lob\": \"LTS\",                 \"wipCust\": \"N\",                 \"sipSubr\": \"N\",                 \"netLoginId\": \" \",                 \"aloneDialup\": \" \",                 \"eyeGrp\": \"\",                 \"ind2l2b\": \" \",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"indEye\": \" \",                 \"domainTy\": \" \",                 \"tos\": \"TEL\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \"\",                 \"systy\": \"MOB\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127162542\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 16,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             },             {                 \"rid\": 87622,                 \"custRid\": 7503,                 \"cusNum\": \" \",                 \"acctNum\": \"05054861\",                 \"srvId\": \"60207076\",                 \"srvNum\": \"60207076\",                 \"bsn\": \" \",                 \"fsa\": \" \",                 \"lob\": \"O2F\",                 \"wipCust\": \" \",                 \"sipSubr\": \" \",                 \"netLoginId\": \" \",                 \"aloneDialup\": \" \",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \" \",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"indEye\": \" \",                 \"domainTy\": \" \",                 \"tos\": \"CSL\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"CSL\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151130170218\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"20160216141244\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 16,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             },             {                 \"rid\": 44775,                 \"custRid\": 7503,                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200094879\",                 \"srvId\": \"60070059\",                 \"srvNum\": \"b60070059\",                 \"bsn\": \" \",                 \"fsa\": \"60070059\",                 \"lob\": \"PCD\",                 \"wipCust\": \"N\",                 \"sipSubr\": \" \",                 \"netLoginId\": \"b60070059\",                 \"aloneDialup\": \"N\",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \"N\",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \"Y\",                 \"indTv\": \"N\",                 \"indEye\": \"N\",                 \"domainTy\": \"N\",                 \"tos\": \"IMS\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"IMS\",                 \"alias\": \"TESTING\",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127164938\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 45,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             },             {                 \"rid\": 44776,                 \"custRid\": 7503,                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200101993\",                 \"srvId\": \"60069429\",                 \"srvNum\": \"a60069429\",                 \"bsn\": \" \",                 \"fsa\": \"60069429\",                 \"lob\": \"PCD\",                 \"wipCust\": \"N\",                 \"sipSubr\": \" \",                 \"netLoginId\": \"a60069429\",                 \"aloneDialup\": \"N\",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \"N\",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \"Y\",                 \"indTv\": \"N\",                 \"indEye\": \"N\",                 \"domainTy\": \"N\",                 \"tos\": \"IMS\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"IMS\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127164938\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 1637,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             },             {                 \"rid\": 44777,                 \"custRid\": 7503,                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200102006\",                 \"srvId\": \"60069005\",                 \"srvNum\": \"a60069005\",                 \"bsn\": \" \",                 \"fsa\": \"60069005\",                 \"lob\": \"PCD\",                 \"wipCust\": \"N\",                 \"sipSubr\": \" \",                 \"netLoginId\": \"a60069005\",                 \"aloneDialup\": \"N\",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \"N\",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \"Y\",                 \"indTv\": \"N\",                 \"indEye\": \"N\",                 \"domainTy\": \"N\",                 \"tos\": \"IMS\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"IMS\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127164938\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 1637,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             },             {                 \"rid\": 44778,                 \"custRid\": 7503,                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200121942\",                 \"srvId\": \"60069009\",                 \"srvNum\": \"a60069009\",                 \"bsn\": \" \",                 \"fsa\": \"60069009\",                 \"lob\": \"PCD\",                 \"wipCust\": \"N\",                 \"sipSubr\": \" \",                 \"netLoginId\": \"a60069009\",                 \"aloneDialup\": \"N\",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \"N\",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \"Y\",                 \"indTv\": \"N\",                 \"indEye\": \"N\",                 \"domainTy\": \"N\",                 \"tos\": \"IMS\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"IMS\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127164938\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 1637,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             },             {                 \"rid\": 44779,                 \"custRid\": 7503,                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200124349\",                 \"srvId\": \"60071827\",                 \"srvNum\": \"a60071827\",                 \"bsn\": \" \",                 \"fsa\": \"60071827\",                 \"lob\": \"PCD\",                 \"wipCust\": \"N\",                 \"sipSubr\": \" \",                 \"netLoginId\": \"a60071827\",                 \"aloneDialup\": \"N\",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \"N\",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \"Y\",                 \"indTv\": \"Y\",                 \"indEye\": \"N\",                 \"domainTy\": \"N\",                 \"tos\": \"IMS\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"IMS\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127164938\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 1637,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             },             {                 \"rid\": 44780,                 \"custRid\": 7503,                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200124352\",                 \"srvId\": \"60071827\",                 \"srvNum\": \"1200124352\",                 \"bsn\": \" \",                 \"fsa\": \"60071827\",                 \"lob\": \"TV\",                 \"wipCust\": \"N\",                 \"sipSubr\": \" \",                 \"netLoginId\": \"a60071827\",                 \"aloneDialup\": \"N\",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \"N\",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \"Y\",                 \"indTv\": \"Y\",                 \"indEye\": \"N\",                 \"domainTy\": \"N\",                 \"tos\": \"IMS\",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"IMS\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20151127164938\",                 \"createPsn\": \"dldsbat\",                 \"lastupdTs\": \"201602161412448180226\",                 \"lastupdPsn\": \"keith.kk.mak@pccw.com\",                 \"rev\": 1637,                 \"instAdr\": \"FT 23 234/F VP11 BLDG, KWUN TONG KOWLOON\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             }         ],         \"zmSubnRecAry\": [             {                 \"rid\": 87705,                 \"custRid\": 7503,                 \"cusNum\": \" \",                 \"acctNum\": \"05054861\",                 \"srvId\": \"~ZOMBIE\",                 \"srvNum\": \" \",                 \"bsn\": \" \",                 \"fsa\": \" \",                 \"lob\": \"O2F\",                 \"wipCust\": \"N\",                 \"sipSubr\": \"N\",                 \"netLoginId\": \" \",                 \"aloneDialup\": \" \",                 \"eyeGrp\": \" \",                 \"ind2l2b\": \" \",                 \"refFsa2l2b\": \" \",                 \"indPcd\": \" \",                 \"indTv\": \" \",                 \"indEye\": \" \",                 \"domainTy\": \" \",                 \"tos\": \" \",                 \"datCd\": \" \",                 \"tariff\": \" \",                 \"priMob\": \" \",                 \"systy\": \"CSL\",                 \"alias\": \" \",                 \"assoc\": \"Y\",                 \"createTs\": \"20160201113243\",                 \"createPsn\": \"KM\",                 \"lastupdTs\": \"20160201113243\",                 \"lastupdPsn\": \"KM\",                 \"rev\": 0,                 \"instAdr\": \"\",                 \"storeTy\": \"\",                 \"ivr_pwd\": null,                 \"auth_ph\": null,                 \"acct_ty\": null             }         ]     },     \"oAcctAry\": [         {             \"custRid\": 7503,             \"lob\": \"MOB\",             \"cusNum\": \"10004631\",             \"acctNum\": \"73001278660919\",             \"live\": true,             \"sysTy\": \"MOB\"         }     ],     \"oBomCustAry\": [         {             \"lob\": \"O2F\",             \"cusNum\": \" \",             \"sysTy\": \"CSL\",             \"acctNum\": \"05054861\"         },         {             \"lob\": \"PCD\",             \"cusNum\": \"9500602\",             \"sysTy\": \"IMS\",             \"acctNum\": \"1200094879\"         },         {             \"lob\": \"MOB\",             \"cusNum\": \"10004631\",             \"sysTy\": \"MOB\",             \"acctNum\": \"73001278660919\"         }     ],     \"oSrvReqAry\": [],     \"oGnrlApptAry\": [],     \"oBillListAry\": [         {             \"iAcct\": {                 \"custRid\": 7503,                 \"lob\": \"MOB\",                 \"cusNum\": \"10004631\",                 \"acctNum\": \"73001278660919\",                 \"live\": true,                 \"sysTy\": \"MOB\"             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"custRid\": 7503,                 \"lob\": \"O2F\",                 \"cusNum\": \" \",                 \"acctNum\": \"05054861\",                 \"live\": true,                 \"sysTy\": \"CSL\"             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"custRid\": 7503,                 \"lob\": \"PCD\",                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200094879\",                 \"live\": true,                 \"sysTy\": \"IMS\"             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"custRid\": 7503,                 \"lob\": \"PCD\",                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200101993\",                 \"live\": true,                 \"sysTy\": \"IMS\"             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"custRid\": 7503,                 \"lob\": \"PCD\",                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200102006\",                 \"live\": true,                 \"sysTy\": \"IMS\"             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"custRid\": 7503,                 \"lob\": \"PCD\",                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200121942\",                 \"live\": true,                 \"sysTy\": \"IMS\"             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"custRid\": 7503,                 \"lob\": \"PCD\",                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200124349\",                 \"live\": true,                 \"sysTy\": \"IMS\"             },             \"oBillAry\": []         },         {             \"iAcct\": {                 \"custRid\": 7503,                 \"lob\": \"TV\",                 \"cusNum\": \"9500602\",                 \"acctNum\": \"1200124352\",                 \"live\": true,                 \"sysTy\": \"IMS\"             },             \"oBillAry\": []         }     ],     \"oChatTok\": \"e970e74c85bd4844ffd37ebdc44e8f496ad960a58043bc0df64fe09c1de35bb857c3063a53e3494140be3a2f99b10d4fbed0b9cb13c99bc3f6806\",     \"oSoGud\": \"\",     \"oTaSubnRec\": {         \"rid\": 0,         \"custRid\": 0,         \"cusNum\": \"\",         \"acctNum\": \"\",         \"srvId\": \"\",         \"srvNum\": \"\",         \"bsn\": \"\",         \"fsa\": \"\",         \"lob\": \"\",         \"wipCust\": \"\",         \"sipSubr\": \"\",         \"netLoginId\": \"\",         \"aloneDialup\": \"\",         \"eyeGrp\": \"\",         \"ind2l2b\": \"\",         \"refFsa2l2b\": \"\",         \"indPcd\": \"\",         \"indTv\": \"\",         \"indEye\": \"\",         \"domainTy\": \"\",         \"tos\": \"\",         \"datCd\": \"\",         \"tariff\": \"\",         \"priMob\": \"\",         \"systy\": \"\",         \"alias\": \"\",         \"assoc\": \"\",         \"createTs\": \"\",         \"createPsn\": \"\",         \"lastupdTs\": \"\",         \"lastupdPsn\": \"\",         \"rev\": 0,         \"instAdr\": \"\",         \"storeTy\": \"\",         \"ivr_pwd\": null,         \"auth_ph\": null,         \"acct_ty\": null     },     \"oTaCra\": {         \"iApi\": \"\",         \"iGwtGud\": \"\",         \"iMyFrm\": \"\",         \"oHgg\": \"\",         \"oNxUrl\": \"\",         \"oDuct\": null,         \"reply\": {             \"code\": \"RC_SUCC\",             \"cargo\": null         },         \"serverTS\": \"\"     },     \"iApi\": \"\",     \"iGwtGud\": \"\",     \"iMyFrm\": \"\",     \"oHgg\": \"\",     \"oNxUrl\": \"\",     \"oDuct\": null,     \"reply\": {         \"code\": \"RC_SUCC\",         \"cargo\": null     },     \"serverTS\": \"201602011422121046\" }";
        //		}
        //		if (BILL.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			//MOB
        //			if (result.contains("MOB"))
        //				return "{     \"serverTS\": \"20160131124938\",     \"iSubnRec\": {         \"rev\": 14,         \"createTs\": \"20151127162542\",         \"netLoginId\": \" \",         \"custRid\": 7503,         \"systy\": \"MOB\",         \"instAdr\": \"\",         \"eyeGrp\": \" \",         \"sipSubr\": \"N\",         \"wipCust\": \"N\",         \"storeTy\": \"\",         \"lastupdTs\": \"20160118183735\",         \"datCd\": \" \",         \"indTv\": \" \",         \"lastupdPsn\": \"keith.kk.mak@pccw.com\",         \"lob\": \"MOB\",         \"tos\": \"3G\",         \"indPcd\": \" \",         \"auth_ph\": null,         \"tariff\": \" \",         \"indEye\": \" \",         \"createPsn\": \"dldsbat\",         \"cusNum\": \"10004631\",         \"assoc\": \"Y\",         \"fsa\": \" \",         \"acct_ty\": null,         \"alias\": \" \",         \"acctNum\": \"73001278660919\",         \"ind2l2b\": \" \",         \"domainTy\": \" \",         \"srvNum\": \"98238921\",         \"bsn\": \" \",         \"rid\": 1,         \"refFsa2l2b\": \" \",         \"srvId\": \"100121382\",         \"priMob\": \" \",         \"aloneDialup\": \" \",         \"ivr_pwd\": null     },     \"iGwtGud\": \"D6AE202E755E4D4E\",     \"oDuct\": null,     \"iLoginId\": \"keith.kk.mak@pccw.com\",     \"oBillPreview\": {         \"billMsg\": null,         \"loginID\": null,         \"previewItems\": null,         \"acctNum\": null,         \"billDate\": null,         \"currentBalance\": null,         \"billAmount\": null,         \"isDate\": null,         \"noBillFlg\": false,         \"displayBillDate\": null,         \"noBillMsgFlg\": false,         \"displayDueDate\": null,         \"dueDate\": null,         \"isTos\": null,         \"receiptAcctNo\": null     },     \"oG3ServiceCallBarStatusWS\": {         \"rtnMsg\": null,         \"g3ServiceCallBarStatusDTO\": {             \"g3AccountInformationDTO\": {                 \"pendingRequestIndicator\": null,                 \"paymentMethod\": null,                 \"accountNumber\": null,                 \"accountCreditStatus\": null             },             \"g3SubExtInfoDTO\": {                 \"statusReaCD\": null,                 \"resultCode\": null,                 \"srvStatus\": null,                 \"errorMsg\": null,                 \"busTxnType\": null,                 \"txnSeqNum\": null             },             \"serviceStatusChi\": null,             \"alertMessageEng\": null,             \"hasAlertMessage\": null,             \"serviceStatusEng\": null,             \"g3PaymentRejectStatusDTO\": {                 \"errCode\": null,                 \"resultRefCode\": null,                 \"errMsg\": null,                 \"paymentRejSts\": null,                 \"acctNum\": null             },             \"enableReleaseCallBar\": null,             \"alertMessageChi\": null,             \"alertLevel\": null,             \"releaseCallBarPending\": null,             \"callBarred\": null,             \"showServiceStatus\": null         },         \"rtnCd\": null     },     \"oBillList\": {         \"oBillAry\": [                     ],         \"iAcct\": {             \"custRid\": 0,             \"lob\": \"\",             \"sysTy\": \"\",             \"cusNum\": \"\",             \"live\": false,             \"acctNum\": \"\"         }     },     \"iBillDate\": \"\",     \"oNxUrl\": \"\",     \"iMyFrm\": \"\",     \"oSummUrl\": \"\",     \"oHgg\": \"\",     \"iAcct\": {         \"custRid\": 7503,         \"lob\": \"MOB\",         \"sysTy\": \"MOB\",         \"cusNum\": \"10004631\",         \"live\": true,         \"acctNum\": \"73001278660919\"     },     \"reply\": {         \"cargo\": null,         \"code\": \"RC_SUCC\"     },     \"iApi\": \"BILL\",     \"oG3OutstandingBalanceDTO\": {         \"contractEndDate\": null,         \"totalOSBal\": 8.0,         \"pymtDueDate\": \"Jan 9, 2016 12:00:00 AM\",         \"lastBillDate\": \"Jan 9, 2016 12:00:00 AM\",         \"currOSBal\": 18.0,         \"overdueAmt\": 0.0,         \"acctNum\": null     } }";
        //			//PCD
        //			if (result.contains("TV") || result.contains("PCD"))
        //				return "{     \"serverTS\": \"20160131213055\",     \"iSubnRec\": {         \"rev\": 14,         \"createTs\": \"20151127164938\",         \"netLoginId\": \"a60071827\",         \"custRid\": 7503,         \"systy\": \"IMS\",         \"instAdr\": \"FT 23 234F VP11 BLDG, KWUN TONG KOWLOON\",         \"eyeGrp\": \" \",         \"sipSubr\": \" \",         \"wipCust\": \"N\",         \"storeTy\": \"\",         \"lastupdTs\": \"20160118183736\",         \"datCd\": \" \",         \"indTv\": \"Y\",         \"lastupdPsn\": \"keith.kk.mak@pccw.com\",         \"lob\": \"TV\",         \"tos\": \"IMS\",         \"indPcd\": \"Y\",         \"auth_ph\": null,         \"tariff\": \" \",         \"indEye\": \"N\",         \"createPsn\": \"dldsbat\",         \"cusNum\": \"9500602\",         \"assoc\": \"Y\",         \"fsa\": \"60071827\",         \"acct_ty\": null,         \"alias\": \" \",         \"acctNum\": \"1200124352\",         \"ind2l2b\": \"N\",         \"domainTy\": \"N\",         \"srvNum\": \"1200124352\",         \"bsn\": \" \",         \"rid\": 44780,         \"refFsa2l2b\": \" \",         \"srvId\": \"60071827\",         \"priMob\": \" \",         \"aloneDialup\": \"N\",         \"ivr_pwd\": null     },     \"iGwtGud\": \"347E7293487D4E58\",     \"oDuct\": null,     \"iLoginId\": \"keith.kk.mak@pccw.com\",     \"oBillPreview\": {         \"billMsg\": null,         \"loginID\": null,         \"previewItems\": null,         \"acctNum\": null,         \"billDate\": null,         \"currentBalance\": \"11.9\",         \"billAmount\": \"10.5\",         \"isDate\": null,         \"noBillFlg\": false,         \"displayBillDate\": null,         \"noBillMsgFlg\": false,         \"displayDueDate\": null,         \"dueDate\": \"01-MAY-2016\",         \"isTos\": null,         \"receiptAcctNo\": \"12345678901234\"     },     \"oG3ServiceCallBarStatusWS\": {         \"rtnMsg\": null,         \"g3ServiceCallBarStatusDTO\": {             \"g3AccountInformationDTO\": {                 \"pendingRequestIndicator\": null,                 \"paymentMethod\": null,                 \"accountNumber\": null,                 \"accountCreditStatus\": null             },             \"g3SubExtInfoDTO\": {                 \"statusReaCD\": null,                 \"resultCode\": null,                 \"srvStatus\": null,                 \"errorMsg\": null,                 \"busTxnType\": null,                 \"txnSeqNum\": null             },             \"serviceStatusChi\": null,             \"alertMessageEng\": null,             \"hasAlertMessage\": null,             \"serviceStatusEng\": null,             \"g3PaymentRejectStatusDTO\": {                 \"errCode\": null,                 \"resultRefCode\": null,                 \"errMsg\": null,                 \"paymentRejSts\": null,                 \"acctNum\": null             },             \"enableReleaseCallBar\": null,             \"alertMessageChi\": null,             \"alertLevel\": null,             \"releaseCallBarPending\": null,             \"callBarred\": null,             \"showServiceStatus\": null         },         \"rtnCd\": null     },     \"oBillList\": {         \"oBillAry\": [                     ],         \"iAcct\": {             \"custRid\": 0,             \"lob\": \"\",             \"sysTy\": \"\",             \"cusNum\": \"\",             \"live\": false,             \"acctNum\": \"\"         }     },     \"iBillDate\": \"\",     \"oNxUrl\": \"\",     \"iMyFrm\": \"\",     \"oSummUrl\": \"\",     \"oHgg\": \"\",     \"iAcct\": {         \"custRid\": 7503,         \"lob\": \"TV\",         \"sysTy\": \"IMS\",         \"cusNum\": \"9500602\",         \"live\": true,         \"acctNum\": \"1200124352\"     },     \"reply\": {         \"cargo\": null,         \"code\": \"RC_SUCC\"     },     \"iApi\": \"BILL\",     \"oG3OutstandingBalanceDTO\": {         \"contractEndDate\": null,         \"totalOSBal\": 0,         \"pymtDueDate\": null,         \"lastBillDate\": null,         \"currOSBal\": 0,         \"overdueAmt\": 0,         \"acctNum\": null     } }";
        //			///101 / O2F
        //			if (result.contains("101") || result.contains("O2F"))
        //			//						    result = "{     \"serverTS\": \"20160131233511\",     \"iSubnRec\": {         \"rev\": 14,         \"createTs\": \"20151130170218\",         \"netLoginId\": \" \",         \"custRid\": 7503,         \"systy\": \"CSL\",         \"instAdr\": \"\",         \"eyeGrp\": \" \",         \"sipSubr\": \" \",         \"wipCust\": \" \",         \"storeTy\": \"\",         \"lastupdTs\": \"20160118183735\",         \"datCd\": \" \",         \"indTv\": \" \",         \"lastupdPsn\": \"keith.kk.mak@pccw.com\",         \"lob\": \"O2F\",         \"tos\": \"CSL\",         \"indPcd\": \" \",         \"auth_ph\": null,         \"tariff\": \" \",         \"indEye\": \" \",         \"createPsn\": \"dldsbat\",         \"cusNum\": \" \",         \"assoc\": \"Y\",         \"fsa\": \" \",         \"acct_ty\": null,         \"alias\": \" \",         \"acctNum\": \"05054861\",         \"ind2l2b\": \" \",         \"domainTy\": \" \",         \"srvNum\": \"60207076\",         \"bsn\": \" \",         \"rid\": 87622,         \"refFsa2l2b\": \" \",         \"srvId\": \"60207076\",         \"priMob\": \" \",         \"aloneDialup\": \" \",         \"ivr_pwd\": null     },     \"iGwtGud\": \"41AE494E970743A1\",     \"oDuct\": null,     \"iLoginId\": \"keith.kk.mak@pccw.com\",     \"oBillPreview\": {         \"billMsg\": null,         \"loginID\": null,         \"previewItems\": null,         \"acctNum\": null,         \"billDate\": null,         \"currentBalance\": null,         \"billAmount\": null,         \"isDate\": null,         \"noBillFlg\": false,         \"displayBillDate\": null,         \"noBillMsgFlg\": false,         \"displayDueDate\": null,         \"dueDate\": null,         \"isTos\": null,         \"receiptAcctNo\": null     },     \"oG3ServiceCallBarStatusWS\": {         \"rtnMsg\": null,         \"g3ServiceCallBarStatusDTO\": {             \"g3AccountInformationDTO\": {                 \"pendingRequestIndicator\": null,                 \"paymentMethod\": null,                 \"accountNumber\": null,                 \"accountCreditStatus\": null             },             \"g3SubExtInfoDTO\": {                 \"statusReaCD\": null,                 \"resultCode\": null,                 \"srvStatus\": null,                 \"errorMsg\": null,                 \"busTxnType\": null,                 \"txnSeqNum\": null             },             \"serviceStatusChi\": null,             \"alertMessageEng\": null,             \"hasAlertMessage\": null,             \"serviceStatusEng\": null,             \"g3PaymentRejectStatusDTO\": {                 \"errCode\": null,                 \"resultRefCode\": null,                 \"errMsg\": null,                 \"paymentRejSts\": null,                 \"acctNum\": null             },             \"enableReleaseCallBar\": null,             \"alertMessageChi\": null,             \"alertLevel\": null,             \"releaseCallBarPending\": null,             \"callBarred\": null,             \"showServiceStatus\": null         },         \"rtnCd\": null     },     \"oBillList\": {         \"oBillAry\": [                     ],         \"iAcct\": {             \"custRid\": 0,             \"lob\": \"\",             \"sysTy\": \"\",             \"cusNum\": \"\",             \"live\": false,             \"acctNum\": \"\"         }     },     \"iBillDate\": \"\",     \"oNxUrl\": \"\",     \"iMyFrm\": \"\",     \"oSummUrl\": \"\",     \"oHgg\": \"\",     \"iAcct\": {         \"custRid\": 7503,         \"lob\": \"O2F\",         \"sysTy\": \"CSL\",         \"cusNum\": \" \",         \"live\": true,         \"acctNum\": \"05054861\"     },     \"reply\": {         \"cargo\": null,         \"code\": \"RC_SUCC\"     },     \"iApi\": \"BILL\",     \"oG3OutstandingBalanceDTO\": {         \"contractEndDate\": null,         \"totalOSBal\": 28.7,         \"pymtDueDate\": \"2016-04-16 10:15:22.5\",         \"lastBillDate\": null,         \"currOSBal\": 32.0,         \"overdueAmt\": 12.8,         \"acctNum\": null     } }";
        //				return "{     \"serverTS\": \"20160131233511\",     \"iSubnRec\": {         \"rev\": 14,         \"createTs\": \"20151130170218\",         \"netLoginId\": \" \",         \"custRid\": 7503,         \"systy\": \"CSL\",         \"instAdr\": \"\",         \"eyeGrp\": \" \",         \"sipSubr\": \" \",         \"wipCust\": \" \",         \"storeTy\": \"\",         \"lastupdTs\": \"20160118183735\",         \"datCd\": \" \",         \"indTv\": \" \",         \"lastupdPsn\": \"keith.kk.mak@pccw.com\",         \"lob\": \"O2F\",         \"tos\": \"CSL\",         \"indPcd\": \" \",         \"auth_ph\": null,         \"tariff\": \" \",         \"indEye\": \" \",         \"createPsn\": \"dldsbat\",         \"cusNum\": \" \",         \"assoc\": \"Y\",         \"fsa\": \" \",         \"acct_ty\": null,         \"alias\": \" \",         \"acctNum\": \"05054861\",         \"ind2l2b\": \" \",         \"domainTy\": \" \",         \"srvNum\": \"60207076\",         \"bsn\": \" \",         \"rid\": 87622,         \"refFsa2l2b\": \" \",         \"srvId\": \"60207076\",         \"priMob\": \" \",         \"aloneDialup\": \" \",         \"ivr_pwd\": null     },     \"iGwtGud\": \"41AE494E970743A1\",     \"oDuct\": null,     \"iLoginId\": \"keith.kk.mak@pccw.com\",     \"oBillPreview\": {         \"billMsg\": null,         \"loginID\": null,         \"previewItems\": null,         \"acctNum\": null,         \"billDate\": null,         \"currentBalance\": null,         \"billAmount\": null,         \"isDate\": null,         \"noBillFlg\": false,         \"displayBillDate\": null,         \"noBillMsgFlg\": false,         \"displayDueDate\": null,         \"dueDate\": null,         \"isTos\": null,         \"receiptAcctNo\": null     },     \"oG3ServiceCallBarStatusWS\": {         \"rtnMsg\": null,         \"g3ServiceCallBarStatusDTO\": {             \"g3AccountInformationDTO\": {                 \"pendingRequestIndicator\": null,                 \"paymentMethod\": null,                 \"accountNumber\": null,                 \"accountCreditStatus\": null             },             \"g3SubExtInfoDTO\": {                 \"statusReaCD\": null,                 \"resultCode\": null,                 \"srvStatus\": null,                 \"errorMsg\": null,                 \"busTxnType\": null,                 \"txnSeqNum\": null             },             \"serviceStatusChi\": null,             \"alertMessageEng\": null,             \"hasAlertMessage\": null,             \"serviceStatusEng\": null,             \"g3PaymentRejectStatusDTO\": {                 \"errCode\": null,                 \"resultRefCode\": null,                 \"errMsg\": null,                 \"paymentRejSts\": null,                 \"acctNum\": null             },             \"enableReleaseCallBar\": null,             \"alertMessageChi\": null,             \"alertLevel\": null,             \"releaseCallBarPending\": null,             \"callBarred\": null,             \"showServiceStatus\": null         },         \"rtnCd\": null     },     \"oBillList\": {         \"oBillAry\": [                     ],         \"iAcct\": {             \"custRid\": 0,             \"lob\": \"\",             \"sysTy\": \"\",             \"cusNum\": \"\",             \"live\": false,             \"acctNum\": \"\"         }     },     \"iBillDate\": \"\",     \"oNxUrl\": \"\",     \"iMyFrm\": \"\",     \"oSummUrl\": \"\",     \"oHgg\": \"\",     \"iAcct\": {         \"custRid\": 7503,         \"lob\": \"O2F\",         \"sysTy\": \"CSL\",         \"cusNum\": \" \",         \"live\": true,         \"acctNum\": \"05054861\"     },     \"reply\": {         \"cargo\": null,         \"code\": \"RC_SUCC\"     },     \"iApi\": \"BILL\",     \"oG3OutstandingBalanceDTO\": {         \"contractEndDate\": null,         \"totalOSBal\": 28.7,         \"pymtDueDate\": \"Jan 9, 2016 12:00:00 AM\",         \"lastBillDate\": null,         \"currOSBal\": 32.0,         \"overdueAmt\": 12.8,         \"acctNum\": null     } }";
        //		}
        //		Log.i("APISManager", (int)Math.ceil((double)result.length()/(double)2000) +"/" + result.length());
        //		for (int i=0; i < (int)Math.ceil((double)result.length()/(double)2000); i++) {
        //			String msg ;
        //			int len = result.length()-1;
        //			Log.i("APISManager", 2000*i +"/" + len);
        //			if (i == (int)Math.ceil((double)result.length()/(double)2000) -1) {
        //				msg = result.substring(2000*i, result.length()-1);
        //			} else {
        //				msg = result.substring(2000*i, 2000*(i+1));
        //			}
        //			Log.i("APISManager", msg);
        //		}

        int maxLogSize = 1000;
        for (int i = 0; i <= result.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > result.length() ? result.length() : end;
            if (debug) Log.v("APISManager", result.substring(start, end));
        }
        return result;
    }

    public static void forceCloseProgressDialog(Context cxt) {
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.dismissProgressDialog(true);
    }


    public static final <T extends BaseCraEx> void doHelo(final Activity activity) {
        Context cxt = activity;
        final AQuery aq = new AQuery(activity);
        // doHelo first
        final String TAGNAME = HELO;
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/helo";
        HeloCra heloCra = new HeloCra();
        heloCra.setIApi(HELO);

        //including spssCra on every dohelo
        SpssCra spssCra = new SpssCra();
        spssCra.setICkSum(Utils.sha256(ClnEnv.getPref(activity.getApplicationContext(), activity.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt)));

        if (ClnEnv.isLoggedIn()) {
            spssCra.setISpssRec(ClnEnv.getLgiCra().getISpssRec());
            spssCra.setILoginId(ClnEnv.getLoginId());
        } else {
            //if not doing login or save login
            spssCra.getISpssRec().devTy = SpssRec.TY_ANDROID;
            spssCra.getISpssRec().devId = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GCM_REGID), "");
            //				spssCra.getISpssRec().devId = ClnEnv.getDeviceID(activity);
            spssCra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
            spssCra.getISpssRec().gni = ClnEnv.getPref(activity.getApplicationContext(), activity.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
            //				spssCra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";
        }
        heloCra.setISpssCra(spssCra);

        String jstr = gson.toJson(heloCra);
        ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
        if (debug) Log.d(TAGNAME, String.format("%s %s", TAGNAME, "request sent:") + jstr);

        try {
            // 1st request
            //			progressManager.showProgressDialog(activity);
            // 1st call doHelo
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(AQuery.POST_ENTITY, entity);

            aq.ajax(url, params, JSONObject.class, new AjaxCallback<JSONObject>() {

                OnAPIsListener callback;
                APIsResponse response;
                HeloCra cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    Boolean isFailCase = false;
                    mCookies = status.getCookies();
                    try {
                        try {
                            callback = (OnAPIsListener) activity;
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
                        }

                        response = new APIsResponse();
                        response.setActionTy(HELO);
                        cra = new HeloCra();
                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else if (json == null) {
                            // TODO: httppost response null Object from api
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            String resultStr = json.toString();
                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Log.d(TAGNAME, String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                Type collectionType = new TypeToken<HeloCra>() {
                                }.getType();
                                cra = gson.fromJson(json.toString(), collectionType);
                            } catch (Exception e) {
                                if (debug) Log.e(TAGNAME, "/gson.fromJson error:" + e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || !cra.getReply().isSucc()) {
                                if (cra == null) {
                                    cra = new HeloCra();
                                    cra.setReply(NULL_OBJECT_CRA);
                                }

                                isFailCase = true;
                            } else {
                                //set mobile number prefix
                                String mobPfx = cra.getOClnCfg().getProp().get("CLN_MOB_PFX");
                                String ltsPfx = cra.getOClnCfg().getProp().get("CLN_LTS_PFX");
                                if (mobPfx != null && mobPfx.length() > 0)
                                    ClnEnv.setPref(activity, "mobPfx", mobPfx);
                                if (ltsPfx != null && ltsPfx.length() > 0)
                                    ClnEnv.setPref(activity, "ltsPfx", ltsPfx);

                                ClnEnv.setHeloCra(cra);

                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onSuccess(response);

                                if (debug)
                                    Utils.showLog(response.getActionTy(), String.format("%s %s %s", response.getActionTy(), cra.getClass().getSimpleName(), "response received:") + resultStr);

                                //							response.setReply(cra.getReply());
                                //	            			response.setCra(cra);
                                //	            			callback.onSuccess(response);
                            }
                        }
                        // dismiss
                        progressManager = ProgressManager.getInstance(activity);
                        progressManager.dismissProgressDialog(isFailCase);

                    } catch (Exception e) {
                        //						progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            });
        } catch (Exception e1) {
            e1.toString();
            // dismiss
            //        	progressManager = ProgressManager.getInstance(activity);
            //			progressManager.dismissProgressDialog(true);
        }
    }

    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }

    public static boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName(PRD_domain); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }
}
