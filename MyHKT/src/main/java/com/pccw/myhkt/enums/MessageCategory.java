package com.pccw.myhkt.enums;

import com.pccw.myhkt.R;

public enum MessageCategory {

    //BI, Billing
    //IM, Important
    //MA, Maintenance
    //GL, General
    //PM, Promotion
    //RM, Redemption
    //SV, Service

    MESSAGE_CAT_IMPORTANT("IM"),
    MESSAGE_CAT_BILLING("BI"),
    MESSAGE_CAT_SERVICE("SV"),
    MESSAGE_CAT_REDEMPTION("RM"),
    MESSAGE_CAT_MAINTENANCE("MA"),
    MESSAGE_CAT_PROMOTION("PM"),
    MESSAGE_CAT_MY_HKT("GL"); //General

    private String _value;

    MessageCategory(String Value) {
        this._value = Value;
    }

    public String getValue() {
        return _value;
    }

    public static MessageCategory fromString(String i) {
        for (MessageCategory b : MessageCategory.values()) {
            if (b.getValue().equalsIgnoreCase(i)) { return b; }
        }
        return null;
    }

    public static int getMessageCategoryIcon(MessageCategory category) {
        switch (category){
            case  MESSAGE_CAT_IMPORTANT:
                return R.drawable.icon_message_important;
            case  MESSAGE_CAT_BILLING:
                return R.drawable.icon_message_billing;
            case  MESSAGE_CAT_SERVICE:
                return R.drawable.icon_message_service;
            case  MESSAGE_CAT_REDEMPTION:
                return R.drawable.icon_message_redemption;
            case  MESSAGE_CAT_MAINTENANCE:
                return R.drawable.icon_message_maintenance;
            case  MESSAGE_CAT_PROMOTION:
                return R.drawable.icon_message_promotion;
            case  MESSAGE_CAT_MY_HKT:
                return R.drawable.icon_message_myhkt;
        }

        return -1;
    }
}
