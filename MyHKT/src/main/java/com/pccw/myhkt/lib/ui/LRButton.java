package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.R;
import com.pccw.myhkt.APIsManager.OnAPIsListener;

public class LRButton extends LinearLayout {

	private AQuery aq;
	private Boolean isLeftClick = true;
	private Context context;
	private OnLRButtonClickListener 	callback;
	public OnLRButtonClickListener getCallback() {
		return callback;
	}

	public void setCallback(OnLRButtonClickListener callback) {
		this.callback = callback;
	}

	public LRButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public LRButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public LRButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
	}
	
	public interface OnLRButtonClickListener {
		public void onLRBtnLeftClick();
		public void onLRBtnRightClick();
	}

	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs
				, R.styleable.HKTButton, 0, 0);
		try {
			//			int textSize =	a.getInt(R.styleable.BlueButton_android_textSize, (int)getResources().getDimension(R.dimen.bodytextsize));
			//			String text = a.getString(R.styleable.BlueButton_android_text);
			//			int height = a.getDimensionPixelOffset(R.styleable.BlueButton_android_layout_height, (int)context.getResources().getDimension(R.dimen.buttonblue_height));
			//			int width = a.getDimensionPixelOffset(R.styleable.BlueButton_android_layout_width, (int)context.getResources().getDimension(R.dimen.buttonblue_width));
			//			initViews(context, text, textSize, height, width);
		} finally {
			a.recycle();
		}
	}


	public void initViews(final Context context, String leftTxt , String rightTxt) {	
		initViews(context, leftTxt, rightTxt, getResources().getDimension(R.dimen.bodytextsize));
	}

	public void initViews(final Context context, String leftTxt , String rightTxt, float textSize) {	
		initViews(context, leftTxt, rightTxt, textSize,(int)context.getResources().getDimension(R.dimen.lr_button_height), LayoutParams.MATCH_PARENT);
	}

	public void initViews(final Context context, String leftTxt , String rightTxt, float textSize, int height, int width) {	
		aq = new AQuery(this);
		this.context = context;
		LayoutInflater.from(context).inflate(R.layout.lrbutton, this);
		aq.id(R.id.lrbutton_left).text(leftTxt).height(height, false);	
		aq.id(R.id.lrbutton_left).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);		
		aq.id(R.id.lrbutton_right).textSize(textSize).text(rightTxt).height(height, false);
		aq.id(R.id.lrbutton_right).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		setBtns();
		aq.id(R.id.lrbutton_left).clicked(onLeftClick);
		aq.id(R.id.lrbutton_right).clicked(onRightClick);
    	try {
    		callback = (OnLRButtonClickListener) context; 
    	} catch (ClassCastException e) {
		}	
	}
	
	public void setTexts(String leftTxt, String rightTxt) {
		aq.id(R.id.lrbutton_left).text(leftTxt);
		aq.id(R.id.lrbutton_right).text(rightTxt);
	}
	private OnClickListener onLeftClick = new OnClickListener(){
		@Override
		public void onClick(View v) {
			isLeftClick = true;
			setBtns();
			if (callback != null) {
				callback.onLRBtnLeftClick();
			}
		}		
	};
	
	private OnClickListener onRightClick = new OnClickListener(){
		@Override
		public void onClick(View v) {
			isLeftClick = false;
			setBtns();
			if (callback != null) {
				callback.onLRBtnRightClick();
			}
		}		
	};

	public Boolean isLeftClick() {
		return isLeftClick;
	}
	
	public void setBtns(Boolean isLClick) {
		isLeftClick = isLClick;
		setBtns();
	}
	private void setBtns() {
		aq.id(R.id.lrbutton_left).background(isLeftClick? R.drawable.hkt_btnleft_bg_blue :R.drawable.hkt_btnleft_bg_white);
		aq.id(R.id.lrbutton_left).textColor(isLeftClick? Color.WHITE :context.getResources().getColor(R.color.hkt_txtcolor_grey));
		aq.id(R.id.lrbutton_right).background(!isLeftClick? R.drawable.hkt_btnright_bg_blue :R.drawable.hkt_btnright_bg_white);
		aq.id(R.id.lrbutton_right).textColor(!isLeftClick? Color.WHITE :context.getResources().getColor(R.color.hkt_txtcolor_grey));
	}	
}
