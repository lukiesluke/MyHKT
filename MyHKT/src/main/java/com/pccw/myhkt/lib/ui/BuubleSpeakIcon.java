package com.pccw.myhkt.lib.ui;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
//Ref : https://github.com/JakeWharton/ViewPagerIndicator/blob/master/library/src/com/viewpagerindicator/PageIndicator.java
public class BuubleSpeakIcon extends View {

	private float mPointX=0;
	private float mPointY=0;
	private Bitmap bitmap;
	private int    tabCount;
	private int    tabPos;
	private String TAG = "IndicatorBar";

	public BuubleSpeakIcon(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	} 
	public BuubleSpeakIcon(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public BuubleSpeakIcon(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public void initPos(int tabPos , int tabCount){
		this.tabCount = tabCount;
		this.tabPos = tabPos;
		isFirstTime = true;
	}


	Paint p;
	Boolean isFirstTime = true;
	@Override
	protected void onDraw(Canvas canvas) {
		//TODO not finialize yet
		super.onDraw(canvas);
			 
		p=new Paint();
		p.setColor(getContext().getResources().getColor(R.color.livechat_blue));
		p.setAntiAlias(true);
		
		int width = canvas.getWidth();
        int height = canvas.getHeight();
        
//        int width = 0;
//        int height = 0;
//        
		int padding = Utils.dpToPx(getContext().getResources().getInteger(R.integer.padding_screen));
		int btnHeight = getContext().getResources().getDimensionPixelOffset(R.dimen.button_height);
		
		int startX = width -(btnHeight+padding*2)/2 ;
		int startY = (btnHeight+padding*2)/2;
		
		// Use the ppt layout for ref 
		Path triangle = new Path();
		triangle.moveTo(width-(btnHeight+padding*2)*3/2,height);
		triangle.lineTo(width-(btnHeight+padding*2)*8/9,height);
		triangle.lineTo(startX,startY);
		triangle.close();
		canvas.drawPath(triangle, p);		
//		this.invalidate();

	}


}
