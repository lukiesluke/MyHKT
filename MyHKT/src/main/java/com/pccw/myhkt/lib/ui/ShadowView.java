package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class ShadowView extends androidx.appcompat.widget.AppCompatImageView {

    private Bitmap bitmap = null;
    private int imageRes;

    public ShadowView(Context context) {
        super(context);
        //		bitmap = BitmapFactory.decodeResource(getResources(),imageRes);
    }

    Paint paint4 = new Paint();

    public ShadowView(Context context, int ImageRes) {
        super(context);
        this.imageRes = imageRes;
        bitmap = BitmapFactory.decodeResource(getResources(), imageRes);
        paint4.setShadowLayer(5, 8, 7, Color.parseColor("#000000"));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//		Paint paint1 = new Paint();  
        //		         paint1.setColor(0xFFFFFF00);
        //         paint1.setShadowLayer(5, 3, 3, 0xFFFF00FF);
        //         Paint paint2 = new Paint();
        //         paint2.setColor(Color.GREEN);
        //         paint2.setShadowLayer(10, 5, 2, Color.YELLOW);
        //
        //         Paint paint3 = new Paint();
        //         paint3.setColor(Color.RED);
        //         paint3.setShadowLayer(30, 5, 2, Color.GREEN);
        //         canvas.drawCircle(50, 130,30, paint3);
        //

        canvas.drawBitmap(bitmap, 50, 130, paint4);
    }
} 	

