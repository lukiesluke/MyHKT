package com.pccw.myhkt.lib.ui;

import com.androidquery.AQuery;
import com.pccw.myhkt.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class OptionButton extends LinearLayout {
	private AQuery aq;
	private Boolean isLeftClick = true;
	private Context context;
	
	public OptionButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public OptionButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public OptionButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
	}
	
	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HKTButton, 0, 0);
		try {
		} finally {
			a.recycle();
		}
	}

	public void initViews(final Context context, String leftTxt , String rightTxt) {	
		initViews(context, leftTxt, rightTxt, getResources().getDimension(R.dimen.bodytextsize));
	}

	public void initViews(final Context context, String leftTxt , String rightTxt, float textSize) {	
		initViews(context, leftTxt, rightTxt, textSize,(int)context.getResources().getDimension(R.dimen.lr_button_height), LayoutParams.MATCH_PARENT);
	}

	public void initViews(final Context context, String leftTxt , String rightTxt, float textSize, int height, int width) {	
		aq = new AQuery(this);
		this.context = context;
		LayoutInflater.from(context).inflate(R.layout.button_option, this);
		aq.id(R.id.option_button_left).text(leftTxt).height(height, false);	
		aq.id(R.id.option_button_left).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);		
		aq.id(R.id.option_button_right).textSize(textSize).text(rightTxt).height(height, false);
		aq.id(R.id.option_button_right).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		
		aq.id(R.id.option_button_right).getView().setClickable(true);
		aq.id(R.id.option_button_left).getView().setClickable(true);
		setBtns();
		aq.id(R.id.option_button_left).clicked(onLeftClick);
		aq.id(R.id.option_button_right).clicked(onRightClick);
	}
	
	private OnClickListener onLeftClick = new OnClickListener(){
		@Override
		public void onClick(View v) {
			isLeftClick = true;
			setBtns();
		}		
	};
	
	private OnClickListener onRightClick = new OnClickListener(){
		@Override
		public void onClick(View v) {
			isLeftClick = false;
			setBtns();
		}		
	};
	
	private void setBtns() {
		aq.id(R.id.option_button_left).background(isLeftClick? R.drawable.option_2px :R.drawable.optionb_2px);
		aq.id(R.id.option_button_left).textColor(isLeftClick? Color.BLACK :context.getResources().getColor(R.color.hkt_txtcolor_grey));
		aq.id(R.id.option_button_right).background(!isLeftClick? R.drawable.option_2px :R.drawable.optionb_2px);
		aq.id(R.id.option_button_right).textColor(!isLeftClick? Color.WHITE :context.getResources().getColor(R.color.hkt_txtcolor_grey));
	}	
}
