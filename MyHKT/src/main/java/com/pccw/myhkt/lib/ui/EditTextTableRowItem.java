package com.pccw.myhkt.lib.ui;

import com.androidquery.AQuery;
import com.pccw.myhkt.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TableRow;

public class EditTextTableRowItem extends TableRow {
	private AQuery aq;
	
	public EditTextTableRowItem(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public EditTextTableRowItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HomeImageButton, 0, 0);
		aq = new AQuery(this);

		LayoutInflater.from(context).inflate(R.layout.edittext_reg_input, this);
		try {
			// Style Initialize
//			String text = (String) a.getText(R.styleable.RegInputItem_title1);
//			aq.id(R.id.edittextrow_txt).text(text!=null ? text :"");
//			String hint = (String) a.getText(R.styleable.RegInputItem_hint);
//			aq.id(R.id.edittextrow_et).getEditText().setHint(hint!=null ? hint :"");
//			String input = (String) a.getText(R.styleable.RegInputItem_input);
//			aq.id(R.id.edittextrow_et).getEditText().setText(input!=null ? input :"");		

		} finally {
			a.recycle();
		}
	}

	public void initViews(final Context context, int height, String title, String hint, String input) {
		initViews(context, height, title, hint, input , InputType.TYPE_CLASS_TEXT) ;
	}

	public void initViews(final Context context, String title, String hint, String input) {
		initViews(context, (int)this.getResources().getDimension(R.dimen.reg_input_height), title, hint, input , InputType.TYPE_CLASS_TEXT) ;
	}

	public void initViews(final Context context, String title, String hint, String input , int inputType) {
		initViews(context, (int)this.getResources().getDimension(R.dimen.reg_input_height), title, hint, input , inputType) ;
	}

	public void initViews(final Context context, int height, String title, String hint, String input , int inputType) {
		LayoutInflater.from(context).inflate(R.layout.edittext_reg_input, this, false);
			
		aq.id(R.id.edittextrow_txt).text(title);
//		if (title ==null || title.equals("")){ aq.id(R.id.edittextrow_txt).gone(); }

		aq.id(R.id.edittextrow_et).getEditText().setHint(hint);
		aq.id(R.id.edittextrow_et).getEditText().setText(input);
		aq.id(R.id.edittextrow_et).getEditText().setInputType(inputType);
		if (inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
			aq.id(R.id.edittextrow_et).getEditText().setTransformationMethod(new PasswordTransformationMethod());
		}
		aq.id(R.id.edittextrow_txt).height(height/2, false);
		aq.id(R.id.edittextrow_et).height(height/2, false);
	}

	public String getInput(){
		return aq.id(R.id.edittextrow_et).getEditText().getEditableText().toString();
	}
	
	public void setInputType(int inputType) {
		aq.id(R.id.edittextrow_et).getEditText().setInputType(inputType);
	}
	
	public void setText(String text) {
		aq.id(R.id.edittextrow_txt).text(text);
	}
	
	public void setEditText(String text) {
		aq.id(R.id.edittextrow_et).text(text);
	}
	
	public void setTexts(String text, String hints) {
		aq.id(R.id.edittextrow_txt).text(text);
		aq.id(R.id.edittextrow_et).getEditText().setHint(hints);
	}

	public void setMaxLength(int max) {
		InputFilter[] fileter = new InputFilter[1] ;
		fileter[0] = 	new InputFilter.LengthFilter(max);
		aq.id(R.id.edittextrow_et).getEditText().setFilters(fileter);
	}	
}
