package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.androidquery.AQuery;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.R;

public class RegImageButton extends LinearLayout {

	private AQuery aq;
	private int buttonPadding;
	
	public int getButtonPadding() {
		return buttonPadding;
	}

	public void setButtonPadding(int buttonPadding) {
		this.buttonPadding = buttonPadding;
	}

	public RegImageButton(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public RegImageButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public RegImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
	}
	
	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HomeImageButton, 0, 0);
		aq = new AQuery(this);
		
		LayoutInflater.from(context).inflate(R.layout.button_reg, this, false);
		try {

		} finally {
			a.recycle();
		}
	}
	public void initViews(final Context context, int imageRes, String text, int displayWidth) {
		initViews(context, imageRes, text, 0 ,0, displayWidth);
	}

	public void initViews(final Context context, int imageRes, String lob, int textSize, int bgcolorRes, int displayWidth) {
		LayoutInflater.from(context).inflate(R.layout.button_reg, this);
		aq = new AQuery(this);

		aq.id(R.id.reg_imagelogo).image(imageRes);
		aq.id(R.id.reg_imagebg).image(R.drawable.logo_container_selector);

		// Style Initialize
		if (buttonPadding == 0) {
			buttonPadding = displayWidth / 20;
		}
		aq.id(R.id.reg_imagelogo).height(displayWidth, false);
		aq.id(R.id.reg_imagelogo).width(displayWidth, false);

		if (SubnRec.LOB_PCD.equalsIgnoreCase(lob)) {
			aq.id(R.id.reg_imagelogo).getImageView().setPadding(2, 2, 2, 2);
		} else {
			aq.id(R.id.reg_imagelogo).getImageView().setPadding(buttonPadding, buttonPadding, buttonPadding, buttonPadding);
		}
		aq.id(R.id.reg_imagebg).height(displayWidth, false);
		aq.id(R.id.reg_imagebg).width(displayWidth, false);
		if (Build.VERSION.SDK_INT > 19) {
			aq.id(R.id.reg_imagebg).image(R.drawable.logo_container_gray_sh);
		}
	}
	
	public void setSelect(Boolean isSelected) {
		if(Build.VERSION.SDK_INT > 19){
			aq.id(R.id.reg_imagebg).image(isSelected? R.drawable.logo_container_blue_sh : R.drawable.logo_container_gray_sh);
		}
		else{
			aq.id(R.id.reg_imagebg).image(isSelected? R.drawable.logo_container_blue_sh : R.drawable.logo_container_selector);
		}
	}
	
	public void setBg(int resId) {
		aq.id(R.id.reg_imagebg).image(resId);
	}
}
