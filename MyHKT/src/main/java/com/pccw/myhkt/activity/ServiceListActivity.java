package com.pccw.myhkt.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.TextViewCompat;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.adapter.ServiceListViewAdapter;
import com.pccw.myhkt.adapter.ServiceListViewAdapter.OnServiceListViewAdapterListener;
import com.pccw.myhkt.dialogs.ChangeDisplayNameDialog;
import com.pccw.myhkt.lib.swipelistview.BaseSwipeListViewListener;
import com.pccw.myhkt.lib.swipelistview.SwipeListView;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.util.Constant;

/************************************************************************
 * File : ServiceListActivity.java 
 * Desc : Service List
 * by : Derek Tsui 
 * Date : 30/10/2015
 *
 * Change History: 
 * Date Modified By Description ---------- ----------------------------------------------- 
 * 30/10/2015 	Derek Tsui 			1.) First draft
 *************************************************************************/
public class ServiceListActivity extends BaseActivity implements OnServiceListViewAdapterListener, ChangeDisplayNameDialog.OnChangeDisplayName {
    // Common Components
    private boolean debug = false;
    private static ServiceListActivity me;
    public Context cxt;
    private AlertDialog alertDialog = null;
    private String TAG = this.getClass().getName();
    private AAQuery aq;
    public static int deviceWidth;
    // List View adapter
    private ServiceListViewAdapter serviceListViewAdapter;
    //ui
    //SwipeListView
    public SwipeListView servicelist_listView;
    public LinearLayout servicelist_remark;

    private int parentOnClickId = 0;
    private int extralinespace = 0;
    private int basePadding = 0;
    private int padding_screen = 0;
    private int padding_2col = 0;
    private androidx.appcompat.app.AlertDialog sessionTimeoutDialog;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        me = this;
        deviceWidth = getDeviceWidth();
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        setContentView(R.layout.activity_servicelist);

        // Check for Line Test Redirect Screen Immediately
        // Assume CONST_PREF_LNTT_NOTICEFLAG = true, srv_num must be not empty, checked by MainMenu
        if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false)) {
            SubnRec rSubnRec = Utils.getPrefLnttAgent(this).getLnttCra().getISubnRec().copyMe();
            AcctAgent rAcctAgent = getAcctAgentBySubnRec(rSubnRec);
            // Line Test Result completed, redirect to targetService
            Intent targetIntent = new Intent(this, ServiceActivity.class);
            targetIntent.putExtra(Constant.KEY_IS_MY_LINE_TEST, true);
            if (targetIntent != null) {
                Bundle lnttBundle = new Bundle();
                lnttBundle.putBoolean("ISASSOCARY", true);
                lnttBundle.putSerializable("SUBNREC", rSubnRec);
                lnttBundle.putSerializable("ACCTAGENT", rAcctAgent);
                targetIntent.putExtras(lnttBundle);
                startActivity(targetIntent);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    MyReceiver rhelper;

    @Override
    protected final void onStart() {
        super.onStart();
        //Register Receiver for lineTest so it can refresh the listview
        rhelper = new MyReceiver(this);
        rhelper.registerAction("LineTest");

        // Check for Bill Message Redirection Screen Immediately
        if (debug) Log.i(TAG, "Check bill status");
        if (debug) Log.i(TAG, "isShowing:" + String.valueOf(sessionTimeoutDialog != null && sessionTimeoutDialog.isShowing()));

        initData();
        initUI();
    }

    @Override
    protected final void onResume() {
        super.onResume();
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_SERVICELIST, false);
        moduleId = getResources().getString(R.string.MODULE_HOME);
        if (Utils.isLnttCompleted(this)) {
            initUI();
        }

        aq.id(R.id.navbar_button_right).visibility(View.INVISIBLE);
        if (ClnEnv.getPushDataBill() != null && ClnEnv.getPref(this, getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false)) {
            processBillNotification();
        }
    }

    private void processBillNotification() {
        if (ClnEnv.getPushDataBill().getType().equals("N")) {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    if(sessionTimeoutDialog == null || sessionTimeoutDialog != null && sessionTimeoutDialog.isShowing())
                        Utils.checkBillMsgAndRedirect(me, true);
                }
            }, 500);
        } else if (ClnEnv.getPushDataBill().getType().equals("B")) {

            // Check for Bill Message Redirection Screen Immediately
            if (debug) Log.i(TAG, "Check bill status");
            if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false)) {
                SubnRec selectedSubnRec = new SubnRec();
                AcctAgent rAcctAgent = new AcctAgent();
                if (ClnEnv.getPushDataBill() != null) {
                    for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry())) {
                        if (rSubnRec.acctNum.equalsIgnoreCase(ClnEnv.getPushDataBill().getAcctNum())) {
                            selectedSubnRec = rSubnRec;
                            rAcctAgent = getAcctAgentBySubnRec(rSubnRec);
                        }
                    }
                    if (debug) Log.i(TAG, "Check login id");
                    if (ClnEnv.getLoginId().equalsIgnoreCase(ClnEnv.getPushDataBill().getLoginId())) {
                        if (debug) Log.i(TAG, "same login id");
                        // Redirect to targetService
                        Intent targetIntent = new Intent(this, ServiceActivity.class);
                        Bundle lnttBundle = new Bundle();
                        lnttBundle.putBoolean("ISASSOCARY", true);
                        lnttBundle.putSerializable("SUBNREC", selectedSubnRec);
                        lnttBundle.putSerializable("ACCTAGENT", rAcctAgent);
                        targetIntent.putExtras(lnttBundle);
                        targetIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(targetIntent);
                    } else {
                        DialogHelper.createSimpleDialog(me, getResString(R.string.MYHKT_PN_ERR_NOSUBN));
                        ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
                        ClnEnv.getPushDataBill().clear();
                    }
                }
            }
        }
    }

    Parcelable state;

    @Override
    protected final void onPause() {
        if (debug) Log.i(TAG, "pause");
        state = servicelist_listView.onSaveInstanceState();
        super.onPause();
    }

    @Override
    protected final void onStop() {
        try {
            if (rhelper != null) {
                this.unregisterReceiver(rhelper);
            }
        } catch (Exception e) {
            // rhelper is already unregistered
            rhelper = null;
        }
        try {
            if (broadcastReceiver != null) {
                this.unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            // lineTestBroadcastReceiver is already unregistered
            broadcastReceiver = null;
        }
        super.onStop();
    }

    @Override
    protected final void onDestroy() {
        try {
            if (broadcastReceiver != null) {
                this.unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            // lineTestBroadcastReceiver is already unregistered
            broadcastReceiver = null;
        }
        super.onDestroy();
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private void initData() {
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        padding_screen = (int) getResources().getDimension(R.dimen.padding_screen);
        padding_2col = (int) getResources().getDimension(R.dimen.padding_screen);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
    }

    private void initUI() {
        cxt = getApplicationContext();
        aq = new AAQuery(this);
        //navbar
        updateGuidelineTitle();
        aq.navBarTitle(R.id.navbar_title, getResString(getIntent().getBooleanExtra(Constant.KEY_IS_MY_LINE_TEST, false) || Utils.ISMYLIENTEST ? R.string.title_my_linetest : R.string.title_my_account));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        //service list
        servicelist_listView = (SwipeListView) findViewById(R.id.servicelist_listView);

        aq.id(R.id.servicelist_empty).margin(padding_screen, padding_screen, padding_screen, padding_screen);
        aq.id(R.id.servicelist_empty).getTextView().setText(Utils.ISMYLIENTEST ? R.string.MYHKT_NO_SUBSCRIPTION_LINETEST : R.string.MYHKT_NO_SUBSCRIPTION);

        servicelist_remark = (LinearLayout) findViewById(R.id.servicelist_remark);

        try {
            //Constant.KEY_IS_MY_LINE_TEST is from the MainMenuActivity My XXX onclick
            serviceListViewAdapter = new ServiceListViewAdapter(this, 0, servicelist_listView,
                    getIntent().getBooleanExtra(Constant.KEY_IS_MY_LINE_TEST, false));

            servicelist_listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            servicelist_listView.setAdapter(serviceListViewAdapter);
            servicelist_listView.setEmptyView(aq.id(R.id.servicelist_empty).getView());
            servicelist_listView.setSwipeListViewListener(new ServiceListSwipeListViewListener());
            servicelist_listView.setDeviceWidth(deviceWidth);
            servicelist_listView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
            servicelist_listView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
            servicelist_listView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
            servicelist_listView.setOffsetLeft(deviceWidth * 5 / 6);
            servicelist_listView.setAnimationTime(0);
            servicelist_listView.setSwipeOpenOnLongPress(false);

        } catch (Exception e) {
            // If logincra = null, then closed the Servicelist and directly to MainMenu
            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        }

        aq.marginpx(R.id.servicelist_remark, basePadding, 0, basePadding, 0);

        try {
            aq.normTxtBtn(R.id.servicelist_btn_alias, getResString(R.string.myhkt_btn_alias),
                    LinearLayout.LayoutParams.MATCH_PARENT)
                    .setVisibility(serviceListViewAdapter != null && serviceListViewAdapter.getCount() == 0 ? View.GONE : View.VISIBLE);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        aq.id(R.id.servicelist_btn_alias).clicked(this, "onClick");
        // aq.id(R.id.servicelist_btn_alias).text(R.string.myhkt_btn_alias);
        aq.marginpx(R.id.servicelist_btn_alias, basePadding, basePadding, basePadding, basePadding);
        // servicelist_listView.scrollTo(0, listViewPos);
        if (state != null) {
            servicelist_listView.onRestoreInstanceState(state);
        }

        IntentFilter filter = new IntentFilter(Constant.ACTION_CACHED_APPT_RESPONSE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(broadcastReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
        } else {
            registerReceiver(broadcastReceiver, filter);
        }
    }

    private void updateGuidelineTitle() {
        float density = Utils.getDensity(this.getApplicationContext());
        if(density <= 1.0f) {
            TextView title = (TextView) aq.id(R.id.navbar_title).getTextView();
            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams)
                    title.getLayoutParams();

            TextViewCompat.setAutoSizeTextTypeWithDefaults(title, TextViewCompat.AUTO_SIZE_TEXT_TYPE_NONE);
            title.setLayoutParams(lp);
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                finish();
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                break;
            case R.id.servicelist_btn_alias:
                if (servicelist_listView.isOpened()) {
                    servicelist_listView.closeOpenedItems();
                } else {
                    for (int i = servicelist_listView.getFirstVisiblePosition(); i <= servicelist_listView.getLastVisiblePosition(); i++) {
                        if (!serviceListViewAdapter.isZombie(i))
                            servicelist_listView.openAnimate(i);
                    }
                }
                break;
        }
    }

    private void goToServiceActivity(AcctAgent agent, int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("CLICKBUTTON", parentOnClickId);
        bundle.putSerializable("ACCTAGENT", agent);
        String selectedLob = serviceListViewAdapter.getLOB(position);
        if (selectedLob != null) bundle.putString("LOB", selectedLob);

        Intent intent = new Intent(getApplicationContext(), ServiceActivity.class);
        intent.putExtra(Constant.KEY_IS_MY_LINE_TEST, getIntent().getBooleanExtra(Constant.KEY_IS_MY_LINE_TEST, false));
        intent.putExtras(bundle);
        startActivity(intent);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    class ServiceListSwipeListViewListener extends BaseSwipeListViewListener {
        @Override
        public int onChangeSwipeMode(int position) {
            if (serviceListViewAdapter.isZombie(position)) {
                return SwipeListView.SWIPE_MODE_NONE;
            } else {
                me.refreshSwipeMode();
                return SwipeListView.SWIPE_MODE_LEFT;
            }
        }

        //		AdapterView<?> adapterView, View view, int position, long arg3
        @Override
        public void onClickFrontView(final int position, View view) {
            super.onClickFrontView(position, view);
            Intent intent = null;
            Bundle bundle = new Bundle();

            final AcctAgent agent = (AcctAgent) serviceListViewAdapter.getItem(position);
            if (agent.getSubnRec().inMipMig) {
                if (agent.getSubnRec().isZombie()) {
                    new AlertDialog.Builder(ServiceListActivity.this)
                            .setMessage(getResString(R.string.M_CSIM_ACCT_IN_MIG))
                            .setCancelable(false)
                            .setPositiveButton(getResString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    goToServiceActivity(agent, position);
                                }
                            }).create().show();
                    return;
                } else {
                    new AlertDialog.Builder(ServiceListActivity.this)
                            .setMessage(getResString(R.string.M_CSIM_IN_MIG))
                            .setCancelable(false)
                            .setPositiveButton(getResString(R.string.btn_ok), null).create().show();
                    return;
                }
            }
            //pass button click
            bundle.putInt("CLICKBUTTON", parentOnClickId);
            bundle.putSerializable("ACCTAGENT", agent);
            String selectedLob = serviceListViewAdapter.getLOB(position);
            if (selectedLob != null) bundle.putString("LOB", selectedLob);
            intent = new Intent(getApplicationContext(), ServiceActivity.class);
            if (intent != null) {
                intent.putExtra(Constant.KEY_IS_MY_LINE_TEST, getIntent().getBooleanExtra(Constant.KEY_IS_MY_LINE_TEST, false));
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        }

        @Override
        public void onClickBackView(int position) {
            servicelist_listView.closeOpenedItems();
        }

        @Override
        public void onDismiss(int[] reverseSortedPositions) {
            //			for (int position : reverseSortedPositions) {
            //				testData.remove(position);
            //			}
            //			mAdapter.notifyDataSetChanged();
        }
    }

    public void closeOpenedListItems() {
        if (servicelist_listView != null) {
            if (servicelist_listView.isOpened()) {
                servicelist_listView.closeOpenedItems();
            }
        }
    }

    private void refreshSwipeMode() {
        servicelist_listView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
        servicelist_listView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
        servicelist_listView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
        servicelist_listView.setOffsetLeft(deviceWidth * 5 / 6);
        servicelist_listView.setAnimationTime(0);
        servicelist_listView.setSwipeOpenOnLongPress(false);
    }

    public void showRemark(boolean show) {
        if (servicelist_remark != null) {
            if (show) {
                servicelist_remark.setVisibility(View.VISIBLE);
            } else {
                servicelist_remark.setVisibility(View.GONE);
            }
        }
    }

    private int getDeviceWidth() {
        return getResources().getDisplayMetrics().widthPixels;
    }

    public final void displayEditDialog(String message, final int position) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        closeOpenedListItems();

        ChangeDisplayNameDialog changeDisplayNameDialog = ChangeDisplayNameDialog.newInstance(serviceListViewAdapter.getAlias(position), position);
        changeDisplayNameDialog.setCallback(this);
        changeDisplayNameDialog.show(getSupportFragmentManager(), "dialog");
    }

    public void updateAlias(String alias, SubnRec subnRec) {
        // doSvcAso
        // Prepare subnRecAry which are selected
        SubnRec[] subnRecAry = new SubnRec[ClnEnv.getQualSvee().getSubnRecAry().length];
        SubnRec selectedSubnRec = new SubnRec();
        for (int i = 0; i < ClnEnv.getQualSvee().getSubnRecAry().length; i++) {
            subnRecAry[i] = ClnEnv.getQualSvee().getSubnRecAry()[i].copyMe();

            if (Utils.matchSubnRec(subnRecAry[i], subnRec)) {
                subnRecAry[i].alias = alias;
                selectedSubnRec = subnRecAry[i];
            }
        }

        AcMainCra acMainCra = new AcMainCra();
        acMainCra.setIChgPwd(false);
        acMainCra.getISveeRec().pwd = ClnEnv.getSessionPassword();
        acMainCra.setISubnRecAry(subnRecAry);
        acMainCra.setISveeRec(ClnEnv.getQualSvee().getSveeRec().copyMe());
        acMainCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
        acMainCra.setISubnRec(selectedSubnRec);

        APIsManager.doSvcAso4Subn(me, null, acMainCra);
    }

    //refresh listview to display new changes
    private void refreshList() {
        try {
            // List View re-init
            ////Constant.KEY_IS_MY_LINE_TEST is from the MainMenuActivity My XXX onclick
            serviceListViewAdapter = new ServiceListViewAdapter(this, parentOnClickId, servicelist_listView,
                    getIntent().getBooleanExtra(Constant.KEY_IS_MY_LINE_TEST, false)); //Constant.KEY_IS_MY_LINE_TEST is from the MainMenuAcr

            servicelist_listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            servicelist_listView.setAdapter(serviceListViewAdapter);
            servicelist_listView.setEmptyView(findViewById(R.id.servicelist_empty));
            servicelist_listView.setSwipeListViewListener(new ServiceListSwipeListViewListener());
            servicelist_listView.setDeviceWidth(deviceWidth);
            servicelist_listView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
            servicelist_listView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
            servicelist_listView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
            servicelist_listView.setOffsetLeft(deviceWidth * 5 / 6);
            servicelist_listView.setAnimationTime(0);
            servicelist_listView.setSwipeOpenOnLongPress(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private AcctAgent getAcctAgentBySubnRec(SubnRec subnRec) {
        //for login assoc account only
        AcctAgent rAcctAgent = new AcctAgent();
        rAcctAgent.setLob(subnRec.lob);
        rAcctAgent.setSrvNum(subnRec.srvNum);
        rAcctAgent.setCusNum(subnRec.cusNum);
        rAcctAgent.setAcctNum(subnRec.acctNum);
        rAcctAgent.setSrvId(subnRec.srvId);
        rAcctAgent.setSysTy(subnRec.systy);
        //		rAcctAgent.setSubnRecX(rx);
        rAcctAgent.setAcctX(-1);
        rAcctAgent.setLobType(Utils.getLobType(rAcctAgent.getLob()));
        rAcctAgent.setAlias(subnRec.alias.trim());
        rAcctAgent.setLive(true);
        rAcctAgent.setAssoc("Y".equalsIgnoreCase(subnRec.assoc) ? true : false);
        rAcctAgent.setSubnRec(subnRec);
        return rAcctAgent;
    }

    // use this as an inner class like here or as a top-level class
    public class MyReceiver extends BroadcastReceiver {
        Context ct = null;
        MyReceiver receiver;

        public MyReceiver(Context c) {
            ct = c;
            receiver = this;
        }

        public void registerAction(String action) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(action);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                ct.registerReceiver(receiver, filter, Context.RECEIVER_NOT_EXPORTED);
            } else {
                ct.registerReceiver(receiver, filter);
            }
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            state = servicelist_listView.onSaveInstanceState();
            if (Utils.isLnttCompleted(context)) {
                initUI();
            }
        }
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
        AcMainCra acMainCra = (AcMainCra) response.getCra();

        DialogHelper.createSimpleDialog(me, getString(R.string.RSVM_DONE));
        ClnEnv.getQualSvee().setSveeRec(acMainCra.getOSveeRec());
        ClnEnv.getQualSvee().setSubnRecAry(acMainCra.getOSubnRecAry());

        // Reset check RC_ALT error time
        //		me.errALT2nd = false;
        refreshList();
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        // General Error Message
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            displayDialog(this, response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            ivSessDialog();
        } else {
            displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
        }
    }

    @Override
    public void onChangeNickname(String name, int position) {
        SubnRec subnRec = (SubnRec) serviceListViewAdapter.getSubnRecItem(position);
        updateAlias(name, subnRec);
    }

    private void showCachedApptResponsePopup() {
        DialogInterface.OnClickListener onclick = (dialog, which) -> {
            Intent intent = new Intent();
            intent = new Intent(getApplicationContext(), MyApptActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        };

        DialogHelper.createSimpleDialog(this, Utils.getString(this, R.string.cappt_reminder),
                Utils.getString(this, R.string.btn_ok), onclick,
                Utils.getString(this, R.string.MYHKT_BTN_LATER));

    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(Constant.ACTION_CACHED_APPT_RESPONSE)) {
                boolean appt_ind = ClnEnv.getPref(getApplicationContext(),
                        getString(R.string.CONST_PREF_APPTIND_FLAG).concat("-").concat(String.valueOf(ClnEnv.getLoginId())), false);
                if(appt_ind) {
                    showCachedApptResponsePopup();
                }
            }
        }
    };

    private void showCachedApptExhaustedPopup() {
        DialogInterface.OnClickListener onclick = (dialog, which) -> dialog.dismiss();
        DialogHelper.createSimpleDialog(this, "Something went wrong (Cached Appt)", "OK", onclick);
    }
}
