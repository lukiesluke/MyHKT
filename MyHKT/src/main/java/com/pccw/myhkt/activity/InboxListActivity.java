package com.pccw.myhkt.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.pccw.dango.shared.cra.InbxCra;
import com.pccw.dango.shared.entity.IbxMsgRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.LiveChatHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.EmailChildCell;
import com.pccw.myhkt.cell.model.EmailParentCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.dialogs.LOBSelectionDialog;
import com.pccw.myhkt.dialogs.LOBSelectionDialogCompat;
import com.pccw.myhkt.enums.MessageCategory;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.ImageHolder;
import com.pccw.myhkt.util.Constant;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/************************************************************************
 * File : InboxListActivity.java
 * Desc : Inbox list that will show the list of message group by Year and month
 * Name : InboxListActivity
 * by : Addulmoiz Esmail
 * Date : 16/07/2018
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 16/07/2018 Abdulmoiz Esmail  -First draft
 * 20/07/2018 Abdulmoiz Esmail  -Navigating to inbox details screen after select inbox item
 * 10/09/2018 Abdulmoiz Esmail  -Apply hashmap sorting and sorting of each IbxMessage list n map item
 *************************************************************************/
public class InboxListActivity extends BaseActivity {

    public static final String IBX_DATE_FORMAT = "yyyyMMddHHmmss";
    private static final int MESSAGE_DETAIL = 1;



    private AAQuery aq;
    private List<Cell> cellList;
    CellViewAdapter cellViewAdapter;

    private IbxMsgRec[] inboxList;
    private List<String> headerList;
    private int scrollX;
    private int scrollY;

    // create map to store
    Map<String, List<IbxMsgRec>> map;

    LinearLayout frame;
    private ArrayList<Integer> listOfDeletedMsgs = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_list);
    }

    @Override
    public void onStart() {
        super.onStart();
        aq.id(R.id.navbar_button_right).clicked(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
            //login required
            if (LiveChatHelper.isPause) {
                openLiveChat();
            } else {
                popupLOBSelection();
            }
            }
        });
    }

    @Override
    protected void initUI2() {
        super.initUI2();
        scrollX = 0;
        scrollY = 0;
        aq = new AAQuery(this);
        cellViewAdapter = new CellViewAdapter(me);

        frame = (LinearLayout) aq.id(R.id.my_message_frame).getView();

        //navbar
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarTitle(R.id.navbar_title, getString(R.string.title_inbox));
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        isLiveChatShown = true;
    }

    private void popupLOBSelection() {
        ArrayList<ImageHolder> arrayList = new ArrayList<>();
        arrayList.add(new ImageHolder(R.drawable.lob_pcd_plain, SubnRec.LOB_PCD));
        arrayList.add(new ImageHolder(R.drawable.lob_lts_plain, SubnRec.LOB_LTS));
        arrayList.add(new ImageHolder(R.drawable.lob_tv_plain, SubnRec.LOB_TV));
        arrayList.add(new ImageHolder(R.drawable.lob_1010_plain, SubnRec.WLOB_X101));
        arrayList.add(new ImageHolder(R.drawable.lob_csl_plain, SubnRec.WLOB_CSL));
        arrayList.add(new ImageHolder(R.drawable.lob_csp_plain, SubnRec.LOB_CSP));
        spawnLOBDialog(arrayList);
    }

    private void spawnLOBDialog(ArrayList<ImageHolder> arrayList) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            new LOBSelectionDialog(this, true,
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            Log.d(this.getClass().getName(),
                                    "onCancel: No selection was made.");
                        }
                    }, arrayList).show();
        else
            new LOBSelectionDialogCompat(this, true,
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            Log.d(this.getClass().getName(),
                                    "onCancel: No selection was made.");
                        }
                    }, arrayList).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        moduleId = getResString(R.string.MODULE_MY_MESSAGES);
        refreshView();

        FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_INBOX_LIST, false);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private void refreshView() {
        cellList = new ArrayList<Cell>();

        InbxCra inbxCra = Utils.getPrefInboxCra(this);
        if (inbxCra != null && inbxCra.getOIbxMsgRecAry() != null) {
            //initListData();
            inboxList = inbxCra.getOIbxMsgRecAry();

            headerList = sortByTitle();
            populateMap();
        } else {
            inboxList = new IbxMsgRec[0];
            scrollX = 0;
            scrollY = 0;
        }
        aq.id(R.id.me_sv).getView().post(new Runnable() {
            @Override
            public void run() {
                aq.id(R.id.me_sv).getView().scrollTo(scrollX, scrollY);

                scrollX =0;
                scrollY = 0;
            }
        });

    }

    private void prepareListFromMap(Map<String, List<IbxMsgRec>> map) {

        for (String mapHeaderKey : map.keySet()) {


            for (int i = 0; i < map.get(mapHeaderKey).size(); i++) {
                final IbxMsgRec ibxMsgRec = map.get(mapHeaderKey).get(i);
                if(i == 0){
                    //header :month/year
                    String titleDate = Utils.toDateString(ibxMsgRec.createTs, IBX_DATE_FORMAT,  isZh ? "yyyy'年'MMM":"MMM yyyy", isZh);
                    EmailParentCell emailParentCell = new EmailParentCell(titleDate);
                    emailParentCell.setBgColorId(android.R.color.transparent);
                    cellList.add(emailParentCell);
                }

                //On select inbox item listener
                View.OnClickListener onClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        scrollX = aq.id(R.id.me_sv).getView().getScrollX();
                        scrollY = aq.id(R.id.me_sv).getView().getScrollY();
                        Intent intent = new Intent(InboxListActivity.this, InboxDetailActivity.class);
                        intent.putExtra(Constant.INBOX_BUNDLE_CONTENT, ibxMsgRec);
                        startActivityForResult(intent, MESSAGE_DETAIL);
                        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                    }
                };

                int remaining = Utils.daysRemaining(Utils.toDate(ibxMsgRec.expTs, IBX_DATE_FORMAT));
                String remainingDays = MessageFormat.format(getResString(R.string.inbox_remainingday), remaining);


                EmailChildCell emailChildCell = new EmailChildCell(
                        MessageCategory.fromString(ibxMsgRec.msgCat),
                        Utils.toDateString(ibxMsgRec.createTs, IBX_DATE_FORMAT, "dd/MM/yyyy"),
                        isZh ? ibxMsgRec.subjZh : ibxMsgRec.subjEn,
                        isZh ? ibxMsgRec.ctntZh : ibxMsgRec.ctntEn,
                        remainingDays,
                        ibxMsgRec.readSts.equalsIgnoreCase("Y"),
                        i == map.get(mapHeaderKey).size() - 1,
                        isZh ? ibxMsgRec.pathZh : ibxMsgRec.pathEn,
                        onClickListener);

                cellList.add(emailChildCell);
            }
        }

        cellViewAdapter.setView(frame, cellList);
    }

    private List<String> sortByTitle() {
        List<String> headerDate = new ArrayList<>();
        List<Date> listOfDates = new ArrayList<>();
        for (int i = 0; i < inboxList.length; i++) {
            //String titleDate = Utils.toDateString(inboxList[i].createTs, IBX_DATE_FORMAT,  isZh ? "yyyy'年'MMM":"MMM yyyy", isZh);
            Date date = Utils.toDate(inboxList[i].createTs, IBX_DATE_FORMAT);
            listOfDates.add(date);
        }

        for (Date monthDate : sortDateDescending(listOfDates)) {
            String dateString  = Utils.toDateString(monthDate, "MM yyyy");
            if (!headerDate.contains(dateString))
                headerDate.add(dateString);
        }


        return headerDate;
    }

    private List<Date> sortDateDescending(List<Date> listOfDates) {
        Collections.sort(listOfDates, new Comparator<Date>(){
            public int compare(Date date1, Date date2){
                return date2.compareTo(date1);
            }
        });

        return listOfDates;
    }


    private void populateMap() {
        List<IbxMsgRec> childList;
        map = new LinkedHashMap<>();
        if(headerList.size() == 0 || inboxList.length == listOfDeletedMsgs.size()){
            aq.id(R.id.inbox_no_message).getTextView().setVisibility(View.VISIBLE);
        }
        else{
            aq.id(R.id.inbox_no_message).getTextView().setVisibility(View.GONE);
        }

        for (String header : headerList) {
            childList = new ArrayList<>();
            for (int i = 0; i < inboxList.length; i++) {
                String createdDate = Utils.toDateString(inboxList[i].createTs, IBX_DATE_FORMAT,  "MM yyyy");
                if (createdDate.contains(header) && !listOfDeletedMsgs.contains(inboxList[i].rid)) {
                    childList.add(inboxList[i]);
                }
            }

            Collections.sort(childList, new CustomComparator());

            map.put(header, childList);
        }

        prepareListFromMap(map);
    }

    public class CustomComparator implements Comparator<IbxMsgRec> {
        @Override
        public int compare(IbxMsgRec o1, IbxMsgRec o2) {

            String date1 = Utils.toDateString(o1.createTs, IBX_DATE_FORMAT, "dd/MM/yyyy HHmmss");
            String date2 = Utils.toDateString(o2.createTs, IBX_DATE_FORMAT, "dd/MM/yyyy HHmmss");

            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HHmmss", Locale.ENGLISH);

            Date d1 = null;
            Date d2 = null;
            try {
                d1 = format.parse(date1);
                d2 = format.parse(date2);
            } catch (ParseException e) {
                e.printStackTrace();
            }


            assert d1 != null;
            return d2.compareTo(d1);
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent != null) {
            listOfDeletedMsgs.add(intent.getIntExtra("MSG_ID", 0));
        }
    }
}
