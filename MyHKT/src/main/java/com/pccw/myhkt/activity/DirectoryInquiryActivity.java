package com.pccw.myhkt.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.DqryCra;
import com.pccw.dango.shared.entity.DQReq;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.adapter.DirectoryInquiryListViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.LRTabButton;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.listeners.OnCallPhoneListener;
import com.pccw.myhkt.model.DQArea;
import com.pccw.myhkt.model.DQAreas;
import com.pccw.myhkt.model.DQDistrict;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DirectoryInquiryActivity extends BaseActivity implements OnCallPhoneListener {
    private boolean debug = false;
    private AAQuery aq;
    private String TAG = "DirectoryInquiryActivity";

    // Fragment
    private int extralinespace = 0;
    private int titleWdith = 0;
    private int deviceWidth = 0;
    private int btnWidth = 0;
    private EditText editTextName;
    private EditText editTextSurname;
    private EditText editTextGivenName;
    private ListView listView;
    private RelativeLayout loadingfooter;
    private LRTabButton btn;
    private DirectoryInquiryListViewAdapter directoryInquiryListViewAdapter;

    //Status
    private String nameInput = "";
    private String surnameInput = "";
    private String givenNameInput = "";
    private Boolean isCompany = true;

    //To advoid start a new search when the current search is not complete
    private Boolean isSearching = false;
    //To recongize if it is reload search or new search
    private Boolean isReloading = false;
    //Use when resume the page
    private Boolean isRemarkShown = true;

    private String[] areaNames;
    private String[][] districtNames;
    private List<List<DQDistrict>> districtList;
    private List<DQArea> areaList;
    private int areaPos = 0;
    private int districtPos = 0;
    private Parcelable state;
    //Object send to server(api)
    private DqryCra dqryCra;
    private DqryCra rDqryCra;

    private HKTButton searchBtn;

    private String phoneNum = "";
    private Boolean isNameTextNull = true;
    private Boolean isSurnameTextNull = true;
    private Boolean isGivenNameTextNull = true;

    public void recieveCallBack(int areaPos, int districtPos) {
        this.areaPos = areaPos;
        this.districtPos = districtPos;
        setAreaDistcirt();
    }

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        me = this;
        isLiveChatShown = false;
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        setContentView(R.layout.activity_directoryinquiry);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        titleWdith = (int) getResources().getDimension(R.dimen.dq_title_width);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        btnWidth = (deviceWidth - extralinespace * 4) / 2;
        nameInput = "";
        surnameInput = "";
        givenNameInput = "";
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        super.onStart();
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    protected final void initUI2() {
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        isZh = "zh".equalsIgnoreCase(this.getResources().getString(R.string.myhkt_lang));
        aq = new AAQuery(this);
        //navbar
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.MYHKT_DQ_PAGE_TITLE));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        aq.id(R.id.dq_frame1).backgroundColorId(R.color.white);
        aq.marginpx(R.id.dq_frame1, basePadding, 0, basePadding, 0);
        aq.id(R.id.dq_frame1).getView().setPadding(0, extralinespace / 2, 0, 0);

        //Top tab-bar
        btn = (LRTabButton) aq.id(R.id.dq_lrtabbtn).getView();
        btn.initViews(this, getResString(R.string.MYHKT_DQ_LBL_FIND_COMPANY), getResString(R.string.MYHKT_DQ_LBL_FIND_PERSON), R.drawable.search_company_on_3x, R.drawable.search_person_on_3x);
        btn.setMenuTabRightVisibility(false);

        aq.id(R.id.dq_lrtabbtn).height((int) getResources().getDimension(R.dimen.lr_button_height), false);
        btn.clicked(this, "onClick");

        aq.normText(R.id.dq_name_title, getResString(R.string.MYHKT_DQ_LBL_NAME));
        aq.id(R.id.dq_name_title).width(titleWdith, false);
        aq.id(R.id.dq_name_title).getTextView().setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        aq.normEditText(R.id.dq_name_input, nameInput, "");
        editTextName = aq.id(R.id.dq_name_input).getEditText();
        editTextName.setBackgroundResource(0);
        editTextName.setPadding(basePadding, 0, basePadding, 0);
        editTextName.setSingleLine();
        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.toString().length() == 0) {
                    isNameTextNull = true;
                } else {
                    isNameTextNull = false;
                }
                setSearchBtn();
            }
        });
        aq.normText(R.id.dq_surname_title, getResString(R.string.MYHKT_DQ_LBL_SURNAME));
        aq.id(R.id.dq_surname_title).width(titleWdith, false);
        aq.id(R.id.dq_surname_title).getTextView().setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        aq.normEditText(R.id.dq_surname_input, surnameInput, "");
        editTextSurname = aq.id(R.id.dq_surname_input).getEditText();
        editTextSurname.setBackgroundResource(0);
        editTextSurname.setPadding(extralinespace, 0, extralinespace, 0);
        editTextSurname.setSingleLine();
        editTextSurname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.toString().isEmpty()) {
                    isSurnameTextNull = true;
                } else {
                    isSurnameTextNull = false;
                }
                setSearchBtn();
            }
        });

        aq.normText(R.id.dq_givenname_title, getResString(R.string.MYHKT_DQ_LBL_GIVEN_NAME));
        aq.id(R.id.dq_givenname_title).width(titleWdith, false);
        aq.id(R.id.dq_givenname_title).getTextView().setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        aq.normEditText(R.id.dq_givenname_input, givenNameInput, "");
        editTextGivenName = aq.id(R.id.dq_givenname_input).getEditText();
        editTextGivenName.setBackgroundResource(0);
        editTextGivenName.setPadding(basePadding, 0, basePadding, 0);
        editTextGivenName.setSingleLine();
        editTextGivenName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s == null || s.toString().isEmpty()) {
                    isGivenNameTextNull = true;
                } else {
                    isGivenNameTextNull = false;
                }
                setSearchBtn();
            }
        });

        aq.normText(R.id.dq_area_title, getResString(R.string.MYHKT_DQ_LBL_AREA_DISTRICT));
        aq.id(R.id.dq_area_title).width(titleWdith, false);
        aq.id(R.id.dq_area_title).getTextView().setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        aq.spinText(R.id.dq_area_spinner, "", false);
        aq.id(R.id.dq_area_spinner).clicked(this, "onClick");
        aq.id(R.id.dq_area_spinner).getView().setPadding(extralinespace, 0, 0, 0);
        aq.id(R.id.dq_area_spinner).background(0);
        setAreaDistcirt();

        aq.line(R.id.dq_line1, R.id.dq_line2, R.id.dq_line3, R.id.dq_line4, R.id.dq_line5, R.id.dq_line6, R.id.dq_line7);

        aq.id(R.id.dq_line1).getView().setPadding(titleWdith + basePadding, 0, 0, 0);
        aq.id(R.id.dq_line2).getView().setPadding(titleWdith + basePadding, 0, 0, 0);
        aq.id(R.id.dq_line3).getView().setPadding(titleWdith + basePadding, 0, 0, 0);
        aq.id(R.id.dq_line4).getView().setPadding(titleWdith + basePadding, 0, 0, 0);
        aq.id(R.id.dq_line5).getView().setPadding(titleWdith + basePadding, 0, 0, 0);
        aq.id(R.id.dq_line7).getView().setPadding(extralinespace, 0, 0, 0);

        aq.id(R.id.dq_line6).visibility(isRemarkShown ? View.GONE : View.VISIBLE);
        aq.id(R.id.dq_line7).visibility(isRemarkShown ? View.GONE : View.VISIBLE);

        aq.id(R.id.dq_btns_layout).getView().setPadding(basePadding, extralinespace, basePadding, extralinespace);
        ((LinearLayout) aq.id(R.id.dq_btns_layout).getView()).setGravity(Gravity.CENTER);
        searchBtn = aq.normTxtBtn(R.id.dq_btns, getResString(R.string.MYHKT_BTN_SEARCH), btnWidth);
        aq.id(R.id.dq_btns).clicked(this, "onClick");

        aq.id(R.id.dq_remarks).getWebView().getSettings().setDefaultTextEncodingName("utf-8");
        aq.id(R.id.dq_remarks).getWebView().loadUrl(isZh ? "file:///android_asset/dq_remarks_zh.html" : "file:///android_asset/dq_remarks.html");
        aq.id(R.id.dq_remarks).visibility(!isRemarkShown ? View.GONE : View.VISIBLE);
        aq.id(R.id.dq_remarks).getWebView().setFocusable(false);

        //Result header
        aq.id(R.id.dq_result_header1).image(R.drawable.rightarrow_small);
        aq.id(R.id.dq_result_header1).getImageView().setScaleType(ScaleType.CENTER);
        aq.id(R.id.dq_result_header1).width(basePadding, false);
        aq.normTextGrey(R.id.dq_result_header2, getResString(R.string.MYHKT_CNT_SEARCHING_HEADER), -2);
        aq.id(R.id.dq_result_header2).getTextView().setGravity(Gravity.CENTER_VERTICAL);
        aq.normText(R.id.dq_result_header3, "");

        aq.id(R.id.dq_result_header3).getTextView().setGravity(Gravity.CENTER_VERTICAL);
        aq.id(R.id.dq_result_header3).getView().setPadding(basePadding, 0, 0, 0);
        aq.id(R.id.dq_result_header_layout).visibility(isRemarkShown ? View.GONE : View.VISIBLE);

        listView = aq.id(R.id.dq_listview).getListView();
        listView.setVisibility(isRemarkShown ? View.GONE : View.VISIBLE);

        if (!isRemarkShown) {
            setSeacrhResult();
        }
        initNameLayout();
        APIsManager.doReadDistrict(this);
    }

    private void setSearchBtn() {
        if (isCompany) {
            if (isNameTextNull) {
                aq.id(R.id.dq_btns).getButton().setEnabled(false);
                int m_color = Color.argb(200, 255, 255, 255);
                aq.id(R.id.dq_btns).getButton().setAlpha(0.2f);
            } else {
                aq.id(R.id.dq_btns).getButton().setEnabled(true);
                aq.id(R.id.dq_btns).getButton().setAlpha(1.0f);
            }
        } else {
            if (isSurnameTextNull && isGivenNameTextNull) {
                aq.id(R.id.dq_btns).getButton().setEnabled(false);
                int m_color = Color.argb(200, 255, 255, 255);
                aq.id(R.id.dq_btns).getButton().setAlpha(0.2f);
            } else {
                aq.id(R.id.dq_btns).getButton().setEnabled(true);
                aq.id(R.id.dq_btns).getButton().setAlpha(1.0f);
            }
        }
    }

    /****************************************
     *
     * ListView
     *
     ****************************************/
    private void addListViewFooter() {

        if (loadingfooter != null) {
            listView.removeFooterView(loadingfooter);
            loadingfooter = null;
        }
        // Create a new footer
        loadingfooter = new RelativeLayout(me);
        int padding = (int) getResources().getDimension(R.dimen.padding_screen);
        TextView loadingText = new TextView(me);
        loadingText.setPadding(padding, padding, padding, padding);
        loadingText.setLayoutParams(new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        loadingText.setGravity(Gravity.CENTER);

        if (!("".equalsIgnoreCase(dqryCra.getOPageInfo().lr.trim()) &&
                "".equalsIgnoreCase(dqryCra.getOPageInfo().en.trim()) &&
                "".equalsIgnoreCase(dqryCra.getOPageInfo().mc.trim())) &&
                ("".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) ||
                        "0".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) ||
                        "7".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) ||
                        "8".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) ||
                        "9".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()))) {
            // add loading footer if there is not the last page for display
            ProgressBar progressBar = new ProgressBar(me, null, android.R.attr.progressBarStyleSmallInverse);
            progressBar.setPadding(padding, padding, padding, padding);
            loadingText.setText(getResString(R.string.MYHKT_DQ_LOADING));

            loadingfooter.addView(progressBar, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        } else if ("4".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) || "5".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) || "6".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim())) {
            // Add end of Record footer
            loadingText.setText(getResString(R.string.MYHKT_DQ_ERR_4));
        }

        loadingfooter.addView(loadingText);
        listView.addFooterView(loadingfooter);
    }

    private void initListView() {
        if (listView == null) {
            listView = aq.id(R.id.dq_listview).getListView();
        }
        listView.setOnScrollListener(new OnScrollListener() {
            int previousVisibleItem = 0;

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int amountVisible, int totalItems) {
                // now get the point where you have to reload data
                directoryInquiryListViewAdapter.lastItem = amountVisible + previousVisibleItem;
                previousVisibleItem = firstVisibleItem;

                if (firstVisibleItem + amountVisible == totalItems) {
                    if (!isSearching && !("".equalsIgnoreCase(dqryCra.getOPageInfo().lr.trim()) && "".equalsIgnoreCase(dqryCra.getOPageInfo().en.trim()) && "".equalsIgnoreCase(dqryCra.getOPageInfo().mc.trim())) && (""
                            .equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) || "0".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) || "7".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()) || "8".equalsIgnoreCase(dqryCra.getOPageInfo().msg
                            .trim()) || "9".equalsIgnoreCase(dqryCra.getOPageInfo().msg.trim()))) {

                        DqryCra sDqryCra = new DqryCra();
                        sDqryCra.setIReq(dqryCra.getIReq().copyMe());
                        sDqryCra.getIReq().pageInfo = dqryCra.getOPageInfo().copyMe();
                        sDqryCra.getIReq().remoteAddr = Utils.getIpAddress();
                        isSearching = true;
                        APIsManager.doDearDqry(me, sDqryCra);
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    directoryInquiryListViewAdapter.enableAnimation = false;
                } else {
                    directoryInquiryListViewAdapter.enableAnimation = true;
                }
            }
        });

        Animation anim = AnimationUtils.loadAnimation(this, R.anim.list_right_fade_in_slow);
        anim.reset();
        listView.clearAnimation();
        listView.startAnimation(anim);
    }

    //To show company or person layout
    public void initNameLayout() {
        isNameTextNull = true;
        isSurnameTextNull = true;
        isGivenNameTextNull = true;
        editTextName.setText("");
        editTextSurname.setText("");
        editTextGivenName.setText("");
        aq.id(R.id.dq_name_layout).visibility(isCompany ? View.VISIBLE : View.GONE);
        aq.id(R.id.dq_line2).visibility(isCompany ? View.VISIBLE : View.GONE);
        aq.id(R.id.dq_surname_layout).visibility(!isCompany ? View.VISIBLE : View.GONE);
        aq.id(R.id.dq_line3).visibility(!isCompany ? View.VISIBLE : View.GONE);
        aq.id(R.id.dq_givename_layout).visibility(!isCompany ? View.VISIBLE : View.GONE);
        aq.id(R.id.dq_line4).visibility(!isCompany ? View.VISIBLE : View.GONE);
    }

    public void setAreaDistcirt() {
        if (areaNames != null && areaNames.length > 0 && districtNames != null && districtNames.length > 0) {
            String area = areaNames[areaPos];
            String district = districtNames[areaPos][districtPos];
            if (getResString(R.string.MYHKT_DQ_SEARCH_ALL).equalsIgnoreCase(district)) {
                aq.id(R.id.dq_area_spinner).text(area);
            } else if (getResString(R.string.MYHKT_DQ_SEARCH_ALL).equalsIgnoreCase(area)) {
                aq.id(R.id.dq_area_spinner).text(district);
            } else {
                aq.id(R.id.dq_area_spinner).text(area + " / " + district);
            }
        } else {
            aq.id(R.id.dq_area_spinner).text(getResString(R.string.MYHKT_DQ_SEARCH_ALL));
        }
    }

    /************************************
     *
     * Search result part
     *
     ***************************************/
    public void setSeacrhResult() {
        addListViewFooter();
        directoryInquiryListViewAdapter = new DirectoryInquiryListViewAdapter(me, dqryCra.getOSrchDtlLst(), this);
        listView.setAdapter(directoryInquiryListViewAdapter);
        aq.id(R.id.dq_result_header3).text(isCompany ? dqryCra.getIReq().bizName : dqryCra.getIReq().fName + " " + dqryCra.getIReq().gName);

        aq.id(R.id.dq_remarks).visibility(View.GONE);
        aq.id(R.id.dq_listview).visibility(View.VISIBLE);
        aq.id(R.id.dq_line6).visibility(View.VISIBLE);
        aq.id(R.id.dq_line7).visibility(View.VISIBLE);
        aq.id(R.id.dq_result_header_layout).visibility(View.VISIBLE);

        isReloading = true;
        initListView();
        if (state != null) {
            listView.onRestoreInstanceState(state);
        }
    }

    public void onClick(View v) {
        //Screen Tracker

        switch (v.getId()) {
            case R.id.navbar_button_left:
                finish();
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                break;
            case R.id.dq_btns:
                FAWrapper.getInstance().sendFAEvents(me, R.string.CONST_GA_CATEGORY_USERLV, R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_DQ, false);

                Utils.closeSoftKeyboard(me, v);
                DQReq rDQReq = new DQReq();

                if (!isCompany) {
                    rDQReq.fName = editTextSurname.getText().toString().trim().replaceAll("滙", "匯").replaceAll("啓", "啟");
                    rDQReq.gName = editTextGivenName.getText().toString().trim().replaceAll("滙", "匯").replaceAll("啓", "啟");
                    rDQReq.perArea = areaPos > 0 ? areaList.get(areaPos - 1).getValue() : "";
                    rDQReq.perDist = districtPos > 0 ? districtList.get(areaPos).get(districtPos - 1).getValue() : "";
                    rDQReq.directory = "R";
                } else {
                    rDQReq.bizName = editTextName.getText().toString().trim().replaceAll("滙", "匯").replaceAll("啓", "啟");
                    rDQReq.bizArea = areaPos > 0 ? areaList.get(areaPos - 1).getValue() : "";
                    rDQReq.bizDist = districtPos > 0 ? districtList.get(areaPos).get(districtPos - 1).getValue() : "";
                    rDQReq.directory = "B";
                }
                rDQReq.lang = "B"; // Keep this value use Chinese only, [B] for chinese, [A] for English

                rDQReq.remoteAddr = Utils.getIpAddress();
                dqryCra = new DqryCra();
                dqryCra.setIReq(rDQReq);
                isSearching = true;
                isReloading = false;
                state = null;
                APIsManager.doDearDqry(me, dqryCra);
                break;
            case R.id.dq_area_spinner:
                ////////wheel action
                final QuickAction pickerWheelAction = new QuickAction(this, 0, 0);

                pickerWheelAction.initPickerWheel(areaNames, districtNames, areaPos, districtPos);
                pickerWheelAction.setOnPickerScrollListener(new QuickAction.OnPickerScrollListener() {
                    @Override
                    public void onPickerScroll(String leftResult, String rightResult, int leftPos, int rightPos) {
                        areaPos = leftPos;
                        districtPos = rightPos;
                        setAreaDistcirt();
                    }
                });
                pickerWheelAction.showPicker(v, aq.id(R.id.dq_frame1).getView());
                break;
            case LRTabButton.leftBTnid:
                Utils.closeSoftKeyboard(me, v);

                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_DQ_BY_COMPANY, false);

                isCompany = true;
                isRemarkShown = true;
                moduleId = getResources().getString(R.string.MODULE_DQ_COMPANY);
                initNameLayout();
                aq.id(R.id.dq_remarks).visibility(View.VISIBLE);
                aq.id(R.id.dq_listview).visibility(View.GONE);
                aq.id(R.id.dq_line6).visibility(View.GONE);
                aq.id(R.id.dq_line7).visibility(View.GONE);
                aq.id(R.id.dq_result_header_layout).visibility(View.GONE);
                aq.id(R.id.dq_listview).getListView().setAdapter(null);
                directoryInquiryListViewAdapter = null;
                btn.setBtns(isCompany);
                break;
            case LRTabButton.rightBTnid:
                Utils.closeSoftKeyboard(me, v);

                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_DQ_BY_PERSON, false);

                isCompany = false;
                isRemarkShown = true;
                moduleId = getResources().getString(R.string.MODULE_DQ_PERSON);
                initNameLayout();
                aq.id(R.id.dq_remarks).visibility(View.VISIBLE);
                aq.id(R.id.dq_listview).visibility(View.GONE);
                aq.id(R.id.dq_line6).visibility(View.GONE);
                aq.id(R.id.dq_line7).visibility(View.GONE);
                aq.id(R.id.dq_result_header_layout).visibility(View.GONE);
                aq.id(R.id.dq_listview).getListView().setAdapter(null);
                directoryInquiryListViewAdapter = null;
                btn.setBtns(isCompany);
                break;
        }
    }

    @Override
    protected final void onResume() {
        super.onResume();
        moduleId = isCompany ? getResources().getString(R.string.MODULE_DQ_COMPANY) : getResources().getString(R.string.MODULE_DQ_PERSON);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_DQ, false);
    }

    @Override
    protected final void onPause() {
        nameInput = editTextName.getEditableText().toString();
        surnameInput = editTextSurname.getEditableText().toString();
        givenNameInput = editTextGivenName.getEditableText().toString();
        state = listView.onSaveInstanceState();
        super.onPause();
    }

    @Override
    protected final void onStop() {
        super.onStop();
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    /****************************************
     *
     * Handle the dq data
     *
     ***************************************/
    public void prepareAreaDistrict(DQAreas dQAreas) {
        int areaSize = dQAreas.areas.length;
        areaNames = new String[areaSize + 1];
        areaList = new ArrayList<DQArea>();
        districtList = new ArrayList<List<DQDistrict>>();
        districtNames = new String[areaSize + 1][];
        //Add area all
        areaNames[0] = getResString(R.string.MYHKT_DQ_SEARCH_ALL);
        //Add disrtict for area all
        List<DQDistrict> allDistrict = new ArrayList<DQDistrict>();
        districtList.add(allDistrict);
        for (int i = 0; i < areaSize; i++) {
            areaList.add(dQAreas.areas[i]);
            areaNames[i + 1] = isZh ? dQAreas.areas[i].cname : dQAreas.areas[i].ename;
            int districtSize = dQAreas.areas[i].districts.length;
            List<DQDistrict> districts = new ArrayList<DQDistrict>();
            for (int j = 0; j < districtSize; j++) {
                districts.add(dQAreas.areas[i].districts[j]);
                allDistrict.add(dQAreas.areas[i].districts[j]);
            }
            sortDistrict(districts);
            districtList.add(districts);
        }
        sortDistrict(allDistrict);
        districtList.set(0, allDistrict);

        for (int i = 0; i < districtList.size(); i++) {
            String[] districtArray = new String[districtList.get(i).size() + 1];
            districtArray[0] = getResString(R.string.MYHKT_DQ_SEARCH_ALL);
            districtNames[i] = new String[districtList.get(i).size() + 1];
            districtNames[i][0] = getResString(R.string.MYHKT_DQ_SEARCH_ALL);

            for (int j = 0; j < districtList.get(i).size(); j++) {
                districtArray[j + 1] = isZh ? districtList.get(i).get(j).cname : districtList.get(i).get(j).ename;
                districtNames[i][j + 1] = isZh ? districtList.get(i).get(j).cname : districtList.get(i).get(j).ename;
            }
        }
    }

    private void sortDistrict(List<DQDistrict> dList) {
        // Sort district list alphabetically
        Collections.sort(dList, new Comparator<DQDistrict>() {
            @Override
            public int compare(DQDistrict o1, DQDistrict o2) {
                return o1.ename.compareTo(o2.ename);
            }
        });
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);

        if (response != null) {
            if (debug) Log.i(TAG, "Search");
            if ((response.getActionTy() != null && response.getActionTy().equals(APIsManager.DQRY))) {
                isSearching = false;
                rDqryCra = (DqryCra) response.getCra();
                if (debug) Log.i(TAG, new Gson().toJson(rDqryCra));
                if ("0".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim()) || "1".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim()) || "2".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim()) || "3"
                        .equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim()) || "8".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim()) || "9".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim()) || "10"
                        .equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim()) || "11".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim())) {
                    // Display Error Message Only
                    displayDialog(me, InterpretRCManager.interpretDQ_Mdu(me, rDqryCra.getOPageInfo().msg.trim(), response.getReply().getCode()));
                } else {
                    if (debug) Log.i(TAG, "im" + rDqryCra.getOPageInfo().msg);
                    isRemarkShown = false;
                    if ("7".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim())) {
                        // Display Error Message, then redirect Page after confirm ok
                        displayDialog(me, InterpretRCManager.interpretDQ_Mdu(me, rDqryCra.getOPageInfo().msg.trim(), response.getReply().getCode()));
                    }
                    dqryCra.setOPageInfo(rDqryCra.getOPageInfo().copyMe());
                    for (int i = 0; i < rDqryCra.getOSrchDtlLst().size(); i++) {
                        dqryCra.getOSrchDtlLst().add(rDqryCra.getOSrchDtlLst().get(i));
                    }

                    if (!isReloading) {
                        //A new Search
                        setSeacrhResult();
                    } else {
                        //Reload Search
                        addListViewFooter();
                        if (!rDqryCra.getOSrchDtlLst().isEmpty()) {
                            directoryInquiryListViewAdapter.addDirectoryInquiryRec(rDqryCra.getOSrchDtlLst());
                        }
                        directoryInquiryListViewAdapter.notifyDataSetChanged();
                    }

                    if (!"".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim()) && !"4".equalsIgnoreCase(rDqryCra.getOPageInfo().msg.trim())) {
                        // Go to the Top of the List if any Error
                        listView.setSelection(0);
                        displayDialog(me, InterpretRCManager.interpretDQ_Mdu(me, rDqryCra.getOPageInfo().msg.trim(), response.getReply().getCode()));
                    }
                }
            } else {
                DQAreas dQAreas = (DQAreas) response.getCra();
                prepareAreaDistrict(dQAreas);
            }
        }
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);

        if (response != null) {
            if ((response.getActionTy() != null && response.getActionTy().equals(APIsManager.DQRY))) {
                isSearching = false;
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    displayDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    ivSessDialog();
                } else {
                    DqryCra rDqryCra = (DqryCra) response.getCra();
                    if (debug) Log.i(TAG, "Code");
                    displayDialog(me, InterpretRCManager.interpretDQ_Mdu(me, rDqryCra.getOPageInfo().msg.trim(), response.getReply().getCode()));
                }
            } else
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    displayDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    ivSessDialog();
                } else {
                    displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCallPhoneNumber(String phoneNum) {
        this.phoneNum = phoneNum;
        if (RuntimePermissionUtil.isActionCallPermissionGranted(this)) {
            Intent dialIntent = new Intent(Intent.ACTION_CALL, Uri.parse(phoneNum));
            startActivity(dialIntent);
        } else {
            if (ClnEnv.getPref(this, Constant.CALL_PHONE_PERMISSION_DENIED, false)) {
                displayDialog(this, getString(R.string.permission_denied_setting_enabled));
            } else {
                RuntimePermissionUtil.requestCallPhonePermission(this);
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constant.REQUEST_CALL_PHONE_PERMISSION) {
            //Permission is granted
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && !TextUtils.isEmpty(phoneNum)) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(String.format(Utils.getString(this, R.string.SUPPORT_PREMIER_TEL_FORMAT), phoneNum)));
                startActivity(intent);
            } else {
                displayDialog(this, getString(R.string.permission_denied));
                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowCallPhoneRequestPermission(this)) {
                    ClnEnv.setPref(getApplicationContext(), Constant.CALL_PHONE_PERMISSION_DENIED, true);
                }
            }
        }
    }
}
