package com.pccw.myhkt.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;

/************************************************************************
 * File : BrowserActivity.java
 * Desc : Web Page
 * Name : BrowserActivity
 * by 	: Andy Wong
 * Date : 22/12/2015
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 22/12/2015 Andy Wong 		-First draft
   01/02/2016 Derek Tsui		-add logic
   22/02/2016 Derek Tsui		-usable condition, but need more polishing and throw away junks
 *************************************************************************/
public class PPSActivity extends BaseActivity {
	private String TAG = this.getClass().getName();
	public final static String BUNDLEURL = "bundleurl";
	public final static String BUNDLETITLE = "bundletitle";
	private boolean debug = false;
	private String ppsURL = null;
	private String title = null;

	private AAQuery aq;
	private WebView webView;

	private AcctAgent acctAgent;
	private String acctnum;
	private int lob = 0;
	private int ltsType = 0;
	private String[] billSumDetail = new String[3];
	private String navbarText = "";
	private int index = 0;
	private boolean isLOBPCDTV = false;

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		debug = getResources().getBoolean(R.bool.DEBUG);
		ppsURL = Utils.getString(me, R.string.myhkt_pps_url);
		ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
		setContentView(R.layout.activity_pps);

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			acctnum = bundle.getString("ACCTNUM") != null ? bundle.getString("ACCTNUM") : "";
			String amount = bundle.getString("AMOUNT") != null ? bundle.getString("AMOUNT") : "";
			lob = bundle.getInt("LOB") != 0 ? bundle.getInt("LOB") : 0;
			loadData(acctnum, lob, amount);
		} else {
			loadData("", 0, "");
		}
		setModuleID();
	}

	@Override
	public void onResume() {
		super.onResume();
		//Screen Tracker
		FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_PPS, true);
	}

	public void onBackPressed() {
		displayConfirmExitDialog();
	}

	// Restore the Bundle data when re-built Activity
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		index = savedInstanceState.getInt("INDEX");
		billSumDetail = savedInstanceState.getStringArray("STRINGARY");
		super.onRestoreInstanceState(savedInstanceState);
	}

	// Saved data before Destroy Activity
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("INDEX", index);
		outState.putStringArray("STRINGARY", billSumDetail);
		super.onSaveInstanceState(outState);
	}

	protected void initUI2() {
		aq = new AAQuery(this);
		// Navbar
		aq.navBarTitleLob(lob,R.id.navbar_title, R.id.navbar_leftdraw, 0, R.drawable.lob_pps, 0, getString(R.string.myhkt_pps_paybypps));
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
		aq.id(R.id.pps_footbar_doublearrow_right).clicked(this, "onClick");
		aq.id(R.id.pps_footbar_doublearrow_left).clicked(this, "onClick");
		aq.id(R.id.pps_footbar_Txt).clicked(this, "onClick");

		//Set status bar color
		if (ClnEnv.isIs101Flag()) {
			this.getWindow().setStatusBarColor(getResources().getColor(R.color.black));
		} else if (ClnEnv.isMyMobFlag() && lob == R.string.CONST_LOB_CSP) {
			this.getWindow().setStatusBarColor(getResources().getColor(R.color.mymob_club_sim));
		}
		updatePPSBarText();
	}

	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.navbar_button_left:
				onBackPressed();
				break;
			case R.id.pps_footbar_doublearrow_left:
				index = (index - 1 + 3) % 3;

				if (index == 1 && !isLOBPCDTV) {
					// Show bill type for PCD & TV Only
					index = 1;
				}
				updatePPSBarText();
				break;
			case R.id.pps_footbar_doublearrow_right:
				index = (index + 1) % 3;

				if (index == 2 && !isLOBPCDTV) {
					// Show bill type for PCD & TV Only
					index = 2;
				}
				updatePPSBarText();
				break;
			case R.id.pps_footbar_Txt:
				if (index == 1) {
					// Copy to clipboard only supports Account No.
					ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
					cm.setText(acctnum);
					DialogHelper.createSimpleDialog(this, getString(R.string.myhkt_pps_copyacctnum));
				}
				break;
		}
	}

	private final void loadData(String mAcctNum, int mLOB, String mAmount) {
		aq = new AAQuery(this);
		String billtype;
		String mechCode;
		if (mLOB == R.string.CONST_LOB_LTS) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_lts);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_MOB || mLOB == R.string.CONST_LOB_IOI) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_mob);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_NE) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_mob);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_PCD) {
			billtype = Utils.getString(me, R.string.myhkt_pps_billtype_pcd);
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_pcd);
			isLOBPCDTV = true;
		} else if (mLOB == R.string.CONST_LOB_TV) {
			billtype = Utils.getString(me, R.string.myhkt_pps_billtype_pcd);
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_pcd);
			isLOBPCDTV = true;
		} else if (mLOB == R.string.CONST_LOB_1010) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_1010);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_O2F) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_o2f);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_CSP) {
			billtype = Utils.getString(me, R.string.myhkt_pps_billtype_csp);
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_csp);
			isLOBPCDTV = false;
		} else {
			// Unknown Case
			billtype = "";
			mechCode = "";
			isLOBPCDTV = false;
		}

		billSumDetail[0] = String.format("%s %s", Utils.getString(me, R.string.myhkt_pps_merchantcode), mechCode);
		if (mLOB == R.string.CONST_LOB_TV || mLOB == R.string.CONST_LOB_PCD) {
			billSumDetail[1] = String.format("%s %s", Utils.getString(me, R.string.pcd_summary_recacct_num), mAcctNum);
		} else {
			billSumDetail[1] = String.format("%s %s", Utils.getString(me, R.string.qrcode_acc_no_txt), mAcctNum);

		}
//		billSumDetail[2] = ("".equalsIgnoreCase(billtype)) ? "" : String.format("%s %s", Utils.getString(me, R.string.qrcode_bill_type_mobile_txt), billtype);
		billSumDetail[2] = String.format("%s %s", Utils.getString(me, R.string.qrcode_amt_txt), mAmount);

		// Loading WebView once OnCreate
		webView = aq.id(R.id.pps_webview).getWebView();
		//		webView.loadUrl(ppsURL);
		//		viewholder.pps_webview = (WebView) findViewById(R.id.pps_webview);

		WebSettings websettings = webView.getSettings();
		websettings.setSupportZoom(true);
		websettings.setBuiltInZoomControls(true);
		websettings.setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				//				progressDialogHelper.dismissProgressDialog(true);
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				//				progressDialogHelper.showProgressDialog(me, me.onstopHandler, true);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
				//				progressDialogHelper.dismissProgressDialog(true);
			}
		});
		//		progressDialogHelper.showProgressDialog(me, me.onstopHandler, true);
		webView.loadData("LOADING.....", "", "");

		DialogHelper.createSimpleDialog(this, getString(R.string.myhkt_pps_enterweb), getString(R.string.btn_ok), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				webView.loadUrl(ppsURL);
			}
		});
	}

	private void updatePPSBarText() {
		aq = new AAQuery(this);

		if (index == 1) {
			aq.id(R.id.pps_footbar_Txt).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pps_btn_copy, 0);
			// viewholder.pps_footbar_Txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pps_btn_copy, 0);

		} else {
			aq.id(R.id.pps_footbar_Txt).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			// viewholder.pps_footbar_Txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}

		if (index < billSumDetail.length) {
			aq.id(R.id.pps_footbar_Txt).text(billSumDetail[index]);
			// viewholder.pps_footbar_Txt.setText(billSumDetail[index]);
		}
	}

	protected void setModuleID() {
		//Module id
		switch (lob) {
		case R.string.CONST_LOB_LTS:
			moduleId = getResString(R.string.MODULE_LTS_BILL);
			break;
		case R.string.CONST_LOB_PCD:
			moduleId = getResString(R.string.MODULE_PCD_BILL);
			break;
		case R.string.CONST_LOB_TV:
			moduleId = getResString(R.string.MODULE_TV_BILL);
			break;
		case R.string.CONST_LOB_O2F:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_O2F_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_O2F_BILL);
			}
			break;
		case R.string.CONST_LOB_MOB:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_MOB_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_MOB_BILL);
			}
			break;
		case R.string.CONST_LOB_IOI:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_IOI_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_IOI_BILL);
			}
			break;
		case R.string.CONST_LOB_1010:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_101_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_101_BILL);
			}
			break;	
		}			
	}

	private int getLobIcon() {
		int lobType = acctAgent.getLobType();
		if (lobType == R.string.CONST_LOB_1010 || lobType == R.string.CONST_LOB_IOI) {
			return R.drawable.logo_1010;
		} else if (lobType == R.string.CONST_LOB_MOB || lobType == R.string.CONST_LOB_O2F) {
			return R.drawable.logo_csl;
		} else if (lobType == R.string.CONST_LOB_LTS) {
			switch (ltsType) {
			case R.string.CONST_LTS_FIXEDLINE:
				return R.drawable.logo_fixedline_residential;
			case R.string.CONST_LTS_EYE:
				return R.drawable.logo_fixedline_eye;
			case R.string.CONST_LTS_IDD0060:
				return R.drawable.logo_fixedline_0060;
			case R.string.CONST_LTS_CALLINGCARD:
				return R.drawable.logo_fixedline_global;
			case R.string.CONST_LTS_ONECALL:
				return R.drawable.logo_fixedline_onecall;
			case R.string.CONST_LTS_ICFS:
				return R.drawable.logo_fixedline_icfs;
			default:
				return R.drawable.logo_fixedline;
			}
		} else if (lobType == R.string.CONST_LOB_PCD) {
			return R.drawable.logo_netvigator;
		} else if (lobType == R.string.CONST_LOB_TV) {
			return R.drawable.logo_now;
		}
		return 0;
	}

	// Confirm Exit
	protected final void displayConfirmExitDialog() {
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage(Utils.getString(me, R.string.myhkt_pps_exitweb));
		builder.setPositiveButton(Utils.getString(me, R.string.myhkt_btn_no), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.setNegativeButton(Utils.getString(me, R.string.myhkt_btn_yes), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finish();
				overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			}
		});	
		builder.create().show();
	}
}
