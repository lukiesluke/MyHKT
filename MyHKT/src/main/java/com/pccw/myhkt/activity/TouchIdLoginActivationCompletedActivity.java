package com.pccw.myhkt.activity;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;

/************************************************************************
 * File : TouchIdLoginActivationCompletedActivity.java 
 * Desc : Touch Id Login Activation Complete is the page after successful Touch Id login activation 
 * Name : TouchIdLoginActivationCompletedActivity
 * by : Abdulmoiz Esmail 
 * Date : 25/10/2017
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 25/10/2017 Abdulmoiz Esmail  - First Draft
 *************************************************************************/
public class TouchIdLoginActivationCompletedActivity extends Activity {
	
	TextView mTitle;
	HKTButton mDoneButton;
	
	private AAQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_touch_id_login_activation_completed);
      
        aq = new AAQuery(this);
        
        mDoneButton = (HKTButton) aq.id(R.id.button_done).getView();
        mDoneButton.initViews(this, getResources().getString(R.string.fp_activate_comp_btn_done), LayoutParams.MATCH_PARENT);
        
        mTitle = (TextView) findViewById(R.id.main_view_title);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "Roboto-Light.ttf");	
        mTitle.setTypeface(face);

    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Screen Tracker
        FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_FP_ACTIVATION_COMPLETE, false);
    }

    public void onDoneButtonClicked(View view) {
    	setResult(RESULT_OK);
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }
    
    @Override
    public void onBackPressed() {
    	setResult(RESULT_OK);
    	finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    	super.onBackPressed();
    }
}
