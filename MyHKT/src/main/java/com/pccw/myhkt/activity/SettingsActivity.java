package com.pccw.myhkt.activity;

import static com.pccw.myhkt.util.Constant.KEY_SHARED_APP_CONFIG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.SpssCra;
import com.pccw.dango.shared.entity.SpssRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRButton;
import com.pccw.myhkt.lib.ui.LRButton.OnLRButtonClickListener;
import com.pccw.myhkt.lib.ui.PopOverInputView;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.model.AppConfig;
import com.pccw.myhkt.service.QueryAppointmentService;
import com.pccw.myhkt.util.Constant;

import java.util.Locale;
import java.util.Objects;

/************************************************************************
 File       : SettingsActivity.java
 Desc       : Setting Screen
 Name       : SettingsActivity
 Created by : Derek Tsui
 Date       : 16/12/2015

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 16/12/2015 Derek Tsui			- First draft
 09/11/2017 Abdulmoiz Esmail     - Added Touch Id setting
 *************************************************************************/

public class SettingsActivity extends BaseActivity implements OnLRButtonClickListener {
    private final String TAG = this.getClass().getName();
    // Common Components
    private boolean debug = false;            // toggle debug logs
    private static SettingsActivity me;
    private AAQuery aq;
    private LRButton settings_lrbtn;
    private int extralinespace = 0;
    private PopOverInputView settings_popover_input;
    private QuickAction mQuickAction; //popover view
    // GCM
    private Intent chkappt = null;
    final int PERMISSION_REQUEST_CODE = 112;
    boolean enableNotification;
    private AppConfig appConfig;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        me = this;
        chkappt = null;
        debug = getResources().getBoolean(R.bool.DEBUG);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        setContentView(R.layout.activity_settings);

        if (Build.VERSION.SDK_INT > 32) {
            if (!shouldShowRequestPermissionRationale(String.valueOf(PERMISSION_REQUEST_CODE))) {
                getNotificationPermission();
            }
        }
        APIsManager.requestAppConfigJSON(this, this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getNotificationPermission();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        super.onStart();
        initData();
        initUI();
    }

    @Override
    protected final void onResume() {
        super.onResume();
        moduleId = getResources().getString(R.string.MODULE_SETTING);
        //Screen Tracker
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_SETTINGS, false);
    }

    @Override
    protected final void onStop() {
        super.onStop();
        if (chkappt != null) {
            ClnEnv.setApptCra(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (broadcastReceiver != null) {
                this.unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            // lineTestBroadcastReceiver is already unregistered
            broadcastReceiver = null;
        }
    }

    // Custom broadcast receiver after images are downloaded
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constant.ACTION_SESSION_TIMEOUT.equalsIgnoreCase(intent.getAction())) {
                BaseActivity.ivSessDialog();
            }
        }
    };

    // Android Device Back Button Handling
    public final void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private void initData() {
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
    }

    private void initUI() {
        aq = new AAQuery(this);

        // Navbar
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_settings));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        settings_lrbtn = (LRButton) aq.id(R.id.settings_lrbtn).getView();
        settings_lrbtn.initViews(this, "中文", "English");
        settings_lrbtn.setPadding(extralinespace, 0, extralinespace, extralinespace);

        if (ClnEnv.getAppLocale(getApplicationContext()).toLowerCase(Locale.US).startsWith(getResString(R.string.CONST_LOCALE_ZH))) {
            settings_lrbtn.setBtns(true);
        } else {
            settings_lrbtn.setBtns(false);
        }

        //Display Lang
        aq.id(R.id.settings_dislang_text).text(getResString(R.string.PAMF_LANG));
        aq.id(R.id.settings_dislang_text).textSize(getResources().getInteger(R.integer.textsize_default_int));

        //new bill indicator checkbox
        aq.id(R.id.settings_checkbox_newbill).getCheckBox().setButtonDrawable(R.drawable.checkbox);
        aq.id(R.id.settings_checkbox_newbill).checked(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true));
        aq.id(R.id.settings_checkbox_newbill).getCheckBox().setOnCheckedChangeListener(onCheckedChangeListener);
        aq.id(R.id.settings_newbill_text).text(getResString(R.string.myhkt_settings_newbill));
        aq.id(R.id.settings_newbill_text).textSize(getResources().getInteger(R.integer.textsize_default_int));

        //Notification checkbox
        aq.id(R.id.settings_LBL_NOTIFICATION_text).text(getResString(R.string.MYHKT_SETTING_LBL_NOTIFICATION));
        aq.id(R.id.settings_LBL_NOTIFICATION_text).textSize(getResources().getInteger(R.integer.textsize_default_int));
        aq.id(R.id.settings_checkbox_noti).getCheckBox().setButtonDrawable(R.drawable.checkbox);
        int playServiceResultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y").equalsIgnoreCase("Y") && playServiceResultCode == ConnectionResult.SUCCESS) {
            aq.id(R.id.settings_checkbox_noti).checked(true);
        } else {
            aq.id(R.id.settings_checkbox_noti).checked(false);
        }
        aq.id(R.id.settings_checkbox_noti).getCheckBox().setOnCheckedChangeListener(onCheckedChangeListener);

        //appointment alert
        aq.id(R.id.settings_appt_text).text(getResString(R.string.myhkt_settings_appt));
        aq.id(R.id.settings_appt_text).textSize(getResources().getInteger(R.integer.textsize_default_int));
        Drawable drawable = ContextCompat.getDrawable(this, ClnEnv.getSessionPremierFlag() ? R.drawable.icon_appointment_alert_premierx60 : R.drawable.icon_appointment_alertx60);
        assert drawable != null;
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        aq.id(R.id.settings_appt_text).getTextView().setCompoundDrawables(null, null, drawable, null);

        settings_popover_input = aq.popOverInputView(R.id.settings_popover_input, getResources().getString(R.string.myhkt_settings_appt_3days));
        ActionItem[] actionItem = new ActionItem[3];
        actionItem[0] = new ActionItem(3, getResources().getString(R.string.myhkt_settings_appt_3days));
        actionItem[1] = new ActionItem(5, getResources().getString(R.string.myhkt_settings_appt_5days));
        actionItem[2] = new ActionItem(7, getResources().getString(R.string.myhkt_settings_appt_7days));
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        mQuickAction = new QuickAction(this, metrics.widthPixels / 2, 0);
        for (ActionItem item : actionItem) {
            mQuickAction.addActionItem(item);
        }

        //setup the action item click listener
        mQuickAction.setOnActionItemClickListener((quickAction, pos, actionId) -> {
            ActionItem actionItem1 = quickAction.getActionItem(pos);
            aq.id(R.id.popover_input_layouttxt).text(actionItem1.getTitle());

            if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_DAYS), 3) != actionItem1.getActionId()) {
                ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_DAYS), actionItem1.getActionId());
                if (ClnEnv.getApptCra() == null) {
                    chkappt = new Intent(me, QueryAppointmentService.class);
                    startService(chkappt);
                } else {
                    filterFromCachedAppointment();
                }
            }
        });

        aq.id(R.id.settings_popover_input).clicked(this, "onClick");
        //init appointment selection
        ActionItem action = mQuickAction.getActionItemById(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_DAYS), 3));
        String apptDaysString = action != null ? action.getTitle() : getResources().getString(R.string.myhkt_settings_appt_3days);
        aq.id(R.id.popover_input_layouttxt).text(apptDaysString);

        //touch id indicator checkbox
        aq.id(R.id.settings_checkbox_touch_id).getCheckBox().setButtonDrawable(R.drawable.checkbox);
        aq.id(R.id.settings_checkbox_touch_id).checked(Utils.isTouchIdLoginEnabled(getApplicationContext()));
        aq.id(R.id.settings_checkbox_touch_id).getCheckBox().setOnCheckedChangeListener(onCheckedChangeListener);
        aq.id(R.id.settings_touch_id_text).text(getResString(R.string.fp_setting_fp_title));
        aq.id(R.id.settings_touch_id_text).textSize(getResources().getInteger(R.integer.textsize_default_int));

        if (!ClnEnv.isLoggedIn()) {
            aq.id(R.id.settings_newbill_layout).visibility(View.GONE);
            aq.id(R.id.settings_appt_layout).visibility(View.GONE);
        } else {
            aq.id(R.id.settings_newbill_layout).visibility(View.VISIBLE);
            aq.id(R.id.settings_appt_layout).visibility(View.VISIBLE);
        }

        if (Utils.isToShowTouchID(this)) {
            aq.id(R.id.settings_touch_id_layout).visibility(View.VISIBLE);
            if (Utils.isTouchIDLoginActivated(getApplicationContext()) && !Utils.isTouchIdUserEqualsToLoggedInUser(getApplicationContext())) {
                aq.id(R.id.settings_checkbox_touch_id).getCheckBox().setEnabled(false);
            }
        } else {
            aq.id(R.id.settings_touch_id_layout).visibility(View.GONE);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(Constant.ACTION_SESSION_TIMEOUT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(broadcastReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
        } else {
            registerReceiver(broadcastReceiver, filter);
        }
    }

    private void filterFromCachedAppointment() {
        ApptCra apptCra = ClnEnv.getApptCra();
        Utils.filterApptToShowClock(SettingsActivity.me, apptCra);
        sendBroadcast(new Intent().setAction(Constant.ACTION_CACHED_APPT_RESPONSE).setPackage(getPackageName()));
    }

    //on checkbox check listener
    OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
        @SuppressLint("NonConstantResourceId")
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.settings_checkbox_noti:
                    if (isChecked) {
                        if (Build.VERSION.SDK_INT > 32) {
                            getNotificationPermission();

                            if (!enableNotification) {
                                aq.id(R.id.settings_checkbox_noti).checked(false);
                                displayDialog(buttonView.getContext(), Utils.getString(me, R.string.MYHKT_NOTIFICATION_DISABLE));
                                break;
                            }
                        }
                        //Check if Google Play Services available
                        int playServiceResultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(me);
                        if (playServiceResultCode == ConnectionResult.SUCCESS) {
                            doUpdateSmartPhone();
                        } else {
                            //NETWORK_ERROR
                            //LICENSE_CHECK_FAILED
                            //INVALID_ACCOUNT
                            //SERVICE_DISABLED
                            //SERVICE_MISSING
                            aq.id(R.id.settings_checkbox_noti).checked(false);
                            try {
                                Objects.requireNonNull(GooglePlayServicesUtil.getErrorDialog(playServiceResultCode, me, -1)).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(me) == ConnectionResult.SUCCESS) {
                            doUpdateSmartPhone();
                        }
                    }
                    break;
                case R.id.settings_checkbox_newbill:
                    if (isChecked) {
                        ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true);
                    } else {
                        ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_NEWBILLIND_FLAG), false);
                    }
                    break;
                case R.id.settings_checkbox_touch_id:
                    aq.id(R.id.settings_checkbox_touch_id).getCheckBox().setChecked(Utils.isTouchIdLoginEnabled(getApplicationContext()));
                    if (isChecked) {
                        if (!Utils.isTouchIDLoginActivated(getApplicationContext())) {
                            if (Utils.isTouchIDEnrolledByUser(SettingsActivity.this)) {
                                Intent intent = new Intent(SettingsActivity.this, TouchIdLoginActivationActivity.class);
                                startActivity(intent);
                            } else {
                                displayDialog(me, getString(R.string.fp_setting_not_enrolled_alert_msg));
                            }
                        } else {
                            aq.id(R.id.settings_checkbox_touch_id).getCheckBox().setChecked(isChecked);
                            Utils.setTouchIdLoginEnabled(getApplicationContext(), true);
                        }
                    } else {
                        DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Clear Touch ID credentials and flags
                                Utils.clearTouchIdUserDetails(getApplicationContext());
                                Utils.setTouchIdLoginEnabled(getApplicationContext(), false);
                                Utils.setTouchIDLoginActivated(getApplicationContext(), false);
                                aq.id(R.id.settings_checkbox_touch_id).getCheckBox().setChecked(false);
                            }
                        };
                        DialogInterface.OnClickListener onNegativeClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //TOOD: Set to check with out passing the onChange listener
                            }
                        };

                        if (Utils.isTouchIdLoginEnabled(getApplicationContext())) {
                            DialogHelper.createTitleDialog(me, getString(R.string.fp_setting_off_alert_title), getString(R.string.fp_setting_off_alert_msg), getString(R.string.fp_failure_lock_btn_confirm), onPositiveClickListener, getString(R.string.fp_login_cancel), onNegativeClickListener);
                        }
                    }
                    break;
            }
        }
    };

    //on view click listener
    @SuppressLint("NonConstantResourceId")
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.navbar_button_right:
                //livechat
                break;
            case R.id.settings_popover_input:
                mQuickAction.show(v, aq.id(R.id.settings_parentlayout).getView());
                mQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.settings_help_newbill:
                displayDialog(me, getString(R.string.MYHKT_SETTING_HINT_NEW_BILL));
                break;
            case R.id.settings_help_appt:
                displayDialog(me, getString(R.string.MYHKT_SETTING_HINT_NEW_APPT));
                break;
            case R.id.settings_help_noti:
                displayDialog(me, getResString(R.string.MYHKT_SETTING_HINT_NOTIFICATION));
                break;
            case R.id.settings_help_touch_id:
                if (Utils.isTouchIDLoginActivated(getApplicationContext())) {
                    if (Utils.isTouchIdUserEqualsToLoggedInUser(getApplicationContext())) {
                        displayDialog(me, getString(R.string.fp_setting_fp_hint_normal));
                    } else {
                        displayDialog(me, getString(R.string.fp_setting_fp_hint_duplicated));
                    }
                } else {
                    displayDialog(me, getString(R.string.fp_setting_fp_hint_normal));
                }
                break;
        }
    }

    @Override
    public void onLRBtnLeftClick() {
        settings_lrbtn.setBtns(true);
        ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LOCALE), getString(R.string.CONST_LOCALE_ZH));
        doUpdateSmartPhone();
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        initData();
        initUI();
        setLiveChangeIcon(false);
        downloadAppConfigImages();
    }

    @Override
    public void onLRBtnRightClick() {
        settings_lrbtn.setBtns(false);
        ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LOCALE), getString(R.string.CONST_LOCALE_EN));
        doUpdateSmartPhone();
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        initData();
        initUI();
        setLiveChangeIcon(false);
        downloadAppConfigImages();
    }

    private void downloadAppConfigImages() {
        String appConfigString = ClnEnv.getPref(getApplicationContext(), KEY_SHARED_APP_CONFIG, getResString(R.string.appConfig));
        appConfig = gson.fromJson(appConfigString, AppConfig.class);
        APIsManager.requestDownloadImage(this, this, appConfig, ClnEnv.getAppLocale(getBaseContext()));
    }

    //Update Push Notification Settings AsyncTask
    private void doUpdateSmartPhone() {
        aq = new AAQuery(this);

        SpssRec spssRec = new SpssRec();
        if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false) && ClnEnv.isLoggedIn()) {                            //Bill Notificatiion "Y" if password is saved
            spssRec.bni = "Y";
        } else {
            spssRec.bni = "N";
        }
        spssRec.gni = aq.id(R.id.settings_checkbox_noti).isChecked() ? "Y" : "N";
        spssRec.lang = ClnEnv.getAppLocale(me.getBaseContext());

        SpssCra spssCra = new SpssCra();
        spssCra.setISpssRec(spssRec);

        APIsManager.doUpdSmph(this, spssCra);
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        aq = new AAQuery(this);
        if (response != null) {
            if (APIsManager.SPSS_UPD.equals(response.getActionTy())) {
                if (!RC.SPSS_IV_DID.equalsIgnoreCase(response.getReply().getCode())) {
                    if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                        displayDialog(this, response.getMessage());
                    } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                        ivSessDialog();
                    } else {
                        displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                    }
                }
                if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y").equalsIgnoreCase("Y")) {
                    aq.id(R.id.settings_checkbox_noti).checked(true);
                } else {
                    aq.id(R.id.settings_checkbox_noti).checked(false);
                }
            } else {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    displayDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    ivSessDialog();
                } else {
                    displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
            }
        }
    }

    public void onSuccess(APIsResponse response) {
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy())) {
                debugLog(TAG, "doHelo complete!!");
            } else if (APIsManager.SPSS_UPD.equals(response.getActionTy())) {
                SpssCra spsscra = (SpssCra) response.getCra();
                // Display Message if Notification CheckBox is OFF and user changed CheckBox selection
                if (!spsscra.getISpssRec().gni.equalsIgnoreCase(ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y"))) {
                    if ("N".equalsIgnoreCase(spsscra.getISpssRec().gni)) {
                        displayDialog(this, Utils.getString(me, R.string.MYHKT_PN_MSG_BILL_PN_ALWAYS_ON));
                    } else if ("Y".equalsIgnoreCase(spsscra.getISpssRec().gni)) {
                        displayDialog(this, Utils.getString(me, R.string.MYHKT_PN_MSG_NTC_PN_ON));
                    }
                }

                if ("Y".equalsIgnoreCase(spsscra.getISpssRec().gni)) {
                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
                } else {
                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "N");
                }

                if (ClnEnv.getLoginId() != null) {
                    //temporary "update profile" call
                    AcMainCra acMainCra = new AcMainCra();
                    acMainCra.setILoginId(ClnEnv.getLoginId());
                    SveeRec sveeRec = Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec();
                    SveeRec mSveeRec = sveeRec.copyMe();
                    mSveeRec.lang = ClnEnv.getAppLocale(me.getBaseContext());
                    acMainCra.setISveeRec(mSveeRec);
                    APIsManager.doUpdProfile(this, null, acMainCra);
                }
            } else if (APIsManager.ACMAIN.equals(response.getActionTy())) {
                AcMainCra acMainCra = (AcMainCra) response.getCra();
                Objects.requireNonNull(ClnEnv.getQualSvee()).setSveeRec(acMainCra.getOSveeRec());
            } else if (APIsManager.API_APP_CONFIG.equals(response.getActionTy())) {
                // Update AppConfig data model class
                appConfig = (AppConfig) response.getCra();
            }
        }
    }

    public void getNotificationPermission() {
        try {
            if (Build.VERSION.SDK_INT > 32) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.POST_NOTIFICATIONS}, PERMISSION_REQUEST_CODE);
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // allow
                enableNotification = true;
            } else {
                //deny
                enableNotification = false;
            }
        }
    }
}
