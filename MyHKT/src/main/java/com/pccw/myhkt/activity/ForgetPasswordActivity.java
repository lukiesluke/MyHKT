package com.pccw.myhkt.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.LRButton;
import com.pccw.myhkt.lib.ui.LRButton.OnLRButtonClickListener;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.wheat.shared.tool.Reply;

import java.util.Locale;

/************************************************************************
 * File : ForgetPasswordActivity.java 
 * Desc : Forgot Password Or Login ID 
 * Name : ForgetPasswordActivity
 * by : Henry Law 
 * Date : 08 Mar 2016
 * 
 * Change History:
 * Date       	Modified By		Description
 * ---------- ----------------	-------------------------------
 * 08 Mar 2016 	Henry Law 		-First draft
 *************************************************************************/

public class ForgetPasswordActivity extends BaseActivity implements OnLRButtonClickListener, OnAPIsListener {
	// Common Components
	private boolean debug = false;
	private String 					TAG = this.getClass().getName();
	private AAQuery 				aq;
	private final int 				colMaxNum = 3;
	private int 					deviceWidth = 0;
	private int						colWidth = 0;
	private int 					buttonPadding = 0;
	private int 					extralinespace = 0;
	private int 					padding_twocol = 0;
	private int						txtColor;
	private int 					regInputHeight;
	private Boolean 				isFirstTime = true;
	private int 					btnWidth = 0;
	private Bundle					tempBundle;
	private Boolean 				isZh;

	private LRButton 				lrButton; 
	private RegInputItem 			regInputHKID;
	
	//Save data
	private int 					regServicePos = -1;
	private String 					regHkid ="";

	private SubnRec					subnRec = new SubnRec();
	private CustRec 				custRec = new CustRec();
	private SveeRec 				sveeRec = new SveeRec();

	public final static String BUNDLE_CUSTREC = "BUNDLE_CUSTREC";
	public final static String BUNDLE_SVEEREC = "BUNDLE_SVEEREC";
	public final static String BUNDLE_SUBNREC = "BUNDLE_SUBNREC";
	public final static String BUNDLE_SERVICERES = "BUNDLE_SERVICERES";

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		debug = getResources().getBoolean(R.bool.DEBUG);	

		if (debug) Log.i(TAG, "onCreate");


		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		isZh = !ClnEnv.getAppLocale(this).equals(Utils.getString(this, R.string.CONST_LOCALE_EN));
		setContentView(R.layout.activity_forgetpassword);
		initData();	
		if(	savedInstanceState !=null) {
			regHkid = savedInstanceState.getString("REGHKID");
		}		
	}

	private void initData(){
		me = this;
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);		
		padding_twocol = (int) getResources().getDimension(R.dimen.padding_twocol);
		buttonPadding =  (int) getResources().getDimension(R.dimen.reg_logo_padding);
		regInputHeight = (int) getResources().getDimension(R.dimen.reg_input_height);		
		btnWidth = 	(deviceWidth - buttonPadding * 4)/2;
		txtColor = getResources().getColor(R.color.hkt_txtcolor_grey);

		//For validation
		String mobPfx = ClnEnv.getPref(this, "mobPfx", "51,52,53,54,55,56,57,59,6,9,84,85,86,87,89");
		String ltsPfx = ClnEnv.getPref(this, "ltsPfx", "2,3,81,82,83");

		DirNum.getInstance(mobPfx, ltsPfx);
		subnRec = new SubnRec();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
	}

	@Override
	protected final void onStart() {
		super.onStart();
	}

	@Override
	protected final void onResume() {
		super.onResume();
		moduleId = getResources().getString(R.string.MODULE_FORGET_PWD);	
		// Update Locale
		ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
		
		//Screen Tracker
		FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_FORGOTPASS, false);
	}	

	@Override
	protected final void initUI2() {
		aq = new AAQuery(this);		

		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_title_forgetpassword));

		aq.id(R.id.navbar_button_left).clicked(this, "onClick");

		aq.id(R.id.forgetpassword_textview).width(LayoutParams.MATCH_PARENT).textSize(16).textColor(txtColor).text(getResString(R.string.FGCF_RECALL));
		aq.id(R.id.forgetpassword_textview).getView().setPadding(buttonPadding, extralinespace, buttonPadding, 0);		

		aq.line(R.id.forgetpassword_line);

		TextView textView = aq.id(R.id.forgetpassword_txt).getTextView();
		aq.id(R.id.forgetpassword_txt).text(getResString(R.string.myhkt_forgetpassword_ITO)).textColor(txtColor);
		textView.setPadding(buttonPadding, extralinespace, buttonPadding, extralinespace);

		lrButton = (LRButton) aq.id(R.id.forgetpassword_lrBtn).getView();
		lrButton.initViews(this,getResString(R.string.REGF_HKID_NO), getResString(R.string.REGF_PASSPORT_NO));
		lrButton.setPadding(buttonPadding, 0, buttonPadding, extralinespace);

		regInputHKID = aq.regInputItem(R.id.forgetpassword_input_hkid, "", getResString(R.string.REGF_FB_DOC_NUM), regHkid, 
				this.getResources().getInteger(R.integer.CONST_MAX_IDDOC));
		regInputHKID.setPadding(basePadding, 0, basePadding, extralinespace);
		aq.marginpx(R.id.regconfirm_relative_layout, 0, extralinespace, 0, extralinespace);
		aq.marginpx(R.id.regconfirm_relative_layout2, 0, extralinespace, 0, extralinespace);
		aq.id(R.id.regconfirm_relative_layout).getView().setPadding(buttonPadding, 0, 0, 0);
		aq.id(R.id.regconfirm_relative_layout2).getView().setPadding(buttonPadding, 0, 0, 0);
		aq.id(R.id.regconfirm_txt_checkbox_loginid).text(getResString(R.string.myhkt_forgetpassword_loginid)).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.regconfirm_txt_checkbox_password).text(getResString(R.string.myhkt_forgetpassword_password)).textColorId(R.color.hkt_textcolor);
		
		aq.id(R.id.forgetpassword_loginid_checkbox).clicked(this, "onClick");
		aq.id(R.id.forgetpassword_password_checkbox).clicked(this, "onClick");
		aq.id(R.id.forgetpassword_loginid_checkbox).checked(true);
		aq.id(R.id.forgetpassword_password_checkbox).checked(false);
		aq.id(R.id.forgetpassword_loginid_checkbox).getCheckBox().setButtonDrawable(R.drawable.checkbox);
		aq.id(R.id.forgetpassword_password_checkbox).getCheckBox().setButtonDrawable(R.drawable.checkbox);
		
		//button layout
		aq.id(R.id.forgetpassword_btn_layout).getView().setPadding(buttonPadding, extralinespace, buttonPadding, extralinespace);
		aq.norm2TxtBtns(R.id.forgetpassword_btn_cancel, R.id.forgetpassword_btn_confirm, getResString(R.string.MYHKT_BTN_CANCEL) ,getResString(R.string.MYHKT_BTN_CONFIRM));
		aq.id(R.id.forgetpassword_btn_confirm).clicked(this, "onClick");
		aq.id(R.id.forgetpassword_btn_cancel).clicked(this, "onClick");
		
		//Enquiry View Layout
		aq.id(R.id.forgetpassword_enquiry_layout).getView().setPadding(buttonPadding, 0, buttonPadding, extralinespace);
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) aq.id(R.id.forgetpassword_enquiry_layout).getView().getLayoutParams();
		params.setMargins(buttonPadding, 0, buttonPadding, 0);
		aq.id(R.id.forgetpassword_enquiry_layout).getView().setLayoutParams(params);
		aq.id(R.id.forgetpassword_enquiry_layout).getView().setFocusable(false);
		aq.id(R.id.forgetpassword_enquiry).getView().setFocusable(false);
		aq.id(R.id.forgetpassword_enquiry).getWebView().getSettings().setDefaultTextEncodingName("utf-8");
		aq.id(R.id.forgetpassword_enquiry).getWebView().loadUrl(isZh ? "file:///android_asset/forgetpassword_enquiry_zh.html" : "file:///android_asset/forgetpassword_enquiry_en.html");	
		aq.id(R.id.forgetpassword_enquiry).visibility(View.VISIBLE);
		
		HKTButton livechatButton = (HKTButton)aq.id(R.id.forgetpassword_btn_livechat).getView();
		aq.normTxtBtn(R.id.forgetpassword_btn_livechat, getResString(R.string.MYHKT_BTN_LIVECHAT), btnWidth);
		aq.id(R.id.forgetpassword_btn_livechat).clicked(this, "onClick");
		int bh = (int) getResources().getDimension(R.dimen.buttonblue_height);
		Drawable drawble = getResources().getDrawable(R.drawable.livechat_small);
		int h = drawble.getIntrinsicHeight(); 
		int w = drawble.getIntrinsicWidth();
		int imageH = bh - padding_twocol *2;
		int imageW = w * (imageH) / h;						
		drawble.setBounds( 0, 0, imageW, imageH);
		livechatButton.setDrawable(drawble);	
	}
	private QuickAction mQuickAction;
	

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
		case R.id.forgetpassword_btn_cancel:
			Utils.closeSoftKeyboard(me, v);
			onBackPressed();
			break;
		case R.id.forgetpassword_loginid_checkbox:
			aq.id(R.id.forgetpassword_loginid_checkbox).checked(true);
			aq.id(R.id.forgetpassword_password_checkbox).checked(false);
			break;
		case R.id.forgetpassword_password_checkbox:
			aq.id(R.id.forgetpassword_loginid_checkbox).checked(false);
			aq.id(R.id.forgetpassword_password_checkbox).checked(true);
			break;
		case R.id.forgetpassword_btn_confirm:
			Utils.closeSoftKeyboard(me, v);
			checkInput();
			break;
		case R.id.forgetpassword_btn_livechat:
			Utils.closeSoftKeyboard(me, v);
			openLiveChat();	
			break;
		}
	}


	private void checkInput(){
		//TODO
//		Utils.closdSoftKeyboard(this, (View)serviceContainer.getParent());	
		
		regHkid = regInputHKID.getInput();
		regHkid = regHkid.trim().toUpperCase(Locale.US);
		regInputHKID.setText(regHkid);	
		
		custRec = new CustRec();
		custRec.docTy = lrButton.isLeftClick() ? CustRec.TY_HKID : CustRec.TY_PASSPORT;
		custRec.docNum = regHkid;
		
//		SveeRec sveeRec = new SveeRec();
//		sveeRec.mobAlrt = "Y";
//		sveeRec.lang = Utils.getString(me, R.string.myhkt_lang).equalsIgnoreCase("en") ? sveeRec.LG_EN : sveeRec.LG_ZH;		

		//Start verify
		Reply reply = new Reply();
		reply = custRec.verifyBaseInput();
		if (!reply.isSucc()) {
			DialogHelper.createSimpleDialog(this, InterpretRCManager.interpretRC_ForgetPassword(me, reply));
			((ScrollView)aq.id(R.id.forgetpassword_scrollView).getView()).scrollTo(0, (int) lrButton.getY() + ((View)lrButton.getParent()).getTop());
			return;
		}	

		LgiCra lgiCra = new LgiCra();
		lgiCra.setICustRec(custRec.copyMe());
		lgiCra.setIRecallLgiId(aq.id(R.id.forgetpassword_loginid_checkbox).isChecked());
	    lgiCra.setIRecallPwd(aq.id(R.id.forgetpassword_password_checkbox).isChecked());
		
		APIsManager.doPrepare4Recall(this, lgiCra);
	}


	// Android Device Back Button Handling
	public final void onBackPressed() {
		//			clearNotificationData();
		//			
		Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
		overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		finish();
	}

	@Override
	protected final void onPause() {
		super.onPause();		
	}

	@Override
	protected final void onStop() {
		super.onStop();
	}

	protected void cleanupUI() {
		lrButton= null ; 
		regInputHKID= null ;
	}

	// Restore the Bundle data when re-built Activity
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		regHkid = savedInstanceState.getString("REGHKID");
	}

	// Saved data before Destroy Activity
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		//TODO may need to remake as they should be in 
		if (regInputHKID != null && regInputHKID.getInput() != null) {
			regHkid 	= regInputHKID.getInput();
		}
		outState.putString("REGHKID", regHkid);
	}

	@Override
	public void onSuccess(APIsResponse response) {
		super.onSuccess(response);
		if (response != null) {
			if (debug) Log.d("Henry", "I am P4 (Not yet)" +  response.getActionTy());
			if (APIsManager.HELO.equals(response.getActionTy())) {
//				Toast.makeText(me, "helo at forget password return", Toast.LENGTH_LONG).show();
				debugLog(TAG, "doHelo complete!!");
			} else if (APIsManager.P4_RCALL.equals(response.getActionTy())) {
				Log.d("Henry", "I am P4");
				LgiCra lgiCra = (LgiCra) response.getCra();
				Gson gson = new Gson();
				String result = gson.toJson(lgiCra);
//				Log.d(TAG, result.substring(0, 3000));
//				Log.d(TAG, result.substring(3001, 6000));
//				Log.d(TAG, result.substring(6001, result.length()));

//				ClnEnv.setLgiCra(lgiCra);
				// Keeping the username and password in memory
//				ClnEnv.setSessionLoginID(regInputUserID.getInput());
//				ClnEnv.setSessionPassword(regInputPw.getInput());
//				ClnEnv.setSessionSavePw(isRememberPw);
				// Save Account Type

				sveeRec = lgiCra.getOQualSvee().getSveeRec().copyMe();

				String message = "";
				if (aq.id(R.id.forgetpassword_loginid_checkbox).isChecked()) { //forgot ID
					message = String.format(getResString(R.string.myhkt_forgetid_message), sveeRec.ctMail, sveeRec.ctMob);
				} else { //forgot password
					message = String.format(getResString(R.string.myhkt_forgetpw_message), sveeRec.ctMail, sveeRec.ctMob);
				}
				
				DialogHelper.createSimpleDialog(this, message,
						getString(R.string.btn_confirm), 
						new DialogInterface.OnClickListener(){
		
							@Override
							public void onClick(DialogInterface dialog, int which) {
								LgiCra lgiCra = new LgiCra();
								lgiCra.setICustRec(custRec.copyMe());
								lgiCra.setISveeRec(sveeRec.copyMe());
								
								lgiCra.setIRecallLgiId(aq.id(R.id.forgetpassword_loginid_checkbox).isChecked());
							    lgiCra.setIRecallPwd(aq.id(R.id.forgetpassword_password_checkbox).isChecked());
							    
								APIsManager.doRecallIDOrPwd(me, lgiCra);
								
//								onBackPressed();
							}			
						}, 
						getString(R.string.btn_cancel));

			} else if (APIsManager.RC_CRED.equals(response.getActionTy())){
				String message = "";
				if (aq.id(R.id.forgetpassword_loginid_checkbox).isChecked()) { //forgot ID
					message = getString(R.string.FGCM_DONE_LOGINID);
				} else { //forgot password
					message = getString(R.string.FGCM_DONE_PWD);
				}
				
				DialogHelper.createSimpleDialog(me, message, getString(R.string.btn_ok), new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog, int which) {
						onBackPressed();
					}			
				});
			}
		}
	}

	public void onFail(APIsResponse response) {
		super.onFail(response);
		if (response != null) {
			if (APIsManager.LGI.equals(response.getActionTy())) {
				LgiCra lgiCra = (LgiCra) response.getCra();
//			if (lgiCra.getReply().getCode().equals(Reply.RC_NO_CTMAIL)) {
				// RC_NO_CTMAIL
//				displayDialog(this, Utils.getString(me, R.string.FGCM_NO_CTMAIL));
//			} else 
				if (lgiCra.getReply().getCode().equals(RC.FILL_SVEE)) {
				// RC_FILL_RCUS
				displayDialog(this, Utils.getString(me, R.string.FGCM_FILL_RCUS));
				} else if (lgiCra.getReply().getCode().equals(RC.NO_LOGIN_ID)) {
					// RC_NO_RELV_RCUS
					displayDialog(this, Utils.getString(me, R.string.FGCM_NO_RELV_RCUS));
				} else if (lgiCra.getReply().getCode().equals(RC.ACTV_SVEE) || 
						lgiCra.getReply().getCode().equals(RC.RESET_SVEE) || 
						lgiCra.getReply().getCode().equals(RC.INITPWD_SVEE) ) {
					// RC_ACTV_RCUS
					// RC_RESET_RCUS
					// RC_INITPWD_RCUS
					displayDialog(this, Utils.getString(me, R.string.FGCM_IVSTATE));
				} else 
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					displayDialog(this, response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					ivSessDialog();
				} else {
					displayDialog(this, InterpretRCManager.interpretRC_ForgetPassword(this, response.getReply()));
				}
			} else {
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					displayDialog(this, response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					ivSessDialog();
				} else {
					displayDialog(this, InterpretRCManager.interpretRC_ForgetPassword(this, response.getReply()));
				}
			}
		}
	}

	@Override
	public void onLRBtnLeftClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLRBtnRightClick() {
		// TODO Auto-generated method stub

	}	
}