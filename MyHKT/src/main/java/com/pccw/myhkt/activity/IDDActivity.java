package com.pccw.myhkt.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.IDDNewsFragment.OnIDDNewsListener;
import com.pccw.myhkt.fragment.IDDNewsV2Fragment;
import com.pccw.myhkt.fragment.IDDRateServiceFragment.OnIDDRateServiceListener;
import com.pccw.myhkt.fragment.IDDRatesFragment;
import com.pccw.myhkt.fragment.IDDRatesFragment.OnIDDRatesListener;
import com.pccw.myhkt.fragment.ServListFragment;
import com.pccw.myhkt.fragment.ServListFragment.OnServListListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRTabButton;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.HomeButtonItem;

/************************************************************************
 * File : IDDActivity.java
 * Desc : IDD Page
 * Name : IDDActivity
 * by 	: Andy Wong
 * Date : 11/01/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 11/01/2016 Andy Wong 		-First draft
 *************************************************************************/

public class IDDActivity extends BaseActivity implements OnIDDRateServiceListener, OnIDDRatesListener, OnServListListener, OnIDDNewsListener {
	private AAQuery aq;
	private Boolean is0060View = false; //STATUS: True--left False--right
	private int extralinespace = 0;
	private AcctAgent acctAgent;

	private IDDRatesFragment iDDRatesFragment;
	private ServListFragment servListFragment;
	private IDDNewsV2Fragment iddNewsFragment;
	private FragmentManager fragmentManager ;
	private LRTabButton btn;
	private Activity me;

	private boolean isLoaded = false;

	//Bundle
	public static String BUNDLEIS0060 = "BUNDLEIS0060";

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		// reload Data
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		setContentView(R.layout.activity_idd);
		me = this;
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		is0060View = getIntent().getBooleanExtra(BUNDLEIS0060, false);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
	}

	@Override
	protected final void onStart() {
		super.onStart();
	}

	protected void initUI2() {
		aq = new AAQuery(this);
		fragmentManager  =  getSupportFragmentManager();
		//navbar
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.MYHKT_IDD_TITLE));

		aq.id(R.id.idd_frame1).backgroundColorId(R.color.white);
		aq.marginpx(R.id.idd_frame1, basePadding, 0, basePadding, 0);
		aq.id(R.id.idd_frame1).getView().setPadding(0, extralinespace, 0, 0);

		btn = (LRTabButton) aq.id(R.id.idd_lrtabbtn).getView();
		btn.initViews(this, getResString(R.string.MYHKT_IDD_BTN_CALL_RATE),
				getString(R.string.MYHKT_IDD_BTN_NEWS),
				-1 ,-1, (int)getResources().getDimension(R.dimen.iddlrtabtextsize), (int) getResources().getDimension(R.dimen.lr_button_height), LayoutParams.MATCH_PARENT);
		btn.setMenuTabRightVisibility(true);

		aq.id(R.id.idd_lrtabbtn).height((int)getResources().getDimension(R.dimen.lr_button_height) , false);
		btn.clicked(this, "onClick");
		btn.setBtns(is0060View);
		changeFragment();
   	}

	public void changeFragment() {
		fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		//Screen Tracker
		if (is0060View) {
			servListFragment = new ServListFragment();
			fragmentTransaction.replace(R.id.idd_frame2, servListFragment);

			FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_IDDRATES, false);
		} else {
			iddNewsFragment = new IDDNewsV2Fragment();
			fragmentTransaction.replace(R.id.idd_frame2, iddNewsFragment);

			FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_IDD_NEWS, false);
		}
		Utils.closeSoftKeyboard(this);
		fragmentTransaction.commit();
	}

	@Override
	public void goToIddRateFragment() {
		fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		iDDRatesFragment = new IDDRatesFragment();
		fragmentTransaction.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
		if (is0060View) {
			fragmentTransaction.replace(R.id.idd_frame2, iDDRatesFragment);
		}
		Utils.closeSoftKeyboard(this);
		fragmentTransaction.commit();
	}

	@Override
	public void goToIDDNewsFragment() {
		fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		iddNewsFragment = new IDDNewsV2Fragment();
		fragmentTransaction.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
		if (is0060View) {
			fragmentTransaction.replace(R.id.idd_frame2, iddNewsFragment);
		}
		Utils.closeSoftKeyboard(this);
		fragmentTransaction.commit();
	}

	// Android Device Back Button Handling
	public final void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			break;
		case LRTabButton.leftBTnid:
			is0060View = true;
			//check the login status
			if (!ClnEnv.isLoggedIn()) {
				loginFirstDialog();
			} else {
				changeFragment();
			}
			btn.setBtns(is0060View);
			moduleId = is0060View ? getResources().getString(R.string.MODULE_LTS_IDD) : getResources().getString(R.string.MODULE_LTS_IDD);
			break;
		case LRTabButton.rightBTnid:
			is0060View = false;
			changeFragment();
			btn.setBtns(is0060View);
			moduleId = is0060View ? getResources().getString(R.string.MODULE_LTS_IDD) : getResources().getString(R.string.MODULE_LTS_IDD);
			break;			
		}
	}

	public void loginFirstDialog() {
		Intent intent = new Intent(me, LoginActivity.class);
		Bundle rbundle = new Bundle();

		rbundle.putSerializable("CLICKBUTTON", HomeButtonItem.MAINMENU.IDD);
		intent.putExtras(rbundle);
		startActivity(intent);
		overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
		finish();
	}
	
	@Override
	protected final void onResume() {
		super.onResume();
		// Update Locale		
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		moduleId = is0060View ? getResources().getString(R.string.MODULE_LTS_IDD) : getResources().getString(R.string.MODULE_LTS_IDD);
		//Screen Tracker
		FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_IDD, false);
		if(isLoaded) {
				if (is0060View) {
					FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_IDDRATES, false);
				} else {
					FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_IDD_NEWS, false);
				}
		}
		else {
			isLoaded = true;
		}
	}

	@Override
	protected final void onPause() {
		super.onPause();
	}

	@Override
	protected final void onStop() {
		super.onStop();
	}

	public void onSuccess(APIsResponse response) {
		super.onSuccess(response);
	}

	public void onFail(APIsResponse response) {
		super.onFail(response);
		if (response != null) {
			// General Error Message
			if (!"".equals(response.getMessage()) && response.getMessage() != null) {
				displayDialog(this, response.getMessage());
			} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
				ivSessDialog();
			} else {
				displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
			}
		}
	}

	@Override
	public String getSrvNum() {
		if (acctAgent != null && acctAgent.getSrvNum() != null) {
			return acctAgent.getSrvNum();
		}
		return "";
	}

	@Override
	public String getCustNum() {
		if (acctAgent != null && acctAgent.getCusNum() != null) {
			return acctAgent.getCusNum();
		}
		return "";
	}

	@Override
	public int getType() {
		return ServListFragment.IDDRate;
	}

	@Override
	public void setAcctAgent(AcctAgent mAcctAgent) {
		acctAgent = mAcctAgent;
	}

	@Override
	public AcctAgent getAcctAgent() {
		return acctAgent;
	}
}