package com.pccw.myhkt.service;

import com.pccw.myhkt.model.AppConfig;
import com.pccw.myhkt.model.Destinations;
import com.pccw.myhkt.model.SRPlan;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface GetDataService {

    @GET("destinations")
    Call<Destinations> getDestinations();

    @GET("customer/{customer}/srps")
    Call<SRPlan> getSrpRate(@Path("customer") String customer, @Query("servicenum") String servicenum,
                            @Query("srvtype") String srvtype, @Query("dest") String dest,
                            @Query("destext") String destext);

    // option 1: a resource relative to your base URL
    @GET
    Call<ResponseBody> downloadFileWithFixedUrl(@Url String url);

    @GET
    Call<AppConfig> getAppConfigJSON(@Url String host);

    @GET("mba/images/{fileName}")
    Call<ResponseBody> getImage(@Path("fileName") String fileName);
}
