package com.pccw.myhkt.service;

/************************************************************************
 File       : LineTestIntentService.java
 Desc       : Background Service For LineTest
 Name       : LineTestIntentService
 Created by : Vincent Fung
 Date       : 12/02/2014

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 12/02/2014  Vincent Fung		- First draft
 13/02/2014  Vincent Fung		1.) Created IntentService for background lineTest
 2.) Allowed to Normal LineTest and Reset Modem
 3.) Added Notification to foreground or push Notification to mobile device
 24/02/2014 Vincent Fung			- Code cleanup
 *************************************************************************/

import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.R;
import com.pccw.myhkt.model.VersionCheck;
import com.pccw.myhkt.util.Constant;

import java.util.Objects;

public class VersionCheckService extends IntentService implements OnAPIsListener {
    private boolean debug = false;
    private static VersionCheckService me;
    private final String TAG = "VersionCheckService";
    private String clientVersion = "";
    private boolean requiredUpdate = false;
    private boolean forcedUpdate = false;

    public VersionCheckService() {
        super("VersionCheckService");
    }

    @Override
    public void onCreate() {
        Log.d("lwg", "VersionCheck onCreate");
        // The service is being created
        debug = getResources().getBoolean(R.bool.DEBUG);
        IntentFilter filter = new IntentFilter(Constant.ACTION_APP_VERSION_IS_UPDATED);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(mVersionCheckBReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
        } else {
            registerReceiver(mVersionCheckBReceiver, filter);
        }
        me = this;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("lwg", "VersionCheck onHandleIntent");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("lwg", "VersionCheck onStartCommand");
        try {
            clientVersion = me.getApplicationContext().getPackageManager().getPackageInfo(me.getApplicationContext().getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        APIsManager.doCheckUpdate(this);
        return Service.START_REDELIVER_INTENT;
    }

    @Override
    public void onSuccess(APIsResponse response) {
        Log.d("lwg", "VersionCheck onSuccess APIsResponse response");
        VersionCheck versionCheck;
        if (response != null) {
            versionCheck = (VersionCheck) response.getCra();
            // Checks blockedVersion
            String[] blockedVersion = versionCheck.getBlockedVersion();
            for (String version : blockedVersion) {
                if (version.equalsIgnoreCase(clientVersion)) {
                    ClnEnv.setPref(this, Constant.BLOCK_VERSION, version);
                    requiredUpdate = true;
                    forcedUpdate = true;
                    returnResults(requiredUpdate, forcedUpdate);
                    return;
                } else {
                    ClnEnv.setPref(this, Constant.BLOCK_VERSION, "");
                }
            }

            // forced check?
            if (!versionCheck.getForcedCheck()) {
                requiredUpdate = false;
                forcedUpdate = false;
                returnResults(requiredUpdate, forcedUpdate);
            } else {
                // Simple string comparison
                String headVersion = versionCheck.getHeadVersion();

                if (headVersion.equalsIgnoreCase(clientVersion)) {
                    // current version equals head version, no update required
                    // this is the most common case
                    requiredUpdate = false;
                    forcedUpdate = false;
                    returnResults(requiredUpdate, forcedUpdate);
                } else {
                    // current version either more or less update than the head version
                    // (usually less update)
                    try {
                        if (debug) Log.i(TAG, headVersion + "/" + clientVersion);
                        String[] strHeadVersion = headVersion.split("\\.");
                        String[] strClientVersion = clientVersion.split("\\.");
                        int normalizedHeadVersion = 0;
                        int normalizedClientVersion = 0;

                        normalizedHeadVersion = Integer.parseInt(strHeadVersion[0]) * 1000000 + Integer.parseInt(strHeadVersion[1]) * 1000 + Integer.parseInt(strHeadVersion[2]);
                        normalizedClientVersion = Integer.parseInt(strClientVersion[0]) * 1000000 + Integer.parseInt(strClientVersion[1]) * 1000 + Integer.parseInt(strClientVersion[2]);

                        if (normalizedHeadVersion > normalizedClientVersion) {
                            // Head version somehow greater than client version
                            requiredUpdate = true;
                            forcedUpdate = false;
                            returnResults(requiredUpdate, forcedUpdate);
                        } else {
                            // Client is more recent. no update required
                            requiredUpdate = false;
                            forcedUpdate = false;
                            returnResults(requiredUpdate, forcedUpdate);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        // Unexpected error - skip version check
                        requiredUpdate = false;
                        forcedUpdate = false;
                        returnResults(requiredUpdate, forcedUpdate);
                    }
                }
            }
        }
    }

    @Override
    public void onFail(APIsResponse response) {
        ClnEnv.isVCFinish = true;
        ClnEnv.setPref(this, Constant.BLOCK_VERSION, "");

        //broadcast to Splash screen to show next screen
        Intent intent = new Intent();
        intent.setAction(Constant.ACTION_APP_VERSION_IS_UPDATED);
        intent.setPackage(getPackageName());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        }
        sendBroadcast(intent);
    }

    @Override
    public void onLowMemory() {
    }

    @Override
    public void onDestroy() {
        Log.d("lwg", "VersionCheck onDestroy");
        // clear all Timer schedule to avoid restart line test
        try {
            if (mVersionCheckBReceiver != null) {
                this.unregisterReceiver(mVersionCheckBReceiver);
            }
        } catch (Exception e) {
            // lineTestBroadcastReceiver is already unregistered
            mVersionCheckBReceiver = null;
        }
    }

    private void returnResults(boolean requiredUpdate, boolean forcedUpdate) {
        if (debug) Log.i(TAG, requiredUpdate + "/" + forcedUpdate);
        if (forcedUpdate) {
            // The current version is blocked. Customer can only upgrade or exit
            displayForcedUpdateDialog();
        } else {
            if (!requiredUpdate) {
                // No update is required, continue the original logic
                ClnEnv.isVCFinish = true;

                //broadcast to Splash screen to show next screen
                ClnEnv.setPref(this, Constant.BLOCK_VERSION, "");
                Intent intent = new Intent();
                intent.setAction(Constant.ACTION_APP_VERSION_IS_UPDATED);
                intent.setPackage(getPackageName());
                sendBroadcast(intent);
            } else {
                // Prompt upgrade, or continue the original logic
                displayOptionalUpdateDialog();
            }
        }
    }

    // Dialog: upgrade or exit
    private void displayForcedUpdateDialog() {
        me.stopSelf();
        Intent dialogIntent = new Intent(me.getApplicationContext(), GlobalDialog.class);
        dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_VC_FORCE_UPDATE);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        me.getApplicationContext().startActivity(dialogIntent);
    }

    // Dialog: continue or exit
    private void displayOptionalUpdateDialog() {
        me.stopSelf();
        Intent dialogIntent = new Intent(me.getApplicationContext(), GlobalDialog.class);
        dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_VC_OPTION_UPDATE);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        me.getApplicationContext().startActivity(dialogIntent);
    }

    BroadcastReceiver mVersionCheckBReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("lwg", "VersionCheck BroadcastReceiver onReceive");
            if (Objects.requireNonNull(intent.getAction()).equals(Constant.ACTION_CHECK_APP_VERSION)) {
                try {
                    clientVersion = me.getApplicationContext().getPackageManager().getPackageInfo(me.getApplicationContext().getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    throw new RuntimeException(e);
                }
                // APIsManager.doCheckUpdate(VersionCheckService.this);
            }
        }
    };
}