package com.pccw.myhkt.service;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

public class MyWorker extends Worker {

    private static final String TAG = "lwg";
    private String PATH = null;  //app config storage path
    private String HOST = null; // CSP HOST path

    public MyWorker(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
        super(appContext, workerParams);
        PATH = appContext.getResources().getString(R.string.PATH);
        HOST = ClnEnv.getPref(appContext, appContext.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP); //image download path
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.d(TAG, "Performing long running task in scheduled job");
        String endpoint = "/mba/data/app_config.json";

        String status = DownloadFromUrl(PATH, HOST + endpoint, "app_config.json");
        Log.d(TAG, "status: " + status);

        if (status.equalsIgnoreCase("Ok")) {
           return Result.success();
       } else {
           return Result.failure();
       }
    }

    public String DownloadFromUrl(String storagePath, String fileURL, String fileName) {  //this is the downloader method
        String status = "OK";
        try {
            URL url = new URL(fileURL);
            File file = new File(storagePath + fileName);
            boolean doDownload = false;
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            long date = httpCon.getLastModified();

            Date serverFileLastModified = new Date(date);
            if (date == 0) {
                System.out.println("No last-modified information.");
                status = "FAILED";
            } else {
                System.out.println("Last-Modified: " + serverFileLastModified);
                if (file.exists()) {
                    Date lastModified = new Date(file.lastModified());
                    if (lastModified.compareTo(serverFileLastModified) != 0) {
                        Log.e("lwg", "serverFileLastModified :" + serverFileLastModified);
                        Log.e("lwg", "lastModified :" + lastModified);
                        doDownload = true;
                    } else {
                        Log.e("lwg", "File :" + fileName + " Same");
                    }
                } else {
                    doDownload = true;
                }
            }

            if (doDownload) {
                Log.d("lwg", "download url:" + url);
                URLConnection urlConnection = url.openConnection();
                InputStream is = urlConnection.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayBuffer baf = new ByteArrayBuffer(50);
                int current = 0;
                while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
                }
                FileOutputStream fos = new FileOutputStream(file, false);
                fos.write(baf.toByteArray());
                fos.close();

                file.setReadable(true, false);
                file.setWritable(true, false);
                file.setLastModified(date);
            }

            return status;
        } catch (IOException e) {
            e.printStackTrace();
            status = "FAILED";
            return status;
        }
    }
}
