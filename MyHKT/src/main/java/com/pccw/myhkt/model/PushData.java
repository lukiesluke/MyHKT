package com.pccw.myhkt.model;

import java.io.Serializable;


public class PushData implements Serializable{
	
	private static final long	serialVersionUID	= -3287654226998309480L;
	
	private String message;
	private String loginId;
	private String acctNum;
	private String type;
	private boolean isAppActive;

	
	public PushData() {
		message = new String();
		loginId = new String();
		acctNum = new String();
		type = new String();
		isAppActive = new Boolean(false);
	}
	
//	public void initAndClear() {
//		init();
//		clear();
//	}
	
	public PushData copyTo(PushData rDes) {
		rDes.copyFrom(this);
		return (rDes);
	}

	public PushData copyMe() {
		PushData rDes;

		rDes = new PushData();
		rDes.copyFrom(this);
		return (rDes);
	}
	
	public PushData copyFrom(PushData rSrc) {
		setMessage(rSrc.getMessage());
		setLoginId(rSrc.getLoginId());
		setAcctNum(rSrc.getAcctNum());
		setType(rSrc.getType());
		setType(rSrc.getType());
		setAppActive(rSrc.isAppActive());
		return (this);
	}
	
	protected void init() {
	}
	
	public void clear() {
		clearMessage();
		clearAcctNum();
		clearType();
		clearLoginId();
	}
	
	public void clearMessage() {
		setMessage("");
	}
	
	public void clearAcctNum() {
		setAcctNum("");
	}
	
	public void clearType() {
		setType("");
	}
	
	public void clearLoginId() {
		setLoginId("");
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getAcctNum() {
		return acctNum;
	}

	public void setAcctNum(String acctNum) {
		this.acctNum = acctNum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAppActive() {
		return isAppActive;
	}

	public void setAppActive(boolean appActive) {
		isAppActive = appActive;
	}
	
}