package com.pccw.myhkt.model;

public class HomeButtonItem {
    public int image;
    public String text;
    public int textSize;
    public int bgColor;
    public int txtColor;
    public MAINMENU type;

    public HomeButtonItem(int image, String text, int textSize, int bgColor, int txtColor, MAINMENU type) {
        this.image = image;
        this.text = text;
        this.textSize = textSize;
        this.bgColor = bgColor;
        this.txtColor = txtColor;
        this.type = type;
    }

    public void setImageResource(int resId) {
        this.image = resId;
    }

    public enum MAINMENU {
        MYACCT,
        MYPROFILE,
        MYWALLET,
        MYAPPT,
        MYMOB,
        THECLUB,
        TAPNGO,
        MOX,
        DIRECTINQ,
        IDD,
        SHOPS,
        HKT_SHOP,
        CONTACTUS,
        NULL,
        SETTING,
        SOLUTION,
        NEWS,
        AR_LENS,
        TRAVEL_GUIDE,
        TRAVEL_ZONE,
        MYLINETEST,
        DRGO
    }
}