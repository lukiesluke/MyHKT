package com.pccw.myhkt.model;

import java.io.Serializable;

public class IDDCity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4906484124255173388L;
	
	private String ename;
	private String cname;
	private String val;

	public IDDCity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}
}