package com.pccw.myhkt.model;

import com.google.gson.annotations.SerializedName;

public class AppConfigImage {

    private String key;

    @SerializedName("en_img")
    private String enImg;

    @SerializedName("zh_img")
    private String zhImg;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEnImg() {
        return enImg;
    }

    public void setEnImg(String enImg) {
        this.enImg = enImg;
    }

    public String getZhImg() {
        return zhImg;
    }

    public void setZhImg(String zhImg) {
        this.zhImg = zhImg;
    }
}
