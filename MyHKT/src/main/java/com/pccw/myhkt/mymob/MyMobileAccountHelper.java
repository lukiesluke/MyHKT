package com.pccw.myhkt.mymob;

/************************************************************************
 File       : MyMobileAccountHelper.java
 Desc       : Database for mobile number account list in MyHKT in MyMobile 
 Name       : myMobileAccountHelper
 Created by : Rex Liu
 Date       : 15/07/2013

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 09/01/2014 Rex Liu        		- First draft
 12/30/2014 Rex Liu          	  1. Changed database column "display_id" to be "alias"
								  2. Changed database column "mobnum" to be "mob_num"
 06/03/2015 Derek Tsui			- Added SMS flag and related functions
 *************************************************************************/

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

public class MyMobileAccountHelper {
    private static boolean debug = false;            // toggle debug logs
    private static final String DATABASE_NAME = "myhkt.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "myhkt_mymob";
    private static MyMobileAccountHelper myMobileAccountHelper = null;
    private OpenHelper openHelper;
    private Context context;
    private SQLiteDatabase db;
    private static final String INSERT = "insert into " + TABLE_NAME + " (alias, mob_num, password,card_type, lob,latest_bill) values (?, ?, ?, ?, ?,?)";
    private static final String UPDATE_INFO = "update " + TABLE_NAME + " set alias = ?,password =?,latest_bill=? where mob_num = ? ";
    private static final String UPDATE_SMS = "update " + TABLE_NAME + " set sms_flag = ? where mob_num = ? ";
    private static final String UPDATE_LASTBILLDT = "update " + TABLE_NAME + " set latest_bill = ? where mob_num = ? ";
    private static final String DELETE = "delete from " + TABLE_NAME + " where mob_num = ?";
    private SQLiteStatement insertStmt;
    //private SQLiteStatement updateFlagStmt;
    private SQLiteStatement updateAccountInfo;
    private SQLiteStatement updateSms;
    private SQLiteStatement updateLastBillDt;
    private SQLiteStatement deleteDateStmt;

    private MyMobileAccountHelper(Context context) {
        this.context = context;
        openHelper = new OpenHelper(this.context);
        db = openHelper.getWritableDatabase();
        openHelper.onCreate(db);
        insertStmt = db.compileStatement(INSERT);
        updateAccountInfo = db.compileStatement(UPDATE_INFO);
        updateSms = db.compileStatement(UPDATE_SMS);
        updateLastBillDt = db.compileStatement(UPDATE_LASTBILLDT);
        deleteDateStmt = db.compileStatement(DELETE);
    }

    public static MyMobileAccountHelper getInstance(Context context) {
        if (myMobileAccountHelper == null) {
            myMobileAccountHelper = new MyMobileAccountHelper(context);
        }
        return myMobileAccountHelper;
    }

    public long insert(String alias, String mob_num, String password, String cardType, String lob, String latest_bill) {
        long result = -1;
        if ((mob_num != null) && (!"".equals(mob_num.trim()))) {
            insertStmt.bindString(1, alias == null ? "" : alias);
            insertStmt.bindString(2, mob_num == null ? "" : mob_num);
            insertStmt.bindString(3, password == null ? "" : password);
            insertStmt.bindString(4, cardType);
            insertStmt.bindString(5, lob);
            insertStmt.bindString(6, latest_bill);
            result = insertStmt.executeInsert();
        }
        return result;
    }

    public void updateAccountInfo(String alias, String mob_num, String password, String latest_bill) {
        if (debug) Log.d("update acctInfo", alias + " " + mob_num + " " + password + " " + latest_bill);
        updateAccountInfo.bindString(1, alias == null ? "" : alias);
        updateAccountInfo.bindString(2, password == null ? "" : password);
        updateAccountInfo.bindString(4, mob_num == null ? "" : mob_num);
        updateAccountInfo.bindString(3, latest_bill);
        updateAccountInfo.execute();
    }

    public void updateSms(String mob_num) {
        if (debug) Log.d("update sms ", mob_num);
        updateSms.bindString(1, "N");
        updateSms.bindString(2, mob_num == null ? "" : mob_num);
        updateSms.execute();
    }

    public void updateLastBillDt(String mob_num, String date) {
        if (debug) Log.d("update_MyMobAcct", mob_num);
        updateLastBillDt.bindString(1, date);
        updateLastBillDt.bindString(2, mob_num == null ? "" : mob_num);
        updateLastBillDt.execute();
    }

    public void delete(String mob_num) {
        deleteDateStmt.bindString(1, mob_num);
        deleteDateStmt.execute();
    }

    public void close() {
        openHelper.close();
    }

    public Boolean isRecordExists(String mob_num) {
//		Cursor cursor = this.db.rawQuery("select 1 from myhkt_mymob where mob_num = ? ", new String[] {mob_num});
        Cursor cursor = this.db.rawQuery("select count(*) from myhkt_mymob where mob_num = ? ", new String[]{mob_num});
        if (cursor.moveToFirst()) {
            return (cursor.getInt(0) > 0);
        }
        return false;
    }

    public Boolean isSavedPassword(String mob_num) {
        Cursor cursor = this.db.rawQuery("select password from myhkt_mymob where mob_num = ? ", new String[]{mob_num});
        if (cursor.moveToFirst()) {
            if (cursor.getString(0).equalsIgnoreCase("")) {
                if (debug) Log.d("Mymob account helper", "not savepassword");
                return false;
            }
        }
        return true;
    }

    public ArrayList<HashMap<String, ?>> getList() {
        ArrayList<HashMap<String, ?>> list = new ArrayList<HashMap<String, ?>>();
        Cursor cursor = this.db.query(TABLE_NAME,
                null, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, Object> temp = new HashMap<String, Object>();
                temp.put("ALIAS", cursor.getString(0));
                temp.put("MOBNUM", cursor.getString(1));
                temp.put("MOBPWD", cursor.getString(2));
                temp.put("CARDTYPE", cursor.getString(3));
                temp.put("LOB", cursor.getString(4));
                temp.put("LATEST_BILL", cursor.getString(5));
                temp.put("SMS_FLAG", cursor.getString(6));
                list.add(temp);
            } while (cursor.moveToNext());
        }
//	    if (cursor != null && !cursor.isClosed()) {
//	        cursor.close();
//	    }
        return list;
    }

    private static class OpenHelper extends SQLiteOpenHelper {

        OpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                    "alias TEXT" +
                    ", mob_num TEXT" +
                    ", password TEXT" +
                    ", card_type TEXT" +
                    ", lob TEXT" +
                    ", latest_bill TEXT" +
                    ", sms_flag TEXT NOT NULL DEFAULT 'Y'" +
                    ", PRIMARY KEY (mob_num)" +
                    ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Nothing to do in Upgrade
        }
    }
}