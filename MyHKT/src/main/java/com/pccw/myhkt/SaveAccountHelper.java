package com.pccw.myhkt;

/************************************************************************
 File       : SaveAccountHelper.java
 Desc       : Database for a new bill in MyHKT
 Name       : SaveAccountHelper
 Created by : Vincent Fung
 Date       : 15/07/2013

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 15/07/2013 Vincent Fung          - First draft
 19/08/2013 Vincent Fung          1. Changed database column "last_bill_issued_date" to be "last_bill_date"
 2. Removed deleting all records if flag = true
 3. Changed insert and update record logic
 4. Changed No record to return null in getDateByLoginIDAndAcctNum(...)
 5. Changed housekeep procedure, i.e. delete record 1 year or earlier than from the current date
 25/10/2013 Vincent Fung		  - Changed housekeep from 1 year up to 3 years
 *************************************************************************/

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SaveAccountHelper {
    private static boolean debug = false;            // toggle debug logs
    private static final String DATABASE_NAME = "myhkt.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "myhkt_account";
    private static SaveAccountHelper saveAccountHelper = null;
    private final OpenHelper openHelper;
    private final Context context;
    private final SQLiteDatabase db;

    private static final String INSERT = "insert into "
            + TABLE_NAME + "(login_id, acctnum, last_bill_date, unread_bill_flag) values (?, ?, ?, ?)";
    private static final String UPDATE_FLAG = "update "
            + TABLE_NAME + " set unread_bill_flag = ? where login_id = ? and acctnum = ?";
    private static final String UPDATE_LAST_BILL_DATE = "update "
            + TABLE_NAME + " set last_bill_date = ?, unread_bill_flag = ? where login_id = ? and acctnum = ?";
    private static final String DELETE_DATE = "delete from "
            + TABLE_NAME + " where last_bill_date = ?";

    private final SQLiteStatement insertStmt;
    private final SQLiteStatement updateFlagStmt;
    private final SQLiteStatement updateLastBillDateStmt;
    private final SQLiteStatement deleteDateStmt;

    private SaveAccountHelper(Context context) {
        this.context = context;
        openHelper = new OpenHelper(this.context);
        db = openHelper.getWritableDatabase();
        openHelper.onCreate(db);
        insertStmt = db.compileStatement(INSERT);
        updateFlagStmt = db.compileStatement(UPDATE_FLAG);
        updateLastBillDateStmt = db.compileStatement(UPDATE_LAST_BILL_DATE);
        deleteDateStmt = db.compileStatement(DELETE_DATE);
    }

    public static SaveAccountHelper getInstance(Context context) {
        if (saveAccountHelper == null) {
            saveAccountHelper = new SaveAccountHelper(context);
        }
        return saveAccountHelper;
    }

    public long insert(String loginID, String acctNum) {
        long result = -1;
        if ((acctNum != null) && (!acctNum.trim().isEmpty())) {
            insertStmt.bindString(1, loginID == null ? "" : loginID);
            insertStmt.bindString(2, acctNum);
            insertStmt.bindString(3, "");
            insertStmt.bindLong(4, 1);    // unread flag = true;
            result = insertStmt.executeInsert();
        }
        return result;
    }

    public void updateLastBillDate(String loginID, String acctNum, String lastBillDate) {
        updateLastBillDateStmt.bindString(1, lastBillDate == null ? "" : lastBillDate); // yyyymmdd
        updateLastBillDateStmt.bindLong(2, 0); // unread flag = false;
        updateLastBillDateStmt.bindString(3, loginID == null ? "" : loginID);
        updateLastBillDateStmt.bindString(4, acctNum == null ? "" : acctNum);
        updateLastBillDateStmt.execute();
    }

    public void updateFlag(String loginID, String acctNum) {
        updateFlagStmt.bindLong(1, 1); // unread flag = true;
        updateFlagStmt.bindString(2, loginID == null ? "" : loginID);
        updateFlagStmt.bindString(3, acctNum == null ? "" : acctNum);
        updateFlagStmt.execute();
    }

    public void updateFlag(String loginID, String acctNum, int flag) {
        updateFlagStmt.bindLong(1, flag);
        updateFlagStmt.bindString(2, loginID == null ? "" : loginID);
        updateFlagStmt.bindString(3, acctNum == null ? "" : acctNum);
        updateFlagStmt.execute();
    }

    public String getDateByLoginIDAndAcctNum(String loginID, String acctNum) throws Exception {
        String result = "";
        String selection = "login_id = ? and acctnum = ?";
        String[] selectionArgs = {loginID, acctNum};
        //Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
        Cursor cursor = db.query(TABLE_NAME, new String[]{"last_bill_date"}, selection, selectionArgs, null, null, "unread_bill_flag desc");
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            result = cursor.getString(0);
        } else if (cursor.getCount() == 0) {
            result = null;
        } else {
            throw new Exception("Invalid num of rec, num of rec: " + cursor.getCount());
        }
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return result;
    }

    public boolean getFlagByLoginIDAndAcctNum(String loginID, String acctNum) throws Exception {
        int result;
        String selection = "login_id = ? and acctnum = ?";
        String[] selectionArgs = {loginID, acctNum};
        //Cursor cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
        Cursor cursor = db.query(TABLE_NAME, new String[]{"unread_bill_flag"}, selection, selectionArgs, null, null, "unread_bill_flag desc");
        if (cursor.getCount() == 1) {
            cursor.moveToFirst();
            result = (int) cursor.getLong(0);
        } else if (cursor.getCount() == 0) {
            result = -1;
        } else {
            throw new Exception("Invalid num of rec, num of rec: " + cursor.getCount());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return (result > 0);
    }

    public int getCount(String loginID) {
        int count = 0;
        String selection = "login_id = ? and unread_bill_flag = 1";
        String[] selectionArgs = {loginID};
        Cursor cursor = db.query(TABLE_NAME, new String[]{"unread_bill_flag"}, selection, selectionArgs, null, null, "unread_bill_flag desc");
        count = cursor.getCount();
        if (!cursor.isClosed()) {
            cursor.close();
        }
        return count;
    }

    public void RemoveRecordForInitial(String currServerTS) {
        // variables for setting removal period
        int year = 3;
        int mth = 0;
        int day = 0;
        // Removed all if lastBillDate is one year or earlier than current Date
        Cursor cursor = db.query(TABLE_NAME, new String[]{"last_bill_date"}, null, null, null, null, "unread_bill_flag desc");
        if (cursor.moveToFirst()) {
            do {
                // Compare Date
                String lastBill = cursor.getString(0);
                if (!"".equals(lastBill) && !"".equals(currServerTS)) {
                    SimpleDateFormat inputdf = new SimpleDateFormat("yyyyMMdd", Locale.US);
                    SimpleDateFormat inputdf_currTS = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
                    Date billTimestampDate, currServerTSDate;
                    try {
                        billTimestampDate = inputdf.parse(lastBill);
                        currServerTSDate = inputdf_currTS.parse(currServerTS);

                        Calendar cal = Calendar.getInstance();
                        cal.setTime(currServerTSDate);
                        cal.add(Calendar.YEAR, -year);
                        cal.add(Calendar.MONTH, -mth);
                        cal.add(Calendar.DATE, -day);
                        currServerTSDate = cal.getTime();

                        if (currServerTSDate.compareTo(billTimestampDate) >= 0) {
                            // Removed one year or earlier than current date
                            deleteDateStmt.bindString(1, lastBill);
                            deleteDateStmt.execute();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } while (cursor.moveToNext());
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
    }

    public void close() {
        openHelper.close();
    }

    private static class OpenHelper extends SQLiteOpenHelper {

        OpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
                    "login_id TEXT" +
                    ", acctnum TEXT" +
                    ", last_bill_date TEXT" +
                    ", unread_bill_flag INTEGER" +
                    ", PRIMARY KEY (login_id, acctnum)" +
                    ");");
            if (debug) Log.d("SaveAccountHelper", "create table");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Nothing to do in Upgrade
        }
    }
}
