package com.pccw.myhkt;


import static android.content.Context.CLIPBOARD_SERVICE;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.pccw.myhkt.activity.ActivationActivity;
import com.pccw.myhkt.activity.DirectoryInquiryActivity;
import com.pccw.myhkt.activity.RegConfirmActivity;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.listeners.OnAccountDeleteListener;
import com.pccw.myhkt.listeners.OnDialogSelectionListener;
import com.pccw.myhkt.listeners.OnLogoutListener;
import com.pccw.myhkt.qrcode.Contents;
import com.pccw.myhkt.qrcode.QRCodeEncoder;
import com.pccw.myhkt.service.LineTestIntentService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DialogHelper {

    public static void createSimpleDialog(Activity act, String message) {
        createSimpleDialog(act, message, Utils.getString(act, R.string.btn_ok));
    }

    public static void createSimpleDialog(Context ctx, String message, String btnName) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btnName, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void createSimpleDialog(Activity act, String message, String btnName) {

        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btnName, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void createSimpleDialog(Activity act, String title, String message, String btnName, OnClickListener onPositiveClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btnName, onPositiveClickListener);
        builder.show();
    }

    public static void createSimpleDialog(Activity act, String message, String btnName, OnClickListener onPositiveClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(btnName, onPositiveClickListener);
        builder.show();
    }

    public static void createSimpleDialog(Activity act, String message, String btnName, OnClickListener onPositiveClickListener, String negBtnName) {

        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setMessage(message);
        builder.setCancelable(false);

        //To Align to the layout of IOS, will exchange the position of positive button and negative button
        builder.setNegativeButton(btnName, onPositiveClickListener);
        builder.setPositiveButton(negBtnName, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void createSimpleDialog(Activity act, String message, String btnName, OnClickListener onPositiveClickListener, String negBtnName, OnClickListener onNegativeClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);
        builder.setMessage(message);

        //To Align to the layout of IOS, will exchange the position of positive button and negative button
        builder.setNegativeButton(btnName, onPositiveClickListener);
        builder.setPositiveButton(negBtnName, onNegativeClickListener);
        builder.show();
    }

    public static void createRedirectActivationDialog(final Activity act, final String id, final String pwd) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        if (act instanceof RegConfirmActivity) {
            builder.setMessage(Utils.getString(act, R.string.REGM_SUCCREG));
        } else {
            builder.setMessage(Utils.getString(act, R.string.LGIM_SHLD_ACTV));
        }
        builder.setPositiveButton(Utils.getString(act, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(act, ActivationActivity.class);
                // Loading required information to Activation
                Bundle bundle = new Bundle();
                bundle.putString("LOGINID", id);
                bundle.putString("PWD", pwd);
                intent.putExtras(bundle);
                act.startActivity(intent);
                act.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                //				finish();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                //					Intent intent = new Intent(getApplicationContext(), ActivationActivity.class);
                //					// Loading required information to Activation
                //					Bundle bundle = new Bundle();
                //					bundle.putString("LOGINID", viewholder.login_loginname.getText().toString());
                //					bundle.putString("PWD", viewholder.login_password.getText().toString());
                //					intent.putExtras(bundle);
                //					startActivity(intent);
                //					overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                //					finish();
            }
        });
        builder.create().show();
    }

    public static void createTitleDialog(Activity act, String title, String message, String btnName, OnClickListener onPositiveClickListener, String negBtnName, OnClickListener onNegativeClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setMessage(message);

        //To Align to the layout of IOS, will exchange the position of positive button and negative button
        builder.setNegativeButton(btnName, onPositiveClickListener);
        builder.setPositiveButton(negBtnName, onNegativeClickListener);
        builder.show();
    }

    public static void createTitleDialog(Activity act, String title, String message, String btnName) {
        boolean debug = act.getResources().getBoolean(R.bool.UATPRDSWITCH);
        AtomicInteger clicked = new AtomicInteger();

        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setMessage(message);
        builder.setCancelable(false);
        LayoutInflater inflater = act.getLayoutInflater();
        TextView view = (TextView) inflater.inflate(R.layout.custom_dialog_header, null);
        view.setText(title);
        if (debug) {
            view.setOnClickListener(view1 -> {
                clicked.getAndIncrement();
                String regId = ClnEnv.getPref(act, act.getString(R.string.CONST_PREF_GCM_REGID), "No token provided");
                if (regId.trim().length() > 20 && clicked.get() >= 3) {
                    ClipboardManager clipboard = (ClipboardManager) act.getSystemService(CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("label", regId);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(act, "Success token copy: " + regId, Toast.LENGTH_SHORT).show();
                    Log.d("lwg", regId);
                    clicked.set(0);
                }
            });
        }
        builder.setCustomTitle(view);
        builder.setPositiveButton(btnName, (dialog, which) -> dialog.dismiss());
        builder.create();
        builder.show();
    }

    public static void createDeleteAccountDialog(Activity act, String message, OnAccountDeleteListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);

        LinearLayout view = (LinearLayout) act.getLayoutInflater().inflate(R.layout.custom_dialog_delete_account, null);
        TextView txtViewHeader = view.findViewById(R.id.txtViewHeader);
        TextView txtEmail = view.findViewById(R.id.txtViewEmail);

        txtViewHeader.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
        txtEmail.setTextSize(14);

        txtEmail.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
        txtEmail.setTextSize(14);
        message = "(" + act.getString(R.string.delete_acount_login_id) + " " + message + ")";
        txtEmail.setText(message);

        builder.setPositiveButton(Utils.getString(act, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (listener != null) {
                    listener.onAccountDeleted();
                }
                dialogInterface.dismiss();
            }
        });
        builder.setView(view);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void createTitleDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setTitle(title);
        builder.setPositiveButton(Utils.getString(context, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void createLogoutDialog(final Activity act, String message, String btnName, String negBtnName, final OnLogoutListener logoutListener) {
        int extralinespace = act.getResources().getDimensionPixelOffset(R.dimen.extralinespace);
        int padding = Utils.dpToPx(act.getResources().getDimensionPixelOffset(R.dimen.extralinespace));
        LinearLayout logoutLayout = (LinearLayout) act.getLayoutInflater().inflate(R.layout.dialog_checkbox, null);
        logoutLayout.setPadding(extralinespace, extralinespace, extralinespace, extralinespace);
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        final CheckBox dialog_checkbox_remember = logoutLayout.findViewById(R.id.dialog_checkbox_remember);
        dialog_checkbox_remember.setButtonDrawable(R.drawable.checkbox);
        dialog_checkbox_remember.setText(act.getString(R.string.myhkt_rememberlogin));
        final float scale = act.getResources().getDisplayMetrics().density;

        builder.setView(logoutLayout);
        builder.setMessage(message);

        // Reload previous state
        if (ClnEnv.getPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_USERSAVELOGINID), false)) {
            dialog_checkbox_remember.setChecked(true);
        } else {
            dialog_checkbox_remember.setChecked(false);
        }

        builder.setCancelable(false);

        //To Align to the layout of IOS, will exchange the position of positive button and negative button
        builder.setNegativeButton(btnName, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                if (!dialog_checkbox_remember.isChecked()) {
                    // we do not keep login id
                    // clear saved login id and password (if any)
                    ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_USERSAVELOGINID), false);
                    ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_SAVELOGINID), false);
                    ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
                    ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_LOGINID), "");
                    ClnEnv.setEncPref(act.getApplicationContext(), "", act.getString(R.string.CONST_PREF_PASSWORD), "");
                    ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_PREMIER_FLAG), false);
                } else {
                    // save username login id - assume password saved elsewhere
                    ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_USERSAVELOGINID), true);
                    ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_SAVELOGINID), true);
                    ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_LOGINID), ClnEnv.getSessionLoginID());

                    // user chose not to save password; ensure password is erased
                    if (!ClnEnv.getPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_SAVEPASSWORD), false)) {
                        ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_PASSWORD), "");
                        ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_PREMIER_FLAG), false);
                    }
                }

//				// Clear Cached appointment result
//				ClnEnv.setPref(act.getApplicationContext(), act.getString(R.string.CONST_PREF_APPTIND_FLAG), false);

                //				 Stop any LineTest if lineTest is running
                act.stopService(new Intent(act, LineTestIntentService.class));
                Utils.clearLnttService(act);

                // Call doLogout();
                //				progressDialogHelper.showProgressDialog(me, me.onstopHandler, false);
                //				loginGrq = new LoginGrq();
                //				loginGrq.actionTy = DoLogoutAsyncTask.actionTy;
                //				loginGrq.sessTok = ClnEnv.getSessTok();
                //				loginGrq.utype = "RCUS";
                //
                //				doLogoutAsyncTask = new DoLogoutAsyncTask(me.getApplicationContext(), callbackHandler).execute(loginGrq);

                ClnEnv.setLgiCra(null); //remove ClnEnv's lgicra to permanently remove login data
                if (LiveChatHelper.getInstance(act, act).isPause) {
                    LiveChatHelper.getInstance(act, act).closeLiveChat();
                }
                APIsManager.doLogout((FragmentActivity) act);
                logoutListener.onLogout();
            }
        });

        builder.setPositiveButton(negBtnName, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void createRememberDialog(final Activity act, String title, String message, String btnName, String negBtnName, final OnDialogSelectionListener listener) {
        int extralinespace = act.getResources().getDimensionPixelOffset(R.dimen.extralinespace);
        int padding = Utils.dpToPx(act.getResources().getDimensionPixelOffset(R.dimen.extralinespace));
        LinearLayout logoutLayout = (LinearLayout) act.getLayoutInflater().inflate(R.layout.dialog_checkbox, null);
        logoutLayout.setPadding(extralinespace, extralinespace, extralinespace, extralinespace);
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        final CheckBox dialogCheckBox = logoutLayout.findViewById(R.id.dialog_checkbox_remember);
        dialogCheckBox.setButtonDrawable(R.drawable.checkbox);
        dialogCheckBox.setText(message);
        builder.setView(logoutLayout);
        builder.setMessage(title);
        builder.setCancelable(false);

        //To Align to the layout of IOS, will exchange the position of positive button and negative button
        builder.setNegativeButton(btnName, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onPositiveClickListener(dialogCheckBox.isChecked());
            }
        });

        builder.setPositiveButton(negBtnName, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onCancelClickListener();
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void createAreaDistrictDialog(final Activity act, String[] area, final List<String[]> district, int areaPos, int districtPos, OnClickListener onPositiveClickListener) {

        int aPos = areaPos;
        int dPos = districtPos;
        View view = act.getLayoutInflater().inflate(R.layout.area_district_picker, null);


        final NumberPicker numberPicker2 = view.findViewById(R.id.numberPicker2);
        numberPicker2.setMinValue(0);
        numberPicker2.setMaxValue(district.get(aPos).length - 1);
        numberPicker2.setDisplayedValues(district.get(aPos));
        numberPicker2.setWrapSelectorWheel(false);

        final NumberPicker numberPicker1 = view.findViewById(R.id.numberPicker1);
        numberPicker1.setMinValue(0);
        numberPicker1.setMaxValue(area.length - 1);
        numberPicker1.setDisplayedValues(area);
        numberPicker1.setWrapSelectorWheel(false);
        numberPicker1.setOnValueChangedListener(new OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (oldVal != newVal) {

                    numberPicker2.setDisplayedValues(null);

                    numberPicker2.setMinValue(0);
                    numberPicker2.setMaxValue(district.get(newVal).length - 1);
                    numberPicker2.setDisplayedValues(district.get(newVal));
                    numberPicker2.setValue(0);
                    numberPicker2.setWrapSelectorWheel(false);
                }
            }
        });
        numberPicker1.setValue(aPos);
        numberPicker2.setValue(dPos);
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setCancelable(false);
        builder.setView(view);

        //To Align to the layout of IOS, will exchange the position of positive button and negative button
        builder.setNegativeButton("Confirm", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (act instanceof DirectoryInquiryActivity) {
                    ((DirectoryInquiryActivity) act).recieveCallBack(numberPicker1.getValue(), numberPicker2.getValue());
                }
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Cancel", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void createInfoDialog(final Activity act, String msg) {
        Rect rectangle = new Rect();
        Window window = act.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);

        int statusBarHeight = rectangle.top;
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = Math.abs(contentViewTop - statusBarHeight);
        int btnH = act.getResources().getDimensionPixelOffset(R.dimen.livechat_browser_btn);
        int extralinespace = (int) act.getResources().getDimension(R.dimen.extralinespace);
        int imageHeight = act.getResources().getDimensionPixelOffset(R.dimen.button_height) + act.getResources().getDimensionPixelOffset(R.dimen.padding_screen) * 5 / 2;
        int padding = act.getResources().getDimensionPixelOffset(R.dimen.edittextpadding);

        //Dialog setting
        final Dialog lDialog = new Dialog(act, android.R.style.Theme_Translucent_NoTitleBar);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ctView = inflater.inflate(R.layout.dialog_info, null);
        lDialog.setContentView(ctView);
        lDialog.setCanceledOnTouchOutside(false);
        lDialog.setCancelable(false);
        AAQuery aq = new AAQuery(ctView);
        ImageView close = lDialog.findViewById(R.id.dialog_info_close);

        Bitmap bmmclose = ViewUtils.scaleToFitHeight(act, R.drawable.btn_close, btnH);
        aq.id(R.id.dialog_info_close).image(bmmclose);
        aq.marginpx(R.id.dialog_info_close, padding, padding, extralinespace * 2, padding);

        aq.id(R.id.dialog_info_frame).background(R.drawable.hkt_edittextblue_bg);
        aq.marginpx(R.id.dialog_info_frame, extralinespace * 2, 0, extralinespace * 2, padding / 2);
        aq.padding(R.id.dialog_info_frame, extralinespace, extralinespace, extralinespace, extralinespace);

        aq.normText(R.id.dialog_info_title, msg);
        aq.id(R.id.dialog_info_title).height(ViewGroup.LayoutParams.WRAP_CONTENT, false);
        aq.gravity(R.id.dialog_info_title, Gravity.CENTER);

        ((LinearLayout) aq.id(R.id.dialog_info_frame).getView()).setGravity(Gravity.CENTER);


        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lDialog.dismiss();
            }
        });
//		lDialog.create();
        lDialog.show();
    }


    public static void createAlignedInfoDialog(final Activity act, String msg, int alignment) {
        Rect rectangle = new Rect();
        Window window = act.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);

        int statusBarHeight = rectangle.top;
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = Math.abs(contentViewTop - statusBarHeight);
        int btnH = act.getResources().getDimensionPixelOffset(R.dimen.livechat_browser_btn);
        int extralinespace = (int) act.getResources().getDimension(R.dimen.extralinespace);
        int imageHeight = act.getResources().getDimensionPixelOffset(R.dimen.button_height) + act.getResources().getDimensionPixelOffset(R.dimen.padding_screen) * 5 / 2;
        int padding = act.getResources().getDimensionPixelOffset(R.dimen.edittextpadding);

        //Dialog setting
        final Dialog lDialog = new Dialog(act, R.style.MyDialogStyle);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ctView = inflater.inflate(R.layout.dialog_info, null);
        lDialog.setContentView(ctView);
        lDialog.setCanceledOnTouchOutside(false);
        lDialog.setCancelable(false);
        AAQuery aq = new AAQuery(ctView);
        ImageView close = lDialog.findViewById(R.id.dialog_info_close);

        Bitmap bmmclose = ViewUtils.scaleToFitHeight(act, R.drawable.btn_close, btnH);
        aq.id(R.id.dialog_info_close).image(bmmclose);
        aq.marginpx(R.id.dialog_info_close, padding, padding, extralinespace * 2, padding);

        aq.id(R.id.dialog_info_frame).background(R.drawable.hkt_edittextblue_bg);
        aq.marginpx(R.id.dialog_info_frame, extralinespace * 2, 0, extralinespace * 2, padding / 2);
        aq.padding(R.id.dialog_info_frame, extralinespace, extralinespace, extralinespace, extralinespace);

        aq.displayText(R.id.dialog_info_title, msg);
        aq.id(R.id.dialog_info_title).height(ViewGroup.LayoutParams.WRAP_CONTENT, false);
        aq.gravity(R.id.dialog_info_title, alignment);

        ((LinearLayout) aq.id(R.id.dialog_info_frame).getView()).setGravity(Gravity.CENTER);


        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lDialog.dismiss();
            }
        });
        lDialog.show();
    }

    public static void create7ElevenDialog(final Activity act, int lob, String srvType, String billDt, String acctNum, String billType, double billAmt) {
        //UI
        Rect rectangle = new Rect();
        Window window = act.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = Math.abs(contentViewTop - statusBarHeight);
        int btnH = act.getResources().getDimensionPixelOffset(R.dimen.livechat_browser_btn);
        int extralinespace = (int) act.getResources().getDimension(R.dimen.extralinespace);
        int imageHeight = act.getResources().getDimensionPixelOffset(R.dimen.button_height) + act.getResources().getDimensionPixelOffset(R.dimen.padding_screen) * 5 / 2;
        int padding = act.getResources().getDimensionPixelOffset(R.dimen.edittextpadding);
        int reglogo_padding = act.getResources().getDimensionPixelOffset(R.dimen.reg_logo_padding_1);
        int basePadding = (int) act.getResources().getDimension(R.dimen.basePadding);

        //Dialog setting
        final Dialog lDialog = new Dialog(act, R.style.MyDialogStyle);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ctView = inflater.inflate(R.layout.dialog_7eleven, null);
        lDialog.setContentView(ctView);
        lDialog.setCanceledOnTouchOutside(false);
        lDialog.setCancelable(false);

        AAQuery aq = new AAQuery(ctView);
        ImageView close = lDialog.findViewById(R.id.dialog_7eleven_close);

        aq.id(R.id.dialog_7eleven_lob).image(Utils.getLobIcon(lob, 0));
        //lob footer
        aq.id(R.id.dialog_7eleven_remark_zh).text(R.string.myhkt_qrcode_remark_zh).textSize(AAQuery.getDefaultTextSize() - 2);
        aq.marginpx(R.id.dialog_7eleven_remark_zh, basePadding, padding, basePadding, 0);

        aq.id(R.id.dialog_7eleven_remark).text(R.string.myhkt_qrcode_remark).textSize(AAQuery.getDefaultTextSize() - 2);
        aq.marginpx(R.id.dialog_7eleven_remark, basePadding, 0, basePadding, 0);

        WindowManager wm = (WindowManager) act.getSystemService(Context.WINDOW_SERVICE);
        int screenWidth = wm.getDefaultDisplay().getWidth();
        aq.id(R.id.dialog_7eleven_qrcode).width(150);
        aq.id(R.id.dialog_7eleven_qrcode).height(150);
        aq.id(R.id.dialog_7eleven_qrcode).image(R.drawable.lob_1010_plain);
        aq.gravity(R.id.dialog_7eleven_qrcode, Gravity.CENTER);
        aq.normText(R.id.dialog_7eleven_title, act.getResources().getString(R.string.qrcode_title));
        aq.gravity(R.id.dialog_7eleven_title, Gravity.CENTER);
        aq.marginpx(R.id.dialog_7eleven_title, basePadding, 0, basePadding, 0);

        Bitmap bmmclose = ViewUtils.scaleToFitHeight(act, R.drawable.btn_close, btnH);
        aq.id(R.id.dialog_7eleven_close).image(bmmclose);
//		aq.marginpx(R.id.dialog_7eleven_close, padding, padding, extralinespace, padding);
        aq.marginpx(R.id.dialog_7eleven_close, 0, 0, extralinespace, 0);

        aq.id(R.id.dialog_7eleven_frame).background(R.drawable.hkt_edittextblue_bg);
        aq.marginpx(R.id.dialog_7eleven_frame, extralinespace, 0, extralinespace, padding / 2);
        aq.padding(R.id.dialog_7eleven_frame, 0, reglogo_padding, 0, extralinespace);
        CellViewAdapter cellViewAdapter = new CellViewAdapter(ctView.getContext());
        ((LinearLayout) aq.id(R.id.dialog_7eleven_frame).getView()).setGravity(Gravity.CENTER);
        LinearLayout frame = ((LinearLayout) aq.id(R.id.dialog_7eleven_listview).getView());
        //Prepare QR info
        String merchantCode = "055";
        List<Cell> cellList = new ArrayList<Cell>();
        SmallTextCell cell1 = new SmallTextCell(act.getResources().getString(R.string.myhkt_qrcode_servicetype), srvType);
        if (lob == R.string.CONST_LOB_LTS && "en".equals(ClnEnv.getAppLocale(act.getBaseContext()))) {
            cell1.setContentSizeDelta(-2);
        }
        cell1.setBgColorId(R.color.transparent);
        cell1.setLeftPadding(basePadding);
        cell1.setRightPadding(basePadding);
        cell1.setServiceType(srvType);

        SmallTextCell cell2 = new SmallTextCell(act.getResources().getString(R.string.qrcode_bill_date_txt), billDt);
        cell2.setBgColorId(R.color.transparent);
        cell2.setLeftPadding(basePadding);
        cell2.setRightPadding(basePadding);
        cell2.setServiceType(srvType);
        String accNoLabel = (lob == R.string.CONST_LOB_PCD || lob == R.string.CONST_LOB_TV) ? act.getResources().getString(R.string.pcd_qrcode_recacct_num) : act.getResources().getString(R.string.qrcode_acc_no_txt);

        SmallTextCell cell3 = new SmallTextCell(accNoLabel, Tool.formatAcctNum(acctNum));
        cell3.setBgColorId(R.color.transparent);
        cell3.setLeftPadding(basePadding);
        cell3.setRightPadding(basePadding);
        cell3.setServiceType(srvType);

        SmallTextCell cell4 = new SmallTextCell(act.getResources().getString(R.string.qrcode_bill_type_mobile_txt), billType);
        cell4.setBgColorId(R.color.transparent);
        cell4.setLeftPadding(basePadding);
        cell4.setRightPadding(basePadding);
        cell4.setServiceType(srvType);

        SmallTextCell cell5 = new SmallTextCell(act.getResources().getString(R.string.qrcode_amt_txt), Utils.convertStringToPrice(Utils.convertDoubleToString(billAmt, 2)));
        cell5.setBgColorId(R.color.transparent);
        cell5.setLeftPadding(basePadding);
        cell5.setRightPadding(basePadding);
        cell5.setServiceType(srvType);

        cellList.add(cell1);
        cellList.add(cell2);
        cellList.add(cell3);
        cellList.add(cell4);
        cellList.add(cell5);

        cellViewAdapter.setView(frame, cellList);
        //Prepare QR string
        //format bill amount
        String amt = Utils.convertDoubleToString(billAmt, 2);
        amt = amt.replaceAll("\\.", "");
        // Add padding up to 10 digits
        char[] paddedAmt = "0000000000".toCharArray(), amtCharArr = amt.toCharArray();
        int amtLength = amt.length();
        for (int iCount = 10 - amtLength; iCount < 10; iCount++) {
            paddedAmt[iCount] = amtCharArr[iCount - 10 + amtLength];
        }
        amt = String.valueOf(paddedAmt);
        String qrString = String.format("%s%s%s%s", merchantCode, billType, acctNum, amt);

        //Generate QR
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(act, "", qrString, null, Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(), 360);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            aq.id(R.id.dialog_7eleven_qrcode).image(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        aq.id(R.id.dialog_7eleven_qrcode_layout).background(R.drawable.shadow4);
        aq.gravity(R.id.dialog_7eleven_qrcode_layout, Gravity.CENTER);
        aq.padding(R.id.dialog_7eleven_qrcode_layout, extralinespace, basePadding, extralinespace, basePadding);
        close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                lDialog.dismiss();
            }
        });
        lDialog.show();
    }
}
