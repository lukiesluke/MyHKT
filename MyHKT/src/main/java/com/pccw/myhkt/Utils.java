package com.pccw.myhkt;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import androidx.browser.customtabs.CustomTabColorSchemeParams;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.InbxCra;
import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.myhkt.enums.HKTDataType;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.service.LineTestIntentService;
import com.pccw.myhkt.util.Constant;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final String TAG = "lwg";
    private static final String USAGE_CONDITION_FUP = "FUP";
    private static final String USAGE_CONDITION_QOS = "QoS";
    private static final String USAGE_CONDITION_NORMAL = "normal";
    public static final int RESULT_RE_LOGIN = 1983;

    private static Context myContext = null;
    private static int id = 1;
    public static boolean ISMYLIENTEST = false;
    public static boolean IS_IDD = false;
    // REF-JF-20111220
    private static String currentModule;
    private static boolean needLiveChatDisclaimer = true; //true = required, false = not required

    static public Context getContext(Context context) {
        if (myContext == null) {
            myContext = context.getApplicationContext();
        }
        return context;
    }

    public static int getUniqueId() {
        return id++;
    }

    public static void alert(Context ctx, String message, String okLabel) {
        alert(ctx, Utils.getString(ctx, R.string.alert), message, okLabel);
    }

    public static void alert(Context ctx, String title, String message, String okLabel) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNeutralButton(okLabel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        AlertDialog ad = builder.create();
        ad.setCanceledOnTouchOutside(false);
        ad.show();
    }

    public static String truncateStringDot(String input) {
        String output = "";
        try {
            if (input.indexOf(".") != -1) {
                output = input.substring(0, input.indexOf("."));
                return output;
            } else return input;
        } catch (Exception e) {
            e.printStackTrace();
            return input;
        }
    }

    public static String convertDoubleToString(double value, int decimalPlace) {
        String result = "";
        try {
            result = String.valueOf(value);
            String token = "#0.";
            for (int i = 0; i < decimalPlace; i++) {
                token += "0";
            }
            DecimalFormat decimalFormat = new DecimalFormat(token);
            result = decimalFormat.format(value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String convertStringToPrice(String str) {
        DecimalFormat myFormatter = new DecimalFormat("$###,##0.00");
        try {
            return myFormatter.format(parseDouble(str));
        } catch (Exception e) {
            e.printStackTrace();
            return "$" + str;
        }
    }

    public static String convertStringToPrice0dec(String str) {
        DecimalFormat myFormatter = new DecimalFormat("$###,###");
        try {
            return myFormatter.format(parseDouble(str));
        } catch (Exception e) {
            e.printStackTrace();
            return "$" + str;
        }
    }

    public static double parseDouble(String str) {
        str = str.trim();
        str = str.replace("$", "");
        str = str.replace(",", "");

        try {
            return Double.parseDouble(str);
        } catch (NumberFormatException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean isVersionUptodate(String currentVersion, String serverVersion) {
        boolean isUptodate = false;
        // JF-20111011 ++
        // change version number format from x.xx to x.x.x
        int currentDotCount = currentVersion.length() - currentVersion.replaceAll("\\.", "").length();
        int serverDotCount = serverVersion.length() - serverVersion.replaceAll("\\.", "").length();

        if ((currentDotCount == 1) && (serverDotCount == 1)) {
            // old version number x.x
            isUptodate = Double.parseDouble(currentVersion) >= Double.parseDouble(serverVersion);
        } else if ((currentDotCount == 2) && (serverDotCount == 2)) {
            // new version number x.x.x
            isUptodate = Double.parseDouble(currentVersion.replaceFirst("\\.", "")) >= Double.parseDouble(serverVersion.replaceFirst("\\.", ""));
        } else isUptodate = (currentDotCount != 1) || (serverDotCount != 2);
        // JF-20111011 --
        return isUptodate;
    }

    public static String toDateString(String dateString, String patternString, String outPatternString) {
        try {
            SimpleDateFormat formatStrDate = new SimpleDateFormat(patternString, Locale.ENGLISH);
            Date date = formatStrDate.parse(dateString);

            SimpleDateFormat dateFormat = new SimpleDateFormat(outPatternString, Locale.ENGLISH);
            return dateFormat.format(date);
        } catch (Exception e) {
            Log.e(TAG, "toLocaleDateString error: dateString=" + dateString + ", patternString=" + patternString + ", Exception:" + e.toString());
            return dateString;
        }
    }

    public static Date toDate(String dateString, String patternString) {
        SimpleDateFormat sdf = new SimpleDateFormat(patternString, Locale.ENGLISH);
        Date d;
        try {
            d = sdf.parse(dateString);
            return d;
        } catch (Exception e) {
            Log.e("Utils", "toLocaleDateString error: dateString=" + dateString + ", patternString=" + patternString + ", Exception:" + e.toString());
            return null;
        }
    }

    public static String toDateString(String dateString, String patternString, String outPatternString, boolean isZH) {
        SimpleDateFormat sdf = new SimpleDateFormat(patternString, isZH ? Locale.CHINESE : Locale.ENGLISH);
        Date d;
        try {
            d = sdf.parse(dateString);
            sdf = new SimpleDateFormat(outPatternString, isZH ? Locale.CHINESE : Locale.ENGLISH);
            return sdf.format(d);
        } catch (Exception e) {
            Log.e("Utils", "toLocaleDateString error: dateString=" + dateString + ", patternString=" + patternString + ", Exception:" + e.toString());
            return dateString;
        }
    }

    public static String toDateString(Date date, String outPatternString) {
        SimpleDateFormat sdf = new SimpleDateFormat(outPatternString, Locale.ENGLISH);
        try {
            return sdf.format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static String toTimeString(String timeString, String patternString, String outPatternString) {
        SimpleDateFormat sdf = new SimpleDateFormat(patternString, Locale.ENGLISH);
        Date d;

        try {
            d = sdf.parse(timeString);
            sdf = new SimpleDateFormat(outPatternString, Locale.ENGLISH);

            return sdf.format(d);
        } catch (Exception e) {
            Log.e("Utils", "toLocaleDateString error: dateString=" + timeString + ", patternString=" + patternString + ", Exception:" + e.toString());
            return timeString;
        }
    }

    public static void changeButtonImageColor(Button button, int color, int position) {
        //drawable position Left=0 Top=1 Right=2 Bottom=3
        Drawable drawable = button.getCompoundDrawables()[position];
        if (drawable != null) {
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            switch (position) {
                case 0:
                    button.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                    break;
                case 1:
                    button.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
                    break;
                case 2:
                    button.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
                    break;
                case 3:
                    button.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
                    break;
            }
        }
    }

    // REF-FL 20111104 ++
    public static String formatNumber(double value) {
        DecimalFormat formatter = new DecimalFormat("#,###,###,###,###,###");
        return formatter.format(value);
    }

    // REF-FL 20111104 --
    public static String formatNumber(String value) {
        try {
            return formatNumber(Double.parseDouble(value));
        } catch (Exception e) {
            return value;
        }
    }

    // CSP-CR2013011 - Adding IDD service & Onecall at CS Portal
    public static int getLtsSrvTypeDemo(String srv_num) {
        int num = Integer.parseInt(srv_num);
        return switch (num % 5) {
            case 0 -> R.string.CONST_LTS_IDD0060;
            case 1 -> R.string.CONST_LTS_EYE;
            case 2 -> R.string.CONST_LTS_CALLINGCARD;
            case 3 -> R.string.CONST_LTS_ONECALL;
            case 4 -> R.string.CONST_LTS_FIXEDLINE;
            default -> R.string.CONST_LTS_INVALID;
        };
    }

    public static int getLtsSrvType(String tos, String eye_grp, String pri_login_ind) {
        // validation
        if (tos == null || eye_grp == null || pri_login_ind == null) {
            return R.string.CONST_LTS_INVALID;
        }

        // IDD0060 - MOB MOB / MOB TEL
        if (tos.equals(SubnRec.TOS_LTS_MOB)) {
            return R.string.CONST_LTS_IDD0060;
        }

        // ITS - Global Calling Card
        if (tos.equals(SubnRec.TOS_LTS_ITS)) {
            return R.string.CONST_LTS_CALLINGCARD;
        }

        // eye
        if (tos.equals(SubnRec.TOS_LTS_EYE)) {
            return R.string.CONST_LTS_EYE;
        }

        // OneCall Service
        if (tos.equals(SubnRec.TOS_LTS_ONC)) {
            return R.string.CONST_LTS_ONECALL;
        }

        // ICFS
        if (tos.equals(SubnRec.TOS_LTS_ICF)) {
            return R.string.CONST_LTS_ICFS;
        }

        if (tos.equals(SubnRec.TOS_LTS_TEL)) {
            // eye
            if (!eye_grp.trim().isEmpty()) {
                return R.string.CONST_LTS_EYE;
            }

            // OneCall Service
            if (pri_login_ind.equalsIgnoreCase("O")) {
                return R.string.CONST_LTS_ONECALL;
            }

            // ICFS
            if (pri_login_ind.equalsIgnoreCase("I")) {
                return R.string.CONST_LTS_ICFS;
            }
            // Fixed Line
            return R.string.CONST_LTS_FIXEDLINE;
        }
        return R.string.CONST_LTS_INVALID;
    }

    // Replaced all 1000 by 28886228 when Account Type is Premier
    public static String getString(Context cxt, int stringResId) {
        String str = "";
        boolean isPremier = ClnEnv.getSessionPremierFlag();
        str = cxt.getResources().getString(stringResId);
        if (isPremier) {
            switch (stringResId) {
                case R.string.boost_result_fail:
                case R.string.ATIMF_HTLN_NUM:
                case R.string.support_consumer_hotline_no:
                case R.string.myhkt_linetest_hotline:
                case R.string.LGI_INACTIVE_CUST:
                case R.string.LGI_INACTIVE_SVEE:
                    str = str.replaceAll("1000", "28886228");
                    str = str.replaceAll("28834609", "28886228");
                    break;
            }
        }
        return str;
    }

    public static String getCurrentModule() {
        return currentModule;
    }

    public static void setCurrentModule(String moduleId) {
        currentModule = moduleId;
    }

    public static boolean getLiveChatDisclaimerFlag() {
        return needLiveChatDisclaimer;
    }

    public static void setLiveChatDisclaimerFlag(boolean needDisclaimer) {
        needLiveChatDisclaimer = needDisclaimer;
    }

    public static void clearBillMsgService(Context cxt) {
        ClnEnv.setPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
    }

    // Convert Object to String by Serializing Object
    public static <T extends Serializable> String serialize(T item) {
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream;
        try {
            objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(item);
            objectOutputStream.close();
            return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
        } catch (Exception e) {
            // fail to convert serializable Object
            return "";
        }
    }

    // Convert String to Object by deSerializing String
    public static <T extends Serializable> T deserialize(String data) {
        try {
            byte[] dataBytes = Base64.decode(data, Base64.DEFAULT);
            final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(dataBytes);
            final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

            @SuppressWarnings({"unchecked"}) final T obj = (T) objectInputStream.readObject();

            objectInputStream.close();
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // Convert timeslot to datetime string for SR
    // Timeslot Code represents to corresponding timeslots [refer to CSP excel]
    // 09 : 0900 - 1100
    // 11 : 1100 - 1300
    // 14 : 1400 - 1600
    // 16 : 1600 - 1800
    // 18 : 1800 - 2000
    // 20 : 2000 - 2200
    // AM : 1000 - 1300
    // PM : 1400 - 1800
    public static String getDateTime(String timeslot, String outputFormat, boolean isStartTime) {
        String datetime = "";
        if ("09".equalsIgnoreCase(timeslot)) {
            datetime = isStartTime ? "090000" : "110000";
        } else if ("11".equalsIgnoreCase(timeslot)) {
            datetime = isStartTime ? "110000" : "130000";
        } else if ("14".equalsIgnoreCase(timeslot)) {
            datetime = isStartTime ? "140000" : "160000";
        } else if ("16".equalsIgnoreCase(timeslot)) {
            datetime = isStartTime ? "160000" : "180000";
        } else if ("18".equalsIgnoreCase(timeslot)) {
            datetime = isStartTime ? "180000" : "200000";
        } else if ("20".equalsIgnoreCase(timeslot)) {
            datetime = isStartTime ? "200000" : "220000";
        } else if ("AM".equalsIgnoreCase(timeslot)) {
            datetime = isStartTime ? "100000" : "130000";
        } else if ("PM".equalsIgnoreCase(timeslot)) {
            datetime = isStartTime ? "140000" : "180000";
        }
        return toDateString(datetime, "HHmmss", outputFormat);
    }

    // Output TimeSlot String format : dd-MMM HH:mm - HH:mm
    public static String getTimeSlotDateTime(Context cxt, String dateString, String timeslot) {
        //String timeslotDate = toDateString(dateString, Utils.getString(cxt, R.string.input_date_format), Tool.DATEFMT_6);
        String timeslotDate = dateString;
        try {
            SimpleDateFormat inputdf = new SimpleDateFormat(Utils.getString(cxt, R.string.input_date_format), Locale.US);
            Date apptDate = inputdf.parse(dateString);
            inputdf = new SimpleDateFormat("dd-MMM", Locale.US);
            timeslotDate = inputdf.format(apptDate);
        } catch (Exception e) {
            timeslotDate = dateString;
        }
        String timeslotTime = String.format("%s - %s", getDateTime(timeslot, getString(cxt, R.string.output_time_format), true), getDateTime(timeslot, getString(cxt, R.string.output_time_format), false));
        //special handling for time string that is "09:00 to 13:00" switch to "10:00 to 13:00", for display only
        timeslotTime = "09:00 - 13:00".equals(timeslotTime) ? "10:00 - 13:00" : timeslotTime;
        return String.format("%s %s", timeslotDate, timeslotTime).trim();
    }

    // Output TimeSlot String format : dd/MM/YYYY HH:mm - HH:mm
    public static String getTimeSlotDateTime1(Context cxt, String dateString, String timeslot) {
        String timeslotDate;
        try {
            SimpleDateFormat inputdf = new SimpleDateFormat(Utils.getString(cxt, R.string.input_date_format), Locale.US);
            Date apptDate = inputdf.parse(dateString);
            inputdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            timeslotDate = inputdf.format(apptDate);
        } catch (Exception e) {
            timeslotDate = dateString;
        }
        String timeslotTime = String.format("%s - %s", getDateTime(timeslot, getString(cxt, R.string.output_time_format), true), getDateTime(timeslot, getString(cxt, R.string.output_time_format), false));
        //special handling for time string that is "09:00 to 13:00" switch to "10:00 to 13:00", for display only
        timeslotTime = "09:00 - 13:00".equals(timeslotTime) ? "10:00 - 13:00" : timeslotTime;
        return String.format("%s %s", timeslotDate, timeslotTime).trim();
    }

    // Translate SR Validation Message
    public static String interpretRC_SRMdu(Context cxt, String rRC) {
        if (rRC.equals("RC_SR_IVCTNAME")) {
            return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_NAME);
        }
        if (rRC.equals("RC_SR_NONENG_NAME")) {
            return Utils.getString(cxt, R.string.MYHKT_SR_ERR_NOT_ENGLISH_NAME);
        }
        if (rRC.equals("RC_SR_IVCTNUM")) {
            return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_TEL);
        }
        if (rRC.equals("RC_SR_IVAPPTTS")) {
            return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_TIMESLOT);
        }
        if (rRC.equals("RC_SR_IVUPDTY")) {
            return Utils.getString(cxt, R.string.MYHKT_SR_ERR_NO_UPDATE_CHANGE);
        }
        return ClnEnv.getRPCErrMsg(cxt, rRC);
    }

    public static String getIpAddress() {
        try {
            for (Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            //Log.e("Socket exception in GetIP Address of Utilities", ex.toString());
        }
        return null;
    }

    public static String sha256(String input, String salt) {
        input = salt + input;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();

            byte[] byteData = digest.digest(input.getBytes(StandardCharsets.UTF_8));
            StringBuffer sb = new StringBuffer();
            for (byte byteDatum : byteData) {
                sb.append(Integer.toString((byteDatum & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public static String randomString(int length) {
        String AB = "123456789";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static int dpToPx(int dp) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics());
        return (int) px;
    }

    //Search and remove punctuation between 2 strings from a pool of string
    public static String punctuationRemover(String str, String startPoint, String endPoint, String textToRemove) {
        Pattern pattern = Pattern.compile(startPoint + "(.*?)" + endPoint);
        Matcher matcher = pattern.matcher(str);
        StringBuffer sb = new StringBuffer(str.length());
        while (matcher.find()) {
            String result = Objects.requireNonNull(matcher.group(1)).replaceAll(textToRemove, "");
            matcher.appendReplacement(sb, startPoint + Matcher.quoteReplacement(result) + endPoint);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    //scale image bitmap from drawable
    public static Bitmap scaleImage(Context cxt, int imgResource, int width, int height) {
        Bitmap image = BitmapFactory.decodeResource(cxt.getResources(), imgResource);
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static void scaleImage(ImageView view, float reduceScale) {
        // Get bitmap from the the ImageView.
        Bitmap bitmap = null;
        Drawable drawing = view.getDrawable();
        bitmap = ((BitmapDrawable) drawing).getBitmap();

        // Get current dimensions AND the desired bounding box
        int width = 0;
        int height = 0;
        width = bitmap.getWidth();
        height = bitmap.getHeight();

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(reduceScale, reduceScale);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        BitmapDrawable result = new BitmapDrawable(scaledBitmap);

        // Apply the scaled bitmap
        view.setImageDrawable(result);

        // Now change ImageView's dimensions to match the scaled image
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);
        view.setScaleType(ScaleType.CENTER_INSIDE);
    }

    //Concat array
    public static <T> T[] concatArray(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static String convertCsimAccNum(String accnum) {
        //convert c-sim account number to 14 digit account number
        accnum = "7700" + accnum;
        int sum = Integer.parseInt(Character.toString(accnum.charAt(0))) * 12;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(1))) * 11;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(2))) * 10;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(3))) * 9;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(4))) * 8;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(5))) * 7;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(6))) * 6;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(7))) * 5;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(8))) * 4;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(9))) * 3;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(10))) * 2;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(11))) * 1;

        int checkdigit = 97 - (sum % 97);
        String sCheckDigit = Integer.toString(checkdigit).length() == 1 ? "0" + Integer.toString(checkdigit) : Integer.toString(checkdigit);
        accnum = accnum + sCheckDigit;
        return accnum;
    }

    public static String convertPCDTVAccNum(String accnum) {
        //convert PCDTV account number to 14 digit account number
        accnum = "68" + accnum;
        int sum = Integer.parseInt(Character.toString(accnum.charAt(0))) * 12;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(1))) * 11;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(2))) * 10;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(3))) * 9;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(4))) * 8;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(5))) * 7;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(6))) * 6;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(7))) * 5;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(8))) * 4;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(9))) * 3;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(10))) * 2;
        sum = sum + Integer.parseInt(Character.toString(accnum.charAt(11))) * 1;

        int checkdigit = 97 - (sum % 97);
        String sCheckDigit = Integer.toString(checkdigit).length() == 1 ? "0" + Integer.toString(checkdigit) : Integer.toString(checkdigit);
        accnum = accnum + sCheckDigit;
        return accnum;
    }

    public static void closeSoftKeyboard(Activity act, View focusView) {
        try {
            InputMethodManager inputManager = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(focusView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            inputManager = null;
        } catch (Exception e) {
            // fail to hide the keyboard
        }
    }

    public static void closeSoftKeyboard(Activity act) {
        try {
            InputMethodManager inputManager = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            inputManager = null;
        } catch (Exception e) {
            // fail to hide the keyboard
        }
    }

    // Get LnttCra from SharePreference, if empty String return new LnttAgent Object
    public static LnttAgent getPrefLnttAgent(Context cxt) {
        String strLnttAgent = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "");
        LnttAgent rlnttAgent = !"".equalsIgnoreCase(strLnttAgent) ? (LnttAgent) Utils.deserialize(strLnttAgent) : new LnttAgent();
        return rlnttAgent;
    }

    public static boolean isLnttCompleted(Context cxt) {
        LnttAgent rlnttAgent = getPrefLnttAgent(cxt);
        if (rlnttAgent.getLnttCra() == null) return false;
        return !"".equalsIgnoreCase(rlnttAgent.getLnttCra().getISubnRec().srvNum) && !"".equalsIgnoreCase(rlnttAgent.getEndTimestamp());
    }

    // Check Lntt Result expired or not
    public static boolean isExpiredLnttResult(Context cxt, String LtrsTimestamp) {
        boolean debug = cxt.getResources().getBoolean(R.bool.DEBUG);
        SimpleDateFormat inputdf = new SimpleDateFormat(Utils.getString(cxt, R.string.input_datetime_format), Locale.US);
        Date ltrsDate, currDate;
        try {
            Calendar cal = Calendar.getInstance();
            currDate = new Date(System.currentTimeMillis());
            if (debug) Log.d("isExpiredLnttResult", "currDate = " + currDate.toString());
            ltrsDate = inputdf.parse(LtrsTimestamp);
            // TODO: test on expired case
            cal.setTime(ltrsDate);
            cal.add(Calendar.HOUR_OF_DAY, 2);
            ltrsDate = cal.getTime();
            if (debug) Log.d("isExpiredLnttResult", "ltrsDate = " + ltrsDate.toString());
            if (ltrsDate.compareTo(currDate) > 0) {
                return false;
            }
        } catch (Exception e) {
            // fail to convert date
            e.printStackTrace();
        }
        return true;
    }

    public static void clearLnttService(Context cxt) {
        LineTestIntentService.lnttRtryCnt = 0;
        ClnEnv.setPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "");
        ClnEnv.setPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);

        // Remove Notification
        ((NotificationManager) cxt.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(cxt.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_LNTT));
    }

    public static int getLobType(String lob) {
        if (SubnRec.LOB_LTS.equalsIgnoreCase(lob)) return R.string.CONST_LOB_LTS;
        else if (SubnRec.LOB_101.equalsIgnoreCase(lob)) return R.string.CONST_LOB_1010;
        else if (SubnRec.LOB_MOB.equalsIgnoreCase(lob)) return R.string.CONST_LOB_MOB;
        else if (SubnRec.LOB_IOI.equalsIgnoreCase(lob)) return R.string.CONST_LOB_IOI;
        else if (SubnRec.LOB_O2F.equalsIgnoreCase(lob)) return R.string.CONST_LOB_O2F;
        else if (SubnRec.LOB_PCD.equalsIgnoreCase(lob)) return R.string.CONST_LOB_PCD;
        else if (SubnRec.LOB_TV.equalsIgnoreCase(lob)) return R.string.CONST_LOB_TV;
        else if (SubnRec.LOB_CSP.equalsIgnoreCase(lob) || SubnRec.WLOB_XCSP.equalsIgnoreCase(lob))
            return R.string.CONST_LOB_CSP;
        else return 0;
    }

    private static boolean is1010(int lob) {
        return lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI;
        //Note: SubnRec.WLOB_X101 is not yet included because key is not stored in the R.String
    }

    public static int theme(int resId, int lob) {
        int res = resId;
        if (ClnEnv.isMyMobFlag()) {
            switch (resId) {
                case R.drawable.icon_mms:
                    if (is1010(lob)) {
                        res = R.drawable.icon_mms_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_mms_csp;
                    } else {
                        res = R.drawable.icon_mms_csl;
                    }
                    break;
                case R.drawable.icon_sms:
                    if (is1010(lob)) {
                        res = R.drawable.icon_sms_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_sms_csp;
                    } else {
                        res = R.drawable.icon_sms_csl;
                    }
                    break;
                case R.drawable.icon_voicecall:
                    if (is1010(lob)) {
                        res = R.drawable.icon_voicecall_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_voicecall_csp;
                    } else {
                        res = R.drawable.icon_voicecall_csl;
                    }
                    break;
                case R.drawable.icon_videocall:
                    if (is1010(lob)) {
                        res = R.drawable.icon_videocall_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_videocall_csp;
                    } else {
                        res = R.drawable.icon_videocall_csl;
                    }
                    break;
                case R.drawable.billinfo_icon:
                    if (is1010(lob)) {
                        res = R.drawable.billinfo_icon_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_billinfo_csp;
                    } else {
                        res = R.drawable.billinfo_icon_csl;
                    }
                    break;
                case R.drawable.icon_topup:
                    if (is1010(lob)) {
                        res = R.drawable.icon_topup_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_topup_csp;
                    } else {
                        res = R.drawable.icon_topup_csl;
                    }
                    break;
                case R.drawable.icon_mobusage:
                    if (is1010(lob)) {
                        res = R.drawable.icon_mobusage_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_mobusage_csp;
                    } else {
                        res = R.drawable.icon_mobusage_csl;
                    }
                    break;
                case R.drawable.icon_bbmail:
                    if (is1010(lob)) {
                        res = R.drawable.icon_bbmail_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_bbmail_csp;
                    } else {
                        res = R.drawable.icon_bbmail_csl;
                    }
                    break;
                case R.drawable.icon_others:
                    if (is1010(lob)) {
                        res = R.drawable.icon_others_1010;
                    } else if (lob == R.string.CONST_LOB_CSP) {
                        res = R.drawable.icon_others_csp;
                    } else {
                        res = R.drawable.icon_others_csl;
                    }
                    break;
            }
        }
        return res;
    }

    public static void showLog(String tag, String msg) {
        int length = msg.toCharArray().length;
        int row = (int) Math.floor((double) length / 2000);
        if (msg.toCharArray().length < 2000) {
            Log.i(tag, msg);
        } else {
            for (int i = 0; i < row; i++) {
                if (i != row - 1) {
                    Log.i(tag, msg.substring(i * 2000, (i + 1) * 2000));
                } else {
                    Log.i(tag, msg.substring((i) * 2000, length));
                }
            }
        }
    }

    // Compare T-n Days
    public static boolean CompareDateAdd3(Context cxt, String apptcsdt, String gettodaydt, String srcDateFormat) {
        SimpleDateFormat inputdf = new SimpleDateFormat(getString(cxt, R.string.input_datetime_format), Locale.US);
        SimpleDateFormat sdf = new SimpleDateFormat(srcDateFormat);

        Date apptcsdtDate, gettodaydtDate;
        try {
            //converted apptcsdt to correct date format
            Date d = sdf.parse(apptcsdt);
            sdf.applyPattern(getString(cxt, R.string.input_datetime_format));
            apptcsdt = sdf.format(d);

            apptcsdtDate = inputdf.parse(apptcsdt);
            gettodaydtDate = inputdf.parse(gettodaydt);

            // Reset Time = 00:00 before compare
            Calendar cal = Calendar.getInstance();
            cal.setTime(apptcsdtDate);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            apptcsdtDate = cal.getTime();

            cal.setTime(gettodaydtDate);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            gettodaydtDate = cal.getTime();

            cal.setTime(gettodaydtDate);
            cal.add(Calendar.DATE, ClnEnv.getPref(cxt.getApplicationContext(), getString(cxt, R.string.CONST_PREF_APPTIND_DAYS), 3));
            gettodaydtDate = cal.getTime();

            if (apptcsdtDate.compareTo(gettodaydtDate) <= 0) return true;
        } catch (Exception e) {
            // fail to convert day
            return false;
        }
        return false;
    }

    public static boolean isCurrentDay(Context cxt, String serverTs, String apptlastTs, String srcDateFormat) {
        SimpleDateFormat inputdf = new SimpleDateFormat(getString(cxt, R.string.input_datetime_format), Locale.US);
        SimpleDateFormat sdf = new SimpleDateFormat(srcDateFormat);

        Date apptLastDate, serverDate;
        try {
            //converted apptcsdt to correct date format
            serverDate = serverTs.isEmpty() ? Calendar.getInstance().getTime() : inputdf.parse(serverTs);
            apptLastDate = inputdf.parse(apptlastTs);

            // Reset Time = 00:00 before compare
            Calendar cal = Calendar.getInstance();
            cal.setTime(serverDate);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            serverDate = cal.getTime();

            cal.setTime(apptLastDate);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            apptLastDate = cal.getTime();
            if (apptLastDate.compareTo(serverDate) < 0) return true;
        } catch (Exception e) {
            // fail to convert day
            return false;
        }
        return false;
    }

    //Search for matching SubnRec (for Alias editing in service list)
    public static boolean matchSubnRec(SubnRec subnRec, SubnRec rSubnRec) {
        return subnRec.acctNum.equalsIgnoreCase(rSubnRec.acctNum) &&
                subnRec.assoc.equalsIgnoreCase(rSubnRec.assoc) &&
                subnRec.bsn.equalsIgnoreCase(rSubnRec.bsn) &&
                subnRec.createPsn.equalsIgnoreCase(rSubnRec.createPsn) &&
                subnRec.createTs.equalsIgnoreCase(rSubnRec.createTs) &&
                subnRec.cusNum.equalsIgnoreCase(rSubnRec.cusNum) &&
                subnRec.custRid == rSubnRec.custRid &&
                subnRec.datCd.equalsIgnoreCase(rSubnRec.datCd) &&
                subnRec.domainTy.equalsIgnoreCase(rSubnRec.domainTy) &&
                subnRec.eyeGrp.equalsIgnoreCase(rSubnRec.eyeGrp) &&
                subnRec.fsa.equalsIgnoreCase(rSubnRec.fsa) &&
                subnRec.ind2l2b.equalsIgnoreCase(rSubnRec.ind2l2b) &&
                subnRec.indEye.equalsIgnoreCase(rSubnRec.indEye) &&
                subnRec.indPcd.equalsIgnoreCase(rSubnRec.indPcd) &&
                subnRec.indTv.equalsIgnoreCase(rSubnRec.indTv) &&
                subnRec.instAdr.equalsIgnoreCase(rSubnRec.instAdr) &&
                subnRec.lastupdPsn.equalsIgnoreCase(rSubnRec.lastupdPsn) &&
                subnRec.lastupdTs.equalsIgnoreCase(rSubnRec.lastupdTs) &&
                subnRec.lob.equalsIgnoreCase(rSubnRec.lob) &&
                subnRec.netLoginId.equalsIgnoreCase(rSubnRec.netLoginId) &&
                subnRec.priMob.equalsIgnoreCase(rSubnRec.priMob) &&
                subnRec.refFsa2l2b.equalsIgnoreCase(rSubnRec.refFsa2l2b) &&
                subnRec.rev == rSubnRec.rev &&
                subnRec.rid == rSubnRec.rid &&
                subnRec.sipSubr.equalsIgnoreCase(rSubnRec.sipSubr) &&
                subnRec.srvId.equalsIgnoreCase(rSubnRec.srvId) &&
                subnRec.srvNum.equalsIgnoreCase(rSubnRec.srvNum) &&
                subnRec.storeTy.equalsIgnoreCase(rSubnRec.storeTy) &&
                subnRec.systy.equalsIgnoreCase(rSubnRec.systy) &&
                subnRec.tariff.equalsIgnoreCase(rSubnRec.tariff) &&
                subnRec.tos.equalsIgnoreCase(rSubnRec.tos) &&
                subnRec.wipCust.equalsIgnoreCase(rSubnRec.wipCust);
    }

    public static boolean isCsimOnly() {
        int csimCount = 0;
        if (ClnEnv.isLoggedIn()) {
            if (ClnEnv.getLgiCra() != null) {
                if (ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry().length == 0) {
                    return false;
                } else {
                    for (int i = 0; i < ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry().length; i++) {
                        if (!(SubnRec.LOB_101.equals(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[i].lob) || SubnRec.LOB_O2F.equals(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[i].lob))) {
                            return false;
                        } else {
                            csimCount++;
                        }
                    }
                    return csimCount != 0;
                }
            }
        }
        return false;
    }

    //lob icon
    public static int getLobIcon(int lob, int ltsType) {
        //There will be no acctAgent if it come from line test
        //		int lobType = acctAgent.getLobType();
        if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI) {
            return R.drawable.logo_1010;
        } else if (lob == R.string.CONST_LOB_MOB || lob == R.string.CONST_LOB_O2F) {
            return R.drawable.logo_csl;
        } else if (lob == R.string.CONST_LOB_LTS) {
            return R.drawable.lob_lts_plain;
        } else if (lob == R.string.CONST_LOB_PCD) {
            return R.drawable.logo_netvigator;
        } else if (lob == R.string.CONST_LOB_TV) {
            return R.drawable.logo_now;
        } else if (lob == R.string.CONST_LOB_CSP) {
            return R.drawable.lob_csp_plain;
        } else if (lob == R.string.CONST_LOB_VOBB) {
            return R.drawable.ic_launcher;
        }
        return 0;
    }

    public static String getLtsTtypeRightText(Context ctx, int ltsType) {
        String title;
        switch (ltsType) {
            case R.string.CONST_LTS_FIXEDLINE:
                title = ctx.getResources().getString(R.string.myhkt_lts_fixedline);
                break;
            case R.string.CONST_LTS_EYE:
                title = ctx.getResources().getString(R.string.myhkt_lts_eye);
                break;
            case R.string.CONST_LTS_IDD0060:
                title = ctx.getResources().getString(R.string.myhkt_lts_idd0060);
                break;
            case R.string.CONST_LTS_CALLINGCARD:
                title = ctx.getResources().getString(R.string.myhkt_lts_callingcard);
                break;
            case R.string.CONST_LTS_ONECALL:
                title = ctx.getResources().getString(R.string.myhkt_lts_onecall);
                break;
            case R.string.CONST_LTS_ICFS:
                title = ctx.getResources().getString(R.string.myhkt_lts_icfs);
                break;
            default:
                title = "";
                break;
        }
        return title;
    }

    public static String mbToGb(double mb, boolean isRoundDown) {
        //changing double mb value to gb if necessary
        NumberFormat decFormat = new DecimalFormat("#.0");
        if (mb >= 1024) { //GB
            mb = mb / 1024;
            return isRoundDown ? decFormat.format(Math.floor(mb * 100) / 100) + "GB" : decFormat.format((mb * 100) / 100) + "GB";
        } else { //MB
            return isRoundDown ? decFormat.format(Math.floor(mb)) + "MB" : Math.floor(mb) + "MB";
        }
    }

    /**
     * 06-14-17: Moiz
     *
     * @param remainingMB
     * @param remainingKB
     * @return DataUsage depending on size, if in remainingKV <= 1024 return the KB otherwise return MB
     */
    public static String getRemainingDataUsage(String remainingMB, String remainingKB) {
        String dataUsage = "---";
        int dataNumberFormat = 0;
        if (remainingKB != null && !TextUtils.isEmpty(remainingKB)) {
            dataNumberFormat = Integer.parseInt(remainingKB.replace(",", ""));
            if (dataNumberFormat <= 1024) {
                dataUsage = remainingKB + "KB";
            } else {
                // return the MB if not empty
                dataUsage = getRemaingDataUsageMB(remainingMB);
            }
        } else {
            // return the MB if not empty
            dataUsage = getRemaingDataUsageMB(remainingMB);
        }
        return dataUsage;
    }

    private static String getRemaingDataUsageMB(String remainingMB) {
        String dataUsage = "---";

        if (remainingMB != null && !TextUtils.isEmpty(remainingMB)) {
            dataUsage = remainingMB + "MB";
        }
        return dataUsage;
    }

    /**
     * 7.9.19 - Yong
     * Added formatter copied from iOS intentionally for Global asia plan
     * Soon will use it in the other plans
     *
     * @param input
     * @param dataType
     * @param withUnit
     * @return
     */
    public static String formattedDataByKBStr(String input, HKTDataType dataType, Boolean withUnit) {
        double inputKBf = input != null && !input.isEmpty() ? Double.valueOf(input.replaceFirst(" ", "").replace(",", "")) : 0;
        String unit;
        double outputf;
        final double _gb = 1048576.0;
        final double _mb = 1024.0;
        int dp = 0;
        if (inputKBf >= _gb || !withUnit) {
            outputf = inputKBf / _gb;
            unit = "GB";
            dp = 1;
        } else if (inputKBf >= _mb) {
            outputf = inputKBf / _mb;
            unit = "MB";
            dp = 1;
        } else {
            outputf = inputKBf;
            unit = "KB";
            dp = 1;
        }

        outputf *= Math.pow(10, dp);

        switch (dataType) {
            case HKTDataTypeEntitled:
                outputf = Math.round(outputf);
                break;
            case HKTDataTypeRemaining:
                outputf = Math.ceil(outputf);
                break;
            case HKTDataTypeUsed:
                outputf = Math.floor(outputf);
                break;
        }

        outputf /= Math.pow(10, dp);

        NumberFormat numberFormat;
        numberFormat = new DecimalFormat("##.0");

        numberFormat.setGroupingUsed(true);
        numberFormat.setMaximumFractionDigits(dp);
        numberFormat.setMinimumFractionDigits(dp);
        numberFormat.setMinimumIntegerDigits(1);

        String formatted = numberFormat.format(outputf);
        String result;
        if (withUnit) {
            result = formatted + unit;
        } else {
            result = formatted;
        }
        return result;
    }

    /**
     * 6.18.18 - Moiz
     * Added formatter copied from iOS intentionally for Global asia plan
     * Soon will use it in the other plans
     * 7.24.18 Moiz - Added null and empty check for inputs to prevent nullpointerexpceptions and remove space
     *
     * @param input
     * @param dataType
     * @param withUnit
     * @return
     */
    public static String formattedDataByMBStr(String input, HKTDataType dataType, Boolean withUnit) {
        double inputMBf = input != null && !input.equals("") ? Double.valueOf(input.replaceFirst(" ", "").replace(",", "")) : 0;
        String unit = null;
        double outputf = 0;
        int dp = 0;
        if (inputMBf >= 1024 || !withUnit) {
            outputf = inputMBf / 1024.0;
            unit = "GB";
            dp = 1;
        } else if (inputMBf < 1.024) {
            outputf = inputMBf * 1024.0;
            unit = "KB";
            dp = 1;
        } else {
            outputf = inputMBf;
            unit = "MB";
            dp = 1;
        }
        outputf *= Math.pow(10, dp);

        switch (dataType) {
            case HKTDataTypeEntitled:
            case HKTDataTypeRemaining:
                outputf = Math.round(outputf);
                break;
            case HKTDataTypeUsed:
                outputf = Math.floor(outputf);
                break;
        }

        outputf /= Math.pow(10, dp);

        NumberFormat numberFormat;
        numberFormat = new DecimalFormat("##.0");

        numberFormat.setGroupingUsed(true);
        numberFormat.setMaximumFractionDigits(dp);
        numberFormat.setMinimumFractionDigits(dp);
        numberFormat.setMinimumIntegerDigits(1);

        String formatted = numberFormat.format(outputf);
        String result;
        if (withUnit) {
            result = formatted + unit;
        } else {
            result = formatted;
        }
        return result;
    }

    /**
     * 06-13-17: Moiz
     *
     * @param context Description: Change the locale to Chinese,
     *                This method will be called once the webview set the locale to default English on the first launch
     *                This fix apply only apply in the API 24 and above (Nougat)
     */
    public static void switchToZhLocale(Context context) {
        Locale locale = new Locale("zh", "HK");
        Locale.setDefault(locale);
        Configuration config = new Configuration();

        if (Build.VERSION.SDK_INT >= 24) {
            config.setLocale(locale);
        } else {
            config.locale = locale;
        }
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        Log.d(TAG, context.getResources().getString(R.string.myhkt_lang));
    }

    /**
     * Oct 30, 2017 : Moiz
     * Description: Check if finger print available in the device
     * Failed message "Your Device does not have a Fingerprint Sensor"
     */
    @TargetApi(23)
    public static boolean isTouchIDAvailableOnSystem(Activity activity) {
        FingerprintManager fingerprintManager = (FingerprintManager) activity.getSystemService(Context.FINGERPRINT_SERVICE);
        if (activity.checkSelfPermission(Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            // Now the protection level of USE_FINGERPRINT permission is normal instead of dangerous.
            // See http://developer.android.com/reference/android/Manifest.permission.html#USE_FINGERPRINT
        }
        return fingerprintManager != null && fingerprintManager.isHardwareDetected();
    }

    /**
     * Oct 30, 2017 : Moiz
     * Description: Check whether at least one fingerprint is registered
     * Failed message: "Register at least one fingerprint in Settings"
     */
    @TargetApi(23)
    public static boolean isTouchIDEnrolledByUser(Activity activity) {
        FingerprintManager fingerprintManager = (FingerprintManager) activity.getSystemService(Context.FINGERPRINT_SERVICE);
        if (activity.checkSelfPermission(Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            // Now the protection level of USE_FINGERPRINT permission is normal instead of dangerous.
            // See http://developer.android.com/reference/android/Manifest.permission.html#USE_FINGERPRINT
        }
        return fingerprintManager != null && fingerprintManager.hasEnrolledFingerprints();
    }

    /**
     * Nov 2, 2017
     * Description: Checks whether lock screen security is enabled or not
     * Failed Message: Secure lock screen hasn't set up. "Go to 'Settings -> Screen Security
     */
    public static boolean isKeyguardSecured(Activity activity) {
        KeyguardManager keyguardManager = (KeyguardManager) activity.getSystemService(Context.KEYGUARD_SERVICE);
        return keyguardManager != null && keyguardManager.isKeyguardSecure();
    }

    /**
     * Nov 2, 2017 : Moiz
     * Description: Check if the Touch ID is successfully activated by the user
     */
    public static boolean isTouchIDLoginActivated(Context context) {
        return ClnEnv.getPref(context, Constant.TOUCH_ID_LOGIN_ACTIVATED, false);
    }

    /**
     * Nov 6, 2017 : Moiz
     * Description: Set Touch ID Activated successfully
     */
    public static void setTouchIDLoginActivated(Context context, boolean bool) {
        ClnEnv.setPref(context, Constant.TOUCH_ID_LOGIN_ACTIVATED, bool);
    }

    /**
     * Nov 6, 2017 : Moiz
     * Description: Check of the Touch Login enabled in the setting page
     */
    public static boolean isTouchIdLoginEnabled(Context context) {
        return ClnEnv.getPref(context, Constant.TOUCH_ID_SETTING_ENABLED, false);
    }

    /**
     * Nov 6, 2017 : Moiz
     * Description: Set the Touch login enabled, the value bool will be set in the Setting Touch Id checkbox
     */
    public static void setTouchIdLoginEnabled(Context context, boolean bool) {
        ClnEnv.setPref(context, Constant.TOUCH_ID_SETTING_ENABLED, bool);
    }

    /**
     * Nov 6, 3017 : Moiz
     * Description: Check it touch ID should be visible, this will be call in showing the touch id setting in setting page
     */
    public static boolean isToShowTouchID(Activity activity) {
        return Build.VERSION.SDK_INT >= 23 && ClnEnv.isLoggedIn() && isTouchIDAvailableOnSystem(activity);
    }

    /**
     * Nov 8, 2017 : Moiz
     * Description: This will check if the Touch Id banner should be shown to the user
     */
    public static boolean isToShowTouchIdBanner(Activity activity) {
        return Build.VERSION.SDK_INT >= 23 && ClnEnv.isLoggedIn() && isTouchIDAvailableOnSystem(activity)
                && ClnEnv.getPref(activity, Constant.IS_SERVER_TO_SHOW_FP_BANNER, "N").equalsIgnoreCase("Y");
    }

    /**
     * Nov 8, 2017 : Moiz
     * Description: Save the temp user's credentials after successful Touch ID login activated
     */
    public static void saveTempUserCredentialsAsTouchIdDefault(Context context) {
        String tempUserId = ClnEnv.getPref(context, Constant.TOUCH_ID_TEMP_USER_ID, "");
        String tempPwd = ClnEnv.getEncPref(context, ClnEnv.getPref(context, Constant.TOUCH_ID_TEMP_USER_ID, ""),
                Constant.TOUCH_ID_TEMP_PWD, "");
        ClnEnv.setPref(context, Constant.TOUCH_ID_USER_ID_DEFAULT, tempUserId);
        ClnEnv.setEncPref(context, tempUserId, Constant.TOUCH_ID_PWD_DEFAULT, tempPwd);
    }

    /**
     * Nov 8, 2017 : Moiz
     * Description: Save the user's credentials after successful login if the device has finger print capability
     */
    public static void saveTempUserCredentials(Context context, String userName, String password) {
        ClnEnv.setPref(context, Constant.TOUCH_ID_TEMP_USER_ID, userName);
        ClnEnv.setEncPref(context, userName, Constant.TOUCH_ID_TEMP_PWD, password);
    }

    /**
     * Nov 20, 2017 : Moiz
     * Description: Save the user's default touch id credentials as temporary. This will be call logging-in using Touch Id
     */
    public static void saveDefaultTouchIDUserToTempUser(Context context) {
        String userName = ClnEnv.getPref(context, Constant.TOUCH_ID_USER_ID_DEFAULT, "");
        String pwd = ClnEnv.getEncPref(context, ClnEnv.getPref(context, Constant.TOUCH_ID_USER_ID_DEFAULT, ""), Constant.TOUCH_ID_PWD_DEFAULT, "");
        ClnEnv.setPref(context, Constant.TOUCH_ID_TEMP_USER_ID, userName);
        ClnEnv.setEncPref(context, userName, Constant.TOUCH_ID_TEMP_PWD, pwd);
    }

    /**
     * Nov 8, 2017 : Moiz
     * Description: Clear Touch ID credentials of the user who activated the touch ID
     */
    public static void clearTouchIdUserDetails(Context context) {
        ClnEnv.setEncPref(context, ClnEnv.getPref(context, Constant.TOUCH_ID_USER_ID_DEFAULT, ""),
                Constant.TOUCH_ID_PWD_DEFAULT, "");
        ClnEnv.setPref(context, Constant.TOUCH_ID_USER_ID_DEFAULT, "");
    }

    /**
     * Nov 8, 2017 : Moiz
     * Description: Return the saved temp password
     */
    public static String getSavedPassword(Context context) {
        return ClnEnv.getEncPref(context, ClnEnv.getPref(context, Constant.TOUCH_ID_TEMP_USER_ID, ""),
                Constant.TOUCH_ID_TEMP_PWD, "");
    }

    /**
     * Nov 8, 2017 : Moiz
     * Description: Check if the logged in user same with user who activated the Touch Id
     * Logged in user saved as temp, while Touch ID user enrolled saved as touch id user default
     */
    public static boolean isTouchIdUserEqualsToLoggedInUser(Context context) {
        return ClnEnv.getPref(context, Constant.TOUCH_ID_USER_ID_DEFAULT, "").equalsIgnoreCase(
                ClnEnv.getPref(context, Constant.TOUCH_ID_TEMP_USER_ID, ""));
    }

    /**
     * Feb 26, 2018 : Moiz
     * Description: Set part of the text clickable.
     * Source: https://stackoverflow.com/questions/10696986/how-to-set-the-part-of-the-text-view-is-clickable
     */
    public static void setTextViewClickableSpan(TextView textView, String[] links, ClickableSpan[] clickableSpans) {
        SpannableString spannableString = new SpannableString(textView.getText());
        for (int i = 0; i < links.length; i++) {
            ClickableSpan clickableSpan = clickableSpans[i];
            String link = links[i];

            int startIndexOfLink = textView.getText().toString().indexOf(link);
            spannableString.setSpan(clickableSpan, startIndexOfLink, startIndexOfLink + link.length(),
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(spannableString, TextView.BufferType.SPANNABLE);
    }

    /**
     * April 18, 2018 : Moiz
     * Description: Check if the country is Taiwan_.
     * Used in: APIsManager: doGetIDDCodeResult
     */
    public static String isCountryTaiwan(String country) {
        if (country.length() >= 7 && country.contains("Taiwan_")) {
            return "Taiwan";
        }
        return country;
    }

    /**
     * July 18, 2018 : Moiz
     * Description: Return day/s remaining, expiryDate less currentDate
     * Used in: Inbox list screen
     */
    public static int daysRemaining(Date expiryDate) {
        int daysRemaining = 0;
        // Jarlou 10/16/2018 date remaining date computation
        Date cDate = new Date();
        long diff = expiryDate.getTime() - cDate.getTime();
        double daysDiff = (double) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        int intDiff = (int) Math.floor(daysDiff);
        //if <= 1 return the value otherwise 1
        daysRemaining = intDiff <= 1 ? 1 : intDiff;
        return daysRemaining;
    }

    /**
     * July 18, 2018 : Moiz
     * Description: Return current date with format dd/MM/yyyy
     * Used in: Inbox list screen
     */
    public static String getCurrentDate() {
        try {
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            Date netDate = (new Date(Calendar.getInstance().getTimeInMillis()));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * Get InbxCra from SharePreference, if empty String return new InbxCra Object
     */
    public static InbxCra getPrefInboxCra(Context cxt) {
        String strInbxCra = ClnEnv.getPref(cxt, Constant.CONST_INBOX_CRA_RESPONSE, "");
        return !"".equalsIgnoreCase(strInbxCra) ? (InbxCra) Utils.deserialize(strInbxCra) : new InbxCra();
    }

    /**
     * Check if service account is for Technical
     *
     * @param acctAgent
     * @return
     */
    public static boolean isTechnicalAccount(AcctAgent acctAgent) {
        if (acctAgent.getLob().equalsIgnoreCase(SubnRec.LOB_PCD) || acctAgent.getLob().equalsIgnoreCase(SubnRec.LOB_TV) || isFixedLineLTS(acctAgent)) {
            return ("N".equals(acctAgent.getSubnRec().aloneDialup) || !SubnRec.LOB_PCD.equals(acctAgent.getSubnRec().lob)) && !SubnRec.TOS_IMS_GGDY.equals(acctAgent.getSubnRec().tos);
        }
        return false;
    }

    public static boolean isFixedLineLTS(AcctAgent acctAgent) {
        if (acctAgent.getLob().equalsIgnoreCase(SubnRec.LOB_LTS)) {
            int ltsType = Utils.getLtsSrvType(acctAgent.getSubnRec().tos, acctAgent.getSubnRec().eyeGrp, acctAgent.getSubnRec().priMob);
            return ltsType != R.string.CONST_LTS_ONECALL && ltsType != R.string.CONST_LTS_ICFS && ltsType != R.string.CONST_LTS_CALLINGCARD && ltsType != R.string.CONST_LTS_IDD0060;
        }
        return false;
    }

    public static boolean isUAT(Context context) {
        try {
            return APIsManager.UAT_domain.equals(ClnEnv.getPref(context, context.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain));
        } catch (Exception e) {
            return false;
        }
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static boolean hasUnlimitedPlan(G3DisplayServiceItemDTO dto) {
        return isFUP(dto) || isQOS(dto);
    }

    public static boolean isFUP(G3DisplayServiceItemDTO dto) {
//        return USAGE_CONDITION_FUP.equalsIgnoreCase(dto.getUsageCondition());
        return false;
    }

    public static boolean isQOS(G3DisplayServiceItemDTO dto) {
//        return USAGE_CONDITION_QOS.equalsIgnoreCase(dto.getUsageCondition());
        return false;
    }

    public static boolean is5G(G3DisplayServiceItemDTO dto) {
        return dto.getDisplayOrder() == 5;
    }

    public static boolean hasReachUnlimitedPlan(G3DisplayServiceItemDTO item) {
        return item.getUsageDescription() == null
                || (item.getUsageDescription() != null
                && (item.getUsageDescription().getRemainingInKB() == null
                || (item.getUsageDescription().getRemainingInKB() != null
                && item.getUsageDescription().getRemainingInKB().equals(""))
                || (item.getUsageDescription().getRemainingInKB() != null
                && item.getUsageDescription().getRemainingInKB().equals("0"))));
    }

    public static void setNavigationBarTitle_service(Context ctx, AAQuery aq, int lob, int ltsType, boolean isZombie, String acctNum, String srvNum) {
        String navBarTitle;
        String rightText = "";

        aq.id(R.id.navbar_service_title).getTextView().setMaxLines(isZombie ? 2 : 1);
        aq.id(R.id.navbar_service_title).getTextView().setTypeface(null, Typeface.BOLD);

        if (lob == R.string.CONST_LOB_LTS && !isZombie) {
            rightText = Utils.getLtsTtypeRightText(ctx, ltsType);
        }

        if (isZombie) {
            navBarTitle = "*" + acctNum + "\n" + ctx.getString(R.string.LABEL_EXPIRED);
        } else {
            navBarTitle = srvNum;
            if (lob == R.string.CONST_LOB_LTS) {
                if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
                    int last4index = (srvNum.length() - 4) < 0 ? 0 : srvNum.length() - 4;
                    navBarTitle = String.format("CARD%s", srvNum.substring(last4index)) + "\n";
                }
            }
        }

        if (ClnEnv.getSessionPremierFlag()) {
            aq.id(R.id.navbar_righttext).text(rightText).textColorId(R.color.white);
        } else {
            aq.id(R.id.navbar_righttext).text(rightText).textColorId(R.color.hkt_txtcolor_grey);
        }

        aq.navBarTitle(R.id.navbar_service_title, R.id.navbar_leftdraw, 0, Utils.getLobIcon(lob, ltsType), 0, navBarTitle);
        if (lob == R.string.CONST_LOB_PCD) {
            aq.id(R.id.navbar_leftdraw).width(120);
            aq.id(R.id.navbar_leftdraw).height(60);
        } else if (lob == R.string.CONST_LOB_TV) {
            aq.id(R.id.navbar_leftdraw).width(80);
            aq.id(R.id.navbar_leftdraw).height(40);
        }
        aq.navBarTitleServiceNumberColor(R.id.navbar_service_title, lob);
    }

    public static float getDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static void setNavigationBarTitle(Context ctx, AAQuery aq, int lob, int ltsType, boolean isZombie, String acctNum, String srvNum) {
        String navBarTitle;
        String rightText = "";

        aq.id(R.id.navbar_title).getTextView().setMaxLines(isZombie ? 2 : 1);

        if (lob == R.string.CONST_LOB_LTS && !isZombie) {
            rightText = Utils.getLtsTtypeRightText(ctx, ltsType);
        }

        if (isZombie) {
            navBarTitle = "*" + acctNum + "\n" + ctx.getString(R.string.LABEL_EXPIRED);
        } else {
            navBarTitle = srvNum;
            if (lob == R.string.CONST_LOB_LTS) {
                if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
                    int last4index = (srvNum.length() - 4) < 0 ? 0 : srvNum.length() - 4;
                    navBarTitle = String.format("CARD%s", srvNum.substring(last4index)) + "\n";
                }
            }
        }
        if (ClnEnv.getSessionPremierFlag()) {
            aq.id(R.id.navbar_righttext).text(rightText).textColorId(R.color.white);
        } else {
            aq.id(R.id.navbar_righttext).text(rightText).textColorId(R.color.hkt_txtcolor_grey);
        }
        aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, 0, Utils.getLobIcon(lob, ltsType), 0, navBarTitle);
    }

    public static void filterApptToShowClock(Context context, ApptCra apptCra) {
        if (apptCra.getOGnrlApptAry() != null) {
            ClnEnv.setPref(context, context.getString(R.string.CONST_PREF_APPTIND_FLAG).concat("-").concat(String.valueOf(ClnEnv.getLoginId())), false);
            // Determine whether show the alert Icon in MainMenu
            for (int i = 0; i < apptCra.getOGnrlApptAry().length; i++) {
                GnrlAppt gnrlAppt = apptCra.getOGnrlApptAry()[i];
                if (Utils.CompareDateAdd3(context, gnrlAppt.getApptStDT(), apptCra.getServerTS(), context.getString(R.string.input_datetime_format))) {
                    ClnEnv.setPref(context, context.getString(R.string.CONST_PREF_APPTIND_FLAG).concat("-").concat(String.valueOf(ClnEnv.getLoginId())), true);
                    return;
                }
            }
        }
        if (apptCra.getOSrvReqAry() != null) {
            ClnEnv.setPref(context, context.getString(R.string.CONST_PREF_APPTIND_FLAG).concat("-").concat(String.valueOf(ClnEnv.getLoginId())), false);
            // Determine whether show the alert Icon in MainMenu
            for (int i = 0; i < apptCra.getOSrvReqAry().length; i++) {
                SrvReq srvReq = apptCra.getOSrvReqAry()[i];
                if (Utils.CompareDateAdd3(context, srvReq.getApptTS().getApptDate(), apptCra.getServerTS(), context.getString(R.string.input_date_format))) {
                    ClnEnv.setPref(context, context.getString(R.string.CONST_PREF_APPTIND_FLAG).concat("-").concat(String.valueOf(ClnEnv.getLoginId())), true);
                    break;
                }
            }
        }
    }

    //Check if bill message exist during Main Menu launch
    public static void checkBillMsgAndRedirect(Context context, boolean isFromServiceActivity) {
        //demo to test the notification steps flow
        if (ClnEnv.getPref(context, context.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false)) {
            Log.i(TAG, "check bill1");
            try {
                Log.i("TAG", "check bill2");
                if (ClnEnv.getPushDataBill() != null) {
                    Log.i("TAG", "check bill3");
                    if (ClnEnv.isLoggedIn() && ClnEnv.getPushDataBill().isAppActive()) {
                        Log.i(TAG, "check bill5");
                        // session timeout from Background
                        if (ClnEnv.getPref(context, Constant.CONST_SHOW_SESS_TIMEOUT, false)) {
                            Log.i("TAG", "check bill6");
                            Intent dialogIntent = new Intent(context, GlobalDialog.class);
                            dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_SESSION_TIMEOUT);

                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            context.startActivity(dialogIntent);
                            ClnEnv.setPref(context, Constant.CONST_SHOW_SESS_TIMEOUT, false);
                        } else {
                            Log.i("TAG", "check bill7");
                            Intent dialogIntent = new Intent(context, GlobalDialog.class);
                            dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_MSG_NEW_BILL);
                            dialogIntent.putExtra("ISFROMSERVICEACTIVITY", isFromServiceActivity);

                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            context.startActivity(dialogIntent);
                        }
                    }
                }
            } catch (Exception e) {
                //Prevent throw null pointer exception for BillPushData
                ClnEnv.setPref(context.getApplicationContext(), context.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
            }
        }
    }

    public static int setCharLimit(int lob) {
        int charLimit = 50;
        if (lob == R.string.CONST_LOB_MOB || lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI) {
            charLimit = 100;
        } else if (lob == R.string.CONST_LOB_CSP) {
            charLimit = 100;
        }
        return charLimit;
    }

    public static String getScreenSize(Context context) {
        int screenSize = context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        return switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_LARGE -> Constant.SCREEN_LARGE;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL -> Constant.SCREEN_NORMAL;
            case Configuration.SCREENLAYOUT_SIZE_SMALL -> Constant.SCREEN_SMALL;
            default -> Constant.SCREEN_UNDEFINED;
        };
    }

    public static String getRealPathImageFromUri(FragmentActivity activity, Uri uri) {
        String fileName = null;
        if ("content".equals(uri.getScheme())) {
            try (Cursor cursor = Objects.requireNonNull(activity).getContentResolver().query(uri, null, null, null, null)) {
                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return fileName;
    }

    public static String getLobString(SubnRec subnRec) {
        if (SubnRec.LOB_LTS.equalsIgnoreCase(subnRec.lob)) {
            return "LTS";
        } else if (SubnRec.LOB_MOB.equalsIgnoreCase(subnRec.lob)) {
            return "MOB";
        } else if (SubnRec.WLOB_XMOB.equalsIgnoreCase(subnRec.lob)) {
            return "XMOB";
        } else if (SubnRec.LOB_IOI.equalsIgnoreCase(subnRec.lob)) {
            return "IOI";
        } else if (SubnRec.LOB_PCD.equalsIgnoreCase(subnRec.lob)) {
            return "PCD";
        } else if (SubnRec.LOB_TV.equalsIgnoreCase(subnRec.lob)) {
            return "TV";
        } else if (SubnRec.LOB_101.equalsIgnoreCase(subnRec.lob)) {
            return "101";
        } else if (SubnRec.LOB_O2F.equalsIgnoreCase(subnRec.lob)) {
            return "O2F";
        } else if (SubnRec.LOB_CSP.equalsIgnoreCase(subnRec.lob)) {
            return "CSP";
        } else if (SubnRec.WLOB_XCSP.equalsIgnoreCase(subnRec.lob)) {
            return "XCSP";
        }
        return "";
    }

    public static void setCrashlytics(AcctAgent acctAgent) {
        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
        if (acctAgent != null) {
            crashlytics.setCustomKey("user_srvNum", acctAgent.getSubnRec().srvNum);
            crashlytics.setCustomKey("user_acctNum", acctAgent.getSubnRec().acctNum);
            crashlytics.setCustomKey("user_email", acctAgent.getSubnRec().lastupdPsn);
            crashlytics.setCustomKey("user_lob", acctAgent.getSubnRec().lob);
        }
    }

    public static void openUrlOnCustomTab(Context context, String url, int toolbarColor) {
        CustomTabColorSchemeParams toolbarColorParam = new CustomTabColorSchemeParams.Builder()
                .setToolbarColor(toolbarColor).build();
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setShowTitle(true).setDefaultColorSchemeParams(toolbarColorParam);
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.intent.setPackage("com.android.chrome");
        customTabsIntent.intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        customTabsIntent.launchUrl(context, Uri.parse(url));
    }

    public static String getTimeStamp() {
        String timeStamp = String.valueOf(new Date().getTime());
        return timeStamp.substring(0, 10);
    }
}