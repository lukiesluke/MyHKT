package com.pccw.myhkt.cell.model;

import android.text.style.ClickableSpan;
import android.view.Gravity;

import com.pccw.myhkt.R;

public class SmallSpannedTextCell extends Cell{

	private int titleGravity = Gravity.LEFT|Gravity.CENTER;

	String[] links = null;
	ClickableSpan[] clickableSpans = null;



	public SmallSpannedTextCell(String title, String content) {
		super();
		type = Cell.SMALLTEXT2;
		this.title = title;
		this.content = content;
		titleColorId = R.color.hkt_txtcolor_grey;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;
		isArrowShown = false;
	}

	public SmallSpannedTextCell(String title, String content, int titleColor) {
		super();
		type = Cell.SMALLTEXT2;
		this.title = title;
		this.content = content;
		titleColorId = titleColor;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;	
		isArrowShown = false;
	}

	public Boolean getIsArrowShown() {
		return isArrowShown;
	}

	public void setIsArrowShown(Boolean isArrowShown) {
		this.isArrowShown = isArrowShown;
	}
	
	public int getTitleGravity() {
		return titleGravity;
	}

	public void setTitleGravity(int titleGravity) {
		this.titleGravity = titleGravity;
	}

	public void setClickableSpans(String[] links, ClickableSpan[] clickableSpans) {
		this.links = links;
		this.clickableSpans = clickableSpans;
	}

	public String[] getLinks() {
		return links;
	}

	public ClickableSpan[] getClickableSpans() {
		return clickableSpans;
	}

}