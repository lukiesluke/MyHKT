package com.pccw.myhkt.cell.model;

import com.pccw.myhkt.R;

public class PlanTextIconCell extends Cell {


	//TEXTICON
	public PlanTextIconCell(int draw, String title) {
		super();
		type = Cell.PLANTEXTICON;
		this.title = title;
		this.draw = draw;
		titleColorId = R.color.black;
		titleSizeDelta = 4;
	}
}