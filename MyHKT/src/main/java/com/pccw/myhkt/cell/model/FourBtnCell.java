package com.pccw.myhkt.cell.model;

import android.graphics.drawable.Drawable;
import android.view.View.OnClickListener;

import com.pccw.myhkt.R;

public class FourBtnCell extends Cell{

	private int firstDraw;
	private int secondDraw;
	private int thirdDraw;
	private int fourthDraw;
	private OnClickListener firstClickListener;
	private OnClickListener secondClickListener;
	private OnClickListener thirdClickListener;
	private OnClickListener fourthClickListener;
	private Drawable firstDrawable;
	private Drawable secondDrawable;
	private Drawable thirdDrawable;
	private Drawable fourthDrawable;


	private Boolean isFirstHide = true;
	private Boolean isSecondHide = true;
	private Boolean isThirdHide = true;
	private Boolean isFourthHide = true;

	public FourBtnCell(String title, String content, int[] drawableArray, String[] clickArray){
		super();
		//Default setting
		type = Cell.FOURBTN;
		titleColorId = R.color.black;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = -2;

		this.title = title;
		this.content = content;
		this.firstDraw = drawableArray[0];
		this.secondDraw = drawableArray[1];
		this.thirdDraw = drawableArray[2];
		this.fourthDraw = drawableArray[3];
		this.clickArray = clickArray;
	}
	
	public int getFirstDraw() {
		return firstDraw;
	}

	public void setFirstDraw(int firstDraw) {
		this.firstDraw = firstDraw;
	}

	public int getSecondDraw() {
		return secondDraw;
	}

	public void setSecondDraw(int secondDraw) {
		this.secondDraw = secondDraw;
	}

	public int getThirdDraw() {
		return thirdDraw;
	}

	public void setThirdDraw(int thirdDraw) {
		this.thirdDraw = thirdDraw;
	}

	public int getFourthDraw() {
		return fourthDraw;
	}

	public void setFourthDraw(int fourthDraw) {
		this.fourthDraw = fourthDraw;
	}

	public OnClickListener getFirstClickListener() {
		return firstClickListener;
	}

	public void setFirstClickListener(OnClickListener firstClickListener) {
		this.firstClickListener = firstClickListener;
	}

	public OnClickListener getSecondClickListener() {
		return secondClickListener;
	}

	public void setSecondClickListener(OnClickListener secondClickListener) {
		this.secondClickListener = secondClickListener;
	}

	public OnClickListener getThirdClickListener() {
		return thirdClickListener;
	}

	public void setThirdClickListener(OnClickListener thirdClickListener) {
		this.thirdClickListener = thirdClickListener;
	}

	public OnClickListener getFourthClickListener() {
		return fourthClickListener;
	}

	public void setFourthClickListener(OnClickListener fourthClickListener) {
		this.fourthClickListener = fourthClickListener;
	}
	
	public Drawable getFirstDrawable() {
		return firstDrawable;
	}

	public void setFirstDrawable(Drawable firstDrawable) {
		this.firstDrawable = firstDrawable;
	}

	public Drawable getSecondDrawable() {
		return secondDrawable;
	}

	public void setSecondDrawable(Drawable secondDrawable) {
		this.secondDrawable = secondDrawable;
	}

	public Drawable getThirdDrawable() {
		return thirdDrawable;
	}

	public void setThirdDrawable(Drawable thirdDrawable) {
		this.thirdDrawable = thirdDrawable;
	}

	public Drawable getFourthDrawable() {
		return fourthDrawable;
	}

	public void setFourthDrawable(Drawable fourthDrawable) {
		this.fourthDrawable = fourthDrawable;
	}

	public Boolean getIsFirstHide() {
		return isFirstHide;
	}

	public void setIsFirstHide(Boolean isFirstHide) {
		this.isFirstHide = isFirstHide;
	}

	public Boolean getIsSecondHide() {
		return isSecondHide;
	}

	public void setIsSecondHide(Boolean isSecondHide) {
		this.isSecondHide = isSecondHide;
	}

	public Boolean getIsThirdHide() {
		return isThirdHide;
	}

	public void setIsThirdHide(Boolean isThirdHide) {
		this.isThirdHide = isThirdHide;
	}

	public Boolean getIsFourthHide() {
		return isFourthHide;
	}

	public void setIsFourthHide(Boolean isFourthHide) {
		this.isFourthHide = isFourthHide;
	}

}