package com.pccw.myhkt.cell.model;

import com.pccw.myhkt.R;

import android.util.Log;

public class TextIconCell extends Cell {

	
	//TEXTICON
	public TextIconCell(int draw, String title) {
		super();
		type = Cell.TEXTICON;
		this.title = title;
		this.draw = draw;
		titleColorId = R.color.black;
		titleSizeDelta = 4;
	}
}