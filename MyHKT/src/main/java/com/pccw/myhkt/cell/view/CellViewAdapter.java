package com.pccw.myhkt.cell.view;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.CircleViewCell;
import com.pccw.myhkt.cell.model.DetailBtnCell;
import com.pccw.myhkt.cell.model.EmailChildCell;
import com.pccw.myhkt.cell.model.EmailParentCell;
import com.pccw.myhkt.cell.model.FourBtnCell;
import com.pccw.myhkt.cell.model.ImageViewCell;
import com.pccw.myhkt.cell.model.LineTest;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.SmallSpannedTextCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.SpinnerTextCell;
import com.pccw.myhkt.cell.model.TextBtnCell;
import com.pccw.myhkt.cell.model.TextImageBtnCell;
import com.pccw.myhkt.cell.model.TitleSpinnerCell;
import com.pccw.myhkt.cell.model.TwoBtnCell;
import com.pccw.myhkt.cell.model.TwoTextBtnCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.enums.MessageCategory;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.CircularProgressBar;
import com.pccw.myhkt.lib.ui.HKTButton;

public class CellViewAdapter {
	private boolean		debug;
	private int 			extralinespace;
	private int 			padding_twocol;
	private int 			textViewHeight;
	private Context 		context;
	private LayoutInflater 	inflater;
	private Fragment 		fragment;
	private int deviceWidth ;
	private int basePadding ;

	private int scrollViewPos = 0 ;
	private Boolean isZh = true;
	private String lobString = "";

	public CellViewAdapter(Fragment frag) {
		this.fragment = frag;
		this.context = frag.getActivity();
		debug = context.getResources().getBoolean(R.bool.DEBUG);
		extralinespace = (int) context.getResources().getDimension(R.dimen.extralinespace);
		padding_twocol = (int) context.getResources().getDimension(R.dimen.padding_twocol);
		textViewHeight = (int) context.getResources().getDimension(R.dimen.textviewheight);
		Display display =  ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		basePadding = (int) context.getResources().getDimension(R.dimen.basePadding);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if ("zh".equalsIgnoreCase( frag.getString(R.string.myhkt_lang))) {
			isZh = true;
		} else {
			isZh = false;
		}
	}

	public CellViewAdapter (Context context){
		this.context = context;
		extralinespace = (int) context.getResources().getDimension(R.dimen.extralinespace);
		padding_twocol = (int) context.getResources().getDimension(R.dimen.padding_twocol);
		textViewHeight = (int) context.getResources().getDimension(R.dimen.textviewheight);
		Display display =  ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		basePadding = (int) context.getResources().getDimension(R.dimen.basePadding);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if ("zh".equalsIgnoreCase( context.getString(R.string.myhkt_lang))) {
			isZh = true;
		} else {
			isZh = false;
		}
	}

	//	public void setView(int layoutId ,List<Cell> cellList) {
	//		AAQuery aaq = new AAQuery(context);
	//		LinearLayout layout = (LinearLayout)aaq.id(layoutId).getView();
	//		layout.removeAllViews();
	//		for (Cell cell: cellList) {
	//			layout.addView(getView(cell));
	//		}
	//
	//	}

	public void setView(String lobString, LinearLayout layout, List<Cell> cellList) {
		this.lobString = lobString;
		setView(layout, cellList, 0);
	}

	public void setView(LinearLayout layout, List<Cell> cellList) {
		setView(layout, cellList, 0);
		//		AAQuery aaq = new AAQuery(context);
		//		LinearLayout layout = (LinearLayout)aaq.id(layoutId).getView();
		layout.removeAllViews();
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.invalidate();
		for (Cell cell : cellList) {
			layout.addView(getView(cell));
		}
	}

	public void setView(final LinearLayout layout, List<Cell> cellList, final int svPos) {
		//		AAQuery aaq = new AAQuery(context);
		//		LinearLayout layout = (LinearLayout)aaq.id(layoutId).getView();
		scrollViewPos = svPos;
		layout.removeAllViews();
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.invalidate();
		for (Cell cell : cellList) {
			layout.addView(getView(cell));
		}
		if (layout.getParent() instanceof ScrollView) {
			((ScrollView) layout.getParent()).post(new Runnable() {

				@Override
				public void run() {
					((ScrollView) layout.getParent()).scrollTo(0, svPos);
				}
			});
		}
	}

	public void changeView(LinearLayout layout, int position, Cell cell) {
		layout.removeViewAt(position);
		layout.addView(getView(cell), position);
	}

	public void addCustomView(LinearLayout layout, int position, View view) {
		layout.removeViewAt(position);
		layout.addView(view , position);
	}

	private View getView(Cell cell) {
		View cellView = null;
		AAQuery aq =null;
		int leftMargin = 0;
		int rightMargin = 0;
		int topMargin = 0;
		int botMargin = 0;
		int leftPadding = 0;
		int rightPadding = 0;
		int topPadding = 0;
		int botPadding = 0;
		Drawable draw = cell.getDrawable();
		switch(cell.getType()) {
		case Cell.ICONTEXT:
			cellView = inflater.inflate(R.layout.tablecell_icontext, null);
			aq = new AAQuery(cellView);
			//Default margin/padding setting
			leftPadding = basePadding;
			rightPadding = basePadding;

			aq.normText(R.id.table_cell_title, cell.getTitle(), cell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).getTextView().setTypeface(Typeface.MONOSPACE, cell.getTitleTypeface());
			if (cell.getCellHeight() != 0) {
				aq.id(R.id.table_cell_title).height(cell.getCellHeight());
			}
			aq.id(R.id.table_cell_icon).image(cell.getDraw());
			aq.marginpx(R.id.table_cell_icon, 0, 0, padding_twocol, 0);
			aq.gravity(R.id.table_cell_icon, Gravity.CENTER_VERTICAL);
			break;
		case Cell.TEXTICON:
			cellView = inflater.inflate(R.layout.tablecell_texticon, null);
			aq = new AAQuery(cellView);
			//Default margin/padding setting
			leftPadding = extralinespace;
			rightPadding = extralinespace;

			aq.normText(R.id.table_cell_title, cell.getTitle());
			aq.id(R.id.table_cell_title).getTextView().setTypeface(Typeface.MONOSPACE, cell.getTitleTypeface());
			aq.id(R.id.table_cell_icon).image(cell.getDraw());
			aq.marginpx(R.id.table_cell_icon, padding_twocol, 0, 0 , 0);
			aq.gravity(R.id.table_cell_icon, Gravity.CENTER_VERTICAL);
			break;
		case Cell.PLANTEXTICON:
			cellView = inflater.inflate(R.layout.tablecell_texticon, null);
			aq = new AAQuery(cellView);
			//Default margin/padding setting
			leftPadding = extralinespace;
			rightPadding = extralinespace;

			aq.normText(R.id.table_cell_title, cell.getTitle());
			aq.id(R.id.table_cell_title).getTextView().setTypeface(Typeface.MONOSPACE, cell.getTitleTypeface());
			aq.id(R.id.table_cell_title).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
			aq.id(R.id.table_cell_icon).image(cell.getDraw());
			aq.marginpx(R.id.table_cell_icon, padding_twocol, 0, 0 , 0);
			aq.gravity(R.id.table_cell_icon, Gravity.CENTER_VERTICAL);
			break;
		case Cell.LINE:
			cellView = inflater.inflate(R.layout.tablecell_line, null);
			aq = new AAQuery(cellView);

			leftPadding = extralinespace;
			rightPadding = extralinespace;

			LinearLayout.LayoutParams lineParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT , (int) context.getResources().getDimension(R.dimen.greyline_height));
			aq.id(R.id.table_cell_line).getImageView().setLayoutParams(lineParams);
			aq.id(R.id.table_cell_line).getImageView().setImageResource(R.drawable.greyline);
			aq.id(R.id.table_cell_line).getView().setPadding(0, 0, 0,0);

			break;
		case Cell.BIGTEXT1:
			cellView = inflater.inflate(R.layout.tablecell_bigtext1, null);
			BigTextCell bigTextCell = (BigTextCell) cell;
			aq = new AAQuery(cellView);
			//Default margin/padding setting
			leftMargin = extralinespace;
			rightMargin = extralinespace;
			topMargin = extralinespace;
			botMargin = extralinespace;

			aq.normText(R.id.table_cell_getheight, "getArrowHeight", "".equals(bigTextCell.getTitle()) ? bigTextCell.getContentSizeDelta() :bigTextCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_getheight).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).visibility(View.INVISIBLE).backgroundColorId(R.color.blue);
			aq.id(R.id.table_cell_arrow).image(R.drawable.rightarrow_small).visibility(bigTextCell.isArrowShown()? View.VISIBLE : View.GONE);
			aq.id(R.id.table_cell_arrow).getImageView().setScaleType(ScaleType.CENTER);
			aq.marginpx(R.id.table_cell_arrow, 0, 0, padding_twocol, 0);
			//Title
			aq.normText(R.id.table_cell_title, bigTextCell.getTitle(), bigTextCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).textColorId(bigTextCell.getTitleColorId()).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
			aq.id(R.id.table_cell_title).getTextView().setTypeface(Typeface.MONOSPACE, bigTextCell.getTitleTypeface());
			//Content
			aq.normText(R.id.table_cell_contant, bigTextCell.getContent(), bigTextCell.getContentSizeDelta());
			aq.id(R.id.table_cell_contant).textColorId(bigTextCell.getContentColorId()).height(LinearLayout.LayoutParams.WRAP_CONTENT, true);
			aq.gravity(R.id.table_cell_contant, Gravity.TOP);

			if(bigTextCell.getLinks() != null && bigTextCell.getClickableSpans() != null && bigTextCell.getLinks().length == bigTextCell.getClickableSpans().length) {
				Utils.setTextViewClickableSpan(aq.id(R.id.table_cell_contant).getTextView(), bigTextCell.getLinks(), bigTextCell.getClickableSpans());
			}

			break;
		case Cell.SMALLTEXT1 :
			cellView = inflater.inflate(R.layout.tablecell_smalltext, null);
			aq = new AAQuery(cellView);
			SmallTextCell smallTextCell = (SmallTextCell) cell;
			//Default margin/padding setting
			leftPadding = extralinespace;
			rightPadding = extralinespace;
			topPadding = extralinespace/4;
			botPadding = extralinespace/4;

			aq.normTextGrey(R.id.table_cell_title, cell.getTitle(), smallTextCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).textColorId(smallTextCell.getTitleColorId()).height(LinearLayout.LayoutParams.WRAP_CONTENT);
			aq.layoutWeight(R.id.table_cell_title, 1);
			aq.id(R.id.table_cell_title).getTextView().setTypeface(Typeface.MONOSPACE, cell.getTitleTypeface());
			aq.padding(R.id.table_cell_title, 0, 0, extralinespace, 0);
			aq.gravity(R.id.table_cell_title, smallTextCell.getTitleGravity());
			if (smallTextCell.getContent() !=null && !smallTextCell.getContent().equals("")){
				aq.normTextBlue(R.id.table_cell_contant, smallTextCell.getContent(),smallTextCell.getContentSizeDelta());
				aq.id(R.id.table_cell_contant).textColorId(smallTextCell.getContentColorId()).height(LinearLayout.LayoutParams.WRAP_CONTENT).visibility(cell.isExpandText() ? View.GONE : View.VISIBLE);
				if (cell.getServiceType() !=null && cell.getServiceType().contains("IDD")) {
					aq.layoutWeight(R.id.table_cell_contant, 0.6f);
				}	else {
					aq.layoutWeight(R.id.table_cell_contant, 1);
				}
			} else {
				aq.id(R.id.table_cell_contant).visibility(View.GONE);
			}
			break;
		case Cell.SMALLTEXT2 :
			cellView = inflater.inflate(R.layout.tablecell_smalltext, null);
			aq = new AAQuery(cellView);
			SmallSpannedTextCell smallSpannedTextCell = (SmallSpannedTextCell) cell;
			//Default margin/padding setting
			leftPadding = extralinespace;
			rightPadding = extralinespace;
			topPadding = extralinespace/4;
			botPadding = extralinespace/4;

			aq.normTextGrey(R.id.table_cell_title, cell.getTitle(), smallSpannedTextCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).textColorId(smallSpannedTextCell.getTitleColorId()).height(LinearLayout.LayoutParams.WRAP_CONTENT);
			aq.layoutWeight(R.id.table_cell_title, 1);
			aq.id(R.id.table_cell_title).getTextView().setTypeface(Typeface.MONOSPACE, cell.getTitleTypeface());
			aq.padding(R.id.table_cell_title, 0, 0, extralinespace, 0);
			aq.gravity(R.id.table_cell_title, smallSpannedTextCell.getTitleGravity());
			if (smallSpannedTextCell.getContent() !=null && !smallSpannedTextCell.getContent().equals("")) {
				aq.normTextBlue(R.id.table_cell_contant, smallSpannedTextCell.getContent(),smallSpannedTextCell.getContentSizeDelta());
				aq.id(R.id.table_cell_contant).textColorId(smallSpannedTextCell.getContentColorId()).height(LinearLayout.LayoutParams.WRAP_CONTENT).visibility(cell.isExpandText() ? View.GONE : View.VISIBLE);
				aq.layoutWeight(R.id.table_cell_contant, 1);
			} else {
				aq.id(R.id.table_cell_contant).visibility(View.GONE);
			}

			if(smallSpannedTextCell.getLinks() != null && smallSpannedTextCell.getClickableSpans() != null && smallSpannedTextCell.getLinks().length == smallSpannedTextCell.getClickableSpans().length) {
				Utils.setTextViewClickableSpan(aq.id(R.id.table_cell_title).getTextView(), smallSpannedTextCell.getLinks(), smallSpannedTextCell.getClickableSpans());
			}
			break;
		case Cell.WEBVIEW:
			cellView = inflater.inflate(R.layout.tablecell_webview, null);

			//Check if the locale set to EN, if yes switch to Zn
			if(Build.VERSION.SDK_INT >= 24 &&  isZh && "en".equalsIgnoreCase(context.getResources().getString(R.string.myhkt_lang))) {
				Utils.switchToZhLocale(context);
			}

			WebViewCell webViewCell = (WebViewCell) cell;

			aq = new AAQuery(cellView);
			//Default margin/padding setting
			leftMargin = basePadding;
			rightMargin = basePadding;
			if (ClnEnv.getSessionPremierFlag()) {
				if ("file:///android_asset/lntt_reslut_lts_enquiry_en.html".equals(webViewCell.getTitle())) { webViewCell.setTitle( "file:///android_asset/lntt_reslut_lts_enquiry_en_premier.html") ;}
				if ("file:///android_asset/lntt_reslut_lts_enquiry_zh.html".equals(webViewCell.getTitle())) { webViewCell.setTitle( "file:///android_asset/lntt_reslut_lts_enquiry_zh_premier.html") ;}
			}
			aq.id(R.id.table_cell_webview).getWebView().getSettings().setRenderPriority(RenderPriority.HIGH);
			aq.id(R.id.table_cell_webview).getWebView().getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
			aq.id(R.id.table_cell_webview).getWebView().getSettings().setDefaultTextEncodingName("utf-8");
			aq.id(R.id.table_cell_webview).getWebView().setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			aq.id(R.id.table_cell_webview).getWebView().loadUrl(webViewCell.getTitle());
			aq.padding(R.id.table_cell_webview, basePadding ,0 ,basePadding, 0);
			break;
		case Cell.SINGLEBTN:
			cellView = inflater.inflate(R.layout.tablecell_singlebtn, null);
			SingleBtnCell singleBtnCell = (SingleBtnCell) cell;
			aq = new AAQuery(cellView);
			//Default margin/padding setting
			leftMargin = extralinespace;
			rightMargin = extralinespace;
			leftPadding = extralinespace;
			//			topPadding = extralinespace;
			botPadding = extralinespace;
			rightPadding = extralinespace;

			HKTButton hktBtn = aq.normTxtBtn(R.id.table_cell_btn, singleBtnCell.getTitle(), singleBtnCell.getBtnWidth());
			//change theme for MyMob
			if (ClnEnv.isMyMobFlag()) {
				int btnType = HKTButton.TYPE_ORANGE;
				if (ClnEnv.isIs101Flag()) {
					hktBtn.setType(HKTButton.TYPE_BLACK);
				} else if (SubnRec.LOB_CSP.equalsIgnoreCase(lobString) || SubnRec.WLOB_XCSP.equalsIgnoreCase(lobString)) {
					hktBtn.setType(HKTButton.TYPE_PURPLE);
				} else {
					hktBtn.setType(btnType);
				}
			}
			aq.gravity(R.id.table_cell_btn, Gravity.CENTER);
			aq.id(R.id.table_cell_btn).clicked(singleBtnCell.getOnClickListener());
			if (draw != null) {
				hktBtn.setDrawable(draw);
			} else if (singleBtnCell.getDraw() >0 ) {
				int imageH ;
				int imageW ;
				int bh = (int) context.getResources().getDimension(R.dimen.buttonblue_height);
				Drawable drawble = context.getResources().getDrawable(singleBtnCell.getDraw());
				int h = drawble.getIntrinsicHeight();
				int w = drawble.getIntrinsicWidth();
				if (("").equals(singleBtnCell.getTitle())) {
					imageH = bh - extralinespace;
				} else {
					imageH = bh - padding_twocol *2;
				}
				imageW = w * (imageH) / h;
				drawble.setBounds( 0, 0, imageW, imageH);
				hktBtn.setDrawable(drawble);
			}

			aq.gravity(R.id.table_cell_layout, Gravity.CENTER);
			break;
		case Cell.FOURBTN:
			cellView = inflater.inflate(R.layout.tablecell_btns4, null);
			FourBtnCell fourBtnCell = (FourBtnCell) cell;
			aq = new AAQuery(cellView);
//			int btnWidth = (deviceWidth -basePadding * 2 -padding_twocol * 2 - extralinespace * 2) /2;
//			LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//					(int) context.getResources().getDimension(R.dimen.buttonblue_height_4btn) * 2);
//			layoutParams.weight = 1;
			//Default margin/padding setting
			leftPadding = extralinespace;
			topPadding = 0;
			botPadding = extralinespace;
			rightPadding = extralinespace;

			int btn_four_Width =  (deviceWidth -basePadding * 4 -9 * 4 - basePadding * 4) /4;

			processHktButton(aq, btn_four_Width, fourBtnCell.getTitle(), fourBtnCell.getFirstClickListener(),
					fourBtnCell.getClickArray()[0], fourBtnCell.getFirstDrawable(), fourBtnCell.getFirstDraw(), R.id.table_cell_layout_btns4_first);
			processHktButton(aq, btn_four_Width, fourBtnCell.getTitle(), fourBtnCell.getSecondClickListener(),
					fourBtnCell.getClickArray()[1], fourBtnCell.getSecondDrawable(), fourBtnCell.getSecondDraw(), R.id.table_cell_layout_btns4_second);
			processHktButton(aq, btn_four_Width, fourBtnCell.getTitle(), fourBtnCell.getThirdClickListener(),
					fourBtnCell.getClickArray()[2], fourBtnCell.getThirdDrawable(), fourBtnCell.getThirdDraw(), R.id.table_cell_layout_btns4_third);
			processHktButtonButSmaller(aq, btn_four_Width, fourBtnCell.getTitle(), fourBtnCell.getFourthClickListener(),
					fourBtnCell.getClickArray()[3], fourBtnCell.getFourthDrawable(), fourBtnCell.getFourthDraw(), R.id.table_cell_layout_btns4_fourth);

			break;

			case Cell.TWOBTN:
				cellView = inflater.inflate(R.layout.tablecell_btns2, null);
				TwoBtnCell twoBtnCell = (TwoBtnCell) cell;
				aq = new AAQuery(cellView);
//			int btnWidth = (devic   eWidth -basePadding * 2 -padding_twocol * 2 - extralinespace * 2) /2;

				//Default margin/padding setting
				leftPadding = extralinespace;
				topPadding = 0;
				botPadding = extralinespace;
				rightPadding = extralinespace;

				int btnWidth = (deviceWidth -basePadding * 2 -9 * 2 - basePadding * 2) /2;

				HKTButton leftHktBtn = aq.normTxtBtn(R.id.table_cell_layout_btns2_left, twoBtnCell.getTitle(),btnWidth, HKTButton.TYPE_BLUE);
				//change theme for MyMob
				if (ClnEnv.isMyMobFlag()) {
					int btnType = HKTButton.TYPE_ORANGE;
					if (ClnEnv.isIs101Flag()) {
						leftHktBtn.setType(HKTButton.TYPE_BLACK);
					} else if (SubnRec.LOB_CSP.equalsIgnoreCase(lobString) ||  SubnRec.WLOB_XCSP.equalsIgnoreCase(lobString)) {
						leftHktBtn.setType(HKTButton.TYPE_PURPLE);
					} else {
						leftHktBtn.setType(btnType);
					}
				}
				aq.gravity(R.id.table_cell_layout_btns2_left, Gravity.CENTER);
				if (twoBtnCell.getLeftClickListener() != null) {
					aq.id(R.id.table_cell_layout_btns2_left).clicked(twoBtnCell.getLeftClickListener());
				} else if (twoBtnCell.getClickArray() != null && twoBtnCell.getClickArray().length > 0) {
					aq.id(R.id.table_cell_layout_btns2_left).clicked(fragment !=null ? fragment : context, twoBtnCell.getClickArray()[0]);
				}
				if (twoBtnCell.getLeftDrawable() != null) {
					leftHktBtn.setDrawable(twoBtnCell.getLeftDrawable());
				} else if (twoBtnCell.getLeftDraw() >0 ) {

					int imageH ;
					int imageW ;
					int bh = (int) context.getResources().getDimension(R.dimen.buttonblue_height);
					Drawable drawble = context.getResources().getDrawable(twoBtnCell.getLeftDraw());
					int h = drawble.getIntrinsicHeight();
					int w = drawble.getIntrinsicWidth();

					if (("").equals(twoBtnCell.getTitle())) {
						imageH = bh - extralinespace;
					} else {
						imageH = bh - padding_twocol *2;
					}
					imageW = w * (imageH) / h;
					drawble.setBounds( 0, 0, imageW, imageH);
					leftHktBtn.setDrawable(drawble);
				} else {
					if (("").equals(twoBtnCell.getTitle())) {
						leftHktBtn.setVisibility(View.INVISIBLE);
					}
				}
				aq.marginpx(R.id.table_cell_layout_btns2_left, 0 , 0 , padding_twocol, 0);

				HKTButton rightHktBtn = aq.normTxtBtn(R.id.table_cell_layout_btns2_right, twoBtnCell.getContent(), btnWidth, HKTButton.TYPE_BLUE);
				//change theme for MyMob
				if (ClnEnv.isMyMobFlag()) {
					int btnType = HKTButton.TYPE_ORANGE;
					if (ClnEnv.isIs101Flag()) {
						rightHktBtn.setType(HKTButton.TYPE_BLACK);
					} else if (SubnRec.LOB_CSP.equalsIgnoreCase(lobString) ||  SubnRec.WLOB_XCSP.equalsIgnoreCase(lobString)) {
						rightHktBtn.setType(HKTButton.TYPE_PURPLE);
					} else {
						rightHktBtn.setType(btnType);
					}
				}

				aq.gravity(R.id.table_cell_layout_btns2_right, Gravity.CENTER);
				if (twoBtnCell.getRightClickListener() != null) {
					aq.id(R.id.table_cell_layout_btns2_right).clicked(twoBtnCell.getRightClickListener());
				} else if (twoBtnCell.getClickArray() != null && twoBtnCell.getClickArray().length > 1) {
					aq.id(R.id.table_cell_layout_btns2_right).clicked(fragment !=null ? fragment : context, twoBtnCell.getClickArray()[1]);
				}
				if (twoBtnCell.getRightDrawable() != null) {
					rightHktBtn.setDrawable(twoBtnCell.getRightDrawable());
				} else if (twoBtnCell.getRightDraw() >0 ) {

					int imageH ;
					int imageW ;
					int bh = (int) context.getResources().getDimension(R.dimen.buttonblue_height);
					Drawable drawble = context.getResources().getDrawable(twoBtnCell.getRightDraw());
					int h = drawble.getIntrinsicHeight();
					int w = drawble.getIntrinsicWidth();

					if (("").equals(twoBtnCell.getContent())) {
						imageH = bh - extralinespace;
					} else {
						imageH = bh - padding_twocol *2;
					}
					imageW = w * (imageH) / h;
					drawble.setBounds( 0, 0, imageW, imageH);
					rightHktBtn.setDrawable(drawble);
				} else {
					if (("").equals(twoBtnCell.getContent())) {
						rightHktBtn.setVisibility(View.INVISIBLE);
					}
				}
				aq.marginpx(R.id.table_cell_layout_btns2_right, padding_twocol , 0 , 0 , 0);
				break;
		case Cell.ARROWTEXT:
			cellView = inflater.inflate(R.layout.tablecell_cellx, null);
			aq = new AAQuery(cellView);
			ArrowTextCell arrowTextCell = (ArrowTextCell) cell;
			String [] textArray = arrowTextCell.getTextArray();
			String []  noteArray = arrowTextCell.getNoteArray();
			Boolean showNote  = noteArray != null && noteArray.length > 0;
			int []  widthArray = arrowTextCell.getWidthArray();

			//Default margin/padding setting
			leftPadding = basePadding;
			//In arrow text ,right padding will not use in cell_layout bg
			rightPadding = basePadding;
			if (cell.getRightPadding() !=-1) rightPadding = cell.getRightPadding();

			((LinearLayout)aq.id(R.id.table_cell_content_layout).getView()).setGravity(Gravity.RIGHT|Gravity.CENTER);

			//Arrow part
			aq.normText(R.id.table_cell_getheight, "getArrowHeight", "".equals(arrowTextCell.getTitle()) ? arrowTextCell.getContentSizeDelta() :arrowTextCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_getheight).visibility(View.INVISIBLE).backgroundColorId(R.color.blue);
			if (arrowTextCell.getTitleWrapContent()) {
				aq.id(R.id.table_cell_getheight).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
			}
			aq.id(R.id.table_cell_arrow).image(R.drawable.rightarrow_small).visibility(arrowTextCell.isArrowShown()? View.VISIBLE : View.GONE);
			aq.id(R.id.table_cell_arrow).getImageView().setScaleType(ScaleType.CENTER);
			aq.marginpx(R.id.table_cell_arrow, 0, 0, padding_twocol, 0);

			//Title part
			aq.normText(R.id.table_cell_title, arrowTextCell.getTitle(), arrowTextCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).textColorId(arrowTextCell.getTitleColorId());
			if (arrowTextCell.getTitleWrapContent()) {
				aq.id(R.id.table_cell_title).height(ViewGroup.LayoutParams.WRAP_CONTENT, false);
			}

			aq.id(R.id.table_cell_title).getTextView().setTypeface(Typeface.MONOSPACE, arrowTextCell.getTitleTypeface());
			aq.gravity(R.id.table_cell_title, Gravity.LEFT|Gravity.CENTER);
			if (!arrowTextCell.isArrowShown()){
				aq.marginpx(R.id.table_cell_title, 0, 0, 0, 0);
			}

			//Col part
			int textviewheight = arrowTextCell.getCellHeight() != 0 ? arrowTextCell.getCellHeight() : (int) context.getResources().getDimension(R.dimen.textviewheight);
			aq.id(R.id.table_cell_col1_layout).visibility(View.GONE).height(textviewheight, false);
			aq.id(R.id.table_cell_col2_layout).visibility(View.GONE).height(textviewheight, false);
			aq.id(R.id.table_cell_col3_layout).visibility(View.GONE).height(textviewheight, false);
			if (textArray != null) {
				if(textArray.length >= 1) {
					//Content part
					if (arrowTextCell.isTaller()) {//for taller layout
						ViewGroup.LayoutParams paramCont = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0, 4.0f);
						aq.id(R.id.table_cell_col1).getTextView().setLayoutParams(paramCont);
					}
					aq.id(R.id.table_cell_col1_layout).visibility(View.VISIBLE).width(widthArray[0], false);
					aq.normText(R.id.table_cell_col1, textArray[0], arrowTextCell.getContentSizeDelta());
					aq.id(R.id.table_cell_col1).textColorId(arrowTextCell.getContentColorId()).height(0);
					aq.gravity(R.id.table_cell_col1, Gravity.RIGHT|Gravity.BOTTOM);
					//Note part
					if (arrowTextCell.isTaller()) {//for taller layout
						ViewGroup.LayoutParams paramNote = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0, 1.0f);
						aq.id(R.id.table_cell_col1_note).getTextView().setLayoutParams(paramNote);
					}
					if (showNote) {
						aq.normText(R.id.table_cell_col1_note, noteArray[0], arrowTextCell.getNoteSizeDelta());
						aq.gravity(R.id.table_cell_col1_note, Gravity.RIGHT|Gravity.BOTTOM);
						aq.id(R.id.table_cell_col1_note).visibility(View.VISIBLE).textColorId(arrowTextCell.getContentColorId()).height(0);
					} else {
						aq.id(R.id.table_cell_col1_note).visibility(View.GONE).textColorId(arrowTextCell.getContentColorId());
						//* center the content part if no note shown
						aq.gravity(R.id.table_cell_col1, Gravity.RIGHT|Gravity.CENTER);
					}
					aq.padding(R.id.table_cell_col1_layout, 0 ,0 ,rightPadding, 0);
				}

				if(textArray.length >= 2) {
					if (arrowTextCell.isTaller()) {//for taller layout
						ViewGroup.LayoutParams paramCont = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0, 4.0f);
						aq.id(R.id.table_cell_col2).getTextView().setLayoutParams(paramCont);
					}
					aq.id(R.id.table_cell_col2_layout).visibility(View.VISIBLE).width(widthArray[1], false);
					aq.normText(R.id.table_cell_col2, textArray[1], arrowTextCell.getContentSizeDelta());
					aq.id(R.id.table_cell_col2).textColorId(arrowTextCell.getContentColorId()).height(0);
					aq.gravity(R.id.table_cell_col2, Gravity.RIGHT|Gravity.BOTTOM);
					if (arrowTextCell.isTaller()) {//for taller layout
						ViewGroup.LayoutParams paramNote = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0, 1.0f);
						aq.id(R.id.table_cell_col2_note).getTextView().setLayoutParams(paramNote);
					}
					if (showNote) {
						aq.normText(R.id.table_cell_col2_note, noteArray[1], arrowTextCell.getNoteSizeDelta());
						aq.gravity(R.id.table_cell_col2_note, Gravity.RIGHT|Gravity.BOTTOM);
						aq.id(R.id.table_cell_col2_note).visibility(View.VISIBLE).textColorId(arrowTextCell.getContentColorId()).height(0);
					} else {
						aq.id(R.id.table_cell_col2_note).visibility(View.GONE).textColorId(arrowTextCell.getContentColorId());
						//* center the content part if no note shown
						aq.gravity(R.id.table_cell_col2, Gravity.RIGHT|Gravity.CENTER);
					}
					aq.padding(R.id.table_cell_col1_layout, 0 ,0 ,0, 0);
					aq.padding(R.id.table_cell_col2_layout, 0 ,0 ,rightPadding, 0);
				}
				if(textArray.length >= 3) {
					if (arrowTextCell.isTaller()) {//for taller layout
						ViewGroup.LayoutParams paramCont = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0, 4.0f);
						aq.id(R.id.table_cell_col3).getTextView().setLayoutParams(paramCont);
					}
					aq.id(R.id.table_cell_col3_layout).visibility(View.VISIBLE).width(widthArray[2], false);
					aq.normText(R.id.table_cell_col3, textArray[2], arrowTextCell.getContentSizeDelta());
					aq.id(R.id.table_cell_col3).textColorId(arrowTextCell.getContentColorId()).height(0);
					aq.gravity(R.id.table_cell_col3, Gravity.RIGHT|Gravity.BOTTOM);
					if (arrowTextCell.isTaller()) {//for taller layout
						ViewGroup.LayoutParams paramNote = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0, 1.0f);
						aq.id(R.id.table_cell_col3_note).getTextView().setLayoutParams(paramNote);
					}
					if (showNote) {
						aq.normText(R.id.table_cell_col3_note, noteArray[2], arrowTextCell.getNoteSizeDelta());
						aq.gravity(R.id.table_cell_col3_note, Gravity.RIGHT|Gravity.BOTTOM);
						aq.id(R.id.table_cell_col3_note).visibility(View.VISIBLE).textColorId(arrowTextCell.getContentColorId()).height(0);
					} else {
						aq.id(R.id.table_cell_col3_note).visibility(View.GONE).textColorId(arrowTextCell.getContentColorId());
						//* center the content part if no note shown
						aq.gravity(R.id.table_cell_col3, Gravity.RIGHT|Gravity.CENTER);
					}
					aq.padding(R.id.table_cell_col2_layout, 0 ,0 ,0, 0);
					aq.padding(R.id.table_cell_col3_layout, 0 ,0 ,rightPadding, 0);
				}

				rightPadding = 0;
			}
			break;
		case Cell.TEXTIMGBTN:
			cellView = inflater.inflate(R.layout.tablecell_textimgbtn, null);
			aq = new AAQuery(cellView);
			TextImageBtnCell textImgBtnCell = (TextImageBtnCell) cell;

			//Default margin/padding setting
			leftPadding = extralinespace;
			rightPadding = extralinespace;
			topPadding = extralinespace;
			botPadding = extralinespace;

			//Set Title
			aq.normText(R.id.table_cell_title, textImgBtnCell.getTitle(), textImgBtnCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).width(ViewGroup.LayoutParams.MATCH_PARENT);
			//			aq.layoutWeight(R.id.table_cell_title, 1);

			//Set Button
			final ImageView textImgBtn = (ImageView) aq.id(R.id.table_cell_imgbtn).getView();
			//			aq.layoutWeight(R.id.table_cell_imgbtn, 1);
			if(textImgBtnCell.getOnClickListener() != null)
				aq.id(R.id.table_cell_imgbtn).clicked(textImgBtnCell.getOnClickListener());
			else if(textImgBtnCell.getClickArray() != null)
				aq.id(R.id.table_cell_imgbtn).clicked(fragment, textImgBtnCell.getClickArray()[0]);

			if (textImgBtnCell.getDraw() > 0) {
				int bh = (int) context.getResources().getDimension(R.dimen.fbImageSize);
				aq.id(R.id.table_cell_imgbtn).image(context.getResources().getDrawable(textImgBtnCell.getDraw()));
				aq.id(R.id.table_cell_imgbtn).height(bh*2, false).width(bh*2, false);
				aq.id(R.id.table_cell_imgbtn).getImageView().setScaleType(ScaleType.CENTER_INSIDE);
			}

			break;
		case Cell.TEXTBTN:
			cellView = inflater.inflate(R.layout.tablecell_textbtn, null);
			aq = new AAQuery(cellView);
			TextBtnCell textBtnCell = (TextBtnCell) cell;

			//Default margin/padding setting
			leftPadding = basePadding;
			rightPadding = basePadding;

			//Set Title
			aq.normText(R.id.table_cell_title, textBtnCell.getTitle(), textBtnCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).width(0).textColorId(textBtnCell.getTitleColorId());
			aq.layoutWeight(R.id.table_cell_title, 1);
			aq.gravity(R.id.table_cell_title, textBtnCell.getTitleGravity());
			if (textBtnCell.getTitleHeight() !=0) {
				aq.id(R.id.table_cell_title).height(textBtnCell.getTitleHeight() , false);
			}

			//Set Button
			aq.id(R.id.table_cell_btn_layout).width(0);
			aq.layoutWeight(R.id.table_cell_btn_layout, 1);

			final HKTButton textHKBtn = (HKTButton) aq.id(R.id.table_cell_btn).getView();
			if(!textBtnCell.getContent().equals("")) {
				aq.normTxtBtn(textBtnCell.getContentSizeDelta(), R.id.table_cell_btn, textBtnCell.getContent(), textBtnCell.getBtnWidth());

				if(textBtnCell.getOnClickListener() != null)
					aq.id(R.id.table_cell_btn).clicked(textBtnCell.getOnClickListener());
				else if(textBtnCell.getClickArray() != null)
					aq.id(R.id.table_cell_btn).clicked(fragment, textBtnCell.getClickArray()[0]);
				if (draw !=null) {
					textHKBtn.setDrawable(draw);
				} else if (textBtnCell.getDraw() > 0) {
					int imageH ;
					int imageW ;
					int bh = (int) context.getResources().getDimension(R.dimen.buttonblue_height);
					Drawable drawble = context.getResources().getDrawable(textBtnCell.getDraw());
					int h = drawble.getIntrinsicHeight();
					int w = drawble.getIntrinsicWidth();

					if (("").equals(textBtnCell.getTitle())) {
						imageH = bh - extralinespace;
					} else {
						if (textBtnCell.getDraw() == R.drawable.btn_mail) {
							imageH = bh / 3;
						} else {
							imageH = bh - padding_twocol *3;
						}
					}
					imageW = w * (imageH) / h;
					drawble.setBounds( 0, 0, imageW, imageH);
					textHKBtn.setDrawable(drawble);
				}
				textHKBtn.post(new Runnable() {
					@Override
					public void run() {
						if (textHKBtn.getLineCount() >= 2) {
							//				    		RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) textHKBtn.getLayoutParams();
							//				    		lp.height = (int) (context.getResources().getDimension(R.dimen.buttonblue_height)*1.3);
							//				    		textHKBtn.setLayoutParams(lp);
							textHKBtn.setTextSize(10.0f);
						}
					}
				});

				if (ClnEnv.isMyMobFlag()) {
					if (SubnRec.LOB_CSP.equalsIgnoreCase(lobString) ||  SubnRec.WLOB_XCSP.equalsIgnoreCase(lobString)) {
						textHKBtn.setType(HKTButton.TYPE_PURPLE);
					}
				}

			} else {
				textHKBtn.setEnabled(false);
				textHKBtn.setVisibility(View.GONE);
			}

			break;
		case Cell.LINETEST:
			cellView = inflater.inflate(R.layout.tablecell_live_test, null);
			aq = new AAQuery(cellView);
			LineTest lineTestCell = (LineTest) cell;
			int imageHeight = (int) context.getResources().getDimension(R.dimen.reg_confirm_view_height);
			int buttonPadding =  (int) context.getResources().getDimension(R.dimen.reg_logo_padding_1);
			int iconHeight = (int) context.getResources().getDimension(R.dimen.textviewheight) * 2/3 ;
			//Default margin/padding setting
			leftPadding = extralinespace;
			rightPadding = extralinespace;
			topPadding = extralinespace/4;
			botPadding = extralinespace/4;

			//Image part
			aq.id(R.id.tablecell_live_test_container).image(R.drawable.logo_container_gray);
			aq.id(R.id.tablecell_live_test_container).height(imageHeight, false).width(imageHeight, false);

			aq.id(R.id.tablecell_live_test_logo).image(lineTestCell.getDraw());
			if (lineTestCell.getDraw() == R.drawable.logo_now) {
				aq.marginpx(R.id.tablecell_live_test_logo, buttonPadding, 0, buttonPadding,	 0);
			}

			aq.id(R.id.tablecell_live_test_logo).height(imageHeight-2, false).width(imageHeight-2, false);

			//			aq.normText(R.id.tablecell_live_test_name_txt, liveTestCell.getContent(), liveTestCell.getTitleSizeDelta());

			//icon
			aq.id(R.id.tablecell_live_test_state).image(lineTestCell.getRightResId()).height(iconHeight , false).width(iconHeight , false);

			aq.normText(R.id.tablecell_live_test_name_txt, lineTestCell.getTitle(), lineTestCell.getTitleSizeDelta());
			aq.id(R.id.tablecell_live_test_name_txt).textColorId(lineTestCell.getTitleColorId()).height(0);
			if (debug) Log.i("Line Test ", lineTestCell.getContent());
			aq.normText(R.id.tablecell_live_test_status_txt, lineTestCell.getContent(), lineTestCell.getContentSizeDelta());
			aq.id(R.id.tablecell_live_test_status_txt).textColorId(lineTestCell.getContentColorId()).height(ViewGroup.LayoutParams.MATCH_PARENT);
			break;
		case Cell.IMAGEVIEW:
			cellView = inflater.inflate(R.layout.tablecell_image, null);
			aq = new AAQuery(cellView);
			ImageViewCell imageCell = (ImageViewCell) cell;
			String url = imageCell.getUrl();
			if (!"".equals(url)) {
				aq.id(R.id.table_cell_imageview).image(url, true, true);
			}
			if (imageCell.getClickArray() != null) {
				aq.id(R.id.table_cell_imageview).clicked(fragment !=null ? fragment : context, imageCell.getClickArray()[0]);
			}
			if (imageCell.getOnClickListener() !=null) {
				aq.id(R.id.table_cell_imageview).getView().setOnClickListener(imageCell.getOnClickListener());
			}
			break;
		case Cell.IMAGE_VIEW_DRAWABLE:
			cellView = inflater.inflate(R.layout.tablecell_image, null);
			aq = new AAQuery(cellView);
			ImageViewCell imageViewCell = (ImageViewCell) cell;
			if(imageViewCell.getDrawable() != null) {

				RequestOptions requestOptions = new RequestOptions()
						.placeholder(imageViewCell.getDrawable());

				Glide.with(cellView)
						.asDrawable()
						.load("")
						.apply(requestOptions)
						.into(aq.id(R.id.table_cell_imageview).getImageView());
			}
			break;
		case Cell.TITLESPINNER:
			cellView = inflater.inflate(R.layout.tablecell_titlespinner, null);
			aq = new AAQuery(cellView);
			TitleSpinnerCell titleSpinnerCell = (TitleSpinnerCell) cell;

			//Default margin/padding setting
			//			leftPadding = extralinespace;
			//			rightPadding = extralinespace;
			//			topPadding = extralinespace/4;
			//			botPadding = extralinespace/4;

			aq.normTextGrey(R.id.table_cell_title, cell.getTitle(), titleSpinnerCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).textColorId(cell.getTitleColorId());
			aq.gravity(R.id.table_cell_title, Gravity.RIGHT);
			aq.padding(R.id.table_cell_title, 0, 0,padding_twocol ,0);
			aq.layoutWeight(R.id.table_cell_title, 2);

			aq.spinText(R.id.table_cell_contant, cell.getContent(), cell.getContentSizeDelta(), true);
			aq.id(R.id.table_cell_contant).textColorId(cell.getTitleColorId()).clicked(titleSpinnerCell.getOnClick());
			aq.layoutWeight(R.id.table_cell_contant, 3);

			break;

		case Cell.CIRCLEVIEW:
			cellView = inflater.inflate(R.layout.tablecell_circleview, null);
			aq = new AAQuery(cellView);
			CircleViewCell circleViewCell = (CircleViewCell) cell;
			//Default margin/padding setting
			topPadding = extralinespace;
			leftPadding = extralinespace;
			rightPadding = extralinespace;
			botPadding = extralinespace;


			CircularProgressBar circleView = (CircularProgressBar) aq.id(R.id.tablecell_circleview_bar).getView();
			circleView.setProgress(circleViewCell.getProgress());
			circleView.setProgressColor(circleViewCell.getBarColor());

			int px20sp = (int) (20 * context.getResources().getDisplayMetrics().scaledDensity);
			int px40sp = (int) (40 * context.getResources().getDisplayMetrics().scaledDensity);

			aq.id(R.id.compliance_percentage).text(context.getResources().getString(R.string.myhkt_usage_remaining));
			aq.id(R.id.compliance_label).text(circleViewCell.getTitle());
			aq.id(R.id.compliance_percentage).textSize(context.getResources().getDimension(R.dimen.usage_low_size));
			aq.id(R.id.compliance_label).textSize(context.getResources().getDimension(R.dimen.usage_high_size));
			aq.id(R.id.compliance_label).height(ViewGroup.LayoutParams.WRAP_CONTENT);
			aq.id(R.id.compliance_label).getTextView().setIncludeFontPadding(true);
			aq.id(R.id.compliance_label).getTextView().setIncludeFontPadding(false);
//			if (!isZh) {
//				aq.id(R.id.compliance_percentage).text(context.getResources().getString(R.string.myhkt_usage_remaining));
//				aq.id(R.id.compliance_label).text(circleViewCell.getTitle());
//				aq.id(R.id.compliance_percentage).textSize(context.getResources().getDimension(R.dimen.usage_low_size));
//				aq.id(R.id.compliance_label).textSize(context.getResources().getDimension(R.dimen.usage_high_size));
//				aq.id(R.id.compliance_label).height(ViewGroup.LayoutParams.WRAP_CONTENT);
//				aq.id(R.id.compliance_label).getTextView().setIncludeFontPadding(true);
//				aq.id(R.id.compliance_label).getTextView().setIncludeFontPadding(false);
//			} else {
//				aq.id(R.id.compliance_percentage).text(context.getResources().getString(R.string.myhkt_usage_remaining));
//				aq.id(R.id.compliance_label).text(circleViewCell.getTitle());
//				aq.id(R.id.compliance_percentage).textSize(context.getResources().getDimension(R.dimen.usage_low_size));
//				aq.id(R.id.compliance_label).textSize(context.getResources().getDimension(R.dimen.usage_high_size));
//				aq.id(R.id.compliance_percentage).height(ViewGroup.LayoutParams.WRAP_CONTENT);
//				aq.id(R.id.compliance_label).getTextView().setIncludeFontPadding(true);
//				aq.id(R.id.compliance_label).getTextView().setIncludeFontPadding(false);
//			}

			aq.id(R.id.compliance_usage).text(circleViewCell.getContent());

			if(circleViewCell.getTitle().equals("")){
				aq.id(R.id.compliance_percentage).getTextView().setVisibility(View.GONE);
				aq.id(R.id.compliance_label).getTextView().setVisibility(View.GONE);
				aq.id(R.id.compliance_usage).textSize(circleViewCell.getContent().length() > 10
						? context.getResources().getDimension(R.dimen.usage_low_size) :
						context.getResources().getDimension(R.dimen.usage_high_size));
			}
			else {
				aq.id(R.id.compliance_usage).textSize(context.getResources().getDimension(R.dimen.usage_percentage_text_size));
			}

			aq.id(R.id.compliance_percentage).textColorId(circleViewCell.getTitleColorId());
			aq.id(R.id.compliance_label).textColorId(circleViewCell.getTitleColorId());
			aq.id(R.id.compliance_usage).textColorId(circleViewCell.getContentColorId());
			break;

		case Cell.DETAIL_BTN:
			int detailBtnPadding = (int) context.getResources().getDimension(R.dimen.detail_btn_padding);
			cellView = inflater.inflate(R.layout.tablecell_detailbtn, null);
			aq = new AAQuery(cellView);
			DetailBtnCell detailBtnCell = (DetailBtnCell) cell;
			//Default margin/padding setting
			topPadding = basePadding;
			leftPadding = basePadding;
			rightPadding = basePadding;
			botPadding = basePadding;
			Drawable rightDrawable = context.getResources().getDrawable(cell.getDraw());

			//Arrow part
			aq.normText(R.id.table_cell_getheight, "getArrowHeight", "".equals(detailBtnCell.getTitle()) ? detailBtnCell.getContentSizeDelta() :detailBtnCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_getheight).visibility(View.INVISIBLE).backgroundColorId(R.color.blue);
			aq.id(R.id.table_cell_getheight).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
			aq.id(R.id.table_cell_arrow).image(R.drawable.rightarrow_small).visibility(detailBtnCell.isArrowShown()? View.VISIBLE : View.GONE);
			aq.id(R.id.table_cell_arrow).getImageView().setScaleType(ScaleType.CENTER);
			aq.marginpx(R.id.table_cell_arrow, 0, 0, padding_twocol, 0);

			aq.normText(R.id.table_cell_title, detailBtnCell.getTitle(), detailBtnCell.getTitleSizeDelta());
			TextView tvTitle = aq.id(R.id.table_cell_title).getTextView();

			aq.id(R.id.table_cell_title).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).textColorId(detailBtnCell.getTitleColorId());
			if (detailBtnCell.isHasRightDrawable())
				aq.id(R.id.table_cell_title).getTextView().setCompoundDrawablesWithIntrinsicBounds(null, null, rightDrawable, null);
			else
				aq.id(R.id.table_cell_title).getTextView().setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
			aq.id(R.id.table_cell_title).getTextView().setCompoundDrawablePadding(detailBtnPadding);
			if (detailBtnCell.getLeftOnClickListener() !=null) {
				aq.id(R.id.table_cell_title).getView().setOnClickListener(detailBtnCell.getLeftOnClickListener());
			}

			aq.normText(R.id.table_cell_contant, detailBtnCell.getContent(), detailBtnCell.getContentSizeDelta());
			aq.id(R.id.table_cell_contant).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).textColorId(detailBtnCell.getContentColorId());
			aq.gravity(R.id.table_cell_contant, Gravity.RIGHT);


			aq.id(R.id.table_cell_contant).getTextView().setCompoundDrawablesWithIntrinsicBounds(null, null, rightDrawable, null);
			if (detailBtnCell.getClickArray() != null) {
				aq.id(R.id.table_cell_contant).clicked(fragment !=null ? fragment : context, detailBtnCell.getClickArray()[0]);
			}
			if (detailBtnCell.getRightOnClickListener() !=null) {
				aq.id(R.id.table_cell_contant).getView().setOnClickListener(detailBtnCell.getRightOnClickListener());
			}
			aq.id(R.id.table_cell_contant).getTextView().setCompoundDrawablePadding(detailBtnPadding);
			rightDrawable = null;
			break;

		case Cell.BTNS3:
			cellView = inflater.inflate(R.layout.tablecell_btns3, null);
			aq = new AAQuery(cellView);

			//Default margin/padding setting
			topPadding = extralinespace;
			leftPadding = extralinespace;
			rightPadding = extralinespace;
			botPadding = extralinespace;

			aq.id(R.id.table_cell_layout_left_btn_icon).image(cell.getImageArray()[0]);
			aq.id(R.id.table_cell_layout_mid_btn_icon).image(cell.getImageArray()[1]);
			aq.id(R.id.table_cell_layout_right_btn_icon).image(cell.getImageArray()[2]);

			//			aq.id(R.id.table_cell_layout_left_btn_icon).getView().setPadding(0, toppadding/2, 0, 0);
			aq.id(R.id.table_cell_layout_left_btn_text).text(cell.getTextArray()[0]).textColorId(R.color.hkt_textcolor).getTextView().setGravity(Gravity.CENTER);
			//			aq.id(R.id.table_cell_layout_left_btn_text).getView().setPadding(0, 0, 0, bottompadding/2);
			aq.id(R.id.table_cell_layout_left_btn).clicked(fragment !=null ? fragment : context, cell.getClickArray()[0]).background(R.drawable.hover_selector);
			((LinearLayout)aq.id(R.id.table_cell_layout_left_btn).getView()).setGravity(Gravity.CENTER);

			//			aq.id(R.id.table_cell_layout_mid_btn_icon).getView().setPadding(0, toppadding/2, 0, 0);
			aq.id(R.id.table_cell_layout_mid_btn_text).text(cell.getTextArray()[1]).textColorId(R.color.hkt_textcolor).getTextView().setGravity(Gravity.CENTER);
			//			aq.id(R.id.table_cell_layout_mid_btn_text).getView().setPadding(0, 0, 0, bottompadding/2);
			aq.id(R.id.table_cell_layout_mid_btn).clicked(fragment !=null ? fragment : context, cell.getClickArray()[1]).background(R.drawable.hover_selector);
			((LinearLayout)aq.id(R.id.table_cell_layout_mid_btn).getView()).setGravity(Gravity.CENTER);

			//			aq.id(R.id.table_cell_layout_right_btn_icon).getView().setPadding(0, toppadding/2, 0, 0);
			aq.id(R.id.table_cell_layout_right_btn_text).text(cell.getTextArray()[2]).textColorId(R.color.hkt_textcolor).getTextView().setGravity(Gravity.CENTER);
			//			aq.id(R.id.table_cell_layout_right_btn_text).getView().setPadding(0, 0, 0, bottompadding/2);
			aq.id(R.id.table_cell_layout_right_btn).clicked(fragment !=null ? fragment : context, cell.getClickArray()[2]).background(R.drawable.hover_selector);
			((LinearLayout)aq.id(R.id.table_cell_layout_right_btn).getView()).setGravity(Gravity.CENTER);
			break;
		case Cell.TWOTEXTBTN:
			cellView = inflater.inflate(R.layout.tablecell_twotextbtn, null);
			aq = new AAQuery(cellView);
			TwoTextBtnCell twoTextBtnCell = (TwoTextBtnCell) cell;

			//Default margin/padding setting
			leftPadding = basePadding;
			rightPadding = basePadding;


			//Set Title
			aq.normText(R.id.table_cell_title, twoTextBtnCell.getTitle(), twoTextBtnCell.getTitleSizeDelta());
			aq.id(R.id.table_cell_title).width(ViewGroup.LayoutParams.WRAP_CONTENT).height(ViewGroup.LayoutParams.WRAP_CONTENT).textColorId(twoTextBtnCell.getTitleColorId());
			aq.gravity(R.id.table_cell_title, Gravity.BOTTOM);
			aq.layoutWeight(R.id.table_cell_title, 1);
			//Set Content
			aq.normText(R.id.table_cell_content, twoTextBtnCell.getContent(), twoTextBtnCell.getContentSizeDelta());
			aq.id(R.id.table_cell_content).width(ViewGroup.LayoutParams.WRAP_CONTENT).height(ViewGroup.LayoutParams.WRAP_CONTENT).textColorId(twoTextBtnCell.getContentColorId());
			aq.gravity(R.id.table_cell_title, Gravity.TOP);
			aq.layoutWeight(R.id.table_cell_content, 1);

			aq.id(R.id.table_cell_text_layout).height((int) context.getResources().getDimension(R.dimen.textviewheight), false);
			//			aq.layoutWeight(R.id.table_cell_text_layout, 1);

			//Set Button

			final HKTButton twoTextBtn = (HKTButton) aq.id(R.id.table_cell_btn).getView();
			aq.normTxtBtn(twoTextBtnCell.getBtnSizeDelta(), R.id.table_cell_btn, twoTextBtnCell.getBtnText(), twoTextBtnCell.getBtnWidth());
			if(twoTextBtnCell.getOnClickListener() != null)
				aq.id(R.id.table_cell_btn).clicked(twoTextBtnCell.getOnClickListener());
			else if(twoTextBtnCell.getClickArray() != null)
				aq.id(R.id.table_cell_btn).clicked(fragment, twoTextBtnCell.getClickArray()[0]);
			if (draw != null) {
				twoTextBtn.setDrawable(draw);
			} else if (twoTextBtnCell.getDraw() > 0) {
				int imageH ;
				int imageW ;
				int bh = (int) context.getResources().getDimension(R.dimen.buttonblue_height);
				Drawable drawble = context.getResources().getDrawable(twoTextBtnCell.getDraw());
				int h = drawble.getIntrinsicHeight();
				int w = drawble.getIntrinsicWidth();

				if (("").equals(twoTextBtnCell.getTitle())) {
					imageH = bh - extralinespace;
				} else {
					if (twoTextBtnCell.getDraw() == R.drawable.btn_mail) {
						imageH = bh / 3;
					} else {
						imageH = bh - padding_twocol *3;
					}
				}
				imageW = w * (imageH) / h;
				drawble.setBounds( 0, 0, imageW, imageH);
				twoTextBtn.setDrawable(drawble);
			}


			//			} else {
			//				twoTextBtn.setEnabled(false);
			//				twoTextBtn.setVisibility(View.GONE);
			//			}

			//Configure the container
			//			aq.id(R.id.table_cell_layout).getView().setPadding(extralinespace, extralinespace, extralinespace, extralinespace);
			break;
		case Cell.SPINNERTEXT:
			cellView = inflater.inflate(R.layout.tablecell_spinnertext, null);
			aq = new AAQuery(cellView);
			SpinnerTextCell spinnerTextCell = (SpinnerTextCell) cell;

			//Default margin/padding setting
			leftPadding = basePadding;
			rightPadding = basePadding;

			aq.spinText(R.id.table_cell_title, spinnerTextCell.getTitle(), false);
			if (spinnerTextCell.getOnClickListener() != null) {
				aq.id(R.id.table_cell_title).clicked(spinnerTextCell.getOnClickListener());
			} else if (spinnerTextCell.getClickArray() != null && spinnerTextCell.getClickArray().length > 0){
				aq.id(R.id.table_cell_title).clicked(fragment, spinnerTextCell.getClickArray()[0]);
			}

			aq.id(R.id.table_cell_title).background(0);

			break;

		case Cell.MY_MESSAGE_TITLE:
			cellView = inflater.inflate(R.layout.view_email_list_item_title, null);
			aq = new AAQuery(cellView);
			EmailParentCell parentCell = (EmailParentCell) cell;
			aq.id(R.id.text_email_title).getTextView().setText(parentCell.getParentTitle());

			break;

		case Cell.MY_MESSAGE_CHILD:
			cellView = inflater.inflate(R.layout.view_email_list_item_content, null);
			aq = new AAQuery(cellView);

			EmailChildCell childCell = (EmailChildCell) cell;
			aq.id(R.id.email_img_category).getImageView().setImageDrawable(context.getResources().getDrawable(MessageCategory.getMessageCategoryIcon(childCell.getCategory())));
			aq.id(R.id.email_text_date).getTextView().setText(childCell.getDate());
			aq.id(R.id.email_text_title).getTextView().setText(childCell.getTitle());
			// aq.id(R.id.email_text_content).getTextView().setText(childCell.getContent());
			if(!(childCell.getContent() != null && !TextUtils.isEmpty(childCell.getPath().trim()))) {
				aq.id(R.id.email_text_content).getTextView().setText(childCell.getContent());
			}
			/*support for html emails*/
			// aq.id(R.id.email_text_content).getTextView().setText(Html.fromHtml(childCell.getContent()));

			// checking if pathZn or pathEn, if not empty, include to Tap here to see more
//			if(childCell.getContent() != null && !TextUtils.isEmpty(childCell.getPath().trim())) {
//				aq.id(R.id.email_text_content).getTextView().setText(context.getResources().getString(R.string.inbox_image_message));
//			}

			aq.id(R.id.email_text_remaining).getTextView().setText(childCell.getRemaining());

			if(!childCell.isRead()) {
				aq.id(R.id.inbox_unread).getView().setVisibility(View.VISIBLE);
				aq.id(R.id.email_text_title).getTextView().setTypeface(aq.id(R.id.email_text_title).getTextView().getTypeface(), Typeface.BOLD);
			}
			else{
				aq.id(R.id.email_img_category).getImageView().setAlpha(0.7f);
				aq.id(R.id.email_text_title).textColor(Color.parseColor("#7F7F7F"));
			}

			if(childCell.isLastItem()) {
				aq.id(R.id.email_view_line).getView().setVisibility(View.GONE);
				aq.id(R.id.email_view_footer).getView().setVisibility(View.VISIBLE);
			}

			aq.id(R.id.email_cell_content).getView().setOnClickListener(childCell.getOnClickListener());
			break;


		}

		//if (aq!=null && (cell.getType()!=Cell.MY_MESSAGE_TITLE || cell.getType()!=Cell.MY_MESSAGE_CHILD)) {
			if (cell.getLeftMargin() !=-1) leftMargin = cell.getLeftMargin();
			if (cell.getRightMargin() !=-1) rightMargin = cell.getRightMargin();
			if (cell.getTopMargin() !=-1) topMargin = cell.getTopMargin();
			if (cell.getBotMargin() !=-1) botMargin = cell.getBotMargin();
			if (cell.getLeftPadding() !=-1) leftPadding = cell.getLeftPadding();
			if (cell.getRightPadding() !=-1) rightPadding = cell.getRightPadding();
			if (cell.getTopPadding() !=-1) topPadding = cell.getTopPadding();
			if (cell.getBotPadding() !=-1) botPadding = cell.getBotPadding();
//						Log.i("CellViewMargin" , leftMargin + "/" + topMargin+ "/" +  rightMargin+ "/" + botMargin);
//						Log.i("CellViewPadding" , leftPadding + "/" + topPadding+ "/" +  rightPadding+ "/" + botPadding);
			aq.marginpx(R.id.table_cell_layout, leftMargin, topMargin, rightMargin, botMargin);
			aq.padding(R.id.table_cell_layout, leftPadding, topPadding, rightPadding, botPadding);
			aq.id(R.id.table_cell_layout).backgroundColorId(cell.getBgcolorId());
			aq.id(R.id.table_cell_layout).visibility(cell.getIsVisible() ? View.VISIBLE : View.GONE);
		//}

		return cellView;

	}

	public int getImage(String imageName) {

		int drawableResourceId = context.getResources().getIdentifier(imageName, "drawable", context.getPackageName());

		return drawableResourceId;
	}

	public HKTButton processHktButton(AAQuery aq, int btnWidth, String title,
									  View.OnClickListener onClickListener, String onClickCallback,
									  Drawable drawable, int drawableRes, int cellLayoutId) {

Log.d("processHktButton", "onClickListener:" + onClickListener + " onClickCallback:" + onClickCallback.replaceAll("\\s",""));
		HKTButton firstHktBtn = aq.normTxtBtn(cellLayoutId, title, btnWidth, btnWidth, HKTButton.TYPE_BLUE);
		int btnImageDimens = btnWidth - ((int) context.getResources().getDimension(R.dimen.four_btn_margin));
		//change theme for MyMob
		if (ClnEnv.isMyMobFlag()) {
			int btnType = HKTButton.TYPE_ORANGE;
			if (ClnEnv.isIs101Flag()) {
				firstHktBtn.setType(HKTButton.TYPE_BLACK);
			} else if (SubnRec.LOB_CSP.equalsIgnoreCase(lobString) ||  SubnRec.WLOB_XCSP.equalsIgnoreCase(lobString)) {
				firstHktBtn.setType(HKTButton.TYPE_PURPLE);
			} else {
				firstHktBtn.setType(btnType);
			}
		}
		aq.gravity(cellLayoutId, Gravity.CENTER);
		if (onClickListener != null) {
			aq.id(cellLayoutId).clicked(onClickListener);
		} else if (onClickCallback != null) {
			aq.id(cellLayoutId).clicked(fragment != null ? fragment : context, onClickCallback.replaceAll("\\s", ""));
		}
		if (drawable != null) {
			firstHktBtn.setDrawable(drawable);
		} else if (drawableRes > 0) {

			Drawable drawble = context.getResources().getDrawable(drawableRes);
			drawble.setBounds(0, 0, btnImageDimens, btnImageDimens);
			firstHktBtn.setDrawable(drawble);
		} else {
			if (("").equals(title)) {
				firstHktBtn.setVisibility(View.INVISIBLE);
			}
		}

		int marginLeft = cellLayoutId == R.id.table_cell_layout_btns4_first ? 0 : (int) context.getResources().getDimension(R.dimen.four_btn_left_margin);
		aq.marginpx(cellLayoutId, marginLeft, 0, (int) context.getResources().getDimension(R.dimen.four_btn_right_margin), 0);

		return firstHktBtn;
	}

	public HKTButton processHktButtonButSmaller(AAQuery aq, int btnWidth, String title,
									  View.OnClickListener onClickListener, String onClickCallback,
									  Drawable drawable, int drawableRes, int cellLayoutId) {

		Log.d("processHktButton", "onClickListener:" + onClickListener + " onClickCallback:" + onClickCallback.replaceAll("\\s",""));
		HKTButton firstHktBtn = aq.normTxtBtn(cellLayoutId, title,btnWidth, btnWidth, HKTButton.TYPE_BLUE);
		int btnImageDimens = btnWidth - ((int) context.getResources().getDimension(R.dimen.four_btn_margin));
		//change theme for MyMob
		if (ClnEnv.isMyMobFlag()) {
			int btnType = HKTButton.TYPE_ORANGE;
			if (ClnEnv.isIs101Flag()) {
				btnType = HKTButton.TYPE_BLACK;
			}
			if (SubnRec.LOB_CSP.equalsIgnoreCase(lobString) ||  SubnRec.WLOB_XCSP.equalsIgnoreCase(lobString)) {
				firstHktBtn.setType(HKTButton.TYPE_PURPLE);
			} else {
				firstHktBtn.setType(btnType);
			}
			Log.d("lwg", "processHktButtonButSmaller: " + lobString);
		}
		aq.gravity(cellLayoutId, Gravity.CENTER);
		if (onClickListener != null) {
			aq.id(cellLayoutId).clicked(onClickListener);
		} else if (onClickCallback != null) {
			aq.id(cellLayoutId).clicked(fragment !=null ? fragment : context, onClickCallback.replaceAll("\\s",""));
		}
		if (drawable != null) {
			firstHktBtn.setDrawable(drawable);
		} else if (drawableRes >0 ) {

			Drawable drawble = context.getResources().getDrawable(drawableRes);
			drawble.setBounds( 0, 0, btnImageDimens, btnImageDimens);
			firstHktBtn.setDrawable(drawble);
		} else {
			if (("").equals(title)) {
				firstHktBtn.setVisibility(View.INVISIBLE);
			}
		}

		int marginLeft =  cellLayoutId == R.id.table_cell_layout_btns4_first ? 0 : (int) context.getResources().getDimension(R.dimen.four_btn_left_margin);
		aq.marginpx(cellLayoutId, marginLeft, 0 , (int) context.getResources().getDimension(R.dimen.four_btn_right_margin), 0);

		return firstHktBtn;

	}
}
