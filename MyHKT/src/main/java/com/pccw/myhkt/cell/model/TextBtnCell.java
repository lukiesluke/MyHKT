package com.pccw.myhkt.cell.model;

import com.pccw.myhkt.R;

import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

public class TextBtnCell extends Cell{

	private OnClickListener onClickListener = null;
	private int btnWidth = ViewGroup.LayoutParams.MATCH_PARENT;	
	private int titleGravity = Gravity.LEFT|Gravity.CENTER;
	private int titleHeight = 0;



	public TextBtnCell(String title, String content, int drawId, String[] clickArray) {
		super();
		type = Cell.TEXTBTN;
		
		titleColorId = R.color.hkt_txtcolor_grey;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;
		
		this.title = title;
		this.content = content;
		this.clickArray = clickArray;
		this.draw = drawId;
	}
	
	public TextBtnCell(OnClickListener onClickListener, String title, String content, int drawId) {
		super();
		type = Cell.TEXTBTN;
		
		titleColorId = R.color.hkt_txtcolor_grey;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;
		
		this.title = title;
		this.content = content;
		this.onClickListener = onClickListener;
		this.draw = drawId;
	}
	
	public int getBtnWidth() {
		return btnWidth;
	}
	public void setBtnWidth(int btnWidth) {
		this.btnWidth = btnWidth;
	}	
	public OnClickListener getOnClickListener() {
		return onClickListener;
	}

	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

	public int getTitleGravity() {
		return titleGravity;
	}

	public void setTitleGravity(int titleGravity) {
		this.titleGravity = titleGravity;
	}
	
	public int getTitleHeight() {
		return titleHeight;
	}
	public void setTitleHeight(int titleHeight) {
		this.titleHeight = titleHeight;
	}
}