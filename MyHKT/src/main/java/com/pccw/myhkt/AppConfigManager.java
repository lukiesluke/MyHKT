package com.pccw.myhkt;

import static com.pccw.myhkt.util.Constant.ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD;
import static com.pccw.myhkt.util.Constant.KEY_SHARED_APP_CONFIG;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.pccw.myhkt.listeners.AppConfigListener;
import com.pccw.myhkt.model.AppConfig;
import com.pccw.myhkt.model.AppConfigImage;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.FileManager;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/************************************************************************
 * File : AppConfigManager.java
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 29/09/2017 Abdulmoiz Esmail  - Saved the banner url into shared pref from the app_config
 * 08/03/2018 Abdulmoiz Esmail  - Added broadcasting when download is finish
 *************************************************************************/
public class AppConfigManager {
    private static boolean debug = false;    // toggle debug logs
    private final Context cxt;
    private static AppConfigManager appConfigManager = null;
    private static AppConfigListener appconfiglistener;
    private String PATH = null;  //app config storage path
    private String HOST = null; // CSP HOST path
    private String livechat_consumer_flag = "";
    private String livechat_premier_flag = "";
    private String banner_url_en = "";
    private String banner_url_zh = "";
    private String banner_after_url_en = "";
    private String banner_after_url_zh = "";
    private String banner_url_pt_en = "";
    private String banner_url_pt_zh = "";
    private int sizeList;

    private AppConfigManager(Context context) {
        cxt = context;
        debug = context.getResources().getBoolean(R.bool.DEBUG);
        PATH = context.getResources().getString(R.string.PATH);  //app config storage path
//		HOST = context.getResources().getString(R.string.HOST);  //image download path
        HOST = ClnEnv.getPref(context, context.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP); //image download path
    }

    public static synchronized AppConfigManager getInstance(Context context) {
        if (appConfigManager == null) {
            appConfigManager = new AppConfigManager(context);
        }
        return appConfigManager;
    }

    public void setAppConfigListener(AppConfigListener listener) {
        PATH = cxt.getResources().getString(R.string.PATH);  //app config storage path
        HOST = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP); //image download path
        AppConfigManager.appconfiglistener = listener;
    }

    public void downloadAppConfigImages() {
        new AppDataAsyncTask().execute(cxt);
    }

    //App config Json download async task
    public class AppDataAsyncTask extends AsyncTask<Context, Void, String> {

        public AppDataAsyncTask() {

        }

        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(Context... params) {
//            String endPoint = "/mba/data/app_config.json";
//            if ("OK".equalsIgnoreCase(DownloadFromUrl(PATH, HOST + endPoint, "app_config.json"))) {
//                ClnEnv.setAppConfigDownloaded(true);
//            } else {
//                ClnEnv.setAppConfigDownloaded(false);
//            }
            try {
                Gson gson = new Gson();
                String appConfigSetting = ClnEnv.getPref(cxt, KEY_SHARED_APP_CONFIG, FileManager.getAppConfigJson(cxt));
                AppConfig appConfig = gson.fromJson(appConfigSetting, AppConfig.class);

                // Create image directories if they are not exist
                File imgDir = new File(PATH + "images/en");
                File zhImgDir = new File(PATH + "images/zh");
                if (!imgDir.exists()) {
                    imgDir.mkdirs();
                }
                if (!zhImgDir.exists()) {
                    zhImgDir.mkdirs();
                }

                sizeList = appConfig.getImageList().size();
                // Download imageHKT Android
                for (AppConfigImage i : appConfig.getImageList()) {
                    if (i.getEnImg() != null && !i.getEnImg().isEmpty()) {
                        new ImageAsyncTask().execute(PATH + "images/en/", HOST + "/mba/".concat(i.getEnImg()), i.getKey().concat(".png"));
                    }
                    if (i.getZhImg() != null && !i.getZhImg().isEmpty()) {
                        new ImageAsyncTask().execute(PATH + "images/zh/", HOST + "/mba/".concat(i.getZhImg()), i.getKey().concat(".png"));
                    }
                }

                livechat_consumer_flag = appConfig.getCommonConfig().getLivechat_consumer_flag();
                livechat_premier_flag = appConfig.getCommonConfig().getLivechat_premier_flag();

                if (appConfig.getCommonConfig().getBannerUrlEn() != null && appConfig.getCommonConfig().getBannerUrlZh() != null) {
                    banner_url_en = appConfig.getCommonConfig().getBannerUrlEn();
                    banner_url_zh = appConfig.getCommonConfig().getBannerUrlZh();
                }

                if (appConfig.getCommonConfig().getBannerAfterUrlEn() != null && appConfig.getCommonConfig().getBannerAfterUrlZh() != null) {
                    banner_after_url_en = appConfig.getCommonConfig().getBannerAfterUrlEn();
                    banner_after_url_zh = appConfig.getCommonConfig().getBannerAfterUrlZh();
                }

                if (appConfig.getCommonConfig().getBannerUrlPremierEn() != null && appConfig.getCommonConfig().getBannerUrlPremierZh() != null) {
                    banner_url_pt_en = appConfig.getCommonConfig().getBannerUrlPremierEn();
                    banner_url_pt_zh = appConfig.getCommonConfig().getBannerUrlPremierZh();
                }

                if (appConfig.getCommonConfig().getShow_fingerprint_banner() != null) {
                    ClnEnv.setPref(cxt.getApplicationContext(), Constant.IS_SERVER_TO_SHOW_FP_BANNER, appConfig.getCommonConfig().getShow_fingerprint_banner());
                }
                return "success";
            } catch (IOException e) {
                e.printStackTrace();
                return "failed";
            }
        }

        protected void onPostExecute(String status) {
            if ("success".equalsIgnoreCase(status)) {
                ClnEnv.setAppConfigDownloaded(true);
                ClnEnv.setPref(cxt.getApplicationContext(), "livechat_consumer_flag", "Y".equalsIgnoreCase(livechat_consumer_flag));
                ClnEnv.setPref(cxt.getApplicationContext(), "livechat_premier_flag", "Y".equalsIgnoreCase(livechat_premier_flag));
                ClnEnv.setPref(cxt.getApplicationContext(), "banner_url_en", banner_url_en);
                ClnEnv.setPref(cxt.getApplicationContext(), "banner_url_zh", banner_url_zh);
                ClnEnv.setPref(cxt.getApplicationContext(), "banner_after_url_en", banner_after_url_en);
                ClnEnv.setPref(cxt.getApplicationContext(), "banner_after_url_zh", banner_after_url_zh);
                ClnEnv.setPref(cxt.getApplicationContext(), "banner_url_pt_en", banner_url_pt_en);
                ClnEnv.setPref(cxt.getApplicationContext(), "banner_url_pt_zh", banner_url_pt_zh);
                // Broadcast to the main activity to update the appConfig
//                 sendBroadCastMessage(ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD);
            }
        }
    }

    public class ImageAsyncTask extends AsyncTask<String, String, String> {

        public ImageAsyncTask() {
        }

        @Override
        protected String doInBackground(String... params) {
            if ("OK".equalsIgnoreCase(DownloadFromUrl(params[0], params[1], params[2]))) {
                ClnEnv.setAppConfigDownloaded(true);
                return "success";
            } else {
                ClnEnv.setAppConfigDownloaded(false);
                return "failed";
            }
        }

        protected void onPostExecute(String result) {
            // Broadcast to the main activity to reload the banner
            sendBroadCastMessage(ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD);
        }
    }

    public String DownloadFromUrl(String storagePath, String fileURL, String fileName) {  //this is the downloader method
        String status = "OK";
        FileOutputStream fileOutputStream;
        try {
            URL url = new URL(fileURL);
            File file = new File(storagePath + fileName);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            long date = httpCon.getLastModified();
            boolean doDownload = false;

            if (200 != httpCon.getResponseCode()) {
                return "FAILED";
            }

            Date serverFileLastModified = new Date(date);
            if (date == 0) {
                System.out.println("No last-modified information.");
                status = "FAILED";
            } else {
                System.out.println("Last-Modified: " + serverFileLastModified);
                if (file.exists()) {
                    Date lastModified = new Date(file.lastModified());
                    if (lastModified.compareTo(serverFileLastModified) != 0) {
                        doDownload = true;
                    } else {
                        doDownload = false;
                        if (debug) Log.e("AppConfigManager", "File :" + fileName + " Same");
                    }
                } else {
                    doDownload = true;
                }
            }

            if (!ClnEnv.isAppConfigDownloaded()) {
                doDownload = true;
            }

            if (doDownload) {
                URLConnection urlConn = url.openConnection();
                InputStream is = urlConn.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayBuffer baf = new ByteArrayBuffer(50);
                int current = 0;
                while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
                }
                fileOutputStream = new FileOutputStream(file, false);
                fileOutputStream.write(baf.toByteArray());
                fileOutputStream.flush();

                file.setReadable(true, false);
                file.setWritable(true, false);
                file.setLastModified(date);
                fileOutputStream.close();
                is.close();

                Log.e("lwg", "File downloaded true:" + fileName);
            }
            return status;
        } catch (IOException e) {
            e.printStackTrace();
            status = "FAILED";
            return status;
        }
    }

    public void copyJsonFromAsset(Context ctx, String fileName) {
        File file = new File(PATH + fileName);
        if (!file.exists()) {
            try {
                InputStream is = ctx.getAssets().open(fileName);
                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayBuffer baf = new ByteArrayBuffer(50);
                int current = 0;
                while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
                }
                FileOutputStream fos = new FileOutputStream(file, false);
                fos.write(baf.toByteArray());
                fos.close();
                if (debug) Log.d("AppConfigManager", "File Copy Complete");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String loadJson(Context ctx, String fileName) throws IOException {
        File file = new File(PATH + fileName);
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        int size = in.available();

        byte[] buffer = new byte[size];

        in.read(buffer);
        in.close();
        return new String(buffer, StandardCharsets.UTF_8);
    }

    private void sendBroadCastMessage(String actionSignal) {
        Intent intent = new Intent();
        intent.setAction(actionSignal);
        intent.setPackage(cxt.getPackageName());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            PendingIntent.getBroadcast(cxt, 0, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            PendingIntent.getBroadcast(cxt, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        }
        cxt.sendBroadcast(intent);
    }
}
