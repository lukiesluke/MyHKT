package com.pccw.myhkt.listeners;

public interface OnLogoutListener {
    void onLogout();
}
