/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.pccw.myhkt.touchId;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.util.Constant;

/**
 * Small helper class to manage text/icon around fingerprint authentication UI.
 */
public class TouchIdUiHelper extends FingerprintManager.AuthenticationCallback {

    private static final String TAG = TouchIdUiHelper.class.getSimpleName();

    private static final long ERROR_TIMEOUT_MILLIS = 1600;
    private static final long SUCCESS_DELAY_MILLIS = 1300;

    private final FingerprintManager mFingerprintManager;
    private final ImageView mIcon;
    private final TextView mErrorTextView, mTitleTextView;
    private final Callback mCallback;
    private CancellationSignal mCancellationSignal;
    private Context mContext;

    private boolean mSelfCancelled;
    
    
    private boolean mIsLogin;

    /**
     * Constructor for {@link TouchIdUiHelper}.
     */
    TouchIdUiHelper(FingerprintManager fingerprintManager,
                    ImageView icon, TextView titleTextView, TextView errorTextView, Callback callback, Context context, boolean isLogin) {
        mFingerprintManager = fingerprintManager;
        mIcon = icon;
        mErrorTextView = errorTextView;
        mTitleTextView = titleTextView;
        mCallback = callback;
        mContext = context;
        mIsLogin = isLogin;
        
        if(!isLogin) {
        	mErrorTextView.setText(mContext.getString(R.string.fp_activation_reason_msg));
        }
    }

    public boolean isFingerprintAuthAvailable() {
        // The line below prevents the false positive inspection from Android Studio
        // noinspection ResourceType
        return mFingerprintManager.isHardwareDetected()
                && mFingerprintManager.hasEnrolledFingerprints();
    }

    public void startListening(FingerprintManager.CryptoObject cryptoObject) {
        if (!isFingerprintAuthAvailable()) {
            return;
        }
        mCancellationSignal = new CancellationSignal();
        mSelfCancelled = false;
        // The line below prevents the false positive inspection from Android Studio
        // noinspection ResourceType
        mFingerprintManager
                .authenticate(cryptoObject, mCancellationSignal, 0 /* flags */, this, null);
        mIcon.setImageResource(R.drawable.ic_cfingerprint_icon_fp);
    }

    public void stopListening() {
        if (mCancellationSignal != null) {
            mSelfCancelled = true;
            mCancellationSignal.cancel();
            mCancellationSignal = null;
        }
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
    	Log.d(TAG, "onAuthenticationError: " + errMsgId + " Message: " + errString);
    	if(errMsgId == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
    		mCallback.onFingerprintLockout(errString.toString());
    	} else if(errMsgId == FingerprintManager.FINGERPRINT_ERROR_CANCELED) {
    		mCallback.onFingerprintError(errMsgId);
    	} else {
    		showError(errString, false, errMsgId);
    	}

    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
    	Log.d(TAG, "onAuthenticationHelp: " + helpMsgId + " Message: " + helpString);
    	if(helpMsgId == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
    	} else if(helpMsgId == FingerprintManager.FINGERPRINT_ERROR_CANCELED) {
    		mCallback.onFingerprintError(helpMsgId);
    	} else {
    		showError(helpString, false, helpMsgId);
    	}
    }

    @Override
    public void onAuthenticationFailed() {
    	if(mIsLogin) {
	        showError(mIcon.getResources().getString(
	                R.string.fp_login_reason_msg), true, -1);
    	} else {
    		showError(mIcon.getResources().getString(
	                R.string.fp_activation_reason_msg), true, -1);
    	}
    	
    }
    
    

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        mErrorTextView.removeCallbacks(mResetErrorTextRunnable);
        
        if(mContext instanceof LoginActivity) {
        	mErrorTextView.setText(mContext.getString(R.string.fp_login_reason_msg));
        } else {
        	mErrorTextView.setText(mContext.getString(R.string.fp_activation_reason_msg));
        }

        mCallback.onAuthenticated();
    }

    private void showError(CharSequence error, boolean isAuthFailed, int messId) {
    	
    	if(messId == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
    		mCallback.onFingerprintLockout(error.toString());
    	} else if(messId == FingerprintManager.FINGERPRINT_ERROR_CANCELED) {
    		mCallback.onFingerprintError(messId);
    	} else {
    		Log.d(TAG, "Erro: " + messId + " Message: " + error);

	    	mErrorTextView.setText(error);
	        mTitleTextView.setText(mContext.getString(R.string.fp_login_try_again_title));
	        mErrorTextView.removeCallbacks(mResetErrorTextRunnable);
	        mErrorTextView.postDelayed(mResetErrorTextRunnable, ERROR_TIMEOUT_MILLIS);
	        if(isAuthFailed) {

		        int touchIdFailureCount = ClnEnv.getPref(mContext, Constant.TOUCH_ID_FAILURE_COUNT, 0) +1;
		        Log.d(TAG, "TOUCH_ID_FAILURE_COUNT: " + touchIdFailureCount);
		        
		        ClnEnv.setPref(mContext, Constant.TOUCH_ID_FAILURE_COUNT, touchIdFailureCount);
		        mCallback.onError();
		        
	        }    

    	}
    }

    private Runnable mResetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {
            mErrorTextView.setTextColor(
                    mErrorTextView.getResources().getColor(R.color.black, null));
        }
    };

    public interface Callback {

        void onAuthenticated();
        void onError();
        void onFingerprintLockout(String error);
        void onFingerprintError(int messId);
    }
}
