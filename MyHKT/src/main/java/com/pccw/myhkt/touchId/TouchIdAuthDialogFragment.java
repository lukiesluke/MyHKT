/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.pccw.myhkt.touchId;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.util.Constant;

/************************************************************************
 * File : TouchIdAuthDialogFragment.java 
 * Desc : A dialog which uses fingerprint APIs to authenticate the user, and falls back to password
 *        authentication if fingerprint is not available.
 * Name : TouchIdAuthDialogFragment
 * by : Abdulmoiz Esmail 
 * Date : 19/10/2017
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 19/10/2017 Abdulmoiz Esmail  - First Draft
 *************************************************************************/
public class TouchIdAuthDialogFragment extends DialogFragment
        implements  TouchIdUiHelper.Callback {

    private static final String IS_LOGIN_KEY = "is_to_login";

    private Button mCancelButton;
    private Button mEnterPassword;
 
    private boolean mIsLogin;

    private FingerprintManager.CryptoObject mCryptoObject;
    private TouchIdUiHelper mFingerprintUiHelper;

    private OnTouchIdActivityListener mOnTouchIdActivityListener;

    public TouchIdAuthDialogFragment() {

    }

    public static TouchIdAuthDialogFragment onInstance(boolean isLogin) {

        TouchIdAuthDialogFragment fragment = new TouchIdAuthDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_LOGIN_KEY, isLogin);
        fragment.setArguments(bundle);

        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Do not create a new Fragment when the Activity is re-created such as orientation changes.
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);

        mIsLogin = getArguments().getBoolean(IS_LOGIN_KEY);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fingerprint_dialog_container, container, false);

        mCancelButton = (Button) v.findViewById(R.id.cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        mEnterPassword = (Button) v.findViewById(R.id.enter_password_button);
        mEnterPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnTouchIdActivityListener.onEnterPassword();
                dismiss();
            }
        });

        mFingerprintUiHelper = new TouchIdUiHelper(
                getActivity().getSystemService(FingerprintManager.class),
                (ImageView) v.findViewById(R.id.fingerprint_icon),
                (TextView) v.findViewById(R.id.fingerprint_title), 
                (TextView) v.findViewById(R.id.fingerprint_description),
                this, 
                getActivity(), 
                mIsLogin);



        //ADED 
        int failCount = ClnEnv.getPref(getActivity(), Constant.TOUCH_ID_FAILURE_COUNT, 0);
        if(failCount < 3 && failCount > 0 && getActivity() instanceof LoginActivity) {
            mEnterPassword.setVisibility(View.VISIBLE);
        } else {
            mEnterPassword.setVisibility(View.GONE);
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
            mFingerprintUiHelper.startListening(mCryptoObject);
    }


    @Override
    public void onPause() {
        super.onPause();
        mFingerprintUiHelper.stopListening();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mOnTouchIdActivityListener = (OnTouchIdActivityListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    " must implement OnDashboardFragmentListener");
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mOnTouchIdActivityListener = (OnTouchIdActivityListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                    " must implement OnDashboardFragmentListener");
        }
    }


    /**
     * Sets the crypto object to be passed in when authenticating with fingerprint.
     */
    public void setCryptoObject(FingerprintManager.CryptoObject cryptoObject) {
        mCryptoObject = cryptoObject;
    }


    @Override
    public void onAuthenticated() {
        // Callback from TouchIdUiHelper. Let the activity know that authentication was
        // successful.
        mOnTouchIdActivityListener.onTouchIdSuccess();
        dismiss();
    }

    @Override
    public void onError() {
    	
    	//show only the EnterPassword if on the login page
    	if(getActivity() instanceof LoginActivity) {
    		mEnterPassword.setVisibility(View.VISIBLE);
    	}
        int touchIdFailureCount = ClnEnv.getPref(getActivity(), Constant.TOUCH_ID_FAILURE_COUNT, 0);
        if(ClnEnv.getPref(getActivity(), Constant.TOUCH_ID_FAILURE_COUNT, 0) == 3) {
            mFingerprintUiHelper.stopListening();
            dismiss();
            mOnTouchIdActivityListener.onTouchIdFailed(touchIdFailureCount);
        }
    }
    
    @Override
    public void onFingerprintLockout(String error) {
    	mFingerprintUiHelper.stopListening();
        dismiss();
        mOnTouchIdActivityListener.onFingerprintLockout(error);
    }
    
    @Override
    public void onFingerprintError(int messId) {
    	if(messId == FingerprintManager.FINGERPRINT_ERROR_CANCELED) {
//    		mFingerprintUiHelper.stopListening();
            dismissAllowingStateLoss();
            mOnTouchIdActivityListener.onFingerprintError(messId);
    	}
    }

}
