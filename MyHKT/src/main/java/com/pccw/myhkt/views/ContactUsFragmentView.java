package com.pccw.myhkt.views;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface ContactUsFragmentView extends MvpView {

    void onTriggeredLiveChat(int stringId);

    void onTriggeredCall(String phone);

    void onTriggeredEmail(String email);

    void onTriggeredFacebook();

    void onTriggeredInstagram();

    void onTriggeredWhatsApp(String mobile);
}
