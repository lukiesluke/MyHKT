package com.pccw.myhkt.views;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface IDDNewsView extends MvpView {
    void doPopupDialog(String title, String content);
}
