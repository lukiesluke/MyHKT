package com.pccw.myhkt.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MyApptListViewAdapter extends BaseAdapter {
    private final GnrlAppt[] gnrlApptAry;
    private final SrvReq[] mySRApptAry;
    private MyApptAgent myApptAgentAry[] = null;
    public LayoutInflater inflater;
    public Activity context;

    public MyApptListViewAdapter(Fragment frag, GnrlAppt[] gnrlApptAry, SrvReq[] mysrapptary) {
        super();
        this.context = frag.getActivity();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.gnrlApptAry = gnrlApptAry;
        this.mySRApptAry = mysrapptary;
        prepareMyApptAgent();
    }

    @Override
    public int getCount() {
        if (myApptAgentAry == null) return 0;
        return myApptAgentAry.length;
    }

    @Override
    public Object getItem(int position) {
        if (position >= myApptAgentAry.length) {
            return null;
        }

        if (myApptAgentAry[position].isSR) {
            return myApptAgentAry[position].mySrvReq;
        } else {
            return myApptAgentAry[position].gnrlAppt;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ViewHolder holder;
            int basePadding = (int) context.getResources().getDimension(R.dimen.basePadding);

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.adapter_myapptlist, null);

                holder.adapter_myapptlist_logo = (ImageView) convertView.findViewById(R.id.adapter_myapptlist_logo);
//				holder.adapter_myapptlist_container =  (ImageView) convertView.findViewById(R.id.adapter_myapptlist_container);
//				holder.adapter_myapptlist_container.getLayoutParams().height = imageHeight;
//				holder.adapter_myapptlist_container.getLayoutParams().width = imageHeight;
//				holder.adapter_myapptlist_logo.setPadding(buttonPadding, buttonPadding, buttonPadding, buttonPadding);

                holder.adapter_apptlist_acctnum_txt = (TextView) convertView.findViewById(R.id.adapter_apptlist_acctnum_txt);
                holder.adapter_myapplist_appttype_txt = (TextView) convertView.findViewById(R.id.adapter_myapplist_appttype_txt);
                holder.adapter_myapplist_date_txt = (TextView) convertView.findViewById(R.id.adapter_myapplist_date_txt);
                holder.adapter_myapplist_date_txt.setCompoundDrawablesWithIntrinsicBounds(R.drawable.rightarrow_small, 0, 0, 0);
                holder.adapter_myapplist_date_txt.setCompoundDrawablePadding(basePadding);
                holder.adapter_myapplist_time_txt = (TextView) convertView.findViewById(R.id.adapter_myapplist_time_txt);
                holder.adapter_myapplist_time_txt.setPadding(basePadding, 0, 0, 0);
                holder.adapter_myapptlist_address_txt = (TextView) convertView.findViewById(R.id.adapter_myapptlist_address_txt);
                holder.adapter_myapptlist_arrow = (ImageView) convertView.findViewById(R.id.adapter_myapptlist_arrow);
                holder.adapter_myapptlist_line = (ImageView) convertView.findViewById(R.id.adapter_myapptlist_line);
                holder.adapter_myapptlist_line.setImageResource(R.drawable.greyline);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            if (myApptAgentAry[position].isSR) {
                // SR
//				holder.adapter_apptlist_acctnum_txt.setText(Utils.getString(context, R.string.MYHKT_APPT_TYPE_MAINT));
                holder.adapter_apptlist_acctnum_txt.setText(myApptAgentAry[position].mySrvReq.getAssocSubnRec().srvNum);
                holder.adapter_myapplist_appttype_txt.setText(Utils.getString(context, R.string.MYHKT_APPT_TYPE_MAINT));
                int lob_eye_lts_plain = getSrvLogo(myApptAgentAry[position].mySrvReq);
                if (R.drawable.lob_eye_lts_plain == lob_eye_lts_plain) {
                    holder.adapter_myapptlist_logo.setScaleType(ImageView.ScaleType.FIT_CENTER);
                } else {
                    holder.adapter_myapptlist_logo.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }
                holder.adapter_myapptlist_logo.setImageResource(lob_eye_lts_plain);
                holder.adapter_myapplist_date_txt.setText(Utils.toDateString(myApptAgentAry[position].mySrvReq.getApptTS().getApptDate().trim(), Utils.getString(context, R.string.input_date_format), Utils.getString(context, R.string.usage_date_format)));
                holder.adapter_myapplist_time_txt.setText(Utils.getDateTime(myApptAgentAry[position].mySrvReq.getApptTS().getApptTmslot(), Utils.getString(context, R.string.output_time_format), true) + "-" + Utils.getDateTime(myApptAgentAry[position].mySrvReq.getApptTS().getApptTmslot(), Utils.getString(context, R.string.output_time_format), false));
                holder.adapter_myapptlist_address_txt.setText(myApptAgentAry[position].mySrvReq.getSrInfo().getCustAdr());
                holder.adapter_myapptlist_arrow.setVisibility(View.VISIBLE);
            } else {
                // MyAppt
//				holder.adapter_apptlist_acctnum_txt.setText(Utils.getString(context, R.string.MYHKT_APPT_TYPE_ORDER));
                holder.adapter_myapplist_appttype_txt.setText(Utils.getString(context, R.string.MYHKT_APPT_TYPE_ORDER));
                int lob_eye_lts_plain = getSrvLogo(myApptAgentAry[position].gnrlAppt);
                if (R.drawable.lob_eye_lts_plain == lob_eye_lts_plain) {
                    holder.adapter_myapptlist_logo.setScaleType(ImageView.ScaleType.FIT_CENTER);
                } else {
                    holder.adapter_myapptlist_logo.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                }
                holder.adapter_myapptlist_logo.setImageResource(lob_eye_lts_plain);
//				holder.adapter_myapptlist_logo.setBackgroundResource(R.drawable.logo_container_gray);
                holder.adapter_myapplist_date_txt.setText(Utils.toDateString(myApptAgentAry[position].gnrlAppt.getApptStDT(), Utils.getString(context, R.string.input_datetime_format), Utils.getString(context, R.string.usage_date_format)));
                holder.adapter_myapplist_time_txt.setText(Utils.toTimeString(myApptAgentAry[position].gnrlAppt.getApptStDT(), Utils.getString(context, R.string.input_datetime_format), Utils.getString(context, R.string.output_time_format)) + "-" + Utils.toTimeString(myApptAgentAry[position].gnrlAppt.getApptEnDT(), Utils.getString(context, R.string.input_datetime_format), Utils.getString(context, R.string.output_time_format)));
                holder.adapter_myapptlist_address_txt.setText(myApptAgentAry[position].gnrlAppt.getIAdr());
                if (myApptAgentAry[position].gnrlAppt.getShowDtls()) {
                    holder.adapter_myapptlist_arrow.setVisibility(View.VISIBLE);
                } else {
                    holder.adapter_myapptlist_arrow.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    private int getSrvLogo(GnrlAppt gnrlAppt) {
        boolean isLTS = false;
        boolean isEYE = false;
        boolean isPCD = false;
        boolean isTV = false;
        int logo = 0;

        if (gnrlAppt.getSrvAry().length > 0) {
            for (String srvType : gnrlAppt.getSrvAry()) {
                if (GnrlAppt.SRV_TEL.equals(srvType)) {
                    isLTS = true;
                } else if (GnrlAppt.SRV_EYE.equals(srvType)) {
                    isEYE = true;
                } else if (GnrlAppt.SRV_IMS.equals(srvType)) {
                    isPCD = true;
                } else if (GnrlAppt.SRV_TV.equals(srvType)) {
                    isTV = true;
                }
            }
        }

//		// Get image drawable id
//		if (isLTS && isEYE && isPCD && isTV) {
//			// LTS / EYE / PCD / TV
//		} else if (isLTS && isEYE && isPCD) {
//			// LTS / EYE / PCD
//		} else if (isLTS && isEYE && isTV) {
//			// LTS / EYE / TV
//		} else if (isLTS && isPCD && isTV) {
//			logo = R.drawable.lob_eye_pcd_tv_plain; // LTS / PCD / TV
//		} else if (isEYE && isPCD && isTV) {
//			logo = R.drawable.lob_eye_pcd_tv_plain; // EYE / PCD / TV
//		} else if (isLTS && isEYE) {
//			logo = R.drawable.lob_eye_lts_plain; 	// LTS / EYE
//		} else if (isLTS && isPCD) {
//			logo = R.drawable.lob_eye_pcd_plain; 	// LTS / PCD
//		} else if (isEYE && isPCD) {
//			logo = R.drawable.lob_eye_pcd_plain; 	// EYE / PCD
//		} else if (isLTS && isTV) {
//			logo = R.drawable.lob_eye_tv_plain;	 	// LTS / TV
//		} else if (isEYE && isTV) {
//			logo = R.drawable.lob_eye_tv_plain;	 	// EYE / TV
//		} else if (isPCD && isTV) {
//			logo = R.drawable.lob_pcd_tv_plain;  	// PCD / TV
//		} else if (isLTS) {
//			logo = R.drawable.lob_eye_lts_plain; 	 	// LTS
//		} else if (isEYE) {
//			logo = R.drawable.lob_eye_lts_plain;		// EYE
//		} else if (isPCD) {
//			logo = R.drawable.lob_pcd_plain; 	 	// PCD
//		} else if (isTV){
//			logo = R.drawable.lob_tv_plain; 	 	// TV
//		}

        // New implementation - iOS Realignment
        if (isEYE && isPCD && isTV) {
            logo = R.drawable.lob_eye_pcd_tv_plain; // EYE / PCD / TV
        } else if (isLTS && isPCD && isTV) {
            logo = R.drawable.lob_lts_pcd_tv_plain; // LTS / PCD / TV
        } else if (isEYE && isTV) {
            logo = R.drawable.lob_eye_tv_plain;        // EYE / TV
        } else if (isEYE && isPCD) {
            logo = R.drawable.lob_eye_pcd_plain;    // EYE / PCD // search the file
        } else if (isPCD && isTV) {
            logo = R.drawable.lob_pcd_tv;    // PCD / TV
        } else if (isLTS && isTV) {
            logo = R.drawable.lob_lts_tv_plain;        // LTS / TV
        } else if (isLTS && isPCD) {
            logo = R.drawable.lob_lts_pcd_plain;    // LTS / PCD
        } else if (isEYE) {
            logo = R.drawable.lob_lts_plain;        // EYE
        } else if (isTV) {
            logo = R.drawable.lob_tv_plain;        // TV
        } else if (isPCD) {
            logo = R.drawable.lob_pcd_plain;        // PCD
        } else if (isLTS) {
            logo = R.drawable.lob_lts_plain1;        // LTS
        } else if (isLTS && isEYE) {
            logo = R.drawable.lob_lts_plain_eye;
        }
        return logo;
    }

    private int getSrvLogo(SrvReq mySRappt) {
        boolean isLTS = false;
        boolean isEYE = false;
        boolean isPCD = false;
        boolean isTV = false;
        int logo = 0;

        if (SrvReq.OUT_PROD_VCE.equalsIgnoreCase(mySRappt.getOutProd())) {
            isLTS = true;
        } else if (SrvReq.OUT_PROD_PCD.equalsIgnoreCase(mySRappt.getOutProd())) {
            isPCD = true;
        } else if (SrvReq.OUT_PROD_EYE.equalsIgnoreCase(mySRappt.getOutProd())) {
            isEYE = true;
        } else if (SrvReq.OUT_PROD_VI.equalsIgnoreCase(mySRappt.getOutProd())) {
            isTV = true; // refer to csp website
        }

        if (isLTS) {
            logo = R.drawable.lob_eye_lts_plain;        // LTS
        } else if (isEYE) {
            logo = R.drawable.lob_eye_lts_plain;        // EYE
        } else if (isPCD) {
            logo = R.drawable.lob_pcd_plain;        // PCD
        } else if (isTV) {
            logo = R.drawable.lob_tv_plain;        // TV
        }
        return logo;
    }

    private void prepareMyApptAgent() {
        MyApptAgent rMyApptAgent;
        List rMyApptAgentLst;
        rMyApptAgentLst = new ArrayList();
        int rx, rl;

        rl = gnrlApptAry.length;
        for (rx = 0; rx < rl; rx++) {
            rMyApptAgent = new MyApptAgent();
            rMyApptAgent.isSR = false;
            rMyApptAgent.apptSDT = gnrlApptAry[rx].getApptStDT();
            rMyApptAgent.apptEDT = gnrlApptAry[rx].getApptEnDT();
            rMyApptAgent.gnrlAppt = gnrlApptAry[rx].copyMe();
            rMyApptAgent.mySrvReq = null;

            rMyApptAgentLst.add(rMyApptAgent);
        }

        rl = mySRApptAry.length;
        for (rx = 0; rx < rl; rx++) {
            rMyApptAgent = new MyApptAgent();
            rMyApptAgent.isSR = true;
            rMyApptAgent.apptSDT = mySRApptAry[rx].getApptTS().getApptDate().trim() + Utils.getDateTime(mySRApptAry[rx].getApptTS().getApptTmslot().trim(), "HHmmss", true);
            rMyApptAgent.apptEDT = mySRApptAry[rx].getApptTS().getApptDate().trim() + Utils.getDateTime(mySRApptAry[rx].getApptTS().getApptTmslot().trim(), "HHmmss", false);
            rMyApptAgent.mySrvReq = mySRApptAry[rx].copyMe();
            rMyApptAgent.gnrlAppt = null;

            rMyApptAgentLst.add(rMyApptAgent);
        }

        myApptAgentAry = (MyApptAgent[]) rMyApptAgentLst.toArray(new MyApptAgent[0]);

        Arrays.sort(myApptAgentAry, new Comparator<MyApptAgent>() {
            public int compare(MyApptAgent rA, MyApptAgent rB) {
                int rx;

                rx = rA.apptSDT.compareTo(rB.apptSDT);
                if (rx == 0) rx = rA.apptEDT.compareTo(rB.apptEDT);
                return (rx);
            }
        });
    }

    public static class ViewHolder {
        ImageView adapter_myapptlist_logo;
        TextView adapter_apptlist_acctnum_txt;
        //		ImageView	adapter_myapptlist_container;
//		TextView	adapter_apptlist_acctnum_txt;
        TextView adapter_myapplist_appttype_txt;
        TextView adapter_myapplist_date_txt;
        TextView adapter_myapplist_time_txt;
        TextView adapter_myapptlist_address_txt;
        ImageView adapter_myapptlist_arrow;
        ImageView adapter_myapptlist_line;
    }

    private final class MyApptAgent {
        private boolean isSR;
        private String apptSDT;
        private String apptEDT;
        private GnrlAppt gnrlAppt;
        private SrvReq mySrvReq;
    }

    public final boolean isSRItem(int position) {
        if (position >= myApptAgentAry.length) {
            return false;
        } else {
            return myApptAgentAry[position].isSR;
        }
    }
}
