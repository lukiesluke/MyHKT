package com.pccw.myhkt.adapter;

/************************************************************************
File       : EditServiceListViewAdapter.java
Desc       : Screen 7.6 - Edit Service List
Name       : EditServiceListViewAdapter
Created by : Ryan Wong
Date       : 13/12/2012

Change History:
Date       Modified By        	Description
---------- ----------------   	-------------------------------
13/12/2012 Ryan Wong          	- First draft
02/01/2013 Vincent Fung			- Added HashMap to handle list of checkbox
08/01/2013 Vincent Fung			- Changed HashMap to Boolean Linear Array
18/01/2013 Vincent Fung 		- Added loadServiceList to get lobType, acNum, acName and Selected CheckBox
19/02/2013 Vincent Fung			- Changed checkSelected() return number of selection
15/04/2013 Ryan Wong			- Added NE LOB handling 
23/04/2013 Ryan Wong			- Updated service list display format (remove acnum)
25/06/2013 Ryan Wong			- CSP-CR2013011 - Adding IDD service & Onecall at CS Portal (Demo)
26/06/2013 Ryan Wong			- CSP-CR2013011: updated layout to the "one-line approach" for FRS
29/07/2013 Ryan Wong			- CSP-CR2013011 - Replaced Demo codes with the logic defined in spec
07/10/2013 Vincent Fung			- Added Type for LTS ICFS
18/11/2013 Vincent Fung			- Modified getString to call common getString function
26/11/2013 Vincent Fung			- Modified the header as "CARD XXXX" if the service type is LTS - Global Calling Card
06/01/2014 Vincent Fung			- Modified "CARD XXXX" to be "CARDXXXX" in Global Calling Card title
13/05/2014 Vincent Fung 		- passing CONST LOB instead of using number
01/09/2014 Derek Tsui			- Added 1010/O2F service
30/12/2014 Derek Tsui			- SwipeListView added
25/02/2015 Vincent Fung 		- pre-fill Alias when pop up EditAlias Dialog
 *************************************************************************/

import android.app.Activity;
import android.content.Context;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;

import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.model.AcctAgent;

import java.util.Arrays;
import java.util.Comparator;

public class EditServiceListViewAdapter extends BaseAdapter {
	// Contents stored here
	int[]					lobType;
	String[]				acNum;
	String[]				acName;
	boolean					debug	= false;
	Boolean 				isZh        = false;

	// ServiceListArray
	//	private SubscriptionRec	subnRecAry[];
	private SubnRec subnRecAry[];

	// Used control CheckBox status
	public static Boolean[]	isSelected;

	public Activity			context;
	public LayoutInflater	inflater;
	private AcctAgent acctAgentAry[] = null;

	// CallBack
	private OnEditServiceListViewAdapterListener			callback;

	public abstract interface OnEditServiceListViewAdapterListener {
		public void displayEditDialog(String message, final int position);
	}

	public EditServiceListViewAdapter(Activity context, Fragment frag) {
		super();

		// This makes sure that the container activity has implemented
		// the callback interface. If not, it throws an exception
		try {
			callback = (OnEditServiceListViewAdapterListener) frag;
		} catch (ClassCastException e) {
			throw new ClassCastException(context.toString() + " must implement OnEditServiceListViewAdapterListener");
		}

		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//		debug = context.getResources().getBoolean(R.bool.DEBUG);

		//		subnRecAry = ClnEnv.getQualCust().getSubnRecAry();
		subnRecAry = ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry();

		loadServiceList(subnRecAry);

		if ("zh".equalsIgnoreCase( Utils.getString(context, R.string.myhkt_lang))) {
			isZh = true;
		} else {
			isZh = false;
		}
	}

	public static int checkSelected() {
		int count = 0;
		for (int i = 0; i < isSelected.length; i++) {
			if (isSelected[i]) count++;
		}
		return count;
	}

	public int getCount() {
		return lobType.length;
	}

	public Object getItem(int position) {
		if (position >= subnRecAry.length) { return null; }

		return subnRecAry[position];
	}

	public long getItemId(int position) {
		return position;
	}

	public final String getAlias(int position) {
		return subnRecAry[position].alias;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		try {
			ListViewHolder holder = null;
			int ltsType = R.string.CONST_LTS_INVALID;

			if (convertView == null) {
				holder = new ListViewHolder();
				convertView = inflater.inflate(R.layout.adapter_editservicelist, null);

				holder.adapter_editservicelist_logo = (ImageView) convertView.findViewById(R.id.adapter_editservicelist_logo);
				holder.adapter_editservicelist_header_alias = (TextView) convertView.findViewById(R.id.adapter_editservicelist_header_alias);
				holder.adapter_editservicelist_header = (TextView) convertView.findViewById(R.id.adapter_editservicelist_header);
				holder.adapter_editservicelist_ltslabelImage = (ImageView) convertView.findViewById(R.id.adapter_editservicelist_ltslabelImage);
				holder.adapter_editservicelist_ltslabel = (TextView) convertView.findViewById(R.id.adapter_editservicelist_ltslabel);
				holder.adapter_editservicelist_checkBox = (CheckBox) convertView.findViewById(R.id.adapter_editservicelist_checkBox);
				holder.adapter_editservicelist_checkBox.setButtonDrawable(R.drawable.checkbox);

				//SwipeListView
				holder.adapter_editservicelist_edit = (Button) convertView.findViewById(R.id.adapter_editservicelist_edit);
				holder.adapter_editservicelist_edit.setText(R.string.MYMOB_BTN_ALIAS);
				holder.adapter_editservicelist_edit.setTextSize(14);
				LinearLayout.LayoutParams params = (LayoutParams) holder.adapter_editservicelist_edit.getLayoutParams();
				int deviceWidth = context.getResources().getDisplayMetrics().widthPixels;
				params.width = deviceWidth * 1 / 6;
				holder.adapter_editservicelist_edit.setLayoutParams(params);
				//				holder.adapter_editservicelist_edit.setOnTouchListener(new CCMCHoverButton(context, R.string.CONST_BTN_ALIAS, false));

				convertView.setTag(holder);
			} else {
				holder = (ListViewHolder) convertView.getTag();
			}

			holder.adapter_editservicelist_checkBox.setOnClickListener(v -> isSelected[position] = !isSelected[position]);

			switch (lobType[position]) {
				case R.string.CONST_LOB_1010:
					holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_1010_plain);
					holder.adapter_editservicelist_header.setText(acName[position]);
					break;

				case R.string.CONST_LOB_CSP:
					holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_csp_plain);
					holder.adapter_editservicelist_header.setText(acName[position]);
					break;
				case R.string.CONST_LOB_IOI:
					holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_1010_plain);
					holder.adapter_editservicelist_header.setText(acName[position]);
					break;
				case R.string.CONST_LOB_LTS:
					// CSP-CR2013011 - Adding IDD service & Onecall at CS Portal
					// rAcctAgent.ltsType = Utils.getLtsSrvType(ClnEnv.getQualCust().getAssocSubnRecAry()[rx].tos, ClnEnv.getQualCust().getAssocSubnRecAry()[rx].eye_grp, ClnEnv.getQualCust().getAssocSubnRecAry()[rx].pri_login_ind);
					ltsType = Utils.getLtsSrvType(subnRecAry[position].tos, subnRecAry[position].eyeGrp, subnRecAry[position].priMob);
					if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
						int last4index = (acName[position].length() - 4) < 0 ? 0 : acName[position].length() - 4;
						holder.adapter_editservicelist_header.setText(String.format("CARD%s", acName[position].substring(last4index)));
					} else {
						holder.adapter_editservicelist_header.setText(acName[position]);
					}
					holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_lts_plain);
					break;
				case R.string.CONST_LOB_MOB:
					holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_csl_plain);
					holder.adapter_editservicelist_header.setText(acName[position]);
					break;
				case R.string.CONST_LOB_O2F:
					holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_csl_plain);
					holder.adapter_editservicelist_header.setText(acName[position]);
					break;
				case R.string.CONST_LOB_PCD:
					holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_pcd_plain);
					holder.adapter_editservicelist_header.setText(acName[position]);
					break;
				case R.string.CONST_LOB_TV:
					holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_tv_plain);
					holder.adapter_editservicelist_header.setText(acNum[position]);
					break;
//			case R.string.CONST_LOB_MOB:
//			case R.string.CONST_LOB_O2F:
//				holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_csl_plain);
//				holder.adapter_editservicelist_header.setText(acName[position]);
//				break;
//			case R.string.CONST_LOB_1010:
//			case R.string.CONST_LOB_IOI:
//				holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_1010_plain);
//				holder.adapter_editservicelist_header.setText(acName[position]);
//				break;
//			case R.string.CONST_LOB_PCD:
//				holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_pcd_plain);
//				holder.adapter_editservicelist_header.setText(acName[position]);
//				break;
//			case R.string.CONST_LOB_TV:
//				holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_tv_plain);
//				holder.adapter_editservicelist_header.setText(acNum[position]);
//				break;
//			case R.string.CONST_LOB_LTS:
//				// CSP-CR2013011 - Adding IDD service & Onecall at CS Portal
//				// rAcctAgent.ltsType = Utils.getLtsSrvType(ClnEnv.getQualCust().getAssocSubnRecAry()[rx].tos, ClnEnv.getQualCust().getAssocSubnRecAry()[rx].eye_grp, ClnEnv.getQualCust().getAssocSubnRecAry()[rx].pri_login_ind);
//				ltsType = Utils.getLtsSrvType(subnRecAry[position].tos, subnRecAry[position].eyeGrp, subnRecAry[position].priMob);
//				if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
//					int last4index = (acName[position].length() - 4) < 0 ? 0 : acName[position].length() - 4;
//					holder.adapter_editservicelist_header.setText(String.format("CARD%s", acName[position].substring(last4index)));
//				} else {
//					holder.adapter_editservicelist_header.setText(acName[position]);
//				}
//				holder.adapter_editservicelist_logo.setImageResource(R.drawable.lob_lts_plain);
//				break;
			}

			// CSP-CR2013011 - Adding IDD service & Onecall at CS Portal
			if (lobType[position] == R.string.CONST_LOB_LTS) {
				//				holder.adapter_servicelist_header.setText(holder.adapter_servicelist_header.getText() + "  ");
				holder.adapter_editservicelist_ltslabel.setVisibility(View.VISIBLE);
				holder.adapter_editservicelist_ltslabel.setTextColor(context.getResources().getColor(R.color.hkt_txtcolor_grey));
				switch (ltsType) {
				case R.string.CONST_LTS_FIXEDLINE:
					holder.adapter_editservicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_fixedline));
					break;
				case R.string.CONST_LTS_EYE:
					holder.adapter_editservicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_eye));
					break;
				case R.string.CONST_LTS_IDD0060:
					holder.adapter_editservicelist_ltslabel.setText( Utils.getString(context ,R.string.myhkt_lts_idd0060));
					break;
				case R.string.CONST_LTS_CALLINGCARD:
					holder.adapter_editservicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_callingcard));
					break;
				case R.string.CONST_LTS_ONECALL:
					holder.adapter_editservicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_onecall));
					break;
				case R.string.CONST_LTS_ICFS:
					holder.adapter_editservicelist_ltslabel.setText(Utils.getString(context ,R.string.myhkt_lts_icfs));
					break;
				default:
					holder.adapter_editservicelist_ltslabel.setVisibility(View.GONE);
					break;
				}
			} else {
				holder.adapter_editservicelist_ltslabel.setVisibility(View.GONE);
			}

			holder.adapter_editservicelist_checkBox.setChecked(isSelected[position]);

			//trim alias to prevent blank space
			String alias = subnRecAry[position].alias;
			if (alias != null) alias = alias.trim();

			if (alias != null && !"".equalsIgnoreCase(alias)) {
				holder.adapter_editservicelist_header_alias.setVisibility(View.VISIBLE);
				holder.adapter_editservicelist_header_alias.setText(alias);
			} else {
				holder.adapter_editservicelist_header_alias.setText("");
				holder.adapter_editservicelist_header_alias.setVisibility(View.GONE);
			}

			holder.adapter_editservicelist_edit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					callback.displayEditDialog(Utils.getString(context, R.string.MYMOB_PLZ_INPUT_ALIAS), position);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	public static class ListViewHolder {
		public ImageView	adapter_editservicelist_logo;
		public TextView		adapter_editservicelist_header_alias;
		public TextView		adapter_editservicelist_header;
		public ImageView	adapter_editservicelist_ltslabelImage;
		public TextView		adapter_editservicelist_ltslabel;
		public CheckBox		adapter_editservicelist_checkBox;
		public Button 		adapter_editservicelist_edit;
	}

	public void loadServiceList(SubnRec... arrlist) {
		lobType = new int[arrlist.length];
		acName = new String[arrlist.length];
		acNum = new String[arrlist.length];
		isSelected = new Boolean[acNum.length];
		// Initialize list of lob type
		for (int i = 0; i < arrlist.length; i++) {
			lobType[i] = 0;
		}
		for (int i = 0; i < arrlist.length; i++) {
			isSelected[i] = false;
		}
		try {
			for (int i = 0; i < arrlist.length; i++) {
				if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_101)) {
					lobType[i] = R.string.CONST_LOB_1010;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_CSP)) {
					lobType[i] = R.string.CONST_LOB_CSP;
				}
				else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_IOI)) {
					lobType[i] = R.string.CONST_LOB_IOI;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_LTS)) {
					lobType[i] = R.string.CONST_LOB_LTS;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_MOB)) {
					lobType[i] = R.string.CONST_LOB_MOB;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_O2F)) {
					lobType[i] = R.string.CONST_LOB_O2F;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_PCD)) {
					lobType[i] = R.string.CONST_LOB_PCD;
				}
				else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_TV)) {
					lobType[i] = R.string.CONST_LOB_TV;
				}
				else {
					// Unknown Cases
				}

		/*		if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_MOB)) {
					lobType[i] = R.string.CONST_LOB_MOB;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_IOI)) {
					lobType[i] = R.string.CONST_LOB_IOI;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_PCD)) {
					lobType[i] = R.string.CONST_LOB_PCD;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_TV)) {
					lobType[i] = R.string.CONST_LOB_TV;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_LTS)) {
					lobType[i] = R.string.CONST_LOB_LTS;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_101)) {
					lobType[i] = R.string.CONST_LOB_1010;
				} else if (arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_O2F)) {
					lobType[i] = R.string.CONST_LOB_O2F;
				} else {
					// Unknown Cases
				}*/
				acNum[i] = arrlist[i].acctNum;
				if (!arrlist[i].lob.equalsIgnoreCase(SubnRec.LOB_TV)) {
					acName[i] = arrlist[i].srvNum;
				}
				// Check CheckBox selection
				if (arrlist[i].assoc.equalsIgnoreCase("Y")) {
					isSelected[i] = true;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}