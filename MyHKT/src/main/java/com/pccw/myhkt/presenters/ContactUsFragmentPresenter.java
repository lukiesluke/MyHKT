package com.pccw.myhkt.presenters;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.pccw.myhkt.views.ContactUsFragmentView;

public class ContactUsFragmentPresenter extends MvpBasePresenter<ContactUsFragmentView> {

    private static final String TAG = ContactUsFragmentPresenter.class.getName();

    private ContactUsFragmentView view;

    public void initialise(ContactUsFragmentView view) { this.view = view; }

    public void triggerLiveChat(int stringId) {
        view.onTriggeredLiveChat(stringId);
    }

    public void triggerCall(String phone) {
        view.onTriggeredCall(phone);
    }

    public void triggerEmail(String email) {
        view.onTriggeredEmail(email);
    }

    public void triggerFacebook() {
        view.onTriggeredFacebook();
    }

    public void triggerInstagram() {
        view.onTriggeredInstagram();
    }

    public void triggerWhatsApp(String mobile) {
        view.onTriggeredWhatsApp(mobile);
    }
}
