package com.pccw.myhkt.fragment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.BiifCra;
import com.pccw.dango.shared.entity.Account;
import com.pccw.dango.shared.entity.Biif;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.BrowserActivity;
import com.pccw.myhkt.lib.ui.AAQuery;

public class BillInfoLTSChild2Fragment extends BaseServiceFragment {


	private BillInfoLTSChild2Fragment me;
	private View myView;
	private AAQuery aq;	
	private BiifCra biifCra;	

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_billinfolts_child2, container, false);
		myView = fragmentLayout;
		return fragmentLayout;
	}
	int lob ;

	protected void initUI() {
		aq = new AAQuery(myView);		

		aq.id(R.id.paymode_label_frame).backgroundColorId(R.color.white);
		int imageRes = Utils.theme(R.drawable.billinfo_icon, callback_main.getLob());
		aq.id(R.id.paymode_label_paymethod).getTextView().setCompoundDrawablesWithIntrinsicBounds(imageRes, 0, 0, 0);

		//Info
		aq.normEditText(R.id.paymode_masked_cc, "", "");
		aq.id(R.id.paymode_masked_cc).enabled(false);

		aq.normText(R.id.paymode_clicktochage, "" , -2);
		aq.id(R.id.paymode_masked_cc).enabled(false);
		//		((BillInfoLTSFragment)this.getParentFragment()).showRightBtn();
		//		if (biifCra ==null && ((BillInfoLTSFragment)getParentFragment()).activeSubView ==  R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2) {
		//			((BillInfoLTSFragment)this.getParentFragment()).hideRightBtn();
		//		} 
		if (biifCra !=null) {
			String payMeth = biifCra.getOPyif4Lts().getPayMeth();
			if(payMeth.equals("M")) {
				hideUpdateFunction();
				aq.id(R.id.paymode_clicktochage).text(getResString(R.string.BUPLTF_PAYMODE_MSG));
				aq.id(R.id.paymode_masked_cc).text(getResources().getString(R.string.BUPLTF_PM_CASH));
			} else if(payMeth.equals("A")) {
				hideUpdateFunction();
				aq.id(R.id.paymode_clicktochage).text(getResString(R.string.BUPLTF_PAYMODE_MSG));
				aq.id(R.id.paymode_masked_cc).text(getResources().getString(R.string.BUPLTF_PM_AUTOPAY));
			} else if(payMeth.equals("C")) {
				hideUpdateFunction();
				aq.id(R.id.paymode_clicktochage).text(getResString(R.string.BUPLTF_PAYMODE_MSG1));
				//				((BillInfoLTSFragment)this.getParentFragment()).setRightBtnName(getResString(R.string.BUPLTF_BTN_UPDATE));
				//				aq.id(R.id.paymode_clicktochage).text(getResString(R.string.BUPLTF_PAYMODE_MSG1));
				//				aq.id(R.id.paymode_masked_cc).text(getResources().getString(R.string.BUPLTF_PM_CRCARD) + ":" + bupdCra.getTok4Pan());
				aq.id(R.id.paymode_masked_cc).text(getResources().getString(R.string.BUPLTF_PM_CRCARD) + ":" + biifCra.getOPyif4Lts().getTok4Pan());
			}
		}
		((BillInfoLTSFragment)this.getParentFragment()).hideLeftBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2);
		((BillInfoLTSFragment)this.getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2);
	}

	private void hideUpdateFunction() {
		((BillInfoLTSFragment)this.getParentFragment()).setRightBtnName(getResString(R.string.BUPLTF_BTN_CHANGE));
		aq.id(R.id.paymode_clicktochage).gone();
	}

	private void hideBackFunction() {
	}

	private void doGetBillInfo() {
		BiifCra biifCra = new BiifCra();
		biifCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		try {
			callback_main = (OnServiceListener) this.getActivity();
		} catch (ClassCastException e) {
			throw new ClassCastException(this.getActivity().toString() + " must implement OnServiceListener");
		}
		biifCra.setISubnRec(callback_main.getSubnRec().copyMe());
		APIsManager.doGetBillInfo(me, biifCra);
	}

	public void doGetPaymentMethodURL() {
		Account account = callback_main.getAccount();
		//		if (account != null) {

		// doGetBillInfo
		BiifCra mBiffCra = biifCra.copyMe();
		//			biffCra.setIAcct(account);
		mBiffCra.setISubnRec(callback_main.getSubnRec());
		Biif pyid4Lts = biifCra.getOPyif4Lts().copyMe();
		pyid4Lts.setPayName(biifCra.getOBiif4Hkt().getBillNm());
		pyid4Lts.setPayDocTy(ClnEnv.getQualSvee().getCustRec().docTy);
		pyid4Lts.setPayDocNum(ClnEnv.getQualSvee().getCustRec().docNum);
		mBiffCra.setIPyif4Lts(pyid4Lts);
		mBiffCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		mBiffCra.clearOPyif4Lts();

		APIsManager.doUpdatePayMeth(this, mBiffCra);
		//			doUpdatePaymentMethodAsyncTask = new DoUpdatePaymentMethodAsyncTask(me, callbackHandler).execute(bupdGrq);
		//		}
	}

	public void goToWebView(String url) {
		//Screen Tracker
		FAWrapper.getInstance().sendFAScreen(getActivity(),
				R.string.CONST_SCRN_LTSPAYMENTINFO_UPDATE, false);

		Boolean zombie = !callback_main.getAcctAgent().isLive();
		String title = zombie ?  callback_main.getSubnRec().acctNum + "*" : callback_main.getSubnRec().srvNum;
		int icon = getLob(callback_main.getSubnRec());
		Intent intent = new Intent();
		intent.setClass(getActivity(), BrowserActivity.class);
		intent.putExtra(BrowserActivity.INTENTLINK, url);
		intent.putExtra(BrowserActivity.INTENTTITLE, title);
		intent.putExtra(BrowserActivity.INTENTLOB, icon);
		intent.putExtra(BrowserActivity.INTENTLOBTYPE, callback_main.getLtsType());
		this.startActivity(intent);
	}

	private final int getLob(SubnRec subnRec) {
		if (SubnRec.LOB_LTS.equalsIgnoreCase(subnRec.lob)) {
			return R.string.CONST_LOB_LTS;
		} else if (SubnRec.LOB_MOB.equalsIgnoreCase(subnRec.lob)) {
			return R.string.CONST_LOB_MOB;
		} else if (SubnRec.LOB_IOI.equalsIgnoreCase(subnRec.lob)) {
			return R.string.CONST_LOB_IOI;
		} else if (SubnRec.LOB_PCD.equalsIgnoreCase(subnRec.lob)) {
			return R.string.CONST_LOB_PCD;
		} else if (SubnRec.LOB_TV.equalsIgnoreCase(subnRec.lob)) {
			return R.string.CONST_LOB_TV; 
		} else if (SubnRec.LOB_101.equalsIgnoreCase(subnRec.lob)) {
			return R.string.CONST_LOB_1010; 
		} else if (SubnRec.LOB_O2F.equalsIgnoreCase(subnRec.lob)) { 
			return R.string.CONST_LOB_O2F; 
		}
		return 0;
	}

	private final int getLobIcon() {
		//There will be no acctAgent if it come from line test 
		//		int lobType = acctAgent.getLobType();
		if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI) {
			return R.drawable.logo_1010;
		} else if (lob == R.string.CONST_LOB_MOB || lob == R.string.CONST_LOB_O2F) {
			return R.drawable.logo_csl;
		} else if (lob == R.string.CONST_LOB_LTS) {
			//			switch (ltsType) {.
			//			case R.string.CONST_LTS_FIXEDLINE:
			//				return R.drawable.logo_fixedline_fixedline;
			//
			//			case R.string.CONST_LTS_EYE:
			//				return R.drawable.logo_fixedline_eye;
			//
			//			case R.string.CONST_LTS_IDD0060:
			//				return R.drawable.logo_fixedline_0060;
			//
			//			case R.string.CONST_LTS_CALLINGCARD:
			//				return R.drawable.logo_fixedline_global;
			//
			//			case R.string.CONST_LTS_ONECALL:
			//				return R.drawable.logo_fixedline_onecall;
			//
			//			case R.string.CONST_LTS_ICFS:
			//				return R.drawable.logo_fixedline_icfs;
			//
			//			default:
			return R.drawable.logo_fixedline;
			//			}
		} else if (lob == R.string.CONST_LOB_PCD) {
			return R.drawable.logo_netvigator2;
		} else if (lob == R.string.CONST_LOB_TV) {
			return R.drawable.logo_now;
		}
		return 0;
	}

	private String urlEncode(String url) {
		String encodedurl = "";
		try {
			encodedurl = URLEncoder.encode(url,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodedurl;
	}
	protected final void refreshData() {
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
			if (!((BillFragment)getParentFragment().getParentFragment()).isBillSum) {
				if (((BillInfoLTSFragment)getParentFragment()).activeSubView ==  R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2) {
					super.refreshData();
					doGetBillInfo();
				}
			}
		}
	}

	public final void refresh() {
		super.refresh();
		((BillInfoLTSFragment)this.getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2);
		doGetBillInfo();
	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.READ_BIIF.equals(response.getActionTy())) {
			biifCra = new BiifCra();
			biifCra = (BiifCra) response.getCra();
			initUI();
			((BillInfoLTSFragment)this.getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2);
		} else if (APIsManager.REQ_UPDTOK4LTSPI.equals(response.getActionTy())){
			biifCra = new BiifCra();
			biifCra = (BiifCra) response.getCra();
			goToWebView(biifCra.getOCeksUrl());
			//			DialogHelper.createSimpleDialog(getActivity(), getActivity().getString(R.string.BUPIMM_UPD_DONE));
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		//		 General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}

		//Hide the update button when there is error for reading bill
		if (APIsManager.READ_BIIF.equals(response.getActionTy())) {
			((BillInfoLTSFragment) getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2);
		}
	}

}
