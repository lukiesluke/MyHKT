package com.pccw.myhkt.fragment;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.ContactUsNewActivity;
import com.pccw.myhkt.adapter.ContactUsItemAdapter;
import com.pccw.myhkt.model.ContactUsItem;
import com.pccw.myhkt.model.HomeButtonItem;
import com.pccw.myhkt.presenters.ContactUsFragmentPresenter;
import com.pccw.myhkt.views.ContactUsFragmentView;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactUsConsumerFragment extends MvpFragment<ContactUsFragmentView,
        ContactUsFragmentPresenter> implements ContactUsFragmentView {

    private static final String TAG = ContactUsConsumerFragment.class.getName();

    @BindView(R.id.rv_cu_consumer_whats_app)
    RecyclerView rvWhatsApp;
    @BindView(R.id.rv_cu_consumer_live_chat)
    RecyclerView rvLiveChat;
    @BindView(R.id.rv_cu_consumer_support_hotlines)
    RecyclerView rvCsHotline;
    @BindView(R.id.rv_cu_consumer_sales_hotline)
    RecyclerView rvSales;
    @BindView(R.id.rv_cu_consumer_support_email_enquiries)
    RecyclerView rvEmail;
    @BindView(R.id.rv_cu_consumer_support_consumer_others)
    RecyclerView rvOthers;

    private ContactUsItemAdapter cuAdapterWhatsApp, cuAdapterLiveChat, cuAdapterCsHotline, cuAdapterSales,
            cuAdapterEmail, cuAdapterOthers;

    public ContactUsConsumerFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public ContactUsFragmentPresenter createPresenter() {
        return new ContactUsFragmentPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_us_consumer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initialise();
    }

    private void initialise() {
        presenter.initialise(this.getMvpView());
        cuAdapterWhatsApp = new ContactUsItemAdapter(getActivity());
        cuAdapterLiveChat = new ContactUsItemAdapter(getActivity());
        cuAdapterCsHotline = new ContactUsItemAdapter(getActivity());
        cuAdapterSales = new ContactUsItemAdapter(getActivity());
        cuAdapterEmail = new ContactUsItemAdapter(getActivity());
        cuAdapterOthers = new ContactUsItemAdapter(getActivity());
        prepareListData();
    }

    private void prepareListData() {
        prepareWhatsApp();
        prepareLiveChat();
        prepareCsHotline();
        prepareSales();
        prepareEmail();
        prepareOthers();
    }

    private void prepareWhatsApp() {
        ArrayList<ContactUsItem> whatAppList = new ArrayList<>();

        whatAppList.add(new ContactUsItem(R.string.support_whatsapp_netvigator, R.string.support_whatsapp_hotline_netvigator,
                R.drawable.icon_whatsapp, R.string.TITLE_WHATS_APP,
                () -> presenter.triggerWhatsApp(getString(R.string.support_whatsapp_hotline_netvigator))));
        whatAppList.add(new ContactUsItem(R.string.support_whatsapp_now_tv, R.string.support_whatsapp_hotline_now_tv,
                R.drawable.icon_whatsapp, R.string.TITLE_WHATS_APP,
                () -> presenter.triggerWhatsApp(getString(R.string.support_whatsapp_hotline_now_tv))));
        whatAppList.add(new ContactUsItem(R.string.support_whatsapp_eye, R.string.support_whatsapp_hotline_eye,
                R.drawable.icon_whatsapp, R.string.TITLE_WHATS_APP,
                () -> presenter.triggerWhatsApp(getString(R.string.support_whatsapp_hotline_eye))));
        whatAppList.add(new ContactUsItem(R.string.support_whatsapp_1o1o, R.string.support_whatsapp_hotline_1o1o,
                R.drawable.icon_whatsapp, R.string.TITLE_WHATS_APP,
                () -> presenter.triggerWhatsApp(getString(R.string.support_whatsapp_hotline_1o1o))));
        whatAppList.add(new ContactUsItem(R.string.support_whatsapp_csl, R.string.support_whatsapp_hotline_csl,
                R.drawable.icon_whatsapp, R.string.TITLE_WHATS_APP,
                () -> presenter.triggerWhatsApp(getString(R.string.support_whatsapp_hotline_csl))));
        whatAppList.add(new ContactUsItem(R.string.support_whatsapp_club_sim, R.string.support_whatsapp_hotline_club_sim,
                R.drawable.icon_whatsapp, R.string.TITLE_WHATS_APP,
                () -> presenter.triggerWhatsApp(getString(R.string.support_whatsapp_hotline_club_sim))));

        cuAdapterWhatsApp.addItemEntries(whatAppList);
        rvWhatsApp.setAdapter(cuAdapterWhatsApp);
        rvWhatsApp.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareLiveChat() {
        ArrayList<ContactUsItem> alLiveChat = new ArrayList<>();

        alLiveChat.add(new ContactUsItem(R.string.SUB_TITLE_LIVE_CHAT, -1,
                R.drawable.livechat_small, R.string.MYHKT_BTN_LIVECHAT,
                () -> presenter.triggerLiveChat(-1)));
        alLiveChat.add(new ContactUsItem(R.string.support_liveChat_description_1010, -1,
                R.drawable.livechat_small, R.string.MYHKT_BTN_LIVECHAT,
                () -> presenter.triggerLiveChat(R.string.MODULE_101_BILL)));
        alLiveChat.add(new ContactUsItem(R.string.support_liveChat_description_csl, -1,
                R.drawable.livechat_small, R.string.MYHKT_BTN_LIVECHAT,
                () -> presenter.triggerLiveChat(R.string.MODULE_MOB_BILL)));
        alLiveChat.add(new ContactUsItem(R.string.support_liveChat_description_csp, -1,
                R.drawable.livechat_small, R.string.MYHKT_BTN_LIVECHAT,
                () -> presenter.triggerLiveChat(R.string.MODULE_CLUBSIM_PLAN)));

        cuAdapterLiveChat.addItemEntries(alLiveChat);
        rvLiveChat.setAdapter(cuAdapterLiveChat);
        rvLiveChat.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareCsHotline() {
        ArrayList<ContactUsItem> alCsHotline = new ArrayList<>();

        alCsHotline.add(new ContactUsItem(R.string.support_consumer_hotline_title,
                R.string.support_consumer_hotline_no, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_consumer_hotline_no))));
        alCsHotline.add(new ContactUsItem(R.string.support_1010_customer_hotline_title,
                R.string.support_mobile_hotline_no_1010, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_mobile_hotline_no_1010))));
        alCsHotline.add(new ContactUsItem(R.string.support_csl_customer_hotline_title,
                R.string.support_mobile_hotline_no, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_mobile_hotline_no))));
        alCsHotline.add(new ContactUsItem(R.string.support_csp_customer_hotline_title,
                R.string.support_mobile_hotline_no_clubsim, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_mobile_hotline_no_clubsim))));
        alCsHotline.add(new ContactUsItem(R.string.support_consumer_tv_hotline_title,
                R.string.support_consumer_tv_hotline_no, R.drawable.phone_small,
                R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_consumer_tv_hotline_no))));

        cuAdapterCsHotline.addItemEntries(alCsHotline);
        rvCsHotline.setAdapter(cuAdapterCsHotline);
        rvCsHotline.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareSales() {
        ArrayList<ContactUsItem> alSales = new ArrayList<>();

        alSales.add(new ContactUsItem(R.string.support_hotline,
                R.string.support_sales_hotline_no, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_sales_hotline_no))));
        alSales.add(new ContactUsItem(R.string.support_fixed_line_sales_service_title,
                R.string.support_fixed_line_sales_service_hotline_info, R.drawable.phone_small,
                R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_sales_hotline_no))));
        alSales.add(new ContactUsItem(R.string.support_sales_1010_hotline_title,
                R.string.support_sales_1010_hotline_no, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_sales_1010_hotline_no))));
        alSales.add(new ContactUsItem(R.string.sales_premier_hotline_title,
                R.string.sales_premier_hotline_no, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.sales_premier_hotline_no))));
        alSales.add(new ContactUsItem(R.string.support_sales_csl_hotline_title,
                R.string.support_sales_csl_hotline_no, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_sales_csl_hotline_no))));

        cuAdapterSales.addItemEntries(alSales);
        rvSales.setAdapter(cuAdapterSales);
        rvSales.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareEmail() {
        ArrayList<ContactUsItem> alEmail = new ArrayList<>();

        alEmail.add(new ContactUsItem(R.string.support_fixed_line_service_title,
                R.string.support_fixed_line_service_email_info, R.drawable.btn_mail,
                R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.support_fixed_line_service_email_info))));
        alEmail.add(new ContactUsItem(R.string.support_consumer_email,
                R.string.support_consumer_email_info, R.drawable.btn_mail, R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.support_consumer_email_info))));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_pcdTitle,
                -1, -1, -1,
                () -> Log.d(TAG, "Nothing to click")));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_GI,
                R.string.myhkt_cu_pcdemail_cs, R.drawable.btn_mail, R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.myhkt_cu_pcdemail_cs))));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_TS,
                R.string.myhkt_cu_pcdemail_ts, R.drawable.btn_mail, R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.myhkt_cu_pcdemail_ts))));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_tvTitle,
                -1, -1, -1,
                () -> Log.d(TAG, "Nothing to click")));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_GI,
                R.string.myhkt_cu_tvemail_cs, R.drawable.btn_mail, R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.myhkt_cu_tvemail_cs))));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_TS,
                R.string.myhkt_cu_tvemail_ts, R.drawable.btn_mail, R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.myhkt_cu_tvemail_ts))));

        cuAdapterEmail.addItemEntries(alEmail);
        rvEmail.setAdapter(cuAdapterEmail);
        rvEmail.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareOthers() {
        ArrayList<ContactUsItem> alOthers = new ArrayList<>();

        String facebookOfficial = getString(R.string.support_consumer_others_fb) + "\n" +
                getString(R.string.support_consumer_others_fbinfo);
        String instagramOfficial = getString(R.string.support_consumer_others_ig) + "\n" +
                getString(R.string.support_consumer_others_iginfo);
        alOthers.add(new ContactUsItem(facebookOfficial, -1, R.drawable.ic_facebook, -1,
                () -> presenter.triggerFacebook()));
        alOthers.add(new ContactUsItem(instagramOfficial, -1,
                R.drawable.ic_instagram, -1,
                () -> presenter.triggerInstagram()));

        cuAdapterOthers.addItemEntries(alOthers);
        rvOthers.setAdapter(cuAdapterOthers);
        rvOthers.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onTriggeredLiveChat(int stringId) {
        ContactUsNewActivity activity = (ContactUsNewActivity) getActivity();
        if (!ClnEnv.isLoggedIn()) {
            activity.loginFirst(HomeButtonItem.MAINMENU.CONTACTUS);
        } else {
            if (stringId != -1)
                activity.openLiveChat(getContext().getString(stringId));
            else
                activity.openLiveChat();
        }
    }

    @Override
    public void onTriggeredCall(String phone) {
        ContactUsNewActivity activity = (ContactUsNewActivity) getActivity();
        Objects.requireNonNull(activity).callPhone(phone);
    }

    @Override
    public void onTriggeredEmail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.fromParts(
                "mailto", email, null)); // only email apps should handle this
        if (intent.resolveActivity(requireActivity().getPackageManager()) != null) {
            //to app directly
            startActivity(intent);
        } else {
            DialogHelper.createSimpleDialog(getActivity(), "No app can perform this action");
        }
    }

    @Override
    public void onTriggeredFacebook() {
        String fburl = "https://www.facebook.com/n/?pccwcs";
        PackageManager packageManager = requireContext().getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            boolean activated = packageManager.getApplicationInfo("com.facebook.katana", 0).enabled;
            if (activated && versionCode >= 3002850) { // new FB app
                fburl = "fb://page/200383099984165";
            }
        } catch (PackageManager.NameNotFoundException e) {
            fburl = "https://www.facebook.com/n/?pccwcs"; //normal web url
        }
        Intent fbintent = new Intent(Intent.ACTION_VIEW);
        fbintent.setData(Uri.parse(fburl));
        startActivity(fbintent);
    }

    @Override
    public void onTriggeredInstagram() {
        String igUrl = "https://www.instagram.com/pccwcs";
        Intent igintent = new Intent(Intent.ACTION_VIEW);
        igintent.setData(Uri.parse(igUrl));
        startActivity(igintent);
    }

    @Override
    public void onTriggeredWhatsApp(String mobile) {
        DialogHelper.createSimpleDialog(getActivity(), getString(R.string.contact_us_redirect_message), getString(R.string.btn_ok), (dialog, which) -> {
            Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/+852" + mobile.replace(" ", "")));
            startActivity(intentBrowser);
        }, getString(R.string.btn_cancel));
    }
}
