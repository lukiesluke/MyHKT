package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.MobUsage;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3UsageQuotaDTO;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageDataListener;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageListener;
import com.pccw.myhkt.lib.ui.AAQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/************************************************************************
File       : UsageDataChild2Fragment.java
Desc       : View for showing csl/1010/o2f Usage/Roaming
			 isMob: true for csl , false for 1010/o2f case
			 isLocal: true for local data, false for roaming
			 csl(H-sim) does not have this view for roaming(isMob :true, isLocal : false)
			 PlanMobCra is obtained from usageData fragment 
Name       : UsageDataChild2Fragment
Created by : Andy Wong
Date       : 15/03/2016

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
15/03/2016 Andy Wong			- First draft
13/06/2017 Abdulmoiz Esmail     - 59541: [HSim] Usage - Blackberry logic change
15/06/2017 Abdulmoiz Esmail		- 59645: Change HSim Normal Sim header as Secondary Sim in Usage
 *************************************************************************/
public class UsageDataChild2Fragment extends BaseServiceFragment{
	private UsageDataChild2Fragment me;
	private View myView;
	private AAQuery aq;
	private List<Cell> cellList;
	private int deviceWidth ;
	private int extralinespace;
	private int basePadding;
	private int col2Width ;
	private int col3Width ;
	private int[] colWidths;
	private Boolean isMob = false;
	private Boolean isCSimPrim = false;
	private Boolean isCSimProgressShown = false;
	private Boolean isLocal = true;
	private PlanMobCra planMobCra;
	private MobUsage mobUsage;
	private LinearLayout frame;
	private G3UsageQuotaDTO g3UsageQuotaDTO;
	private CellViewAdapter cellViewAdapter;
	private OnUsageDataListener callback;
	private OnUsageListener callback_usage;
	
	private Boolean 	isMup = false;
	private Boolean 	isMaster = false;
	
	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		try {
			callback = (OnUsageDataListener) getParentFragment();
			callback_usage = (OnUsageListener) getParentFragment().getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString() + " must implement OnUsageListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (debug) Log.i("lwg", "attach:UsageDataChild2Fragment");
		if (callback_main != null && callback_main.getAcctAgent() != null && callback_main.getAcctAgent().getSubnRec() != null && !TextUtils.isEmpty(callback_main.getAcctAgent().getSubnRec().lob)) {
			isMob = (SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob) || SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob)
					|| SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
					|| SubnRec.LOB_CSP.equals(callback_main.getAcctAgent().getSubnRec().lob)
					|| SubnRec.WLOB_XCSP.equals(callback_main.getAcctAgent().getSubnRec().lob));
		}
		isLocal = callback_usage.getIsLocal();
		if (callback.getPlanMobCra() != null && callback.getPlanMobCra().getOMobUsage() != null) {
			G3UsageQuotaDTO g3OtherUsageDTO = callback.getPlanMobCra().getOMobUsage().getG3UsageQuotaDTO();
			if (g3OtherUsageDTO != null) {
				isCSimPrim = "P".equalsIgnoreCase(g3OtherUsageDTO.getPrimaryFlag());
			}
		}
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_usagedata_child2, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}

	protected void initData() {
		Display display = me.requireActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		col2Width = (deviceWidth - extralinespace * 2) / 4;
		col3Width = (deviceWidth - basePadding * 2) * 2 / 9;
		planMobCra = callback.getPlanMobCra();
		cellViewAdapter = new CellViewAdapter(me.getActivity());
	}

	protected void initUI() {
		planMobCra = callback.getPlanMobCra();
		if (planMobCra == null) {
			if (isMob) {
				// initMobUI();
			} else {
				// init1010UI();
			}
		} else {
			if (isMob) {
				if (isLocal) {
					setMobUI();
				}
			} else {
				set1010UI();
			}
		}
	}
	
	/**
	 * 06/13/17 Moiz : Updated showing Local Black email Email data 
	 * 				   Check if master, if yes check if grp_local_email then show BB group data usage
	 * 				   else if not master, then show BB email data if used_local_email
	 */
	protected void setMobUI() {
		if (debug) Log.i(TAG, "T1");
		aq = new AAQuery(myView);
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();
		if (debug) Log.i(TAG, "T2");
		cellList = new ArrayList<Cell>();
		planMobCra = callback.getPlanMobCra();


		if (planMobCra != null) {
			planMobCra = callback.getPlanMobCra();
			mobUsage = planMobCra.getOMobUsage();
			g3UsageQuotaDTO = mobUsage.getG3UsageQuotaDTO();
			isMup = planMobCra.isMup();
			isMaster = planMobCra.isMaster();
		}
		if (!isMup) {
			int[] colWidths = {col2Width, col2Width};
			String entitled = getResString(R.string.usage_individual_entitled);
			String used = getResString(R.string.usage_individual);
			String normal = getResString(R.string.usage_normal);
			String inter = getResString(R.string.usage_inter_network);
			String intra = getResString(R.string.usage_intra_network);
			String oversea = getResString(R.string.usage_overseas);
			String subTitle = "";
			if (debug) Log.i(TAG, "T3");
			if (g3UsageQuotaDTO.getAsOfDate() != null) {
				String outStr = "";
				try {
					outStr = Utils.toDateString(g3UsageQuotaDTO.getAsOfDate(), "dd/MM/yyyy", Utils.getString(me.getActivity(), R.string.usage_date_format));
				} catch (Exception e) {
					outStr = g3UsageQuotaDTO.getAsOfDate();
				}
				subTitle = String.format("%s%s", getResString(R.string.as_of), outStr);
			}

			if (debug) Log.i(TAG, "T4");
			//Local Video call airtime
			String videoCallTitle = getResString(R.string.usage_video_call);
			String[] videoCallCols = {entitled, used};
			String[] videoCallUnits = {"(" + getResString(R.string.mins) + ")", "(" + getResString(R.string.mins) + ")"};
			String[] videoCallRowTitle = {normal, intra};
			String[] videoCallNormalData = {convertNum(g3UsageQuotaDTO.getLocalVideoInterEntitlement()), convertNum(g3UsageQuotaDTO.getLocalVideoInterUsage())};
			String[] videoCallIntraData = {convertNum(g3UsageQuotaDTO.getLocalVideoIntraEntitlement()), convertNum(g3UsageQuotaDTO.getLocalVideoIntraUsage())};
			String[][] videoCallRowData = {videoCallNormalData, videoCallIntraData};
			int voicecallImg = Utils.theme(R.drawable.icon_videocall, callback_usage.getLob()); //themed icon
			addPart(voicecallImg, videoCallTitle, subTitle, videoCallCols, videoCallUnits, colWidths, videoCallRowTitle, videoCallRowData);

			//Local BB email Data
			//Show only when user used"
			double used_local_email = 0;
			double total_local_email = 0;
			try {
				used_local_email = Double.parseDouble(g3UsageQuotaDTO.getEmailUsage());
				total_local_email = Double.parseDouble(g3UsageQuotaDTO.getEmailEntitlement());
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (used_local_email > 0) {
				String emailTitle = getResString(R.string.usage_email_usage);
				String[] emailCols = {entitled, used};
				String[] emailUnits = {"(" + getResString(R.string.MB) + ")", "(" + getResString(R.string.MB) + ")"};
				String[] emailRowTitle = {""};
				String[] emailNormalData = {convertNum(g3UsageQuotaDTO.getEmailEntitlement()), convertNum(g3UsageQuotaDTO.getEmailUsage())};
				String[][] emailRowData = {emailNormalData};

				int mmsImg = Utils.theme(R.drawable.icon_mms, callback_usage.getLob()); //themed icon
				addPart(mmsImg, emailTitle, subTitle, emailCols, emailUnits, colWidths, emailRowTitle, emailRowData);
			}

			//Local SMS
			String smsTitle = getResString(R.string.usage_sms_usage);
			String[] smsCols = {used};
			String[] smsUnits = {"(" + getResString(R.string.messages) + ")"};
			String[] smsRowTitle = {inter, intra, oversea};
			String[] smsInterlData = {convertNum(g3UsageQuotaDTO.getLocalSMSInterUsage())};
			String[] smsIntraData = {convertNum(g3UsageQuotaDTO.getLocalSMSIntraUsage())};
			String[] smsOverSeaData = {convertNum(g3UsageQuotaDTO.getOverseasSMSUsage())};
			String[][] smsRowData = {smsInterlData, smsIntraData, smsOverSeaData};
			int smsImg = Utils.theme(R.drawable.icon_sms, callback_usage.getLob()); //themed icon
			addPart(smsImg, smsTitle, subTitle, smsCols, smsUnits, colWidths, smsRowTitle, smsRowData);

			//Local MMS
			String mmsTitle = getResString(R.string.usage_mms_usage);
			String[] mmsCols = {used};
			String[] mmsUnits = {"(" + getResString(R.string.mms) + ")"};
			String[] mmsRowTitle = {inter, intra, oversea};
			String[] mmsInterlData = {convertNum(g3UsageQuotaDTO.getLocalMMSInterUsage())};
			String[] mmsIntraData = {convertNum(g3UsageQuotaDTO.getLocalMMSIntraUsage())};
			String[] mmsOverSeaData = {convertNum(g3UsageQuotaDTO.getOverseasMMSUsage())};
			String[][] mmsRowData = {mmsInterlData, mmsIntraData, mmsOverSeaData};
			int mmsImg = Utils.theme(R.drawable.icon_mms, callback_usage.getLob()); //themed icon
			addPart(mmsImg, mmsTitle, subTitle, mmsCols, mmsUnits, colWidths, mmsRowTitle, mmsRowData);

		} else {
			int[] colWidths = {col2Width};
			int[] col2Widths = {col2Width, col2Width};
			int[] col3Widths = {col3Width, col3Width, col3Width};
			String entitled = getResString(R.string.usage_individual_entitled);
			String entitled1 = getResString(R.string.usage_entitled);
			String indUsed = getResString(R.string.usage_individual);
			String qroupUsed = getResString(R.string.usage_group);
			String normal = getResString(R.string.usage_normal);
			String inter = getResString(R.string.usage_inter_network);
			String intra = getResString(R.string.usage_intra_network);
			String oversea = getResString(R.string.usage_overseas);
			String subTitle = "";

			if (g3UsageQuotaDTO.getAsOfDate() != null) {
				String outStr = "";
				try {
					outStr = Utils.toDateString(g3UsageQuotaDTO.getAsOfDate(), "dd/MM/yyyy", Utils.getString(me.getActivity(), R.string.usage_date_format));
				} catch (Exception e) {
					outStr = g3UsageQuotaDTO.getAsOfDate();
				}
				subTitle = String.format("%s%s", getResString(R.string.as_of), outStr);
			}

			if (debug) Log.i(TAG, "T4");
			//Local Video call airtime
			String videoCallTitle = getResString(R.string.usage_video_call);
			String[] videoCallCols = {entitled, indUsed};
			String[] videoCallColsM = {entitled, indUsed, qroupUsed};
			String[] videoCallUnits = {"(" + getResString(R.string.mins) + ")", "(" + getResString(R.string.mins) + ")"};
			String[] videoCallUnitsM = {"(" + getResString(R.string.mins) + ")", "(" + getResString(R.string.mins) + ")", "(" + getResString(R.string.mins) + ")"};
			String[] videoCallRowTitle = {normal, intra};
			String[] videoCallNormalData = {convertNum(g3UsageQuotaDTO.getLocalVideoInterEntitlement()), convertNum(g3UsageQuotaDTO.getLocalVideoInterUsage())};
			String[] videoCallNormalDataM = {convertNum(g3UsageQuotaDTO.getLocalVideoInterEntitlement()), convertNum(g3UsageQuotaDTO.getLocalVideoInterUsage()), convertNum(g3UsageQuotaDTO.getLocalVideoInterUsageGrp())};
			String[] videoCallIntraData = {convertNum(g3UsageQuotaDTO.getLocalVideoIntraEntitlement()), convertNum(g3UsageQuotaDTO.getLocalVideoIntraUsage())};
			String[] videoCallIntraDataM = {convertNum(g3UsageQuotaDTO.getLocalVideoIntraEntitlement()), convertNum(g3UsageQuotaDTO.getLocalVideoIntraUsage()), convertNum(g3UsageQuotaDTO.getLocalVideoIntraUsageGrp())};
			String[][] videoCallRowData = {videoCallNormalData, videoCallIntraData};
			String[][] videoCallRowDataM = {videoCallNormalDataM, videoCallIntraDataM};
			int voicecallImg = Utils.theme(R.drawable.icon_videocall, callback_usage.getLob()); //themed icon
			if (isMaster) {
				addPart(voicecallImg, videoCallTitle, subTitle, videoCallColsM, videoCallUnitsM, col3Widths, videoCallRowTitle, videoCallRowDataM);
			} else {
				addPart(voicecallImg, videoCallTitle, subTitle, videoCallCols, videoCallUnits, col2Widths, videoCallRowTitle, videoCallRowData);
			}

			//Local BB email Data
			//Show only when user used"
			double used_local_email = 0;
			double total_local_email = 0;
			double grp_local_email = 0;
			String emailUnit = g3UsageQuotaDTO.getEmailUnit();
			try {
				used_local_email = Double.parseDouble(g3UsageQuotaDTO.getEmailUsage());
				total_local_email = Double.parseDouble(g3UsageQuotaDTO.getEmailEntitlement());
				grp_local_email = Double.parseDouble(g3UsageQuotaDTO.getEmailUsageGrp());
			} catch (Exception e) {
				e.printStackTrace();
			}

			boolean isBBShow = false;
			if (isMaster) {
				if (grp_local_email > 0) {
					isBBShow = true;                //Show group BB local data usage
				}
			} else {
				if (used_local_email > 0) {
					isBBShow = true;                //Show individual BB local data usage (no group)
				}
			}

			if (isBBShow) {
				String emailTitle = getResString(R.string.usage_email_usage);
				String[] emailCols = {entitled, indUsed};
				String[] emailColsM = {entitled, indUsed, qroupUsed};
				String[] emailUnits = {"(" + emailUnit + ")", "(" + emailUnit + ")"};
				String[] emailUnitsM = {"(" + emailUnit + ")", "(" + emailUnit + ")", "(" + emailUnit + ")"};
				String[] emailRowTitle = {""};
				String[] emailNormalData = {convertNum(g3UsageQuotaDTO.getEmailEntitlement()), convertNum(g3UsageQuotaDTO.getEmailUsage())};
				String[] emailNormalDataM = {convertNum(g3UsageQuotaDTO.getEmailEntitlement()), convertNum(g3UsageQuotaDTO.getEmailUsage()), convertNum(g3UsageQuotaDTO.getEmailUsageGrp())};
				String[][] emailRowData = {emailNormalData};
				String[][] emailRowDataM = {emailNormalDataM};

				int mmsImg = Utils.theme(R.drawable.icon_mms, callback_usage.getLob()); //themed icon
				if (isMaster) {
					addPart(mmsImg, emailTitle, subTitle, emailColsM, emailUnitsM, col3Widths, emailRowTitle, emailRowDataM);
				} else {
					addPart(mmsImg, emailTitle, subTitle, emailCols, emailUnits, col2Widths, emailRowTitle, emailRowData);
				}
			}

			//Local SMS
			String smsTitle = getResString(R.string.usage_sms_usage);
			String[] smsCols = {indUsed};
			String[] smsColsM = {indUsed, qroupUsed};
			String[] smsUnits = {"(" + getResString(R.string.messages) + ")"};
			String[] smsUnitsM = {"(" + getResString(R.string.messages) + ")", "(" + getResString(R.string.messages) + ")"};
			String[] smsRowTitle = {inter, intra, oversea};
			String[] smsInterlData = {convertNum(g3UsageQuotaDTO.getLocalSMSInterUsage())};
			String[] smsInterlDataM = {convertNum(g3UsageQuotaDTO.getLocalSMSInterUsage()), convertNum(g3UsageQuotaDTO.getLocalSMSInterUsageGrp())};
			String[] smsIntraData = {convertNum(g3UsageQuotaDTO.getLocalSMSIntraUsage())};
			String[] smsIntraDataM = {convertNum(g3UsageQuotaDTO.getLocalSMSIntraUsage()), convertNum(g3UsageQuotaDTO.getLocalSMSIntraUsageGrp())};
			String[] smsOverSeaData = {convertNum(g3UsageQuotaDTO.getOverseasSMSUsage())};
			String[] smsOverSeaDataM = {convertNum(g3UsageQuotaDTO.getOverseasSMSUsage()), convertNum(g3UsageQuotaDTO.getOverseasSMSUsageGrp())};
			String[][] smsRowData = {smsInterlData, smsIntraData, smsOverSeaData};
			String[][] smsRowDataM = {smsInterlDataM, smsIntraDataM, smsOverSeaDataM};
			int smsImg = Utils.theme(R.drawable.icon_sms, callback_usage.getLob()); //themed icon
			if (isMaster) {
				addPart(smsImg, smsTitle, subTitle, smsColsM, smsUnitsM, col2Widths, smsRowTitle, smsRowDataM);
			} else {
				addPart(smsImg, smsTitle, subTitle, smsCols, smsUnits, colWidths, smsRowTitle, smsRowData);
			}

			//Local MMS
			String mmsTitle = getResString(R.string.usage_mms_usage);
			String[] mmsCols = {indUsed};
			String[] mmsColsM = {indUsed, qroupUsed};
			String[] mmsUnits = {"(" + getResString(R.string.mms) + ")"};
			String[] mmsUnitsM = {"(" + getResString(R.string.mms) + ")", "(" + getResString(R.string.mms) + ")"};
			String[] mmsRowTitle = {inter, intra, oversea};
			String[] mmsInterlData = {convertNum(g3UsageQuotaDTO.getLocalMMSInterUsage())};
			String[] mmsInterlDataM = {convertNum(g3UsageQuotaDTO.getLocalMMSInterUsage()), convertNum(g3UsageQuotaDTO.getLocalMMSInterUsageGrp())};
			String[] mmsIntraData = {convertNum(g3UsageQuotaDTO.getLocalMMSIntraUsage())};
			String[] mmsIntraDataM = {convertNum(g3UsageQuotaDTO.getLocalMMSIntraUsage()), convertNum(g3UsageQuotaDTO.getLocalMMSIntraUsageGrp())};
			String[] mmsOverSeaData = {convertNum(g3UsageQuotaDTO.getOverseasMMSUsage())};
			String[] mmsOverSeaDataM = {convertNum(g3UsageQuotaDTO.getOverseasMMSUsage()), convertNum(g3UsageQuotaDTO.getOverseasMMSUsage())};
			String[][] mmsRowData = {mmsInterlData, mmsIntraData, mmsOverSeaData};
			String[][] mmsRowDataM = {mmsInterlDataM, mmsIntraDataM, mmsOverSeaDataM};
			int mmsImg = Utils.theme(R.drawable.icon_mms, callback_usage.getLob()); //themed icon
			if (isMaster) {
				addPart(mmsImg, mmsTitle, subTitle, mmsColsM, mmsUnitsM, col2Widths, mmsRowTitle, mmsRowDataM);
			} else {
				addPart(mmsImg, mmsTitle, subTitle, mmsCols, mmsUnits, colWidths, mmsRowTitle, mmsRowData);
			}
		}
		//addRemark();
		cellViewAdapter.setView(frame, cellList);
	}

	protected void set1010UI() {
		if (debug) Log.i(TAG, "T1");
		aq = new AAQuery(myView);
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();

		cellList = new ArrayList<Cell>();
		planMobCra = callback.getPlanMobCra();
		if (planMobCra !=null) {
			planMobCra = callback.getPlanMobCra();
			mobUsage = planMobCra.getOMobUsage();
			g3UsageQuotaDTO = mobUsage.getG3UsageQuotaDTO();
		}
		// Progress Bar show
		isCSimProgressShown = planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO() != null &&
				planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList() != null &&
				!planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList().isEmpty() &&
				planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList().get(0) != null;

		int[] colWidths = {col2Width, col2Width};	
		if (debug) Log.i(TAG, "T2");
		String asOfDate = "";
		String dataAsOfDate = "";
		String total = getResString(R.string.csl_usage_total);
		String mins = getResString(R.string.DLGM_MIN);
		String[] minsOne = {mins};
		String[] minsTwo = {"", mins};
		String msg = getResString(R.string.csl_messages);
		String[] msgOne = {msg};
		String[] msgTwo = {"", msg};
		String roaming = getResString(R.string.usage_roaming);
		String idd = "IDD";
		String international = getResString(R.string.csl_international);
		String indiv = getResString(R.string.csl_usage_individual);
		String local = getResString(R.string.usage_local);
		String[] primCols = { indiv , total };
		String[] othereCol = {total};
		g3UsageQuotaDTO = mobUsage.getG3UsageQuotaDTO();
		G3UsageQuotaDTO g3SummUsageQuotaDTO = mobUsage.getG3SummUsageQuotaDTO();
		if (debug) Log.i(TAG, "T3");
		//Mobile Data as of date
		dataAsOfDate = !isCSimPrim ? g3UsageQuotaDTO.getDataAsOfDate() : g3SummUsageQuotaDTO.getDataAsOfDate();
		
		if (dataAsOfDate != null) {
			String outStr = "";
			try {
				outStr = Utils.toDateString(g3UsageQuotaDTO.getDataAsOfDate(), "yyyyMMdd", Utils.getString(me.getActivity(), R.string.usage_date_format));
			} catch (Exception e) {
				outStr = isLocal ? g3UsageQuotaDTO.getDataAsOfDate() : g3SummUsageQuotaDTO.getDataAsOfDate();
			}
			dataAsOfDate = String.format("%s%s", Utils.getString(me.requireActivity(), R.string.as_of), outStr);
		}

		//As of Date
		asOfDate = !isCSimPrim ? g3UsageQuotaDTO.getAsOfDate() : g3SummUsageQuotaDTO.getAsOfDate();
		if (asOfDate != null) {
			String outStr = "";
			try {
				outStr = Utils.toDateString(g3UsageQuotaDTO.getAsOfDate(), "dd/MM/yyyy", Utils.getString(me.requireActivity(), R.string.usage_date_format));
			} catch (Exception e) {
				outStr = isLocal ? g3UsageQuotaDTO.getAsOfDate() : g3SummUsageQuotaDTO.getAsOfDate();
			}
			asOfDate = String.format("%s%s", getResString(R.string.as_of), outStr);
		}		
		if (debug) Log.i(TAG, "T4");
		//mobile data
		String mobileDataTitle = getResString(R.string.csl_usage_mobile_data);		
		String[] moblleDataRowTitle = {local} ;
		String indivlDataUsage = isLocal ? g3UsageQuotaDTO.getMobileLocalDataUsage() : g3UsageQuotaDTO.getMobileRoamingDataUsage();
		String indivlDataUnit = isLocal ? g3UsageQuotaDTO.getMobileLocalDataUnit() : g3UsageQuotaDTO.getMobileRoamingDataUnit();
		indivlDataUsage = (indivlDataUsage == null || "null".equals(indivlDataUsage)) ? "-" : indivlDataUsage;
		indivlDataUnit = (indivlDataUnit == null || "null".equals(indivlDataUnit)) ? "" : indivlDataUnit;
		String indivlData = String.format("%s %s", indivlDataUsage,indivlDataUnit);
		if (isCSimPrim) {
			//Primary VBP
			String totalDataUsage = isLocal ? g3SummUsageQuotaDTO.getMobileLocalDataUsage() : g3SummUsageQuotaDTO.getMobileRoamingDataUsage();
			String totalDataUnit = isLocal ? g3SummUsageQuotaDTO.getMobileLocalDataUnit() : g3SummUsageQuotaDTO.getMobileRoamingDataUnit();
			totalDataUsage = (totalDataUsage == null || "null".equals(totalDataUsage)) ? "-" : totalDataUsage;
			totalDataUnit = (totalDataUnit == null || "null".equals(totalDataUnit)) ? "" : totalDataUnit;
			String totalData = String.format("%s %s", totalDataUsage,totalDataUnit);
//			String[] mobileDataCols = {callback_main.getAcctAgent().getSrvNum(), total};
			String[] mobileDataCols = {indiv, total};
			String[] mobileDataUnits = {"", "" };			
			String[][] mobileDataRowData = {{indivlData,totalData}};
			mobileDataTitle = mobileDataTitle + " - " + callback_main.getAcctAgent().getSrvNum();
			int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
			addPart(mobusageImg, mobileDataTitle, dataAsOfDate, mobileDataCols, mobileDataUnits, colWidths, moblleDataRowTitle ,mobileDataRowData);
		} else {
			//Secondary VBP , Single number VBP, Non VBP
			String[] mobileDataCols = {total};			
			String[][] mobileDataRowData = {{indivlData}};
			if (!isCSimProgressShown || !isLocal) {
				// Single number VBP with no topup 
				mobileDataTitle = mobileDataTitle + " - " + callback_main.getAcctAgent().getSrvNum();
				int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
				addPart(mobusageImg, mobileDataTitle, dataAsOfDate, mobileDataCols, null, colWidths, moblleDataRowTitle ,mobileDataRowData);
			}
		}
		if (debug) Log.i(TAG, "T5");
		// Voice
		String voiceTitle = getResString(R.string.csl_voice_call);
		String[] voiceCols = isCSimPrim ? primCols : othereCol ;
		String[] voiceRowTitle = {isLocal? local : idd};

		String[] voiceUnit = isCSimPrim ? minsTwo : minsOne;
		String voiceIndivData = isLocal ? g3UsageQuotaDTO.getLocalVoiceInterUsage() : g3UsageQuotaDTO.getIddVoiceUsage();
		voiceIndivData = (voiceIndivData == null) ? "-" : voiceIndivData;
		if (isCSimPrim) {
			String voiceTotalData = isLocal ? g3SummUsageQuotaDTO.getLocalVoiceInterUsage() : g3SummUsageQuotaDTO.getIddVoiceUsage();
			voiceTotalData = (voiceTotalData == null) ? "-" : voiceTotalData;
			String[][] voiceRowData = {{voiceIndivData, voiceTotalData}};
			int voicecallImg = Utils.theme(R.drawable.icon_voicecall, callback_usage.getLob()); //themed icon
			addPart(voicecallImg, voiceTitle, asOfDate, voiceCols, voiceUnit, colWidths, voiceRowTitle ,voiceRowData);
		} else {
			String[][] voiceRowData = {{voiceIndivData}};
			int voicecallImg = Utils.theme(R.drawable.icon_voicecall, callback_usage.getLob()); //themed icon
			addPart(voicecallImg, voiceTitle, asOfDate, voiceCols, voiceUnit, colWidths, voiceRowTitle ,voiceRowData);
		}
		if (debug) Log.i(TAG, "T6");
		// SMS
		String smsTitle = getResString(R.string.messages);
		String[] smsCols = isCSimPrim ? primCols : othereCol;
		String[] smsUnit = isCSimPrim ? msgTwo : msgOne;

		if (isLocal) {				
			String[] smsRowTitle = {local} 	;
			
			String smsIndivData =  g3UsageQuotaDTO.getLocalSMSInterUsage();
			smsIndivData = (smsIndivData == null) ? "-" : smsIndivData;	
			if (isCSimPrim) {
				String smsTotalData = isLocal ?g3SummUsageQuotaDTO.getLocalSMSInterUsage() : g3SummUsageQuotaDTO.getRoamSMSUsage();
				smsTotalData = (smsTotalData == null) ? "-" : smsTotalData;	
				String[][] smsRowData = {{smsIndivData, smsTotalData}};
				int smsImg = Utils.theme(R.drawable.icon_sms, callback_usage.getLob()); //themed icon
				addPart(smsImg, smsTitle, asOfDate, smsCols, smsUnit, colWidths, smsRowTitle ,smsRowData);
			} else {
				String[][] smsRowData = {{smsIndivData}};
				int smsImg = Utils.theme(R.drawable.icon_sms, callback_usage.getLob()); //themed icon
				addPart(smsImg, smsTitle, asOfDate, smsCols, smsUnit, colWidths, smsRowTitle ,smsRowData);
			}
		} else {
			String[] smsRowTitle = { roaming, international};
			String smsIndivRoamingData =  g3UsageQuotaDTO.getRoamSMSUsage();
			smsIndivRoamingData = (smsIndivRoamingData == null) ? "-" : smsIndivRoamingData;	
			String smsIndivInterData =  g3UsageQuotaDTO.getOverseasSMSUsage();
			smsIndivInterData = (smsIndivInterData == null) ? "-" : smsIndivInterData;	
			if (isCSimPrim) {				
				String smsTotalRoamingData =  g3SummUsageQuotaDTO.getRoamSMSUsage();
				smsTotalRoamingData = (smsIndivRoamingData == null) ? "-" : smsIndivRoamingData;	
				String smsTotalInterData = g3SummUsageQuotaDTO.getRoamSMSUsage();
				smsTotalInterData = (smsTotalInterData == null) ? "-" : smsTotalInterData;	
				String[][] smsRowData = {{smsIndivRoamingData, smsTotalRoamingData}, {smsIndivInterData, smsTotalInterData}};
				int smsImg = Utils.theme(R.drawable.icon_sms, callback_usage.getLob()); //themed icon
				addPart(smsImg, smsTitle, asOfDate, smsCols, smsUnit, colWidths, smsRowTitle ,smsRowData);
			} else {
				String[][] smsRowData = {{smsIndivRoamingData}, {smsIndivInterData}};
				int smsImg = Utils.theme(R.drawable.icon_sms, callback_usage.getLob()); //themed icon
				addPart(smsImg, smsTitle, asOfDate, smsCols, smsUnit, colWidths, smsRowTitle ,smsRowData);
			}
		}
		if (debug) Log.i(TAG, "T7");
		// BlackBerry
		String bbTitle = getResString(R.string.csl_blackberry);
		String[] bbCols = isCSimPrim ? primCols : othereCol ;
		String[] bbRowTitle = {isLocal ? local : roaming};
		String bbDataUnit = isLocal ? g3UsageQuotaDTO.getLocalBBUnit() : g3UsageQuotaDTO.getRoamBBUnit();
		bbDataUnit = (bbDataUnit == null || "null".equals(bbDataUnit)) ? "" : bbDataUnit;
		
		String bbIndivData = isLocal ? g3UsageQuotaDTO.getLocalBBUsage() : g3UsageQuotaDTO.getRoamBBUsage();
		bbIndivData = (bbIndivData == null || "null".equals(bbIndivData)) ? "-" : bbIndivData;
		bbIndivData = String.format("%s %s", bbIndivData, bbDataUnit);
		
		if (isCSimPrim) {
			String bbTotalData = isLocal ? g3SummUsageQuotaDTO.getLocalBBUsage() : g3SummUsageQuotaDTO.getRoamBBUsage();
			bbTotalData = (bbTotalData == null || "null".equals(bbTotalData)) ? "-" : bbTotalData;
			bbTotalData = String.format("%s %s", bbTotalData, bbDataUnit);
			String[][] bbRowData = {{bbIndivData, bbTotalData}};
			int mmsImg = Utils.theme(R.drawable.icon_bbmail, callback_usage.getLob()); //themed icon
			addPart(mmsImg, bbTitle, asOfDate, bbCols, null, colWidths, bbRowTitle ,bbRowData);
		} else {
			String[][] bbRowData = {{bbIndivData}};
			int mmsImg = Utils.theme(R.drawable.icon_bbmail, callback_usage.getLob()); //themed icon
			addPart(mmsImg, bbTitle, asOfDate, bbCols, null, colWidths, bbRowTitle ,bbRowData);
		}
		//addRemark();
		cellViewAdapter.setView(frame, cellList);
	}

	private void addPart(int icon, String title, String subTitle, String[] colname, String[] units, int[] colWidths, String[] rowTitle, String[][] rowData) {

		IconTextCell cellTitle = new IconTextCell(icon, title);
		cellTitle.setLeftPadding(basePadding);
		cellList.add(cellTitle);
		ArrowTextCell arrowTextCell = new ArrowTextCell(subTitle, colname, units, colWidths);
		arrowTextCell.setNoteSizeDelta(-4);
		arrowTextCell.setContentSizeDelta(-4);
		arrowTextCell.setArrowShown(true);
		cellList.add(arrowTextCell);
		for (int i = 0; i < rowTitle.length; i++) {
			ArrowTextCell cell = new ArrowTextCell(rowTitle[i], rowData[i], null, colWidths, i % 2 == 0 ? R.color.cell_bg_grey : R.color.white, R.color.hkt_textcolor);
			cell.setNoteSizeDelta(-4);
			cell.setContentSizeDelta(-4);
			cell.setTitleWrapContent(false);
			cellList.add(cell);
		}
	}

	//Remark will be add at bottom
	public void addRemark() {
		Cell line = new Cell(Cell.LINE);
		line.setBotMargin(basePadding / 2);
		line.setTopMargin(basePadding / 2);
		cellList.add(line);
		SmallTextCell remarkCell = new SmallTextCell(getResString(R.string.usage_reference_remark), "");
		remarkCell.setRightPadding(basePadding);
		remarkCell.setLeftPadding(basePadding);
		remarkCell.setTitleSizeDelta(-5);
		cellList.add(remarkCell);
	}

	protected void cleanupUI() {

	}

	public void refreshData() {
		super.refreshData();
		initUI();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (callback_main != null) {
			if (!Utils.getString(requireActivity(), R.string.CONST_SCRN_MOBUSAGE).equals(callback_main.getTrackerId()) && callback_usage.getIsLocal()) {
				//Screen Tracker
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_MOBUSAGE, false);
				if (debug)
					Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE));
			} else if (!Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING).equals(callback_main.getTrackerId()) && !callback_usage.getIsLocal()) {
				//Screen Tracker
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_MOBUSAGE_ROAMING, false);
				if (debug)
					Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING));
			}
		}
	}

	public void refresh() {
		super.refresh();
		initUI();
	}


	private String convertNum(String num) {
		if (num != null) num = num.trim();
		if (num != null && num.length() != 0) {
			if (num.equalsIgnoreCase("UNLIMIT")) {
				return getResString(R.string.usage_unlimited);
			} else {
				return Utils.formatNumber(Utils.truncateStringDot(num));
			}
		} else {
			return "-";
		}
	}

	public static double parseDouble(String str) {
		str = str.trim();
		str = str.replace("$", "");
		str = str.replace(",", "");

		try {
			return Double.parseDouble(str);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
	}
}
