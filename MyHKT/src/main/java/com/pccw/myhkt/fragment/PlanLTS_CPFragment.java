package com.pccw.myhkt.fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.AbsListView.LayoutParams;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.PlanLtsCra;
import com.pccw.dango.shared.entity.SrvPlan;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.DetailBtnCell;
import com.pccw.myhkt.cell.model.PlanTextIconCell;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.TextIconCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.PlanLTSMainFragment.OnPlanLtsListener;
import com.pccw.myhkt.fragment.PlanLTS_TPFragment.OnPlanMainLtsListener;
import com.pccw.myhkt.lib.ui.AAQuery;

/************************************************************************
 * File : PlanLTSFragment.java Desc : LTS Plan Name : PlanLTSFragment by : Andy
 * Wong Date : 25/1/2015
 * 
 * Change History: Date Modified By Description ---------- ----------------
 * ------------------------------- 18/1/2015 Andy Wong -First draft
 *************************************************************************/

public class PlanLTS_CPFragment extends BaseServiceFragment {
	private PlanLTS_CPFragment me;
	private AAQuery aq;

	private static final String TAG = "PlanLTS_CPFragment";
	private List<Cell> cellList;

	private int colWidth;
	private int sColWidth;

	private LinearLayout frame;

	private CellViewAdapter cellViewAdapter;
	private OnPlanMainLtsListener callback_planlts;
	private OnPlanLtsListener callback_lts;
	private PlanLtsCra planLtsCra;
	private Boolean rShowEff = true;
	private Boolean rShowNotice = false;

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		//ServiceActivity
		try {
			callback_main = (OnServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}
		//PlanLTSMainFragment
		try {
			callback_planlts = (OnPlanMainLtsListener) getParentFragment();
			if (debug) Log.i(TAG, callback_planlts.getCurrentPlanPage() + "");
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString()	+ " must implement OnPlanLtsListener");
		}
		//PlanLTSFragment
		try {
			callback_lts = (OnPlanLtsListener) getParentFragment().getParentFragment();
			if (debug) Log.i(TAG, callback_planlts.getCurrentPlanPage() + "");
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString()	+ " must implement OnPlanLtsListener");
		}
	}
	
	@Override 
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_plan_lts_cp, container, false);
		aq = new AAQuery(fragmentLayout);
		initData();
		return fragmentLayout;
	}

	protected void initData() {
		super.initData();
		colWidth = (deviceWidth - extralinespace * 2) / 4;
		sColWidth = (deviceWidth - extralinespace * 2) * 2 / 9;
		cellViewAdapter = new CellViewAdapter(getActivity());
	}

	protected void initUI(){	

		aq.id(R.id.fragment_plan_lts_cp_sv).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_plan_lts_cp_sv, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_plan_lts_cp_frame).getView();

		cellList = new ArrayList<Cell>();

		//Header
		SmallTextCell smallTextCell = new SmallTextCell(getResString(R.string.PLNLTF_TPL1), "");
		smallTextCell.setTitleTypeface(Typeface.BOLD);
		smallTextCell.setTitleColorId(R.color.black);
		smallTextCell.setLeftPadding(basePadding);
		smallTextCell.setRightPadding(basePadding);
		cellList.add(smallTextCell);
		PlanTextIconCell iddLogoCell = new PlanTextIconCell(R.drawable.logo_fixedline_0060_1, "IDD");
		iddLogoCell.setLeftPadding(basePadding);
		iddLogoCell.setRightPadding(basePadding);
		iddLogoCell.setBotPadding(0);
		cellList.add(iddLogoCell);
		planLtsCra = callback_planlts.getPlanltsCra();

		SmallTextCell disclaimerCell = new SmallTextCell("", "");
		disclaimerCell = new SmallTextCell(getResString(R.string.PLNLTF_CPL_IDD_DISCLAIMER), "");
		disclaimerCell.setLeftPadding(basePadding);
		disclaimerCell.setRightPadding(basePadding);
		disclaimerCell.setTitleColorId(R.color.hkt_textcolor);
		disclaimerCell.setTitleSizeDelta(-2);

		if (planLtsCra != null && planLtsCra.getOCallPlanAry() !=null && planLtsCra.getOCallPlanAry().length >0) {
			SrvPlan[]  srvPlans = planLtsCra.getOCallPlanAry();
			String serverTs = planLtsCra.getServerTS();
			Boolean isEnqShown = false;
			if(srvPlans.length > 0)
				cellList.add(disclaimerCell);
			for (int i=0; i< srvPlans.length; i++) {
				isEnqShown = false;

				SrvPlan srvPlan = srvPlans[i];				
				int[] widthAry = { ViewGroup.LayoutParams.WRAP_CONTENT, sColWidth, sColWidth};

				// 1 - Plan Offer label - to show the description
				String planDetail = Tool.format(getResString(R.string.PLNLTF_CPL_OFFDTL1), String.valueOf(i + 1));
				DetailBtnCell detailBtnCell = new DetailBtnCell(planDetail, getResString(R.string.myhkt_CALL_RATE), R.drawable.btn_details, null, true);


				String desc1 = isZh ? srvPlan.getLongDesn1Zh() :  srvPlan.getLongDesn1En();
				String desc2 = isZh ? srvPlan.getLongDesn2Zh() :  srvPlan.getLongDesn2En();
				String tnc = isZh ? srvPlan.getTncZh() :  srvPlan.getTncEn();

				if (desc1.isEmpty() && desc2.isEmpty() && tnc.isEmpty()) {
					desc1 = getResString(R.string.EMPTY_CALLPLAN_DETAILS_MSG);
				}

				detailBtnCell.setTitleSizeDelta(-2);

				final String desc = desc1.equals(getResString(R.string.EMPTY_CALLPLAN_DETAILS_MSG)) ? String.format("%s\n\n%s\n",planDetail, desc1) : String.format("%s\n\n%s\n\n%s\n\n%s",planDetail, desc1, desc2, tnc);

				OnClickListener detailClick = new OnClickListener(){

					@Override
					public void onClick(View v) {
						DialogHelper.createAlignedInfoDialog(getActivity(), desc, Gravity.LEFT);
					}
				};
				OnClickListener iddClick = new OnClickListener(){

					@Override
					public void onClick(View v) {
						callback_lts.setActiveSubView(R.string.CONST_SELECTEDVIEW_IDDRATES);
						callback_lts.displayChildview(1);
					}
				};
				detailBtnCell.setLeftPadding(basePadding);
				detailBtnCell.setRightPadding(basePadding);
				detailBtnCell.setOnLeftClickListener(detailClick);
				detailBtnCell.setOnRightClickListener(iddClick);
				detailBtnCell.setArrowShown(true);
				cellList.add(detailBtnCell);

				//2 - Rate(Monthly fee / Entitlement) declare part
				String[] rateCols = {getResString(R.string.PLNLTF_CPL_ENT), getResString(R.string.PLNLTF_CPL_MTHLY)};
				String[] rateNote = {getResString(R.string.PLNLTF_CPL_ENT_NOTE), getResString(R.string.PLNLTF_CPL_MTHLY_NOTE)};
				String rateTitle = getResString(R.string.PLNLTF_CPL_CHG);
				String mthlyFee = Utils.convertStringToPrice(srvPlan.getFee());
				String entitlement =String.format("%s %s", Tool.formatNum(srvPlan.getSrvEnt(), Tool.NUMFMT_2), Utils.getString(me.getActivity(), R.string.DLGM_MIN));
				String[] rateRowData = {entitlement, mthlyFee};
				int dur; 
				rShowEff = true;
				rShowNotice = false;				

				//3 - Commitment Detail declare part
				String cntrColsSt = "";
				String cntrColsDur = "";
				String cntrColsEn = "";

				String[] cntrNote = {getResString(R.string.PLNLTF_TPL_DATE_FMT), getResString(R.string.PLNLTF_CPL_CTDU_NOTE), getResString(R.string.PLNLTF_TPL_DATE_FMT)};	
				String cntrTitle = getResString(R.string.PLNLTF_CPL_DETAIL);
				String cntrRowDataSt = "";
				String cntrRowDataDur = "";
				String cntrRowDataEn = "";


				// 2 + 3 logic
				if (srvPlan.getPlnTy().equals("CM") && srvPlan.getPlnSubTy().equals("1")) {
					dur = Tool.toInt2(srvPlan.getCntrDur());
					if (dur >=1 && srvPlan.getCntrEnDt().compareTo(serverTs.substring(0, 8)) < 0) {
						rShowNotice = true;
					}

					rateTitle = getResString(R.string.PLNLTF_CPL_MFT);
					cntrColsSt = getResString(R.string.PLNLTF_CPL_CTST1);
					cntrColsDur = getResString(R.string.PLNLTF_CPL_CTDU1);
					cntrColsEn = getResString(R.string.PLNLTF_CPL_CTEN1);

					cntrRowDataSt = Tool.formatDate(srvPlan.getCntrStDt(), Tool.DATEFMT_1);
					cntrRowDataDur =  Tool.formatNum(srvPlan.getCntrDur(), Tool.NUMFMT_2);
					cntrRowDataEn = Tool.formatDate(srvPlan.getCntrEnDt(), Tool.DATEFMT_1);

					//CR2016002 check if expire within 6 mths
					Calendar c = Calendar.getInstance();
					c.add(Calendar.MONTH, 6);
					Date after6mthsDate = c.getTime();
					try {
						Date endDate;
						DateFormat df = new SimpleDateFormat(Tool.DATEFMT_1);
						endDate = df.parse(cntrRowDataEn);
						if (endDate.before(after6mthsDate)) {
							isEnqShown = true;
							cntrRowDataSt = "-";
							cntrRowDataEn = "-";
							cntrRowDataDur = "-";
							rShowNotice = false;
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					rShowEff = false;

				}

				if (rShowEff) {
					cntrColsSt = getResString(R.string.PLNLTF_CPL_EFF1);
					cntrColsDur = getResString(R.string.PLNLTF_CPL_CTDU1);
					cntrColsEn = Utils.getString(me.getActivity(), R.string.PLNLTF_CPL_CTEN1);

					cntrRowDataSt = Tool.formatDate(srvPlan.getStDt(), Tool.DATEFMT_1);
					if (cntrRowDataSt.equals("00000000") || cntrRowDataSt.trim().length() == 0) {						
						cntrRowDataSt = "-";						
					}
					cntrRowDataDur = "-";
					cntrRowDataEn = Tool.formatDate(srvPlan.getEnDt(), Tool.DATEFMT_1);
					if (srvPlan.getEnDt().equals("00000000") || srvPlan.getEnDt().trim().length() == 0) {
						if (rShowNotice) {
							//							rEnd = Utils.getString(me.getActivity(), R.string.PLNLTF_CPL_TMNOTICE);
							cntrRowDataEn = "-";
						} else {
							cntrRowDataEn = "-";
						}
					}
					//CR2016002 check if expire within 6 mths	
					Calendar c = Calendar.getInstance();
					c.add(Calendar.MONTH, 6);
					Date after6mthsDate = c.getTime();
					try {
					if ("".equals(cntrRowDataEn) || "-".equals(cntrRowDataEn)) {
						isEnqShown = true;
						cntrRowDataSt = "-";
						cntrRowDataEn = "-";
					} else {
						Date endDate;
						DateFormat df = new SimpleDateFormat(Tool.DATEFMT_1); 	
						endDate = df.parse(cntrRowDataEn);
						if (endDate.before(after6mthsDate)) {
							isEnqShown = true;
							cntrRowDataSt = "-";
							cntrRowDataEn = "-";
						}
					}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//					if (cntrRowDataEn.length() <= 10) {
					//						cntrColsEn = Utils.getString(me.getActivity(), R.string.PLNLTF_CPL_TERM);
					//					} else {
					//						cntrColsEn = Utils.getString(me.getActivity(), R.string.PLNLTF_CPL_TERM2);
					//					}					
				}
		
				//Add 2 - rate
				ArrowTextCell rateColCell = new ArrowTextCell("", rateCols, rateNote, widthAry); 
				rateColCell.setContentSizeDelta(-4);
				rateColCell.setNoteSizeDelta(-4);
				cellList.add(rateColCell);

				ArrowTextCell rateDataCell = new ArrowTextCell(rateTitle, rateRowData, null , widthAry); 
				rateDataCell.setBgcolorId(R.color.cell_bg_grey);
				rateDataCell.setContentSizeDelta(-4);
				rateDataCell.setTitleSizeDelta(-4);
				rateDataCell.setContentColorId(R.color.hkt_buttonblue);
				rateDataCell.setTitleColorId(R.color.hkt_txtcolor_grey);
				cellList.add(rateDataCell);

				//Add 3 - Commitment Detail
				String[] cntrCols = {cntrColsSt, cntrColsDur, cntrColsEn};	
				String[] cntrRowData = {cntrRowDataSt, cntrRowDataDur, cntrRowDataEn};

				ArrowTextCell cntrColCell = new ArrowTextCell("", cntrCols, cntrNote, widthAry);
				cntrColCell.setContentSizeDelta(-4);
				cntrColCell.setNoteSizeDelta(-4);
				cellList.add(cntrColCell);

				ArrowTextCell cntrDataCell = new ArrowTextCell(cntrTitle, cntrRowData, null , widthAry); 
				cntrDataCell.setBgcolorId(R.color.cell_bg_grey);
				cntrDataCell.setContentSizeDelta(-4);
				cntrDataCell.setTitleSizeDelta(-4);
				cntrDataCell.setContentColorId(R.color.hkt_buttonblue);
				cntrDataCell.setTitleColorId(R.color.hkt_txtcolor_grey);
				cellList.add(cntrDataCell);

				// notice part
				if (rShowNotice) {
					SmallTextCell noticeCell = new SmallTextCell(getResString(R.string.PLNLTF_CPL_TMNOTICE1), "");
					noticeCell.setTitleSizeDelta(-4);
					noticeCell.setLeftPadding(basePadding);
					noticeCell.setRightPadding(basePadding);
					cellList.add(noticeCell);
				}

				//Usage part
				String usageTime ="";
				if (!srvPlan.getUsgStDt().equals("00000000")) {
					usageTime = serverTs.substring(0, 8);
				}
				String usageMsg = Tool.format(Utils.getString(me.getActivity(), R.string.PLNLTF_CPL_USG), Tool.formatDate(srvPlan.getUsgStDt(), Tool.DATEFMT_4), Tool.formatDate(usageTime, Tool.DATEFMT_4), Tool.formatNum(srvPlan.getUsg(), Tool.NUMFMT_2));
				SmallTextCell usageCell = new SmallTextCell(usageMsg, "");
				usageCell.setTitleSizeDelta(-4);
				usageCell.setLeftPadding(basePadding);
				usageCell.setRightPadding(basePadding);
				if (rShowNotice) {
					usageCell.setBgColorId(R.color.cell_bg_grey);
				}		
				cellList.add(usageCell);
				
				if (isEnqShown) {
					BigTextCell bigTextCell;				
					bigTextCell = new BigTextCell(getResString(R.string.PLNLTM_CPL_REMARK2), "");
					bigTextCell.setTitleSizeDelta(-2);
					bigTextCell.setLeftMargin(basePadding);
					bigTextCell.setRightMargin(basePadding);
					bigTextCell.setTitleColorId(R.color.hkt_txtcolor_grey);
					cellList.add(bigTextCell);
				}
			}		
			//remark part			
			String remark = getResString(R.string.PLNLTF_CPL_REMARK);
			SmallTextCell remarkCell = new SmallTextCell(remark, "");
			remarkCell.setLeftPadding(basePadding);
			remarkCell.setRightPadding(basePadding);
			remarkCell.setTitleSizeDelta(-4);			
			cellList.add(remarkCell);	


			// fix for Mantis 0022136
//			if (!isEnqShown) {
//				//Enquiry
//				WebViewCell webViewCell = new WebViewCell(isZh ? "file:///android_asset/plan_lts_enquiry_zh.html" : "file:///android_asset/plan_lts_enquiry_en.html");
//				cellList.add(webViewCell);
//				SingleBtnCell singleBtnCell = new SingleBtnCell(getResString(R.string.MYHKT_BTN_LIVECHAT), deviceWidth/2- basePadding *2,
//						new OnClickListener() {
//					@Override
//					public void onClick(View v) {
//						((BaseActivity) me.getActivity()).openLiveChat();
//					}
//				});
//				singleBtnCell.setBgcolorId(R.color.cell_bg_grey);
//				singleBtnCell.setDraw(R.drawable.livechat_small);
//				singleBtnCell.setLeftMargin(basePadding);
//				singleBtnCell.setRightMargin(basePadding);
//				singleBtnCell.setLeftPadding(basePadding);
//				singleBtnCell.setRightPadding(basePadding);
//				cellList.add(singleBtnCell);
//			}
			
		} else {
			// No Data
			BigTextCell bigTextCell;
			if (callback_planlts.getReturnCode() != null) {
				 bigTextCell = new BigTextCell(callback_planlts.getReturnCode(), "");
				 if(callback_planlts.getReturnCode().equals(getResString(R.string.PLNLTM_CPL_11_17))) {
				 	bigTextCell.setTopMargin(0);
					bigTextCell.setBotMargin(0);
				 	cellList.add(disclaimerCell);
				 }
			} else {
				 bigTextCell = new BigTextCell(getResString(R.string.PLNLTM_NO_CPL), "");
			}
			bigTextCell.setTitleSizeDelta(-2);
			bigTextCell.setLeftMargin(basePadding);
			bigTextCell.setRightMargin(basePadding);
			bigTextCell.setTitleColorId(R.color.hkt_txtcolor_grey);
			cellList.add(bigTextCell);


	
		}
		cellViewAdapter.setView(frame, cellList);
	}

	// Refresh the data only when it is on current page
	public void refreshData() {
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LTSPLAN) {
			planLtsCra = callback_planlts.getPlanltsCra();
			if (planLtsCra == null) {
				if (callback_planlts.getCurrentPlanPage() == 1) {
					PlanLtsCra mPlanLtsCra = new PlanLtsCra();
					mPlanLtsCra.setILoginId(ClnEnv.getLoginId());
					mPlanLtsCra.setISubnRec(callback_main.getSubnRec());
					APIsManager.doGetPlanLts(this, mPlanLtsCra);
				}
			} else {
				initUI();
			}
		}
	}
	
	public void refresh() {
		super.refresh();
		planLtsCra = callback_planlts.getPlanltsCra();
		if (planLtsCra == null) {
			if (callback_planlts.getCurrentPlanPage() == 1) {
				PlanLtsCra mPlanLtsCra = new PlanLtsCra();
				mPlanLtsCra.setILoginId(ClnEnv.getLoginId());
				mPlanLtsCra.setISubnRec(callback_main.getSubnRec());
				APIsManager.doGetPlanLts(this, mPlanLtsCra);
			}
		} else {
			initUI();
		}
	}

	protected void cleanupUI() {

	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.PLAN_LTS.equals(response.getActionTy())) {
			planLtsCra = (PlanLtsCra) response.getCra();
			callback_planlts.setPlanLtsCra(planLtsCra);
			callback_planlts.setReturnCode(null);
			initUI();
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (response != null) {
			if (APIsManager.PLAN_LTS.equals(response.getActionTy())) {
				callback_planlts.setReturnCode(null);
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					DialogHelper.createSimpleDialog(me.getActivity(), response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else if (RC.LTSA_CPLN_11_17.equalsIgnoreCase(response.getReply().getCode())) {
					callback_planlts.setReturnCode(getResString(R.string.PLNLTM_CPL_11_17));
					initUI();
				} else {
					DialogHelper.createSimpleDialog(me.getActivity(), InterpretRCManager.interpretRC_PlanLtsMdu(me.getActivity(), response.getReply()));
				}
				//				planLtsCra = ClnEnv.getJSONObject(this.getActivity(),
				//						PlanLtsCra.class, "json/plan_lts.json");
				//				callback_planlts.setPlanLtsCra(planLtsCra);
				//				updateUI();

			}
		}
	}
}
