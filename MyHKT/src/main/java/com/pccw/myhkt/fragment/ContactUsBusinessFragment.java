package com.pccw.myhkt.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.ContactUsNewActivity;
import com.pccw.myhkt.adapter.ContactUsItemAdapter;
import com.pccw.myhkt.model.ContactUsItem;
import com.pccw.myhkt.model.HomeButtonItem;
import com.pccw.myhkt.presenters.ContactUsFragmentPresenter;
import com.pccw.myhkt.views.ContactUsFragmentView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by InYong on 2/21/19.
 */
public class ContactUsBusinessFragment extends MvpFragment<ContactUsFragmentView,
        ContactUsFragmentPresenter> implements ContactUsFragmentView {

    private static final String TAG = ContactUsBusinessFragment.class.getName();

    @BindView(R.id.rv_cu_business_relation_hotlines) RecyclerView rvCsHotline;
    @BindView(R.id.rv_cu_business_email_inquiries) RecyclerView rvEmail;
    private ContactUsItemAdapter cuAdapterCsHotline, cuAdapterEmail;

    public ContactUsBusinessFragment() {
        // Required empty public constructor
    }

    @Override
    public ContactUsFragmentPresenter createPresenter() {
        return new ContactUsFragmentPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_us_business, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initialise();
    }

    private void initialise() {
        presenter.initialise(this.getMvpView());
        cuAdapterCsHotline = new ContactUsItemAdapter(getActivity());
        cuAdapterEmail = new ContactUsItemAdapter(getActivity());
        prepareListData();
    }

    private void prepareListData() {
        prepareCsHotline();
        prepareEmail();
    }

    private void prepareCsHotline() {
        ArrayList<ContactUsItem> alCsRelationsHotline = new ArrayList<>();

        alCsRelationsHotline.add(new ContactUsItem(R.string.support_biz_hotline_title,
                R.string.support_biz_hotline_no, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.support_biz_hotline_no))));

        cuAdapterCsHotline.addItemEntries(alCsRelationsHotline);
        rvCsHotline.setAdapter(cuAdapterCsHotline);
        rvCsHotline.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareEmail() {
        ArrayList<ContactUsItem> alEmail = new ArrayList<>();

        alEmail.add(new ContactUsItem(R.string.support_biz_email,
                R.string.support_biz_email_info, R.drawable.btn_mail,
                R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.support_biz_email_info))));
        alEmail.add(new ContactUsItem(R.string.support_biz_email_pcd,
                -1, -1, -1,
                () -> Log.d(TAG, "Nothing to click")));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_SI, R.string.myhkt_cu_bizpcdemail_sales,
                R.drawable.btn_mail, R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.myhkt_cu_bizpcdemail_sales))));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_GI, R.string.myhkt_cu_bizpcdemail_cs,
                R.drawable.btn_mail, R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.myhkt_cu_bizpcdemail_cs))));
        alEmail.add(new ContactUsItem(R.string.myhkt_cu_TS, R.string.myhkt_cu_bizpcdemail_ts,
                R.drawable.btn_mail, R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.myhkt_cu_bizpcdemail_ts))));

        cuAdapterEmail.addItemEntries(alEmail);
        rvEmail.setAdapter(cuAdapterEmail);
        rvEmail.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onTriggeredLiveChat(int stringId) {
        ContactUsNewActivity activity = (ContactUsNewActivity) getActivity();
        if (!ClnEnv.isLoggedIn()) {
            activity.loginFirst(HomeButtonItem.MAINMENU.CONTACTUS);
        } else {
            if(stringId != -1)
                activity.openLiveChat(getContext().getString(stringId));
            else
                activity.openLiveChat();
        }
    }

    @Override
    public void onTriggeredCall(String phone) {
        ContactUsNewActivity activity = (ContactUsNewActivity) getActivity();
        activity.callPhone(phone);
    }

    @Override
    public void onTriggeredEmail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.fromParts(
                "mailto", email, null)); // only email apps should handle this
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            //to app directly
            startActivity(intent);
        } else {
            DialogHelper.createSimpleDialog(getActivity(), "No app can perform this action");
        }
    }

    @Override
    public void onTriggeredFacebook() {
        String fburl = "https://www.facebook.com/pccwcs";
        Intent fbintent = new Intent(Intent.ACTION_VIEW);
        fbintent.setData(Uri.parse(fburl));
        startActivity(fbintent);
    }

    @Override
    public void onTriggeredInstagram() {
        String igurl = "https://www.instagram.com/pccwcs";
        Intent igintent = new Intent(Intent.ACTION_VIEW);
        igintent.setData(Uri.parse(igurl));
        startActivity(igintent);
    }

    @Override
    public void onTriggeredWhatsApp(String phone) {

    }
}
