package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.entity.SRApptTS;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.wheat.shared.tool.Reply;

/************************************************************************
 * File : SRCreationFragment.java 
 * Desc : SR creation page
 * Name : SRCreationFragment
 * by : Derek Tsui 
 * Date : 10/03/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 10/03/2016 Derek Tsui 		-First draft
 * 27/05/2016 Andy Wong			-Eidt the text
 *************************************************************************/

public class SRCreationFragment extends BaseServiceFragment{
	private SRCreationFragment me;
	private AAQuery aq;
	private FragmentTransaction ft;
	private View myView;
	private OnLnttListener callback_lntt;
	private ApptCra										apptCra;

	private String[]	timeAry;
	private int 		timePos = 0;

	private String[]	titleAry;
	private int			titlePos = 0;
	private int 		buttonPadding = 0;
	private int 		padding_twocol = 0;
	private int 		textViewHeight = 0;
	private String 		ctMob = "";
	private String 		ctPerson = "";

	//Use in UsageFragment
	public interface OnLnttListener {
		public static final int ENTER = 1;
		public static final int BACK = 2;
		public void 	setActiveChildview(int index);
		public int 		getActiveChildview();
		public void 	displayChildview(int type);
		void onRebootModemShowLoadingPage();
		void onCallRebootApi();
	}

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity); 
		callback_lntt = (OnLnttListener)getParentFragment();
		apptCra = callback_main.getApptCra();
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_sr_creation, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}

	protected void initData() {   
		super.initData();
		aq = new AAQuery(myView);
		buttonPadding =  (int) getResources().getDimension(R.dimen.reg_logo_padding);
		padding_twocol = (int) getResources().getDimension(R.dimen.padding_twocol);
		textViewHeight  = (int) getResources().getDimension(R.dimen.textviewheight);
		titleAry = new String[] { getResString(R.string.MYHKT_SR_LBL_MR), getResString(R.string.MYHKT_SR_LBL_MRS), getResString(R.string.MYHKT_SR_LBL_MISS),getResString(R.string.MYHKT_SR_LBL_MS) };
	}


	@Override
	public void onResume(){
		super.onResume();
		// Screen Tracker
		if (SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
			callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_SR));
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_LTSSRCREATION,
					false);
		} else if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
			callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_SR));
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_PCDSRCREATION,
					false);
		} else if (SubnRec.LOB_TV.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
			callback_livechat.setModuleId(getResString(R.string.MODULE_TV_SR));
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_TVSRCREATION,
					false);
		}
	}

	@Override
	public void setUserVisibleHint(boolean isFragmentVisible) {
		super.setUserVisibleHint(isFragmentVisible);
	}

	protected void initUI(){
		aq.marginpx(R.id.srcreation_layout2, basePadding, 0, basePadding, 0);
		aq.marginpx(R.id.srcreation_etxtlayout2, 0,0,0,basePadding);
		aq.marginpx(R.id.srcreation_btn_layout, basePadding, 0, basePadding, 0);
		aq.line(0, "", R.id.srcreation_line1,R.id.srcreation_line2, R.id.srcreation_line3, R.id.srcreation_line4, R.id.srcreation_line5);

		//Header
		aq.id(R.id.srcreation_header).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_apptx3, 0,0,0);
		aq.normText(R.id.srcreation_header, getString(R.string.MYHKT_SR_HDR_CREATE));
		aq.id(R.id.srcreation_header).getTextView().setCompoundDrawablePadding(padding_twocol);
		aq.marginpx(R.id.srcreation_header, 0, 0, 0, 0);

		//Servce num
		aq.id(R.id.srcreation_loginid_txt).textColorId(R.color.hkt_textcolor);

		//Time slot header
		aq.normTextGrey(R.id.srcreation_time_desc, getResString(R.string.MYHKT_SR_TITLE_TIMESLOT), -2);
		aq.id(R.id.srcreation_time_desc).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.rightarrow_small, 0, 0, 0);
		aq.id(R.id.srcreation_time_desc).getTextView().setCompoundDrawablePadding(padding_twocol);
		aq.marginpx(R.id.srcreation_time_desc, 0, basePadding, 0, 0);

		//Time slot
//		aq.normText(R.id.srcreation_txt_time, getResString(R.string.MYHKT_SR_LBL_TIMESLOT), 0);
//		aq.gravity(R.id.srcreation_txt_time, Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		aq.spinText(R.id.srcreation_spinner_time , "");	
		aq.id(R.id.srcreation_spinner_time).clicked(this, "onClick").background(0);

		//Contact header
		aq.normTextGrey(R.id.srcreation_contact_desc, getResString(R.string.MYHKT_SR_TITLE_CONTACT), -2);
		aq.id(R.id.srcreation_contact_desc).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.rightarrow_small, 0, 0, 0);
		aq.id(R.id.srcreation_contact_desc).getTextView().setCompoundDrawablePadding(padding_twocol);

		//Title	
		aq.normText(R.id.srcreation_txt_title, getResString(R.string.MYHKT_SR_LBL_TITLE), 0);
		aq.gravity(R.id.srcreation_txt_title, Gravity.CENTER_VERTICAL | Gravity.RIGHT);	
		aq.spinText(R.id.srcreation_spinner_title , titleAry[titlePos]);	
		aq.id(R.id.srcreation_spinner_title).clicked(this, "onClick").background(0);

		//Contact person
		aq.normTextGrey(R.id.srcreation_txt_contperson, getResString(R.string.MYHKT_SR_LBL_NAME), 0); 
		aq.gravity(R.id.srcreation_txt_contperson, Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		aq.normEditText(R.id.srcreation_etxt_contperson, ctPerson, "");
		aq.id(R.id.srcreation_etxt_contperson).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.srcreation_etxt_contperson).background(0).height(textViewHeight, false).getEditText().setSingleLine();
		aq.padding(R.id.srcreation_etxt_contperson, basePadding, 0, basePadding, 0);

		//Contact Number
		aq.normTextGrey(R.id.srcreation_txt_contnum, getResString(R.string.MYHKT_SR_LBL_TEL), 0); 
		aq.gravity(R.id.srcreation_txt_contnum, Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		aq.normEditText(R.id.srcreation_etxt_contnum, ctMob, "");
		aq.id(R.id.srcreation_etxt_contnum).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.srcreation_etxt_contnum).background(0).height(textViewHeight, false).getEditText().setSingleLine();
		aq.id(R.id.srcreation_etxt_contnum).getEditText().setInputType(InputType.TYPE_CLASS_PHONE);
		aq.padding(R.id.srcreation_etxt_contnum, basePadding, 0, basePadding, 0);

		//is English Speaker
		aq.normText(R.id.srcreation_txt_enspk, getResString(R.string.MYHKT_SR_LBL_IS_ENGLISH), -2);
		aq.id(R.id.srcreation_txt_enspk).textColorId(R.color.hkt_buttonblue);

		//Bot Buttom
		aq.marginpx(R.id.srcreation_btn_layout, basePadding, basePadding, basePadding, basePadding);
		aq.norm2TxtBtns(R.id.srcreation_btn_submit, R.id.srcreation_btn_back, getResString(R.string.btn_confirm) ,getResString(R.string.MYHKT_BTN_CANCEL));
		
		aq.id(R.id.srcreation_btn_submit).clicked(me, "onClick");
		aq.id(R.id.srcreation_btn_back).clicked(me, "onClick");
	}

	protected final void refreshData() {
		super.refreshData();

		//LnttFragment is current on Service Activty and SRCreate is current on LnttFragment 
		if (callback_lntt.getActiveChildview() == R.string.CONST_SELECTEDVIEW_SRCREATION && (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LINETEST)) {
			// Initialize UI visibility
			if (SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				// LTS - Label = Telephone No:
				aq.id(R.id.srcreation_loginid_header).text(getResString(R.string.MYHKT_SR_LBL_LTS_ACC));
				aq.id(R.id.srcreation_loginid_txt).text(callback_main.getSubnRec().srvNum);
			} else if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				// PCD - Label = Netvigator Login ID:
				aq.id(R.id.srcreation_loginid_header).text(getResString(R.string.MYHKT_SR_LBL_PCD_ACC));
				aq.id(R.id.srcreation_loginid_txt).text(callback_main.getSubnRec().srvNum);
			} else if (SubnRec.LOB_TV.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				// TV - Label = Account Number:
				aq.id(R.id.srcreation_loginid_header).text(getResString(R.string.MYHKT_SR_LBL_TV_ACC));
				aq.id(R.id.srcreation_loginid_txt).text(callback_main.getSubnRec().srvNum);
			}

			// Prepared String Array for showing TimeSlot
			SRApptTS[] apptTimeSlotAry = apptCra.getOSRApptInfo().getApptTSAry();
			timeAry = new String[apptTimeSlotAry.length];

			for (int i = 0; i < apptTimeSlotAry.length; i++) {
				SRApptTS rSRApptTS = apptTimeSlotAry[i];
				timeAry[i] = Utils.getTimeSlotDateTime1(me.getActivity(), rSRApptTS.getApptDate(), rSRApptTS.getApptTmslot());
			}

			// Show timeslot
			if (timeAry.length > 0) {
				aq.spinText(R.id.srcreation_spinner_time , timeAry[timePos]);	
				//				ArrayAdapter<String> timeSlotArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, timeslotAry);
				//				timeSlotArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				//				viewholder.srcreation_spinner_timeslot.setAdapter(timeSlotArrayAdapter);
			}
		}
	}

	public void onClick(View v) {
		// hide the soft keyboard
		Utils.closeSoftKeyboard(getActivity(), v);

		switch (v.getId()) {
		//		// Line test again
		case R.id.srcreation_btn_submit:
			// TODO: The Error message should be shown in different conditions.
			// allow update if any timeslot exists
			if (apptCra != null) {
				if (apptCra.getOSRApptInfo().getApptTSAry().length > 0) {
					SrvReq rSrvReq = prepareSrvReq(apptCra.getOPendSrvReq().copyMe());

					Reply reply = new Reply();
					reply = rSrvReq.verify4CreateSR();
					if (!reply.isSucc()) {
						displayDialog(Utils.interpretRC_SRMdu(me.getActivity(), reply.getCode()));
					} else {
						// doCreateSR

						ApptCra rApptCra = new ApptCra();
						rApptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
						rApptCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
						rApptCra.setISubnRec(callback_main.getSubnRec().copyMe());
						rApptCra.setISrvReq(rSrvReq.copyMe());
						
						APIsManager.doCreateSR(me, rApptCra);
					}
				}
			}
			break;

		case R.id.srcreation_btn_back:
			callback_lntt.setActiveChildview(R.string.CONST_SELECTEDVIEW_LINETEST3);
			callback_lntt.displayChildview(OnLnttListener.BACK);
			break;
		case R.id.srcreation_spinner_time:
			final QuickAction pickerWheelAction = new QuickAction(getActivity(), 0, 0);

			pickerWheelAction.initPickerWheelSingle(timeAry, timePos);
			pickerWheelAction.setOnPickerScrollSingleListener(new QuickAction.OnPickerScrollSingleListener() {
				@Override
				public void onPickerScroll(String leftResult, int pos) {
					timePos = pos;
					aq.id(R.id.srcreation_spinner_time).text(timeAry[timePos]);
				}
			});
			pickerWheelAction.showPicker(v, aq.id(R.id.srcreation_layout).getView());
			break;
		case R.id.srcreation_spinner_title:
			final QuickAction titlePicker = new QuickAction(getActivity(), 0, 0);

			titlePicker.initPickerWheelSingle(titleAry, titlePos);
			titlePicker.setOnPickerScrollSingleListener(new QuickAction.OnPickerScrollSingleListener() {
				@Override
				public void onPickerScroll(String leftResult, int pos) {
					titlePos = pos;
					aq.id(R.id.srcreation_spinner_title).text(titleAry[titlePos]);
				}
			});
			titlePicker.showPicker(v, aq.id(R.id.srcreation_layout).getView());
			break;
		}
	}

	private final SrvReq prepareSrvReq(SrvReq srvreq) {

		boolean isEnSpkr = aq.id(R.id.srcreation_checkbox_enspk).getCheckBox().isChecked();
		int selectedTimeSlot = timePos;
		SRApptTS rSRApptTS = apptCra.getOSRApptInfo().getApptTSAry()[selectedTimeSlot];
		//Retrun only Eng version to server
		String[] titleAry = new String[] { getResString(R.string.MYHKT_SR_VAL_MR), getResString(R.string.MYHKT_SR_VAL_MRS), getResString(R.string.MYHKT_SR_VAL_MISS),getResString(R.string.MYHKT_SR_VAL_MS) };
		String title = titleAry[titlePos];
		SrvReq rSrvReq = srvreq.copyMe();
		rSrvReq.setCtNm(aq.id(R.id.srcreation_etxt_contperson).getEditText().getText().toString().trim());
		rSrvReq.setCtNmTtl(title);
		rSrvReq.setCtNum(aq.id(R.id.srcreation_etxt_contnum).getEditText().getText().toString().trim());
		rSrvReq.setEnSpkr(isEnSpkr ? "Y" : "N");
		rSrvReq.setRefNum(callback_main.getLnttCra().getOLtrsRec().usrRefno);
		rSrvReq.setSmsLang(isZh ? "zh" : "en");
		rSrvReq.setApptTS(rSRApptTS.copyMe());
		return rSrvReq;
	}
	
	public void onPause() {
		 ctMob = (String) aq.id(R.id.srcreation_etxt_contnum).getText().toString();
		 ctPerson = (String) aq.id(R.id.srcreation_etxt_contperson).getText().toString();
		super.onPause();
		
	}

	@Override
	public void onSuccess(APIsResponse response) {
		super.onSuccess(response);
		if (response != null) {
			if (response.getActionTy().equals(APIsManager.SR_CRT)) {
				ApptCra apptcra = (ApptCra) response.getCra();
				me.apptCra.setISrvReq(apptcra.getISrvReq().copyMe());
				// Pass Data Back Activity
				me.callback_main.setApptCra(me.apptCra.copyMe());

				//goto confirm page 
				callback_lntt.setActiveChildview(R.string.CONST_SELECTEDVIEW_SRCREATION_CONFIRM);
				callback_lntt.displayChildview(OnLnttListener.ENTER);		
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		super.onFail(response);
		if (response != null) {
			if (response.getActionTy().equals(APIsManager.SR_CRT)) {
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					displayDialog(response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else {
					ApptCra apptcra = (ApptCra) response.getCra();
//					if (apptcra.getReply().isEqual(Reply.RC_IVSESS) || apptcra.getReply().isEqual(Reply.RC_ALT)) {
//						// RC_IVSESS
//						// RC_ALT
//						me.redirectDialog(ClnEnv.getRPCErrMsg(me.getActivity(), Reply.RC_IVSESS));
//					} else {
						me.displayDialog(InterpretRCManager.interpretRC_SRMdu(me.getActivity().getApplicationContext(), apptcra.getReply().getCode()));
//					}
				}
			}
		}

	}

}
