package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.ImageViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;

import java.util.ArrayList;
import java.util.List;

public class LnttRebootModemLoadingFragment extends BaseServiceFragment {

    private String TAG = LnttRebootModemLoadingFragment.class.getSimpleName();
    private AAQuery aq;

    private List<Cell> cellList;
    private CellViewAdapter cellViewAdapter;

    private LinearLayout frame;


    public LnttRebootModemLoadingFragment() {
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentLayout = inflater.inflate(R.layout.fragment_linetest_st, container, false);

        aq = new AAQuery(fragmentLayout);
        initData();
        return fragmentLayout;
    }

    protected void initData() {
        super.initData();
        cellViewAdapter = new CellViewAdapter(getActivity());

    }

    @Override
    protected void initUI() {

        cellList = new ArrayList<Cell>();
        //ScrollView
        aq.id(R.id.linetest_st_sv).backgroundColorId(R.color.white);
        frame = (LinearLayout) aq.id(R.id.linetest_st_frame).getView();

        setLTSUI();

        //Bot bar
        aq.id(R.id.linetest_st_botar).backgroundColorId(R.color.white);
        aq.padding(R.id.linetest_st_botar, basePadding, 0, basePadding, 0);
        aq.gravity(R.id.linetest_st_botar, Gravity.CENTER);
        aq.id(R.id.linetest_st_btn).getButton().setVisibility(View.GONE);
        aq.id(R.id.linetest_st_btn).clicked(this, "onClick");

        if (callback_main != null) {
            // Screen Tracker
            if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_PCD_REBOOT_LOADING, false);
            } else if (SubnRec.LOB_TV.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_TV_REBOOT_LOADING, false);
            } else if (SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_LTS_EYE_REBOOT_LOADING, false);
            }

            Log.d(TAG, "LOB: " + callback_main.getSubnRec().lob);
        }
    }

    public void setLTSUI() {
        IconTextCell titleCell = new IconTextCell(R.drawable.test_result_icon , getResString(R.string.myhkt_reboot));
        titleCell.setLeftPadding(basePadding);
        titleCell.setRightPadding(basePadding);
        titleCell.setTopMargin(0);
        cellList.add(titleCell);

        BigTextCell introCell = new BigTextCell("", getResString(R.string.reboot_loading_description));
        introCell.setLeftMargin(basePadding);
        introCell.setRightMargin(basePadding);
        introCell.setContentSizeDelta(0);
        cellList.add(introCell);

        //default image
        ImageViewCell imageViewCell = new ImageViewCell(getResources().getDrawable(R.drawable.lntt_reboot_loading));
        imageViewCell.setLeftMargin((int) getResources().getDimension(R.dimen.lntt_image_padding));
        imageViewCell.setRightMargin((int) getResources().getDimension(R.dimen.lntt_image_padding));
        cellList.add(imageViewCell);

        cellViewAdapter.setView(frame, cellList);
    }
}
