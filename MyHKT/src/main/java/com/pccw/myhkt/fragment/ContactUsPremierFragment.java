package com.pccw.myhkt.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.ContactUsNewActivity;
import com.pccw.myhkt.adapter.ContactUsItemAdapter;
import com.pccw.myhkt.model.ContactUsItem;
import com.pccw.myhkt.presenters.ContactUsFragmentPresenter;
import com.pccw.myhkt.views.ContactUsFragmentView;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by InYong on 2/21/19.
 */
public class ContactUsPremierFragment extends MvpFragment<ContactUsFragmentView,
        ContactUsFragmentPresenter> implements ContactUsFragmentView {

    @BindView(R.id.rv_cu_premier_live_chat) RecyclerView rvLiveChat;
    @BindView(R.id.rv_cu_premier_relation_hotlines) RecyclerView rvCsRelationsHotline;
    @BindView(R.id.rv_cu_premier_sales_hotline) RecyclerView rvSales;
    @BindView(R.id.rv_cu_premium_email_inquiries) RecyclerView rvEmail;

    private ContactUsItemAdapter cuAdapterLiveChat, cuAdapterCsRelationsHotline, cuAdapterEmail,
            cuAdapterSales;


    public ContactUsPremierFragment() {
        // Required empty public constructor
    }

    @Override
    public ContactUsFragmentPresenter createPresenter() {
        return new ContactUsFragmentPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_us_premier, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initialise();
    }

    private void initialise() {
        presenter.initialise(this.getMvpView());
        cuAdapterLiveChat = new ContactUsItemAdapter(getActivity());
        cuAdapterCsRelationsHotline = new ContactUsItemAdapter(getActivity());
        cuAdapterSales = new ContactUsItemAdapter(getActivity());
        cuAdapterEmail = new ContactUsItemAdapter(getActivity());
        prepareListData();
    }

    private void prepareListData() {
        prepareLiveChat();
        prepareCsRelationsHotline();
        prepareSales();
        prepareEmail();
    }

    private void prepareLiveChat() {
        ArrayList<ContactUsItem> alLiveChat = new ArrayList<>();

        alLiveChat.add(new ContactUsItem(R.string.support_liveChat_description_prermier,
                "", R.drawable.livechat_small,
                R.string.MYHKT_BTN_LIVECHAT,
                () -> presenter.triggerLiveChat(-1)));

        cuAdapterLiveChat.addItemEntries(alLiveChat);
        rvLiveChat.setAdapter(cuAdapterLiveChat);
        rvLiveChat.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareCsRelationsHotline() {
        ArrayList<ContactUsItem> alCsRelationsHotline = new ArrayList<>();

        alCsRelationsHotline.add(new ContactUsItem(R.string.SUPPORT_PREMIER_HOTLINE_TITLE,
                R.string.SUPPORT_PREMIER_HOTLINE_NO, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.SUPPORT_PREMIER_HOTLINE_NO))));

        cuAdapterCsRelationsHotline.addItemEntries(alCsRelationsHotline);
        rvCsRelationsHotline.setAdapter(cuAdapterCsRelationsHotline);
        rvCsRelationsHotline.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareSales() {
        ArrayList<ContactUsItem> alSales = new ArrayList<>();

        alSales.add(new ContactUsItem(R.string.sales_premier_hotline_title,
                R.string.sales_premier_hotline_no, R.drawable.phone_small, R.string.btn_call_now,
                () -> presenter.triggerCall(getString(R.string.sales_premier_hotline_no))));

        cuAdapterSales.addItemEntries(alSales);
        rvSales.setAdapter(cuAdapterSales);
        rvSales.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void prepareEmail() {
        ArrayList<ContactUsItem> alEmail = new ArrayList<>();

        alEmail.add(new ContactUsItem(R.string.SUPPORT_PREMIER_HOTLINE_TITLE,
                R.string.SUPPORT_PREMIER_EMAIL_CS, R.drawable.btn_mail,
                R.string.btn_send,
                () -> presenter.triggerEmail(getString(R.string.SUPPORT_PREMIER_EMAIL_CS))));

        cuAdapterEmail.addItemEntries(alEmail);
        rvEmail.setAdapter(cuAdapterEmail);
        rvEmail.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    public void onTriggeredLiveChat(int stringId) {
        ContactUsNewActivity activity = (ContactUsNewActivity) getActivity();
        if (!ClnEnv.isLoggedIn()) {
                activity.loginFirst(MAINMENU.CONTACTUS);
        } else {
            if(stringId != -1)
                activity.openLiveChat(getContext().getString(stringId));
            else
                activity.openLiveChat();
        }
    }

    @Override
    public void onTriggeredCall(String phone) {
        ContactUsNewActivity activity = (ContactUsNewActivity) getActivity();
        activity.callPhone(phone);
    }

    @Override
    public void onTriggeredEmail(String email) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.fromParts(
                "mailto", email, null)); // only email apps should handle this
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            //to app directly
            startActivity(intent);
        } else {
            DialogHelper.createSimpleDialog(getActivity(), "No app can perform this action");
        }
    }

    @Override
    public void onTriggeredFacebook() {
        String fburl = "https://www.facebook.com/pccwcs";
        Intent fbintent = new Intent(Intent.ACTION_VIEW);
        fbintent.setData(Uri.parse(fburl));
        startActivity(fbintent);
    }

    @Override
    public void onTriggeredInstagram() {
        String igurl = "https://www.instagram.com/pccwcs";
        Intent igintent = new Intent(Intent.ACTION_VIEW);
        igintent.setData(Uri.parse(igurl));
        startActivity(igintent);
    }

    @Override
    public void onTriggeredWhatsApp(String phone) {

    }
}
