package com.pccw.myhkt.fragment;

import static com.pccw.myhkt.util.Constant.KEY_NAME_LOB_TYPE;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;

import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;

public class BillFragment extends BaseServiceFragment {
    private BillFragment me;
    private FragmentTransaction ft;
    private BillSumFragment billSumFragment;
    private BillInfoFragment billInfoFragment;
    private BillInfoLTSFragment billInfoLTSFragment;
    private final String TAG = "BillFragment";
    public Boolean isBillSum = true;
    public int billInfoLTSSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_bill, container, false);

        if (isBillSum) {
            openBillSumFrag(false);
        } else {
            openBillInfoFrag(false);
        }
        return fragmentLayout;
    }

    public void openBillInfoFrag() {
        openBillInfoFrag(true);
    }

    public void openBillInfoFrag(Boolean isModIdSet) {
        isBillSum = false;
        ft = getChildFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
        Utils.closeSoftKeyboard(getActivity());
        if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
            billInfoLTSFragment = new BillInfoLTSFragment();

            if (debug) Log.i(TAG, "is child2 " + (billInfoLTSSubView == R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2));

            ft.replace(R.id.bill_main_frame, billInfoLTSFragment).commit();
        } else {
            Bundle bundle = new Bundle();
            bundle.putInt(KEY_NAME_LOB_TYPE, callback_main.getLob());

            billInfoFragment = new BillInfoFragment();
            billInfoFragment.setArguments(bundle);

            ft.replace(R.id.bill_main_frame, billInfoFragment).commit();
        }
        setModuleId(isModIdSet);
    }

    public void openBillSumFrag() {
        openBillSumFrag(true);
    }

    public void openBillSumFrag(Boolean isModIdSet) {
        isBillSum = true;
        billSumFragment = new BillSumFragment();
        billSumFragment.setArguments(this.getArguments());
        ft = getChildFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
        Utils.closeSoftKeyboard(getActivity());
        ft.replace(R.id.bill_main_frame, billSumFragment).commit();
        setModuleId(isModIdSet);
    }

    @Override
    public final void refreshData() {
        super.refreshData();
        if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
            setModuleId();
        } else {
            //Replace the current bill page to BillSum when bill fragment is not shown
            isBillSum = true;
            billSumFragment = new BillSumFragment();
            billSumFragment.setArguments(getArguments());
            ft = getChildFragmentManager().beginTransaction();
            Utils.closeSoftKeyboard(getActivity());
            ft.replace(R.id.bill_main_frame, billSumFragment).commit();
        }
    }

    @Override
    public final void refresh() {
        super.refresh();
        if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
            setModuleId();
            if (isBillSum) {
                billSumFragment.refresh();
            } else {
                if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
                    billInfoLTSSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1;
                    billInfoLTSFragment.refresh();
                } else {
                    billInfoFragment.refresh();
                }
            }
        }
    }

    protected void setModuleId() {
        //Bill Sum and Bill Info have same module id except LTS
        String lob = callback_main.getSubnRec().lob;
        if (ClnEnv.isMyMobFlag()) {
            if (SubnRec.LOB_101.equals(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_101_MM_BILL));
            } else if (SubnRec.LOB_O2F.equals(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_O2F_MM_BILL));
            } else if (SubnRec.LOB_MOB.equals(lob) || SubnRec.WLOB_XMOB.equals(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_MOB_MM_BILL));
            } else if (SubnRec.LOB_IOI.equals(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_IOI_MM_BILL));
            }
        } else {
            if (SubnRec.LOB_MOB.equalsIgnoreCase(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_MOB_BILL));
            } else if (SubnRec.LOB_O2F.equalsIgnoreCase(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_O2F_BILL));
            } else if (SubnRec.LOB_101.equalsIgnoreCase(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_101_BILL));
            } else if (SubnRec.LOB_IOI.equalsIgnoreCase(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_IOI_BILL));
            } else if (SubnRec.LOB_PCD.equalsIgnoreCase(lob)) {
                callback_livechat.setModuleId(getResString(callback_main.getSubnRec().isBillByAgent() ? R.string.MODULE_PCD_BILL_4MOBCS : R.string.MODULE_PCD_BILL));
            } else if (SubnRec.LOB_TV.equalsIgnoreCase(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_TV_BILL));
            } else if (SubnRec.LOB_LTS.equalsIgnoreCase(lob)) {
                callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_BILL));
            }
        }
        super.setModuleId();
    }

    protected void setModuleId(Boolean isModSet) {
        if (isModSet) {
            setModuleId();
        }
    }
}
