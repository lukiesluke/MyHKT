
package com.pccw.myhkt.fragment;

import static com.pccw.myhkt.util.Constant.FORMAT_YYYYMMDD;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3BoostUpOffer1DTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.g3entity.G3RechargeHistDTO;
import com.pccw.dango.shared.g3entity.G3UsageQuotaDTO;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.SpinnerTextCell;
import com.pccw.myhkt.cell.model.TextBtnCell;
import com.pccw.myhkt.cell.model.TwoBtnCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.enums.HKTDataType;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageDataListener;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/************************************************************************
 * File : UsageTopup1Fragment.java
 * Desc : Top-up history page
 * 		  C-Sim will get Top up History and offer list in this page by API call
 * 		  H-Sim will get Top up History and offer list form previous page instead of API call
 * Name : UsageTopup1Fragment
 * by 	: Andy Wong
 * Date : 26/1/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 26/1/2016  Andy Wong 		-First draft
 * 26/1/2016  Derek Tsui		-first draft
 * 18/6/2018  Moiz Esmail		-Removed code comments
 *************************************************************************/

public class UsageTopup1Fragment extends BaseServiceFragment{
	private static int offerListcellPos;
	private static int topupHistcellPos;
	private static int topupHistRemkcellPos;
	private static int twoBtncellPos;
	public String TAG = "lwg";
	String[] spinnerClick = {"onSpinnerClick"};
	private UsageTopup1Fragment me;
	private View 				myView;
	private AAQuery 			aq;
	private List<Cell> 			cellList;
	private OnUsageListener 	callback_usage;
	private OnUsageDataListener callback_usage_data;
	private OnServiceListener callback_service;
	private PlanMobCra planMobcra;
	private AddOnCra addOnCra;
	private G3DisplayServiceItemDTO g3DisplayServiceItemDTO;
	private QuickAction mQuickActionHist;
	private QuickAction mQuickActionOffer;
	private ActionItem[] actionHistItem;
	private ActionItem[] actionOfferItem;
	private G3BoostUpOffer1DTO[] g3BoostUpOfferDTO;
	private int 		 offerPos = 0;
	private LinearLayout frame;
	private CellViewAdapter cellViewAdapter;
	private Drawable callIcon;
	private Boolean isMob;
	private Cell lineCell;
	private String lobString = "";

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		try {
			callback_usage = (OnUsageListener) this.getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(this.getParentFragment().toString() + " must implement OnUsageListener");
		}
		try {
			callback_service = (OnServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_usagetopup, container, false);
		myView = fragmentLayout;
		lobString = callback_main.getAcctAgent().getSubnRec().lob;
		initData();
		return fragmentLayout;
	}

	@Override
	public void onResume() {
		super.onResume();
		//Screen Tracker
		if (SubnRec.WLOB_XCSP.equals(lobString) || SubnRec.LOB_CSP.equals(lobString)) {
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_CSPTOPUP2, false);
		} else {
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBTOPUP2, false);
		}
	}

	protected void initData() {
		super.initData();
		Log.d(TAG, "UsageTopup1Fragment::initData");
		cellViewAdapter = new CellViewAdapter(me);

		ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#187DC3"), PorterDuff.Mode.SRC_IN);
		int bh = (int) getResources().getDimension(R.dimen.buttonblue_height);
		int padding_twocol = (int) getResources().getDimension(R.dimen.padding_twocol);
		callIcon = getResources().getDrawable(R.drawable.phone_small);
		int ch = callIcon.getIntrinsicHeight();
		int cw = callIcon.getIntrinsicWidth();
		callIcon.setColorFilter(filter);
		int imageCH = bh - padding_twocol * 3;
		int imageCW = cw * (imageCH) / ch;
		callIcon.setBounds(0, 0, imageCW, imageCH);
		isMob = (SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.LOB_CSP.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.WLOB_XCSP.equals(callback_main.getAcctAgent().getSubnRec().lob));
		lineCell = new Cell(Cell.LINE);
	}

	protected void initUI() {
		aq = new AAQuery(myView);

		aq.id(R.id.fragment_usagetopup_sv).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagetopup_sv, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagetopup_frame).getView();

		g3DisplayServiceItemDTO = callback_usage.getSelectedServiceItem();
		cellList = new ArrayList<Cell>();
		int mmsImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
		IconTextCell iconTextCell = new IconTextCell(mmsImg, getResString(R.string.usage_boost_up));
		cellList.add(iconTextCell);

		cellViewAdapter.setView(lobString, frame, cellList);

		if(callback_usage != null) {
			planMobcra = callback_usage.getPlanMobCra();
		}

		if (isMob) {
			setMobUI();
			getRechargeHistory();
		} else {
			set1010UI();
		}
	}
	/**
	 * 
	 */
	public void setMobUI(){
		cellList = new ArrayList<Cell>();		

		int mmsImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
		IconTextCell iconTextCell = new IconTextCell(mmsImg, getResString(R.string.usage_boost_up)); 
		cellList.add(iconTextCell);

		//Current local mobile data
		String title = isZh ? g3DisplayServiceItemDTO.getTitleLevel1Chi() : g3DisplayServiceItemDTO.getTitleLevel1Eng();
		BigTextCell mobileDataCell = new BigTextCell(title + ":",isZh? g3DisplayServiceItemDTO.getUsageDescription().getTextChi() : g3DisplayServiceItemDTO.getUsageDescription().getTextEng());
		mobileDataCell.setArrowShown(true);
		mobileDataCell.setTitleSizeDelta(0);
		mobileDataCell.setContentSizeDelta(0);
		mobileDataCell.setTopMargin(0);
		mobileDataCell.setMargin(basePadding);
		mobileDataCell.setTitleColorId(R.color.hkt_txtcolor_grey);
		mobileDataCell.setContentColorId(R.color.topup_orange);
		cellList.add(mobileDataCell);

		//Mothly Package
		String entitlement;
		boolean isGlobal = "GLOBAL".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getRegion());

		entitlement = Utils.formattedDataByMBStr(String.valueOf(g3DisplayServiceItemDTO.getQuotaTopupInfo().getInitialQuotaInKB()/1024), HKTDataType.HKTDataTypeEntitled, true);

		String mthlyTitle;
		if ((g3DisplayServiceItemDTO.getRegion().equalsIgnoreCase("LOCAL") || isGlobal) && "B".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getQuotaTopupInfo().getQuotaValidityPeriod())){
			mthlyTitle = getResString(R.string.usage_boost_up_monthly_package);
		} else {
			mthlyTitle = getResString(R.string.usage_basic_entitle) + "*";
		}
		SmallTextCell monthlyPckCell = new SmallTextCell(mthlyTitle, entitlement);
		monthlyPckCell.setLeftPadding(basePadding*2);
		monthlyPckCell.setRightPadding(basePadding);
		cellList.add(monthlyPckCell);

		//Package Remark1
		//Hide if Asia plan
		if(!isGlobal) {
			SmallTextCell packageRmkCell = new SmallTextCell(getResString(R.string.myhkt_usage_remark1), null);
			packageRmkCell.setTitleSizeDelta(-2);
			packageRmkCell.setLeftPadding(basePadding * 2);
			packageRmkCell.setRightPadding(basePadding);
			packageRmkCell.setBotPadding(basePadding * 4);
			cellList.add(packageRmkCell);
		}


		SmallTextCell histRec = new SmallTextCell(getResString(R.string.usage_boost_up_history), getResString(R.string.usage_boost_up_hist_no_rec));
		histRec.setLeftPadding(basePadding*2);
		histRec.setRightPadding(basePadding);
		cellList.add(histRec);
		topupHistcellPos = cellList.indexOf(histRec);

		//Remark1
		SmallTextCell topupHisRmk1Cell = new SmallTextCell("*" + String.format(getResString(R.string.usage_boost_up_remark), getString(R.string.usage_boost_up_next_bill), "", ""), null);
		topupHisRmk1Cell.setTitleSizeDelta(-2);
		topupHisRmk1Cell.setLeftPadding(basePadding * 2);
		topupHisRmk1Cell.setRightPadding(basePadding);
		cellList.add(topupHisRmk1Cell);
		topupHistRemkcellPos = cellList.indexOf(topupHisRmk1Cell);

		//Hong kong time for roaming only
		if ("ROAMING".equalsIgnoreCase(g3DisplayServiceItemDTO.getRegion()) || isGlobal) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
				Date d = sdf.parse(planMobcra.getServerTS());
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
				String date = df.format(d);

				SmallTextCell smallTextCell3a = new SmallTextCell(String.format(Utils.getString(me.getActivity(), R.string.usage_boost_up_curr_hk_time), date), null);
				smallTextCell3a.setTitleSizeDelta(-2);
				smallTextCell3a.setLeftPadding(basePadding*2);
				smallTextCell3a.setRightPadding(basePadding);
				cellList.add(smallTextCell3a);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//Remark2
		if(!isGlobal) {
			SmallTextCell topupHisRmk2Cell = new SmallTextCell(getResString(R.string.myhkt_usage_remark2), null);
			topupHisRmk2Cell.setTitleSizeDelta(-2);
			topupHisRmk2Cell.setLeftPadding(basePadding * 2);
			topupHisRmk2Cell.setRightPadding(basePadding);
			topupHisRmk2Cell.setBotPadding(basePadding * 4);
			cellList.add(topupHisRmk2Cell);
		}

		//Topup offer list
		//Package select header
		SmallTextCell packeSelHeader = new SmallTextCell(getResString(R.string.boost_up_select_note), null);
		//		packeSelHeader.setTitleSizeDelta(-2);
		packeSelHeader.setLeftPadding(basePadding);
		packeSelHeader.setRightPadding(basePadding);
		packeSelHeader.setTitleColorId(R.color.black);
		cellList.add(packeSelHeader);		

		cellList.add(lineCell);		

		//Offer list selector
			SpinnerTextCell spinnerTextCell = new SpinnerTextCell("", spinnerClick);
			spinnerTextCell.setLeftPadding(basePadding);
			spinnerTextCell.setRightPadding(basePadding);
			cellList.add(spinnerTextCell);
			offerListcellPos = cellList.indexOf(spinnerTextCell);
			cellList.add(lineCell);

		//Back , Continue
		OnClickListener onBackClick = new OnClickListener(){
			@Override
			public void onClick(View v) {
				callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_USAGEDATA);
				callback_usage.displayChildview(true);		
			}			
		};
		OnClickListener onContinueClick = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (g3BoostUpOfferDTO != null && g3BoostUpOfferDTO.length > 0) {
					callback_usage.getSelectedG3BoostUpOfferDTO(g3BoostUpOfferDTO[offerPos]);
					callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP2);
					callback_usage.displayChildview(false);
					//					callback_usage.goToUsageTopup2Frag();
				}
			}
		};
		TwoBtnCell twoBtnCell = new TwoBtnCell(getResString(R.string.btn_back) , "", -1,-1, null);
		twoBtnCell.setPadding(basePadding);
		twoBtnCell.setLeftClickListener(onBackClick);
		twoBtnCell.setRightClickListener(onContinueClick);
		cellList.add(twoBtnCell);
		twoBtncellPos = cellList.indexOf(twoBtnCell);

		cellViewAdapter.setView(lobString, frame, cellList);
	}

	public void set1010UI() {
		cellList = new ArrayList<Cell>();		

		offerListcellPos = -1;
		topupHistcellPos = -1;

		//Title
		int mmsImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
		IconTextCell iconTextCell = new IconTextCell(mmsImg, getResString(R.string.usage_boost_up)); 
		cellList.add(iconTextCell);

		//Current local mobile data
		String title = isZh ? g3DisplayServiceItemDTO.getTitleLevel1Chi() : g3DisplayServiceItemDTO.getTitleLevel1Eng();
		BigTextCell mobileDataCell = new BigTextCell(title + ":",isZh? g3DisplayServiceItemDTO.getUsageDescription().getTextChi() : g3DisplayServiceItemDTO.getUsageDescription().getTextEng());
		mobileDataCell.setArrowShown(true);
		mobileDataCell.setTitleSizeDelta(0);
		mobileDataCell.setContentSizeDelta(0);
		mobileDataCell.setTopMargin(0);
		mobileDataCell.setMargin(basePadding);
		mobileDataCell.setTitleColorId(R.color.hkt_txtcolor_grey);
		mobileDataCell.setContentColorId(R.color.topup_orange);
		cellList.add(mobileDataCell);

		//Monthly package
		String mthPkgMbStr = planMobcra.getOMthlyPkgMB();

		double mthlyPkgMB = 0;
		if (mthPkgMbStr != null && !mthPkgMbStr.equals("")) { mthlyPkgMB = Double.parseDouble(mthPkgMbStr); }	
		SmallTextCell monthlyPckCell = new SmallTextCell(getResString(R.string.usage_boost_up_monthly_package) , String.format("%s %s", Utils.formatNumber((int) mthlyPkgMB), getResString(R.string.MB)));
		monthlyPckCell.setLeftPadding(basePadding*2);
		monthlyPckCell.setRightPadding(basePadding);
		cellList.add(monthlyPckCell);

		//Total entitlement
		String ttlEntMbStr = planMobcra.getOTtlEntMB();

		double oTtlEntMb = 0;
		if (ttlEntMbStr != null && !ttlEntMbStr.equals("")) { oTtlEntMb = Double.parseDouble(ttlEntMbStr); }
		SmallTextCell totalEntileCell = new SmallTextCell(getResString(R.string.usage_total_entitlement), String.format("%s %s", Utils.formatNumber((int) oTtlEntMb), getResString(R.string.MB)));
		totalEntileCell.setLeftPadding(basePadding*2);
		totalEntileCell.setRightPadding(basePadding);
		cellList.add(totalEntileCell);

		
		//Package Remark1
		SmallTextCell packageRmkCell = new SmallTextCell(getResString(R.string.myhkt_usage_remark1), null);
		packageRmkCell.setTitleSizeDelta(-2);
		packageRmkCell.setLeftPadding(basePadding*2);
		packageRmkCell.setRightPadding(basePadding);
		packageRmkCell.setBotPadding(basePadding*4);
		cellList.add(packageRmkCell);

		//Topup history record
		OnClickListener onDetailClick = new OnClickListener(){
			@Override 
			public void onClick(View v) {
				if (mQuickActionHist !=null && actionHistItem !=null && actionHistItem.length >0){
					mQuickActionHist.show(v, (View)v.getParent());
				}					
			}			
		};

		G3RechargeHistDTO[] g3RechargeHistDTO = planMobcra.getOG3RechargeHistDTOAry();
		if (g3RechargeHistDTO != null && g3RechargeHistDTO.length > 0) {
			TextBtnCell histRec;
			histRec = new TextBtnCell(getResString(R.string.usage_boost_up_history), getResString(R.string.usage_boost_up_detail), -1, null);
			histRec.setLeftPadding(basePadding * 2);
			histRec.setRightPadding(basePadding);
			histRec.setOnClickListener(onDetailClick);
			cellList.add(histRec);
			topupHistcellPos = cellList.indexOf(histRec);
		} else {
			SmallTextCell histRec = new SmallTextCell(getResString(R.string.usage_boost_up_history), getResString(R.string.usage_boost_up_hist_no_rec));
			histRec.setLeftPadding(basePadding * 2);
			histRec.setRightPadding(basePadding);
			cellList.add(histRec);
		}

		//Remark1
		G3UsageQuotaDTO otherDTO;
		if ("P".equalsIgnoreCase(planMobcra.getOMobUsage().getG3UsageQuotaDTO().getPrimaryFlag())) {
			otherDTO = planMobcra.getOMobUsage().getG3SummUsageQuotaDTO();
		} else {
			otherDTO = planMobcra.getOMobUsage().getG3UsageQuotaDTO();
		}
		String nextBillDate =  otherDTO.getNxtBilCutoffDt();
		SmallTextCell topupHisRmk1Cell = new SmallTextCell("*" + String.format(getResString(R.string.usage_boost_up_remark) , getString(R.string.usage_boost_up_next_bill), Utils.toDateString(nextBillDate, FORMAT_YYYYMMDD, "dd/MM/yyyy 23:59"), ""), null);
		topupHisRmk1Cell.setTitleSizeDelta(-2);
		topupHisRmk1Cell.setLeftPadding(basePadding*2);
		topupHisRmk1Cell.setRightPadding(basePadding);
		cellList.add(topupHisRmk1Cell);
		
		//Remark2
		SmallTextCell topupHisRmk2Cell = new SmallTextCell(getResString(R.string.myhkt_usage_remark2), null);
		topupHisRmk2Cell.setTitleSizeDelta(-2);
		topupHisRmk2Cell.setLeftPadding(basePadding*2);
		topupHisRmk2Cell.setRightPadding(basePadding);
		topupHisRmk2Cell.setBotPadding(basePadding*4);
		cellList.add(topupHisRmk2Cell);

		//Topup offer list
		//Package select header
		SmallTextCell packeSelHeader = new SmallTextCell(getResString(R.string.boost_up_select_note), null);
		//		packeSelHeader.setTitleSizeDelta(-2);
		packeSelHeader.setLeftPadding(basePadding);
		packeSelHeader.setRightPadding(basePadding);
		packeSelHeader.setTitleColorId(R.color.black);


		//Offer list selector
		SpinnerTextCell spinnerTextCell = new SpinnerTextCell("", spinnerClick);
		spinnerTextCell.setLeftPadding(basePadding);
		spinnerTextCell.setRightPadding(basePadding);

		//No offer list remark
		SmallTextCell noOfferRemark = new SmallTextCell(getResString(R.string.boost_up_no_offer_list), null);
		//		noOfferRemark.setTitleSizeDelta(-2);
		noOfferRemark.setLeftPadding(basePadding);
		noOfferRemark.setRightPadding(basePadding);
		noOfferRemark.setTitleColorId(R.color.black);

		if ("1".equalsIgnoreCase(planMobcra.getOVbpCustTy())) {
			noOfferRemark.setTitle(getResString(R.string.PLNMBF_TP_AUTO));
			cellList.add(noOfferRemark);	
		} else if ("3".equalsIgnoreCase(planMobcra.getOVbpCustTy())) {
			noOfferRemark.setTitle(getResString(R.string.PLNMBF_TP_HIGER));
			cellList.add(noOfferRemark);	
		} else if ("Y".equalsIgnoreCase(g3DisplayServiceItemDTO.getAllowTopup())) {
			G3BoostUpOffer1DTO[] g3BoostUpOfferDTO = planMobcra.getOG3BoostUpOffer1DTOAry();
			if (g3BoostUpOfferDTO !=null && g3BoostUpOfferDTO.length > 0) {
				cellList.add(packeSelHeader);
				cellList.add(lineCell);
				cellList.add(spinnerTextCell);
				offerListcellPos = cellList.indexOf(spinnerTextCell);
				cellList.add(lineCell);
			} else {
				cellList.add(noOfferRemark);			
			}			
		} else {
			cellList.add(noOfferRemark);
		}

		if (offerListcellPos > 0) {
			setOfferList1010(planMobcra);
		} 
		if (topupHistcellPos > 0) {
			setTouUpHistory1010(planMobcra);
		}

		//Back , Continue
		OnClickListener onBackClick = new OnClickListener(){
			@Override
			public void onClick(View v) {
				callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_USAGEDATA);
				callback_usage.displayChildview(true);		
			}			
		};
		OnClickListener onContinueClick = new OnClickListener(){
			@Override
			public void onClick(View v) {
				if (g3BoostUpOfferDTO !=null && g3BoostUpOfferDTO.length > 0) {
					callback_usage.getSelectedG3BoostUpOfferDTO(g3BoostUpOfferDTO[offerPos]);
					callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP2);
					callback_usage.displayChildview(false);
					//					callback_usage.goToUsageTopup2Frag();
				}		
			}			
		};

		TwoBtnCell twoBtnCell;
		if (!cellList.contains(noOfferRemark)) { 
			twoBtnCell = new TwoBtnCell(getResString(R.string.btn_back) , getResString(R.string.btn_continue), -1,-1, null);
		} else {
			//Hide the Continue btn
			twoBtnCell = new TwoBtnCell(getResString(R.string.btn_back) , "", -1,-1, null);
		}
		twoBtnCell.setPadding(basePadding);
		twoBtnCell.setLeftClickListener(onBackClick);
		twoBtnCell.setRightClickListener(onContinueClick);
		cellList.add(twoBtnCell); 

		cellViewAdapter.setView(frame, cellList);
	}

	public void onDetailClick(View v) {	
		if (mQuickActionHist !=null && actionHistItem !=null && actionHistItem.length >0){
			mQuickActionHist.show(v, (View)v.getParent());
		}				
	}
	public void onSpinnerClick(View v) {	
		if (mQuickActionOffer !=null && actionOfferItem !=null && actionOfferItem.length >0){
			mQuickActionOffer.setOnPickerScrollSingleListener(new QuickAction.OnPickerScrollSingleListener() {
				@Override
				public void onPickerScroll(String result, int pos) {
					offerPos = pos;
					SpinnerTextCell spinnerTextCell = new SpinnerTextCell(actionOfferItem[pos].getTitle(), spinnerClick);
					cellList.set(offerListcellPos, spinnerTextCell);
//
					cellViewAdapter.changeView(frame, offerListcellPos, spinnerTextCell);	
				}
			
			});
			mQuickActionOffer.showPicker(v, (View)v.getParent());

		}				
	}

	protected void cleanupUI(){

	}

	private final void getRechargeHistory() {
		// Prepared for AsyncTask, Followed by MyAccountApp
		String begDate = null;
		String endDate = null;

		String currHKTime = null;
		// The new ServerTS serves the same purpose
		currHKTime = planMobcra.getServerTS();

		String bomStartBillDate = planMobcra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomStartBillDate();
		String bomNextBillDate = planMobcra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate();

		// JF-20111130 ++
		// for new SIM card
		if (bomStartBillDate == null) {
			// set (start bill date = next bill date -1 mth)
			try {
				SimpleDateFormat nextDf = new SimpleDateFormat(FORMAT_YYYYMMDD, Locale.US);
				Date nextDate = nextDf.parse(planMobcra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate());

				Calendar cal = Calendar.getInstance();
				cal.setTime(nextDate);
				cal.add(Calendar.MONTH, -1);

				bomStartBillDate = nextDf.format(cal.getTime());
			} catch (ParseException e) {
				// Next bill date parse error
				bomStartBillDate = bomNextBillDate;
			} catch (NullPointerException npe) {
				// bomStartBillDate and bomNextBillDate are null
				begDate = "";
				endDate = "";
			}
		}

		// JF-20111130 --
		try {
			if ("D".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getQuotaTopupInfo().getQuotaValidityPeriod())) {
				// MyAccountApp: dd/MM/yyyy HH:mm changed to CS Portal: yyyyMMddHHmmmss
				begDate = Utils.toDateString(currHKTime, "yyyyMMddHHmmss", FORMAT_YYYYMMDD);
				endDate = Utils.toDateString(currHKTime, "yyyyMMddHHmmss", FORMAT_YYYYMMDD);
			} else {
				begDate = Utils.toDateString(bomStartBillDate, FORMAT_YYYYMMDD, FORMAT_YYYYMMDD);
				endDate = Utils.toDateString(bomNextBillDate, FORMAT_YYYYMMDD, FORMAT_YYYYMMDD);
			}
		} catch (Exception e) {
			// fail to get begDate and endDate
		}
		// END -------- Prepared for APIsManager call

		PlanMobCra mPlanMobCra = new PlanMobCra();

		mPlanMobCra.setISubnRec(callback_service.getSubnRec());
		mPlanMobCra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getLgiCra().getOQualSvee().getSveeRec().loginId : "");

		mPlanMobCra.setITopupItem(callback_usage.getSelectedServiceItem());
		mPlanMobCra.setIBstUpHistEnDt(endDate);		
		mPlanMobCra.setIBstUpHistStDt(begDate);
		//		mPlanMobCra.setITopupActn(PlanMobCra.TP_ACTN_I);

		APIsManager.doTopupHist(me, mPlanMobCra);
	}

	private void doneGetRechargeHistory(PlanMobCra mPlanMobcra){
		Log.i(TAG, "doneGetRechargeHistory");
		setTouUpHistory(mPlanMobcra);
		//		cellViewAdapter.setView(frame,cellList);
		getTopupItem();
	}

	private final void getTopupItem() {
		PlanMobCra mPlanMobCra = new PlanMobCra();
		mPlanMobCra.setISubnRec(callback_service.getSubnRec());
		mPlanMobCra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getLgiCra().getOQualSvee().getSveeRec().loginId : "");
		mPlanMobCra.setITopupItem(callback_usage.getSelectedServiceItem());
		mPlanMobCra.setITopupActn(PlanMobCra.TP_ACTN_I);		
		APIsManager.doTopupItem(me, mPlanMobCra);
	}

	private void doneGetTopupItem(PlanMobCra mPlanMobcra){
		Log.i(TAG, "doneGetTopupItem");
		offerPos = 0;
		setOfferList(mPlanMobcra);		
		cellViewAdapter.setView(frame,cellList);
	}

	private void setOfferList(PlanMobCra mPlanMobcra) {
		//Boost up Offer
		g3BoostUpOfferDTO = mPlanMobcra.getOG3BoostUpOffer1DTOAry();
		if (RC.PLAN_TPUP_DENIED.equalsIgnoreCase(mPlanMobcra.getReply().getCode())){
			//Student plan
			SmallTextCell packeSelHeader = new SmallTextCell(getResString(R.string.boost_up_select_note), null);
			packeSelHeader.setIsVisible(false);
			cellList.set(offerListcellPos-2, packeSelHeader);
			//No offer list remark
			SmallTextCell noOfferRemark = new SmallTextCell(getResString(R.string.STDPLN_REMARK), null);
			noOfferRemark.setTitleSizeDelta(-2);
			noOfferRemark.setLeftPadding(basePadding);
			noOfferRemark.setRightPadding(basePadding);
			noOfferRemark.setTitleColorId(R.color.black);
			cellList.set(offerListcellPos, noOfferRemark);
		} else 	if (RC.MOBA_TPUP_DUPL.equalsIgnoreCase(mPlanMobcra.getReply().getCode())){
			//Student plan
			SmallTextCell packeSelHeader = new SmallTextCell(getResString(R.string.boost_up_select_note), null);
			packeSelHeader.setIsVisible(false);
			cellList.set(offerListcellPos-2, packeSelHeader);
			//No offer list remark
			Cell invLineCell = new Cell(Cell.LINE);
			invLineCell.setIsVisible(false);
			cellList.set(offerListcellPos-1, invLineCell);
			SmallTextCell noOfferRemark = new SmallTextCell(getResString(R.string.STDPLN_REMARK), null);
			noOfferRemark.setTitleSizeDelta(-2);
			noOfferRemark.setLeftPadding(basePadding);
			noOfferRemark.setRightPadding(basePadding);
			noOfferRemark.setTitleColorId(R.color.black);
			noOfferRemark.setIsVisible(false);
			cellList.set(offerListcellPos, noOfferRemark);
			cellList.set(offerListcellPos+1, invLineCell);
			
		} else if ((g3BoostUpOfferDTO != null && g3BoostUpOfferDTO.length > 0)){
				
			actionOfferItem = new ActionItem[g3BoostUpOfferDTO.length];
			String[] offerStringList = new String [g3BoostUpOfferDTO.length];
			for (int i=0; i< g3BoostUpOfferDTO.length; i++) {

				String plan;
				if ("zh".equalsIgnoreCase(ClnEnv.getAppLocale(getActivity().getBaseContext()))) {
					plan = g3BoostUpOfferDTO[i].getChiDescription();
				} else {
					plan = g3BoostUpOfferDTO[i].getEngDescription();
				}
				actionOfferItem[i] = new ActionItem(i, plan);
				offerStringList[i] = plan;
			}
			final OnClickListener onSpinnerClick = new OnClickListener(){
				@Override
				public void onClick(View v) {
					if (mQuickActionOffer !=null && actionOfferItem !=null && actionOfferItem.length >0){
						mQuickActionOffer.showPicker(v, (View)v.getParent());
					}					
				}			
			};		
			
			mQuickActionOffer = new QuickAction(me.getActivity(), 0, 0);			
			mQuickActionOffer.initPickerWheelSingle(offerStringList ,offerPos);
			mQuickActionOffer.setOnPickerScrollSingleListener(new QuickAction.OnPickerScrollSingleListener() {
			@Override
			public void onPickerScroll(String result, int pos) {
				offerPos = pos;
				SpinnerTextCell spinnerTextCell = new SpinnerTextCell(result, spinnerClick);
				spinnerTextCell.setOnClickListener(onSpinnerClick);
				cellList.set(offerListcellPos, spinnerTextCell);

				cellViewAdapter.changeView(frame, offerListcellPos, spinnerTextCell);					
			}			
		});
			SpinnerTextCell spinnerTextCell = new SpinnerTextCell(actionOfferItem[offerPos].getTitle(), spinnerClick);
			spinnerTextCell.setOnClickListener(onSpinnerClick);
			cellList.set(offerListcellPos, spinnerTextCell);	

			//Back , Continue
			OnClickListener onBackClick = new OnClickListener(){
				@Override
				public void onClick(View v) {
					callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_USAGEDATA);
					callback_usage.displayChildview(true);		
				}			
			};
			OnClickListener onContinueClick = new OnClickListener(){
				@Override
				public void onClick(View v) {
					if (g3BoostUpOfferDTO !=null && g3BoostUpOfferDTO.length > 0) {
						callback_usage.getSelectedG3BoostUpOfferDTO(g3BoostUpOfferDTO[offerPos]);
						callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP2);
						callback_usage.displayChildview(false);
						//					callback_usage.goToUsageTopup2Frag();
					}		
				}
			};
			TwoBtnCell twoBtnCell = new TwoBtnCell(getResString(R.string.btn_back) , getResString(R.string.btn_continue), -1,-1, null);
			twoBtnCell.setPadding(basePadding);
			twoBtnCell.setLeftClickListener(onBackClick);
			twoBtnCell.setRightClickListener(onContinueClick);
			cellList.set(twoBtncellPos, twoBtnCell);	
		} else {
			SmallTextCell packeSelHeader = new SmallTextCell(getResString(R.string.boost_up_select_note), null);
			packeSelHeader.setIsVisible(false);
			cellList.set(offerListcellPos-2, packeSelHeader);	
			//No offer list remark
			SmallTextCell noOfferRemark = new SmallTextCell(getResString(R.string.boost_up_no_offer_list), null);
			noOfferRemark.setTitleSizeDelta(-2);
			noOfferRemark.setLeftPadding(basePadding);
			noOfferRemark.setRightPadding(basePadding);
			noOfferRemark.setTitleColorId(R.color.black);
			cellList.set(offerListcellPos, noOfferRemark);	
		}	
	}

	private void setOfferList1010(PlanMobCra mPlanMobcra) {
		//Boost up Offer
		g3BoostUpOfferDTO = mPlanMobcra.getOG3BoostUpOffer1DTOAry();
		String[] offerStringList; 
		if (g3BoostUpOfferDTO!= null && g3BoostUpOfferDTO.length > 0) {
			offerStringList = new String[g3BoostUpOfferDTO.length];
			final OnClickListener onSpinnerClick = new OnClickListener(){
				@Override
				public void onClick(View v) {
					if (mQuickActionOffer !=null && actionOfferItem !=null && actionOfferItem.length >0){
						mQuickActionOffer.showPicker(v, (View)v.getParent());
					}					
				}			
			};		
			actionOfferItem = new ActionItem[g3BoostUpOfferDTO.length];
			for (int i=0; i< g3BoostUpOfferDTO.length; i++) {

				String plan;
				if ("zh".equalsIgnoreCase(ClnEnv.getAppLocale(getActivity().getBaseContext()))) {
					plan = g3BoostUpOfferDTO[i].getChiDescription();
				} else {
					plan = g3BoostUpOfferDTO[i].getEngDescription();
				}
				actionOfferItem[i] = new ActionItem(i, plan);
				offerStringList[i] = plan;
			}
			mQuickActionOffer = new QuickAction(me.getActivity(), 0, 0);
			mQuickActionOffer.initPickerWheelSingle(offerStringList ,offerPos);
			mQuickActionOffer.setOnPickerScrollSingleListener(new QuickAction.OnPickerScrollSingleListener() {
				@Override
				public void onPickerScroll(String result, int pos) {
					offerPos = pos;
					SpinnerTextCell spinnerTextCell = new SpinnerTextCell(result, spinnerClick);
					spinnerTextCell.setOnClickListener(onSpinnerClick);
					cellList.set(offerListcellPos, spinnerTextCell);

					cellViewAdapter.changeView(frame, offerListcellPos, spinnerTextCell);					
				}			
			});

			SpinnerTextCell spinnerTextCell = new SpinnerTextCell(offerStringList[0], spinnerClick);
			spinnerTextCell.setOnClickListener(onSpinnerClick);
			cellList.set(offerListcellPos, spinnerTextCell);	
		} else {
			SmallTextCell packeSelHeader = new SmallTextCell(getResString(R.string.boost_up_select_note), null);
			packeSelHeader.setIsVisible(false);
			cellList.set(offerListcellPos-2, packeSelHeader);	
			//No offer list remark
			SmallTextCell noOfferRemark = new SmallTextCell(getResString(R.string.boost_up_no_offer_list), null);
			noOfferRemark.setTitleSizeDelta(-2);
			noOfferRemark.setLeftPadding(basePadding);
			noOfferRemark.setRightPadding(basePadding);
			noOfferRemark.setTitleColorId(R.color.black);
			cellList.set(offerListcellPos, noOfferRemark);	
		}	
	}

	private void setTouUpHistory(PlanMobCra mpPlanMobCra){
		Log.i(TAG, "setTouUpHistory");
		//History
		G3RechargeHistDTO[] rechargeHistory = mpPlanMobCra.getOG3RechargeHistDTOAry();
		String[] hisStringList;
		if (rechargeHistory.length > 0) {
			Log.i(TAG, "setTouUpHistory1");
			actionHistItem = new ActionItem[rechargeHistory.length];
			hisStringList = new String[rechargeHistory.length];
			double rechargeValueTotal = 0;			
			for (int i=0; i< rechargeHistory.length; i++){
				G3RechargeHistDTO item  = rechargeHistory[i] ;
				String sbBoostHist = "";
				String boostDate = item.getTransactionDateTime(); // JF-20111215
				if (boostDate.length() >= 8) {
					if (boostDate.length() == 14) {
						if (isMob) {
							sbBoostHist = Utils.toDateString(boostDate, "yyyyMMddHHmmss", "dd/MM/yyyy HH:mm");
						} else {
							sbBoostHist = Utils.toDateString(boostDate, "yyyyMMddHHmmss", "dd/MM/yyyy");
						}
					} else {
						sbBoostHist = boostDate;
					}
				} else {
					sbBoostHist = item.getTransactionDateTime();
				}
				//				}
				sbBoostHist +=  " - ";

				sbBoostHist += Utils.formattedDataByMBStr(item.getRechargeValueInMB(), HKTDataType.HKTDataTypeEntitled, true);//String.format("%s ", Utils.formatNumber((int) Math.round(rechargeValue)));

				actionHistItem[i] = new ActionItem(i, sbBoostHist);
				hisStringList[i] = new String(sbBoostHist);
			}
			mQuickActionHist = new QuickAction(me.getActivity(), 0, 0);
			mQuickActionHist.initPickerWheelSingle(hisStringList ,0);

			OnClickListener onDetailClick = new OnClickListener(){
				@Override
				public void onClick(View v) {
					
					if (mQuickActionHist !=null && actionHistItem !=null && actionHistItem.length >0){
						mQuickActionHist.setOnPickerScrollSingleListener(new QuickAction.OnPickerScrollSingleListener() {
							@Override
							public void onPickerScroll(String result, int pos) {
							
							}
						
						});
						mQuickActionHist.showPicker((View)v.getParent(), (View)v.getParent().getParent());
					}					
				}			
			};
			TextBtnCell histRec = new TextBtnCell(getResString(R.string.usage_boost_up_history), getResString(R.string.usage_boost_up_detail), -1, null);
			histRec.setOnClickListener(onDetailClick);
			histRec.setLeftPadding(basePadding * 2);
			histRec.setRightPadding(basePadding);
			cellList.set(topupHistcellPos, histRec);

		} else {
			Log.i(TAG, "setTouUpHistory2");
			//Change the text where there is no history record
			SmallTextCell histRec = new SmallTextCell(getResString(R.string.usage_boost_up_history), getResString(R.string.usage_boost_up_hist_no_rec));
			histRec.setLeftPadding(basePadding*2);
			histRec.setRightPadding(basePadding);
			histRec.setTitleColorId(R.color.black);
			cellList.set(topupHistcellPos, histRec);	
		}

		com.pccw.dango.shared.g3entity.G3AcctBomNextBillDateDTO g3AcctBomNextBillDateDTO = callback_usage.getPlanMobCra().getOMobUsage().getG3AcctBomNextBillDateDTO();
		if (g3AcctBomNextBillDateDTO != null) {
			String nextBillDate = null;
			SmallTextCell histRemark1Cell  = new SmallTextCell("", "");
			if (g3AcctBomNextBillDateDTO.getBomNextBillDate() != null) {
				try {
					//					SimpleDateFormat nextDf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
					SimpleDateFormat nextDf = new SimpleDateFormat(FORMAT_YYYYMMDD, Locale.ENGLISH);
					Date nextDate = nextDf.parse(g3AcctBomNextBillDateDTO.getBomNextBillDate());
					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
					nextBillDate = df.format(nextDate);
					if ("ROAMING".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getRegion()) || "GLOBAL".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getRegion())) {
						if ("B".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getQuotaTopupInfo().getQuotaValidityPeriod())) {
							histRemark1Cell.setTitle(String.format("*" + getResString(R.string.usage_boost_up_remark), getString(R.string.usage_boost_up_next_bill), nextBillDate + " ", getString(R.string.usage_boost_up_hk_time)));
						} else {
							// Day Pass
							try {
								Calendar nextBillDateDayPass = Calendar.getInstance();
								SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
								Date d = sdf.parse(planMobcra.getServerTS());
								nextBillDateDayPass.setTime(d);
								nextBillDateDayPass.add(Calendar.DATE, 1);
								nextBillDateDayPass.set(Calendar.HOUR_OF_DAY, 0);
								nextBillDateDayPass.set(Calendar.MINUTE, 0);
								nextBillDateDayPass.set(Calendar.MILLISECOND, 0);
								nextBillDate = df.format(nextBillDateDayPass.getTime());
								histRemark1Cell.setTitle("*" + String.format(getResString(R.string.usage_boost_up_remark), getString(R.string.usage_boost_up_next_bill), nextBillDate + " ", getString(R.string.usage_boost_up_hk_time)));
							} catch (Exception e1) {
								// fail to convert String to Date
								histRemark1Cell.setTitle("");
							}
						}
					} else {						
						histRemark1Cell.setTitle("*" + String.format(getResString(R.string.usage_boost_up_remark), getString(R.string.usage_boost_up_next_bill), nextBillDate + " ", ""));
					}
				} catch (ParseException e) {
					// Next bill date parse error
					histRemark1Cell.setTitle("");
				}
				histRemark1Cell.setLeftPadding(basePadding);
				histRemark1Cell.setRightPadding(basePadding);
				//				histRemark1Cell.setTitleColorId(R.color.black);
				histRemark1Cell.setTitleSizeDelta(-2);
				cellList.set(topupHistRemkcellPos, histRemark1Cell);
			}
		}	
	}

	private void setTouUpHistory1010(PlanMobCra mpPlanMobCra){

		//History
		G3RechargeHistDTO[] rechargeHistory = mpPlanMobCra.getOG3RechargeHistDTOAry();
		String[] hisStringList;
		if (rechargeHistory.length > 0) {
			actionHistItem = new ActionItem[rechargeHistory.length];
			hisStringList = new String[rechargeHistory.length];
			double rechargeValueTotal = 0;			
			for (int i=0; i< rechargeHistory.length; i++){
				G3RechargeHistDTO item  = rechargeHistory[i] ;
				String sbBoostHist = "";
				String boostDate = item.getTransactionDateTime(); // JF-20111215
				if (boostDate.length() >= 8) {
					if (boostDate.length() == 14) {
						sbBoostHist = Utils.toDateString(boostDate, "yyyyMMddHHmmss", "dd/MM/yyyy");
					} else {
						sbBoostHist = boostDate;
					}
				} else {
					sbBoostHist = item.getTransactionDateTime();
				}
				//				}
				sbBoostHist +=  " - ";
				try {
					double rechargeValue = Double.parseDouble(item.getRechargeValueInMB());
					//					rechargeValueTotal += rechargeValue;
					sbBoostHist += String.format("%s ", Utils.formatNumber((int) Math.round(rechargeValue)));
					//					totalEntitlementValueInMB += rechargeValue;
				} catch (NumberFormatException e) {
					// NumberFormat Exception
				} catch (Exception e) {
					// Exception
				}
				sbBoostHist += getResString(R.string.MB);
				actionHistItem[i] = new ActionItem(i, sbBoostHist);
				hisStringList[i] = sbBoostHist;
			}
			mQuickActionHist = new QuickAction(me.getActivity(), 0, 0);
			mQuickActionHist.initPickerWheelSingle(hisStringList ,0);
//			for (int i = 0; i < actionHistItem.length; i++) {
//				mQuickActionHist.addActionItem(actionHistItem[i]);				
//			}		
			OnClickListener onDetailClick = new OnClickListener(){
				@Override
				public void onClick(View v) {
					if (mQuickActionHist !=null && actionHistItem !=null && actionHistItem.length >0){
						mQuickActionHist.showPicker((View)v.getParent(), (View)v.getParent().getParent());
					}					
				}
			};

			TextBtnCell histRec = new TextBtnCell(getResString(R.string.usage_boost_up_history), getResString(R.string.usage_boost_up_detail), -1, null);
			histRec.setOnClickListener(onDetailClick);
			histRec.setLeftPadding(basePadding * 2);
			histRec.setRightPadding(basePadding);
			cellList.set(topupHistcellPos, histRec);

		} else {
			//Change the text where there is no history record
			SmallTextCell histRec = new SmallTextCell(getResString(R.string.usage_boost_up_history), getResString(R.string.usage_boost_up_hist_no_rec));
			histRec.setLeftPadding(basePadding*2);
			histRec.setRightPadding(basePadding);
			histRec.setTitleColorId(R.color.black);
			cellList.set(topupHistcellPos, histRec);	
		}		
	}


	@Override
	public void onSuccess(APIsResponse response) {
		if (response != null) {
			if (APIsManager.TPUP_HIST.equals(response.getActionTy())) {
				PlanMobCra mPlanMobcra = (PlanMobCra) response.getCra();
				doneGetRechargeHistory(mPlanMobcra);
			} else if (APIsManager.TPUP.equals(response.getActionTy())) {
				PlanMobCra mPlanMobcra = (PlanMobCra) response.getCra();
				doneGetTopupItem(mPlanMobcra);
			} else if (APIsManager.AO_AUTH.equals(response.getActionTy())) {
				addOnCra = (AddOnCra) response.getCra();
				addOnCra.getOSubnRec().ivr_pwd = addOnCra.getISubnRec().ivr_pwd;
				
				callback_service.setSubscriptionRec(addOnCra.getOSubnRec());
				callback_main.setSubscriptionRec(addOnCra.getOSubnRec());
				callback_main.getAcctAgent().setSubnRec(addOnCra.getOSubnRec());
				
				//recall
				if (recallAction != null) {
					if (APIsManager.TPUP_HIST.equals(recallAction)) {
						getRechargeHistory();
					} else if (APIsManager.TPUP.equals(recallAction)) {
						getTopupItem();
					}
					recallAction = null;
				}
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (debug) Log.i(TAG, "fail");
		if (response != null) {		
			if (APIsManager.TPUP_HIST.equals(response.getActionTy())) {
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					DialogHelper.createSimpleDialog(me.getActivity(), response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
					recallDoAuth(me, callback_service.getSubnRec());
					recallAction = response.getActionTy();
				} else {
					DialogHelper.createSimpleDialog(me.getActivity(), ClnEnv.getRPCErrMsg(this.getActivity(), response.getReply().getCode()));
				}
			} else if (APIsManager.TPUP.equals(response.getActionTy())) {
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					DialogHelper.createSimpleDialog(me.getActivity(), response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
					recallDoAuth(me, callback_service.getSubnRec());
					recallAction = response.getActionTy();
				} else if (RC.MOBA_TPUP_DUPL.equalsIgnoreCase(response.getReply().getCode())) {
					DialogHelper.createSimpleDialog(me.getActivity(), Utils.getString(me.getActivity(), R.string.boost_result_dup_top_up_error));
					//to display topup history even the topup option is not received
					PlanMobCra mPlanMobcra = (PlanMobCra) response.getCra();
					doneGetTopupItem(mPlanMobcra);
				} else if (RC.PLAN_TPUP_DENIED.equalsIgnoreCase(response.getReply().getCode())) {
					PlanMobCra mPlanMobcra = (PlanMobCra) response.getCra();
					doneGetTopupItem(mPlanMobcra);
				} else {
					DialogHelper.createSimpleDialog(me.getActivity(), ClnEnv.getRPCErrMsg(this.getActivity(), response.getReply().getCode()));
				}
			}
		}
	}
}
