package com.pccw.myhkt.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.fragment.app.ListFragment;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.pccw.myhkt.BuildConfig;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.InboxListActivity;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.activity.MainMenuActivity;
import com.pccw.myhkt.activity.SettingsActivity;
import com.pccw.myhkt.service.APIClient;
import com.pccw.myhkt.service.GetDataService;
import com.pccw.myhkt.util.RuntimePermissionUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Updates:
 * 07302018		Abdulmoiz Esmail	-Added the inbox item in the list
 * 03242023		Luke Garces		    -Added the menu item General Condition and Privacy Statement
 */

public class SlidingMenuFragment extends ListFragment {
    private SlidingMenu slidingmenu;
    protected boolean debug = false;
    private SlideMenuAdapter adapter;
    private final String TAG = "lwg";

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.menu_list, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setDividerHeight(0);
        getListView().setDivider(null);
        debug = getResources().getBoolean(R.bool.DEBUG);
        List<Integer> menuOptName;
        List<Integer> menuOptImg;
        menuOptName = new ArrayList<>();
        menuOptName.add(R.string.myhkt_settings);
        menuOptName.add(R.string.myhkt_about);
        menuOptName.add(R.string.myhkt_menu_tnc);
        menuOptName.add(R.string.myhkt_gen_conditions);
        menuOptName.add(R.string.myhkt_menu_privacy_statement);

        menuOptImg = new ArrayList<>();
        menuOptImg.add(R.drawable.settting);
        menuOptImg.add(R.drawable.aboutmyhkt);
        menuOptImg.add(R.drawable.ios7_tnc);
        menuOptImg.add(R.drawable.ios7_tnc);
        menuOptImg.add(R.drawable.ios7_tnc);

        if (ClnEnv.isLoggedIn()) {
            menuOptName.add(0, R.string.title_inbox);
            menuOptImg.add(0, R.drawable.app_mymessage);
            menuOptName.add(6, R.string.myhkt_LGIF_LOGIN);
            menuOptImg.add(6, R.drawable.ios7_logout);
        } else {
            menuOptName.add(0, R.string.myhkt_LGIF_LOGIN);
            menuOptImg.add(0, R.drawable.ios7_login);
        }

        adapter = new SlideMenuAdapter(getActivity());
        slidingmenu = ((MainMenuActivity) requireActivity()).slidingmenu;

        for (int i = 0; i < menuOptName.size(); i++) {
            adapter.add(new MenuOptionItem(menuOptName.get(i), menuOptImg.get(i)));
        }
        setBackground();
        setListAdapter(adapter);
    }

    public void setBackground() {
        ((ListView) this.requireView()).setBackgroundColor(this.getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.menu_bg_premier : R.color.hkt_slidemenu_blue));
    }

    private void showContent() {
        if (slidingmenu != null) {
            slidingmenu.showContent();
        }
    }

    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        Intent intent;
        switch (position) {
            case 0:
                if (!ClnEnv.isLoggedIn()) {
                    //login
                    showContent();
                    intent = new Intent(requireActivity().getApplicationContext(), LoginActivity.class);
                    requireActivity().startActivity(intent);
                    requireActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                } else {
                    //inbox list
                    showContent();
                    intent = new Intent(requireActivity().getApplicationContext(), InboxListActivity.class);
                    requireActivity().startActivity(intent);
                    requireActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                }
                break;
            case 1:
                //setting
                showContent();
                intent = new Intent(requireActivity().getApplicationContext(), SettingsActivity.class);
                requireActivity().startActivity(intent);
                requireActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case 2:
                //about
                displayAboutDialog();
                break;
            case 3:
                displayTNCSDialog();
                break;
            case 4:
                try {
                    String urlHostLink = getString(R.string.url_general_conditions);
                    URL urLink = new URL(urlHostLink);
                    String filePath = urLink.getPath();
                    // Get filepath file name
                    String fileName = filePath.substring(filePath.lastIndexOf('/') + 1);
                    Log.d(TAG, "fileName: " + fileName);

                    GetDataService service = APIClient.getRetrofitPDFInstance().create(GetDataService.class);
                    Call<ResponseBody> call = service.downloadFileWithFixedUrl(urlHostLink);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            writeResponseBodyToDisk(response.body(), fileName);
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                break;
            case 5:
                urlLink(getString(R.string.url_privacy_statement));
                break;
            case 6:
                if (ClnEnv.isLoggedIn()) {
                    //logout
                    showContent();
                    ((MainMenuActivity) requireActivity()).doLogout();
                }
                break;
        }
    }

    // About My HKT
    protected final void displayAboutDialog() {

        String versionNum = "";
        try {
            versionNum = requireActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            if (debug) {
                versionNum = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName + " (Build: " + getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode + ")";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String message = String.format("%s\n%s\n%s %s", Utils.getString(requireActivity(), R.string.myhkt_about_appname), Utils.getString(getActivity(), R.string.myhkt_about_copyright), Utils.getString(getActivity(), R.string.myhkt_about_version), versionNum);
        String title = getResources().getString(R.string.myhkt_about);

        DialogHelper.createTitleDialog(getActivity(), title, message, getResources().getString(R.string.btn_ok));
    }

    public void refreshView() {
        List<Integer> menuOptName;
        List<Integer> menuOptImg;
        menuOptName = new ArrayList<>();
        menuOptName.add(R.string.myhkt_settings);
        menuOptName.add(R.string.myhkt_about);
        menuOptName.add(R.string.myhkt_menu_tnc);
        menuOptName.add(R.string.myhkt_gen_conditions);
        menuOptName.add(R.string.myhkt_menu_privacy_statement);

        menuOptImg = new ArrayList<>();
        menuOptImg.add(R.drawable.settting);
        menuOptImg.add(R.drawable.aboutmyhkt);
        menuOptImg.add(R.drawable.ios7_tnc);
        menuOptImg.add(R.drawable.ios7_tnc);
        menuOptImg.add(R.drawable.ios7_tnc);

        //Add the Inbox menu item
        if (ClnEnv.isLoggedIn()) {
            menuOptName.add(0, R.string.title_inbox);
            menuOptImg.add(0, R.drawable.app_mymessage);

            menuOptName.add(6, R.string.SHLF_LOGOUT);
            menuOptImg.add(6, R.drawable.ios7_login);
        } else {
            menuOptName.add(0, R.string.myhkt_LGIF_LOGIN);
            menuOptImg.add(0, R.drawable.ios7_login);
        }

        adapter = new SlideMenuAdapter(getActivity());
        for (int i = 0; i < menuOptName.size(); i++) {
            adapter.add(new MenuOptionItem(menuOptName.get(i), menuOptImg.get(i)));
        }
        setListAdapter(adapter);
    }

    // Show Terms and Conditions Dialog (TNC)
    protected final void displayTNCSDialog() {
        String message = getResources().getString(R.string.term_terms);
        String title = getResources().getString(R.string.term_tnc);
        DialogHelper.createTitleDialog(requireActivity(), title, message, getResources().getString(R.string.btn_ok));
    }

    private static class MenuOptionItem {
        public int tag;
        public int iconRes;

        public MenuOptionItem(int tag, int iconRes) {
            this.tag = tag;
            this.iconRes = iconRes;
        }
    }

    //Custom listview adapter
    public static class SlideMenuAdapter extends ArrayAdapter<MenuOptionItem> {
        public SlideMenuAdapter(Context context) {
            super(context, 0);
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            try {
                ViewHolder holder;
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.menu_list_row, null);

                    holder.menuListRowIcon = convertView.findViewById(R.id.menu_list_row_icon);
                    holder.menuListRowLitle = convertView.findViewById(R.id.menu_list_row_title);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }

                if (position == 6 && ClnEnv.isLoggedIn()) {
                    holder.menuListRowLitle.setText(Utils.getString(getContext(), R.string.SHLF_LOGOUT));
                    holder.menuListRowIcon.setImageResource(R.drawable.ios7_logout);
                } else {
                    holder.menuListRowIcon.setImageResource(Objects.requireNonNull(getItem(position)).iconRes);
                    holder.menuListRowLitle.setText(Utils.getString(getContext(), Objects.requireNonNull(getItem(position)).tag));
                }
                ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);
                holder.menuListRowIcon.setColorFilter(filter);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        public MenuOptionItem getMenuOption(int position) {
            return getItem(position);
        }
    }

    public static class ViewHolder {
        ImageView menuListRowIcon;
        TextView menuListRowLitle;
    }

    private boolean isChromeInstalledAndEnabled() {
        PackageManager pm = requireContext().getPackageManager();
        try {
            pm.getPackageInfo("com.android.chrome", PackageManager.GET_ACTIVITIES);
            ApplicationInfo ai = requireContext().getPackageManager().getApplicationInfo("com.android.chrome", 0);
            return ai.enabled;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void urlLink(String url) {
        if (!TextUtils.isEmpty(url)) {
            if (isChromeInstalledAndEnabled()) {
                Utils.openUrlOnCustomTab(getContext(), url, getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.menu_bg_premier : R.color.hkt_slidemenu_blue));
            } else {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intentBrowser);
            }
        }
    }

    private void writeResponseBodyToDisk(ResponseBody body, String fileName) {
        String filePath;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            filePath = requireActivity().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath();
        } else {
            filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        }

        // Check permission storage
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (!RuntimePermissionUtil.isWriteExternalStoragePermissionGranted(getActivity())) {
                RuntimePermissionUtil.requestFileStoragePermission(this);
            }
        }

        File target = new File(filePath + File.separator + fileName);
        Log.d(TAG, "target.getPath(): " + target.getPath());
        try {
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                if (target.exists()) {
                    if (target.delete()) {
                        Log.d(TAG, "target.delete()>>>: TRUE");
                    } else {
                        Log.d(TAG, "target.delete()>>>: FALSE");
                    }
                }

                Log.d(TAG, "Write to disk");
                byte[] fileReader = new byte[4096];
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(target);

                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                }
                outputStream.flush();

                if (target.exists()) {
                    Uri uri = FileProvider.getUriForFile(requireContext(), "com.pccw.myhkt.provider", target);
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setDataAndType(uri, "application/pdf");
                    requireActivity().startActivity(Intent.createChooser(intent, "Choose PDF File Viewer"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
