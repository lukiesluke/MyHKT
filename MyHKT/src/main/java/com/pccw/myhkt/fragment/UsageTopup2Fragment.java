package com.pccw.myhkt.fragment;

import static com.pccw.myhkt.util.Constant.KEY_NAME_LOB_TYPE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3BoostUpOffer1DTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.g3entity.G3UsageQuotaDTO;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.TwoBtnCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageListener;
import com.pccw.myhkt.lib.ui.AAQuery;

/************************************************************************
 * File : UsageTopup2Fragment.java
 * Desc : Top Up confirm page
 * Name : UsageTopup2Fragment
 * by 	: Andy Wong
 * Date : 26/1/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 26/01/2016 Andy Wong 		-First draft
 * 26/01/2016 Derek Tsui		-first draft
 * 
 *************************************************************************/

public class UsageTopup2Fragment extends BaseServiceFragment{
	private UsageTopup2Fragment me;
	private View 			myView;
	private LinearLayout 	frame;

	private AAQuery 	aq;
	private List<Cell> 	cellList;
	private int 		deviceWidth ;
	private int 		extralinespace;

	private OnUsageListener 	callback_usage;
	private G3BoostUpOffer1DTO 	g3BoostUpOfferDTO;
	private PlanMobCra 	  		planMobCra;
	private AddOnCra				addOnCra;
	private Boolean 			isMob;
	private CellViewAdapter cellViewAdapter;
	private String lobString = "";

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		try {
			callback_usage = (OnUsageListener)getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString() + " must implement OnUsageListener");
		}		
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_usagetopup, container, false);
		myView = fragmentLayout;
		lobString = callback_main.getAcctAgent().getSubnRec().lob;
		Log.d("lwg", "lobType: " + lobString);
		initData();
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume() {
		super.onResume();
		//Screen Tracker
		if (SubnRec.WLOB_XCSP.equals(lobString) || SubnRec.LOB_CSP.equals(lobString)) {
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_CSPTOPUP2, false);
		} else {
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBTOPUP3, false);
		}
	}

	protected void initData() {
		super.initData();
		g3BoostUpOfferDTO = callback_usage.getSelectedG3BoostUpOfferDTO();
		planMobCra = callback_usage.getPlanMobCra();
		isMob = (SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.LOB_CSP.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.WLOB_XCSP.equals(callback_main.getAcctAgent().getSubnRec().lob));

		cellViewAdapter = new CellViewAdapter(me.requireActivity());
	}

	protected void initUI() {
		aq = new AAQuery(myView);			
		cellList = new ArrayList<Cell>();

		aq.id(R.id.fragment_usagetopup_sv).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagetopup_sv, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagetopup_frame).getView();

		if (isMob) {
			initMobUI();
		} else {
			init1010UI();
		}
	}

	protected void initMobUI() {

		//Title Topup Order Detail
		int topupImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
		IconTextCell titleCell = new IconTextCell(topupImg, getResString(R.string.boost_up_order));
		titleCell.setLeftPadding(basePadding);
		titleCell.setRightPadding(basePadding);
		cellList.add(titleCell);

		String selectedTitle;
		if (callback_usage.getSelectedServiceItem().getRegion().equalsIgnoreCase("ROAMING") && "D".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getQuotaTopupInfo().getQuotaValidityPeriod())){
			String titleLevel2 = isZh ? callback_usage.getSelectedServiceItem().getTitleLevel2Chi(): callback_usage.getSelectedServiceItem().getTitleLevel2Eng();
			selectedTitle = String.format("%s(%s)", getResString(R.string.boost_up_info_selected), titleLevel2);
		} else {
			selectedTitle = getResString(R.string.boost_up_info_selected);
		}
		//You have selected
		BigTextCell subHeaderCell = new BigTextCell(selectedTitle,null);
		subHeaderCell.setTitleSizeDelta(0);
		subHeaderCell.setMargin(0);
		subHeaderCell.setLeftMargin(basePadding);
		subHeaderCell.setRightMargin(basePadding);
		subHeaderCell.setArrowShown(true);
		cellList.add(subHeaderCell);

		String plan = isZh ? g3BoostUpOfferDTO.getChiDescription() : g3BoostUpOfferDTO.getEngDescription();
		SmallTextCell smallTextCell3 = new SmallTextCell(plan, null);
		smallTextCell3.setTitleColorId(R.color.hkt_textcolor);
		smallTextCell3.setLeftPadding(basePadding);
		smallTextCell3.setRightPadding(basePadding);
		smallTextCell3.setBgColorId(R.color.cell_bg_grey);
		smallTextCell3.setTitleSizeDelta(-2);
		cellList.add(smallTextCell3);
		
		//Expire Date
//		String expDate = Utils.toDateString(planMobCra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate(), "yyyyMMddHHmmss", "dd/MM/yyyy HH:mm");
		String expDate = Utils.toDateString(planMobCra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate(), "yyyyMMdd", "dd/MM/yyyy HH:mm");
		SmallTextCell expireDateCell = new SmallTextCell(String.format(getResString(R.string.usage_boost_up_remark), getString(R.string.usage_boost_up_next_bill), expDate + " ", ""), null);
		expireDateCell.setLeftPadding(basePadding);
		expireDateCell.setRightPadding(basePadding);
		expireDateCell.setTitleSizeDelta(-2);
		cellList.add(expireDateCell);

		//Hong kong time for roaming only
		if ("ROAMING".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getRegion()) || "GLOBAL".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getRegion())) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
				Date d = sdf.parse(planMobCra.getServerTS());
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
				String date = df.format(d);

				SmallTextCell smallTextCell2a = new SmallTextCell(String.format(Utils.getString(me.getActivity(), R.string.usage_boost_up_curr_hk_time), date), null);
				smallTextCell2a.setTitleSizeDelta(-2);
				smallTextCell2a.setLeftPadding(basePadding);
				smallTextCell2a.setRightPadding(basePadding);
				cellList.add(smallTextCell2a);
			} catch (Exception e) {
				e.printStackTrace();
			}
			//			viewholder.usagedetailmob_monPackageTxt.setText(Utils.getString(me.getActivity(), R.string.usage_basic_entitle));
		}

		//Remark2
		//hide for Asia plan
		if(!"GLOBAL".equalsIgnoreCase(callback_usage.getSelectedServiceItem().getRegion())) {
			SmallTextCell topupHisRmk2Cell = new SmallTextCell(getResString(R.string.myhkt_usage_remark2), null);
			topupHisRmk2Cell.setTitleSizeDelta(-2);
			topupHisRmk2Cell.setLeftPadding(basePadding);
			topupHisRmk2Cell.setRightPadding(basePadding);
			cellList.add(topupHisRmk2Cell);
		}

		String termPlan = getResString(R.string.myhkt_topup_terms); //Default Plan for CSL
		if (SubnRec.WLOB_XCSP.equals(lobString) || SubnRec.LOB_CSP.equals(lobString)) {
			termPlan = getResString(R.string.myhkt_topup_terms_CSP);
		} else if (SubnRec.LOB_IOI.equals(lobString) || SubnRec.LOB_101.equals(lobString) || SubnRec.WLOB_X101.equals(lobString)) {
			termPlan = getResString(R.string.myhkt_topup_terms_1O1O);
		}

		SmallTextCell smallTextCell4 = new SmallTextCell(termPlan, null);
		smallTextCell4.setTitleColorId(R.color.black);
		smallTextCell4.setLeftPadding(basePadding);
		smallTextCell4.setRightPadding(basePadding);
		smallTextCell4.setTitleSizeDelta(-2);
		cellList.add(smallTextCell4);

		SmallTextCell smallTextCell5 = new SmallTextCell(getResString(R.string.boost_confirm_remark), null);
		smallTextCell5.setTitleColorId(R.color.hkt_txtcolor_grey);
		smallTextCell5.setLeftPadding(basePadding);
		smallTextCell5.setRightPadding(basePadding);
		smallTextCell5.setTitleSizeDelta(-2);
		cellList.add(smallTextCell5);

		//Back , Continue
		OnClickListener onBackClick = new OnClickListener() {
			@Override
			public void onClick(View v) {
				callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP1);
				callback_usage.displayChildview(true);
			}
		};
		OnClickListener onContinueClick = new OnClickListener() {
			@Override
			public void onClick(View v) {
				PlanMobCra planMobCra = callback_usage.getPlanMobCra().copyMe();
				G3DisplayServiceItemDTO g3DisplayServiceItemDTO = callback_usage.getSelectedServiceItem();
				planMobCra.setITopupItem(g3DisplayServiceItemDTO);
				planMobCra.setIBstUpOff(g3BoostUpOfferDTO);
				APIsManager.doTopup(me, planMobCra);
			}
		};
		TwoBtnCell twoBtnCell = new TwoBtnCell(getResString(R.string.btn_back), getResString(R.string.btn_confirm), -1, -1, null);
		twoBtnCell.setTopMargin(basePadding);
		twoBtnCell.setTopPadding(basePadding);
		twoBtnCell.setLeftPadding(basePadding);
		twoBtnCell.setRightPadding(basePadding);
		twoBtnCell.setLeftClickListener(onBackClick);
		twoBtnCell.setRightClickListener(onContinueClick);
		cellList.add(twoBtnCell);

		cellViewAdapter.setView(lobString, frame, cellList);
	}

	protected void init1010UI() {

		//Title Topup Order Detail
		int topupImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
		IconTextCell titleCell = new IconTextCell(topupImg, getResString(R.string.boost_up_order));
		titleCell.setLeftPadding(basePadding);
		titleCell.setRightPadding(basePadding);
		cellList.add(titleCell);

		//You have selected
		BigTextCell subHeaderCell = new BigTextCell(getResString(R.string.boost_up_info_selected),null);
		subHeaderCell.setTitleSizeDelta(0);
		subHeaderCell.setMargin(0);
		subHeaderCell.setLeftMargin(basePadding);
		subHeaderCell.setRightMargin(basePadding);
		subHeaderCell.setArrowShown(true);

		cellList.add(subHeaderCell);

		String plan = isZh ? g3BoostUpOfferDTO.getChiDescription() : g3BoostUpOfferDTO.getEngDescription();
		SmallTextCell smallTextCell3 = new SmallTextCell(plan, null);
		smallTextCell3.setTitleColorId(R.color.hkt_textcolor);
		smallTextCell3.setLeftPadding(basePadding);
		smallTextCell3.setRightPadding(basePadding);
		smallTextCell3.setBgColorId(R.color.cell_bg_grey);
		smallTextCell3.setTitleSizeDelta(-2);
		cellList.add(smallTextCell3);
		
		//Expire Date
		G3UsageQuotaDTO otherDTO;
		if ("P".equalsIgnoreCase(planMobCra.getOMobUsage().getG3UsageQuotaDTO().getPrimaryFlag())) {
			otherDTO = planMobCra.getOMobUsage().getG3SummUsageQuotaDTO();
		} else {
			otherDTO = planMobCra.getOMobUsage().getG3UsageQuotaDTO();
		}

		String nextBillDate =  otherDTO.getNxtBilCutoffDt();
		if (nextBillDate != null) {
			try {
				SimpleDateFormat nextDf = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
				Date nextDate =  nextDf.parse(nextBillDate);	
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy 23:59", Locale.ENGLISH);
				nextBillDate = df.format(nextDate);
				SmallTextCell expireDateCell = new SmallTextCell(String.format(getResString(R.string.usage_boost_up_remark), getString(R.string.usage_boost_up_next_bill), nextBillDate," "), null);
				expireDateCell.setLeftPadding(basePadding);
				expireDateCell.setRightPadding(basePadding);
				expireDateCell.setTitleSizeDelta(-2);
				cellList.add(expireDateCell);
			} catch (java.text.ParseException e) {
				// Next bill date parse error
			}
		}

		//Remark2
		SmallTextCell topupHisRmk2Cell = new SmallTextCell(getResString(R.string.myhkt_usage_remark2), null);
		topupHisRmk2Cell.setTitleSizeDelta(-2);
		topupHisRmk2Cell.setLeftPadding(basePadding);
		topupHisRmk2Cell.setRightPadding(basePadding);
		cellList.add(topupHisRmk2Cell);

		SmallTextCell smallTextCell4 = new SmallTextCell(getResString(R.string.myhkt_topup_terms), null);
		smallTextCell4.setTitleColorId(R.color.black);
		smallTextCell4.setLeftPadding(basePadding);
		smallTextCell4.setRightPadding(basePadding);
		smallTextCell4.setTitleSizeDelta(-2);
		cellList.add(smallTextCell4);

		SmallTextCell smallTextCell5 = new SmallTextCell(getResString(R.string.boost_confirm_remark), null);
		smallTextCell5.setTitleColorId(R.color.hkt_txtcolor_grey);
		smallTextCell5.setLeftPadding(basePadding);
		smallTextCell5.setRightPadding(basePadding);
		smallTextCell5.setTitleSizeDelta(-2);
		cellList.add(smallTextCell5);

		//Back , Continue
		OnClickListener onBackClick = new OnClickListener(){
			@Override
			public void onClick(View v) {
				callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP1);
				callback_usage.displayChildview(true);		
			}			
		};
		OnClickListener onContinueClick = new OnClickListener(){
			@Override
			public void onClick(View v) {
				if (debug) Log.i(TAG, "Click");
				PlanMobCra planMobCra = callback_usage.getPlanMobCra().copyMe();
				G3DisplayServiceItemDTO  g3DisplayServiceItemDTO  = callback_usage.getSelectedServiceItem();
				planMobCra.setITopupItem(g3DisplayServiceItemDTO);
				planMobCra.setIBstUpOff(g3BoostUpOfferDTO);
				APIsManager.doTopup(me, planMobCra);		
			}			
		};
		TwoBtnCell twoBtnCell = new TwoBtnCell(getResString(R.string.btn_back) , getResString(R.string.btn_confirm), -1,-1, null);
		twoBtnCell.setTopMargin(basePadding);
		twoBtnCell.setTopPadding(basePadding);
		twoBtnCell.setLeftPadding(basePadding);
		twoBtnCell.setRightPadding(basePadding);
		twoBtnCell.setLeftClickListener(onBackClick);
		twoBtnCell.setRightClickListener(onContinueClick);
		cellList.add(twoBtnCell);

		cellViewAdapter.setView(frame, cellList);
	}

	public void onBackClick(View v) {
		//		callback_usage.backToUsageTopup1Frag();
		callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP1);
		callback_usage.displayChildview(true);
	}

	public void onContinueClick(View v) {
		//		callback_usage.goToUsageTopup3Frag();
		PlanMobCra planMobCra = callback_usage.getPlanMobCra().copyMe();
		G3DisplayServiceItemDTO  g3DisplayServiceItemDTO  = callback_usage.getSelectedServiceItem();
		planMobCra.setITopupItem(g3DisplayServiceItemDTO);
		planMobCra.setIBstUpOff(g3BoostUpOfferDTO);
		APIsManager.doTopup(me, planMobCra);
	}

	protected void cleanupUI(){

	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (debug) Log.i(TAG, "Succ");
		if (APIsManager.TPUP.equals(response.getActionTy())) {
			PlanMobCra pCra = (PlanMobCra)response.getCra();
			callback_usage.setPlanMobCra(pCra);
			//			callback_usage.goToUsageTopup3Frag();
			callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP3);
			callback_usage.displayChildview(false);
		} else if (APIsManager.AO_AUTH.equals(response.getActionTy())) {
			addOnCra = (AddOnCra)response.getCra();
			addOnCra.getOSubnRec().ivr_pwd = addOnCra.getISubnRec().ivr_pwd;
			
			callback_usage.getPlanMobCra().setISubnRec(addOnCra.getOSubnRec());
			callback_main.setSubscriptionRec(addOnCra.getOSubnRec());
			callback_main.getAcctAgent().setSubnRec(addOnCra.getOSubnRec());
			
			//recall
			PlanMobCra planMobCra = callback_usage.getPlanMobCra().copyMe();
			G3DisplayServiceItemDTO  g3DisplayServiceItemDTO  = callback_usage.getSelectedServiceItem();
			planMobCra.setITopupItem(g3DisplayServiceItemDTO);
			planMobCra.setIBstUpOff(g3BoostUpOfferDTO);
			APIsManager.doTopup(me, planMobCra);		
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		//TODO
		if (debug) Log.i(TAG, "Fail");
		if (debug) Log.i(TAG, response.getReply().getCode());
		if(response.getCra() != null && 
				!(response.getReply().equals(APIsManager.NULL_OBJECT_CRA) || response.getReply().equals(APIsManager.HTTPSTATUS_NOT_200) || response.getReply().equals(APIsManager.JSON_RETURN_NULL)  )
				&& response.getCra() instanceof PlanMobCra) {
			if (debug) Log.i(TAG, "Fail1");
			try {
			PlanMobCra pCra = (PlanMobCra)response.getCra();
			callback_usage.setPlanMobCra(pCra);
			} catch (ClassCastException e) {
				callback_usage.setPlanMobCra(null);
			}			
		} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
			recallDoAuth(me, callback_main.getAcctAgent().getSubnRec());
		} else {
			if (debug) Log.i(TAG, "Fail2");
			callback_usage.setPlanMobCra(null);
		}
		if (debug) Log.i(TAG, "Fail3");
		//			callback_usage.goToUsageTopup3Frag();
		callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP3);
		callback_usage.displayChildview(false);
	}

}
