package com.pccw.myhkt.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.MobUsage;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.g3entity.G3UsageQuotaDTO;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.CircleViewCell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.enums.HKTDataType;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageDataListener;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageListener;
import com.pccw.myhkt.lib.ui.AAQuery;
/************************************************************************
File       : UsageDataChild3Fragment.java
Desc       : 
Name       : UsageDataChild3Fragment
Created by : Andy Wong
Date       : 15/06/2016

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
15/06/2016 Andy Wong			- First draft
14/06/2017 Abdulmoiz Esmail     - Update showing of remaining data usage, use the KB or MB form depending on the size. 
 *************************************************************************/
public class UsageDataChild3Fragment extends BaseServiceFragment{
	private UsageDataChild3Fragment me;
	private View 		myView;
	private AAQuery 	aq;
	private List<Cell> 	cellList;
	private int 		colWidth;
	private int 		btnWidth;
	
	private Boolean		isMob = false;
	private Boolean 	isCSimPrim = false;
	private Boolean 	isCSimProgressShown = false;
	private Boolean 	isLocal = true;
	
	private PlanMobCra 		planMobCra;
	private MobUsage 		mobUsage;
	private LinearLayout 	frame;
	private G3UsageQuotaDTO g3UsageQuotaDTO;
	private CellViewAdapter cellViewAdapter;
	
	private OnUsageDataListener callback;
	private OnUsageListener 	callback_usage;
	
	List<G3DisplayServiceItemDTO> serviceList;
	
	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		try {
			callback = (OnUsageDataListener)getParentFragment();
			callback_usage = (OnUsageListener)getParentFragment().getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString() + " must implement OnUsageListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		isMob = (SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.WLOB_XCSP.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.LOB_CSP.equals(callback_main.getAcctAgent().getSubnRec().lob));
		isLocal = callback_usage.getIsLocal();
		if (callback.getPlanMobCra() != null && callback.getPlanMobCra().getOMobUsage()!= null) {
			G3UsageQuotaDTO g3OtherUsageDTO = callback.getPlanMobCra().getOMobUsage().getG3UsageQuotaDTO();
			if (g3OtherUsageDTO != null) {			
				isCSimPrim = "P".equalsIgnoreCase(g3OtherUsageDTO.getPrimaryFlag());
			}		
		}		
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_usagedata_child3, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}

	protected void initData() {
		super.initData();
		colWidth = (deviceWidth - basePadding *2 )/4;
		btnWidth = deviceWidth/2 - basePadding; 
		planMobCra = callback.getPlanMobCra();
		cellViewAdapter = new CellViewAdapter(me.getActivity());
	}

	protected void initUI() {
		aq = new AAQuery(myView);
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();
		
		serviceList = isLocal ? callback.getOtherLocalServiceList() : callback.getOtherRommaServiceList();
		cellList = new ArrayList<Cell>();
//		int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
//		IconTextCell titleCell = new IconTextCell(mobusageImg, getResString(isLocal ? R.string.widget_title_local : R.string.widget_title_roam));
//		cellList.add(titleCell);
		
		if (planMobCra !=null) {
			planMobCra = callback.getPlanMobCra();
			mobUsage = planMobCra.getOMobUsage();
			g3UsageQuotaDTO = mobUsage.getG3UsageQuotaDTO();
		}
		
		if (serviceList !=null && serviceList.size() > 0) {
			for (G3DisplayServiceItemDTO item : serviceList) {
				addCircleView(item);
			}			
		}
		cellViewAdapter.setView(frame, cellList);
	}
	
	private void addCircleView(final G3DisplayServiceItemDTO item) {
		//Title 
		String title = isZh ? item.getTitleLevel1Chi() : item.getTitleLevel1Eng();
		String subTitle = isZh ? item.getTitleLevel2Chi() : item.getTitleLevel2Eng();
		if (subTitle != null && !subTitle.equals("")) {
			if(callback_usage.getLob() != R.string.CONST_LOB_1010 && callback_usage.getLob() != R.string.CONST_LOB_O2F) { //Csim bypass level2 title
				title = title + " (" + subTitle + ")";
			} else if (isCSimPrim) {
				title = title + " (" + getString(R.string.csl_usage_total) + ")";
			}
		}
		int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
		IconTextCell titleCell = new IconTextCell(mobusageImg, title);
		titleCell.setLeftPadding(basePadding);
		titleCell.setRightPadding(basePadding);
		cellList.add(titleCell);
		
		//As of date 
		String asOfDate = isZh ? item.getAsOfDateChi() : item.getAsOfDateEng();
		if (asOfDate != null && !asOfDate.equals("")) {
			ArrowTextCell arrowTextCell = new ArrowTextCell(asOfDate, null, null, null);
			arrowTextCell.setRightPadding(basePadding);
			arrowTextCell.setLeftPadding(basePadding);
			arrowTextCell.setArrowShown(true);
			cellList.add(arrowTextCell);
		}
		
		//ProgressBar
		if ("Y".equalsIgnoreCase(item.getUsageBar().getShowBar())) {	
			String unit = ""; 
			
			//get usage remaining
			double remainingMB = 0;
			double entitlementMB = 0;
			String remaining = "---";
			String entitlement = "---";	
			Boolean isRemaining = false;
			
			remaining = Utils.formattedDataByKBStr(item.getUsageDescription().getRemainingInKB(), HKTDataType.HKTDataTypeRemaining, true);
			if(!TextUtils.isEmpty(remaining)) {
				isRemaining = true;
			}

			//			CircleViewCell circlecell = new CircleViewCell(isZh?  item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() ,"3.67GB", item.getUsageBar().getBarPercent());
			CircleViewCell circlecell;
			if (isRemaining) {
				circlecell = new CircleViewCell(isZh?  item.getUsageBar().getBarPercent() + "%" : item.getUsageBar().getBarPercent() + "%", remaining , item.getUsageBar().getBarPercent());
			} else {
				circlecell = new CircleViewCell(isZh?  item.getUsageBar().getBarPercent() + "%" : item.getUsageBar().getBarPercent() + "%", "", item.getUsageBar().getBarPercent());
			}
			
			circlecell.setLeftPadding(extralinespace*4);
			circlecell.setRightPadding(extralinespace*4);
			
			//set bar color
			if (item.getUsageBar().getBarPercent() < 10) {
				circlecell.setBarColor(R.color.cprogressbar_red);
				circlecell.setTitleColorId(R.color.cprogressbar_red);
			} else if (item.getUsageBar().getBarPercent() >= 10 && item.getUsageBar().getBarPercent() <= 20) {
				circlecell.setBarColor(R.color.cprogressbar_yellow);
				circlecell.setTitleColorId(R.color.cprogressbar_yellow);
			}
			
			cellList.add(circlecell);

			String remainingUsage = isZh? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng();
			if ("QUOTA-BASED".equals(item.getUsageType())) {
				remainingUsage = "";
			}
			if(!TextUtils.isEmpty(remainingUsage)) {
				SmallTextCell smallTextCell = new SmallTextCell(remainingUsage, "");
				smallTextCell.setTitleGravity(Gravity.CENTER);
				smallTextCell.setBgcolorId(R.color.white);
				cellList.add(smallTextCell);
			}
			
		} else {
			SmallTextCell smallTextCell = new SmallTextCell(isZh? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() , "");
			smallTextCell.setBgcolorId(R.color.cell_bg_grey);
			cellList.add(smallTextCell);
		}
		//Topup btn
		if ("Y".equalsIgnoreCase(item.getAllowTopup())) {
			// Only Show the Topup button after doneGetNextBillDate
			if (checkDoneGetNextBillDate()) {
				OnClickListener onClickListener = new OnClickListener(){
					@Override
					public void onClick(View v) {
						callback_usage.setSelectedServiceItem(item);		
						callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP1);
						callback_usage.displayChildview(false);
					}			
				};
				String btnName = getResString(R.string.usage_boost_up1);
				SingleBtnCell singleBtnCell = new SingleBtnCell(btnName, btnWidth, onClickListener);
				int topupImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
				singleBtnCell.setDraw(topupImg);
				cellList.add(singleBtnCell);	
			}
		}
	}
	
	private final boolean checkDoneGetNextBillDate() {
		return callback.getPlanMobCra().getOMobUsage().getG3AcctBomNextBillDateDTO() != null
				&& callback.getPlanMobCra().getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate() != null;
	}
	
	private void addPart(int icon, String title, String subTitle, String[]colname, String[]units, int[] colWidths, String[]rowTitle, String[][] rowData) {

		IconTextCell cellTitle = new IconTextCell(icon, title);
		cellTitle.setLeftPadding(basePadding);
		cellList.add(cellTitle);
		ArrowTextCell arrowTextCell = new ArrowTextCell(subTitle ,colname, units, colWidths);
		arrowTextCell.setNoteSizeDelta(-4);
		arrowTextCell.setContentSizeDelta(-4);
		arrowTextCell.setArrowShown(true);
		cellList.add(arrowTextCell);
		for (int i=0; i < rowTitle.length ;i++){
			ArrowTextCell cell = new ArrowTextCell(rowTitle[i], rowData[i], null, colWidths,i %2 ==0? R.color.cell_bg_grey: R.color.white, R.color.hkt_textcolor);	
			cell.setNoteSizeDelta(-4);		
			cell.setContentSizeDelta(-4);
			cell.setTitleWrapContent(false);
			cellList.add(cell);
		}
	}
 
	//Remark will be add at bottom
	public void addRemark(){		
		Cell line = new Cell(Cell.LINE);
		line.setBotMargin(extralinespace/2);
		line.setTopMargin(extralinespace/2);
		cellList.add(line);
		SmallTextCell remarkCell = new SmallTextCell(getResString(R.string.usage_reference_remark), "");
		remarkCell.setTitleSizeDelta(-5);
		cellList.add(remarkCell);
	}

	protected void cleanupUI(){

	}
	
	public void refreshData(){
		super.refreshData();
		initUI();
	}

	private void onTrackerSCP() {
		if (SubnRec.WLOB_XCSP.equals(callback_main.getAcctAgent().getSubnRec().lob)
				|| SubnRec.LOB_CSP.equals(callback_main.getAcctAgent().getSubnRec().lob)) {
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_CSPDATAVAS, false);
		}
	}

	public void onResume() {
		super.onResume();
		onTrackerSCP();
		if (!Utils.getString(requireActivity(), R.string.CONST_SCRN_MOBDATAVAS).equals(callback_main.getTrackerId())) {
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBDATAVAS, false);
		}
	}

	public void refresh() {
		super.refresh();
		onTrackerSCP();
		if (!Utils.getString(getActivity(), R.string.CONST_SCRN_MOBDATAVAS).equals(callback_main.getTrackerId())) {
			callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_MOBDATAVAS));
			// Screen Tracker
			FAWrapper.getInstance().sendFAScreen(getActivity(),
					R.string.CONST_SCRN_MOBDATAVAS, false);
			if (debug)
				Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBDATAVAS));
		}
		initUI();
	}

	private String convertNum(String num) {
		if (num !=null) num = num.trim();
		if (num != null && num.length() != 0){
			if (num.equalsIgnoreCase("UNLIMIT")) {
				return getResString(R.string.usage_unlimited);
			} else {
				return Utils.formatNumber(Utils.truncateStringDot(num)); 
			}
		} else {
			return "-";
		}
	}
	public static double parseDouble(String str) {
		str = str.trim();
		str = str.replace("$", "");
		str = str.replace(",", "");

		try {
			return Double.parseDouble(str);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub

	}
	
	

}
