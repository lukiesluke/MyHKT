package com.pccw.myhkt.fragment;


import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.model.LnttAgent;

/**
 * A simple {@link Fragment} subclass.
 */
public class LnttRebootModemFragment extends BaseServiceFragment implements SRCreationFragment.OnLnttListener, APIsManager.OnAPIsListener {



    private FragmentTransaction ft;

    public LnttRebootModemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentLayout = inflater.inflate(R.layout.fragment_myproflogin, container, false);
        initData();
        return fragmentLayout;
    }

    @Override
    public void onResume() {
        super.onResume();

        ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_myproflogin_fragment, new LnttRebootModemIntroFragment()).commit();
    }

    @Override
    protected void initData() {
        super.initData();

        ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_myproflogin_fragment, new LnttRebootModemIntroFragment()).commit();
    }

    public void restartUI() {
        ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_myproflogin_fragment, new LnttRebootModemIntroFragment()).commit();
    }

    public void callRebootModemAPI() {

        ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_myproflogin_fragment, new LnttRebootModemIntroFragment()).commit();

        rebootModemAPI();
    }

    private void rebootModemAPI() {
        //TODO:lnttAgent from LnttRsFragment before auto switching to this page
        LnttAgent lnttAgent = callback_main.getLnttAgent();

        LnttCra lnttCra = new LnttCra();
        lnttCra.setISubnRec(callback_main.getSubnRec());
        if(lnttAgent != null) {
            //required if it is called via line test page
            lnttCra.setILtrsRec(lnttAgent.getLnttCra().getOLtrsRec());
        }

        APIsManager.doRebootModem(this, lnttCra);
    }

    @Override
    public void setActiveChildview(int index) {

    }

    @Override
    public int getActiveChildview() {
        return 0;
    }

    @Override
    public void displayChildview(int type) {

    }

    @Override
    public void onRebootModemShowLoadingPage() {
        ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_myproflogin_fragment, new LnttRebootModemLoadingFragment()).commit();
    }

    @Override
    public void onCallRebootApi() {
        rebootModemAPI();
    }

    @Override
    public void onSuccess(APIsResponse response) {
        if (APIsManager.RBMD.equals(response.getActionTy())) {
            onRebootModemShowLoadingPage();
        }
    }

    @Override
    public void onFail(APIsResponse response) {
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        }
        else{
            DialogHelper.createSimpleDialog(getActivity(),InterpretRCManager.interpretRC_LttLtsMdu(getActivity(), response.getReply().getCode()));
        }
    }
}
