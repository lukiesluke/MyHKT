package com.pccw.myhkt.dialogs;

import android.app.Activity;
import android.app.Dialog;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.InboxListActivity;
import com.pccw.myhkt.adapter.LOBAdapter;
import com.pccw.myhkt.model.ImageHolder;

import java.util.ArrayList;

public class LOBSelectionDialogCompat extends Dialog implements LOBAdapter.ItemClickListener {

    private LOBAdapter adapter;
    private Activity activity;

    public LOBSelectionDialogCompat(Activity activity, boolean cancelable,
                                    @Nullable OnCancelListener cancelListener,
                                    ArrayList<ImageHolder> imageHolderArrayList) {
        super(activity, cancelable, cancelListener);
        setTitle(R.string.inbox_enquiry_live_chat);
        this.activity = activity;
        this.setContentView(R.layout.dialog_lobselectioncompat);
        RecyclerView recyclerView = findViewById(R.id.rv_images);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this.activity);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setMaxLine(2);
        layoutManager.setFlexWrap(FlexWrap.WRAP);
        layoutManager.setJustifyContent(JustifyContent.CENTER);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new LOBAdapter(this.activity, imageHolderArrayList);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        setUpMyCancelButton(R.id.tv_cancel);
    }

    private void setUpMyCancelButton(int id) {
        TextView tvCancel = findViewById(id);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LOBSelectionDialogCompat.this.dismiss();
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if(activity instanceof InboxListActivity)
            ((InboxListActivity) activity).openLiveChat(
                    determineLOBService(adapter.getItem(position).getText()));
        this.dismiss();
    }

    private String determineLOBService(String lob) {
        String result = "";
        switch(lob) {
            case SubnRec.LOB_PCD :
                result = activity.getResources().getString(R.string.MODULE_PCD_MYMSG); break;
            case SubnRec.LOB_LTS :
                result = activity.getResources().getString(R.string.MODULE_LTS_MYMSG); break;
            case SubnRec.LOB_TV :
                result = activity.getResources().getString(R.string.MODULE_TV_MYMSG); break;
            case SubnRec.WLOB_X101 :
                result = activity.getResources().getString(R.string.MODULE_101_MYMSG); break;
            case SubnRec.WLOB_CSL :
                result = activity.getResources().getString(R.string.MODULE_CSL_MYMSG); break;
        }
        return result;
    }
}
