package com.pccw.myhkt.dialogs.idd;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.pccw.myhkt.R;
import com.pccw.myhkt.model.Destination;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchListDialog extends Dialog implements ItemRecyclerViewAdapter.DestinationAdapterListener {

    private LinearLayoutManager recyclerLayoutManager;
    @BindView(R.id.rv_items)
    RecyclerView rvItems;
    @BindView(R.id.tv_negative)
    TextView tvNegative;
    @BindView(R.id.tv_positive)
    TextView tvPositive;
    @BindView(R.id.et_search)
    EditText svSearch;

    private Destination selectedDestination;
    private Context context;
    private ArrayList<Destination> destinationList;

    public SearchListDialog(@NonNull Context context,
                            String selected,
                            String selectedExt,
                            @NonNull ArrayList<Destination> destinationList,
                            @NonNull OnCancelListener cancelTrigger,
                            @NonNull OnDismissListener dismissTrigger,
                            @NonNull View.OnClickListener negativeTrigger,
                            @NonNull View.OnClickListener positiveTrigger,
                            boolean isZh) {
        super(context);
        this.context = context;
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.dialog_search_list);
        ButterKnife.bind(this);
        recyclerLayoutManager = new WrapContentLinearLayoutManager(this.getContext());
        rvItems.setLayoutManager(recyclerLayoutManager);
        Destination d = new Destination();
        d.engDestName = getContext().getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY);
        d.chiDestName = getContext().getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY);
        if(isZh ? !(destinationList.get(0).engDestName == getContext().getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY)) :
                !(destinationList.get(0).chiDestName == getContext().getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY)))
        destinationList.add(0, d);
        this.destinationList = destinationList;
        ItemRecyclerViewAdapter rvAdapter = new
                ItemRecyclerViewAdapter(this.destinationList, this.getContext(), this, isZh, selectedDestination);
        rvItems.setAdapter(rvAdapter);
        this.setOnCancelListener(cancelTrigger);
        this.setOnDismissListener(dismissTrigger);
        tvNegative.setOnClickListener(negativeTrigger);
        tvPositive.setOnClickListener(positiveTrigger);
        svSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                rvAdapter.getFilter().filter(s.toString().trim());
            }
        });
        svSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                InputMethodManager inputManager = (InputMethodManager)
                        context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.toggleSoftInput(0, 0);
                rvAdapter.getFilter().filter(svSearch.getText().toString());
                return true;
            }
            return false;
        });
        expandDialog();
        rvAdapter.initSelection(selected, selectedExt.isEmpty() ? null : selectedExt);
    }

    private void shrinkDialog() {
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.45);
        SearchListDialog.this.getWindow().setLayout(width, height);
    }

    private void expandDialog() {
        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.90);
        SearchListDialog.this.getWindow().setLayout(width, height);
    }

    @Override
    public void onNotFound() {
        Destination d = new Destination();
        d.engDestName = getContext().getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY);
        d.chiDestName = getContext().getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY);
        selectedDestination = d;
        Log.d("Not Found", "");
    }

    @Override
    public void onScroll(int pos) {
//        new Handler().postDelayed(() -> rvItems.scrollToPosition(pos), 500);
        recyclerLayoutManager.scrollToPositionWithOffset(pos, 0);
        Log.d("Scrolled", Integer.toString(pos));
    }

    @Override
    public void onItemSelected(int pos, Destination destination) {
        new Handler().postDelayed(() -> rvItems.scrollToPosition(pos), 50);
//        recyclerLayoutManager.scrollToPositionWithOffset(pos, 0);
        selectedDestination = destination;
        if(selectedDestination != null)
            Log.d("Selected", destination.engDestName +
                    (destination.engDestNameExt == null ? "" : " - " + destination.engDestNameExt));
    }

    @Override
    public void onBackPressed() {
        if(svSearch.hasFocus()){
            svSearch.clearFocus();
            return;
        }
        super.onBackPressed();
    }

//    @Override
//    public void dismiss() {
//        this.destinationList.remove(0);
//        super.dismiss();
//    }
//
//    @Override
//    public void cancel() {
//        this.destinationList.remove(0);
//        super.cancel();
//    }

    public Destination getSelectedDestination() {
        return selectedDestination;
    }
}

class WrapContentLinearLayoutManager extends LinearLayoutManager {

    public WrapContentLinearLayoutManager(Context context) {
        super(context);
    }

    //... constructor
    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            super.onLayoutChildren(recycler, state);
        } catch (IndexOutOfBoundsException e) {
            Log.e("TAG", "meet a IOOBE in RecyclerView");
        }
    }
}