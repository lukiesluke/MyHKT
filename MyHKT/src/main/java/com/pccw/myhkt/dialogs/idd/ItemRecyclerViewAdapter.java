package com.pccw.myhkt.dialogs.idd;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RadioButton;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.model.Destination;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemRecyclerViewAdapter extends
        RecyclerView.Adapter<ItemRecyclerViewAdapter.ViewHolder> implements Filterable {

    private ArrayList<Destination> destinationList;
    private ArrayList<Destination> destinationListFiltered;
    private Context context;
    private DestinationAdapterListener listener;
    private boolean isZh;

    private int lastSelectedPosition;
    private Destination lastSelectedItem;
    private boolean selectedCountryNotInFiltered = false;

    public ItemRecyclerViewAdapter(ArrayList<Destination> destinationz, Context ctx,
                                   DestinationAdapterListener listener, boolean isZh,
                                   Destination destination) {
        this.destinationList = destinationz;
        this.destinationListFiltered = destinationz;
        this.lastSelectedItem = destination;
        this.context = ctx;
        this.listener = listener;
        this.isZh = isZh;
    }

    @Override
    public ItemRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        ItemRecyclerViewAdapter.ViewHolder viewHolder =
                new ItemRecyclerViewAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ItemRecyclerViewAdapter.ViewHolder holder,
                                 int position) {
        Destination destination = destinationListFiltered.get(position);
        holder.selectionState.setText(isZh ?
                destination.chiDestName + (destination.chiDestNameExt == null ? "" :
                        destination.chiDestNameExt.isEmpty() ? "" : " - " +
                        destination.chiDestNameExt) : destination.engDestName +
                (destination.engDestNameExt == null ? "" : destination.engDestNameExt.isEmpty() ? ""
                        : " - " + destination.engDestNameExt));

        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections

        if ((lastSelectedItem.engDestName.equals(Utils.getString(context,
                R.string.MYHKT_IDDRATE_SEL_COUNTRY)) ||
                selectedCountryNotInFiltered) && position == 0) {
            holder.selectionState.setChecked(true);
            listener.onNotFound();
        } else if (lastSelectedItem.engDestName.equals(Utils.getString(context,
                R.string.MYHKT_IDDRATE_SEL_COUNTRY)) || position == 0) {
            holder.selectionState.setChecked(false);
        } else {
            boolean isChecked = isZh ? chineseCheck(destination) : englishCheck(destination);
            holder.selectionState.setChecked(isChecked);
        }

    }

    private boolean chineseCheck(Destination destination) {
        Log.d("countryDebug", "destination.chiDestName:" + destination.chiDestName != null ? destination.chiDestName : "null");
        return destination.chiDestName.equals(lastSelectedItem.chiDestName) &&
                (destination.chiDestNameExt == null || lastSelectedItem.chiDestNameExt == null ? true :
                        destination.chiDestNameExt.equals(lastSelectedItem.chiDestNameExt));
    }

    private boolean englishCheck(Destination destination) {
        Log.d("countryDebug", "destination.engDestName:" + destination.engDestName != null ? destination.engDestName : "null");
//        Log.d("countryDebug", "destination.engDestNameExt:" + (destination.engDestNameExt != null ? destination.engDestNameExt : "null"));
//        Log.d("countryDebug", "lastSelectedItem.engDestNameExt:" + lastSelectedItem.engDestNameExt != null ? lastSelectedItem.engDestNameExt : "null");
//        Log.d("countryDebug", "lastSelectedItem.engDestName:" + lastSelectedItem.engDestName != null ? lastSelectedItem.engDestName : "null");
        return destination.engDestName.equals(lastSelectedItem.engDestName) &&
                (destination.engDestNameExt == null || lastSelectedItem.engDestNameExt == null ? true :
                destination.engDestNameExt.equals(lastSelectedItem.engDestNameExt));
    }

    @Override
    public int getItemCount() {
        return destinationListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    destinationListFiltered = destinationList;
                } else {
                    ArrayList<Destination> filteredList = new ArrayList<>();
                    for (Destination row : destinationList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match

                        if (checkStringContain(row.engDestName, charString) ||
                                checkStringContain(row.chiDestName, charString) ||
                                (row.engDestNameExt != null || row.chiDestNameExt != null) &&
                                        ((checkStringContain(row.engDestNameExt, charString)) ||
                                                checkStringContain(row.chiDestNameExt, charString)))
                            filteredList.add(row);
                    }
                    destinationListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = destinationListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                destinationListFiltered = (ArrayList<Destination>) filterResults.values;
                Destination d = new Destination();
                d.engDestName = context.getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY);
                d.chiDestName = context.getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY);
                if(destinationListFiltered.isEmpty()) {

                }
                else if(ClnEnv.getAppLocale(context).startsWith("zh") ?
                            destinationListFiltered.get(0).engDestName
                                    == context.getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY)
                            : destinationListFiltered.get(0).chiDestName
                            == context.getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY)) {
                    destinationListFiltered.remove(0);
                }
                destinationListFiltered.add(0, d);
                // refresh the list with filtered data

                if (!destinationListFiltered.contains(lastSelectedItem)) {
                    selectedCountryNotInFiltered = true;
                } else {
                    selectedCountryNotInFiltered = false;
                }
                removeNullAtFirst();
                smartSearch(lastSelectedItem);
                listener.onItemSelected(lastSelectedPosition, lastSelectedItem);
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_radio) RadioButton selectionState;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            selectionState.setOnClickListener(v -> {
                selectedCountryNotInFiltered = false;
                lastSelectedPosition = getAdapterPosition();
                lastSelectedItem = destinationListFiltered.get(getAdapterPosition());
                listener.onItemSelected(lastSelectedPosition, lastSelectedItem);
                notifyDataSetChanged();
            });
        }
    }

    public interface DestinationAdapterListener {
        void onScroll(int pos);
        void onNotFound();
        void onItemSelected(int pos, Destination destination);
    }

    private void smartSearch(Destination destination) {
//        lastSelectedItem = null;
        lastSelectedPosition = -1;
        Log.v("smartSearch", "Executed");
        if(destination != null) {
            for(int i = 0; i < destinationListFiltered.size(); i++) {
                Log.v("smartSearch", "searching " + i);
                Destination selected = destinationListFiltered.get(i);
                if(ClnEnv.getAppLocale(context).startsWith("zh")) {
                    if (selected.chiDestName.equals(destination.chiDestName)) {
                        if (selected.chiDestNameExt != null && !selected.chiDestNameExt.isEmpty()) {
                            if (selected.chiDestNameExt.equals(destination.chiDestNameExt)) {
                                Log.v("smartSearch", "found " + i);
                                lastSelectedItem = selected;
                                lastSelectedPosition = i;
//                                listener.onItemSelected(lastSelectedPosition, lastSelectedItem);
                                listener.onScroll(lastSelectedPosition);
                                return;
                            }
                        } else {
                            Log.v("smartSearch", "found " + i);
                            lastSelectedItem = selected;
                            lastSelectedPosition = i;
//                            listener.onItemSelected(lastSelectedPosition, lastSelectedItem);
                            listener.onScroll(lastSelectedPosition);
                            return;
                        }
                    }
                }
                else {
                    if (selected.engDestName.equals(destination.engDestName)) {
                        if (selected.engDestNameExt != null && !selected.engDestNameExt.isEmpty()) {
                            if (selected.engDestNameExt.equals(destination.engDestNameExt)) {
                                Log.v("smartSearch", "found " + i);
                                Log.v("smartSearch", "indexOf " + destinationListFiltered.indexOf(lastSelectedItem));
                                lastSelectedItem = selected;
                                lastSelectedPosition = i;
//                                listener.onItemSelected(lastSelectedPosition, lastSelectedItem);
                                listener.onScroll(lastSelectedPosition);
                                return;
                            }
                        } else {
                            Log.v("smartSearch", "found " + i);
                            Log.v("smartSearch", "indexOf " + destinationListFiltered.indexOf(lastSelectedItem));
                            lastSelectedItem = selected;
                            lastSelectedPosition = i;
//                            listener.onItemSelected(lastSelectedPosition, lastSelectedItem);
                            listener.onScroll(lastSelectedPosition);
                            return;
                        }
                    }
                }
            }
        }
        else {
            lastSelectedItem = null;
            lastSelectedPosition = -1;
//            listener.onItemSelected(lastSelectedPosition, lastSelectedItem);
            listener.onScroll(lastSelectedPosition);
        }
    }

    public void initSelection(String selected, String selectedExt) {
        Destination dest = new Destination();
        if(ClnEnv.getAppLocale(context).startsWith("zh")) {
            dest.chiDestName = selected;
            dest.chiDestNameExt = selectedExt;
        }
        else {
            dest.engDestName = selected;
            dest.engDestNameExt = selectedExt;
        }
        removeNullAtFirst();
        smartSearch(dest);
    }

    private boolean checkStringContain(String target, String query) {
        return target.toLowerCase().contains(
                query.toLowerCase());
    }

    public void removeNullAtFirst() {
        if(ClnEnv.getAppLocale(context).startsWith("zh") ?
                destinationListFiltered.get(0).engDestName == null
                : destinationListFiltered.get(0).chiDestName == null) {
            destinationListFiltered.remove(0);
        }
    }
}