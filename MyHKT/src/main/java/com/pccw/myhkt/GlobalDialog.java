package com.pccw.myhkt;

/************************************************************************
 File       : GlobalDialog.java
 Desc       : For everywhere we need to show dialog
 Name       : GlobalDialog
 Created by : Vincent Fung
 Date       : 13/12/2012

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 05/02/2014 Vincent Fung		- First draft
 18/02/2014 Vincent Fung		- Added onNewIntent to handle rebuild Dialog problem
 26/03/2014 Derek Tsui			- Updated createDialog switch for notification message behavior
 *************************************************************************/

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.pccw.myhkt.activity.ClearActivity;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.activity.MainMenuActivity;
import com.pccw.myhkt.activity.ServiceListActivity;
import com.pccw.myhkt.util.Constant;

public class GlobalDialog extends Activity implements DialogInterface.OnCancelListener {
	private boolean debug = false;
	private static GlobalDialog	me;
	private AlertDialog	alertDialog	= null;
	private AlertDialog.Builder	builder;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		debug = getResources().getBoolean(R.bool.DEBUG);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		me = this;
		if (debug) Log.i("test", "dialog create");
		createDialog(me.getIntent().getExtras().getInt("DIALOGTYPE"));
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (debug) Log.i("test", "dialog create1");
		me = this;
		createDialog(intent.getExtras().getInt("DIALOGTYPE"));
	}

	protected void createDialog(int id) {
		if (alertDialog != null) {
			alertDialog.dismiss();
		}

		builder = new AlertDialog.Builder(me);
		switch (id) {
			case R.string.CONST_DIALOG_LNTT_RESULT:
				builder.setMessage(Utils.getString(me, R.string.MYHKT_LT_MSG_COMPLETED));
				builder.setCancelable(false);
				builder.setPositiveButton(Utils.getString(me, R.string.MYHKT_BTN_LATER), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						finish();
					}
				});
				builder.setNegativeButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), true);
						dialog.dismiss();
						Utils.ISMYLIENTEST = true;
						Intent parentIntent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
						parentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(parentIntent);
						me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
						finish();
					}
				});
				alertDialog = builder.create();
				alertDialog.show();
				break;
			case R.string.CONST_DIALOG_MSG_NEW_BILL:
				ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
				if (ClnEnv.getPushDataBill() != null && ClnEnv.isLoggedIn()) {
					if (ClnEnv.getPushDataBill().isAppActive() && (!"".equalsIgnoreCase(ClnEnv.getPushDataBill().getLoginId()) && ClnEnv.getPushDataBill().getLoginId().equalsIgnoreCase(ClnEnv.getSessionLoginID()))) {
						// if loginID of the session matches on the login ID of push notification
						builder.setMessage(getBaseContext().getString(R.string.ebill_notification_msg));
						builder.setCancelable(false);
						builder.setPositiveButton(Utils.getString(me, R.string.LTTF_CLOSE), new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
								ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);

//								Log.d("notifDEbug", "ISFROMSERVICEACTIVITY:" + getIntent().getBooleanExtra("ISFROMSERVICEACTIVITY", false));
//								if (!getIntent().getBooleanExtra("ISFROMSERVICEACTIVITY", false)) {
//									Intent parentIntent = new Intent(me, ServiceListActivity.class);
//									parentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//									startActivity(parentIntent);
//									me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
//								}

								finish();
							}
						});

						alertDialog = builder.create();
						alertDialog.show();
					} else if (!ClnEnv.getPushDataBill().isAppActive() || !ClnEnv.getPushDataBill().getLoginId().equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
						// if loginID of the session dont matches on the login ID of push notification
						finish();
					}
				} else {
					Log.i("checkBillMsgAndRedirect", "check billc");
					// if not logged in
					builder.setMessage(getBaseContext().getString(R.string.ebill_notification_msg));
					builder.setCancelable(false);
					builder.setPositiveButton(Utils.getString(me, R.string.LTTF_CLOSE), new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
//							Intent intent;
							dialog.dismiss();
//							intent = new Intent(getApplicationContext(), LoginActivity.class);
//							intent.putExtra("SHOWSERVICELISTAFTERLOGIN", true);
//
//							ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
//							ClnEnv.getPushDataBill().clear();
							//temp
//							startActivity(intent);
//							overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
							finish();
						}
					});

					alertDialog = builder.create();
					alertDialog.show();

					ClnEnv.getPushDataBill().clear();
					break;
				}
				break;
			case R.string.CONST_DIALOG_MSG_LATE_PAYMENT:
				if (ClnEnv.getPushDataBill() != null && ClnEnv.isLoggedIn()) {
					if ((!"".equalsIgnoreCase(ClnEnv.getPushDataBill().getLoginId()) && ClnEnv.getPushDataBill().getLoginId().equalsIgnoreCase(ClnEnv.getSessionLoginID()))) {

						builder.setMessage(ClnEnv.getPushDataBill().getMessage());
						builder.setCancelable(false);
						builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), true);
								dialog.dismiss();
								Intent parentIntent = new Intent(me, ServiceListActivity.class);
								parentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
								startActivity(parentIntent);
								me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
								finish();
							}
						});

						alertDialog = builder.create();
						alertDialog.show();
					} else {
						finish();
						ClnEnv.getPushDataBill().clear();
					}
				} else {
					finish();
					ClnEnv.getPushDataBill().clear();
					break;
				}
				break;
			case R.string.CONST_DIALOG_MSG_GEN:
				if (debug) Log.d("GLOBAL DIALOG", "I AM HIT");
				if (ClnEnv.getPushDataGen() != null) {
					builder.setMessage(ClnEnv.getPushDataGen().getMessage());
					builder.setCancelable(false);
					builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							me.overridePendingTransition(0, 0);
							finish();
						}
					});
					alertDialog = builder.create();
					alertDialog.show();
				}
				break;
			case R.string.CONST_DIALOG_LNTT_ONSTART:
			case R.string.CONST_DIALOG_LNTT_ONRESET:
				if (id == R.string.CONST_DIALOG_LNTT_ONSTART) {
					builder.setMessage(Utils.getString(me, R.string.MYHKT_LT_MSG_NORMAL_START_SUCCESS));
				} else if (id == R.string.CONST_DIALOG_LNTT_ONRESET) {
					builder.setMessage(Utils.getString(me, R.string.MYHKT_LT_MSG_RESET_START_SUCCESS));
				}

				builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent parentIntent = new Intent(me, ServiceListActivity.class);
						parentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						parentIntent.putExtra(Constant.KEY_IS_MY_LINE_TEST, true);
						startActivity(parentIntent);
						overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
						finish();
					}
				});
				builder.setOnCancelListener(new Dialog.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
						dialog.dismiss();
						finish() ;
					}
				});
				alertDialog = builder.create();
				alertDialog.show();
				break;
			case R.string.CONST_DIALOG_VC_FORCE_UPDATE:
				OnClickListener onPositiveClickListener = new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						gotoAppStore();
					}
				};
				OnClickListener onNegativeClickListener = new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						closeApplication();
					}
				};
				builder = new AlertDialog.Builder(this);
				builder.setMessage(getResString(R.string.version_update_blocked));
				builder.setPositiveButton(getResString(R.string.version_update_update), onPositiveClickListener);
				builder.setNegativeButton(getResString(R.string.version_update_exit), onNegativeClickListener);
				builder.setCancelable(false);
				AlertDialog alertDialog = builder.create();
				alertDialog.show();
				break;
			case R.string.CONST_DIALOG_VC_OPTION_UPDATE:
				OnClickListener onPositiveClickListener1 = new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						gotoAppStore();
					}
				};
				OnClickListener onNegativeClickListener1 = new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						ClnEnv.isVCFinish = true;
						finish();
					}
				};
				builder = new AlertDialog.Builder(this);
				builder.setMessage(getResString(R.string.version_update_optional));
				builder.setPositiveButton(getResString(R.string.version_update_update), onPositiveClickListener1);
				builder.setNegativeButton(getResString(R.string.version_update_exit), onNegativeClickListener1);
				builder.setCancelable(false);
				AlertDialog alertDialog1 = builder.create();
				alertDialog1.show();
				break;
			case R.string.CONST_DIALOG_SESSION_TIMEOUT:
				if (debug) Log.d("GLOBAL DIALOG", "SESSION TIMEOUT");
				Log.d("SESSDEBUG", "SESSION TIMEOUT A");
				builder.setMessage(R.string.DLGM_ABORT_IVSESS);
				builder.setCancelable(false);
				builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
						loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						overridePendingTransition(R.anim.left_slide_in,R.anim.right_slide_out);
						startActivity(loginIntent);
						finish();
					}
				});
				alertDialog = builder.create();
				alertDialog.show();

				ClnEnv.setHeloCra(null);
				ClnEnv.setSessionLoginID(null);
				ClnEnv.setSessionPassword(null);
				break;
			default:
				// create a default dialog
				alertDialog = builder.create();
				alertDialog.show();
				break;
		}
	}

	@Override
	public void onCancel(DialogInterface arg0) {
		// THIS IS VERY IMPORTANT TO REMOVE THE ACTIVITY WHEN THE DIALOG IS DISMISSED
		// IF NOT ADDED USER SCREEN WILL NOT RECEIVE ANY EVENTS BEFORE USER PRESSES BACK
		finish();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public String getResString(int res) {
		return Utils.getString(this, res);
	}

	// Goto Google Play
	private void gotoAppStore() {
		ClnEnv.isVCFinish = true;
		ClnEnv.isForceCloseApp = true;

		try {
			gotoHuaweiAppGallery();
		} catch (Exception exception) {
			exception.printStackTrace();
			gotoGooglePlayStore();
		}
		finish();
	}

	private void gotoGooglePlayStore() {
		Intent intent = new Intent(this, ClearActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.putExtra("FLAG", "PLAY");
		startActivity(intent);
	}

	private void gotoHuaweiAppGallery() {
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("appmarket://details?id=com.pccw.myhkt"));
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	public final void closeApplication() {
		ClnEnv.isVCFinish = true;
		ClnEnv.isForceCloseApp = true;
		gotoAppStore();
	}
}