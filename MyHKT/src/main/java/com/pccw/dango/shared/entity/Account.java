/*
    Class for Account
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/Account.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class Account implements Serializable 
{
    private static final long serialVersionUID = -6427034163229785445L;
    
    private int                     custRid;            /* CUST rid                                      */
    private String                  lob;                /* LOB                                           */
    private String                  cusNum;             /* BOM Customer Number                           */
    private String                  acctNum;            /* BOM Account Number                            */
    private boolean                 live;               /* Active account?                               */
    private String                  sysTy;              /* System Type                                   */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/Account.java $, $Rev: 844 $");
    }


    public Account()
    {
        initAndClear();
    }

    
    public Account(int rCustRid, String rLob, String rCusNum, String rAcctNum, boolean rLive, String rSysty)
    {
        this();
        
        setCustRid(rCustRid);
        setLob(rLob);
        setCusNum(rCusNum);
        setAcctNum(rAcctNum);
        setLive(rLive);
        setSysTy(rSysty);
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearCustRid();
        clearLob();
        clearCusNum();
        clearAcctNum();
        clearLive();
        clearSysTy();
    }


    public Account copyFrom(Account rSrc)
    {
        setCustRid(rSrc.getCustRid());
        setLob(rSrc.getLob());
        setCusNum(rSrc.getCusNum());
        setAcctNum(rSrc.getAcctNum());
        setLive(rSrc.isLive());
        setSysTy(rSrc.getSysTy());

        return (this);
    }


    public Account copyTo(Account rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public Account copyMe()
    {
        Account                     rDes;

        rDes = new Account();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearCustRid()
    {
        custRid = 0;
    }


    public void setCustRid(int rArg)
    {
        custRid = rArg;
    }


    public int getCustRid()
    {
        return (custRid);
    }


    public void clearLob()
    {
        lob = "";
    }


    public void setLob(String rArg)
    {
        lob = rArg;
    }


    public String getLob()
    {
        return (lob);
    }


    public void clearCusNum()
    {
        cusNum = "";
    }


    public void setCusNum(String rArg)
    {
        cusNum = rArg;
    }


    public String getCusNum()
    {
        return (cusNum);
    }


    public void clearAcctNum()
    {
        acctNum = "";
    }


    public void setAcctNum(String rArg)
    {
        acctNum = rArg;
    }


    public String getAcctNum()
    {
        return (acctNum);
    }


    public void clearLive()
    {
        live = false;
    }


    public void setLive(boolean rArg)
    {
        live = rArg;
    }


    public boolean isLive()
    {
        return (live);
    }


    public void clearSysTy()
    {
        sysTy = "";
    }


    public void setSysTy(String rArg)
    {
        sysTy = rArg;
    }


    public String getSysTy()
    {
        return (sysTy);
    }
    
    
    public boolean isSame(Account rAcct)
    {
        /*
            In DANGO, SYSTY + ACCT represents an Unique A/C
        */
        
        return (getSysTy().equals(rAcct.getSysTy()) &&
                getAcctNum().equals(rAcct.getAcctNum()));
    }
}
