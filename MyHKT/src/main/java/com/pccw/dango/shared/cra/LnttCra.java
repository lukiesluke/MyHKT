/*
    Crate for Line Test Result
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/LnttCra.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.LtrsRec;
import com.pccw.dango.shared.entity.SubnRec;

public class LnttCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 5898926235199277715L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private LtrsRec                 iLtrsRec;           /* Line Test Result Record                       */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    private boolean                 iPcdInd;            /* PCD Indicator                                 */
    private boolean                 iTvInd;             /* TV Indicator                                  */
    private boolean                 iEyeInd;            /* Eye Indicator                                 */
    private boolean                 iBBNwInd;           /* Broadband Netword Indicator                   */
    private boolean                 iFixLnInd;          /* Fix Line Indicator                            */
    
    private LtrsRec                 oLtrsRec;           /* Line Test Result Record                       */
    private boolean                 oFixLnInd;          /* Fix Line Indicator                            */
    private String                  oMdmEnMsg;          /* Modem Message (En)                            */
    private String                  oMdmZhMsg;          /* Modem Message (Zh)                            */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/LnttCra.java $, $Rev: 844 $");
    }

    
    public LnttCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearILtrsRec();
        clearISubnRec();
        clearIPcdInd();
        clearITvInd();
        clearIEyeInd();
        clearIBBNwInd();
        clearIFixLnInd();
        clearOLtrsRec();
        clearOFixLnInd();
        clearOMdmEnMsg();
        clearOMdmZhMsg();
    }


    public LnttCra copyFrom(LnttCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setILtrsRec(rSrc.getILtrsRec());
        setISubnRec(rSrc.getISubnRec());
        setIPcdInd(rSrc.isIPcdInd());
        setITvInd(rSrc.isITvInd());
        setIEyeInd(rSrc.isIEyeInd());
        setIBBNwInd(rSrc.isIBBNwInd());
        setIFixLnInd(rSrc.isIFixLnInd());
        setOLtrsRec(rSrc.getOLtrsRec());
        setOFixLnInd(rSrc.isOFixLnInd());
        setOMdmEnMsg(rSrc.getOMdmEnMsg());
        setOMdmZhMsg(rSrc.getOMdmZhMsg());

        return (this);
    }


    public LnttCra copyTo(LnttCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public LnttCra copyMe()
    {
        LnttCra                     rDes;

        rDes = new LnttCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearILtrsRec()
    {
        iLtrsRec = new LtrsRec();
    }


    public void setILtrsRec(LtrsRec rArg)
    {
        iLtrsRec = rArg;
    }


    public LtrsRec getILtrsRec()
    {
        return (iLtrsRec);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }


    public void clearIPcdInd()
    {
        iPcdInd = false;
    }


    public void setIPcdInd(boolean rArg)
    {
        iPcdInd = rArg;
    }


    public boolean isIPcdInd()
    {
        return (iPcdInd);
    }


    public void clearITvInd()
    {
        iTvInd = false;
    }


    public void setITvInd(boolean rArg)
    {
        iTvInd = rArg;
    }


    public boolean isITvInd()
    {
        return (iTvInd);
    }


    public void clearIEyeInd()
    {
        iEyeInd = false;
    }


    public void setIEyeInd(boolean rArg)
    {
        iEyeInd = rArg;
    }


    public boolean isIEyeInd()
    {
        return (iEyeInd);
    }


    public void clearIBBNwInd()
    {
        iBBNwInd = false;
    }


    public void setIBBNwInd(boolean rArg)
    {
        iBBNwInd = rArg;
    }


    public boolean isIBBNwInd()
    {
        return (iBBNwInd);
    }


    public void clearIFixLnInd()
    {
        iFixLnInd = false;
    }


    public void setIFixLnInd(boolean rArg)
    {
        iFixLnInd = rArg;
    }


    public boolean isIFixLnInd()
    {
        return (iFixLnInd);
    }


    public void clearOLtrsRec()
    {
        oLtrsRec = new LtrsRec();
    }


    public void setOLtrsRec(LtrsRec rArg)
    {
        oLtrsRec = rArg;
    }


    public LtrsRec getOLtrsRec()
    {
        return (oLtrsRec);
    }


    public void clearOFixLnInd()
    {
        oFixLnInd = false;
    }


    public void setOFixLnInd(boolean rArg)
    {
        oFixLnInd = rArg;
    }


    public boolean isOFixLnInd()
    {
        return (oFixLnInd);
    }


    public void clearOMdmEnMsg()
    {
        oMdmEnMsg = "";
    }


    public void setOMdmEnMsg(String rArg)
    {
        oMdmEnMsg = rArg;
    }


    public String getOMdmEnMsg()
    {
        return (oMdmEnMsg);
    }


    public void clearOMdmZhMsg()
    {
        oMdmZhMsg = "";
    }


    public void setOMdmZhMsg(String rArg)
    {
        oMdmZhMsg = rArg;
    }


    public String getOMdmZhMsg()
    {
        return (oMdmZhMsg);
    }
}
