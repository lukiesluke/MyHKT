/*
    Class for Directory Inquiry Search Result Details
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/DQSrchDtl.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/


package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class DQSrchDtl  implements Serializable
{
    private static final long serialVersionUID = -843035052263764089L;
    
    public String                   name;
    public String                   addr;
    public String                   cap1;
    public String                   cap2;
    public String                   cap3;
    public String                   cap4;   
    public String                   type;
    public String                   number;


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/DQSrchDtl.java $, $Rev: 844 $");
    }
    
    
    public DQSrchDtl()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        name    = "";
        addr    = "";
        cap1    = "";
        cap2    = "";
        cap3    = "";
        cap4    = "";
        type    = "";
        number  = "";
    }
    
    
    public DQSrchDtl copyFrom(DQSrchDtl rSrc)
    {
        name    = rSrc.name;
        addr    = rSrc.addr;
        cap1    = rSrc.cap1;
        cap2    = rSrc.cap2;
        cap3    = rSrc.cap3;
        cap4    = rSrc.cap4;
        type    = rSrc.type;
        number  = rSrc.number;
    
        return (this);
    }
    
    
    public DQSrchDtl copyTo(DQSrchDtl rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public DQSrchDtl copyMe()
    {
        DQSrchDtl             rDes;
        
        rDes = new DQSrchDtl();
        rDes.copyFrom(this);
        
        return (rDes);
    }
}
