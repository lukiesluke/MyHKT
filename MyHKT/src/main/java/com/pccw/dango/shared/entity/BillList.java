/*
    List of Bills of an Account
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BillList.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import com.pccw.wheat.shared.tool.Tool;


public class BillList implements Serializable
{
    private static final long serialVersionUID = 1626497264865094319L;
    
    private Account                 iAcct;                      /* Account                                  */
    private Bill[]                  oBillAry;                   /* Bill Arrays (0 - Latest)                 */

    public static final int         MAX_BILLARY         = 3;    /* Default Max Bill to be returned          */
    public static final int         MAX_BILLARY_4LTS    = 24;   /* Max Bills to be returned 4LTS thru API   */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BillList.java $, $Rev: 844 $");
    }

    
    public BillList()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearIAcct();
        clearOBillAry();
    }


    public BillList copyFrom(BillList rSrc)
    {
        setIAcct(rSrc.getIAcct());
        setOBillAry(rSrc.getOBillAry());

        return (this);
    }


    public BillList copyTo(BillList rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BillList copyMe()
    {
        BillList                     rDes;

        rDes = new BillList();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearIAcct()
    {
        iAcct = new Account();
    }


    public void setIAcct(Account rArg)
    {
        iAcct = rArg;
    }


    public Account getIAcct()
    {
        return (iAcct);
    }
    
    
    public void clearOBillAry()
    {
        setOBillAry(new Bill[0]);
    }
    
    
    public void setOBillAry(Bill rArg[])
    {
        oBillAry = rArg;
    }
    
    
    public Bill[] getOBillAry()
    {
        return (oBillAry);
    }
    
    
    public void load2BillAry(ArrayList<Bill> rALi) 
    {
        int                         rx, ri, rl;

        if (rALi.size() > 0) {

            oBillAry = new Bill[MAX_BILLARY];
            for (rx=0; rx < MAX_BILLARY; rx++) {
                if (rx < rALi.size()) {
                    oBillAry[rx] = rALi.get(rx);
                }
                else {
                    oBillAry[rx] = new Bill();
                }
            }

            sortBillAry();
        }
    }
    
    
    public void sortBillAry()
    {
        Arrays.sort(
           oBillAry,
            
            new Comparator<Bill>()
            {
                public int compare(Bill rA, Bill rB) 
                {
                    String          rBillDtA, rBillDtB;

                    rBillDtA = Tool.isNil(rA.getInvDate()) ? "" : rA.getInvDate(); 
                    rBillDtB = Tool.isNil(rB.getInvDate()) ? "" : rB.getInvDate();
                    
                    /* Date in Descending Order */
                    return (rBillDtB.compareTo(rBillDtA));
                }
            }
        );
    }
}
