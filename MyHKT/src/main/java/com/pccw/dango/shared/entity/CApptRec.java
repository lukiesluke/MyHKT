package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class CApptRec implements Serializable
{
    public int  		rid;        /* Cache Appointment Record Id  */
    public int  		cust_Rid;   /* Customer Record Id           */
    public String		pend_appt;  /* Pending Appointment Result   */
    public String		expiry_ts;  /* Expiry time                  */
    public String		create_ts;  /* Create time                  */
    public String		update_ts;  /* Last Update time             */
    public String		appt_rmdr;  /* Appointment Reminder          */

    public void setAppt_rmdr(String appt_rmdr) {
        this.appt_rmdr = appt_rmdr;
    }



    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }

    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/CApptRec.java $, $Rev: 1934 $");
    }

    public CApptRec()
    {
        initAndClear();
    }

    final void initAndClear()
    {
        init();
        clear();
    }

    protected void init()
    {
    }

    public void clear()
    {
        rid         = 0;
        cust_Rid    = 0;
        pend_appt   = "";
        expiry_ts   = "";
        create_ts   = "";
        update_ts   = "";
        appt_rmdr   = "";
    }

    public CApptRec copyFrom(CApptRec rSrc)
    {
        rid         = rSrc.rid;
        cust_Rid    = rSrc.cust_Rid;
        pend_appt   = rSrc.pend_appt;
        expiry_ts   = rSrc.expiry_ts;
        create_ts   = rSrc.create_ts;
        update_ts   = rSrc.update_ts;
        appt_rmdr   = rSrc.appt_rmdr;

        return (this);
    }

    public CApptRec copyTo(CApptRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }

    public CApptRec copyMe()
    {
        CApptRec rDes;
        rDes = new CApptRec();
        rDes.copyFrom(this);
        return (rDes);
    }

    public void trim() {
        pend_appt   = pend_appt.trim();
        expiry_ts   = expiry_ts.trim();
        create_ts   = create_ts.trim();
        update_ts   = update_ts.trim();
    }
}

