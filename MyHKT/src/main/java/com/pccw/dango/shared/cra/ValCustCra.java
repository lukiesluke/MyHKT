/*
    Crate for Validate Cust
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/ValCustCra.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.cra;

import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.wheat.shared.rpc.BaseCra;

public class ValCustCra extends BaseCra
{
    private static final long serialVersionUID = 8279470280066880374L;
    
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    
    private String                  oCslAccTy;          /* CSL Account Type                              */
    private boolean                 oUnderMig;          /* Under MIP Migration                           */
    private String                  oAcctTier;          /* Account customer tier                         */
    private String                  oTierTy;            /* Account customer tier type                    */
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/ValCustCra.java $, $Rev: 844 $");
    }

    
    public ValCustCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearISubnRec();
        clearOCslAccTy();
        clearOUnderMig();
        clearOAcctTier();
        clearOTierTy();
    }


    public ValCustCra copyFrom(ValCustCra rSrc)
    {
        setISubnRec(rSrc.getISubnRec());
        setOCslAccTy(rSrc.getOCslAccTy());
        setOAcctTier(rSrc.getOAcctTier());
        setOTierTy(rSrc.getOTierTy());
        
        return (this);
    }


    public ValCustCra copyTo(ValCustCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public ValCustCra copyMe()
    {
        ValCustCra                     rDes;

        rDes = new ValCustCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }
    
    
    public void clearOCslAccTy()
    {
        oCslAccTy = "";
    }


    public void setOCslAccTy(String rArg)
    {
        oCslAccTy = rArg;
    }


    public String getOCslAccTy()
    {
        return (oCslAccTy);
    }
    
    
    public void clearOUnderMig()
    {
        oUnderMig = false;
    }


    public void setOUnderMig(boolean rArg)
    {
        oUnderMig = rArg;
    }


    public boolean isOUnderMig()
    {
        return (oUnderMig);
    }
    
    
    public void clearOAcctTier()
    {
        oAcctTier = "";
    }


    public void setOAcctTier(String rArg)
    {
        oAcctTier = rArg;
    }


    public String getOAcctTier()
    {
        return (oAcctTier);
    }
    
    
    public void clearOTierTy()
    {
        oTierTy = "";
    }


    public void setOTierTy(String rArg)
    {
        oTierTy = rArg;
    }


    public String getOTierTy()
    {
        return (oTierTy);
    }
}
