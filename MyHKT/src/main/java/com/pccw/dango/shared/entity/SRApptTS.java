/*
    Service Request Appointment Timeslot
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/SRApptTS.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class SRApptTS implements Serializable
{
    private static final long serialVersionUID = 9211917161737021508L;
    
    private String                  apptDate;           /* Appointment Date                              */
    private String                  apptTmslot;         /* Appointment Timeslot                          */
    private String                  apptDT;             /* Appointment Datetime                          */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/SRApptTS.java $, $Rev: 844 $");
    }

    
    public SRApptTS()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearApptDate();
        clearApptTmslot();
        clearApptDT();
    }


    public SRApptTS copyFrom(SRApptTS rSrc)
    {
        setApptDate(rSrc.getApptDate());
        setApptTmslot(rSrc.getApptTmslot());
        setApptDT(rSrc.getApptDT());

        return (this);
    }


    public SRApptTS copyTo(SRApptTS rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SRApptTS copyMe()
    {
        SRApptTS                    rDes;

        rDes = new SRApptTS();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearApptDate()
    {
        apptDate = "";
    }


    public void setApptDate(String rArg)
    {
        apptDate = rArg;
    }


    public String getApptDate()
    {
        return (apptDate);
    }


    public void clearApptTmslot()
    {
        apptTmslot = "";
    }


    public void setApptTmslot(String rArg)
    {
        apptTmslot = rArg;
    }


    public String getApptTmslot()
    {
        return (apptTmslot);
    }


    public void clearApptDT()
    {
        apptDT = "";
    }


    public void setApptDT(String rArg)
    {
        apptDT = rArg;
    }


    public String getApptDT()
    {
        return (apptDT);
    }
}
