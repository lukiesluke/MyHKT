/**
 *
 * File Name    : PreviewItem.java
 * Created By   : 
 * Created Date : 27/01/2010
 * Purpose      :  
 *
 *-------------------------------------------------------------------------------------------------
 * History of Modification:
 * Date         Author          Description
 * ------------ --------------- -------------------------------------------------------------------
 */
package com.pccw.dango.shared.g3entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author
 * 
 */
public class BillPreviewItemDTO  implements Serializable 
{
    private static final long serialVersionUID = 5026369561065498493L;
    
    private String currbalance;
	private Date dfDueDate;
	private String dueDate;
	private Date dfDisplayBillDate;
	private String displayBillDate;
	//private String summary;
	private String totalAmountDue;
	private String acctName;
	private String acctNum;
	private String billDate;
	private String isDate;
	private String receiptAcctNo;

	/**
	 * 
	 */
	public BillPreviewItemDTO() {
		super();
		//setInvoice(Calendar.getInstance().getTime());
		//setTotalAmountDue(0.0);
		//setDueDate(Calendar.getInstance().getTime());
		//setSummary(new String());
	}

	public Date getDfDueDate() {
		return dfDueDate;
	}

	public void setDfDueDate(Date dfDueDate) {
		this.dfDueDate = dfDueDate;
	}

	/**
	 * @return the dueDate
	 */
	public String getDueDate() {
		return dueDate;
	}

	/**
	 * @return the summary
	 */
	/*public String getSummary() {
		return summary;
	}*/

	/**
	 * @return the totalAmountDue
	 */
	public String getTotalAmountDue() {
		return totalAmountDue;
	}

	public String getBillDate() {
		return billDate;
	}

	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}

	/**
	 * @param dueDate
	 *            the dueDate to set
	 */
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	/**
	 * @param summary
	 *            the summary to set
	 */
	/*public void setSummary(String summary) {
		this.summary = summary;
	}*/

	/**
	 * @param totalAmountDue
	 *            the totalAmountDue to set
	 */
	public void setTotalAmountDue(String totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	public String getCurrbalance() {
		return currbalance;
	}

	public void setCurrbalance(String currbalance) {
		this.currbalance = currbalance;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}

	public String getAcctNum() {
		return acctNum;
	}

	public void setAcctNum(String acctNum) {
		this.acctNum = acctNum;
	}

	public Date getDfDisplayBillDate() {
		return dfDisplayBillDate;
	}

	public void setDfDisplayBillDate(Date dfDisplayBillDate) {
		this.dfDisplayBillDate = dfDisplayBillDate;
	}

	public String getDisplayBillDate() {
		return displayBillDate;
	}

	public void setDisplayBillDate(String displayBillDate) {
		this.displayBillDate = displayBillDate;
	}

	public String getIsDate() {
		return isDate;
	}

	public void setIsDate(String isDate) {
		this.isDate = isDate;
	}

	public void setReceiptAcctNo(String receiptAcctNo) {
		this.receiptAcctNo = receiptAcctNo;
	}

	public String getReceiptAcctNo() {
		return receiptAcctNo;
	}
}
