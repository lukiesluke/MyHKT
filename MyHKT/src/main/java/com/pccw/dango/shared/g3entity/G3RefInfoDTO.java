package com.pccw.dango.shared.g3entity;

import java.io.Serializable;
import java.util.List;


public class G3RefInfoDTO implements Serializable
{
    private static final long serialVersionUID = 6334712351326378659L;

    private List<G3ServiceItemDTO> quotaServiceItemList;
    
    private String entitlementserviceType;
    private String entitlementUnit;
    public List<G3ServiceItemDTO> getQuotaServiceItemList() {
        return quotaServiceItemList;
    }
    public void setQuotaServiceItemList(List<G3ServiceItemDTO> quotaServiceItemList) {
        this.quotaServiceItemList = quotaServiceItemList;
    }
    public String getEntitlementserviceType() {
        return entitlementserviceType;
    }
    public void setEntitlementserviceType(String entitlementserviceType) {
        this.entitlementserviceType = entitlementserviceType;
    }
    public String getEntitlementUnit() {
        return entitlementUnit;
    }
    public void setEntitlementUnit(String entitlementUnit) {
        this.entitlementUnit = entitlementUnit;
    }
    public String getEntitlementValue() {
        return entitlementValue;
    }
    public void setEntitlementValue(String entitlementValue) {
        this.entitlementValue = entitlementValue;
    }
    public String getEntitlementUsedUsage() {
        return entitlementUsedUsage;
    }
    public void setEntitlementUsedUsage(String entitlementUsedUsage) {
        this.entitlementUsedUsage = entitlementUsedUsage;
    }
    public String getRoamingWiFiInScopeUsageInMins() {
        return roamingWiFiInScopeUsageInMins;
    }
    public void setRoamingWiFiInScopeUsageInMins(
            String roamingWiFiInScopeUsageInMins) {
        this.roamingWiFiInScopeUsageInMins = roamingWiFiInScopeUsageInMins;
    }
    public String getRoamingWiFiFreeEntitlementInMins() {
        return roamingWiFiFreeEntitlementInMins;
    }
    public void setRoamingWiFiFreeEntitlementInMins(
            String roamingWiFiFreeEntitlementInMins) {
        this.roamingWiFiFreeEntitlementInMins = roamingWiFiFreeEntitlementInMins;
    }
    private String entitlementValue;
    private String entitlementUsedUsage;
    
    private String roamingWiFiInScopeUsageInMins;
    private String roamingWiFiFreeEntitlementInMins;
    
}
