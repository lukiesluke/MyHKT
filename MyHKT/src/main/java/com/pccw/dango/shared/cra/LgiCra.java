/*
    Crate for Login
    
    Serve:  Login
            Change Reset Password (Reset by Customer)
            Change Init Password (Init by AdminWS)
            Recall Password (of coz, before Login, fool)
            Recall Login Id (of coz, before Login, fool)
            
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/LgiCra.java $
    $Rev: 1073 $
    $Date: 2017-12-15 16:10:16 +0800 (¶g¤­, 15 ¤Q¤G¤ë 2017) $
    $Author: alpha.lau $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.BillList;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.IbxMsgRec;
import com.pccw.dango.shared.entity.QualSvee;
import com.pccw.dango.shared.entity.SpssRec;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.wheat.shared.tool.Tool;

public class LgiCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -5125318261290287064L;
    
    private CustRec                 iCustRec;           /* Customer (for Recall)                         */
    private SveeRec                 iSveeRec;           /* Servee (Customer with Login Id)               */
    private SpssRec                 iSpssRec;           /* SmartPhone Session Record                     */
    private SubnRec                 iNaSubnRec;         /* Subscription for Next Action                  */
    private BaseCraEx               iNaCra;             /* Cra for Next Action                           */
    private String                  iOrigPwd;           /* Original Password                             */
    private boolean                 iRecallLgiId;       /* Recall Login Id?                              */
    private boolean                 iRecallPwd;         /* Recall Password?                              */
    private String                  iActionId;          /* Jrnl Action ID                                */
    private String                  iDesn;              /* Description for Logging                       */
    private boolean                 iFingerpr;          /* Fingerprint?                                  */
    
    private QualSvee                oQualSvee;          /* Qualified Servee                              */
    private SrvReq[]                oSrvReqAry;         /* Array of Service Request                      */
    private GnrlAppt[]              oGnrlApptAry;       /* Array of General Appointment                  */
    private BillList[]              oBillListAry;       /* Array of Bill List                            */
    private String                  oChatTok;           /* Chat Token                                    */
    private String                  oSoGud;             /* Secure Operation Guard (for b4 Login)         */
    private SubnRec                 oTaSubnRec;         /* Subscription for This Action                  */
    private BaseCraEx               oTaCra;             /* Cra for This Action                           */
    private int                     oIbxUnrdCnt  ;      /*Inbox Unread Count                             */
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/LgiCra.java $, $Rev: 1073 $");
    }


    public LgiCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearICustRec();
        clearISveeRec();
        clearISpssRec();
        clearINaSubnRec();
        clearINaCra();
        clearIOrigPwd();
        clearIRecallLgiId();
        clearIRecallPwd();
        clearIActionId();
        clearIDesn();
        clearIFingerpr();
        clearOQualSvee();
        clearOSrvReqAry();
        clearOGnrlApptAry();
        clearOBillListAry();
        clearOChatTok();
        clearOSoGud();
        clearOTaSubnRec();
        clearOTaCra();
        clearOIbxUnrdCnt();
    }


    public LgiCra copyFrom(LgiCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setICustRec(rSrc.getICustRec());
        setISveeRec(rSrc.getISveeRec());
        setISpssRec(rSrc.getISpssRec());
        setINaSubnRec(rSrc.getINaSubnRec());
        setINaCra(rSrc.getINaCra());
        setIOrigPwd(rSrc.getIOrigPwd());
        setIRecallLgiId(rSrc.isIRecallLgiId());
        setIRecallPwd(rSrc.isIRecallPwd());
        setIActionId(rSrc.getIActionId());
        setIDesn(rSrc.getIDesn());
        setIFingerpr(rSrc.isIFingerpr());
        setOQualSvee(rSrc.getOQualSvee());
        setOSrvReqAry(rSrc.getOSrvReqAry());
        setOGnrlApptAry(rSrc.getOGnrlApptAry());
        setOBillListAry(rSrc.getOBillListAry());
        setOChatTok(rSrc.getOChatTok());
        setOSoGud(rSrc.getOSoGud());
        setOTaSubnRec(rSrc.getOTaSubnRec());
        setOTaCra(rSrc.getOTaCra());
        setOIbxUnrdCnt(rSrc.getOIbxUnrdCnt());
        
        return (this);
    }


    public LgiCra copyTo(LgiCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public LgiCra copyMe()
    {
        LgiCra                      rDes;

        rDes = new LgiCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearICustRec()
    {
        iCustRec = new CustRec();
    }


    public void setICustRec(CustRec rArg)
    {
        iCustRec = rArg;
    }


    public CustRec getICustRec()
    {
        return (iCustRec);
    }


    public void clearISveeRec()
    {
        iSveeRec = new SveeRec();
    }


    public void setISveeRec(SveeRec rArg)
    {
        iSveeRec = rArg;
    }


    public SveeRec getISveeRec()
    {
        return (iSveeRec);
    }


    public void clearISpssRec()
    {
        iSpssRec = new SpssRec();
    }


    public void setISpssRec(SpssRec rArg)
    {
        iSpssRec = rArg;
    }


    public SpssRec getISpssRec()
    {
        return (iSpssRec);
    }


    public void clearINaSubnRec()
    {
        iNaSubnRec = new SubnRec();
    }


    public void setINaSubnRec(SubnRec rArg)
    {
        iNaSubnRec = rArg;
    }


    public SubnRec getINaSubnRec()
    {
        return (iNaSubnRec);
    }

    
    public void clearINaCra()
    {
        iNaCra = new BaseCraEx();
    }


    public void setINaCra(BaseCraEx rArg)
    {
        iNaCra = rArg;
    }


    public BaseCraEx getINaCra()
    {
        return (iNaCra);
    }


    public void clearIOrigPwd()
    {
        iOrigPwd = "";
    }


    public void setIOrigPwd(String rArg)
    {
        iOrigPwd = rArg;
    }


    public String getIOrigPwd()
    {
        return (iOrigPwd);
    }


    public void clearIRecallLgiId()
    {
        iRecallLgiId = false;
    }


    public void setIRecallLgiId(boolean rArg)
    {
        iRecallLgiId = rArg;
    }


    public boolean isIRecallLgiId()
    {
        return (iRecallLgiId);
    }


    public void clearIRecallPwd()
    {
        iRecallPwd = false;
    }


    public void setIRecallPwd(boolean rArg)
    {
        iRecallPwd = rArg;
    }


    public boolean isIRecallPwd()
    {
        return (iRecallPwd);
    }

    
    public void clearIActionId()
    {
        iActionId = "";
    }


    public void setIActionId(String rArg)
    {
        iActionId = rArg;
    }


    public String getIActionId()
    {
        return (iActionId);
    }

    
    public void clearIDesn()
    {
        iDesn = "";
    }


    public void setIDesn(String rArg)
    {
        iDesn = rArg;
    }


    public String getIDesn()
    {
        return (iDesn);
    }


    public void clearIFingerpr()
    {
        iFingerpr = false;
    }


    public void setIFingerpr(boolean rArg)
    {
        iFingerpr = rArg;
    }


    public boolean isIFingerpr()
    {
        return (iFingerpr);
    }

    
    public void clearOQualSvee()
    {
        oQualSvee = new QualSvee();
    }


    public void setOQualSvee(QualSvee rArg)
    {
        oQualSvee = rArg;
    }


    public QualSvee getOQualSvee()
    {
        return (oQualSvee);
    }


    public void clearOSrvReqAry()
    {
        oSrvReqAry = new SrvReq[0];
    }


    public void setOSrvReqAry(SrvReq[] rArg)
    {
        oSrvReqAry = rArg;
    }


    public SrvReq[] getOSrvReqAry()
    {
        return (oSrvReqAry);
    }


    public void clearOGnrlApptAry()
    {
        oGnrlApptAry = new GnrlAppt[0];
    }


    public void setOGnrlApptAry(GnrlAppt[] rArg)
    {
        oGnrlApptAry = rArg;
    }


    public GnrlAppt[] getOGnrlApptAry()
    {
        return (oGnrlApptAry);
    }


    public void clearOBillListAry()
    {
        setOBillListAry(new BillList[0]);
    }


    public void setOBillListAry(BillList[] rArg)
    {
        oBillListAry = rArg;
    }


    public BillList[] getOBillListAry()
    {
        return (oBillListAry);
    }
    
    
    public void clearOChatTok()
    {
        setOChatTok("");
    }
    
    
    public void setOChatTok(String rArg)
    {
        oChatTok = rArg;
    }
    
    
    public String getOChatTok()
    {
        return (oChatTok);
    }
    
    
    public void clearOSoGud()
    {
        setOSoGud("");
    }
    
    
    public void setOSoGud(String rArg)
    {
        oSoGud = rArg;
    }
    
    
    public String getOSoGud()
    {
        return (oSoGud);
    }


    public void clearOTaSubnRec()
    {
        oTaSubnRec = new SubnRec();
    }


    public void setOTaSubnRec(SubnRec rArg)
    {
        oTaSubnRec = rArg;
    }


    public SubnRec getOTaSubnRec()
    {
        return (oTaSubnRec);
    }
    
    
    public void clearOTaCra()
    {
        oTaCra = new BaseCraEx();
    }


    public void setOTaCra(BaseCraEx rArg)
    {
        oTaCra = rArg;
    }


    public BaseCraEx getOTaCra()
    {
        return (oTaCra);
    }
    
    
    public void clearOIbxUnrdCnt()
    {
        oIbxUnrdCnt = 0;
    }


    public void setOIbxUnrdCnt(int rArg)
    {
        oIbxUnrdCnt = rArg;
    }


    public int getOIbxUnrdCnt()
    {
        return (oIbxUnrdCnt);
    }
}
