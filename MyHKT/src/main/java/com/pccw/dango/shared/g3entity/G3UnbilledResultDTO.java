package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3UnbilledResultDTO implements Serializable 
{
    private static final long serialVersionUID = -2902239015901262075L;
    
    private java.lang.String resultRefCode;
    private java.lang.String errMsg;
    private java.lang.String errCode;
    
	public java.lang.String getResultRefCode() {
		return resultRefCode;
	}
	public void setResultRefCode(java.lang.String resultRefCode) {
		this.resultRefCode = resultRefCode;
	}
	public java.lang.String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(java.lang.String errMsg) {
		this.errMsg = errMsg;
	}
	public java.lang.String getErrCode() {
		return errCode;
	}
	public void setErrCode(java.lang.String errCode) {
		this.errCode = errCode;
	}
       
}

