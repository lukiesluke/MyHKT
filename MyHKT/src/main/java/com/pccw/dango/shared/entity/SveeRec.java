/*
    Servee (Customer with Login Id)
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/SveeRec.java $
    $Rev: 947 $
    $Date: 2016-12-19 15:36:50 +0800 (¶g¤@, 19 ¤Q¤G¤ë 2016) $
    $Author: alpha.lau $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.MiniRtException;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;

public class SveeRec extends BaseUserEx implements Serializable
{
    private static final long serialVersionUID = 1258568444994477097L;
    
    public int                      rid;                /* Record Id                                     */
    public String                   loginId;            /* Login Id                                      */
    public String                   pwd;                /* Password (Plain Text)                         */
    public String                   nickname;           /* Nick Name                                     */
    public String                   ctMail;             /* Contact Email                                 */
    public String                   ctMob;              /* Contact Mobile                                */
    public String                   mobAlrt;            /* Mobile Alert Flag                             */
    public String                   lang;               /* Language                                      */
    public int                      secQus;             /* Security Question                             */
    public String                   secAns;             /* Security Answer                               */
    public int                      custRid;            /* CUST rid                                      */
    public String                   status;             /* A/I = Active/Inactive                         */
    public String                   state;              /* State                                         */
    public String                   stateUpTs;          /* State Update TS                               */
    public String                   regnActn;           /* Registration Action Id                        */
    public String                   promoOpt;           /* Promption Message Option                      */
    public String                   pwdEff;             /* Password Effectiveness                        */
    public String                   salesChnl;          /* Sales Channel (who create this Login Id)      */
    public String                   teamCd;             /* Team Code (who create this Login Id)          */
    public String                   staffId;            /* HKT Staff Id (who create this Login Id)       */
    public String                   createTs;           /* Record Create TS                              */
    public String                   createPsn;          /* Record Create Person                          */
    public String                   lastupdTs;          /* Last Update TS                                */
    public String                   lastupdPsn;         /* Last Update Person                            */
    public int                      rev;                /* Record Revision                               */

    public static final int         L_LOGIN_ID    = 40;
    public static final int         L_PWD         = 20;
    public static final int         L_NICK_NAME   = 20;
    public static final int         L_CT_MAIL     = 40;
    public static final int         L_CT_MOB      = 20;
    public static final int         L_MOB_ALRT    = 1;
    public static final int         L_LANG        = 2;
    public static final int         L_SEC_ANS     = 40;   
    public static final int         L_STATUS      = 1;
    public static final int         L_STATE       = 2;
    public static final int         L_STATE_UPTS  = 14;
    public static final int         L_REGN_ACTN   = 20;
    public static final int         L_PROMO_OPT   = 1;
    public static final int         L_PWD_EFF     = 1;
    public static final int         L_SALES_CHNL  = 30;
    public static final int         L_TEAM_CODE   = 10;
    public static final int         L_STAFF_ID    = 10;
    public static final int         L_CREATE_TS   = 14;
    public static final int         L_CREATE_PSN  = 40;
    public static final int         L_LASTUPD_TS  = 14;
    public static final int         L_LASTUPD_PSN = 40;

    /* As as Mirror of Server Side */
    public static final String      OPT_IN        = "I";
    public static final String      OPT_OUT       = "O";
    public static final String      OPT_UNKN      = "N";
    
    public static final String      SE_ALIVE      = "A";
    
    public static final String      SE_REGISTERED = "R";
    
    public static final String      SE_INIT       = "I";  /* Created by EmboRegr        */
    public static final String      SE_INIT_T     = "I2"; /* TNC Spelled Out            */
                                                          /* >> SB Online Sales         */
    
    public static final String      SE_LARVA      = "L";  /* Created by MochiRegr       */
    public static final String      SE_LARVA_T    = "L2"; /* TNC Spelled Out            */
                                                          /* >> Admin Workstation       */
    
    public static final String      SE_INITPRO    = "P";  /* Created by EmboRegrPro     */
    public static final String      SE_INITPRO_T  = "P2"; /* TNC Spelled Out            */
                                                          /* >> SB/BOM                  */

    public static final String      SE_BULK       = "B";  /* Created by BULKREG         */
    
    public static final String      SE_CLUB_REG   = "C";  /* Club Registration          */
    
    public static final String      SE_CLBL_REG   = "U";  /* Club Registration by Staff */
    public static final String      SE_CLBL_REG_T = "U2"; /* TNC Spelled Out            */
                                                          /* >> By PCCW/HKT Staff       */
    
    public static final String      SE_CLUB_ACTV  = "V";  /* Club Activated             */

    public static final String      LG_ZH         = BiTx.LANG_ZH;
    public static final String      LG_EN         = BiTx.LANG_EN; 

    /* For Client Side Validation */
    public static final int         MIN_LOGIN_ID_LEN    = 8;
    public static final int         MIN_PWD_LEN         = 6; 
    public static final int         MIN_NICK_NAME_LEN   = 1;
    public static final int         MIN_SEC_ANS_LEN     = 4;

    public static final int         MIN_DOMAIN_LVL      = 2;
    

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/SveeRec.java $, $Rev: 947 $");
    }

    
    public SveeRec()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        rid             = 0;
        loginId         = "";
        pwd             = "";
        nickname        = "";
        ctMail          = "";
        ctMob           = "";
        mobAlrt         = "";
        lang            = "";
        secQus          = 0;
        secAns          = "";
        custRid         = 0;
        status          = "";
        state           = "";
        stateUpTs       = "";
        regnActn        = "";
        promoOpt        = "";
        pwdEff          = "";
        salesChnl       = "";
        teamCd          = "";
        staffId         = "";
        createTs        = "";
        createPsn       = "";
        lastupdTs       = "";
        lastupdPsn      = "";
        rev             = 0;
    }


    public SveeRec copyFrom(SveeRec rSrc)
    {
        rid         = rSrc.rid;
        loginId     = rSrc.loginId;
        pwd         = rSrc.pwd;
        nickname    = rSrc.nickname;
        ctMail      = rSrc.ctMail;
        ctMob       = rSrc.ctMob;
        mobAlrt     = rSrc.mobAlrt;
        lang        = rSrc.lang;
        secQus      = rSrc.secQus;
        secAns      = rSrc.secAns;
        custRid     = rSrc.custRid;
        status      = rSrc.status;
        state       = rSrc.state;
        stateUpTs   = rSrc.stateUpTs;
        regnActn    = rSrc.regnActn;
        promoOpt    = rSrc.promoOpt;
        pwdEff      = rSrc.pwdEff;
        salesChnl   = rSrc.salesChnl;
        teamCd      = rSrc.teamCd;
        staffId     = rSrc.staffId;
        createTs    = rSrc.createTs;
        createPsn   = rSrc.createPsn;
        lastupdTs   = rSrc.lastupdTs;
        lastupdPsn  = rSrc.lastupdPsn;
        rev         = rSrc.rev;

        return (this);
    }


    public SveeRec copyTo(SveeRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SveeRec copyMe()
    {
        SveeRec                     rDes;

        rDes = new SveeRec();
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public void trim()
    {
        /*
            Password and Security Answer are NOT trimmed.
            Preserve the exact User Input.
        */
        
        loginId     = loginId.trim();
        nickname    = nickname.trim();
        ctMail      = ctMail.trim();
        ctMob       = ctMob.trim();
        mobAlrt     = mobAlrt.trim();
        lang        = lang.trim();
        status      = status.trim();
        state       = state.trim();
        stateUpTs   = stateUpTs.trim();
        regnActn    = regnActn.trim();
        promoOpt    = promoOpt.trim();
        pwdEff      = pwdEff.trim();
        salesChnl   = salesChnl.trim();
        teamCd      = teamCd.trim();
        staffId     = staffId.trim();
        createTs    = createTs.trim();
        createPsn   = createPsn.trim();
        lastupdTs   = lastupdTs.trim();
        lastupdPsn  = lastupdPsn.trim();
    }
    

    public Reply verifyBaseInput()
    {
        /* For default Registration Mode */
        return verifyBaseInput(false);
    }
    
    
    public Reply verifyBaseInput(boolean rAllowPwdEmpty)
    {
        /* For Registration Mode with Option to reset Password */

        Reply                       rRR;
        
        trim();
        
        rRR = verifyLoginId();
        if (rRR.isSucc()) rRR = verifyPwd(rAllowPwdEmpty, true);
        if (rRR.isSucc()) rRR = verifyNickName(false);
        if (rRR.isSucc()) rRR = verifyCtMail();
        if (rRR.isSucc()) rRR = verifyCtMob(false);
        if (rRR.isSucc()) rRR = verifyMobAlrt();
        if (rRR.isSucc()) rRR = verifyLang();
        
        return (rRR);
    }
    
    
    public Reply verifyBaseInput4Embo()
    {
        Reply                       rRR;
        
        trim();
        
        rRR = verifyLoginId();
        if (rRR.isSucc()) rRR = verifySixDigPwd();
        if (rRR.isSucc()) rRR = verifyNickName(false);
        if (rRR.isSucc()) rRR = verifyCtMail();
        if (rRR.isSucc()) rRR = verifyCtMob(true);
        if (rRR.isSucc()) rRR = verifyMobAlrt();
        if (rRR.isSucc()) rRR = verifyLang();
        
        return (rRR);
    }
    
    
    public Reply verifyBaseInput4Larva()
    {
        Reply                       rRR;
        
        trim();
        
        rRR = verifyLoginId();
        if (rRR.isSucc()) rRR = verifySixDigPwd();
        if (rRR.isSucc()) rRR = verifyCtMail();
        if (rRR.isSucc()) rRR = verifyCtMob(true);
        if (rRR.isSucc()) rRR = verifyMobAlrt();
        if (rRR.isSucc()) rRR = verifyLang();
        if (rRR.isSucc()) rRR = verifySalesChnl();
        if (rRR.isSucc()) rRR = verifyTeamCd();
        if (rRR.isSucc()) rRR = verifyStaffId();
        
        return (rRR);
    }
    
    
    public Reply verifyBaseInput4EmboPro()
    {
        Reply                       rRR;
        
        trim();
        
        rRR = verifyLoginId();
        if (rRR.isSucc()) rRR = verifySixDigPwd();
        if (rRR.isSucc()) rRR = verifyCtMail();
        if (rRR.isSucc()) rRR = verifyCtMob(true);
        if (rRR.isSucc()) rRR = verifyMobAlrt();
        if (rRR.isSucc()) rRR = verifyLang();
        if (rRR.isSucc()) rRR = verifySalesChnl();
        if (rRR.isSucc()) rRR = verifyTeamCd();
        if (rRR.isSucc()) rRR = verifyStaffId();
        
        return (rRR);
    }
    
    
    public Reply verifyBaseInput4Bulk()
    {
        Reply                       rRR;
        
        trim();
        
        rRR = verifyLoginId();
        if (rRR.isSucc()) rRR = verifySixDigPwd();
        if (rRR.isSucc()) rRR = verifyCtMail();
        if (rRR.isSucc()) rRR = verifyCtMob(true);
        if (rRR.isSucc()) rRR = verifyMobAlrt();
        if (rRR.isSucc()) rRR = verifyLang();
        if (rRR.isSucc()) rRR = verifySalesChnl();
        if (rRR.isSucc()) rRR = verifyTeamCd();
        if (rRR.isSucc()) rRR = verifyStaffId();
        
        return (rRR);
    }
    
    
    public Reply verifyBaseInput4Club()
    {
        Reply                       rRR;
        
        trim();
        
        rRR = verifyLoginId();
        if (rRR.isSucc()) rRR = verifySixDigPwd();
        if (rRR.isSucc()) rRR = verifyNickName(true);
        if (rRR.isSucc()) rRR = verifyCtMail();
        if (rRR.isSucc()) rRR = verifyCtMob(false);
        if (rRR.isSucc()) rRR = verifyMobAlrt();
        if (rRR.isSucc()) rRR = verifyLang();
        if (rRR.isSucc()) rRR = verifySalesChnl();
        if (rRR.isSucc()) rRR = verifyTeamCd();
        if (rRR.isSucc()) rRR = verifyStaffId();

        return (rRR);
    }
    
    
    protected Reply verifyLoginId(boolean rVerifyWithFmt)
    {
        String                      rLower;
        
        if (!Tool.isASC(loginId, MIN_LOGIN_ID_LEN, L_LOGIN_ID)) {
            return (new Reply(RC.SVEE_NLLOGINID));
        }
        
         rLower = loginId.toLowerCase();
        if (!rLower.equals(loginId)) {
            return (new Reply(RC.SVEE_ICLOGINID));
        }

        
        /* 
            the rVerifyWithFmt is introduced to verify
            the login Id with email format.
            
            This should be true in DANGO.
            
            Only will be false for Wagashi - to cope
            with Malformed Login Id created in DONUT.
        */
        if (rVerifyWithFmt) {
            if (!MyTool.isVaEmail(loginId)) {
                return (new Reply(RC.SVEE_IVLOGINID));
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyLoginId()
    {
        return (verifyLoginId(true));
    }
    
    
    public Reply verifyLoginIdwoFmt()
    {
        return (verifyLoginId(false));
    }
    
    
    public Reply verifyPwd()
    {
        return verifyPwd(false, true);
    }


    public Reply verifyPwd(boolean rAllowEmpty, boolean rVerfCombn)
    {
        if (pwd.length() == 0) {
            if (rAllowEmpty) {
                return (Reply.getSucc());
            }
        }

        if (!Tool.isASC(pwd, MIN_PWD_LEN, L_PWD)) {
            return (new Reply(RC.SVEE_NLPWD));
        }

        if (rVerfCombn) {
            if (!MyTool.isValidPwdCombn(pwd)) {
                return (new Reply(RC.SVEE_IVPWDCBN));
            }
        }

        return (Reply.getSucc());
    }
    
    public Reply verifySixDigPwd()
    {
        /*
            For Password MUST contain 6 digits
        */
        
        if (pwd.length() == MIN_PWD_LEN) {
            if (Tool.isDig(pwd)) {
                return (Reply.getSucc());
            }
        }
        
        return (new Reply(RC.SVEE_IVPWDCBN));
    }
    
    
    public Reply verifyNickName(boolean rAllowEmpty)
    {
        if (nickname.length() == 0) {
            if (rAllowEmpty) return (Reply.getSucc());
            return (new Reply(RC.SVEE_ILNICKNAME));
        }
 
        if (nickname.length() < MIN_NICK_NAME_LEN ||
            nickname.length() > L_NICK_NAME) {
            return (new Reply(RC.SVEE_ILNICKNAME));
        }

//        if (Tool.isEquIC(nickname, Chat.GUEST)) {
//            return (new Reply(RC.SVEE_RESV_NCKNM));
//        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyCtMail()
    {
        String                      rLower;
        
        if (!Tool.isASC(ctMail, MIN_LOGIN_ID_LEN, L_CT_MAIL)) {
            return (new Reply(RC.SVEE_NLCTMAIL));
        }
        
        rLower = ctMail.toLowerCase();

        if (!rLower.equals(ctMail)) {
            return (new Reply(RC.SVEE_ICCTMAIL));
        }
        
        if (!MyTool.isVaEmail(ctMail)) {
            return (new Reply(RC.SVEE_IVCTMAIL));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyCtMob(boolean rAllowEmpty)
    {
        if (ctMob.length() == 0) {
            if (rAllowEmpty) {
                return (Reply.getSucc());
            }
        }
        
        if (!MyTool.isVaMob(ctMob)) {
            return (new Reply(RC.SVEE_IVCTMOB));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyMobAlrt()
    {
        if (!MyTool.isYOrN(mobAlrt)) {
            return (new Reply(RC.SVEE_IVMOBALRT));
        }
        
        return (Reply.getSucc());
    }

    
    public Reply verifyPromoOpt()
    {
        if (promoOpt.length() == 0) return (Reply.getSucc()); 
        
        if (!Tool.isInParm(promoOpt, OPT_IN, OPT_OUT, OPT_UNKN)) {
            return (new Reply(RC.SVEE_IVPROMOOPT));
        }
        
        return (Reply.getSucc());
    }

    
    public Reply verifyLang()
    {
        if (!MyTool.isVaLang(lang)) {
            return (new Reply(RC.SVEE_IVLANG));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifySalesChnl()
    {
        if (!Tool.isASC(salesChnl, 0, L_SALES_CHNL)) {
            return (new Reply(RC.SVEE_NLSALESCHNL));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyTeamCd()
    {
        if (!Tool.isASC(teamCd, 0, L_TEAM_CODE)) {
            return (new Reply(RC.SVEE_NLTEAMCD));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyStaffId()
    {
        if (!Tool.isASC(staffId, 0, L_STAFF_ID)) {
            return (new Reply(RC.SVEE_NLSTAFFID));
        }
        
        return (Reply.getSucc());
    }


    public String getId()
    {
        return (loginId);
    }


    public String getType()
    {
        return (TY_SVEE);
    }


    public String getRole()
    {
        /* Method should never be called, as SVEE is a special "User" */
        throw new MiniRtException("Non-Implemented Method!");
    }


    public String[] getRghtAry()
    {
        /* Method should never be called, as SVEE is a special "User" */
        throw new MiniRtException("Non-Implemented Method!");
    }


    public boolean isAuth(String rRght)
    {
        /* Method should never be called, as SVEE is a special "User" */
        throw new MiniRtException("Non-Implemented Method!");
    }

    public int getRid() {
        return rid;
    }
}