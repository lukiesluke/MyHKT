/*
    HKBR Customer Information Record
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BcifRec.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Const;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;

public class BcifRec implements Serializable
{
    private static final long serialVersionUID = 7711877880706507673L;
    
    public int                      custRid;            /* CUST rid                                      */
    public String                   custNm;             /* Customer Name                                 */
    public String                   industry;           /* Industry                                      */
    public String                   nuStaff;            /* Number of Staff                               */
    public String                   nuPresence;         /* Number of Office/Branch                       */
    public String                   xborder;            /* Cross Border Office/Branch                    */
    public String                   priCtName;          /* Primary Contact Name                          */
    public String                   priCtTitle;         /* Primary Contact Title (MR/MRS/MS)             */
    public String                   priCtMail;          /* Primary Contact Email                         */
    public String                   priCtJobtitle;      /* Primary Contact Job Title                     */
    public String                   priCtTel;           /* Primary Contact Tel#                          */
    public String                   priCtMob;           /* Primary Contact Mobile                        */
    public String                   secCtName;          /* Secondary Contact Name                        */
    public String                   secCtTitle;         /* Secondary Contact Title (MR/MRS/MS)           */
    public String                   secCtMail;          /* Secondary Contact Email                       */
    public String                   secCtJobtitle;      /* Secondary Contact Job Title                   */
    public String                   secCtTel;           /* Secondary Contact Tel#                        */
    public String                   secCtMob;           /* Secondary Contact Mobile                      */
    public String                   createTs;           /* Record Create TS                              */
    public String                   createPsn;          /* Record Create Person                          */
    public String                   lastupdTs;          /* Last Update TS                                */
    public String                   lastupdPsn;         /* Last Update Person                            */
    public int                      rev;                /* Record Revision                               */

    public static final int         L_CUST_NM         = 40;
    public static final int         L_INDUSTRY        = 40;
    public static final int         L_NU_STAFF        = 40;
    public static final int         L_NU_PRESENCE     = 40;
    public static final int         L_XBORDER         = 1;
    public static final int         L_PRI_CT_NAME     = 40;
    public static final int         L_PRI_CT_TITLE    = 3;
    public static final int         L_PRI_CT_MAIL     = 40;
    public static final int         L_PRI_CT_JOBTITLE = 40;
    public static final int         L_PRI_CT_TEL      = 20;
    public static final int         L_PRI_CT_MOB      = 20;
    public static final int         L_SEC_CT_NAME     = 40;
    public static final int         L_SEC_CT_TITLE    = 3;
    public static final int         L_SEC_CT_MAIL     = 40;
    public static final int         L_SEC_CT_JOBTITLE = 40;
    public static final int         L_SEC_CT_TEL      = 20;
    public static final int         L_SEC_CT_MOB      = 20;
    public static final int         L_CREATE_TS       = 14;
    public static final int         L_CREATE_PSN      = 40;
    public static final int         L_LASTUPD_TS      = 14;
    public static final int         L_LASTUPD_PSN     = 40;

    public static final String      TTL_MR            = "MR";
    public static final String      TTL_MRS           = "MRS";
    public static final String      TTL_MS            = "MS";
    

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BcifRec.java $, $Rev: 844 $");
    }

    
    public BcifRec()
    {
        initAndClear();
    }


    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }


    public void clear()
    {
        custRid         = 0;
        custNm          = "";
        industry        = "";
        nuStaff         = "";
        nuPresence      = "";
        xborder         = "";
        priCtName       = "";
        priCtTitle      = "";
        priCtMail       = "";
        priCtJobtitle   = "";
        priCtTel        = "";
        priCtMob        = "";
        secCtName       = "";
        secCtTitle      = "";
        secCtMail       = "";
        secCtJobtitle   = "";
        secCtTel        = "";
        secCtMob        = "";
        createTs        = "";
        createPsn       = "";
        lastupdTs       = "";
        lastupdPsn      = "";
        rev             = 0;
    }


    public BcifRec copyFrom(BcifRec rSrc)
    {
        custRid         = rSrc.custRid;
        custNm          = rSrc.custNm;
        industry        = rSrc.industry;
        nuStaff         = rSrc.nuStaff;
        nuPresence      = rSrc.nuPresence;
        xborder         = rSrc.xborder;
        priCtName       = rSrc.priCtName;
        priCtTitle      = rSrc.priCtTitle;
        priCtMail       = rSrc.priCtMail;
        priCtJobtitle   = rSrc.priCtJobtitle;
        priCtTel        = rSrc.priCtTel;
        priCtMob        = rSrc.priCtMob;
        secCtName       = rSrc.secCtName;
        secCtTitle      = rSrc.secCtTitle;
        secCtMail       = rSrc.secCtMail;
        secCtJobtitle   = rSrc.secCtJobtitle;
        secCtTel        = rSrc.secCtTel;
        secCtMob        = rSrc.secCtMob;
        createTs        = rSrc.createTs;
        createPsn       = rSrc.createPsn;
        lastupdTs       = rSrc.lastupdTs;
        lastupdPsn      = rSrc.lastupdPsn;
        rev             = rSrc.rev;

        return (this);
    }


    public BcifRec copyTo(BcifRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BcifRec copyMe()
    {
        BcifRec                     rDes;

        rDes = new BcifRec();
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public void trim()
    {
        custNm          = custNm.trim();
        industry        = industry.trim();
        nuStaff         = nuStaff.trim();
        nuPresence      = nuPresence.trim();
        xborder         = xborder.trim();
        priCtName       = priCtName.trim();
        priCtTitle      = priCtTitle.trim();
        priCtMail       = priCtMail.trim();
        priCtJobtitle   = priCtJobtitle.trim();
        priCtTel        = priCtTel.trim();
        priCtMob        = priCtMob.trim();
        secCtName       = secCtName.trim();
        secCtTitle      = secCtTitle.trim();
        secCtMail       = secCtMail.trim();
        secCtJobtitle   = secCtJobtitle.trim();
        secCtTel        = secCtTel.trim();
        secCtMob        = secCtMob.trim();
    }
    
    
    public Reply verifyBaseInput()
    {
        Reply                       rRR;
        
        trim();
        
        rRR = verifyCustNm();
        if (rRR.isSucc()) rRR = verifyIndustry();
        if (rRR.isSucc()) rRR = verifyNuStaff();
        if (rRR.isSucc()) rRR = verifyNuPresence();
        if (rRR.isSucc()) rRR = verifyXBorder();
        if (rRR.isSucc()) rRR = verifyPriCtName();
        if (rRR.isSucc()) rRR = verifyPriCtTitle();
        if (rRR.isSucc()) rRR = verifyPriCtMail();
        if (rRR.isSucc()) rRR = verifyPriCtJobTitle();
        if (rRR.isSucc()) rRR = verifyPriCtTel();
        if (rRR.isSucc()) rRR = verifyPriCtMob();
        if (rRR.isSucc()) rRR = verifyPriCtDN();
        if (rRR.isSucc()) rRR = verifySecCtName();
        if (rRR.isSucc()) rRR = verifySecCtTitle();
        if (rRR.isSucc()) rRR = verifySecCtMail();
        if (rRR.isSucc()) rRR = verifySecCtJobTitle();
        if (rRR.isSucc()) rRR = verifySecCtTel();
        if (rRR.isSucc()) rRR = verifySecCtMob();

        return (rRR);
    }
    
    /* All Fields are Optional */
    
    public Reply verifyCustNm()
    {
        //if (custNm.length() == 0 || custNm.length() > L_CUST_NM) {
        if (custNm.length() > L_CUST_NM) {
            return (new Reply(RC.BCIF_ILCUSTNM));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyIndustry()
    {
        //if (!Tool.isASC(industry, 1, L_INDUSTRY)) {
        if (!Tool.isASC(industry, 0, L_INDUSTRY)) {
            return (new Reply(RC.BCIF_NLINDUST));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNuStaff()
    {
        //if (!Tool.isASC(nuStaff, 1, L_NU_STAFF)) {
        if (!Tool.isASC(nuStaff, 0, L_NU_STAFF)) {
            return (new Reply(RC.BCIF_NLNUSTFF));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNuPresence()
    {
        //if (!Tool.isASC(nuPresence, 1, L_NU_PRESENCE)) {
        if (!Tool.isASC(nuPresence, 0, L_NU_PRESENCE)) {  
            return (new Reply(RC.BCIF_NLNUPRSN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyXBorder()
    {
        if (xborder.length() > 0) {
            if (!Tool.isInParm(xborder, Const.Y, Const.N)) {
                return (new Reply(RC.BCIF_IVXBRDR));
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPriCtName()
    {
        //if (priCtName.length() == 0 || priCtName.length() > L_PRI_CT_NAME) {
        if (priCtName.length() > L_PRI_CT_NAME) {
            return (new Reply(RC.BCIF_ILPRICTNM));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPriCtTitle()
    {
        if (priCtTitle.length() > 0) {
            if (!Tool.isInParm(priCtTitle, TTL_MR, TTL_MRS, TTL_MS)) {
                return (new Reply(RC.BCIF_IVPRICTTL));
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPriCtMail()
    {
        if (priCtMail.length() > 0) {
            if (!Tool.isASC(priCtMail, 1, L_PRI_CT_MAIL)) {
                return (new Reply(RC.BCIF_NLPRICTEM));
            }
            else {
                if (!MyTool.isVaEmail(priCtMail)) {
                    return (new Reply(RC.BCIF_IVPRICTEM));
                }
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPriCtJobTitle()
    {
        //if (priCtJobtitle.length() == 0 || priCtJobtitle.length() > L_PRI_CT_JOBTITLE) {
        if (priCtJobtitle.length() > L_PRI_CT_JOBTITLE) {            
            return (new Reply(RC.BCIF_ILPRICTJT));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPriCtTel()
    {
        if (priCtTel.length() > 0) {
            if (!Tool.isASC(priCtTel, 1, L_PRI_CT_TEL)) {
                return (new Reply(RC.BCIF_NLPRICTTEL));
            }
            else {
                if (!MyTool.isVaTel(priCtTel)) {
                    return (new Reply(RC.BCIF_IVPRICTTEL));
                }
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPriCtMob()
    {
        if (priCtMob.length() > 0) {
            if (!Tool.isASC(priCtMob, 1, L_PRI_CT_MOB)) {
                return (new Reply(RC.BCIF_NLPRICTMOB));
            }
            else {
                if (!MyTool.isVaMob(priCtMob)) {
                    return (new Reply(RC.BCIF_IVPRICTMOB));
                }
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPriCtDN()
    {
        /*
        if (priCtTel.length() == 0 && priCtMob.length() == 0) {
            return (new Reply(RC.BCIF_NO_PRICTDN));
        }
        */
        return (Reply.getSucc());
    }
    
    
    /* Secondary Fields are Optional */
    
    public Reply verifySecCtName()
    {
        if (secCtName.length() > L_PRI_CT_NAME) {
            return (new Reply(RC.BCIF_ILSECCTNM));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifySecCtTitle()
    {
        if (!Tool.isInParm(secCtTitle, "", TTL_MR, TTL_MRS, TTL_MS)) {
            return (new Reply(RC.BCIF_IVSECCTTL));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifySecCtMail()
    {
        if (secCtMail.length() > 0) {
            if (!Tool.isASC(secCtMail, 1, L_SEC_CT_MAIL)) {
                return (new Reply(RC.BCIF_NLSECCTEM));
            }
            else {
                if (!MyTool.isVaEmail(secCtMail)) {
                    return (new Reply(RC.BCIF_IVSECCTEM));
                }
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifySecCtJobTitle()
    {
        if (secCtJobtitle.length() > 0) {
            if (secCtJobtitle.length() > L_SEC_CT_JOBTITLE) {
                return (new Reply(RC.BCIF_ILSECCTJT));
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifySecCtTel()
    {
        if (secCtTel.length() > 0) {
            if (!Tool.isASC(secCtTel, 1, L_SEC_CT_TEL)) {
                return (new Reply(RC.BCIF_NLSECCTTEL));
            }
            else {
                if (!MyTool.isVaTel(secCtTel)) {
                    return (new Reply(RC.BCIF_IVSECCTTEL));
                }
            }
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifySecCtMob()
    {
        if (secCtMob.length() > 0) {
            if (!Tool.isASC(secCtMob, 1, L_SEC_CT_MOB)) {
                return (new Reply(RC.BCIF_NLSECCTMOB));
            }
            else {
                if (!MyTool.isVaMob(secCtMob)) {
                    return (new Reply(RC.BCIF_IVSECCTMOB));
                }
            }
        }
        
        return (Reply.getSucc());
    }
}
