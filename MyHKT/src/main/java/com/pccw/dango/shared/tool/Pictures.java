/*
    Class for Format Extra Utilities

    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/Pictures.java $
    $Rev: 1123 $
    $Date: 2018-07-17 15:12:03 +0800 (¶g¤G, 17 ¤C¤ë 2018) $
    $Author: alpha.lau $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;


public class Pictures implements Serializable
{
    private static final long serialVersionUID = -5502422246400498053L;
    
    public static final String      TIMESTAMP               = "yyyyMMddHHmmss";
    public static final String      Y2000_DATE              = "yyyyMMdd";
    public static final String      SLASH_DATE              = "dd/MM/yyyy";
    public static final String      DAY_MONTH               = "dd-MMM";
    public static final String      SLASH_DAY_MONTH         = "dd/MM";
    public static final String      MONTH_YR                = "MMM-yyyy";
    public static final String      HYPHEN_DATE             = "dd-MM-yyyy";
    public static final String      HYPHEN_DATE_YY          = "dd-MM-yy";
    public static final String      HYPHEN_DATE_MON_NA      = "dd-MMM-yyyy";
    public static final String      SLASH_DATE_MON_UP2_MIN  = "dd/MMM/yyyy HH:mm";
    public static final String      SLASH_DATE_MM_UP2_MIN   = "dd/MM/yyyy HH:mm";
    public static final String      SLASH_DATE_MM_UP2_SEC   = "dd/MM/yyyy HH:mm:ss";
    public static final String      SLASH_DATE_MM_UP2_MON   = "MM/yyyy";
    public static final String      PLAIN_TIME              = "HHmmss";
    public static final String      PLAIN_HOUR_MIN          = "HHmm";
    public static final String      FORMAT_HOUR_MIN         = "HH:mm";
    public static final String      FORMAT_UP2_MIN          = "dd-MM-yyyy HH:mm";
    public static final String      FORMAT_UP2_SEC          = "dd-MM-yyyy HH:mm:ss";

    /* Unit to be displayed */
    public static final String      UNT_KB                  = "KB";
    public static final String      UNT_MB                  = "MB";
    public static final String      UNT_GB                  = "GB";
    public static final String      UNT_MIN                 = "min(s)";
    public static final String      UNT_MSG                 = "msg";
    
    public static final String      DOLLAR_SIGN             = "$";
    public static final String      ONE_DECIMIAL            = "0.0";
    public static final String      ZERO                    = "0";
    public static final String      DASH                    = "-";
    
    public static final String      VAL_UNLIMIT             = "Unlimit";
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/Pictures.java $, $Rev: 1123 $");
    }
}    
