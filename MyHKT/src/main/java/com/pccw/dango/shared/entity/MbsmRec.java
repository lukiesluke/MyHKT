/*
    Mobile Service Message Record
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/MbsmRec.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class MbsmRec implements Serializable
{
    private static final long serialVersionUID = -3983007530921042987L;
    
    public int                      srid;            /* Static Record ID         */
    public String                   srvTy;           /* Service Type             */
    public String                   srvSts;          /* Service Status           */
    public String                   stsReasonCd;     /* Status Reason Code       */
    public String                   acctCredSts;     /* Account Credit Status    */
    public String                   pymtMth;         /* Payment Method           */
    public String                   pymtRejSts;      /* Payment Reject Status    */
    public String                   holdDunCd;       /* Hold Dunning Code        */
    public String                   msgEn;           /* English Message          */
    public String                   msgZh;           /* Chinese Message          */
    public String                   status;          /* Create TS                */
    
    public static final int         L_SRV_TY        = 50;
    public static final int         L_SRV_STS       = 20;
    public static final int         L_STS_REASON_CD = 10;
    public static final int         L_ACCT_CRED_STS = 20;
    public static final int         L_PYMT_MTH      = 20;
    public static final int         L_PYMT_REJ_STS  = 20;
    public static final int         L_HOLD_DUN_CD   = 10;
    public static final int         L_MSG_EN        = 512;
    public static final int         L_MSG_ZH        = 512;
    public static final int         L_STATUS        = 1;
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/MbsmRec.java $, $Rev: 844 $");
    }

    
    public MbsmRec()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        srid            = 0;
        srvTy          = "";
        srvSts         = "";
        stsReasonCd   = "";
        acctCredSts   = "";
        pymtMth        = "";
        pymtRejSts    = "";
        holdDunCd     = "";
        msgEn          = "";
        msgZh          = "";
    }
    
    
    public MbsmRec copyFrom(MbsmRec rSrc)
    {
        srid            = rSrc.srid;
        srvTy           = rSrc.srvTy;
        srvSts          = rSrc.srvSts;
        stsReasonCd     = rSrc.stsReasonCd;
        acctCredSts     = rSrc.acctCredSts;
        pymtMth         = rSrc.pymtMth;
        pymtRejSts      = rSrc.pymtRejSts;
        holdDunCd       = rSrc.holdDunCd;
        msgEn           = rSrc.msgEn;
        msgZh           = rSrc.msgZh;
        status          = rSrc.status;
    
        return (this);
    }
    
    
    public MbsmRec copyTo(MbsmRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public MbsmRec copyMe()
    {
        MbsmRec             rDes;
        
        rDes = new MbsmRec();
        rDes.copyFrom(this);
        
        return (rDes);
    }
}
