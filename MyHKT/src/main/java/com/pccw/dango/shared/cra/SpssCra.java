/*
    Crate for Smart Phone Session
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/SpssCra.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.SpssRec;

public class SpssCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -3050685656674676902L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private String                  iCkSum;             /* Checksum                                      */
    private SpssRec                 iSpssRec;           /* SmartPhone Session Record                     */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/SpssCra.java $, $Rev: 844 $");
    }

    
    public SpssCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearICkSum();
        clearISpssRec();
    }


    public SpssCra copyFrom(SpssCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setICkSum(rSrc.getICkSum());
        setISpssRec(rSrc.getISpssRec());

        return (this);
    }


    public SpssCra copyTo(SpssCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SpssCra copyMe()
    {
        SpssCra                     rDes;

        rDes = new SpssCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }
    
    
    public void clearICkSum()
    {
        iCkSum = "";
    }


    public void setICkSum(String rArg)
    {
        iCkSum = rArg;
    }


    public String getICkSum()
    {
        return (iCkSum);
    }


    public void clearISpssRec()
    {
        iSpssRec = new SpssRec();
    }


    public void setISpssRec(SpssRec rArg)
    {
        iSpssRec = rArg;
    }


    public SpssRec getISpssRec()
    {
        return (iSpssRec);
    }
}
