/*
    Class for BiTx Map

    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/BiTxMap.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.pccw.dango.shared.entity.BiTx;


public class BiTxMap implements Serializable
{
    private static final long serialVersionUID = -2095883277584053140L;
    
    private Map<String, BiTx>       map;
    private String                  lang;
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/BiTxMap.java $, $Rev: 844 $");
    }


    public BiTxMap()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        clearMap();
        clearLang();
    }

    
    public void clearMap()
    {
        setMap(new HashMap<String, BiTx>());
    }
    
    
    public void setMap(Map<String, BiTx> rMap)
    {
        map = rMap;
    }
    
    
    public Map<String, BiTx> getMap()
    {
        return (map);
    }
    
    
    public void clearLang()
    {
        /* Default to ZH */
        setZh();
    }
    
    
    private void setLang(String rLang)
    {
        lang = rLang;
    }
    
    
    public String getLang()
    {
        return (lang);
    }
    
    
    public void setZh()
    {
        setLang(BiTx.LANG_ZH);
    }
    
    
    public void setEn()
    {
        setLang(BiTx.LANG_EN);
    }
    
    
    public boolean isZh()
    {
        return (BiTx.isZh(getLang()));
    }
    
    
    public boolean isEn()
    {
        return (BiTx.isEn(getLang()));
    }
    

    public void put(BiTx rBiTx)
    {
        getMap().put(rBiTx.getTag(), rBiTx);
    }
    
    
    public BiTx getBiTx(String rVar)
    {
        return (getMap().get(rVar));
    }


    public String get(String rVar, boolean rNullIfNotFnd)
    {
        BiTx                        rBiTx;
        String                      rRes;
        
        rRes = "";
        rBiTx = getBiTx(rVar);
        
        if (rBiTx == null) {
            /* rNullIfNotFnd normally for Non-Text base LabelText */
            
            if (rNullIfNotFnd) return (null);
            rRes = varNotFound(rVar);
        }
        else {
            rRes = rBiTx.getText(getLang());
        }
        
        return (rRes);
    }
    
    
    public String get(String rVar)
    {
        return (get(rVar, false));
    }
    
    
    public static String varNotFound(String rVar)
    {
        return (":?" + rVar + ":");
    }
}
