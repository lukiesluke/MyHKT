/*
    Class for Activity Log Record to be used in GWT
    (At the moment of Sep 2016, only WAGASHI will use it)
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/AlogRec.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;



public class AlogRec implements Serializable
{
    private static final long serialVersionUID = -189623657322013190L;
    
    public int                      rid;            /* Record ID            */
    public String                   rcpt_psn_ty;    /* Rcpt PSN Type        */
    public String                   rcpt_psn;       /* Rcpt PSN (Login ID)  */
    public int                      rcpt_rid;       /* Rcpt Record ID       */
    public String                   rcpt_lob;       /* Rcpt LOB             */
    public String                   rcpt_cus_num;   /* Customer Number      */
    public String                   rcpt_acct_num;  /* Account Number       */
    public String                   rcpt_srv_id;    /* BOM Srv Instance ID  */
    public String                   rcpt_srv_num;   /* Service Number       */
    public String                   rcpt_phylum;    /* Phylum               */
    public String                   action_id;      /* Action ID            */
    public String                   desn;           /* Description          */
    public String                   action_ip;      /* Action IP            */
    public String                   chnl;           /* Channel-Web/App/API  */
    public String                   sys_id;         /* ID of System Peer    */
    public String                   store_ty;       /* Store Type           */
    public String                   create_psn_ty;  /* Create Person Type   */
    public String                   create_ts;      /* Create TS            */
    public String                   create_psn;     /* Create Person        */

    public static final int         L_RCPT_PSN_TY   = 4;
    public static final int         L_RCPT_PSN      = 40;
    public static final int         L_RCPT_LOB      = 6;
    public static final int         L_RCPT_CUS_NUM  = 8;
    public static final int         L_RCPT_ACCT_NUM = 14;
    public static final int         L_RCPT_SRV_ID   = 16;
    public static final int         L_RCPT_SRV_NUM  = 32;
    public static final int         L_RCPT_PHYLUM   = 4;
    public static final int         L_ACTION_ID     = 20;
    public static final int         L_DESN          = 512;
    public static final int         L_ACTION_IP     = 20;
    public static final int         L_CHNL          = 8;
    public static final int         L_SYS_ID        = 40;
    public static final int         L_STORE_TY      = 2;
    public static final int         L_CREATE_PSN_TY = 4;
    public static final int         L_CREATE_TS     = 14;
    public static final int         L_CREATE_PSN    = 40;
    

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/AlogRec.java $, $Rev: 844 $");
    }

    
    public AlogRec()
    {
        initAndClear();
    }


    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        rid             = 0;
        rcpt_psn_ty     = "";
        rcpt_psn        = "";
        rcpt_rid        = 0;
        rcpt_lob        = "";
        rcpt_cus_num    = "";
        rcpt_acct_num   = "";
        rcpt_srv_id     = "";
        rcpt_srv_num    = "";
        rcpt_phylum     = "";
        action_id       = "";
        desn            = "";
        action_ip       = "";
        chnl            = "";
        sys_id          = "";
        store_ty        = "";
        create_psn_ty   = "";
        create_ts       = "";
        create_psn      = "";
    }


    public AlogRec copyFrom(AlogRec rSrc)
    {
        rid                         = rSrc.rid;
        rcpt_psn_ty                 = rSrc.rcpt_psn_ty;
        rcpt_psn                    = rSrc.rcpt_psn;
        rcpt_rid                    = rSrc.rcpt_rid;
        rcpt_lob                    = rSrc.rcpt_lob;
        rcpt_cus_num                = rSrc.rcpt_cus_num;
        rcpt_acct_num               = rSrc.rcpt_acct_num;
        rcpt_srv_id                 = rSrc.rcpt_srv_id;
        rcpt_srv_num                = rSrc.rcpt_srv_num;
        rcpt_phylum                 = rSrc.rcpt_phylum;
        action_id                   = rSrc.action_id;
        desn                        = rSrc.desn;
        action_ip                   = rSrc.action_ip;
        chnl                        = rSrc.chnl;
        sys_id                      = rSrc.sys_id;
        store_ty                    = rSrc.store_ty;
        create_psn_ty               = rSrc.create_psn_ty;
        create_ts                   = rSrc.create_ts;
        create_psn                  = rSrc.create_psn;

        return (this);
    }


    public AlogRec copyTo(AlogRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public AlogRec copyMe()
    {
        AlogRec                     rDes;

        rDes = new AlogRec();
        rDes.copyFrom(this);
        return (rDes);
    }
}
