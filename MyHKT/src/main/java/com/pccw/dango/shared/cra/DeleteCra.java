package com.pccw.dango.shared.cra;

import com.pccw.wheat.shared.tool.Reply;

import java.io.Serializable;

public class DeleteCra implements Serializable {
    private String iLoginId;
    private String iSveeRid;
    private String apiTy;
    private String clnVer;
    private String sysId;
    private String sysPwd;
    private String userId;
    private String phylum;
    private String psnTy;
    private Reply reply;

    public String getiLoginId() {
        return iLoginId;
    }

    public void setiLoginId(String iLoginId) {
        this.iLoginId = iLoginId;
    }

    public String getiSveeRid() {
        return iSveeRid;
    }

    public void setiSveeRid(String iSveeRid) {
        this.iSveeRid = iSveeRid;
    }

    public String getApiTy() {
        return apiTy;
    }

    public void setApiTy(String apiTy) {
        this.apiTy = apiTy;
    }

    public String getClnVer() {
        return clnVer;
    }

    public void setClnVer(String clnVer) {
        this.clnVer = clnVer;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getSysPwd() {
        return sysPwd;
    }

    public void setSysPwd(String sysPwd) {
        this.sysPwd = sysPwd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhylum() {
        return phylum;
    }

    public void setPhylum(String phylum) {
        this.phylum = phylum;
    }

    public String getPsnTy() {
        return psnTy;
    }

    public void setPsnTy(String psnTy) {
        this.psnTy = psnTy;
    }

    public Reply getReply() {
        return reply;
    }

    public void setReply(Reply reply) {
        this.reply = reply;
    }
}
