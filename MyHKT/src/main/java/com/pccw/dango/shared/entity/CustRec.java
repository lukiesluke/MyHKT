/*
    Customer Record
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/CustRec.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;

import java.io.Serializable;

public class CustRec implements Serializable
{
    private static final long serialVersionUID = 6015017640243272780L;
    
    public int                      rid;                /* Record Id                                     */
    public String                   docTy;              /* Document Type: HKID/PASS/HKBR                 */
    public String                   docNum;             /* Document Number                               */
    public String                   premier;            /* Premier Indicator                             */
    public String                   phylum;             /* Phylum: COMM/CSUM                             */
    public String                   status;             /* A/I = Active/Inactive                         */
    public String                   createTs;           /* Record Create TS                              */
    public String                   createPsn;          /* Record Create Person                          */
    public String                   lastupdTs;          /* Last Update TS                                */
    public String                   lastupdPsn;         /* Last Update Person                            */
    public int                      rev;                /* Record Revision                               */

    public static final int         L_DOC_TY      = 4;
    public static final int         L_DOC_NUM     = 30;
    public static final int         L_PREMIER     = 4;
    public static final int         L_PHYLUM      = 4;
    public static final int         L_STATUS      = 1;
    public static final int         L_CREATE_TS   = 14;
    public static final int         L_CREATE_PSN  = 40;
    public static final int         L_LASTUPD_TS  = 14;
    public static final int         L_LASTUPD_PSN = 40;

    /* Customer Type, prefer match with value stored in BOM */
    public static final String      TY_HKID       = "HKID";
    public static final String      TY_PASSPORT   = "PASS";
    public static final String      TY_HKBR       = "BS";
    
    public static final String      PREM_NOTDEF   = "";     /* No Premier Definition        */
    public static final String      PREM_NOTCLZD  = "N";    /* Not Classified as Premier    */

    public static final String      PH_COMM       = "COMM"; /* Commercial Segment           */
    public static final String      PH_CSUM       = "CSUM"; /* Consumers                    */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/CustRec.java $, $Rev: 844 $");
    }

    
    public CustRec()
    {
        initAndClear();
    }


    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }


    public void clear()
    {
        rid             = 0;
        docTy           = "";
        docNum          = "";
        premier         = "";
        phylum          = "";
        status          = "";
        createTs        = "";
        createPsn       = "";
        lastupdTs       = "";
        lastupdPsn      = "";
        rev             = 0;
    }


    public CustRec copyFrom(CustRec rSrc)
    {
        rid             = rSrc.rid;
        docTy           = rSrc.docTy;
        docNum          = rSrc.docNum;
        premier         = rSrc.premier;
        phylum          = rSrc.phylum;
        status          = rSrc.status;
        createTs        = rSrc.createTs;
        createPsn       = rSrc.createPsn;
        lastupdTs       = rSrc.lastupdTs;
        lastupdPsn      = rSrc.lastupdPsn;
        rev             = rSrc.rid;

        return (this);
    }


    public CustRec copyTo(CustRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public CustRec copyMe()
    {
        CustRec                     rDes;

        rDes = new CustRec();
        rDes.copyFrom(this);
        return (rDes);
    }

    
    public void trim()
    {
        docTy           = docTy.trim();
        docNum          = docNum.trim();
        premier         = premier.trim();
        phylum          = phylum.trim();
        status          = status.trim();
        createTs        = createTs.trim();
        createPsn       = createPsn.trim();
        lastupdTs       = lastupdTs.trim();
        lastupdPsn      = lastupdPsn.trim();
    }
    
    
    public Reply verifyBaseInput()
    {
        Reply                       rRC;

        trim();
        
        rRC = verifyDocTy();
        if (rRC.isSucc()) rRC = verifyDocNum();
        
        return (rRC);
    }
    
    
    public Reply verifyDocTy()
    {
        if (!Tool.isInParm(docTy, TY_HKID, TY_PASSPORT, TY_HKBR)) {
            return (new Reply(RC.CUST_IVDOCTY));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyDocTy4Csum()
    {
        if (!Tool.isInParm(docTy, TY_HKID, TY_PASSPORT)) {
            return (new Reply(RC.CUST_DOCTYMMPH));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyDocTy4Comm()
    {
        if (!Tool.isInParm(docTy, TY_HKBR)) {
            return (new Reply(RC.CUST_DOCTYMMPH));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyDocNum()
    {
        String                      rUpper;
        
        if (!Tool.isASC(docNum, 1, L_DOC_NUM)) {
            return (new Reply(RC.CUST_NLDOCNUM));
        }
        
        rUpper = docNum.toUpperCase();
        if (!rUpper.equals(docNum)) {
            return (new Reply(RC.CUST_ICDOCNUM));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyPremier()
    {
        /*
            Optional Verification, for API Access
        */
        
        if (premier.length() == 0 || premier.length() > L_PREMIER) {
            return (new Reply(RC.CUST_IVPREMIER));
        }
        
        return (Reply.getSucc());
    }

    
    public boolean isSameCust(String rDocTy, String rDocNum)
    {
        return (rDocTy.equals(docTy) && rDocNum.equals(docNum));
    }

    
    public boolean isSameCust(CustRec rCustRec)
    {
        return (isSameCust(rCustRec.docTy, rCustRec.docNum));
    }

    public boolean isPremier() {
        /* Any Value Not = 'N' or '' (after trimmed) is Premier */
        if (!premier.equals(PREM_NOTCLZD)) {
            return !premier.trim().isEmpty();
        }
        return false;
    }

    public boolean isComm() {
        return (phylum.equals(PH_COMM));
    }

    public boolean isCsum() {
        return (phylum.equals(PH_CSUM));
    }
}
