/*
    Bill Inquiry
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/Bill.java $
    $Rev: 919 $
    $Date: 2016-11-03 14:59:09 +0800 (¶g¥|, 03 ¤Q¤@¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class Bill  implements Serializable
{
    private static final long serialVersionUID = 4277682926362534310L;
    
    private String                  sysTy;              /* System Type                                   */
    private String                  acctNum;            /* Account Number                                */
    private String                  invDate;            /* Invoice Date                                  */
    private String                  invAmt;             /* Invoice Amount                                */
    private String                  withheld;           /* Withhold Indicator (for DRG only)             */
    private String                  dueDate;            /* Due Date (for DRG only)                       */
    private String                  status;             /* Status                                        */
    private String                  drgBillty;          /* drgBillty                                     */

    public static final String      ST_INIT         = "I";
    public static final String      ST_PREPARE      = "P";
    public static final String      ST_READY        = "R";
    public static final String      ST_ERROR        = "E";

    public static void main(String rArg[])
    {
        System.out.println(getVer());
    }

    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/Bill.java $, $Rev: 919 $");
    }

    public Bill() {
        initAndClear();
    }

    final void initAndClear() {
        init();
        clear();
    }

    protected void init() {
    }

    public void clear() {
        clearSysTy();
        clearAcctNum();
        clearInvDate();
        clearInvAmt();
        clearWithheld();
        clearDueDate();
        clearStatus();
        clearDrgBillty();
    }

    public Bill copyFrom(Bill rSrc) {
        setSysTy(rSrc.getSysTy());
        setAcctNum(rSrc.getAcctNum());
        setInvDate(rSrc.getInvDate());
        setInvAmt(rSrc.getInvAmt());
        setWithheld(rSrc.getWithheld());
        setDueDate(rSrc.getDueDate());
        setStatus(rSrc.getStatus());
        setDrgBillty(rSrc.getDrgBillty());
        return (this);
    }

    public Bill copyTo(Bill rDes) {
        rDes.copyFrom(this);
        return (rDes);
    }

    public Bill copyMe() {
        Bill rDes;
        rDes = new Bill();
        rDes.copyFrom(this);
        return (rDes);
    }

    public void clearSysTy() {
        sysTy = "";
    }

    public String getSysTy() {
        return (sysTy);
    }

    public void setSysTy(String rArg) {
        sysTy = rArg;
    }

    public void clearAcctNum() {
        acctNum = "";
    }

    public String getAcctNum() {
        return (acctNum);
    }

    public void setAcctNum(String rArg) {
        acctNum = rArg;
    }

    public void clearInvDate() {
        invDate = "";
    }

    public String getInvDate() {
        return (invDate);
    }

    public void setInvDate(String rArg) {
        invDate = rArg;
    }

    public void clearInvAmt() {
        invAmt = "";
    }

    public String getInvAmt() {
        return (invAmt);
    }

    public void setInvAmt(String rArg) {
        invAmt = rArg;
    }

    public void clearWithheld() {
        withheld = "";
    }

    public String getWithheld() {
        return (withheld);
    }

    public void setWithheld(String rArg) {
        withheld = rArg;
    }

    public void clearDueDate() {
        dueDate = "";
    }

    public String getDueDate() {
        return (dueDate);
    }

    public void setDueDate(String rArg) {
        dueDate = rArg;
    }

    public void clearStatus() {
        status = "";
    }

    public void clearDrgBillty() {
        drgBillty = "";
    }

    public String getStatus() {
        return (status);
    }

    public void setStatus(String rArg) {
        status = rArg;
    }

    public String getDrgBillty() {
        return drgBillty;
    }

    public void setDrgBillty(String drgBillty) {
        this.drgBillty = drgBillty;
    }
}
