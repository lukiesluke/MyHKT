package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3DisplayServiceItemDTO implements Serializable
{
    private static final long serialVersionUID = -1169865527341518847L;
    
    private String region;
    private String serviceType;
    
    private String show;
    private String showInMainPage;
    private int displayOrder;
    
    private String titleLevel1Eng;
    private String titleLevel1Chi;
    private String titleLevel2Eng;
    private String titleLevel2Chi;
    private String titleLevel1Display;
    private String titleLevel2Display;
    private String planType;
    
    
    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    private G3UsageDescriptionDTO usageDescription;
    
    private G3UsageBarDTO usageBar;
        
    private String asOfDateEng;
    private String asOfDateChi;
    private String asOfDateDisplay;
    
    private G3MessageDTO additionalMessage;
    
    private String usageType;
    private String allowTopup;
    private String otherVAS;
    
    private G3QuotaTopupInfoDTO quotaTopupInfo;
    
    private G3RefInfoDTO refInfo;

    private String usageCondition;

    
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public String getShowInMainPage() {
        return showInMainPage;
    }

    public void setShowInMainPage(String showInMainPage) {
        this.showInMainPage = showInMainPage;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getTitleLevel1Eng() {
        return titleLevel1Eng;
    }

    public void setTitleLevel1Eng(String titleLevel1Eng) {
        this.titleLevel1Eng = titleLevel1Eng;
    }

    public String getTitleLevel1Chi() {
        return titleLevel1Chi;
    }

    public void setTitleLevel1Chi(String titleLevel1Chi) {
        this.titleLevel1Chi = titleLevel1Chi;
    }

    public String getTitleLevel2Eng() {
        return titleLevel2Eng;
    }

    public void setTitleLevel2Eng(String titleLevel2Eng) {
        this.titleLevel2Eng = titleLevel2Eng;
    }

    public String getTitleLevel2Chi() {
        return titleLevel2Chi;
    }

    public void setTitleLevel2Chi(String titleLevel2Chi) {
        this.titleLevel2Chi = titleLevel2Chi;
    }

    public G3UsageDescriptionDTO getUsageDescription() {
        return usageDescription;
    }

    public void setUsageDescription(G3UsageDescriptionDTO usageDescription) {
        this.usageDescription = usageDescription;
    }

    public G3UsageBarDTO getUsageBar() {
        return usageBar;
    }

    public void setUsageBar(G3UsageBarDTO usageBar) {
        this.usageBar = usageBar;
    }

    public String getAsOfDateEng() {
        return asOfDateEng;
    }

    public void setAsOfDateEng(String asOfDateEng) {
        this.asOfDateEng = asOfDateEng;
    }

    public String getAsOfDateChi() {
        return asOfDateChi;
    }

    public void setAsOfDateChi(String asOfDateChi) {
        this.asOfDateChi = asOfDateChi;
    }

    public G3MessageDTO getAdditionalMessage() {
        return additionalMessage;
    }

    public void setAdditionalMessage(G3MessageDTO additionalMessage) {
        this.additionalMessage = additionalMessage;
    }

    public String getUsageType() {
        return usageType;
    }

    public void setUsageType(String usageType) {
        this.usageType = usageType;
    }

    public String getAllowTopup() {
        return allowTopup;
    }   

    public String getOtherVAS() {
        return otherVAS;
    }

    public void setOtherVAS(String otherVAS) {
        this.otherVAS = otherVAS;
    }

    public void setAllowTopup(String allowTopup) {
        this.allowTopup = allowTopup;
    }

    public G3QuotaTopupInfoDTO getQuotaTopupInfo() {
        return quotaTopupInfo;
    }

    public void setQuotaTopupInfo(G3QuotaTopupInfoDTO quotaTopupInfo) {
        this.quotaTopupInfo = quotaTopupInfo;
    }

    public G3RefInfoDTO getRefInfo() {
        return refInfo;
    }

    public void setRefInfo(G3RefInfoDTO refInfo) {
        this.refInfo = refInfo;
    }

    public String getTitleLevel1Display() {
        return titleLevel1Display;
    }

    public void setTitleLevel1Display(String titleLevel1Display) {
        this.titleLevel1Display = titleLevel1Display;
    }

    public String getTitleLevel2Display() {
        return titleLevel2Display;
    }

    public void setTitleLevel2Display(String titleLevel2Display) {
        this.titleLevel2Display = titleLevel2Display;
    }

    public String getAsOfDateDisplay() {
        return asOfDateDisplay;
    }

    public void setAsOfDateDisplay(String asOfDateDisplay) {
        this.asOfDateDisplay = asOfDateDisplay;
    }

    public String getUsageCondition() {
        return usageCondition;
    }

    public void setUsageCondition(String usageCondition) {
        this.usageCondition = usageCondition;
    }
}

