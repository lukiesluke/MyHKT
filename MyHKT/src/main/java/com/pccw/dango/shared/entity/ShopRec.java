/*
    Shop Record
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/ShopRec.java $
    $Rev: 1073 $
    $Date: 2017-12-15 16:10:16 +0800 (¶g¤­, 15 ¤Q¤G¤ë 2017) $
    $Author: alpha.lau $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;


public class ShopRec implements Serializable
{
    private static final long serialVersionUID = -1242307486477227203L;
    
    public int                      rid;        /* Record ID                */
    public String                   shop_cd;    /* Shop Code                */
    public String                   shop_ty;    /* Shop Type                */
    public String                   shop_cat;   /* Shop Category            */
    public String                   area;       /* Area                     */
    public String                   district_en;/* District in English      */
    public String                   district_zh;/* District in Chinese      */
    public String                   addr_en;    /* Address in English       */
    public String                   addr_zh;    /* Address in Chinese       */
    public String                   ct_num;     /* Contact Number           */
    public String                   bz_hr_en;   /* Business Hour            */
    public String                   bz_hr_zh;   /* Business Hour            */
    public double                   longitude;  /* Longitude                */
    public double                   latitude;   /* Latitude                 */
    public String                   desn_en;    /* Description in English   */
    public String                   desn_zh;    /* Description in Chinese   */
    public String                   name_en;    /* Name in English          */
    public String                   name_zh;    /* Name in Chinese          */
    public String                   create_ts;  /* Create TS                */
    public String                   create_psn; /* Create Person            */
    public String                   lastupd_ts; /* Last Update TS           */
    public String                   lastupd_psn;/* Last Update Person       */
    public int                      rev;        /* Record Revision          */
    
    public static final int         L_SHOP_CODE   = 6;
    public static final int         L_SHOP_TY     = 2;
    public static final int         L_SHOP_CAT    = 8;
    public static final int         L_AREA        = 10;
    public static final int         L_DISTRICT_EN = 40;
    public static final int         L_DISTRICT_ZH = 40;
    public static final int         L_ADDR_EN     = 1024;
    public static final int         L_ADDR_ZH     = 1024;
    public static final int         L_CT_NUM      = 20;
    public static final int         L_BZ_HR_EN    = 150;
    public static final int         L_BZ_HR_ZH    = 150;
    public static final int         L_DESN_EN     = 100;
    public static final int         L_DESN_ZH     = 100;
    public static final int         L_NAME_EN     = 100;
    public static final int         L_NAME_ZH     = 100;
    public static final int         L_CREATE_TS   = 14;
    public static final int         L_CREATE_PSN  = 40;
    public static final int         L_LASTUPD_TS  = 14;
    public static final int         L_LASTUPD_PSN = 40;
    
    public static final String      TY_SHOP       = "S";
    public static final String      TY_CENTRE     = "C";
    public static final String      TY_SMART_LIV  = "L";
    public static final String      TY_1010       = "1";
    public static final String      TY_CSL_DOT    = "D";
    public static final String      TY_IOT        = "I";
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/ShopRec.java $, $Rev: 1073 $");
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        rid             = 0;
        shop_cd         = "";
        shop_ty         = "";
        shop_cat        = "";
        area            = "";
        district_en     = "";
        district_zh     = "";
        addr_en         = "";
        addr_zh         = "";
        ct_num          = "";
        bz_hr_en        = "";
        bz_hr_zh        = "";
        longitude       = 0;
        latitude        = 0;
        desn_en         = "";
        desn_zh         = "";
        name_en         = "";
        name_zh         = "";
        create_ts       = "";
        create_psn      = "";
        lastupd_ts      = "";
        lastupd_psn     = "";
        rev             = 0;                
    }
    
    
    public ShopRec copyFrom(ShopRec rSrc)
    {
        rid             = rSrc.rid;
        shop_cd         = rSrc.shop_cd;
        shop_ty         = rSrc.shop_ty;
        shop_cat        = rSrc.shop_cat;
        area            = rSrc.area;
        district_en     = rSrc.district_en;
        district_zh     = rSrc.district_zh;
        addr_en         = rSrc.addr_en;
        addr_zh         = rSrc.addr_zh;
        ct_num          = rSrc.ct_num;
        bz_hr_en        = rSrc.bz_hr_en;
        bz_hr_zh        = rSrc.bz_hr_zh;
        longitude       = rSrc.longitude;
        latitude        = rSrc.latitude;
        desn_en         = rSrc.desn_en;
        desn_zh         = rSrc.desn_zh;
        name_en         = rSrc.name_en;
        name_zh         = rSrc.name_zh;
        create_ts       = rSrc.create_ts;
        create_psn      = rSrc.create_psn;
        lastupd_ts      = rSrc.lastupd_ts;
        lastupd_psn     = rSrc.lastupd_psn;
        rev             = rSrc.rev;              
    
        return (this);
    }
    
    
    public ShopRec copyTo(ShopRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public ShopRec copyMe()
    {
        ShopRec             rDes;
        
        rDes = new ShopRec();
        rDes.copyFrom(this);
        
        return (rDes);
    }
    
    
    public void trim() 
    {
        shop_cd         = shop_cd.trim();
        shop_ty         = shop_ty.trim();
        shop_cat        = shop_cat.trim();
        area            = area.trim();
        district_en     = district_en.trim();
        district_zh     = district_zh.trim();
        addr_en         = addr_en.trim();
        addr_zh         = addr_zh.trim();
        ct_num          = ct_num.trim();
        bz_hr_en        = bz_hr_en.trim();
        bz_hr_zh        = bz_hr_zh.trim();
        desn_en         = desn_en.trim();
        desn_zh         = desn_zh.trim();
        name_en         = name_en.trim();
        name_zh         = name_zh.trim();
    }
    
    
    public Reply verifyBaseInput()
    {
        Reply                       rRR;
        
        trim();
        
        rRR = verifyShopCd();
        if (rRR.isSucc()) rRR = verifyShopTy();
        if (rRR.isSucc()) rRR = verifyShopCat();
        if (rRR.isSucc()) rRR = verifyArea();
        if (rRR.isSucc()) rRR = verifyDistrictEn();
        if (rRR.isSucc()) rRR = verifyDistrictZh();
        if (rRR.isSucc()) rRR = verifyAddrEn();
        if (rRR.isSucc()) rRR = verifyAddrZh();
        if (rRR.isSucc()) rRR = verifyCtNum();
        if (rRR.isSucc()) rRR = verifyBzHrEn();
        if (rRR.isSucc()) rRR = verifyBzHrZh();
        if (rRR.isSucc()) rRR = verifyLongitude();
        if (rRR.isSucc()) rRR = verifyLatitude();
        if (rRR.isSucc()) rRR = verifyDesnEn();
        if (rRR.isSucc()) rRR = verifyDesnZh();
        if (rRR.isSucc()) rRR = verifyNameEn();
        if (rRR.isSucc()) rRR = verifyNameZh();
        
        return (rRR);
    }
    
    
    public Reply verifyShopCd()
    {
        if (!Tool.isASC(shop_cd, 1, L_SHOP_CODE)) {
            return (new Reply(RC.SHOP_IVSHOPCD));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyShopTy()
    {
        if (!Tool.isASC(shop_ty, 1, L_SHOP_TY)) {
            return (new Reply(RC.SHOP_IVSHOPTY));
        }
        
        if (!Tool.isInParm(shop_ty, TY_SHOP, TY_CENTRE, TY_SMART_LIV, TY_1010, TY_CSL_DOT, TY_IOT)) {
            return (new Reply(RC.SHOP_IVSHOPTY));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyShopCat()
    {
        if (!Tool.isASC(shop_cat, 0, L_SHOP_CAT)) {
            return (new Reply(RC.SHOP_IVSHOPCAT));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyArea()
    {
        if (!Tool.isASC(area, 1, L_AREA)) {
            return (new Reply(RC.SHOP_IVAREA));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyDistrictEn()
    {
        if (district_en.length() > L_DISTRICT_EN) {
            return (new Reply(RC.SHOP_IVDIST_EN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyDistrictZh()
    {
        if (district_zh.length() > L_DISTRICT_ZH) {
            return (new Reply(RC.SHOP_IVDIST_ZH));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyAddrEn()
    {
        if (addr_en.length() > L_ADDR_EN) {
            return (new Reply(RC.SHOP_IVADDR_EN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyAddrZh()
    {
        if (addr_zh.length() > L_ADDR_ZH) {
            return (new Reply(RC.SHOP_IVADDR_ZH));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyCtNum()
    {
        if (!Tool.isASC(ct_num, 0, L_CT_NUM)) {
            return (new Reply(RC.SHOP_IVCT_NUM));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyBzHrEn()
    {
        if (bz_hr_en.length() > L_BZ_HR_EN) {
            return (new Reply(RC.SHOP_IVBZHR_EN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyBzHrZh()
    {
        if (bz_hr_zh.length() > L_BZ_HR_ZH) {
            return (new Reply(RC.SHOP_IVBZHR_ZH));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyLongitude()
    {
        if (longitude < -180 || longitude > 180) {
            return (new Reply(RC.SHOP_IVLONG));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyLatitude()
    {
        if (latitude < -90 || latitude > 90) {
            return (new Reply(RC.SHOP_IVLAT));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyDesnEn()
    {
        if (desn_en.length() > L_DESN_EN) {
            return (new Reply(RC.SHOP_IVDESN_EN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyDesnZh()
    {
        if (desn_zh.length() > L_DESN_ZH) {
            return (new Reply(RC.SHOP_IVDESN_ZH));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNameEn()
    {
        if (name_en.length() > L_NAME_EN) {
            return (new Reply(RC.SHOP_IVNAME_EN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNameZh()
    {
        if (name_zh.length() > L_NAME_ZH) {
            return (new Reply(RC.SHOP_IVNAME_ZH));
        }
        
        return (Reply.getSucc());
    }
}
