/*
    Crate for DQ
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/DqryCra.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.pccw.dango.shared.entity.DQPageInfo;
import com.pccw.dango.shared.entity.DQReq;
import com.pccw.dango.shared.entity.DQSrchDtl;

public class DqryCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 1799068914105750244L;
    
    private DQReq                   iReq;               /* DQ Request                                    */
    
    private List<DQSrchDtl>         oSrchDtlLst;        /* Detail of Current Result Page                 */
    private DQPageInfo              oPageInfo;          /* Current Result Page                           */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/DqryCra.java $, $Rev: 844 $");
    }

    
    public DqryCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearIReq();
        clearOSrchDtlLst();
        clearOPageInfo();
    }


    public DqryCra copyFrom(DqryCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setIReq(rSrc.getIReq());
        setOSrchDtlLst(rSrc.getOSrchDtlLst());
        setOPageInfo(rSrc.getOPageInfo());

        return (this);
    }


    public DqryCra copyTo(DqryCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public DqryCra copyMe()
    {
        DqryCra                     rDes;

        rDes = new DqryCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearIReq()
    {
        iReq = new DQReq();
    }


    public void setIReq(DQReq rArg)
    {
        iReq = rArg;
    }


    public DQReq getIReq()
    {
        return (iReq);
    }


    public void clearOSrchDtlLst()
    {
        oSrchDtlLst = new ArrayList<DQSrchDtl>();
    }


    public void setOSrchDtlLst(List<DQSrchDtl> rArg)
    {
        oSrchDtlLst = rArg;
    }


    public List<DQSrchDtl> getOSrchDtlLst()
    {
        return (oSrchDtlLst);
    }


    public void clearOPageInfo()
    {
        oPageInfo = new DQPageInfo();
    }


    public void setOPageInfo(DQPageInfo rArg)
    {
        oPageInfo = rArg;
    }


    public DQPageInfo getOPageInfo()
    {
        return (oPageInfo);
    }
}
