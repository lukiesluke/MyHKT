package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3QuotaTopupInfoDTO implements Serializable
{
    private static final long serialVersionUID = -5155181427851809887L;
    
    private String topupQuotaName;
    private String quotaValidityPeriod;
    private long initialQuotaInKB;  
    private String pcrfServiceName;
    private String pcrfServiceType;
    private String displayNameInEnglish;
    
    public String getTopupQuotaName() {
        return topupQuotaName;
    }
    public void setTopupQuotaName(String topupQuotaName) {
        this.topupQuotaName = topupQuotaName;
    }
    public String getQuotaValidityPeriod() {
        return quotaValidityPeriod;
    }
    public void setQuotaValidityPeriod(String quotaValidityPeriod) {
        this.quotaValidityPeriod = quotaValidityPeriod;
    }   
    public String getPcrfServiceName() {
        return pcrfServiceName;
    }
    public void setPcrfServiceName(String pcrfServiceName) {
        this.pcrfServiceName = pcrfServiceName;
    }
    public String getPcrfServiceType() {
        return pcrfServiceType;
    }
    public void setPcrfServiceType(String pcrfServiceType) {
        this.pcrfServiceType = pcrfServiceType;
    }
    public String getDisplayNameInEnglish() {
        return displayNameInEnglish;
    }
    public void setDisplayNameInEnglish(String displayNameInEnglish) {
        this.displayNameInEnglish = displayNameInEnglish;
    }
    public long getInitialQuotaInKB() {
        return initialQuotaInKB;
    }
    public void setInitialQuotaInKB(long initialQuotaInKB) {
        this.initialQuotaInKB = initialQuotaInKB;
    }   
    
    
}
