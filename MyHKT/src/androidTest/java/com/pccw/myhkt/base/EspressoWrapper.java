package com.pccw.myhkt.base;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.test.espresso.FailureHandler;
import androidx.test.espresso.NoMatchingViewException;
import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.matcher.BoundedMatcher;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.kyleduo.switchbutton.SwitchButton;
import com.pccw.myhkt.helpers.RecyclerViewMatcher;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeDown;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.Intents.times;
import static androidx.test.espresso.intent.matcher.ComponentNameMatchers.hasClassName;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withHint;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.pccw.myhkt.helpers.EspressoTestsMatchers.withDrawable;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;

public class EspressoWrapper {
    public boolean isStop = false;

    private ViewInteraction findStringContaining(String hardText) {
        return onView(withText(hardText)).withFailureHandler(new FailureHandler() {
            @Override
            public void handle(Throwable error, Matcher<View> viewMatcher) {
                if (error != null){
                    isStop = false;
                }else{
                    isStop = true;
                }
            }
        });
    }

    public void checkDialogStringIsDisplayed(Context context, int stringId){
        String text = context.getString(stringId);
        checkDialogStringIsDisplayed(text);
    }

    public void checkStringIsDisplayed(Context context, int stringId){
        String text = context.getString(stringId);
        checkStringIsDisplayed(text);
    }

    public void checkDialogStringIsDisplayed(String text){
        findStringContaining(text).inRoot(isDialog()).check(matches(isDisplayed()));
    }

    public void checkStringIsDisplayed(String text){
        findStringContaining(text).check(matches(isDisplayed()));
    }

    public void checkStringIsDisplayed(String text, int viewId, int parentDescendantOfId, int grandParentDescendantOfId) {
        onView(allOf(withId(viewId),
                withText(text),
                isDescendantOfA(allOf(withId(parentDescendantOfId),
                        isDescendantOfA(withId(grandParentDescendantOfId))))))
                .check(matches(isDisplayed()));
    }

    public void checkStringIsDisplayed(int viewId, int descentantOfId, String text){
        onView(allOf(withId(viewId),
                isDescendantOfA(withId(descentantOfId))))
                .check(matches(compareEditText(text)));
    }

    public void checkDialogStringIsHidden(String text){
        findStringContaining(text).inRoot(isDialog()).check(matches(not(isDisplayed())));
    }

    public void checkStringIsHidden(String text){
        findStringContaining(text).check(matches(not(isDisplayed())));
    }

    public void checkStringIsHidden(Context context, int stringId){
        String text = context.getString(stringId);
        checkStringIsHidden(text);
    }

    public void clickString(Context context, int stringId){
        String text = context.getString(stringId);
        clickString(text);
    }

    public void clickDialogString(String text){
        findStringContaining(text).inRoot(isDialog()).perform(click());
    }

    public void clickString(String text){
        findStringContaining(text).perform(click());
    }

    public void clickString(String text, int descendantOfId){
        onView(allOf(withText(text), isDescendantOfA(withId(descendantOfId)))).perform(click());
    }

    public void checkIntendedActivity(String name){
        intended(hasComponent(hasClassName(name)));
    }

    public void checkIntendedActivity(String name, int times){
        intended(hasComponent(hasClassName(name)), times(times));
    }

    protected void checkViewVisibility(int viewId, int visibilityFlag) {
        switch(visibilityFlag) {
            case View.VISIBLE :
                onView(allOf(withId(viewId))).check(matches(checkIfVisible()));
                break;
            case View.INVISIBLE :
                onView(allOf(withId(viewId))).check(matches(checkIfInvisible()));
                break;
            case View.GONE :
                onView(allOf(withId(viewId))).check(matches(checkIfGone()));
                break;
        }
    }

    protected void checkViewVisibility(int viewId, int descendantId, int visibilityFlag) {
        switch(visibilityFlag) {
            case View.VISIBLE :
                onView(allOf(withId(viewId),
                        isDescendantOfA(withId(descendantId)))).check(matches(checkIfVisible()));
                break;
            case View.INVISIBLE :
                onView(allOf(withId(viewId),
                        isDescendantOfA(withId(descendantId)))).check(matches(checkIfInvisible()));
                break;
            case View.GONE :
                onView(allOf(withId(viewId),
                        isDescendantOfA(withId(descendantId)))).check(matches(checkIfGone()));
                break;
        }
    }

    public void checkViewIsDisplayed(int viewId){
        onView(withId(viewId)).withFailureHandler(new FailureHandler() {
            @Override
            public void handle(Throwable error, Matcher<View> viewMatcher) {
                if (error != null){
                    isStop = false;
                }else{
                    isStop = true;
                }
            }
        }).check(matches(isDisplayed()));
    }

    public void checkViewIsDisplayed(int viewId, int descendantOfId){
        onView(allOf(withId(viewId),isDescendantOfA(withId(descendantOfId)))).check(matches(isDisplayed()));
    }

    public void checkViewIsDisplayed(int adapterViewId, int viewId, int position){
        onData(anything()).inAdapterView(withId(adapterViewId))
                .atPosition(position)
                .onChildView(withId(viewId))
                .check(matches(isDisplayed()));
    }

    public void checkViewIsNotDisplayed(int viewId){
        onView(withId(viewId)).check(matches(not(isDisplayed())));
    }

    public void checkViewIsNotDisplayed(int viewId, int descendantOfId){
        onView(allOf(withId(viewId),isDescendantOfA(withId(descendantOfId)))).check(matches(not(isDisplayed())));
    }

    public void clickView(int viewId){
        onView(withId(viewId)).perform(click());
    }

    public void clickView(int viewId, int descendantOfId){
        onView(allOf(withId(viewId),isDescendantOfA(withId(descendantOfId)))).perform(click());
    }

    public void clickViewChildView(int viewOfId, int childPosition){
        onView(nthChildOf(withId(viewOfId), childPosition)).perform(click());
    }

    public void clickViewInAdapter(int adapterViewId, int position){
        onData(anything()).inAdapterView(withId(adapterViewId))
                .atPosition(position).perform(click());
    }

    public void clickViewChildInAdapter(int adapterViewId, int viewId, int position){
        onData(anything()).inAdapterView(withId(adapterViewId))
                .atPosition(position)
                .onChildView(withId(viewId))
                .perform(click());
    }

    public void clickView(Context context, int recycleViewId, int position, int viewId, int stringId){
        onView(withRecyclerView(recycleViewId)
                .atPositionOnView(position, viewId))
                .check(matches(withText(context.getResources().getString(stringId))))
                .perform(click());
    }

    public void clearTextView(int viewId){
        onView(withId(viewId)).perform(clearText());
    }

    protected void clearTextView(int viewId, int descendantOfId){
        onView(allOf(withId(viewId),
                isDescendantOfA(withId(descendantOfId))))
                .perform(clearText());
    }

    public void scrollView(int viewId){
        onView(withId(viewId)).perform(scrollTo());
    }

    public void scrollView(int viewId, int descendantOf){
        onView(allOf(withId(viewId), isDescendantOfA(withId(descendantOf)))).perform(ViewActions.scrollTo());
    }

    public void checkViewIsChecked(int viewId){
        onView(withId(viewId)).check(matches(isChecked()));
    }

    public void checkViewIsNotChecked(int viewId){
        onView(withId(viewId)).check(matches(not(isChecked())));
    }

    public void typeTextView(int viewId, String text){
        onView(withId(viewId))
                .perform(clearText(), typeText(text),closeSoftKeyboard());
    }

    public void typeTextView(int viewId, int descendantOfId, String text){
        onView(allOf(withId(viewId),
                isDescendantOfA(withId(descendantOfId))))
                .perform(clearText(), typeText(text),closeSoftKeyboard());
    }

    public void checkStringHint( int viewId, String text){
        onView(withId(viewId))
                .check(matches(withHintCustom(text)));
    }

    public void checkStringHint(Context context, int viewId, int stringId){
        checkStringHint(viewId,context.getString(stringId));
    }

    public void checkStringHint(int viewId, int descendantOfId, String text){
        onView(allOf(withId(viewId),
                isDescendantOfA(withId(descendantOfId))))
                .check(matches(withHint(text)));
    }

    public void checkStringHint(Context context, int viewId, int descendantOfId, int stringId){
        checkStringHint(viewId,descendantOfId,context.getString(stringId));
    }

    public void checkViewIsEnabled(int viewId){
        onView(withId(viewId)).check(matches(isEnabled()));
    }

    public void checkViewIsDisabled(int viewId){
        onView(withId(viewId)).check(matches(not(isEnabled())));
    }

    public void checkViewString(int viewId, int descendantOfId, String text) {
        onView(allOf(withId(viewId),
                isDescendantOfA(withId(descendantOfId))))
                .check(matches(withText(text)));
    }

    public void checkViewString(int viewId, String hardText){
        onView(withId(viewId))
                .check(matches(withText(containsString(hardText))));
    }

    public void checkViewPasswordVisible(int viewId, int descendantOfId){
        onView(allOf(withId(viewId),
                isDescendantOfA(withId(descendantOfId))))
                .check(matches(isPasswordVisible()));
    }

    public void checkViewPasswordHidden(int viewId, int descendantOfId){
        onView(allOf(withId(viewId),
                isDescendantOfA(withId(descendantOfId))))
                .check(matches(isPasswordHidden()));
    }

    public void swipeUpView(int viewId){
        onView(withId(viewId)).perform(swipeUp());
    }

    public void swipeDownView(int viewId){
        onView(withId(viewId)).perform(swipeDown());
    }

    public void swipeLeftView(int viewId){
        onView(withId(viewId)).perform(swipeLeft());
    }

    public void swipeRightView(int viewId){
        onView(withId(viewId)).perform(swipeRight());
    }

    public void pressBackView(int viewId){
        onView(withId(viewId)).perform(pressBack());
    }

    public void pressBackRoot(){
        onView(isRoot()).perform(ViewActions.pressBack());
    }

    public void swipeLeftView(int adapterId, int position){
        onData(anything())
                .inAdapterView(withId(adapterId))
                .atPosition(position)
                .perform(swipeLeft());
    }

    public void checkViewWithText(int viewId, String text){
        onView(withId(viewId)).check(matches(withText(text)));
    }

    public void checkViewWithText(int viewId, Context context, int stringId){
        onView(withId(viewId)).check(matches(withText(context.getString(stringId))));
    }

    public void checkViewWithText(int viewId, int descendantOfId, String text){
        onView(allOf(withId(viewId), isDescendantOfA(withId(descendantOfId)))).check(matches(withText(text)));
    }

    public void checkViewWithDrawable(int viewId, int drawableId){
        onView(withId(viewId)).check(matches(withDrawable(drawableId)));
    }

    protected void checkViewStartsWith(int viewId, String prefix) {
        onView(allOf(withId(viewId)))
                .check(matches(checkIfTextStartsWith(prefix)));
    }

    public Matcher<View> isPasswordVisible() {
        return new BoundedMatcher<View, EditText>(EditText.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("Password is hidden");
            }

            @Override
            public boolean matchesSafely(EditText editText) {
                //returns true if password is hidden
                return editText.getTransformationMethod() instanceof HideReturnsTransformationMethod;
            }
        };
    }

    public Matcher<View> isPasswordHidden() {
        return new BoundedMatcher<View, EditText>(EditText.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("Password is hidden");
            }

            @Override
            public boolean matchesSafely(EditText editText) {
                //returns true if password is hidden
                return editText.getTransformationMethod() instanceof PasswordTransformationMethod;
            }
        };
    }

    public static Matcher<View> withHintCustom(final String expectedHint) {
        return new TypeSafeMatcher<View>() {

            @Override
            public boolean matchesSafely(View view) {
                if (!(view instanceof EditText)) {
                    return false;
                }

                String hint = ((EditText) view).getHint().toString();

                return expectedHint.equals(hint);
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }

    public RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {
        return new RecyclerViewMatcher(recyclerViewId);
    }

    protected static TypeSafeMatcher<View> compareEditText(final String string) {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                String target = ((EditText) item).getText().toString();

                return target.equals(string);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("compareEditText");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIfEnabled() {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {

                return item.isEnabled();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIfEnabled");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIfVisible() {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                return item.getVisibility() == View.VISIBLE;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIfVisible");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIfInvisible() {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                return item.getVisibility() == View.INVISIBLE;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIfInvisible");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIfGone() {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                return item.getVisibility() == View.GONE;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIfGone");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIfTextStartsWith(final String prefix) {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                String target = ((TextView) item).getText().toString();
                return target.startsWith(prefix);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIfTextStartsWith");
            }
        };
    }

    public Matcher<View> nthChildOf(final Matcher<View> parentMatcher, final int childPosition) {
        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("with "+childPosition+" child view of type parentMatcher");
            }

            @Override
            public boolean matchesSafely(View view) {
                if (!(view.getParent() instanceof ViewGroup)) {
                    return parentMatcher.matches(view.getParent());
                }

                ViewGroup group = (ViewGroup) view.getParent();
                return parentMatcher.matches(view.getParent()) && group.getChildAt(childPosition).equals(view);
            }
        };
    }

    protected void clickSpannableView(int viewId, String text) {
        onView(withId(viewId)).perform(clickClickableSpan(text));
    }

    protected void clickSpannableViewByString(String originString, String targetString) {
        onView(withText(originString)).perform(clickClickableSpan(targetString));
    }

    protected void clickSpannableViewByString(Context context, int originStringId, int targetStringId) {
        clickSpannableViewByString(context.getResources().getString(originStringId),
                context.getResources().getString(targetStringId));
    }

    protected void clickSpannableViewByString(String originString, Context context, int targetStringId) {
        clickSpannableViewByString(originString, context.getResources().getString(targetStringId));
    }

    protected void clickSpannableView(int viewId, Context context, int resId) {
        clickSpannableView(viewId, context.getResources().getString(resId));
    }

    protected static ViewAction clickClickableSpan(final CharSequence textToClick) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return Matchers.instanceOf(TextView.class);
            }

            @Override
            public String getDescription() {
                return "clicking on a ClickableSpan";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView textView = (TextView) view;
                SpannableString spannableString = (SpannableString) textView.getText();

                if (spannableString.length() == 0) {
                    // TextView is empty, nothing to do
                    throw new NoMatchingViewException.Builder()
                            .includeViewHierarchy(true)
                            .withRootView(textView)
                            .build();
                }

                // Get the links inside the TextView and check if we find textToClick
                ClickableSpan[] spans = spannableString.getSpans(0, spannableString.length(), ClickableSpan.class);
                if (spans.length > 0) {
                    ClickableSpan spanCandidate;
                    for (ClickableSpan span : spans) {
                        spanCandidate = span;
                        int start = spannableString.getSpanStart(spanCandidate);
                        int end = spannableString.getSpanEnd(spanCandidate);
                        CharSequence sequence = spannableString.subSequence(start, end);
                        if (textToClick.toString().equals(sequence.toString())) {
                            span.onClick(textView);
                            return;
                        }
                    }
                }

                // textToClick not found in TextView
                throw new NoMatchingViewException.Builder()
                        .includeViewHierarchy(true)
                        .withRootView(textView)
                        .build();

            }
        };
    }

    /*************************************************************************************
     *
     * Unused Matchers
     *
     *************************************************************************************/

    protected static TypeSafeMatcher<View> compareTextView(final String string) {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                String target = ((TextView) item).getText().toString();

                return target.equals(string);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("compareTextView");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkMaxLength(final int lines) {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                InputFilter[] filters = ((EditText) item).getFilters();
                InputFilter.LengthFilter lengthFilter =(InputFilter.LengthFilter) filters[0];

                return lengthFilter.getMax() == lines;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkMaxLength");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIfDisabled() {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {

                return !item.isEnabled();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIfDisabled");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkTextViewMaxLength(final int lines) {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                InputFilter[] filters = ((EditText) item).getFilters();
                InputFilter.LengthFilter lengthFilter =(InputFilter.LengthFilter) filters[0];

                return lengthFilter.getMax() == lines;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkTextViewMaxLength");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkEditTextMaxLength(final int lines) {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                InputFilter[] filters = ((EditText) item).getFilters();
                InputFilter.LengthFilter lengthFilter =(InputFilter.LengthFilter) filters[0];

                return lengthFilter.getMax() == lines;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkEditTextMaxLength");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIfTextEndsWith(final String suffix) {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                String target = ((TextView) item).getText().toString();
                return target.endsWith(suffix);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIfTextEndsWith");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIsChecked() {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                return ((SwitchButton) item).isChecked();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIsChecked");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkIsUnchecked() {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                return !((CheckBox) item).isChecked();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkIsUnchecked");
            }
        };
    }

    protected static TypeSafeMatcher<View> checkInputType(final int inputType) {
        return new TypeSafeMatcher<View>() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            protected boolean matchesSafely(View item) {
                return ((EditText) item).getInputType() == inputType;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("checkInputType");
            }
        };
    }
}
