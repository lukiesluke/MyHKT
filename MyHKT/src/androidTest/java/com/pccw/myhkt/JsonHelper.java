package com.pccw.myhkt;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

public class JsonHelper {

    public static String getUatMockData(Context context) throws JSONException, IOException {
        InputStream inputStream = context.getAssets().open("login_uat_mock_response.json");
        String jsonString = read(inputStream);
        JSONObject jsonObject = new JSONObject(jsonString);
        return jsonObject.toString();
    }

    public static String getMockData(Context context, String filename) throws JSONException, IOException {
        InputStream inputStream = context.getAssets().open(filename);
        String jsonString = read(inputStream);
        JSONObject jsonObject = new JSONObject(jsonString);
        return jsonObject.toString();
    }

    private static String read(InputStream inputStream) throws IOException {
        int size = inputStream.available();
        byte[] buffer = new byte[size];
        inputStream.read(buffer);
        inputStream.close();

        return new String(buffer, "UTF-8");
    }
}
