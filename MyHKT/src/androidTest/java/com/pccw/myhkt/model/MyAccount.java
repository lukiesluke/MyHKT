package com.pccw.myhkt.model;

public class MyAccount {
    private String accountId;
    private String accountLabel;
    private String nickname;
    private int accountIcon;
    private boolean newIconDisplayed;


    public MyAccount() {

    }

    public boolean isNewIconDisplayed() {
        return newIconDisplayed;
    }

    public void setNewIconDisplayed(boolean newIconDisplayed) {
        this.newIconDisplayed = newIconDisplayed;
    }

    public int getAccountIcon() {
        return accountIcon;
    }

    public void setAccountIcon(int accountIcon) {
        this.accountIcon = accountIcon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAccountLabel() {
        return accountLabel;
    }

    public void setAccountLabel(String accountLabel) {
        this.accountLabel = accountLabel;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
