package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ContactInfoMyProfileActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;
    private String notApplicable = "N/A";

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);

        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }else{
            Intents.init();
        }

        loginUser(context, shared,
                AccountHelper.getUATAccount7().getUsername(),
                AccountHelper.getUATAccount7().getPassword(),
                true);

        gotoMainMenuItem(3);
        clickString(context,R.string.myhkt_mycontactinfo_tabname);

        delay(TestTimeouts.API_TIMEOUTS,waitForViewEnabled());
    }

    @After
    public void tearDown() {
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.release();
    }

    @Test
    public void testTabs(){
        clickString(context, R.string.myhkt_myservicelist_tabname);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkViewIsDisplayed(R.id.myprofservlist_btn_update);
        clickString(context, R.string.myhkt_myloginprofile_tabname);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
        clickString(context, R.string.myhkt_mycontactinfo_tabname);
        delay(TestTimeouts.API_TIMEOUTS, waitForViewEnabled());
        checkViewIsDisplayed(R.id.myprofcontact_btn_update);
    }

    @Test
    public void testLiveChat(){
        checkLiveChat(context,R.string.LIVE_CHAT_DISCLAIMER,
                R.id.myprofcontact_btn_update);
    }

    @Test
    public void testBackBtn(){
        checkBackBtn(MainMenuActivity.class.getName(),3);
    }

    @Test
    public void testDisplayUI(){
        checkStringIsDisplayed(context, R.string.myhkt_myprof_title);
        findStringContaining(context, R.string.myhkt_myprof_title);
        backBtnExist();
        checkLiveChatBtnIsDisplayed();

        checkStringIsDisplayed(context, R.string.myhkt_myloginprofile_tabname);
        checkStringIsDisplayed(context, R.string.myhkt_myservicelist_tabname);
        checkStringIsDisplayed(context, R.string.myhkt_mycontactinfo_tabname);

        checkStringIsDisplayed(context, R.string.myhkt_myprofcontact_remark);

        checkViewIsDisplayed(R.id.myprofcontact_drg);
        checkViewIsNotDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_drg);
        checkViewIsDisplayed(R.id.myprofcontact_ims);
        checkViewIsDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_ims);
        checkViewIsDisplayed(R.id.myprofcontact_mob);
        checkViewIsNotDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_mob);

        checkViewIsDisplayed(R.id.myprofcontact_btn_update);

        hiddenEditContactInfo();
    }

    @Test
    public void testShowContactInfo(){
        clickView(R.id.myprofcontact_btn_update);
        shownEditContactInfo();
    }

    @Test
    public void testHideContactInfo(){
        clickView(R.id.myprofcontact_btn_update);
        clickView(R.id.myprofcontact_btn_cancel);
        hiddenEditContactInfo();
    }

    @Test
    public void testEmptyUpdateContactInfo(){
        clickView(R.id.myprofcontact_btn_update);

        clearTextView(R.id.myprofcontact_mob_et);
        clearTextView(R.id.myprofcontact_email_et);
        clearTextView(R.id.myprofcontact_daymob_et);
        clearTextView(R.id.myprofcontact_nightmob_et);
        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_SEL_TO_UPD);
        clickOK();
    }

    @Test
    public void testResidentialTelephoneLineEyeServices(){
        clickView(R.id.item_profilecontact_header,R.id.myprofcontact_drg);
        checkViewIsDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_drg);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_mobilenum,
                R.id.profilecontactInfo, R.id.myprofcontact_drg);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_officenum,
                R.id.profilecontactInfo, R.id.myprofcontact_drg);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_homenum,
                R.id.profilecontactInfo, R.id.myprofcontact_drg);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_email,
                R.id.profilecontactInfo, R.id.myprofcontact_drg);

        clickView(R.id.item_profilecontact_header,R.id.myprofcontact_drg);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkViewIsNotDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_drg);
    }

    @Test
    public void testNetvigatorNowTV(){
        clickView(R.id.item_profilecontact_header,R.id.myprofcontact_ims);
        checkViewIsNotDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_ims);

        clickView(R.id.item_profilecontact_header,R.id.myprofcontact_ims);
        checkViewIsDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_ims);

        checkStringIsDisplayed("64899811", R.id.item_profilecontact_mobilenum,
                R.id.profilecontactInfo, R.id.myprofcontact_ims);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_officenum,
                R.id.profilecontactInfo, R.id.myprofcontact_ims);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_homenum,
                R.id.profilecontactInfo, R.id.myprofcontact_ims);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_email,
                R.id.profilecontactInfo, R.id.myprofcontact_ims);
    }

    @Test
    public void test1010CSL(){
        clickView(R.id.item_profilecontact_header,R.id.myprofcontact_mob);
        checkViewIsDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_mob);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_mobilenum,
                R.id.profilecontactInfo, R.id.myprofcontact_mob);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_officenum,
                R.id.profilecontactInfo, R.id.myprofcontact_mob);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_homenum,
                R.id.profilecontactInfo, R.id.myprofcontact_mob);

        checkStringIsDisplayed(notApplicable, R.id.item_profilecontact_email,
                R.id.profilecontactInfo, R.id.myprofcontact_mob);

        clickView(R.id.item_profilecontact_header,R.id.myprofcontact_mob);
        checkViewIsNotDisplayed(R.id.profilecontactInfo,R.id.myprofcontact_mob);
    }

    @Test
    public void testEditContactInfo(){
        clickView(R.id.myprofcontact_btn_update);
        scrollToView(R.id.myprofcontact_ims_txt);
        checkViewIsNotChecked(R.id.myprofcontact_ims_checkbox);

        typeTextView(R.id.myprofcontact_mob_et,"64899811");
        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context, R.string.CTAM_SEL_TO_UPD);
        clickOK();

        clearTextView(R.id.myprofcontact_daymob_et);
        clearTextView(R.id.myprofcontact_nightmob_et);
        clearTextView(R.id.myprofcontact_email_et);

        clickView(R.id.myprofcontact_ims_checkbox);
        checkViewIsChecked(R.id.myprofcontact_ims_checkbox);

        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_REQ_OFCNUM);
        clickOK();

        typeTextView(R.id.myprofcontact_daymob_et,"64899811");

        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_REQ_HMNUM);
        clickOK();

        typeTextView(R.id.myprofcontact_nightmob_et,"64899811");
        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_REQ_EMAIL);
        clickOK();

        typeTextView(R.id.myprofcontact_email_et,"testing");
        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_IVEMAIL);
        clickOK();

        typeTextView(R.id.myprofcontact_mob_et,"6489981");
        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_IVMOB);
        clickOK();

        typeTextView(R.id.myprofcontact_mob_et,"64899811");
        typeTextView(R.id.myprofcontact_daymob_et,"6489981");

        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_IVOFCNUM);
        clickOK();

        typeTextView(R.id.myprofcontact_daymob_et,"64899811");
        typeTextView(R.id.myprofcontact_nightmob_et,"6489981");

        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_IVHMNUM);
        clickOK();

        typeTextView(R.id.myprofcontact_nightmob_et,"64899811");
        typeTextView(R.id.myprofcontact_email_et,"testingsr002@gmail.com");

        clickView(R.id.myprofcontact_btn_apply);

        checkDialogStringIsDisplayed(context,R.string.CTAM_CFMMSG);
        clickCANCEL();

        delay(TestTimeouts.DIALOG_TIMEOUTS);

        checkDialogStringIsDisplayed(context,R.string.CFMM_DISCARD);
        clickOK();

        checkStringHint(context,R.id.myprofcontact_mob_et,  R.string.CTAF_IM_MOBPH);
        checkStringHint(context,R.id.myprofcontact_email_et, R.string.CTAF_EMAIL);
        checkStringHint(context,R.id.myprofcontact_daymob_et, R.string.CTAF_IM_OFCPH);
        checkStringHint(context,R.id.myprofcontact_nightmob_et, R.string.CTAF_IM_HOMEPH);
    }

    public void hiddenEditContactInfo(){
        checkViewIsNotDisplayed(R.id.myprofcontact_mob_icon);
        checkViewIsNotDisplayed(R.id.myprofcontact_email_icon);
        checkViewIsNotDisplayed(R.id.myprofcontact_daymob_icon);
        checkViewIsNotDisplayed(R.id.myprofcontact_nightmob_icon);

        checkViewIsNotDisplayed(R.id.myprofcontact_mob_et);
        checkViewIsNotDisplayed(R.id.myprofcontact_email_et);
        checkViewIsNotDisplayed(R.id.myprofcontact_daymob_et);
        checkViewIsNotDisplayed(R.id.myprofcontact_nightmob_et);

        checkStringIsHidden(context,R.string.myhkt_myprofcontact_text);

        checkViewIsNotDisplayed(R.id.myprofcontact_drg_checkbox);
        checkViewIsNotDisplayed(R.id.myprofcontact_ims_checkbox);
        checkViewIsNotDisplayed(R.id.myprofcontact_mob_checkbox);
    }

    public void shownEditContactInfo(){
        checkViewIsDisplayed(R.id.myprofcontact_mob_icon);
        checkViewIsDisplayed(R.id.myprofcontact_email_icon);
        checkViewIsDisplayed(R.id.myprofcontact_daymob_icon);
        checkViewIsDisplayed(R.id.myprofcontact_nightmob_icon);

        checkViewIsDisplayed(R.id.myprofcontact_mob_et);
        checkViewIsDisplayed(R.id.myprofcontact_email_et);
        checkViewIsDisplayed(R.id.myprofcontact_daymob_et);
        checkViewIsDisplayed(R.id.myprofcontact_nightmob_et);

        checkStringHint(context,R.id.myprofcontact_mob_et, -1 , R.string.CTAF_IM_MOBPH);
        checkStringHint(context,R.id.myprofcontact_email_et, -1 , R.string.CTAF_EMAIL);
        checkStringHint(context,R.id.myprofcontact_daymob_et, -1 , R.string.CTAF_IM_OFCPH);
        checkStringHint(context,R.id.myprofcontact_nightmob_et, -1 , R.string.CTAF_IM_HOMEPH);

        scrollToView(R.id.myprofcontact_ims_txt);

        checkStringIsDisplayed(context,R.string.myhkt_myprofcontact_text);

        checkViewIsDisplayed(R.id.myprofcontact_drg_checkbox);
        checkViewIsDisabled(R.id.myprofcontact_drg_checkbox);
        checkViewIsDisplayed(R.id.myprofcontact_ims_checkbox);
        checkViewIsEnabled(R.id.myprofcontact_ims_checkbox);
        checkViewIsDisplayed(R.id.myprofcontact_mob_checkbox);
        checkViewIsDisabled(R.id.myprofcontact_mob_checkbox);
    }
}