package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.view.View;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;
import com.pccw.myhkt.util.Constant;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class SettingsActivityTest extends BaseTest {
    private Context context;
    private SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putInt(Constant.TOUCH_ID_WELCOME_TOUCH_ID_COUNTER, 3);
        editor.commit();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testLoggedOutDisplayUI() {
        openSettings();

        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);

        checkViewVisibility(R.id.navbar_button_left, R.id.navbar_base_layout, View.VISIBLE);

        checkViewVisibility(R.id.navbar_button_right, R.id.navbar_base_layout, View.VISIBLE);

        checkViewWithText(R.id.settings_dislang_text, context, R.string.PAMF_LANG);

        checkViewIsDisplayed(R.id.lrbutton_left,R.id.settings_lrbtn);

        checkViewWithText(R.id.lrbutton_left,R.id.settings_lrbtn, "中文");

        checkViewIsDisplayed(R.id.lrbutton_right,R.id.settings_lrbtn);

        checkViewWithText(R.id.lrbutton_right,R.id.settings_lrbtn, "English");

        checkViewVisibility(R.id.settings_touch_id_layout, View.GONE);

        checkViewVisibility(R.id.settings_newbill_layout, View.GONE);

        checkViewVisibility(R.id.settings_appt_layout, View.GONE);

        checkViewVisibility(R.id.settings_noti_layout, View.VISIBLE);

    }

    @Test
    public void testLoggedInDisplayUI() {
        loginUAT7();

        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);

        checkViewVisibility(R.id.navbar_button_left, R.id.navbar_base_layout, View.VISIBLE);

        checkViewVisibility(R.id.navbar_button_right, R.id.navbar_base_layout, View.VISIBLE);

        checkViewWithText(R.id.settings_dislang_text, context, R.string.PAMF_LANG);

        checkViewIsDisplayed(R.id.lrbutton_left,R.id.settings_lrbtn);

        checkViewWithText(R.id.lrbutton_left,R.id.settings_lrbtn, "中文");

        checkViewIsDisplayed(R.id.lrbutton_right,R.id.settings_lrbtn);

        checkViewWithText(R.id.lrbutton_right,R.id.settings_lrbtn, "English");

        checkViewVisibility(R.id.settings_touch_id_layout, View.VISIBLE);

        checkViewVisibility(R.id.settings_newbill_layout, View.VISIBLE);

        checkViewVisibility(R.id.settings_appt_layout, View.VISIBLE);

        checkViewVisibility(R.id.settings_noti_layout, View.VISIBLE);

    }

    @Test
    public void testChangeLanguage() {
        openSettings();

        clickView(R.id.lrbutton_left,R.id.settings_lrbtn);

        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);

        checkViewWithText(R.id.settings_dislang_text, "介面語言");

        clickString("English",R.id.settings_lrbtn);

        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);

        checkViewWithText(R.id.settings_dislang_text, "Display Language");

    }

    @Test
    public void testInfoBtnLoginWithFingerId() {
        loginUAT7();

        clickView(R.id.settings_help_touch_id);

        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));

        checkDialogStringIsDisplayed(context,R.string.fp_setting_fp_hint_normal);

        clickOK();

        executeLogout(context);

    }

    @Test
    public void testInfoBtnNewBillReminder() {
        loginUAT7();

        clickView(R.id.settings_help_newbill);

        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));

        checkStringIsDisplayed(context,R.string.MYHKT_SETTING_HINT_NEW_BILL);

        clickOK();

        executeLogout(context);

    }


    @Test
    public void testInfoBtnAppointmentReminder() {
        loginUAT7();

        clickView(R.id.settings_help_appt);

        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));

        checkDialogStringIsDisplayed(context,R.string.MYHKT_SETTING_HINT_NEW_APPT);

        clickOK();

        executeLogout(context);
    }

    @Test
    public void testInfoBtnReceiveServiceNoticeOnThisApp() {
        loginUAT7();

        clickView(R.id.settings_help_noti);

        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));

        checkDialogStringIsDisplayed("Once this function is activated and Google Play Services available, you\'d be able to receive push message related to our service.");

        clickOK();

        executeLogout(context);
    }

    @Test
    public void testSettingsLiveChatButton() {
        loginUAT7();

        clickView(R.id.navbar_button_right,R.id.navbar_base_layout);

        clickOK();

        checkStringIsDisplayed(context, R.string.MYHKT_BTN_LIVECHAT);

        clickView(R.id.dialog_livechat_close);

        clickOK();

        executeLogout(context);
    }

    @Test
    public void testSettingsBackButton() {
        Intents.init();

        loginUAT7();

        clickLeftNavBar();

        checkIntendedActivity(MainMenuActivity.class.getName());

        Intents.release();

        executeLogout(context);
    }

    public void loginUAT7(){
        clickLeftNavBar();

        clickString(context, R.string.myhkt_LGIF_LOGIN);

        loginMyCredentialsUAT(AccountHelper.getUATAccount7().getUsername(),
                AccountHelper.getUATAccount7().getPassword(), shared);

        openSettings();
    }

    public void openSettings(){
        clickLeftNavBar();

        clickString(context, R.string.myhkt_settings);

    }

}
