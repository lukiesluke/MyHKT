package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class LoginProfileMyProfileActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }else{
            Intents.init();
        }

        loginUser(context, shared,
                AccountHelper.getUATAccount7().getUsername(),
                AccountHelper.getUATAccount7().getPassword(),
                true);

        gotoMainMenuItem(3);
    }

    @After
    public void tearDown(){
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.release();
    }

    @Test
    public void testDisplayUI(){
        checkStringIsDisplayed(context, R.string.myhkt_myprof_title);
        backBtnExist();
        checkLiveChatBtnIsDisplayed();
        checkStringIsDisplayed(context,R.string.myhkt_myloginprofile_tabname);
        checkStringIsDisplayed(context,R.string.myhkt_myservicelist_tabname);
        checkStringIsDisplayed(context,R.string.myhkt_mycontactinfo_tabname);
        checkStringIsDisplayed(context,R.string.myhkt_myprof_remark);
        checkStringIsDisplayed(context,R.string.myhkt_myprof_loginid);
        checkViewIsDisplayed(R.id.myprofloginid_changepwd);
        checkViewString(R.id.myprofloginid_email,"testingsr002@gmail.com");
        checkStringIsDisplayed(context,R.string.myhkt_myprof_loginid);
        checkStringIsDisplayed(context,R.string.myhkt_myprof_nickname);
        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_name_et,"testingsr002");
        checkStringIsDisplayed(context,R.string.myhkt_myprof_email);
        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_email_et,"testingsr002@gmail.com");
        checkStringIsDisplayed(context,R.string.myhkt_myprof_mobnum);
        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et,"53359899");
    }

    @Test
    public void testHintClearFields(){
        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_name_et);
        checkStringHint(context, R.id.reg_input_layoutet,
                R.id.myprofloginid_name_et, R.string.myhkt_myprof_nickname_hint);

        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_email_et);
        checkStringHint(context, R.id.reg_input_layoutet,
                R.id.myprofloginid_email_et, R.string.myhkt_myprof_email_hint);

        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et);
        checkStringHint(context, R.id.reg_input_layoutet,
                R.id.myprofloginid_mobnum_et, R.string.myhkt_myprof_mobnum_hint);
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.REGM_ILNICKNAME);
        clickOK();
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
        checkViewIsDisplayed(R.id.myprofloginid_header);
    }

    @Test
    public void testCancelBtn(){
        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_name_et);
        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_email_et);
        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et);

        clickView(R.id.myprofloginid_btn_cancel);
        checkIntendedActivity(MainMenuActivity.class.getName());
        gotoMainMenuItem(3);

        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_name_et,"testingsr002");
        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_email_et,"testingsr002@gmail.com");
        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et,"53359899");
    }

    @Test
    public void testChangePasswordBtn(){
        clickView(R.id.myprofloginid_changepwd);
        checkViewIsDisplayed(R.id.myproflogin_changepw_header);
    }

    @Test
    public void testBackBtn(){
        checkBackBtn(MainMenuActivity.class.getName(),3);
    }

    @Test
    public void testLiveChat(){
        checkLiveChat(context,R.string.LIVE_CHAT_DISCLAIMER,
                R.id.myprofloginid_btn_update);
    }

    @Test
    public void testEmptyNicknameUpdate(){
        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_name_et);
        clickView(R.id.myprofloginid_btn_update);

        checkDialogStringIsDisplayed(context,R.string.PAMM_ILNICKNAME);
        clickOK();
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
    }
    @Test
    public void testEmptyMobileNumberUpdate(){
        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et);
        clickView(R.id.myprofloginid_btn_update);

        checkDialogStringIsDisplayed(context,R.string.PAMM_IVMOB);
        clickOK();
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
    }

    @Test
    public void testEmptyEmailUpdate(){
        clearTextView(R.id.reg_input_layoutet,R.id.myprofloginid_email_et);
        clickView(R.id.myprofloginid_btn_update);

        checkDialogStringIsDisplayed(context,R.string.PAMM_ILCTMAIL);
        clickOK();
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
    }

    @Test
    public void testNicknameUpdate(){
        typeTextView(R.id.reg_input_layoutet,R.id.myprofloginid_name_et,"change test");
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_DONE);
        clickOK();
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
        clickLeftNavBar();
        checkIntendedActivity(MainMenuActivity.class.getName());
        gotoMainMenuItem(3);
        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_name_et,"change test");

        typeTextView(R.id.reg_input_layoutet,R.id.myprofloginid_name_et,"testingsr002");
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_DONE);
        clickOK();
    }

    @Test
    public void testMobileNumberUpdate(){
        typeTextView(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et,"5335989");
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_IVMOB);
        clickOK();
        typeTextView(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et,"53359891");
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_DONE);
        clickOK();
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
        clickLeftNavBar();
        checkIntendedActivity(MainMenuActivity.class.getName());
        gotoMainMenuItem(3);
        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et,"53359891");
        typeTextView(R.id.reg_input_layoutet,R.id.myprofloginid_mobnum_et,"53359899");
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_DONE);
        clickOK();
    }

    @Test
    public void testEmailUpdate(){
        typeTextView(R.id.reg_input_layoutet,R.id.myprofloginid_email_et,"testings123");
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.REGM_IVCTMAIL);
        clickOK();
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);

        typeTextView(R.id.reg_input_layoutet,R.id.myprofloginid_email_et,"testing123@gmail.com");
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_DONE);
        clickOK();
        clickLeftNavBar();
        checkIntendedActivity(MainMenuActivity.class.getName());
        gotoMainMenuItem(3);
        checkViewString(R.id.reg_input_layoutet,R.id.myprofloginid_email_et,"testing123@gmail.com");
        checkViewString(R.id.myprofloginid_email,"testingsr002@gmail.com");
        typeTextView(R.id.reg_input_layoutet,R.id.myprofloginid_email_et,"testingsr002@gmail.com");
        clickView(R.id.myprofloginid_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_DONE);
        clickOK();
    }

    @Test
    public void testTabs() {
        clickString(context, R.string.myhkt_myservicelist_tabname);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkViewIsDisplayed(R.id.myprofservlist_btn_update);

        clickString(context, R.string.myhkt_mycontactinfo_tabname);
        delay(TestTimeouts.API_TIMEOUTS, waitForViewEnabled());
        checkViewIsDisplayed(R.id.myprofcontact_btn_update);

        clickString(context, R.string.myhkt_myloginprofile_tabname);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
    }
}