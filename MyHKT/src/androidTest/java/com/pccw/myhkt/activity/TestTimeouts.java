package com.pccw.myhkt.activity;

public class TestTimeouts {
    public static final int API_TIMEOUTS = 30000;
    public static final int LIVECHAT_TIMEOUTS = 10000;
    public static final int DIALOG_TIMEOUTS = 2000;
    public static final int ACTIVITY_LOAD_TIMEOUTS = 3000;
}
