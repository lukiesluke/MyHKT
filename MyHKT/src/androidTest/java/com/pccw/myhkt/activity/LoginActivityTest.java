package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<LoginActivity> activityTestRule = new ActivityTestRule<>(LoginActivity.class);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
    }

    @After
    public void tearDown(){
        Intents.release();
    }

    @Test
    public void verifyLoginUI() {
        checkStringIsDisplayed(context, R.string.myhkt_login_title);
        checkStringIsDisplayed(context, R.string.myhkt_login_registerintro);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_loginid);
        checkStringIsDisplayed(context, R.string.myhkt_login_userid_hint);
        checkStringIsDisplayed(context, R.string.myhkt_login_pw_txt);
        checkStringIsDisplayed(context, R.string.myhkt_LGIF_FORGOT);
        checkStringIsDisplayed(context, R.string.myhkt_login_remember_pw_txt);
        checkStringIsDisplayed(context, R.string.myhkt_login_login_txt);
        checkStringIsDisplayed(context, R.string.myhkt_login_reg_txt);
        checkStringIsDisplayed(context, R.string.login_bottom_des);
    }

    @Test
    public void testBackBtn() {
        clickLeftNavBar();
        checkIntendedActivity(MainMenuActivity.class.getName());
    }

    @Test
    public void testLoginWithoutEmailAddressAndPassword(){
        loginMyCredentials("", "", waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testLoginWithEmailAddressAndWithoutPassword() {
        loginMyCredentials(AccountHelper.getPRDAccount1().getUsername(), "", waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testLoginWithPasswordAndWithoutEmailAddress(){
        loginMyCredentials("", "1234", waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testChangeModeUATValidCredentials(){
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        checkIntendedActivity(checkFingerPrintConfirmation(shared) ?
                TouchIdWelcomeActivity.class.getName() : MainMenuActivity.class.getName());
        executeLogout(context);
    }

    @Test
    public void testChangeModeUATInvalidCredentials() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        clickOK();
    }

    @Test
    public void testChangeModePRDValidCredentials(){
        loginMyCredentialsPRD(AccountHelper.getPRDAccount1().getUsername(),
                AccountHelper.getPRDAccount1().getPassword(), shared);

        checkIntendedActivity(checkFingerPrintConfirmation(shared) ?
                TouchIdWelcomeActivity.class.getName() : MainMenuActivity.class.getName());
        executeLogout(context);
    }

    @Test
    public void testChangeModePRDInvalidCredentials() {
        loginMyCredentialsPRD(AccountHelper.getPRDAccount1().getUsername(), "k12979371234");
        clickOK();
    }

    @Test
    public void testLoginFromMyAccountRedirection() {
        executeRedirection(0, ServiceListActivity.class.getName());
    }

    @Test
    public void testLoginFromMyLineTestRedirection() {
        executeRedirection(1, ServiceListActivity.class.getName());
    }

    @Test
    public void testLoginFromProfileSettingsRedirection() {
        executeRedirection(3, MyProfileActivity.class.getName());
    }

    @Test
    public void testLoginFromIDDRedirection() {
        executeRedirection(6, IDDActivity.class.getName());
    }

    @Test
    public void testLoginFromSettingsRedirection() {
        executeSettings();
    }

    @Test
    public void testLoginForgotPasswordNavigation() {
        clickString(context, R.string.myhkt_LGIF_FORGOT);
        checkIntendedActivity(ForgetPasswordActivity.class.getName());
    }

    @Test
    public void testLoginRegisterNowNavigation() {
        clickString(context, R.string.myhkt_login_reg_txt);
        checkIntendedActivity(RegBasicActivity.class.getName());
    }

    @Test
    public void testLoginToggles() {
        typeTextView(R.id.reg_input_layoutet,R.id.login_input_pw,"k1297937");
        clickView(R.id.reg_input_button_show_pwd,R.id.login_input_pw);
        checkViewPasswordVisible(R.id.reg_input_layoutet,R.id.login_input_pw);
        clickView(R.id.reg_input_button_show_pwd,R.id.login_input_pw);
        checkViewPasswordHidden(R.id.reg_input_layoutet,R.id.login_input_pw);
    }

    public void executeRedirection(int position, String activityName) {
        switch(position) {
            case 0:
            case 1:
            case 3: executeLoginRedirect(position); break;
            case 6: clickLeftNavBar();
                    gotoMainMenuItem(position);
                    executeLiveChat();
                    if(checkFingerPrintConfirmation(shared))
                        closeMyFingerprintBecauseIDontNeedIt();
                    break;
//            case 11: executeLiveChatButton(); executeLoginRedirect(position); break;
        }
        checkIntendedActivity(activityName, position == 6 ? 2 : 1);
        executeLogout(context);
    }

    public void executeSettings() {
        clickLeftNavBar();
        clickLeftNavBar();
        clickString(context, R.string.myhkt_settings);
        executeLiveChat();
        if(checkFingerPrintConfirmation(shared))
            closeMyFingerprintBecauseIDontNeedIt();
        checkIntendedActivity(SettingsActivity.class.getName(), 2);
        executeLogout(context);
    }

    private void executeLiveChat() {
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        clickLiveChat();
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
    }

    private void clickLiveChat() {
        clickLiveChatNavBar();
    }

    private void executeLoginRedirect(int position) {
        clickLeftNavBar();
        gotoMainMenuItem(position);
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
    }

}