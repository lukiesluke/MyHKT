package com.pccw.myhkt.activity;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class MyLocationMapUIActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(
            MainMenuActivity.class);
    @Rule public GrantPermissionRule permissionRule = GrantPermissionRule.grant(
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        gotoMainMenuItem(10);
        clickViewInAdapter(R.id.shoptype_listview,0);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
    }

    @Test
    public void testDisplayUI() {
        checkStringIsDisplayed(context, R.string.myhkt_shop_title);
        checkViewIsDisplayed(R.id.navbar_button_left, R.id.navbar_base_layout);
        checkViewIsDisplayed(R.id.shopmyloc_mapview);
        checkStringIsDisplayed(context, R.string.myhkt_shop_myloc);
    }

    @Test
    public void testMyLocationClick() {
        clickString(context, R.string.myhkt_shop_myloc);
    }

    @Test
    public void testBackClick() {
        clickView(R.id.navbar_button_left, R.id.navbar_base_layout);
    }

}