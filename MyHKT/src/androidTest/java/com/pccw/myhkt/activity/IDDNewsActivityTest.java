package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class IDDNewsActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;
    
    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        gotoMainMenuItem(6);
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void testCheckIDDNewsDisplay() {
        checkStringIsDisplayed(context, R.string.MYHKT_IDD_NEWS_GLANCE);
        checkStringIsDisplayed(context, R.string.MYHKT_IDD_BTN_NEWS);
        checkStringIsDisplayed(context, R.string.MYHKT_IDD_NEWS_SERVICE);
        checkStringIsDisplayed(context, R.string.MYHKT_IDD_NEWS_BEST);
        checkStringIsDisplayed(context, R.string.MYHKT_IDD_NEWS_BEST);
        backBtnExist();
        checkLiveChatBtnIsDisplayed();

    }

    @Test
    public void testCheckTermsAndConditions() {
        clickString(context, R.string.MYHKT_IDD_NEWS_TNC);
        checkStringIsDisplayed(context, R.string.MYHKT_IDD_NEWS_TNC_TITLE);
        checkStringIsDisplayed(context, R.string.MYHKT_IDD_NEWS_TNC_CONTENT);
        clickOK();
    }

    @Test
    public void testCheckLiveChat() {
        clickLiveChatNavBar();
        checkIntendedActivity(LoginActivity.class.getName());
        loginUAT();
        checkLiveChat(context,R.string.LIVE_CHAT_DISCLAIMER,
                R.id.tv_idd_news_c22);
        executeLogout(context);
    }

    public void loginUAT(){
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
    }

}