package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;

@Ignore
@RunWith(AndroidJUnit4.class)
public class LineTestResultTest extends BaseTest {
    private Context context;
    SharedPreferences shared;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
    }

    @After
    public void tearDown() {
        executeLogout(context);
        Intents.release();
    }

    @Test
    public void displayUISuccess() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(),
                AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(1);
        clickViewInAdapter(R.id.servicelist_listView, 6);
        delay(10000);
        findStringContaining(context, R.string.LTTF_TEST).perform(click());
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        delay(60000);
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        // Display UI
        clickLeftNavBar();
    }

    @Test
    public void displayUIFailure() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(),
                AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(1);
        clickViewInAdapter(R.id.servicelist_listView, 8);
        delay(10000);
        findStringContaining(context, R.string.LTTF_TEST).perform(click());
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        delay(60000);
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        // Display UI
        clickLeftNavBar();
    }

    @Test
    public void displayUIWarning() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount9().getUsername(),
                AccountHelper.getUATAccount9().getPassword(), shared);
        gotoMainMenuItem(1);
        clickViewInAdapter(R.id.servicelist_listView, 0);
        delay(10000);
        findStringContaining(context, R.string.LTTF_TEST).perform(click());
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        delay(60000);
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        // Display UI
        clickLeftNavBar();
    }

    @Test
    public void successClickLater() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(),
                AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(1);
        clickViewInAdapter(R.id.servicelist_listView, 6);
        delay(10000);
        findStringContaining(context, R.string.LTTF_TEST).perform(click());
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        delay(60000);
        findStringContaining("Later").inRoot(isDialog()).perform(click());
    }

    @Ignore
    @Test
    public void successRebootModem() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(),
                AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(1);
        clickViewInAdapter(R.id.servicelist_listView, 6);
        delay(10000);
        findStringContaining(context, R.string.LTTF_TEST).perform(click());
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        delay(60000);
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        delay(5000);
        findStringContaining(context, R.string.reboot_start_btn).perform(click());
        delay(10000);
        clickLeftNavBar();
    }

    @Ignore
    @Test
    public void warningSRCreation() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount9().getUsername(),
                AccountHelper.getUATAccount9().getPassword(), shared);
        gotoMainMenuItem(1);
        clickViewInAdapter(R.id.servicelist_listView, 0);
        delay(10000);
        findStringContaining(context, R.string.LTTF_TEST).perform(click());
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        delay(60000);
        findStringContaining("OK").inRoot(isDialog()).perform(click());
        findStringContaining(context, R.string.MYHKT_LT_BTN_REPORT).perform(click());
        delay(10000);
        clickLeftNavBar();
    }

}