package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.Intent;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.MockUtils.TestMockData;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class RegConfirmActivityTest extends BaseTest {
    private Context context;

    @Rule
    public ActivityTestRule<RegConfirmActivity> activityTestRule = new ActivityTestRule<>(RegConfirmActivity.class, false, false);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();

        Intent myIntent = new Intent();
        myIntent.putExtra(RegBasicActivity.BUNDLE_CUSTREC, TestMockData.getUATCustRecObject());
        myIntent.putExtra(RegBasicActivity.BUNDLE_SVEEREC, TestMockData.getUATSveeRecObject());
        myIntent.putExtra(RegBasicActivity.BUNDLE_SUBNREC, TestMockData.getUATSubnRecObject());
        activityTestRule.launchActivity(myIntent);
    }

    @Test
    public void verifyRegConfirmUI() {
        checkStringIsDisplayed(context, R.string.myhkt_reg_title);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_head2);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_head1);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_hkid);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_loginid);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_nickname);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_reset);
        checkStringIsDisplayed(context, R.string.REGF_TNC_BTN);
        checkStringIsDisplayed(context, R.string.REGF_TNC_BTN_OPT);
    }

    @Test
    public void verifyAcceptEnabledWhenTicked() {
        clickView(R.id.regconfirm_checkbox);
        checkViewIsEnabled(R.id.regconfirm_btn_confirm);
    }

    @Test
    public void verifyAcceptEnabledWhenUnticked() {
        clickView(R.id.regconfirm_checkbox);
        clickView(R.id.regconfirm_checkbox);
        checkViewIsDisabled(R.id.regconfirm_btn_confirm);
    }

//    @Test
//    public void verifyAcceptEnabledWhenTickedLabel() {
//        checkStringIsDisplayed(R.string.REGF_TNC_BTN, R.id.regconfirm_relative_layout).perform(click());
//        clickLeftNavBar();
//        checkStringIsDisplayed(R.string.myhkt_confirm_submit).check(matches(checkIfEnabled()));
//    }

    @Test
    public void verifyLiveChatBrowser() {
        clickSpannableView(R.id.regconfirm_txt_agree, context, R.string.REGF_TNC_GC_LINK);
        executeLiveChat();
        checkStringIsDisplayed(context, R.string.LIVE_CHAT_DISCLAIMER);
    }

    @Test
    public void verifyGeneralConditionWorks() {
        clickSpannableView(R.id.regconfirm_txt_agree, context, R.string.REGF_TNC_GC_LINK);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_ack2);
        clickLeftNavBar();
    }

    @Test
    public void verifyPrivacyStatementWorks() {
        clickSpannableView(R.id.regconfirm_txt_agree, context, R.string.REGF_TNC_PS_LINK);
        checkStringIsDisplayed(context, R.string.myhkt_confirm_ack3);
        clickLeftNavBar();
    }

    @Test
    public void verifyPersonalInformationCollectionStatementWorks() {
        clickSpannableView(R.id.regconfirm_txt_agree, context, R.string.REGF_TNC_PICS_LINK);
        checkStringIsDisplayed(context, R.string.REGF_PIC_TITLE);
        clickLeftNavBar();
    }

//    @Test
//    public void takeALookMuna() throws InterruptedException {
//        Thread.sleep(Long.MAX_VALUE);
//    }

    @Test
    public void testLiveChatInquiryNavigation() {
        executeLiveChat();
        checkStringIsDisplayed(context, R.string.LIVE_CHAT_DISCLAIMER);
    }

    private void executeLiveChat() {
        clickLiveChatNavBar();
    }

}