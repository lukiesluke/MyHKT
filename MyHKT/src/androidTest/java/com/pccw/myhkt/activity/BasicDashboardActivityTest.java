package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.view.View;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;
import com.pccw.myhkt.mymob.activity.MyMobileActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class BasicDashboardActivityTest extends BaseTest {
    private Context context;
    private SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }else{
            Intents.init();
        }

        loginUser(context, shared,
                AccountHelper.getPRDAccount1().getUsername(),
                AccountHelper.getPRDAccount1().getPassword(),
                false);
    }

    @After
    public void tearDown(){
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.release();
    }

    @Test
    public void testNormalDisplayUI(){
        checkIntendedActivity(MainMenuActivity.class.getName());
        checkStringIsDisplayed(context,R.string.app_name);
        checkViewVisibility(R.id.navbar_button_left,
                R.id.navbar_base_layout, View.VISIBLE);
        checkViewVisibility(R.id.navbar_message_icon,
                R.id.navbar_base_layout, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_topbanner, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_pccwlogo, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_pccwlogo1, View.VISIBLE);
        checkViewStartsWith(R.id.mainmenu_label_welcome, determineGreetingTime());
        checkViewVisibility(R.id.mainmenu_gridlayout, View.VISIBLE);
    }

    @Test
    public void testNormalMyAccount() {
        executeMainMenu(0, ServiceListActivity.class.getName());
    }

    @Test
    public void testNormalMyLineTest() {
        executeMainMenu(1, ServiceListActivity.class.getName());
    }

    @Test
    public void testNormalMyAppointment() {
        executeMainMenu(2, MyApptActivity.class.getName());
    }

    @Test
    public void testNormalProfileSettings() {
        executeMainMenu(3, MyProfileActivity.class.getName());
    }

    @Test
    public void testNormalMyMobile() {
        executeMainMenu(4, MyMobileActivity.class.getName());
    }

    @Test
    public void testNormalDirectoryInquiries() {
        executeMainMenu(5, DirectoryInquiryActivity.class.getName());
    }

    @Test
    public void testNormalIDD0060() {
        executeMainMenu(6, IDDActivity.class.getName());
    }

    @Test
    public void testNormalTheClub() {
        gotoMainMenuItem(7);
        clickString(context,R.string.btn_cancel);
//        findStringContaining("OK").perform(click());
//        intended(allOf(
//                hasAction(Intent.ACTION_VIEW),
//                hasData(Uri.parse("http://play.google.com/store/apps/details?id=com.pccw.theclub"))
//        ));
//        UiDevice.getInstance(getInstrumentation()).pressBack();
    }

    @Test
    public void testNormalTapNGo() {
        gotoMainMenuItem(8);
        clickString(context,R.string.btn_cancel);
//        findStringContaining("OK").perform(click());
//        intended(allOf(
//                hasAction(Intent.ACTION_VIEW),
//                hasData(Uri.parse("http://play.google.com/store/apps/details?id=com.hktpayment.tapngo"))
//        ));
//        UiDevice.getInstance(getInstrumentation()).pressBack();
    }

    @Test
    public void testNormalHKTShop() {
        gotoMainMenuItem(9);
        clickString(context,R.string.btn_cancel);
//        findStringContaining("OK").perform(click());
//        intended(allOf(
//                hasAction(Intent.ACTION_VIEW),
//                hasData(Uri.parse("http://play.google.com/store/apps/details?id=com.hkt.android.hktshop"))
//        ));
//        UiDevice.getInstance(getInstrumentation()).pressBack();
    }

    @Test
    public void testNormalShops() {
        executeMainMenu(10, ShopActivity.class.getName());
    }

    @Test
    public void testNormalContactUs() {
        executeMainMenu(11, ContactUsNewActivity.class.getName());
    }

    @Test
    public void testNormalBannerClick() {
        clickView(R.id.mainmenu_topbanner);
        activateMyFingerprintLater();
    }

    @Test
    public void testNormalMyMessagesNavigation() {
        clickView(R.id.navbar_message_icon, R.id.navbar_base_layout);
    }

    @Test
    public void testNormalMenuClick() {
        clickLeftNavBar();
        checkStringIsDisplayed(context,R.string.SHLF_LOGOUT);
        checkStringIsDisplayed(context,R.string.myhkt_settings);
        checkStringIsDisplayed(context,R.string.myhkt_about);
        checkStringIsDisplayed(context,R.string.myhkt_menu_tnc);
        checkStringIsDisplayed(context,R.string.title_inbox);

        clickLeftNavBar();
    }

    @Test
    public void testNormalMyMessagesHamburger()  {
        clickLeftNavBar();
        clickString(context, R.string.title_inbox);
        checkIntendedActivity(InboxListActivity.class.getName());
    }

    @Test
    public void testNormalSettings()  {
        clickLeftNavBar();
        clickString(context, R.string.myhkt_settings);
        checkIntendedActivity(SettingsActivity.class.getName());
    }

    @Test
    public void testNormalAboutMyHKT()  {
        clickLeftNavBar();
        clickString(context, R.string.myhkt_about);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        clickOK();
        clickLeftNavBar();
    }

    @Test
    public void testNormalTermsAndConditions()  {
        clickLeftNavBar();
        clickString(context, R.string.myhkt_menu_tnc);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        clickOK();
        clickLeftNavBar();
    }

    @Test
    public void testNormalLogout()  {
        clickLeftNavBar();
        clickString(context, R.string.SHLF_LOGOUT);
        checkStringIsDisplayed(context, R.string.myhkt_confirmlogout);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        clickString(context, R.string.myhkt_btn_yes);
    }

    private String determineGreetingTime() {
        String greeting;
        Calendar cal = Calendar.getInstance();
        int rHr = cal.get(Calendar.HOUR_OF_DAY);
        if (rHr >= 5 && rHr < 12) {
            greeting = context.getResources().getString(R.string.SHLF_MORNING);
        } else {
            if (rHr >= 12 && rHr < 18) {
                greeting = context.getResources().getString(R.string.SHLF_AFTERNOON);
            } else {
                greeting = context.getResources().getString(R.string.SHLF_NIGHT);
            }
        }
        return greeting;
    }

    private void executeMainMenu(int position, String activityName) {
        gotoMainMenuItem(position);
        checkIntendedActivity(activityName);
        if(position == 2) {
            delay(TestTimeouts.API_TIMEOUTS);
        }
    }

    private void activateMyFingerprintLater() {
        clickString(context, R.string.fp_welcome_btn_later);
    }

}
